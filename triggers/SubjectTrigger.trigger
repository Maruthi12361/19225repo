trigger SubjectTrigger on hed__Course__c (after insert, after update)
{
    new SubjectTriggerHandler().run();
}