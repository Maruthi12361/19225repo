trigger SubjectEnrolmentTrigger on hed__Course_Enrollment__c (
    before insert,
    before update,
    before delete,
    after insert,
    after update,
    after delete,
    after undelete)
{
    SubjectEnrolmentTriggerHandler triggerHandler = new SubjectEnrolmentTriggerHandler();
    triggerHandler.run();
}