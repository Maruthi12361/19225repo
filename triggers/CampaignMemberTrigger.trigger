trigger CampaignMemberTrigger on CampaignMember (after delete) {

    CampaignMemberTriggerHandler campaignMemberHandler = new CampaignMemberTriggerHandler();
    campaignMemberHandler.run();
}