trigger SkillSetTrigger on SkillSet__c (after insert, after update, after delete)
{
    new SkillSetTriggerHandler().run();
}