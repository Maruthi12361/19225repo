trigger TaskTrigger on Task (before insert, before update, after insert, after update)
{
    TaskTriggerHandler handler = new TaskTriggerHandler();
    handler.run();
}