trigger CaseEnrolmentTrigger on CaseEnrolment__c (after insert, after update, after delete)
{
    new CaseEnrolmentTriggerHandler().run();
}