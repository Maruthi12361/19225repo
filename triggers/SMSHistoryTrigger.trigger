trigger SMSHistoryTrigger on smagicinteract__smsMagic__c (after insert, after update)
{
    new SMSTriggerHandler().run();
}