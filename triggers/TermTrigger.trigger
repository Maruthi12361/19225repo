trigger TermTrigger on hed__Term__c (after update)
{
    new TermTriggerHandler().run();
}