trigger InterestingMomentTrigger on InterestingMoment__c (after insert)
{
    new InterestingMomentTriggerHandler().run();
}