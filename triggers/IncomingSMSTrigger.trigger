trigger IncomingSMSTrigger on smagicinteract__Incoming_SMS__c (after insert, after update) 
{
    new SMSTriggerHandler().run();
}