trigger NVMCallSummaryTrigger on NVMStatsSF__NVM_Call_Summary__c (before insert, before update, before delete, after insert, after update, after delete, after undelete)
{
    NVMCallSummaryTriggerHandler handler = new NVMCallSummaryTriggerHandler();
    handler.run();
}