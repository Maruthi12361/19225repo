trigger ContactReferrerTrigger on ContactReferrer__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) 
{
    ContactReferrerTriggerHandler handler = new ContactReferrerTriggerHandler();
    handler.run();
}