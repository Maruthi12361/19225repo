trigger PlanRequirementTrigger on hed__Plan_Requirement__c (after insert, after update, after delete)
{
    new PlanRequirementTriggerHandler().run();
}