trigger AffiliationTrigger on hed__Affiliation__c (before insert, before update, before delete, after insert, after update, after delete, after undelete)
{
    AffiliationTriggerHandler handler = new AffiliationTriggerHandler();
    handler.run();
}