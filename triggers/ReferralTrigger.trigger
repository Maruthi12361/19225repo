trigger ReferralTrigger on Referral__c (
    before insert,
    before update,
    before delete,
    after insert,
    after update,
    after delete,
    after undelete) 
{
    ReferralTriggerHandler triggerHandler = new ReferralTriggerHandler();
    triggerHandler.run();
}