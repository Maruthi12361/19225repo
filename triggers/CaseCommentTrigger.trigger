trigger CaseCommentTrigger on CaseComment (before insert, 
                                           before update, 
                                           before delete, 
                                           after insert, 
                                           after update, 
                                           after delete, 
                                           after undelete) 
{
    CaseCommentTriggerHandler caseCommentHandler = new CaseCommentTriggerHandler();
    caseCommentHandler.run();
}