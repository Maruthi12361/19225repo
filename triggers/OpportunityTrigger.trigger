trigger OpportunityTrigger on Opportunity (before insert, before update, before delete, after insert, after update, after delete, after undelete) 
{
    OpportunityTriggerHandler triggerHandler = new OpportunityTriggerHandler();
    triggerHandler.run();
}