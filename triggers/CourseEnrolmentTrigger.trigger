trigger CourseEnrolmentTrigger on hed__Program_Enrollment__c (after insert, after update, after delete)
{
    new CourseEnrolmentTriggerHandler().run();
}