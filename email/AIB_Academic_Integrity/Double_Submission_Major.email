<table border="0" cellpadding="5" width="550" cellspacing="5" height="400" >
<tr valign="top" height="400" >
<td tEditID="c1r1" style=" background-color:#FFFFFF; color:#000000; bEditID:r3st1; bLabel:main; font-size:12pt; font-family:arial;" aEditID="c1r1" locked="0" >
<![CDATA[<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal"><span style="font-size: 10.5pt; font-family: Verdana, sans-serif; background: white;">{!TEXT(DAY(TODAY()))} {!CASE(MONTH(TODAY()),1,"January",2,"February",3,"March",4,"April",5,"May",6,"June",7,"July",8,"August",9,"September",10,"October",11,"November","December")} {!TEXT(YEAR(TODAY()))}</span><span style="font-size: 10.5pt; font-family: Verdana, sans-serif;"><o:p></o:p></span></p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal;background:white"><span style="font-size: 10.5pt; font-family: Verdana, sans-serif;">{!Contact.Salutation} {!Contact.FirstName}
{!Contact.LastName}<o:p></o:p></span></p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal;background:white"><span style="font-size: 10.5pt; font-family: Verdana, sans-serif;">By Email: {!Contact.Email}<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal;background:white"><span style="font-size: 10.5pt; font-family: Verdana, sans-serif;"><br></span></p>

<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal"><span style="font-size: 10.5pt; font-family: Verdana, sans-serif;">Dear&nbsp;</span><span style="font-size: 10.5pt; font-family: Verdana, sans-serif; background: white;">{!Contact.Salutation} {!Contact.LastName},</span><span style="font-family: Arial, sans-serif; background: white;"><o:p></o:p></span></p><p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal"><span style="font-size: 10.5pt; font-family: Verdana, sans-serif; background: white;"><br></span></p>

<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal"><b><span style="font-size: 10.5pt; font-family: Verdana, sans-serif;">Re: Breach of AIB Academic Integrity
Policy</span></b><span style="font-size: 10.5pt; font-family: Verdana, sans-serif;"><o:p></o:p></span></p><p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal"><b><span style="font-size: 10.5pt; font-family: Verdana, sans-serif;"><br></span></b></p>

<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal"><span style="font-size: 10.5pt; font-family: Verdana, sans-serif;">We are writing with respect to
your&nbsp;</span><span style="font-size: 10.5pt; font-family: Verdana, sans-serif; background: white;">{!Case.SubjectEnrolmentSummary__c}</span><span style="font-family: Arial, sans-serif; background: white;"> </span><span style="font-size: 10.5pt; font-family: Verdana, sans-serif;">assignment, submitted on&nbsp;<span style="background:yellow">[date]</span>.<br>
<br>
Assignments, including Projects, submitted to AIB are electronically checked
for possible plagiarism through the use of advanced text-matching software.
Following the submission of your assignment an unusually high score has been
received indicating the potential for academic misconduct to have
occurred.&nbsp;<br>
<br>
A copy of the originality report is attached which shows your similarity result
and highlights the&nbsp;areas where a direct match has occurred.<br>
<br>
Assignments which return a high score are checked by one of our specialist
Academics to ascertain if there are clear indications within the assignment
which could account for the returned score. This process has recognised that
high proportions of this assignment directly match your&nbsp;<span style="background:yellow">[original subject]</span>&nbsp;assignment. Whilst
students are able to utilise their organisational experience for multiple
assignments, this practical experience must be tailored to the specific
requirements of each subject. Therefore, the text within each assignment should
be unique to that piece of work, unless agreed upon with the facilitators of
the different subjects as outlined within AIB’s Academic Integrity policy and
Assignment guide.&nbsp;<br>
<br>
As AIB students may only submit a piece of work once, this assignment is
considered to be a Double Submission, as described within our Academic
Integrity policy. This current high similarity result in addition to your
previous record of Academic Misconduct as advised on&nbsp;<span style="background:yellow">XXXX</span>, is considered Major Academic Misconduct.
Due to the severity of this breach, the following penalties have been applied:<o:p></o:p></span></p>

<ul type="disc">
 <li class="MsoNormal" style="line-height: normal;"><span style="font-size:10.5pt;font-family:&quot;Verdana&quot;,sans-serif;mso-fareast-font-family:
     &quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;mso-fareast-language:
     EN-AU">Your assignment will not be sent for marking, and<o:p></o:p></span></li>
 <li class="MsoNormal" style="line-height: normal;"><span style="font-size:10.5pt;font-family:&quot;Verdana&quot;,sans-serif;mso-fareast-font-family:
     &quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;mso-fareast-language:
     EN-AU">You will be awarded zero (0) for the entire subject (any exam which
     has already been undertaken will be discarded).<o:p></o:p></span></li>
</ul>

<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal"><span style="font-size: 10.5pt; font-family: Verdana, sans-serif;">You are therefore required to re-enrol
in the subject, paying the relevant enrolment fee. You are also required to
submit a completely new assignment and to undertake the final assessment within your
re-enrolment period.<br>
<br>
It is important to ensure that your future assignments do not breach our&nbsp;</span><a href="http://www.aib.edu.au/policies-and-procedures/"><span style="font-size:
10.5pt;font-family:&quot;Verdana&quot;,sans-serif;mso-fareast-font-family:&quot;Times New Roman&quot;;
mso-bidi-font-family:&quot;Times New Roman&quot;;color:blue;mso-fareast-language:EN-AU">AIB
Academic Integrity Policy</span></a><span style="font-size: 10.5pt; font-family: Verdana, sans-serif;">&nbsp;(available via
the AIB website) and we strongly encourage you to contact us to discuss the
result of this report, so that we can support you to avoid any future breach of
this policy. &nbsp;It is also essential to be aware that any further breach of
our Academic Integrity policy may result in your expulsion from the course.<br>
<br>
Yours sincerely<br>
<br></span></p><p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal"><span style="font-size: 12pt; font-family: Arial, sans-serif;">Kristine Valerio<o:p></o:p></span></p><p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal">

<span style="font-size: 12pt; line-height: 107%; font-family: Arial, sans-serif;">Senior Academic Operations
Specialist</span></p>

<p class="MsoNormal"><o:p>&nbsp;</o:p></p>]]></td>
</tr>
</table>