<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
    <!--[if gte mso 9]><xml>
      <o:OfficeDocumentSettings>
        <o:AllowPNG/>
        <o:PixelsPerInch>96</o:PixelsPerInch>
      </o:OfficeDocumentSettings>
    </xml><![endif]-->
    <!--[if gte mso 9]>
    <style type="text/css">
    h1,h2,h3,h4,h5,h6,p,a,span,td,strong{font-family: Helvetica, Arial, Verdana, sans-serif !important;}
    </style>
    <![endif]-->
    <!-- / Outlook -->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width">
    <title></title>
    <style type="text/css">
        body {
            background-color: #ffffff;
        }

        a:hover {
            color: #2795b6 !important;
        }

        a:active {
            color: #2795b6 !important;
        }

        a:visited {
            color: #2ba6cb !important;
        }

        h1 a:active {
            color: #2ba6cb !important;
        }

        h2 a:active {
            color: #2ba6cb !important;
        }

        h3 a:active {
            color: #2ba6cb !important;
        }

        h4 a:active {
            color: #2ba6cb !important;
        }

        h5 a:active {
            color: #2ba6cb !important;
        }

        h6 a:active {
            color: #2ba6cb !important;
        }

        h1 a:visited {
            color: #2ba6cb !important;
        }

        h2 a:visited {
            color: #2ba6cb !important;
        }

        h3 a:visited {
            color: #2ba6cb !important;
        }

        h4 a:visited {
            color: #2ba6cb !important;
        }

        h5 a:visited {
            color: #2ba6cb !important;
        }

        h6 a:visited {
            color: #2ba6cb !important;
        }

        table.button:hover td {
            background: #2795b6 !important;
        }

        table.button:visited td {
            background: #2795b6 !important;
        }

        table.button:active td {
            background: #2795b6 !important;
        }

        table.button:hover td a {
            color: #fff !important;
        }

        table.button:visited td a {
            color: #fff !important;
        }

        table.button:active td a {
            color: #fff !important;
        }

        table.button:hover td {
            background: #2795b6 !important;
        }

        table.tiny-button:hover td {
            background: #2795b6 !important;
        }

        table.small-button:hover td {
            background: #2795b6 !important;
        }

        table.medium-button:hover td {
            background: #2795b6 !important;
        }

        table.large-button:hover td {
            background: #2795b6 !important;
        }

        table.button:hover td a {
            color: #ffffff !important;
        }

        table.button:active td a {
            color: #ffffff !important;
        }

        table.button td a:visited {
            color: #ffffff !important;
        }

        table.tiny-button:hover td a {
            color: #ffffff !important;
        }

        table.tiny-button:active td a {
            color: #ffffff !important;
        }

        table.tiny-button td a:visited {
            color: #ffffff !important;
        }

        table.small-button:hover td a {
            color: #ffffff !important;
        }

        table.small-button:active td a {
            color: #ffffff !important;
        }

        table.small-button td a:visited {
            color: #ffffff !important;
        }

        table.medium-button:hover td a {
            color: #ffffff !important;
        }

        table.medium-button:active td a {
            color: #ffffff !important;
        }

        table.medium-button td a:visited {
            color: #ffffff !important;
        }

        table.large-button:hover td a {
            color: #ffffff !important;
        }

        table.large-button:active td a {
            color: #ffffff !important;
        }

        table.large-button td a:visited {
            color: #ffffff !important;
        }

        table.secondary:hover td {
            background: #d0d0d0 !important;
            color: #555;
        }

            table.secondary:hover td a {
                color: #555 !important;
            }

        table.secondary td a:visited {
            color: #555 !important;
        }

        table.secondary:active td a {
            color: #555 !important;
        }

        table.success:hover td {
            background: #457a1a !important;
        }

        table.alert:hover td {
            background: #970b0e !important;
        }

        .image-container img {
            outline: none;
            text-decoration: none;
            -ms-interpolation-mode: bicubic;
            width: 100% !important;
            max-width: 100%;
            clear: both;
            display: block;
            margin: 0 auto;
        }

        .product-image img {
            outline: none;
            text-decoration: none;
            -ms-interpolation-mode: bicubic;
            width: auto;
            max-width: 100%;
            clear: both;
            display: block;
        }

        #social-media img {
            outline: none;
            text-decoration: none;
            -ms-interpolation-mode: bicubic;
            width: auto;
            max-width: 100%;
            clear: both;
            display: inline-block;
            border: none;
        }

        @media only screen and (max-width: 600px) {
            table[class="body"] img {
                width: auto !important;
                height: auto !important;
            }

            table[class="body"] .images-height img {
                height: 1px !important;
                width: 96% !important;
            }

            table[class="body"] center {
                min-width: 0 !important;
            }

            table[class="body"] .logo-container {
                min-width: 0 !important;
            }

            table[class="body"] .container {
                width: 95% !important;
            }

            table[class="body"] .row {
                width: 100% !important;
                display: block !important;
            }

            table[class="body"] .wrapper {
                display: block !important;
                padding-right: 0 !important;
            }

            table[class="body"] .columns {
                table-layout: fixed !important;
                float: none !important;
                width: 100% !important;
                padding-right: 0px !important;
                padding-left: 0px !important;
                display: block !important;
            }

            table[class="body"] .column {
                table-layout: fixed !important;
                float: none !important;
                width: 100% !important;
                padding-right: 0px !important;
                padding-left: 0px !important;
                display: block !important;
            }

            table[class="body"] .wrapper.first .columns {
                display: table !important;
            }

            table[class="body"] .wrapper.first .column {
                display: table !important;
            }

            table[class="body"] table.columns td {
                width: 100% !important;
            }

            table[class="body"] table.column td {
                width: 100% !important;
            }

            table[class="body"] td.offset-by-one {
                padding-left: 0 !important;
            }

            table[class="body"] td.offset-by-two {
                padding-left: 0 !important;
            }

            table[class="body"] td.offset-by-three {
                padding-left: 0 !important;
            }

            table[class="body"] td.offset-by-four {
                padding-left: 0 !important;
            }

            table[class="body"] td.offset-by-five {
                padding-left: 0 !important;
            }

            table[class="body"] td.offset-by-six {
                padding-left: 0 !important;
            }

            table[class="body"] td.offset-by-seven {
                padding-left: 0 !important;
            }

            table[class="body"] td.offset-by-eight {
                padding-left: 0 !important;
            }

            table[class="body"] td.offset-by-nine {
                padding-left: 0 !important;
            }

            table[class="body"] td.offset-by-ten {
                padding-left: 0 !important;
            }

            table[class="body"] td.offset-by-eleven {
                padding-left: 0 !important;
            }

            table[class="body"] .expander {
                width: 9999px !important;
            }

            table[class="body"] .right-text-pad {
                padding-left: 10px !important;
            }

            table[class="body"] .text-pad-right {
                padding-left: 10px !important;
            }

            table[class="body"] .left-text-pad {
                padding-right: 10px !important;
            }

            table[class="body"] .text-pad-left {
                padding-right: 10px !important;
            }

            table[class="body"] .hide-for-small {
                display: none !important;
            }

            table[class="body"] .show-for-desktop {
                display: none !important;
            }

            table[class="body"] .show-for-small {
                display: inherit !important;
            }

            table[class="body"] .hide-for-desktop {
                display: inherit !important;
            }

            *[id]#center-table {
                width: 250px !important;
                margin: 0 auto;
                text-align:;
            }

            *[id]#center-table1 {
                width: 250px !important;
                margin: 0 auto;
            }

            *[id]#center-table2 {
                width: 250px !important;
                margin: 0 auto;
            }

            *[class].images-height {
                height: 1px !important;
                width: 96% !important;
            }

            *[class].no-pad {
                padding-left: 0px !important;
            }

            *[id]#center-table1 .table-images {
                text-align: left !important;
            }

            *[id]#center-table2 .table-images {
                padding-top: 10px !important;
            }
        }

        @media only screen and (max-width: 479px) {
            table[class="body"] .images-height img {
                height: 1px !important;
                width: 94% !important;
            }
        }
    </style>
</head>
<body style="width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; color:#7b7b7b; font-family:'Helvetica', 'Arial', sans-serif; font-weight:normal; text-align:left; line-height:19px; font-size:14px; margin:0; padding:0; background-color:#ffffff; ">
    <table class="body" style="border-spacing:0; border-collapse:collapse; vertical-align:top; text-align:left; height:100%; width:100%; color:#7b7b7b; font-family:'Helvetica', 'Arial', sans-serif; font-weight:normal; line-height:19px; font-size:14px; margin:0; padding:0; ">
        <tbody>
            <tr style="vertical-align:top; text-align:left; padding:0; " align="left">
                <td class="center" align="left" valign="top" style="word-break:break-word; -webkit-hyphens:auto; -moz-hyphens:auto; hyphens:auto; border-collapse:collapse !important; vertical-align:top; padding:0; ">
                    <table class="container" style="border-spacing:0; border-collapse:collapse; vertical-align:top; text-align:inherit; width:580px; margin:0; padding:0; background-color:#ffffff; ">
                        <tbody>
                            <tr style="vertical-align:top; text-align:left; padding:0; " align="left">
                                <td class="wrapper last" style="word-break:break-word; -webkit-hyphens:auto; -moz-hyphens:auto; hyphens:auto; border-collapse:collapse !important; vertical-align:top; text-align:left; position:relative; padding:10px 0px 0px; " align="left" valign="top">
                                    <table class="twelve columns" style="border-spacing:0; border-collapse:collapse; vertical-align:top; text-align:left; width:580px; margin:0 auto; padding:0; ">
                                        <tbody>
                                            <tr style="vertical-align:top; text-align:left; padding:0; " align="left">
                                                <td width="100%" class="center image-container" stylek="word-break:break-word; -webkit-hyphens:auto; -moz-hyphens:auto; hyphens:auto; border-collapse:collapse !important; vertical-align:top; text-align:center; padding:0px; width:100% !important; " align="center" valign="top">
                                                    <center style="">
                                                        <div class="mktEditable logo-container mktEditable logo-container" id="logo-container" style="">
                                                            <a href="http://www.aib.edu.au"><img src="https://s3-ap-southeast-2.amazonaws.com/aib-marketing/marketo/files/EM-AIBv1-logo-tagline.png" alt="Australian Institute of Business" border="0"></a>
                                                        </div>
                                                    </center>
                                                </td>
                                                <td class="expander" style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; visibility: hidden; width: 0px; padding: 0;" align="left" valign="top"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr style="vertical-align:top; text-align:left; padding:0; " align="left">
                                <td class="wrapper last" style="word-break:break-word; -webkit-hyphens:auto; -moz-hyphens:auto; hyphens:auto; border-collapse:collapse !important; vertical-align:top; text-align:left; position:relative; padding:0px; " align="left" valign="top">
                                    <table class="twelve columns" style="border-spacing:0; border-collapse:collapse; vertical-align:top; text-align:left; width:580px; padding:0; ">
                                        <tbody>
                                            <tr style="vertical-align:top; text-align:left; padding:0; " align="left">
                                                <td class="center image-container" style="word-break:break-word; -webkit-hyphens:auto; -moz-hyphens:auto; hyphens:auto; border-collapse:collapse !important; vertical-align:top; text-align:center; padding:0px; width:100% !important; " align="center" valign="top">
                                                    <div class="mktEditable" id="banner1" style=""></div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr style="vertical-align:top; text-align:left; padding:0; " align="left">
                                <td style="word-break:break-word; -webkit-hyphens:auto; -moz-hyphens:auto; hyphens:auto; border-collapse:collapse !important; vertical-align:top; text-align:left; padding:0; " align="left" valign="top">
                                    <table class="row" style="border-spacing:0; border-collapse:collapse; vertical-align:top; text-align:left; width:100%; position:relative; display:block; padding:0px; ">
                                        <tbody>
                                            <tr style="vertical-align:top; text-align:left; padding:0; " align="left">
                                                <td class="wrapper last" style="word-break:break-word; -webkit-hyphens:auto; -moz-hyphens:auto; hyphens:auto; border-collapse:collapse !important; vertical-align:top; text-align:left; position:relative; padding:0px 0px; " align="left" valign="top">
                                                    <table class="twelve columns" style="border-spacing:0; border-collapse:collapse; vertical-align:top; text-align:left; width:580px; margin:0 auto; padding:0; ">
                                                        <tbody>
                                                            <tr style="vertical-align:top; text-align:left; padding:0; " align="left">
                                                                <td style="word-break:break-word; -webkit-hyphens:auto; -moz-hyphens:auto; hyphens:auto; border-collapse:collapse !important; vertical-align:top; text-align:left; color:#7b7b7b; font-family:'Helvetica', 'Arial', sans-serif; font-weight:normal; line-height:19px; font-size:14px; padding:15px 30px 5px 30px; " align="left">
                                                                    <div class="mktEditable" id="content1" style="">
                                                                        <p><span style="color: #000000;"><strong></strong></span><strong><span style="color: #000000;"></span></strong><span style="color: #000000;"><strong>SALES INSIGHTS ALERT:</strong></span></p>
                                                                        <p style="color: #7b7b7b; font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial;text-decoration-color: initial;">
                                                                            <span style="color: #000000;">Hi&nbsp;{!Contact.OwnerFirstName}<br></span><br><span style="color: #000000;"><strong><span style="text-decoration: underline;">Reason for this alert:</span><br></strong></span><span style="color: #000000;">Prospective student has clicked on GCM brochure within email.&nbsp;<br><br><span style="text-decoration: underline;"><strong>Action required:</strong></span><br>Call to discuss their interest in the GCM.<br><br></span><strong><span style="color: #000000;">Please call&nbsp;</span><span style="color: #EE3D29;">{!Contact.Name}</span><span>&nbsp;</span><span style="color: #000000;">immediately.&nbsp;<br><br></span>
                                                                        </p>
                                                                        <p>
                                                                            <span style="color: #000000;">
                                                                                <strong><span style="color: #000000;">Alert Information</span></strong><br>
                                                                                <strong><span style="color: #000000;">Lead: <a href="{!Contact.Link}" target="_blank">{!Contact.Name} (SFDC Detail)</a></span></strong><br>
                                                                                <strong><span style="color: #000000;">Time: {!NOW()}</span></strong>
                                                                            </span>
                                                                        </p>
                                                                        <p style="color: #7b7b7b; font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial;text-decoration-color: initial;">
                                                                            <strong>
                                                                                <span style="color: #000000;"><br><span style="text-decoration: underline;">Important information:<br><br></span></span>
                                                                                <span style="color: #000000;">Lead score: </span>
                                                                            </strong>
                                                                            <span style="color: #000000;">{!Contact.LeadScore__c}</span><span style="color: #000000;"><br><span style="color: #000000;"><br><strong>Last Interesting Moment:&nbsp;</strong></span></span><span style="color: #000000;"><span style="color: #000000;">{{lead.Last Interesting Moment Desc:default=edit me}}</span></span>
                                                                            <span style="color: #000000;">
                                                                                <br>
                                                                                <span style="color: #000000;">
                                                                                    <br><strong>Last Interesting Moment Date:&nbsp;</strong>
                                                                                </span>
                                                                            </span>
                                                                            <span style="color: #000000;"><span style="color: #000000;">{{lead.Last Interesting Moment Date:default=edit me}}</span></span><br>
                                                                            <span style="color:#000000;">
                                                                                <span style="color: #000000;">
                                                                                    <br><strong>Country:</strong><span style="color: #000000; font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows:2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; float: none; display: inline !important;">&nbsp;{!Contact.MailingCountry}<br></span>
                                                                                    <br><strong>Mailing State:</strong><span style="color: #000000; font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing:normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; float: none; display: inline !important;"><strong>&nbsp;</strong>{!Contact.MailingState}</span>
                                                                                </span>
                                                                            </span>
                                                                        </p><br>
                                                                        <p>
                                                                            <br>
                                                                        </p>
                                                                    </div>
                                                                </td>
                                                                <td class="expander" style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; visibility: hidden; width: 0px; padding: 0;" align="left" valign="top"></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr style="vertical-align:top; text-align:left; padding:0; " align="left">
                                <td class="wrapper last" style="word-break:break-word; -webkit-hyphens:auto; -moz-hyphens:auto; hyphens:auto; border-collapse:collapse !important; vertical-align:top; text-align:left; position:relative; padding:10px 0px 0px; " align="left" valign="top">
                                    <table class="twelve columns" style="border-spacing:0; border-collapse:collapse; vertical-align:top; text-align:left; width:580px; margin:0 auto; padding:0; ">
                                        <tbody>
                                            <tr style="vertical-align:top; text-align:left; padding:0; " align="left">
                                                <td style="word-break:break-word; -webkit-hyphens:auto; -moz-hyphens:auto; hyphens:auto; border-collapse:collapse !important; vertical-align:top; text-align:left; padding:0px 30px 15px 30px; color:#7b7b7b; font-family:'Helvetica', 'Arial', sans-serif; font-weight:normal; line-height:19px; font-size:14px; margin:0; " align="left">
                                                    <div class="mktEditable" id="content5" style=""></div>
                                                </td>
                                                <td class="expander" style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; visibility: hidden; width: 0px; padding: 0;" align="left" valign="top"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr style="vertical-align:top; text-align:left; padding:0; " align="left">
                                <td style="word-break:break-word; -webkit-hyphens:auto; -moz-hyphens:auto; hyphens:auto; border-collapse:collapse !important; vertical-align:top; text-align:left; padding:0; " align="left" valign="top">
                                    <div class="mktEditable" id="footer2" style=""></div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</body>
</html>