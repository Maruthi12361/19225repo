<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <!-- utf-8 works for most cases -->
    <meta name="viewport" content="width=device-width">
    <!-- Use the latest (edge) version of IE rendering engine -->
    <meta name="x-apple-disable-message-reformatting">
    <!-- Disable auto-scale in iOS 10 Mail entirely -->
    <title>
    </title>
    <!-- The title tag shows in email notifications, like Android 4.4. -->

    <!-- CSS Reset -->
    <style>
        /* What it does: Remove spaces around the email design added by some email clients. */

        /* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */

        html,
        body
        {
            margin: 0 auto !important;
            padding: 0 !important;
            height: 100% !important;
            width: 100% !important;
        }

        /* What it does: Stops email clients resizing small text. */

        *
        {
            -ms-text-size-adjust: 100%;
        }

        /* What it does: Centers email on Android 4.4 */

        div[style*="margin: 16px 0"]
        {
            margin: 0 !important;
        }

        /* What it does: Stops Outlook from adding extra spacing to tables. */

        table,
        td
        {
            mso-table-lspace: 0pt !important;
            mso-table-rspace: 0pt !important;
        }

        /* What it does: Fixes webkit padding issue. Fix for Yahoo mail table alignment bug. Applies table-layout to the first 2 tables then removes for anything nested deeper. */

        table
        {
            border-spacing: 0 !important;
            border-collapse: collapse !important;
            table-layout: fixed !important;
            margin: 0 auto !important;
        }

        table table table
        {
            table-layout: auto;
        }

        /* What it does: Uses a better rendering method when resizing images in IE. */

        img
        {
            -ms-interpolation-mode: bicubic;
        }

        /* What it does: A work-around for iOS meddling in triggered links. */

        *[x-apple-data-detectors]
        {
            color: inherit !important;
            text-decoration: none !important;
        }

        /* What it does: A work-around for email clients meddling in triggered links. */

        *[x-apple-data-detectors],
        /* iOS */

        .x-gmail-data-detectors,
        /* Gmail */

        .x-gmail-data-detectors *,
        .aBn,
        .dontLink a
        {
            border-bottom: 0 !important;
            cursor: default !important;
            color: inherit !important;
            text-decoration: none !important;
            font-size: inherit !important;
            font-family: inherit !important;
            font-weight: inherit !important;
            line-height: inherit !important;
        }

        /* What it does: Prevents Gmail from displaying an download button on large, non-linked images. */

        .a6S
        {
            display: none !important;
            opacity: 0.01 !important;
        }

        /* If the above doesn't work, add a .g-img class to any image in question. */

        img.g-img+div
        {
            display: none !important;
        }

        /* What it does: Prevents underlining the button text in Windows 10 */

        .button-link
        {
            text-decoration: none !important;
        }

        .mobileOnly
        {
            max-height: 0px;
            min-height: 0px;
            /*Gmail seems to add min-height by itself*/
            font-size: 0;
            display: none;
            max-width: 0px;
            overflow: hidden;
        }

        /* What it does: Removes right gutter in Gmail iOS app: https://github.com/TedGoas/Cerberus/issues/89  */

        /* Create one of these media queries for each additional viewport size you'd like to fix */

        /* Thanks to Eric Lepetit @ericlepetitsf) for help troubleshooting */

        @media only screen and (min-device-width: 375px) and (max-device-width: 413px)
        {
            /* iPhone 6 and 6+ */
            .email-container
            {
                min-width: 375px !important;
            }
        }
    </style>
    <!-- Progressive Enhancements -->
    <style>
        /* Media Queries */

        @media screen and (max-width: 480px)
        {
            u+.body .gmail
            {
                display: none !important;
            }
            /* What it does: Forces elements to resize to the full width of their container. Useful for resizing images beyond their max-width. */
            .fluid,
            .fluid img
            {
                width: 100% !important;
                max-width: 100% !important;
                height: auto !important;
                margin-left: auto !important;
                margin-right: auto !important;
            }
            /* What it does: Forces table cells into full-width rows. */
            .stack-column,
            .stack-column-center
            {
                display: block !important;
                width: 100% !important;
                max-width: 100% !important;
                direction: ltr !important;
            }
            /* And center justify these ones. */
            .stack-column-center
            {
                text-align: center !important;
            }
            /* What it does: Generic utility class for centering. Useful for images, buttons, and nested tables. */
            .center-on-narrow
            {
                text-align: center !important;
                display: block !important;
                margin-left: auto !important;
                margin-right: auto !important;
                float: none !important;
            }
            /* What it does: Generic utility class for centering. Useful for images, buttons, and nested tables. */
            .center-on-narrow-2
            {
                text-align: center !important;
                margin-left: auto !important;
                margin-right: auto !important;
                float: none !important;
            }
            table.center-on-narrow
            {
                display: inline-block !important;
            }
            /* What it does: Hides content on mobile. */
            .mobile-hidden
            {
                display: none !important;
            }

            .mobileOnly
            {
                max-height: none !important;
                min-height: none !important;
                font-size: 15px !important;
                line-height: 20px !important;
                display: block !important;
                max-width: none !important;
                overflow: visible !important;
            }
            .m-pad
            {
                padding: 0 40px 0 0 !important;
            }
            .m-pad-2
            {
                padding: 20px 40px !important;
            }

            .h2-text
            {
                font-size: 18px !important;
                line-height: 24px !important;
            }
            .m-text
            {
                font-size: 12px !important;
                line-height: 15px !important;
            }
        }
    </style>
    <!-- Progressive Enhancements : END -->
    <!--[if mso]> <style type="text/css"> ul, ol {margin-left: 30px !important;} .contact span {mso-text-raise: 12px !important;} </style> <![endif]-->
    <!--[if (mso)|(mso 16)]> <style type="text/css"> a {text-decoration: none;} </style> <![endif]-->

    <!--[if gt mso 15]>      <style type="text/css" media="all">      /* Outlook 2016 Height Fix */      table, tr, td {border-collapse: collapse;}      tr { font-size:0px; line-height:0px; border-collapse: collapse; }      </style>      <![endif]-->

    <!-- What it does: Makes background images in 72ppi Outlook render at correct size. -->
    <!--[if gte mso 9]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml> <![endif]-->
</head>

<body width="100%" bgcolor="#ffffff" style="margin: 0; mso-line-height-rule: exactly;">
    <center style="width: 100%; background: #ffffff; text-align: left;">
        <div style="max-width: 640px; margin: 0 auto;" class="email-container">
            <!--[if mso | IE]> <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%" align="center" style="width:100%;" bgcolor="#ffffff"> <tr> <td> <table bgcolor="#ffffff" role="presentation" border="0" cellpadding="0" cellspacing="0" width="640" align="center" style="width:640px;"> <tr> <td> <![endif]-->
            <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="max-width: 640px;">
                <tr>
                    <td>
                        <!-- Email Content : BEGIN -->
                        <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tr>
                                <td align="center" style="font-size: 0px; line-height: 0;">
                                    <table cellpadding="0" cellspacing="0" width="100%" style="background-color: #EE3D29; min-width: 100%; " class="stylingblock-content-wrapper">
                                        <tr>
                                            <td style="padding: 0px; " class="stylingblock-content-wrapper camarker-inner">
                                                <!-- Header : BEGIN -->
                                                <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
                                                    <tr>
                                                        <td style="padding: 0px 35px;max-width: 100%;">
                                                            <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
                                                                <tr>
                                                                    <td align="left" style="padding: 0px;max-width: 100%;">
                                                                        <a href="https://www.aib.edu.au" title=""><img alt="AIB" src="https://s3-ap-southeast-2.amazonaws.com/cdn.aib.edu.au/email/2020/aib-logo-chevron-reversed.png" style="width: 100%; min-width: 80px; max-width: 100px; height: auto; padding: 0px; text-align: center;" width="84"></a></td>
                                                                    <td align="right">
                                                                        <table align="right" border="0" cellpadding="0" cellspacing="0" role="presentation">
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <!-- Header : END -->
                                            </td>
                                        </tr>
                                    </table>
                                    <table cellpadding="0" cellspacing="0" width="100%" style="background-color: transparent; min-width: 100%; " class="stylingblock-content-wrapper">
                                        <tr>
                                            <td style="padding: 40px 40px 35px;" class="stylingblock-content-wrapper camarker-inner">
                                                <!-- Content : BEGIN -->
                                                <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
                                                    <tr>
                                                        <td style="font-family: 'DIN Next W01 Regular', Arial , sans-serif; font-size: 14px; line-height: 20px; color: #3e3834; text-align: left;">
                                                            <p style="margin: 0;">
                                                                <div style="word-wrap: break-word;">Dear {{{Recipient.FirstName}}},<br />
															<br />
															Thank you for your email. I hope you are well.<br />
															<br />
															Please know that as per your request we have withdrawn you from <span style="background-color: yellow;">INSERT SUBJECT</span> and the following will apply.</div>

															<ul>
																<li style="overflow-wrap: break-word;">We have removed you from this subject after the census date date <span style="background-color: yellow;">16/01/2018 INSERT DATE</span></li>
																<li style="overflow-wrap: break-word;"><span style="background-color: yellow;">Your Fee-Help Debt will be incurred / subject fee will be</span>&nbsp;incurred and will be again upon re-enrolment</li>
																<li style="overflow-wrap: break-word;">A Withdraw Fail grade will display on the academic transcript and will carry academic penalties as this is equivalent to a fail grade</li>
																<li style="overflow-wrap: break-word;">Re-Enrolment at a later date will be required and must be completed 5 weeks prior to the relevant start date</li>
																<li style="overflow-wrap: break-word;">Please note subject fees are subject to change and re-enrolments will be charged accordingly at the time of enrolment</li>
															</ul>

															<div style="word-wrap: break-word;"><br />
															Please note whilst we have withdrawn you from this subject, the remainder of the timetable will continue on as scheduled with your next subject <span style="background-color: yellow;">INSERT SUBJECT</span> commencing the <span style="background-color: yellow;">INSERT DATE</span>. <span style="background-color: aqua;">PLEASE DELETE IF THERE ARE NO FUTURE ENROLMENTS</span><br />
															<br />
															Have a lovely afternoon ahead and we look forward to you returning to your studies with AIB very soon.
                                                                <br>
                                                                <br>
                                                                Kind regards,
                                                                <br>
                                                                {{{Sender.FirstName}}} {{{Sender.LastName}}}
                                                                <br>
                                                                {{{Sender.Title}}}
                                                                <br>
                                                                <br>
                                                                <a href="mailto:studentcentral@aib.edu.au" style="color:#EE3D29;text-decoration:none;">studentcentral@aib.edu.au</a>
                                                                <br>
                                                                1300 304 820
                                                            </p>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <!-- Content : END -->
                                            </td>
                                        </tr>
                                    </table>
                                    <table cellpadding="0" cellspacing="0" width="100%" class="stylingblock-content-wrapper" style="min-width: 100%; ">
                                        <tr>
                                            <td class="stylingblock-content-margin-cell" style="padding: 10px 0px 0px; ">
                                                <table cellpadding="0" cellspacing="0" width="100%" style="background-color: #EE3D29; border: 0px; min-width: 100%; " class="stylingblock-content-wrapper">
                                                    <tr>
                                                        <td style="padding: 0px; " class="stylingblock-content-wrapper camarker-inner">
                                                            <!-- Footer — Signature : BEGIN -->
                                                            <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
                                                                <!--[if gt mso 15]>      <style type="text/css" media="all">      /* Outlook 2016 Height Fix */      table, tr, td {border-collapse: collapse;}      tr { font-size:0px; line-height:0px; border-collapse: collapse; }      </style>      <![endif]-->
                                                                <tr>
                                                                    <td align="center" dir="rtl">
                                                                        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="padding: 0px 30px 0px 0px;" width="100%">
                                                                            <tr>
                                                                                <td align="center" style="text-align:center; font-size:0;">
                                                                                    <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
                                                                                        <tr>
                                                                                            <td dir="ltr">
                                                                                                <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation">
                                                                                                    <tr>
                                                                                                        <td style="text-align: center;" width="50%">
                                                                                                            &nbsp;<a href="https://www.aib.edu.au" style="color: #EE3D29; text-decoration: none;" target="_blank" title=""><img alt="AIB" height="178" src="https://s3-ap-southeast-2.amazonaws.com/cdn.aib.edu.au/email/2020/aib-logo-reversed.png" style="width: 178px; height: 178px; padding: 0px; text-align: center; border: 0px;" width="178"></a>&nbsp;&nbsp;</td>
                                                                                                        <td align="right">
                                                                                                            <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
                                                                                                                <tr>
                                                                                                                    <td class="m-text" style="padding: 20px 20px 20px 0px; font-family: 'DIN Next W01 Regular', Arial , sans-serif; font-size: 12px; line-height: 16px; color: #ffffff; text-align: left;">
                                                                                                                        <b>Australian Institute of Business
                                                                                                                            <br>
                                                                                                                            <br>
                                                                                                                            Tel: 1300 304 820
                                                                                                                            <br>
                                                                                                                            <a href="https://www.aib.edu.au" style="color:#FFFFFF;text-decoration:none;" title="www.aib.edu.au">www.aib.edu.au</a>
                                                                                                                        </b>
                                                                                                                        <br>
                                                                                                                        <br>
                                                                                                                        <span class="x-gmail-data-detectors" style="font-size:10px;">27 Currie Street, Adelaide, South Australia 5000</span>
                                                                                                                        <br>
                                                                                                                        <br>
                                                                                                                        <a href="https://www.aib.edu.au/privacy-legals/" style="color: #ffffff; text-decoration:underline;" target="_blank">Privacy Policy</a>
                                                                                                                        <br>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        <!-- Footer — Signature : END -->
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <table cellpadding="0" cellspacing="0" width="100%" class="stylingblock-content-wrapper" style="min-width: 100%; ">
                                        <tr>
                                            <td class="stylingblock-content-margin-cell" style="padding: 10px 0px 0px; ">
                                                <table cellpadding="0" cellspacing="0" width="100%" style="background-color: #EE3D29; min-width: 100%; " class="stylingblock-content-wrapper">
                                                    <tr>
                                                        <td style="padding: 5px 0px; " class="stylingblock-content-wrapper camarker-inner">
                                                            <!-- Footer — Social Media : BEGIN -->
                                                            <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
                                                                <tr>
                                                                    <td align="center" style="padding: 0px 60px 0px 50px;max-width: 100%">
                                                                        <!--[endif]---->
                                                                        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="max-width: 540px;" width="100%">
                                                                            <tr>
                                                                                <td align="center" style="text-align:center; font-size:0;">
                                                                                    <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
                                                                                        <tr>
                                                                                            <td style="padding: 10px;">
                                                                                                <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation">
                                                                                                    <tr>
                                                                                                        <td align="center" style="padding: 8px;">
                                                                                                            <a href="https://www.linkedin.com/school/australian-institute-of-business/"><img alt="L" border="0" class="g-img" src="https://s3-ap-southeast-2.amazonaws.com/cdn.aib.edu.au/email/2020/icon-linkedin-reversed.png" style="width: 100%; max-width: 35px; height: auto; font-family: 'DIN Next W01 Light', Arial , sans-serif; color: #ffffff; display: block;" width="35"></a></td>
                                                                                                        <td align="center" style="padding: 0 10px;">
                                                                                                            <a href="https://www.facebook.com/AIBeducation/"><img alt="F" border="0" class="g-img" src="https://s3-ap-southeast-2.amazonaws.com/cdn.aib.edu.au/email/2020/icon-facebook-reversed.png" style="width: 100%; max-width: 30px; height: auto; font-family: 'DIN Next W01 Light', Arial , sans-serif; color: #ffffff; display: block;" width="30"></a></td>
                                                                                                        <td align="center" style="padding: 0 10px;">
                                                                                                            <a href="https://twitter.com/aibeducation"><img alt="T" border="0" class="g-img" src="https://s3-ap-southeast-2.amazonaws.com/cdn.aib.edu.au/email/2020/icon-twitter-reversed.png" style="width: 100%; max-width: 37px; height: auto; font-family: 'DIN Next W01 Light', Arial , sans-serif; color: #ffffff; display: block;" width="37"></a></td>
                                                                                                        <td align="center" style="padding: 0 10px;">
                                                                                                            <a href="https://www.instagram.com/australianinstituteofbusiness/"><img alt="I" border="0" class="g-img" src="https://s3-ap-southeast-2.amazonaws.com/cdn.aib.edu.au/email/2020/icon-instagram-reversed.png" style="width: 100%; max-width: 30px; height: auto; font-family: 'DIN Next W01 Light', Arial , sans-serif; color: #ffffff; display: block;" width="30"></a></td>
                                                                                                        <td align="center" style="padding: 0 0 0 10px;">
                                                                                                            <a href="https://www.youtube.com/channel/UCCEryWxGwVVm9Qmu9ptYCmQ"><img alt="Y" border="0" class="g-img" src="https://s3-ap-southeast-2.amazonaws.com/cdn.aib.edu.au/email/2020/icon-youtube-reversed.png" style="width: 100%; max-width: 43px; height: auto; font-family: 'DIN Next W01 Light', Arial , sans-serif; color: #ffffff; display: block;" width="43"></a></td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                    <!---</td-->
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        <!-- Footer — Social Media : END -->
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <!-- Email Content : END -->
                    </td>
                </tr>
            </table>
            <!-- Email Body : END -->
            <!--[if mso | IE]> </td></tr></table> </td></tr></table> <![endif]-->
        </div>
    </center>
    <!--[if !mso]><!-- -->
    <!--<![endif]-->
</body>

</html>