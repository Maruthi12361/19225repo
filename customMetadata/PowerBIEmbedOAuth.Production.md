<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Production</label>
    <protected>false</protected>
    <values>
        <field>ClientID__c</field>
        <value xsi:type="xsd:string">a14f5dae-4b66-46ea-b774-0dde3024fd8e</value>
    </values>
    <values>
        <field>ClientSecret__c</field>
        <value xsi:type="xsd:string">EAaOUdPkf7irIhwwIpElmwRDd67g6se0Os5yKGcEudg=</value>
    </values>
    <values>
        <field>ResourceURI__c</field>
        <value xsi:type="xsd:string">https://analysis.windows.net/powerbi/api</value>
    </values>
</CustomMetadata>
