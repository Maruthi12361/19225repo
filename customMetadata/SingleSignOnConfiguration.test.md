<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Test</label>
    <protected>false</protected>
    <values>
        <field>AzureAPIEndpoint__c</field>
        <value xsi:type="xsd:string">https://graph.windows.net/aibtest.com.au</value>
    </values>
    <values>
        <field>AzureAPIID__c</field>
        <value xsi:type="xsd:string">cb82e551-f0fd-49ef-89c3-e671f58475e1</value>
    </values>
    <values>
        <field>AzureAPISecret__c</field>
        <value xsi:type="xsd:string">5pDJPDBTj3VZjaq64hLqU4M/t+TfCojgTQX01GxLZB8=</value>
    </values>
    <values>
        <field>AzurePassword__c</field>
        <value xsi:type="xsd:string">by4YcD~3!=&gt;3EZK&lt;</value>
    </values>
    <values>
        <field>AzureUsername__c</field>
        <value xsi:type="xsd:string">svc-salesforce-azure-sync@aibtest.edu.au</value>
    </values>
    <values>
        <field>CerebroAPIEndpoint__c</field>
        <value xsi:type="xsd:string">https://api.aibtest.edu.au</value>
    </values>
    <values>
        <field>CerebroResource__c</field>
        <value xsi:type="xsd:string">https://testaibcorpoutlook.onmicrosoft.com/74880207-2d83-4ed4-9a1e-3ef1af68d124</value>
    </values>
    <values>
        <field>StudentDomain__c</field>
        <value xsi:type="xsd:string">@study.aibtest.edu.au</value>
    </values>
</CustomMetadata>
