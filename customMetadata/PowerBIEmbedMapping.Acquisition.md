<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Acquisition</label>
    <protected>false</protected>
    <values>
        <field>Default_Page__c</field>
        <value xsi:type="xsd:string">ReportSection8b2547b0d3ffecf7ecc6</value>
    </values>
    <values>
        <field>EmbedURLFormat__c</field>
        <value xsi:type="xsd:string">https://app.powerbi.com/reportEmbed?groupId={0}&amp;reportId={1}</value>
    </values>
    <values>
        <field>Filter_Column__c</field>
        <value xsi:type="xsd:string">Contact ID</value>
    </values>
    <values>
        <field>Filter_Table__c</field>
        <value xsi:type="xsd:string">Contacts</value>
    </values>
    <values>
        <field>GroupID__c</field>
        <value xsi:type="xsd:string">e675a7b1-7897-44c7-8de6-86800fb38a79</value>
    </values>
    <values>
        <field>ObjectAPI__c</field>
        <value xsi:type="xsd:string">Contact</value>
    </values>
    <values>
        <field>ReportID__c</field>
        <value xsi:type="xsd:string">b1c7be99-c06b-4f21-88c8-79d96567af2f</value>
    </values>
    <values>
        <field>VisualforcePageName__c</field>
        <value xsi:type="xsd:string">PowerBIEmbedContactAcquisition</value>
    </values>
</CustomMetadata>
