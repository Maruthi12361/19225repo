<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Intervention</label>
    <protected>false</protected>
    <values>
        <field>Default_Page__c</field>
        <value xsi:type="xsd:string">ReportSection8af58c8b0e8ef206abe5</value>
    </values>
    <values>
        <field>EmbedURLFormat__c</field>
        <value xsi:type="xsd:string">https://app.powerbi.com/reportEmbed?groupId={0}&amp;reportId={1}</value>
    </values>
    <values>
        <field>Filter_Column__c</field>
        <value xsi:type="xsd:string">Contact ID</value>
    </values>
    <values>
        <field>Filter_Table__c</field>
        <value xsi:type="xsd:string">Students</value>
    </values>
    <values>
        <field>GroupID__c</field>
        <value xsi:type="xsd:string">d51253d4-d754-4bd6-85ee-ea636acde166</value>
    </values>
    <values>
        <field>ObjectAPI__c</field>
        <value xsi:type="xsd:string">Contact</value>
    </values>
    <values>
        <field>ReportID__c</field>
        <value xsi:type="xsd:string">7da489b4-7b6b-4e8b-8fa3-962c23bc4ff4</value>
    </values>
    <values>
        <field>VisualforcePageName__c</field>
        <value xsi:type="xsd:string">PowerBIEmbedContactProgression</value>
    </values>
</CustomMetadata>
