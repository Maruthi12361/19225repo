<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Production</label>
    <protected>false</protected>
    <values>
        <field>AzureAPIEndpoint__c</field>
        <value xsi:type="xsd:string">https://graph.windows.net/aib.edu.au</value>
    </values>
    <values>
        <field>AzureAPIID__c</field>
        <value xsi:type="xsd:string">23e2ad7d-66ce-4ed7-947a-eccd5e1d75c2</value>
    </values>
    <values>
        <field>AzureAPISecret__c</field>
        <value xsi:type="xsd:string">L4AQjFy/8yx0YZMJX13EZwRXH1tVMogBOINLKP8MG/c=</value>
    </values>
    <values>
        <field>AzurePassword__c</field>
        <value xsi:type="xsd:string">7&amp;Ge6?k4@Xa3%#Ql</value>
    </values>
    <values>
        <field>AzureUsername__c</field>
        <value xsi:type="xsd:string">svc-salesforce-azure-sync@aib.edu.au</value>
    </values>
    <values>
        <field>CerebroAPIEndpoint__c</field>
        <value xsi:type="xsd:string">https://api.aib.edu.au</value>
    </values>
    <values>
        <field>CerebroResource__c</field>
        <value xsi:type="xsd:string">https://aibeduau.onmicrosoft.com/7896adfd-b5ab-478e-b573-9a73c6393fd2</value>
    </values>
    <values>
        <field>StudentDomain__c</field>
        <value xsi:type="xsd:string">@study.aib.edu.au</value>
    </values>
</CustomMetadata>
