<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Staging</label>
    <protected>false</protected>
    <values>
        <field>AzureAPIEndpoint__c</field>
        <value xsi:type="xsd:string">https://graph.windows.net/aibstaging.edu.au</value>
    </values>
    <values>
        <field>AzureAPIID__c</field>
        <value xsi:type="xsd:string">acb891d4-4b13-4ee4-bcb1-121fc5c469af</value>
    </values>
    <values>
        <field>AzureAPISecret__c</field>
        <value xsi:type="xsd:string">6RxvtMimDbrBKb+5ZTAGQUP6EraZD7bT6NjPY1InpCM=</value>
    </values>
    <values>
        <field>AzurePassword__c</field>
        <value xsi:type="xsd:string">by4YcD~3!=&gt;3EZK&lt;</value>
    </values>
    <values>
        <field>AzureUsername__c</field>
        <value xsi:type="xsd:string">svc-salesforce-azure-sync@aibstaging.edu.au</value>
    </values>
    <values>
        <field>CerebroAPIEndpoint__c</field>
        <value xsi:type="xsd:string">https://api.aibstaging.com.au</value>
    </values>
    <values>
        <field>CerebroResource__c</field>
        <value xsi:type="xsd:string">https://api.aibstaging.com.au</value>
    </values>
    <values>
        <field>StudentDomain__c</field>
        <value xsi:type="xsd:string">@study.aibstaging.edu.au</value>
    </values>
</CustomMetadata>
