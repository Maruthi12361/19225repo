<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <brand>
        <headerColor>#0070D2</headerColor>
        <shouldOverrideOrgTheme>false</shouldOverrideOrgTheme>
    </brand>
    <description>Used to manage the support, operations and administration of students governed by AIB. Via Service Console; Student Contacts, Cases and Case Enrolments. ie: Student Management</description>
    <formFactors>Large</formFactors>
    <isNavAutoTempTabsDisabled>false</isNavAutoTempTabsDisabled>
    <isNavPersonalizationDisabled>false</isNavPersonalizationDisabled>
    <label>AIB Service Console</label>
    <navType>Console</navType>
    <setupExperience>service</setupExperience>
    <tabs>standard-Case</tabs>
    <tabs>standard-Contact</tabs>
    <tabs>standard-Account</tabs>
    <tabs>standard-report</tabs>
    <tabs>standard-Dashboard</tabs>
    <tabs>standard-home</tabs>
    <tabs>Knowledge__kav</tabs>
    <uiType>Lightning</uiType>
    <utilityBar>LightningService_UtilityBar</utilityBar>
    <workspaceConfig>
        <mappings>
            <tab>Knowledge__kav</tab>
        </mappings>
        <mappings>
            <tab>standard-Account</tab>
        </mappings>
        <mappings>
            <fieldName>ContactId</fieldName>
            <tab>standard-Case</tab>
        </mappings>
        <mappings>
            <tab>standard-Contact</tab>
        </mappings>
        <mappings>
            <tab>standard-Dashboard</tab>
        </mappings>
        <mappings>
            <tab>standard-home</tab>
        </mappings>
        <mappings>
            <tab>standard-report</tab>
        </mappings>
    </workspaceConfig>
</CustomApplication>
