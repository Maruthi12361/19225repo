import { LightningElement, track, api, wire } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { getRecord } from 'lightning/uiRecordApi';
import { loadStyle } from 'lightning/platformResourceLoader';

import calcResource from '@salesforce/resourceUrl/CalculatorStyles';
import baseUrl from '@salesforce/label/c.OAFBaseURL';

const FIELDS =
[
    'Contact.CalculatorAgeRange__c',
    'Contact.CalculatorSalaryPerAnnum__c',
    'Contact.CalculatorRemainingWorkYears__c'
];

export default class RoiCalculator extends LightningElement 
{
    @track loaded = false;
    @track noValues = null;
    @track iframeUrl;
    @api recordId;
    @track contact;

    @wire(getRecord, { recordId: '$recordId', fields: FIELDS })
    wiredRecord({ error, data })
    {
        if (error)
        {
            let message = 'Unknown error';

            if (Array.isArray(error.body)) 
                message = error.body.map(e => e.message).join(', ');
            else if (typeof error.body.message === 'string')
                message = error.body.message;

            this.dispatchEvent(
                new ShowToastEvent(
                {
                    title: 'Error loading contact',
                    message,
                    variant: 'error',
                }),
            );
        }
        else if (data)
        {
            this.contact = data;

            if (this.contact.fields.CalculatorSalaryPerAnnum__c.value != null && this.contact.fields.CalculatorAgeRange__c.value != null
                    && this.contact.fields.CalculatorRemainingWorkYears__c.value != null)
            {
                this.noValues = false;
                this.iframeUrl = baseUrl +  "roi-calculator/embed.html?salary=" + this.contact.fields.CalculatorSalaryPerAnnum__c.value + "&age=" + 
                    this.contact.fields.CalculatorAgeRange__c.value +"&workyear=" + this.contact.fields.CalculatorRemainingWorkYears__c.value;
            }
            else
                this.noValues = true;
        }

        this.loaded = true;
    }

    renderedCallback()
    {
        if (this.initialised)
            return;

        Promise.all(
        [
            loadStyle(this, calcResource)
        ])
        .then(() =>
        {
            this.initialised = true;
        })
        .catch(error =>
        {
            this.dispatchEvent(
                new ShowToastEvent(
                {
                    title: 'Error loading css',
                    message: JSON.stringify(error),
                    variant: 'error',
                }),
            );
        });
    }
}