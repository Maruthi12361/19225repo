import { LightningElement, track, api, wire } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { getRecord } from 'lightning/uiRecordApi';

export default class TermsStaffPlan extends LightningElement 
{
    @api recordId;
    @track showDetails = false;

    @wire(getRecord, { recordId: '$recordId', fields: ['hed__Term__c.PlanningStatus__c'] })
    wiredRecord({ error, data })
    {
        if (error)
        {
            let message = 'Unknown error';

            if (Array.isArray(error.body)) 
                message = error.body.map(e => e.message).join(', ');
            else if (typeof error.body.message === 'string')
                message = error.body.message;

            this.dispatchEvent(new ShowToastEvent({ title: 'Error loading contact', message: message, variant: 'error' }));
        }
        else if (data)
        {
            if (data.fields.PlanningStatus__c.value === 'In Planning' || data.fields.PlanningStatus__c.value === 'Planned')
                this.showDetails = true;
        }
    }

    kickoffSuccessHandler()
    {
        this.showDetails = true;
    }
}