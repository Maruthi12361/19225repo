import { LightningElement, track, api, wire } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

import getList from '@salesforce/apex/SubjectOfferingController.getList';
import kickOff from '@salesforce/apex/TermController.kickOff';

const columns = 
[
    { label: 'Subject code', fieldName: 'RecordUrl', type: 'url', typeAttributes: { label: { fieldName: 'Code' }, target: '_blank' }},
    { label: 'Subject Name', fieldName: 'Name'},
    { label: 'AQF Level', fieldName: 'AQFLevel' },
    { label: 'Head Of Discipline', fieldName: 'HeadOfDiscipline' },
    { label: 'Students Per OLF', fieldName: 'StudentsTaughtPerOLF' },
    { label: 'Students Per Subject Coordinator', fieldName: 'StudentsTaughtPerSC' },
    { label: 'Students Coordinated per Subject Coordinator', fieldName: 'StudentsCoordinatedPerSC' },
    { label: 'Total contacts with skill', fieldName: 'TotalSkilledContacts' },
    { label: 'Actual Student Numbers', fieldName: 'ActualStudents' }
];

export default class TermsStaffPlanKickOff extends LightningElement
{
    @api recordId;

    @track columns = columns;
    @track subjects;
    @track loading = true;
    @track toDisable = false;

    @wire(getList, { termId: '$recordId' }) getSubjectList({ error, data })
    {
        if (error)
        {
            let message = 'Unknown error';

            if (Array.isArray(error.body)) 
                message = error.body.map(e => e.message).join(', ');
            else if (typeof error.body.message === 'string')
                message = error.body.message;

            this.dispatchEvent(new ShowToastEvent({ title: 'Error loading subject list', message, variant: 'error' }));
            this.loading = false;
        }
        else if (data)
        {
            this.subjects = JSON.parse(data);
            this.setValidFlag();
            this.loading = false;
        }
    }

    handleButtonClick()
    {
        this.loading = true;

        kickOff({ termId: this.recordId })
            .then(result => 
            {
                this.loading = false;
                this.dispatchEvent(new CustomEvent('kickoffsuccess'));
            })
            .catch(error =>
            {
                let message;
                
                if (Array.isArray(error.body))
                    message = error.body.map(e => e.message).join(', ');
                else if (typeof error.body.message === 'string')
                    message = error.body.message;

                this.dispatchEvent(new ShowToastEvent({ title: 'Error', message: message, variant: 'error' }));
                this.loading = false;
            });
    }

    setValidFlag()
    {
        if (this.subjects)
            this.toDisable = this.subjects.every(subject => subject.IsValid) ? false : true;
    }
}