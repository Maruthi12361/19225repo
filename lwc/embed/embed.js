import { LightningElement, track, api, wire } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { getRecord } from 'lightning/uiRecordApi';

export default class Embed extends LightningElement
{
    @api recordId;
    @api frameId;
    @api sourceFieldName;
    @api sourceFieldObject;
    @api width;
    @api height;

    @track urlExists;
    @track sourceUrl;
    @track frameHeight;
    @track fieldNames;

    connectedCallback()
    {
        let self = this;

        this.urlExists = false;
        this.frameHeight = this.height;
        this.fieldNames = [this.sourceFieldObject + '.'+ this.sourceFieldName];

        window.addEventListener("message", function(event)
        {
            self.handleMessage(event);
        });
    }

    @wire(getRecord, { recordId: '$recordId', fields: '$fieldNames' })
    getData({ error, data })
    {
        if (error)
        {
            let message = 'Unknown error';

            if (Array.isArray(error.body)) 
                message = error.body.map(e => e.message).join(', ');
            else if (typeof error.body.message === 'string')
                message = error.body.message;

            this.dispatchEvent(new ShowToastEvent({ title: 'Error loading data', message: message, variant: 'error' }));
        }
        else if (data)
        {
            if (this.sourceFieldName.includes('.'))
            {
                let relationObjectIndex = this.sourceFieldName.indexOf('.');
                let relationObject = this.sourceFieldName.substring(0, relationObjectIndex);
                let relationField = this.sourceFieldName.substring(relationObjectIndex + 1);

                if (data.fields[relationObject].value && data.fields[relationObject].value.fields[relationField]
                        && data.fields[relationObject].value.fields[relationField].value)
                {
                    this.sourceUrl = data.fields[relationObject].value.fields[relationField].value;
                    this.urlExists = true;
                }
            }
            else if (data.fields[this.sourceFieldName] && data.fields[this.sourceFieldName].value)
            {
                this.sourceUrl = data.fields[this.sourceFieldName].value;
                this.urlExists = true;
            }
        }
    }

    handleMessage(event)
    {
        if (typeof event.data !== 'string')
            return;

        let data = null;

        try
        {
            data = JSON.parse(event.data);
        }
        catch(e)
        {
        }

        if (data == null || !('AIBWidget' in data))
            return;

        if ('id' in data.AIBWidget && 'frameId' in data.AIBWidget && 'primaryContentHeight' in data.AIBWidget)
        {
            let iframe = this.template.querySelector('[data-id="' + data.AIBWidget.id + '"]');

            if (iframe)
            {
                let height = iframe.scrollHeight - (iframe.scrollHeight - data.AIBWidget.primaryContentHeight);
                this.frameHeight = height;
            }
        }
    }
}