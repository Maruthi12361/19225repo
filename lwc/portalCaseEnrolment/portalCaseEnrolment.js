import { LightningElement, api, track } from 'lwc';
import getSubjectsByPortalUser from '@salesforce/apex/CaseEnrolmentController.getSubjectsByPortalUser';
import createCaseEnrolments from '@salesforce/apex/CaseEnrolmentController.createCaseEnrolments';

export default class PortalCaseEnrolment extends LightningElement
{
    @track data = [];
    @track selectingRows = [];
    @track showData = false;
    @track showSpinner = false;
    @api showSpinnerByParent;
    selectedRows = null;
    @track columns;
    windowWidth = document.documentElement.clientWidth;

    get displaySpinner()
    {
        return this.showSpinner || this.showSpinnerByParent;
    }

    // Display the row number on lightning data table only if the window is wide enough, e.g. greater than 1000px
    get showRowNumber()
    {
        return this.windowWidth >= 1000;
    }

    connectedCallback()
    {
        window.addEventListener('resize', this.handleWindwowResize.bind(this));
    }

    disconnectedCallback()
    {
        window.removeEventListener('resize', this.handleWindwowResize.bind(this));
    }

    // Set columns of data table in terms of the window width
    setColumns(width)
    {
        if (!width)
        {
            width = document.documentElement.clientWidth;
        }

        this.columns =
            [
                { label: 'Subject Name', fieldName: 'Name', type: 'text' }
            ];

        if (width >= 330)
        {
            this.columns.push({ label: 'Term', fieldName: 'Term', type: 'text' });
        }

        if (width >= 450)
        {
            this.columns.push({ label: 'Admin Date', fieldName: 'AdminDate', type: 'date' });
        }

        if (width >= 550)
        {
            this.columns.push({ label: 'Start Date', fieldName: 'StartDate', type: 'date' });
        }

        if (width >= 650)
        {
            this.columns.push({ label: 'Status', fieldName: 'Status', type: 'text' });
        }

        if (width >= 700)
        {
            this.columns[0].initialWidth = 200;
        }

        if (width >= 950)
        {
            this.columns[0].initialWidth = 300;
        }
    }

    handleWindwowResize(event)
    {
        var dimensions =
        {
            height: (event.srcElement || event.currentTarget).innerHeight,
            width: (event.srcElement || event.currentTarget).innerWidth
        };

        if (this.windowWidth != dimensions.width)
        {
            this.windowWidth = dimensions.width;
            this.setColumns(dimensions.width);
        }
    }

    handleShowData()
    {
        this.showData = true;
        this.handleLoadData();
    }

    handleLoadData()
    {
        if (this.selectedRows && this.selectedRows.length > 0)
        {
            this.selectedRows = null;
            this.selectingRows = [];
        }

        this.setColumns(this.windowWidth);
        this.showSpinner = true;
        getSubjectsByPortalUser()
            .then(result => {
                this.data = result;
            })
            .catch(error => {
                this.showNotification('Failed To retrieve Subjects', error, 'error');
            })
            .then(() => {
                this.showSpinner = false;
            });
    }

    handleSelectRows(event)
    {
        this.selectedRows = event.detail.selectedRows;
    }

    handleCancel()
    {
        this.closeUI();
    }

    @api handleCreate(caseId)
    {
        if (caseId && this.selectedRows && this.selectedRows.length > 0)
        {
            var param = [];
            this.selectedRows.forEach(function(item){
                param.push(item.Id);
            });

            this.showSpinner = true;
            createCaseEnrolments({ "caseId" : caseId, "param" : param })
            .then(result => {
                this.showNotification('Case Enrolments Saved', 'The selected subjects are linked with Case.', 'success');
            })
            .catch(error => {
                this.showNotification('Failed To Save Case Enrolments', error, 'error');
            })
            .then(() => {
                this.showSpinner = false;
            });
        }
    }

    showNotification(title, message, type)
    {
        if (type === 'success')
        {
            console.log(title + ' - ' + message);
        }
        else
        {
            console.error(title + ' - ' + message);
        }
    }

    closeUI()
    {
        this.showData = false;
        this.data = null;
        this.selectedRows = null;
        this.selectingRows = [];
    }
}