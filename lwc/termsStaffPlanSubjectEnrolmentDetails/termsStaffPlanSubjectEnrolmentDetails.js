import { LightningElement, track, api} from 'lwc';

export default class TermsStaffPlanSubjectEnrolmentDetails extends LightningElement
{
    @api subjectEnrolment;
    @api options;
    @api editMode;
    @api faculties;
    @api index;
    @api roleStudentCapacities;
    @api selectedFaculties;

    @track filteredFaculties;
    @track showRemoveButton;
    @track maxCapacity;
    @track showOverflowWarning;
    @track showPrerequisitesWarning;

    initValueSet;
    editableSubjectEnrolment;

    connectedCallback()
    {
        // initialize component
        this.initValueSet = true;
        this.filteredFaculties = { "Role": "", "Data": [] };
        
        if (this.faculties)
            this.bindComboboxData(this.faculties);

        if (this.index != null && this.index === 0)
            this.showRemoveButton = false;
        else
            this.showRemoveButton = true;

        if (this.subjectEnrolment)
        {
            this.filteredFaculties.Role = this.subjectEnrolment.RoleName;
            this.setMaxCapacity();
            this.setWarningIcons();
        }
    }

    renderedCallback() 
    {
        if (this.faculties)
        {
            if ((this.filteredFaculties.Data.length === 0 && this.initValueSet) || this.filteredFaculties.Role !== this.subjectEnrolment.RoleName)
            {
                this.bindComboboxData(this.faculties);
                this.initValueSet = false;
            }
        }

        this.setMaxCapacity();
        this.setWarningIcons();
    }

    @api
    resetStaffMemberComboValues(paramFaculties)
    {
        this.selectedFaculties = paramFaculties;
        this.bindComboboxData(this.faculties);
    }

    setMaxCapacity()
    {
        if (this.subjectEnrolment && this.maxCapacity !== this.subjectEnrolment.MaxCapacity)
            this.maxCapacity = this.subjectEnrolment.MaxCapacity;
    }

    setWarningIcons()
    {
        if (this.subjectEnrolment.AllocatedStudents)
        {
            let toShowOverflow = this.subjectEnrolment.AllocatedStudents > this.maxCapacity;

            if (this.showOverflowWarning !== toShowOverflow)
                this.showOverflowWarning = toShowOverflow;
        }

        if (!this.subjectEnrolment.StaffId && !this.showPrerequisitesWarning)
            this.showPrerequisitesWarning = false; // Staff not selected, no need to show warning
        else
        {
            let toShowPrequisites = this.subjectEnrolment.StaffPrerequisitesCompleted === true ? false : true;

            if (this.showPrerequisitesWarning !== toShowPrequisites)
                this.showPrerequisitesWarning = toShowPrequisites;
        }
    }

    handleRemoveButtonClick()
    {
        let result = {};
        result.Id = this.subjectEnrolment.SubjectEnrolmentId;
        result.IsDeleted = true;

        const dataChangedEvent = new CustomEvent('dataupdated', { detail:  JSON.stringify(result) });
        this.dispatchEvent(dataChangedEvent);
    }

    handleChange(event)
    {
        let result = {};
        let rebindData = false;
        let value = event.detail.value;
        result[event.target.name] = value;
        result.Id = this.subjectEnrolment.SubjectEnrolmentId;

        if (event.target.name === 'StaffId')
        {
            if (value && this.faculties)
            {
                let facultyMember = this.faculties.find(faculty => faculty.Id === value);

                if (facultyMember)
                {
                    this.showPrerequisitesWarning = facultyMember.PrerequisitesCompleted === true ? false : true;
                    result.StaffPrerequisitesCompleted = facultyMember.PrerequisitesCompleted;
                }
                else
                    this.showPrerequisitesWarning = true; // Show prerequisite warning as staff not found in a list
            }
        }

        if (event.target.name === 'RoleName')
        {
            rebindData = true;
            let roleStudentCapacity = this.roleStudentCapacities.find(roleCapacity => roleCapacity.RoleName === value);

            if (roleStudentCapacity)
                this.maxCapacity = roleStudentCapacity.Capacity; // Set max capacity value based on role

            result.StaffId = ''; // Reset staff member
            result.MaxCapacity = this.maxCapacity;

            this.showPrerequisitesWarning = false;
        }

        if (event.target.name === 'AllocatedStudents')
        {
            if (value && this.maxCapacity)
                this.showOverflowWarning = value > this.maxCapacity;
        }

        if (event.target.name === 'Status')
        {
            if (!this.subjectEnrolment.StaffId && value === 'Requested')
                result.Status = 'Draft'; // Requested status selected without staff member, set status to Draft
        }

        const dataChangedEvent = new CustomEvent('dataupdated', { detail:  JSON.stringify(result) });
        this.dispatchEvent(dataChangedEvent);

        if (rebindData)
            this.bindComboboxData(this.faculties, true);
    }

    bindComboboxData(data, roleChanged)
    {
        let self = this;
        self.filteredFaculties.Role = self.subjectEnrolment.RoleName;
        self.filteredFaculties.Data = [];

        data
            .filter(faculty => faculty.Role === self.subjectEnrolment.RoleName)
            .forEach(faculty =>
            {
                let isCurrentlySelected = faculty.Id === self.subjectEnrolment.StaffId;
                let selectedFaculty = self.selectedFaculties.find(facultySelected => (facultySelected.Role === faculty.Role && facultySelected.StaffId === faculty.Id));

                if (!selectedFaculty || isCurrentlySelected)
                {
                    // Faculty not selected in any other combobox or faculty is currenlty selected
                    let dataItem = { "label" : faculty.Name, "value" : faculty.Id };

                    if (!isCurrentlySelected)
                        self.filteredFaculties.Data.push(dataItem);
                    else
                        self.filteredFaculties.Data.unshift(dataItem);
                }
            });

        if (!roleChanged)
        {
            let savedFaculty = self.filteredFaculties.Data.find(faculty => faculty.value === self.subjectEnrolment.StaffId);

            if (!savedFaculty && self.subjectEnrolment.StaffName)
            {
                // Saved staff member not in a list, add it to the top of the list
                self.filteredFaculties.Data.unshift(
                {
                    "label" : self.subjectEnrolment.StaffName, "value" : self.subjectEnrolment.StaffId
                });
            }
        }
    }
}