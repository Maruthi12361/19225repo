import { LightningElement, track, api } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getSubjectOfferingFaculties from '@salesforce/apex/FacultyController.getSubjectOfferingFaculties';
import saveEnrolments from '@salesforce/apex/SubjectOfferingController.saveEnrolments';

export default class TermsStaffPlanSubjectDetails extends LightningElement
{
    @api subjectOffering;
    @api options;

    @track loading;
    @track editMode = false;
    @track subjectOfferingFaculties;
    @track filteredSubjectOfferingFaculties = [];
    @track subjectEnrolments;
    @track selectedFaculties;
    @track offering;

    connectedCallback()
    {
        // initialize component
        this.selectedFaculties = [];
        this.offering = Object.assign({}, this.subjectOffering); 
        this.subjectEnrolments = this.subjectOffering.SubjectEnrolments.map(enrolments => ({...enrolments})); // Imutable objects, create deep copy of enrolments
        
        this.subjectEnrolments.forEach(enrolment => 
        {
            if (enrolment.StaffId && enrolment.RoleName)
                this.selectedFaculties.push({"Role": enrolment.RoleName, "StaffId" : enrolment.StaffId });
        });
    }

    handleEditButtonClick()
    {
        let self = this;

        if (!self.subjectOfferingFaculties)
        {
            this.loading = true;

            getSubjectOfferingFaculties({ offeringId: this.subjectOffering.SubjectOfferingId})
            .then(result =>
            {
                self.subjectOfferingFaculties = JSON.parse(result);
                self.editMode = true;
                this.loading = false;
            })
            .catch(error =>
            {
                let message = 'Failed to retrieve faculty contacts.';

                if (!error)
                    message += JSON.stringify(error);

                this.dispatchEvent(new ShowToastEvent({ title: 'Error loading contact', message, variant: 'error' }));
                this.loading = false;
            });
        }
        else
        {
            self.editMode = true;
            this.updateSelectedFaculties();
            this.resetComboValues();
        }
    }

    handleSaveButtonClick()
    {
        this.loading = true;

        saveEnrolments({ offeringId: this.subjectOffering.SubjectOfferingId, enrolments: JSON.stringify(this.subjectEnrolments) })
            .then(result =>
            {
                let offering = JSON.parse(result);
                this.subjectOffering = Object.assign({}, offering); // Imutable object, make a object copy to reset values
                this.subjectEnrolments = offering.SubjectEnrolments.map(enrolments => ({...enrolments}));
                this.editMode = false;
                this.loading = false;
            })
            .catch(error =>
            {
                let message = 'Failed to save subject enrolments.';

                if (!error)
                    message += JSON.stringify(error);

                this.dispatchEvent(new ShowToastEvent({ title: 'Error saving subject enrolments', message, variant: 'error' }));
                this.loading = false;
            });
    }

    handleCancelButtonClick()
    {
        this.editMode = false;
        this.offering = Object.assign({}, this.subjectOffering);
        this.subjectEnrolments = this.subjectOffering.SubjectEnrolments.map(enrolments => ({...enrolments}));
    }

    handleAddButtonClick()
    {
        let roleStudentCapacity = this.subjectOffering.RoleStudentCapacities.find(roleCapacity => roleCapacity.RoleName === 'Online Facilitator');
        let maxCapacity = roleStudentCapacity != null ? roleStudentCapacity.Capacity : 50; // Default value to 50

        this.subjectEnrolments.push(
        {
            "SubjectEnrolmentId": "UUID" + Math.floor(Math.random() * Math.floor(1000000)), 
            "RoleName" : "Online Facilitator",
            "StaffId" : "",
            "StaffName": "",
            "AllocatedStudents" : 0,
            "MaxCapacity" : maxCapacity,
            "Status" : "Recommended",
            "IsDeleted" : false,
            "StaffPrerequisitesCompleted" : false
        });
    }

    handleDataUpdate(event)
    {
        let data = JSON.parse(event.detail);

        if (data.Id)
        {
            let enrolmentToUpdate = this.subjectEnrolments.find(enrolment => enrolment.SubjectEnrolmentId === data.Id);

            if (enrolmentToUpdate)
            {
                let staffUpdated = false;
                let studentStatusUpdated = false;

                Object.keys(data).forEach(key =>
                {
                    if (key !== 'Id')
                        enrolmentToUpdate[key] = data[key];

                    if (key === 'StaffId' || key === 'IsDeleted')
                        staffUpdated = true;

                    if (key === 'AllocatedStudents' || key === 'Status')
                        studentStatusUpdated = true;
                });

                if (staffUpdated)
                {
                    this.updateSelectedFaculties();
                    this.resetComboValues();
                }

                if (studentStatusUpdated)
                    this.updateCapacity();
            }
        }
    }

    updateSelectedFaculties()
    {
        this.selectedFaculties = [];
        this.subjectEnrolments.forEach((enrolment) =>
        {
            if (!enrolment.IsDeleted)
                this.selectedFaculties.push({"Role": enrolment.RoleName, "StaffId" : enrolment.StaffId });
        });
    }

    resetComboValues()
    {
        let enrolmentElements = this.template.querySelectorAll('c-terms-staff-plan-subject-enrolment-details');

        if (enrolmentElements)
            enrolmentElements.forEach(x => x.resetStaffMemberComboValues(this.selectedFaculties)); 
    }

    updateCapacity()
    {
        let capacity = 0;
        let projecteCapacity = 0;

        this.subjectEnrolments.forEach(enrolment => 
        {
            let allocatedStudents = isNaN(parseInt(enrolment.AllocatedStudents, 10)) ? 0 : parseInt(enrolment.AllocatedStudents, 10);

            if (enrolment.Status === 'Enrolled')
            {
                capacity += allocatedStudents;
                projecteCapacity += allocatedStudents;
            }
            else if (enrolment.Status === 'Requested')
                projecteCapacity += allocatedStudents;
        });

        this.offering.Capacity = capacity;
        this.offering.ProjectedCapacity = projecteCapacity;
    }
}