import { LightningElement, api, wire, track } from 'lwc';
import { getRecord } from 'lightning/uiRecordApi';
import resetPassword from '@salesforce/apex/ContactController.resetPassword';

export default class ContactResetPassword extends LightningElement
{
    @api recordId;
    @track contactName;
    @track resetSuccess = false;
    @track loading = true;
    @track resetEnabled = true;
    @track disabled = false;
    @track disableResetButton = false;
    @track errorMessage;

    @wire(getRecord, { recordId: '$recordId', fields: ['Contact.Name', 'Contact.Cerebro_Student_ID__c', 'Contact.SingleSignOnStatus__c'] })
    wiredRecord({ error, data })
    {
        if (error)
        {
            let message = 'Unable to reset contact password. ';

            if (Array.isArray(error.body)) 
                message = message += error.body.map(e => e.message).join(', ');
            else if (typeof error.body.message === 'string')
                message = message += error.body.message;

            this.loading = false;
            this.resetEnabled = false;
            this.disabled = false;
            this.disableResetButton = true;

            this.errorMessage = message;
        }
        else if (data)
        {
            if (data.fields.Name.value != null)
                this.contactName = data.fields.Name.value;

            if (data.fields.Cerebro_Student_ID__c.value == null || data.fields.SingleSignOnStatus__c.value == null || data.fields.SingleSignOnStatus__c.value === "None")
            {
                this.resetEnabled = false;
                this.disabled = false;
                this.disableResetButton = true;
            }

            this.loading = false;
        }
    }

    handleConfirmButtonClick()
    {
        this.loading = true;
        this.disabled = true;
        this.disableResetButton = true;

        resetPassword({ contactId: this.recordId })
        .then(result => 
        {
            if (result && result.includes("success"))
                this.resetSuccess = true;

            this.loading = false;
        })
        .catch(error =>
        {
            let message;
            
            if (Array.isArray(error.body))
                message = error.body.map(e => e.message).join(', ');
            else if (typeof error.body.message === 'string')
                message = error.body.message;

            this.errorMessage = message;

            this.loading = false;
            this.disabled = false;
            this.resetEnabled = false;
        });
    }

    handleCancelButtonClick()
    {
        this.dispatchEvent(new CustomEvent('closemodal'));
    }

    updateStates(loading, disabled, resetEnabled)
    {
        this.loading = loading;
        this.disabled = disabled;
        this.resetEnabled = resetEnabled;
    }
}