import { LightningElement, api, wire } from 'lwc';

import { getRecord, getFieldValue } from 'lightning/uiRecordApi';
import CONTACT_OBJECT from '@salesforce/schema/Contact';
import NAME_FIELD from '@salesforce/schema/Contact.Name';
import RECTYPE_FIELD from '@salesforce/schema/Contact.RecordType.DeveloperName';
import resourceName from '@salesforce/resourceUrl/ContactRecordTypeBarIcons';

export default class contactRecordTypeBar extends LightningElement {

    iconProspect = resourceName + '/icons/IdCardIcon.svg#Prospect_RecordType_Icon';
    iconStudent = resourceName + '/icons/StudyIcon.svg#Student_RecordType_Icon';
    iconOther = resourceName + '/icons/GeneralContactIcon.svg#Other_RecordTypes_Icon';

    recordTypeDevName = '';

    /** Id of record to display. */
    @api recordId;

    /* Expose schema objects/fields to the template. */
    contactObject = CONTACT_OBJECT;

    /* Load Contact.RecordType.DeveloperName and Contact.Name for custom rendering */
    @wire(getRecord, { recordId: '$recordId', fields: [NAME_FIELD, RECTYPE_FIELD] })
    record;

    get isStudentRecordType()
    {
        this.recordTypeDevName = getFieldValue(this.record.data, RECTYPE_FIELD);

        return (this.recordTypeDevName === 'Students') ? true : false;
    }

    get isProspectRecordType()
    {
        this.recordTypeDevName = getFieldValue(this.record.data, RECTYPE_FIELD);

        return (this.recordTypeDevName === 'Prospect') ? true : false;
    }

    get isOtherRecordType()
    {
        this.recordTypeDevName = getFieldValue(this.record.data, RECTYPE_FIELD);

        return (this.recordTypeDevName !== undefined && this.recordTypeDevName !== 'Students' && this.recordTypeDevName !== 'Prospect') ? true : false;
    }
}