import { LightningElement, track, api, wire } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { getRecord } from 'lightning/uiRecordApi';
import { loadStyle } from 'lightning/platformResourceLoader';

import calcResource from '@salesforce/resourceUrl/CalculatorStyles';
import baseUrl from '@salesforce/label/c.OAFBaseURL';

const FIELDS =
[
    'Contact.CalculatorPayPeriod__c',
    'Contact.CalculatorSalaryPerAnnum__c'
];

export default class FeeHelpCalculator extends LightningElement
{
    @track loaded = false;
    @track noValues = true;
    @track iframeUrl;
    @api recordId;
    @track contact;

    initialised = false;

    @wire(getRecord, { recordId: '$recordId', fields: FIELDS })
    wiredRecord({ error, data })
    {
        if (error)
        {
            let message = 'Unknown error';

            if (Array.isArray(error.body))
                message = error.body.map(e => e.message).join(', ');
            else if (typeof error.body.message === 'string')
                message = error.body.message;

            this.dispatchEvent(
                new ShowToastEvent(
                {
                    title: 'Error loading contact',
                    message,
                    variant: 'error',
                }),
            );
        }
        else if (data)
        {
            this.contact = data;

            if (this.contact.fields.CalculatorSalaryPerAnnum__c.value != null && this.contact.fields.CalculatorPayPeriod__c.value != null)
            {
                this.noValues = false;
                this.iframeUrl = baseUrl +  "fee-help-calculator/embed.html?salary=" + this.contact.fields.CalculatorSalaryPerAnnum__c.value + "&period=" +
                    this.contact.fields.CalculatorPayPeriod__c.value;
            }
        }

        this.loaded = true;
    }

    renderedCallback()
    {
        if (this.initialised)
            return;

        Promise.all(
        [
            loadStyle(this, calcResource)
        ])
        .then(() =>
        {
            this.initialised = true;
        })
        .catch(error =>
        {
            this.dispatchEvent(
                new ShowToastEvent(
                {
                    title: 'Error loading css',
                    message: JSON.stringify(error),
                    variant: 'error',
                }),
            );
        });
    }
}