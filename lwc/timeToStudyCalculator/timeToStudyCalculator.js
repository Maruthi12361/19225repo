import { LightningElement, track, api, wire } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { getRecord } from 'lightning/uiRecordApi';
import { loadStyle } from 'lightning/platformResourceLoader';

import calcResource from '@salesforce/resourceUrl/CalculatorStyles';
import baseUrl from '@salesforce/label/c.OAFBaseURL';

const FIELDS =
[
    'Contact.CalculatorStudyEvening__c',
    'Contact.CalculatorStudyWeekend__c',
    'Contact.CalculatorStudyWork__c',
    'Contact.CalculatorStudyCommuting__c',
    'Contact.CalculatorStudyMorning__c'
];

export default class TimeToStudyCalculator extends LightningElement
{
    @track loaded = false;
    @track noValues = true;
    @track iframeUrl;
    @api recordId;
    @track contact;

    @wire(getRecord, { recordId: '$recordId', fields: FIELDS })
    wiredRecord({ error, data })
    {
        if (error)
        {
            let message = 'Unknown error';

            if (Array.isArray(error.body)) 
                message = error.body.map(e => e.message).join(', ');
            else if (typeof error.body.message === 'string')
                message = error.body.message;

            this.dispatchEvent(
                new ShowToastEvent(
                {
                    title: 'Error loading contact',
                    message,
                    variant: 'error',
                }),
            );
        }
        else if (data)
        {
            this.contact = data;

            if (this.contact.fields.CalculatorStudyMorning__c.value != null && this.contact.fields.CalculatorStudyEvening__c.value != null
                && this.contact.fields.CalculatorStudyCommuting__c.value != null && this.contact.fields.CalculatorStudyWork__c.value != null
                && this.contact.fields.CalculatorStudyWeekend__c.value != null)
            {
                this.noValues = false;
                this.iframeUrl = baseUrl +  "time-to-study-calculator/embed.html?morning=" + this.contact.fields.CalculatorStudyMorning__c.value + "&evening=" +
                    this.contact.fields.CalculatorStudyEvening__c.value + "&commuting=" + this.contact.fields.CalculatorStudyCommuting__c.value + "&work=" +
                    this.contact.fields.CalculatorStudyWork__c.value + "&weekend=" + this.contact.fields.CalculatorStudyWeekend__c.value;
            }
        }
        this.loaded = true;
    }

    renderedCallback()
    {
        if (this.initialised)
            return;

        Promise.all(
        [
            loadStyle(this, calcResource)
        ])
        .then(() =>
        {
            this.initialised = true;
        })
        .catch(error =>
        {
            this.dispatchEvent(
                new ShowToastEvent(
                {
                    title: 'Error loading css',
                    message: JSON.stringify(error),
                    variant: 'error',
                }),
            );
        });
    }
}