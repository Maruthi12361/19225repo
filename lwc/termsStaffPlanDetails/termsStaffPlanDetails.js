import { LightningElement, track, api, wire } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

import getStaffPlanDetails from '@salesforce/apex/SubjectOfferingController.getStaffPlanDetails';

export default class TermsStaffPlanDetails extends LightningElement
{
    @api recordId;

    @track subjectOfferings;
    @track options;
    @track loading = true;

    @wire(getStaffPlanDetails, { termId: '$recordId' }) getSubjectOfferingList({ error, data })
    {
        if (error)
        {
            let message = 'Unknown error';

            if (Array.isArray(error.body))
                message = error.body.map(e => e.message).join(', ');
            else if (typeof error.body.message === 'string')
                message = error.body.message;

            this.dispatchEvent(new ShowToastEvent({ title: 'Error loading contact', message: message, variant: 'error' }));
            this.loading = false;
        }
        else if (data)
        {
            let result = JSON.parse(data);
            this.subjectOfferings = result.SubjectOfferings;

            if (result.Option && result.Option.Data)
                this.options = result.Option.Data;

            this.loading = false;
        }
    }
}