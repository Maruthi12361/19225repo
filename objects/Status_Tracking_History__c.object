<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableLicensing>false</enableLicensing>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>Private</externalSharingModel>
    <fields>
        <fullName>Active__c</fullName>
        <description>TRUE = Flags the record to identify if it is open (i.e.: an endDateTime has not been set) 
 FALSE = identifies that the Snapshot is closed (i.e.: an endDateTime has been set)</description>
        <externalId>false</externalId>
        <formula>ISBLANK(EndDateTime__c)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Active</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>BusinessDaysInStage__c</fullName>
        <description>Calculated field, showing time in current stage in Business Days to 2 decimal places. There are 8.5 business hours per day and hence Business Days = Business Minutes / 60 / 8.5.</description>
        <externalId>false</externalId>
        <formula>BusinessMinutesInStage__c/510</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Business Days In Stage</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>BusinessHoursInStage__c</fullName>
        <description>Calculated field, showing time in current stage in Business Hours to 2 decimal places.</description>
        <externalId>false</externalId>
        <formula>BusinessMinutesInStage__c/60</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Business Hours In Stage</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>BusinessMinutesInStage__c</fullName>
        <description>Number of minutes this record has been in this stage for as of when this field was last calculated.</description>
        <externalId>false</externalId>
        <label>Business Minutes In Stage</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Call_Outcome__c</fullName>
        <description>This picklist is used to set a Call Outcome against the Tracking History record</description>
        <externalId>false</externalId>
        <inlineHelpText>This picklist is used to set a Call Outcome against the Tracking History record</inlineHelpText>
        <label>Call Outcome</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Phone Disconnected/Barred</fullName>
                    <default>false</default>
                    <label>Phone Disconnected/Barred</label>
                </value>
                <value>
                    <fullName>Spoke to Another person</fullName>
                    <default>false</default>
                    <label>Spoke to Another person</label>
                </value>
                <value>
                    <fullName>Phone off</fullName>
                    <default>false</default>
                    <label>Phone off</label>
                </value>
                <value>
                    <fullName>Invalid Phone Number</fullName>
                    <default>false</default>
                    <label>Invalid Phone Number</label>
                </value>
                <value>
                    <fullName>CA Missed Call</fullName>
                    <default>false</default>
                    <label>CA Missed Call</label>
                </value>
                <value>
                    <fullName>Left Voicemail Message</fullName>
                    <default>false</default>
                    <label>Left Voicemail Message</label>
                </value>
                <value>
                    <fullName>Engaged</fullName>
                    <default>false</default>
                    <label>Engaged</label>
                </value>
                <value>
                    <fullName>Rings Out</fullName>
                    <default>false</default>
                    <label>Rings Out</label>
                </value>
                <value>
                    <fullName>Didn&apos;t leave Voicemail</fullName>
                    <default>false</default>
                    <label>Didn&apos;t leave Voicemail</label>
                </value>
                <value>
                    <fullName>Spoke to Lead By Phone</fullName>
                    <default>false</default>
                    <label>Spoke to Lead By Phone</label>
                </value>
                <value>
                    <fullName>Email Received from Lead</fullName>
                    <default>false</default>
                    <label>Email Received from Lead</label>
                </value>
                <value>
                    <fullName>Admin (Neutral)</fullName>
                    <default>false</default>
                    <label>Admin (Neutral)</label>
                </value>
                <value>
                    <fullName>Not Contacted Yet</fullName>
                    <default>false</default>
                    <label>Not Contacted Yet</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Call_Stage__c</fullName>
        <description>This field is used to store the Call Stage value (i.e. 0FC to 6FC)</description>
        <externalId>false</externalId>
        <inlineHelpText>This field is used to store the Call Stage value (i.e. 0FC to 6FC)</inlineHelpText>
        <label>Call Stage</label>
        <length>5</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Campaign__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Campaign</label>
        <referenceTo>Campaign</referenceTo>
        <relationshipLabel>Status Tracking History</relationshipLabel>
        <relationshipName>Status_Tracking_History</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>CaseId__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>This lookup field is used to link each tracking history record to its parent Case.</description>
        <externalId>false</externalId>
        <inlineHelpText>This lookup field is used to link each tracking history record to its parent Case.</inlineHelpText>
        <label>Case Id</label>
        <referenceTo>Case</referenceTo>
        <relationshipLabel>Status Tracking History</relationshipLabel>
        <relationshipName>TrackingHistory</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Contact_ID__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>This lookup field is used to link each tracking history record to a Contact</description>
        <externalId>false</externalId>
        <inlineHelpText>This lookup field is used to link each tracking history record to a Contact</inlineHelpText>
        <label>Contact ID</label>
        <referenceTo>Contact</referenceTo>
        <relationshipLabel>Status Tracking History</relationshipLabel>
        <relationshipName>Tracking_History</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>DaysInStage__c</fullName>
        <description>Calculated field, which calculates time in days between startDateTime and endDateTime or if no end time uses now for endDateTime to 2 decimal places.</description>
        <externalId>false</externalId>
        <formula>MinutesInStage__c / 1440</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Days In Stage</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Department__c</fullName>
        <description>Field to capture the department value for a STH Phase</description>
        <externalId>false</externalId>
        <label>Department</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Direction__c</fullName>
        <description>Field to capture the direction of status change (forward or backward) for a STH Phase</description>
        <externalId>false</externalId>
        <label>Direction</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Forward</fullName>
                    <default>true</default>
                    <label>Forward</label>
                </value>
                <value>
                    <fullName>Backward</fullName>
                    <default>false</default>
                    <label>Backward</label>
                </value>
                <value>
                    <fullName>None</fullName>
                    <default>false</default>
                    <label>None</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>EndDateTime__c</fullName>
        <description>The end date time of the status tracking history record indicating when this status/phase was closed/completed and changed over to the next state.</description>
        <externalId>false</externalId>
        <label>End Date Time</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>HoursInStage__c</fullName>
        <description>Calculated field, which calculates time in hours to 2 decimal places between startDateTime and endDateTime or if no end time uses now for endDateTime.</description>
        <externalId>false</externalId>
        <formula>MinutesInStage__c / 60</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Hours In Stage</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Is_Current_Opportunity_Status__c</fullName>
        <externalId>false</externalId>
        <formula>IF( NOT(ISNULL(Opportunity_ID__c)) &amp;&amp; NOT(ISBLANK(Opportunity_ID__c)), 

IF( TEXT(Opportunity_ID__r.StageName) =  Status_Stage__c , TRUE, FALSE), FALSE)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Is Current Opportunity Status</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Is_Milestone__c</fullName>
        <defaultValue>false</defaultValue>
        <description>A flag to identify if a record is a point in time with a non-duration</description>
        <externalId>false</externalId>
        <label>Is Milestone</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Lead_Status_ID__c</fullName>
        <externalId>true</externalId>
        <label>Lead Status ID</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Lead_Timer_Minutes__c</fullName>
        <description>This field is used to store the Lead Timer value in minutes since the most recent Call. THis is the working minutes and not the elapsed time since last call.</description>
        <externalId>false</externalId>
        <inlineHelpText>This field is used to store the Lead Timer value in minutes since the most recent Call. THis is the working minutes and not the elapsed time since last call.</inlineHelpText>
        <label>Lead Timer Minutes</label>
        <precision>5</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>MinutesInStage__c</fullName>
        <description>Calculated field, which calculates time in minutes between startDateTime and endDateTime or if no end time uses now for endDateTime.</description>
        <externalId>false</externalId>
        <formula>IF
(
    ISBLANK(EndDateTime__c),
    ABS(NOW() - StartDateTime__c)*1440,
    ABS(EndDateTime__c - StartDateTime__c)*1440
)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Minutes In Stage</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Next_Status_Stage__c</fullName>
        <description>Holds the exit, close status value</description>
        <externalId>false</externalId>
        <label>Next Status/Stage</label>
        <length>50</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Next_User__c</fullName>
        <description>Holds the exit, close User value</description>
        <externalId>false</externalId>
        <label>Next User</label>
        <length>128</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Object__c</fullName>
        <description>Formulate the primary object that the record is related to based on whether the Opportunity/Case or Contact Id fields are blank or not.</description>
        <externalId>false</externalId>
        <formula>IF
(
    AND
    (
        ISBLANK(Opportunity_ID__c),
        ISBLANK(CaseId__c),
        NOT(ISBLANK(Contact_ID__c))
    ),
    &apos;Contact&apos;,
    IF
    (
        NOT(ISBLANK(Opportunity_ID__c)),
        &apos;Opportunity&apos;,
        IF
        (
            NOT(ISBLANK(CaseId__c)),
            &apos;Case&apos;,
            null
        )
    )
)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Object</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Opportunity_ID__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>This lookup field is used to link each tracking history record to an Opportunity</description>
        <externalId>false</externalId>
        <inlineHelpText>This lookup field is used to link each tracking history record to an Opportunity</inlineHelpText>
        <label>Opportunity ID</label>
        <referenceTo>Opportunity</referenceTo>
        <relationshipName>Tracking_History</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>PhaseMetric__c</fullName>
        <description>Formulate the phase between start and end of the record and therefore the inferred or formulated metric</description>
        <externalId>false</externalId>
        <formula>Status_Stage__c &amp; &quot;-&quot; &amp; Next_Status_Stage__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Phase/Metric</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Related_Record__c</fullName>
        <description>This formula field displays a more user-friendly link to the referencing record. It will either be an Application Case, an Opportunity, a Contact or a Lead.</description>
        <externalId>false</externalId>
        <formula>IF
(
    NOT(ISBLANK(CaseId__c)),
    HYPERLINK(&apos;/&apos; &amp; CaseId__c, &apos;Application-&apos; &amp; &apos;Application Case: &apos; &amp; CaseId__r.CaseNumber &amp; &apos;-&apos; &amp; TEXT(CaseId__r.Status), &apos;_self&apos;),
    IF
    (
        NOT(ISBLANK(Opportunity_ID__c)),
        HYPERLINK(&apos;/&apos; &amp; Opportunity_ID__c, &apos;Opportunity: &apos; &amp; Opportunity_ID__r.Name, &apos;_self&apos;),
        IF
        (
            NOT(ISBLANK(Contact_ID__c)),
            HYPERLINK(&apos;/&apos; &amp; Contact_ID__r.Id , &apos;Contact: &apos; &amp; Contact_ID__r.FirstName &amp; &apos; &apos; &amp; Contact_ID__r.LastName  &amp; &apos;-Cerebro Student ID: &apos; &amp; Contact_ID__r.Cerebro_Student_ID__c, &apos;_self&apos;),
            NULL
        )
    )
)</formula>
        <inlineHelpText>This formula field displays a more user-friendly link to the referencing record. It will either be an Application Case, an Opportunity, a Contact or a Lead.</inlineHelpText>
        <label>Related Record</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>StartDateTime__c</fullName>
        <description>The start date time of the status tracking history record indicating when this status/phase was begun.</description>
        <externalId>false</externalId>
        <label>Start Date Time</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>StatusTrackingHistory__c</fullName>
        <description>A field using &apos;The Power of One&apos; formula to allow a count of parent records.</description>
        <externalId>false</externalId>
        <formula>1</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>StatusTrackingHistory</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Status_Stage__c</fullName>
        <description>This field is used to store the Lead Status for any Lead Tracking and the Opportunity Stage for any Opportunities Status Tracking records.</description>
        <externalId>false</externalId>
        <inlineHelpText>This field is used to store the Lead Status for any Lead Tracking and the Opportunity Stage for any Opportunities Status Tracking records.</inlineHelpText>
        <label>Status/Stage</label>
        <length>50</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Type__c</fullName>
        <description>Defines the type of snapshot eg state change, user reassignment, or waiting on customer parallel Boolean etc</description>
        <externalId>false</externalId>
        <label>Type</label>
        <length>128</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>User__c</fullName>
        <description>Holds the entry, open User value</description>
        <externalId>false</externalId>
        <label>User</label>
        <length>128</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Status Tracking History</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>Related_Record__c</columns>
        <columns>Call_Outcome__c</columns>
        <columns>Call_Stage__c</columns>
        <columns>Status_Stage__c</columns>
        <columns>Lead_Timer_Minutes__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <displayFormat>Tracking ID-{00000000}</displayFormat>
        <label>Status History ID</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Status Tracking History</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>Related_Record__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Call_Outcome__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Call_Stage__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Status_Stage__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Lead_Timer_Minutes__c</customTabListAdditionalFields>
    </searchLayouts>
    <sharingModel>Read</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
