<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <comment>Action override created by Lightning App Builder during activation.</comment>
        <content>CoursePlanRecordPage</content>
        <formFactor>Large</formFactor>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Flexipage</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>CoursePlanCompactLayout</compactLayoutAssignment>
    <compactLayouts>
        <fullName>CoursePlanCompactLayout</fullName>
        <fields>Name</fields>
        <fields>hed__Account__c</fields>
        <fields>hed__Start_Date__c</fields>
        <fields>hed__End_Date__c</fields>
        <fields>hed__Version__c</fields>
        <fields>hed__Is_Primary__c</fields>
        <label>Course Plan Compact Layout</label>
    </compactLayouts>
    <compactLayouts>
        <fullName>hed__HEDA_Program_Plan_Compact_Layout</fullName>
        <fields>Name</fields>
        <fields>hed__Start_Date__c</fields>
        <fields>hed__End_Date__c</fields>
        <label>HEDA Program Plan Compact Layout</label>
    </compactLayouts>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>Links Courses to an Academic Program to create a path for students to follow. An Academic Program can list multiple Courses, and a Course can be associated with multiple Academic Programs.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ReadWrite</externalSharingModel>
    <fields>
        <fullName>CerebroID__c</fullName>
        <caseSensitive>false</caseSensitive>
        <externalId>true</externalId>
        <label>Cerebro ID</label>
        <length>10</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>hed__Account__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>Lookup field to find an Account (often an Academic Program).</description>
        <externalId>false</externalId>
        <inlineHelpText>Click to search for an Account (often an Academic Program) to associate with the Course Plan.</inlineHelpText>
        <label>Account</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Program Plans</relationshipLabel>
        <relationshipName>Program_Plans</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>hed__Description__c</fullName>
        <deprecated>false</deprecated>
        <description>The description of Program Plan.</description>
        <externalId>false</externalId>
        <inlineHelpText>Describe the Course Plan.</inlineHelpText>
        <label>Description</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>hed__End_Date__c</fullName>
        <deprecated>false</deprecated>
        <description>The end date for the Program Plan.</description>
        <externalId>false</externalId>
        <inlineHelpText>The end date for the Course Plan.</inlineHelpText>
        <label>End Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>hed__Is_Primary__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Checkbox to designate this Plan as the one to assign to a student when creating a Program Enrollment</description>
        <externalId>false</externalId>
        <inlineHelpText>Checkbox to designate this Course Plan as the one to assign to a student when creating a Course Enrolment</inlineHelpText>
        <label>Is Primary</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>hed__Start_Date__c</fullName>
        <deprecated>false</deprecated>
        <description>The date the Program Plan went into effect.</description>
        <externalId>false</externalId>
        <inlineHelpText>The date the Course Plan went into effect.</inlineHelpText>
        <label>Start Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>hed__Status__c</fullName>
        <deprecated>false</deprecated>
        <description>The status of the program plan.</description>
        <externalId>false</externalId>
        <inlineHelpText>The status of the Course Plan.</inlineHelpText>
        <label>Status</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Current</fullName>
                    <default>true</default>
                    <label>Current</label>
                </value>
                <value>
                    <fullName>Archived</fullName>
                    <default>false</default>
                    <label>Archived</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>hed__Total_Required_Credits__c</fullName>
        <deprecated>false</deprecated>
        <description>The total number of credits a student must complete to satisfy this Program Plan.</description>
        <externalId>false</externalId>
        <inlineHelpText>Specify the total number of credits a student must complete to satisfy this program plan.</inlineHelpText>
        <label>Total Required Credits</label>
        <precision>6</precision>
        <required>false</required>
        <scale>3</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>hed__Version__c</fullName>
        <deprecated>false</deprecated>
        <description>The version of the Program Plan. For example, “Spring 2019.”</description>
        <externalId>false</externalId>
        <inlineHelpText>The version of the Course Plan. For example, “Spring 2019.”</inlineHelpText>
        <label>Version</label>
        <length>25</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Program Plan</label>
    <listViews>
        <fullName>hed__All</fullName>
        <columns>NAME</columns>
        <columns>hed__Account__c</columns>
        <columns>hed__Status__c</columns>
        <columns>hed__Version__c</columns>
        <columns>hed__Is_Primary__c</columns>
        <columns>CerebroID__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <listViews>
        <fullName>hed__Primary_Program_Plan</fullName>
        <columns>NAME</columns>
        <columns>hed__Account__c</columns>
        <columns>hed__Start_Date__c</columns>
        <columns>hed__End_Date__c</columns>
        <columns>hed__Is_Primary__c</columns>
        <columns>hed__Status__c</columns>
        <filterScope>Mine</filterScope>
        <filters>
            <field>hed__Is_Primary__c</field>
            <operation>equals</operation>
            <value>1</value>
        </filters>
        <label>Primary Program Plan</label>
    </listViews>
    <nameField>
        <label>Program Plan Name</label>
        <type>Text</type>
    </nameField>
    <pluralLabel>Program Plans</pluralLabel>
    <searchLayouts>
        <searchResultsAdditionalFields>hed__Account__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>hed__Start_Date__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>hed__End_Date__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>hed__Is_Primary__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>hed__Status__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
