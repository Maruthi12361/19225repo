<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>true</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableLicensing>false</enableLicensing>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>Private</externalSharingModel>
    <fields>
        <fullName>AutorabitExtId__c</fullName>
        <description>External Id created by AutoRABIT</description>
        <externalId>true</externalId>
        <label>AutorabitExtId__c</label>
        <length>18</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Description__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Enter a brief description of this Subject Content item.</inlineHelpText>
        <label>Description</label>
        <length>100</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Documentation_URL__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Copy and paste the URL of a SharePoint document and library.</inlineHelpText>
        <label>Documentation URL</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Url</type>
    </fields>
    <fields>
        <fullName>Due_Date__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>The due date of this Subject Content item.</inlineHelpText>
        <label>Due Date</label>
        <required>false</required>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Name__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>The name of this Subject Content item.</inlineHelpText>
        <label>Name</label>
        <length>50</length>
        <required>true</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Owner_Name__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Owner Name</label>
        <referenceTo>User</referenceTo>
        <relationshipName>Subject_Content_Owner</relationshipName>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Process_Log__c</fullName>
        <externalId>false</externalId>
        <label>Process Log</label>
        <length>131072</length>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Progress_Bar__c</fullName>
        <externalId>false</externalId>
        <formula>IF(ISPICKVAL(Status__c,&quot;In Progress&quot;), 
	IMAGE(&quot;/resource/1501559418000/ProgressBar/20.png&quot;,&quot;20%&quot;), 
IF(ISPICKVAL(Status__c,&quot;In Review&quot;),  
	IMAGE(&quot;/resource/1501559418000/ProgressBar/80.png&quot;,&quot;80%&quot;), 
IF(ISPICKVAL(Status__c,&quot;Closed&quot;), 
	IMAGE(&quot;/resource/1501559418000/ProgressBar/100.png&quot;,&quot;100%&quot;), 
	IMAGE(&quot;/resource/1501559418000/ProgressBar/0.png&quot;,&quot;0%&quot;)
	)))</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Progress</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Progress_Percentage__c</fullName>
        <externalId>false</externalId>
        <formula>IF(ISPICKVAL(Status__c,&quot;In Progress&quot;), 
	20, 
	IF(ISPICKVAL(Status__c,&quot;In Review&quot;),  
		80, 
		IF(ISPICKVAL(Status__c,&quot;Closed&quot;), 
			100, 
			0
		)
	)
)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Progress %</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <externalId>false</externalId>
        <label>Status</label>
        <required>true</required>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>New</fullName>
                    <default>true</default>
                    <label>New</label>
                </value>
                <value>
                    <fullName>In Progress</fullName>
                    <default>false</default>
                    <label>In Progress</label>
                </value>
                <value>
                    <fullName>In Review</fullName>
                    <default>false</default>
                    <label>In Review</label>
                </value>
                <value>
                    <fullName>Closed</fullName>
                    <default>false</default>
                    <label>Closed</label>
                </value>
                <value>
                    <fullName>On Hold</fullName>
                    <default>false</default>
                    <label>On Hold</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Subject_Build_Owner__c</fullName>
        <externalId>false</externalId>
        <formula>Subject_Build__r.Owner:User.FirstName &amp; &quot; &quot; &amp; Subject_Build__r.Owner:User.LastName</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Subject Build Owner</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Subject_Build__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <externalId>false</externalId>
        <inlineHelpText>The parent Subject Build that this Subject Content item refers to.</inlineHelpText>
        <label>Subject Build</label>
        <referenceTo>Subject_Build__c</referenceTo>
        <relationshipLabel>Subject Content</relationshipLabel>
        <relationshipName>Subject_Content_Item_Lookup</relationshipName>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <label>Subject Content</label>
    <nameField>
        <displayFormat>SC-{00000}</displayFormat>
        <label>Subject Content ID</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Subject Content</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
    <validationRules>
        <fullName>SubjectContentOwner_Is_Not_Blank</fullName>
        <active>true</active>
        <description>Subject Content items always requires an owner.</description>
        <errorConditionFormula>AND(
    ISBLANK(Owner_Name__c),
    NOT(ISNEW())
)</errorConditionFormula>
        <errorDisplayField>Owner_Name__c</errorDisplayField>
        <errorMessage>Subject Content Owner can not be blank.</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
</CustomObject>
