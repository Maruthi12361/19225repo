<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Case_Set_Status_Waiting</fullName>
        <description>DELETE 41 - Updates the case status to waiting</description>
        <field>Status</field>
        <literalValue>Waiting</literalValue>
        <name>Case-Set Status Waiting</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Set_Status_With_Contact</fullName>
        <description>DELETE 41 - Updates the case status to With Contact</description>
        <field>Status</field>
        <literalValue>With Contact</literalValue>
        <name>Case-Set Status With Contact</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Update_First_Email_DateTime</fullName>
        <description>DELETE 41 - Updates the Case.First_Email_Date_Time__c field with the time of the first AIB Email sent (as a result of a response to a students email).</description>
        <field>First_Email_Date_Time__c</field>
        <formula>IF(ISNULL(Parent.First_Email_Date_Time__c), NOW(), Parent.First_Email_Date_Time__c)</formula>
        <name>Case - Update First AIB Email DateTime</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Update_First_Student_Email_DateTime</fullName>
        <description>DELETE 41 - Updates the Case.First_Email_Student_Date_Time__c field with the time of the first Student Email received.</description>
        <field>First_Email_Student_Date_Time__c</field>
        <formula>IF(ISNULL(Parent.First_Email_Student_Date_Time__c), NOW(), Parent.First_Email_Student_Date_Time__c)</formula>
        <name>Case-Update First Student Email DateTime</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Update_Latest_Contact_Date</fullName>
        <description>DELETE 41 - Updates the Latest_Interaction_Date_Time__c whenever an email is sent or received by AIB or from Student.</description>
        <field>Latest_Interaction_Date_Time__c</field>
        <formula>NOW()</formula>
        <name>Case - Update Latest Contact Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>EmailMessage - Rollover update %27With Student%27 when SSA recieves an email</fullName>
        <actions>
            <name>Case_Set_Status_Waiting</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_Update_Latest_Contact_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>DELETE 41 - Update case status when Rollover Case receives an email from student to Waiting and update Latest Contact Date</description>
        <formula>AND
(
    Incoming,
    Parent.RecordType.DeveloperName = &apos;Rollover&apos;
)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>EmailMessage - Rollover update %27With Student%27 when SSA sends an email</fullName>
        <actions>
            <name>Case_Set_Status_With_Contact</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_Update_Latest_Contact_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>DELETE 41 - Update the case status when Rollover case sends an email to the Student to With Contact and update the Latest Contact Date</description>
        <formula>AND (      Parent.RecordType.DeveloperName = &apos;Rollover&apos;,      FromName &lt;&gt; &apos;AIB Student Support&apos;,      NOT(Incoming) )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>EmailMessage - Update Case Status When Agent sends an email</fullName>
        <actions>
            <name>Case_Set_Status_With_Contact</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_Update_Latest_Contact_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>DELETE 41 - Update Case Status When Agent sends an email as well as the Case: Latest Interaction Date/Time, as of 20170906 this should only be applied to Cases that are from the Case Management type, ie: not Application or Rollover.</description>
        <formula>AND (     Parent.RecordType.DeveloperName &lt;&gt; &apos;Application&apos;,     Parent.RecordType.DeveloperName &lt;&gt; &apos;Rollover&apos;,     NOT(ISPICKVAL(Parent.WorkType__c, &apos;Legacy Kayako&apos;)),     FromName &lt;&gt; &apos;AIB Student Support&apos;,     NOT(Incoming) )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>EmailMessage - Update Case Status When Student Replies</fullName>
        <actions>
            <name>Case_Set_Status_Waiting</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_Update_Latest_Contact_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>EmailMessage.Incoming</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Application,Rollover</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.WorkType__c</field>
            <operation>notEqual</operation>
            <value>Legacy Kayako</value>
        </criteriaItems>
        <description>DELETE 41 - For cases that are of Student Support type (i.e.: Not Application or Rollover) then Update Case Status When Student replies to Waiting as well as the Case: Latest Interaction Date/Time</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>EmailMessage - Update First AIB Email Response Time</fullName>
        <actions>
            <name>Case_Update_First_Email_DateTime</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>DELETE 41 - Updates the First Email field for AIB when an outgoing message that is not the Auto Response email gets sent and only if a student email has already been received earlier AND the First Email AIB field is not yet set.</description>
        <formula>AND
(
    FromName &lt;&gt; &apos;AIB Student Support&apos;,
    NOT(Incoming),
    OR
    (
        ISNULL(Parent.First_Email_Date_Time__c),
        ISBLANK(Parent.First_Email_Date_Time__c)
    ),
    OR
    (
        NOT(ISNULL(Parent.First_Email_Student_Date_Time__c)),
        NOT(ISBLANK(Parent.First_Email_Student_Date_Time__c))
    )
)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>EmailMessage - Update First Student Email Received Time</fullName>
        <actions>
            <name>Case_Update_First_Student_Email_DateTime</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>DELETE 41 - Updates the First Email Student field when an incoming message from a Student is received and only if the First Email Student Date Time field has not yet been set. In this way we capture the FIRST email from the student.</description>
        <formula>AND
(
    Incoming,
    OR
    (
        ISNULL(Parent.First_Email_Student_Date_Time__c),
        ISBLANK(Parent.First_Email_Student_Date_Time__c)
    )
)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
