<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>IT_Notification_of_New_Starter</fullName>
        <ccEmails>staffsupport@aib.edu.au</ccEmails>
        <description>IT Notification of New Starter</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>AIB_HR/New_Starter_Request_IT_Notification</template>
    </alerts>
    <alerts>
        <fullName>New_Starter_Reception_Notification</fullName>
        <ccEmails>Reception2@aib.edu.au</ccEmails>
        <ccEmails>Darren.Clark@aib.edu.au</ccEmails>
        <description>New Starter Reception Notification</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>AIB_HR/New_Starter_Request_Reception_Darren_Notification</template>
    </alerts>
</Workflow>
