<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Approval_of_single_step_notification</fullName>
        <description>Approval of single step notification</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Single_Step_Approval_of_Request_for_Resourcing</template>
    </alerts>
    <alerts>
        <fullName>Full_Approval_of_RFR_Notification_to_start_recruitment</fullName>
        <description>Full Approval of RFR - Notification to start recruitment</description>
        <protected>false</protected>
        <recipients>
            <recipient>AIB_HR</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>donotreply@aib.edu.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>AIB_HR/Request_for_Resourcing_Start_Recruitment_Notification</template>
    </alerts>
    <alerts>
        <fullName>Notification_of_Step_1_Approval_of_the_Request_for_Resourcing</fullName>
        <description>Notification of Step 1 Approval of the Request for Resourcing</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Single_Step_Approval_of_Request_for_Resourcing</template>
    </alerts>
    <alerts>
        <fullName>Request_for_Resourcing_Approval_for_single_Step</fullName>
        <description>Request for Resourcing Approval for single Step</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Single_Step_Approval_of_Request_for_Resourcing</template>
    </alerts>
    <alerts>
        <fullName>Request_for_Resourcing_Full_Approval</fullName>
        <description>Request for Resourcing Full Approval</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>Initial_Approver__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>AIB_HR/Request_for_Resourcing_Full_Approval_Notification</template>
    </alerts>
    <alerts>
        <fullName>Request_for_Resourcing_Rejection_Email_Alert</fullName>
        <description>Request for Resourcing Rejection Email Alert</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>AIB_HR/Request_for_Resourcing_Rejection_Notification</template>
    </alerts>
    <alerts>
        <fullName>Single_Approval_for_Request_for_Resourcing</fullName>
        <description>Single Approval for Request for Resourcing</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>AIB_HR/Request_for_Resourcing_Single_Approval_Notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>Set_RFR_Approval_Status</fullName>
        <field>Final_Approval_Acquired__c</field>
        <literalValue>1</literalValue>
        <name>Set RFR Approval Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
