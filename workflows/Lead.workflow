<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>DecsOnD__Set_Email_Domain</fullName>
        <description>Set the Decisions on Demand Email Domain field from the Email</description>
        <field>DecsOnD__Email_Domain_DecsOnD__c</field>
        <formula>SUBSTITUTE(Email, LEFT(Email, FIND(&quot;@&quot;, Email)), NULL)</formula>
        <name>Set Email Domain</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>true</protected>
    </fieldUpdates>
    <rules>
        <fullName>DecsOnD__Set Email Domain</fullName>
        <actions>
            <name>DecsOnD__Set_Email_Domain</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set the Decisions on Demand Email Domain field from the Email</description>
        <formula>ISNEW() || ISCHANGED( Email )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
