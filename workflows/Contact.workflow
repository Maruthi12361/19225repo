<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Contact_Email_Express_Consent_Date_Remo</fullName>
        <description>This workflow will remove the date in Email_Express_Consent_Date__c</description>
        <field>Email_Express_Consent_Date__c</field>
        <name>Contact -Email Express Consent Date Remo</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contact_Email_Express_Consent_Date_adde</fullName>
        <description>This Field will be updated when the Email_Express_Consent_Exists__c is updated and is given today&apos;s date</description>
        <field>Email_Express_Consent_Date__c</field>
        <formula>TODAY ()</formula>
        <name>Contact -Email Express Consent Date adde</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contact_SMS_Express_Consent_Date_Remove</fullName>
        <description>This workflow will remove the date in SMS_Express_Consent_Date__c</description>
        <field>SMS_Express_Consent_Date__c</field>
        <name>Contact -SMS Express Consent Date Remove</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contact_SMS_Express_Consent_Date_added</fullName>
        <description>This Field will be updated when the SMS_Express_Consent_Exists__c is updated and is given today&apos;s date</description>
        <field>SMS_Express_Consent_Date__c</field>
        <formula>TODAY ()</formula>
        <name>Contact - SMS Express Consent Date added</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contact_Set_Express_Consent_Expires_Date</fullName>
        <description>Set Express Consent Expires Date to 6 months after CreatedDate if MailingCountry is Canada or MailingCountry is blank and Inferred Country is Canada otherwise 12 months.</description>
        <field>Express_Consent_Expires__c</field>
        <formula>IF 
(
    OR
    (
        MailingCountry = &quot;Canada&quot;, 
        AND 
        (
            MailingCountry = &quot;&quot;, 
            InferredCountry__c = &quot;Canada&quot;
        )
    ), 
    ADDMONTHS(CreatedDate,6), 
    ADDMONTHS(CreatedDate,12)
)</formula>
        <name>Contact Set Express Consent Expires Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DecsOnD__Set_Email_Domain</fullName>
        <description>Set the Decisions on Demand Email Domain field from the Email</description>
        <field>DecsOnD__Email_Domain_DecsOnD__c</field>
        <formula>SUBSTITUTE(Email, LEFT(Email, FIND(&quot;@&quot;, Email)), NULL)</formula>
        <name>Set Email Domain</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>true</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateCASLImpliedConsentExpiry</fullName>
        <field>CASLImpliedConsentExpiry__c</field>
        <formula>IF 
(
    OR
    (
        MailingCountry = &quot;Canada&quot;, 
        AND 
        (
            MailingCountry = &quot;&quot;, 
            InferredCountry__c = &quot;Canada&quot;
        )
    ), 
    CreatedDate + (365 * 2) - 30, 
    NULL
)</formula>
        <name>Update CASL Implied Consent Expiry</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_PreferredStartDateTime</fullName>
        <description>Updates the PreferredStartDateTime field based on whether in Preferred Start Date picklist we have the value null or At a Later Date in which case it sets it to null or else sets it to a date based on the value in the picklist.</description>
        <field>PreferredStartDateTime__c</field>
        <formula>IF
(
    AND
    (    
        NOT ISBLANK(TEXT(Preferred_Start_Date__c)),
        LEN(TEXT(Preferred_Start_Date__c)) = 10,
        NOT ISBLANK
        (
            DATEVALUE
            (
                RIGHT(TEXT(Preferred_Start_Date__c), 4) &amp; &quot;-&quot; &amp; 
                MID(TEXT(Preferred_Start_Date__c), 4, 2) &amp; &quot;-&quot; &amp; 
                LEFT(TEXT(Preferred_Start_Date__c), 2)
            )
        )
    ), 
    DATEVALUE
    (
        RIGHT(TEXT(Preferred_Start_Date__c), 4) &amp; &quot;-&quot; &amp; 
        MID(TEXT(Preferred_Start_Date__c), 4, 2) &amp; &quot;-&quot; &amp; 
        LEFT(TEXT(Preferred_Start_Date__c), 2)
    ), 
    null
)</formula>
        <name>Update PreferredStartDateTime</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Contact - Canada CASL Implied Expiration</fullName>
        <actions>
            <name>UpdateCASLImpliedConsentExpiry</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates the CASL Implied Expiry Date field to the date when Canadian people affected by CASL legislation should be contacted for explicit consent.</description>
        <formula>OR(ISCHANGED(MailingCountry), ISCHANGED(InferredCountry__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Contact - Set Express Consent Expires Date</fullName>
        <actions>
            <name>Contact_Set_Express_Consent_Expires_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Check if the Express Consent is null and calls the Field Update if it is to update the field.</description>
        <formula>ISNULL(Express_Consent_Expires__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Contact - Update Email Express Consent Date</fullName>
        <actions>
            <name>Contact_Email_Express_Consent_Date_adde</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Email_Express_Consent_Exists__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>This workflow will update the Email_Express_Consent_Date__c field when the Email_Express_Consent_Exists__c is updated to be true</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Contact - Update SMS Express Consent Date</fullName>
        <actions>
            <name>Contact_SMS_Express_Consent_Date_added</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.SMS_Express_Consent_Exists__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>This workflow will update the SMS_Express_Consent_Date__c field when the SMS_Express_Consent_Exists__c is updated to be true</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Contact -Update Email Express Consent Date Remove</fullName>
        <actions>
            <name>Contact_Email_Express_Consent_Date_Remo</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Email_Express_Consent_Exists__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>This workflow will remove the date from the Email_Express_Consent_Date__c field when the Email_Express_Consent_Exists__c is updated</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Contact -Update SMS Express Consent Date Remove</fullName>
        <actions>
            <name>Contact_SMS_Express_Consent_Date_Remove</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Email_Express_Consent_Exists__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>This workflow will remove the date from the SMS_Express_Consent_Date__c field when the SMS_Express_Consent_Exists__c is updated</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>DecsOnD__Set Email Domain</fullName>
        <actions>
            <name>DecsOnD__Set_Email_Domain</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set the Decisions on Demand Email Domain field from the Email</description>
        <formula>ISNEW() || ISCHANGED( Email )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Preferred Start Date Time</fullName>
        <actions>
            <name>Update_PreferredStartDateTime</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Used to update the PreferredStartDateTime__c field using the value from the picklist in Preferred_Start_Date__c and converting it to a date or if the value is &apos;At a Later Date&apos; converting it to a null value on creation or change of the record.</description>
        <formula>OR (     ISCHANGED(Preferred_Start_Date__c),     ISNEW() )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
