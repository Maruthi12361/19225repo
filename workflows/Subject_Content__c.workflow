<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Subject_Content_Change_Owner_Notification</fullName>
        <description>Subject Content Change Owner Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>salesforce.administrator@aib.edu.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>AIB_General_Templates/Subject_Content_Change_Owner_Notification</template>
    </alerts>
    <alerts>
        <fullName>Subject_Content_Due_Date_Notification</fullName>
        <description>Subject Content Due Date Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>salesforce.administrator@aib.edu.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>AIB_General_Templates/Subject_Content_Due_Date_Notification</template>
    </alerts>
</Workflow>
