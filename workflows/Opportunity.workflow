<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Opportunity_Update_Submit_Time</fullName>
        <field>OAF_Last_Submitted__c</field>
        <formula>Now()</formula>
        <name>Opportunity Update Submit Time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_New_Progression_Opportunity_Index</fullName>
        <field>UniqueProgressionOpportunityIndex__c</field>
        <formula>CASESAFEID(Contact__c) &amp; &apos;-&apos; &amp; CASESAFEID(Contact__r.Current_Rollover_Campaign__c)</formula>
        <name>Set New Progression Opportunity Index</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Progression_Opportunity_Index</fullName>
        <field>UniqueProgressionOpportunityIndex__c</field>
        <formula>CASESAFEID(Contact__c) &amp; &apos;-&apos; &amp; CASESAFEID(CampaignId)</formula>
        <name>Update Progression Opportunity Index</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>New Progression Opportunity</fullName>
        <actions>
            <name>Set_New_Progression_Opportunity_Index</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Progression</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity Update Application Submit Date</fullName>
        <actions>
            <name>Opportunity_Update_Submit_Time</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(ischanged(OAF_Submitted__c),OAF_Submitted__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Single Progression Opportunity</fullName>
        <actions>
            <name>Update_Progression_Opportunity_Index</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND (   RecordType.DeveloperName = &apos;Progression&apos;,   ISCHANGED(CampaignId),   NOT ISBLANK(CampaignId) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
