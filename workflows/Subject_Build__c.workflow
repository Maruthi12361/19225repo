<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Subject_Build_Final_Release_Date_Reminder</fullName>
        <description>Subject Build Final Release Date Reminder</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>salesforce.administrator@aib.edu.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>AIB_General_Templates/Subject_Build_Final_Release_Date_Reminder</template>
    </alerts>
</Workflow>
