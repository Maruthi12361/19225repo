<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Case_Due_Date_Reached</fullName>
        <description>Case Due Date Reached</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>salesforce.administrator@aib.edu.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>AIB_General_Templates/Case_Due_Notification</template>
    </alerts>
    <alerts>
        <fullName>Case_Follow_Up_Reminder</fullName>
        <description>Case Follow-Up Reminder</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>salesforce.administrator@aib.edu.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>AIB_General_Templates/Case_Follow_Up_Notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>Case_SetDepartmentToStudentCentral</fullName>
        <description>Sets the Case department__c field to Student Central and is used for the Email to Case setup of a new Email coming in.</description>
        <field>Department__c</field>
        <literalValue>Student Central</literalValue>
        <name>Case-Set Department To Student Central</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetWorkTypeToGeneral</fullName>
        <description>Sets the WorkType field to General and is used by the Case - Process Teaching Centre Support or Student Central Cases workflow rule triggered when either of those two Email-to-Case setups creates a new Student Support case.</description>
        <field>WorkType__c</field>
        <literalValue>General</literalValue>
        <name>Set WorkType to General</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetWorkTypeToOnlineExam</fullName>
        <description>DELETE 41 - Sets the WorkType field to Online Exam and is used by the Case - Process online exam cases workflow rule triggered when Email-to-Case Online Exam Support setup creates a new Online Exam Support Queue case.</description>
        <field>WorkType__c</field>
        <literalValue>Online Exam</literalValue>
        <name>DELETE 41 - Set WorkType to Online Exam</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetWorkTypeToTimetable</fullName>
        <description>Sets the WorkType field to Timetable and is used by the Case - Process ESS Team Cases workflow rule triggered when Email-to-Case Enrolment Support setup creates a new ESS Team case.</description>
        <field>WorkType__c</field>
        <literalValue>Timetable</literalValue>
        <name>Set WorkType to Timetable</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_New_Rollover_Case_Index</fullName>
        <field>Existing_Open_Rollover_Case__c</field>
        <formula>CASESAFEID(ContactId) &amp; CASESAFEID(Contact.Current_Rollover_Campaign__c) &amp; &apos;RolloverOpen&apos;</formula>
        <name>Set New Rollover Case Index</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Unique_Rollover_Case_Flag</fullName>
        <field>Existing_Open_Rollover_Case__c</field>
        <formula>CASESAFEID(ContactId) &amp; CASESAFEID(Rollover_Campaign__c)  &amp; &apos;RolloverOpen&apos;</formula>
        <name>Update Rollover Case Index</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Case - Process ESS Team Cases</fullName>
        <actions>
            <name>SetWorkTypeToTimetable</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>ESS Team</value>
        </criteriaItems>
        <description>If the Owner of a newly created Case is ESS Team queue (set by Email-to-Case for Enrolment Support, which also sets Record Type to Student Support) set the WorkType to Timetable.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case - Process Teaching Centre Support or Student Central Cases</fullName>
        <actions>
            <name>Case_SetDepartmentToStudentCentral</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>SetWorkTypeToGeneral</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>Teaching Centre Support</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>Student Central</value>
        </criteriaItems>
        <description>If the Owner of a newly created Case is Teaching Centre Support Queue or Student Central Queue (set by Email-to-Case for Teaching Centre Support or Student Central, which also sets Record Type to Student Support) set the WorkType to General for either.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case - Process online exam cases</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>Online Exam Support Queue</value>
        </criteriaItems>
        <description>DELETE 41 - If the Owner of a newly created Case is Online Exam Support Queue (set by Email-to-Case for Online Exam Support, which also sets Record Type to Student Support) set the WorkType to Online Exam.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>New Rollover Case</fullName>
        <actions>
            <name>Set_New_Rollover_Case_Index</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND (   RecordType.DeveloperName = &apos;Rollover&apos;,   OR   (     ISNEW(),     ISCHANGED(RecordTypeId)   ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Single Rollover Case</fullName>
        <actions>
            <name>Set_Unique_Rollover_Case_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND (   RecordType.DeveloperName = &apos;Rollover&apos;,   ISCHANGED(Rollover_Campaign__c),   NOT ISBLANK(Rollover_Campaign__c) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
