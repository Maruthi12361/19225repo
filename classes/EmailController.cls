@RestResource(urlMapping='/email/*')
global class EmailController 
{
    @HttpPost
    global static void verify() 
    {
        try
        {
            String reference = '';
            RestRequest req = RestContext.request;
            ViewModels.Param param = new ViewModels.Param();
            Map<String, Object> paramMap = new Map<String, Object>();

            if(req.headers.containsKey('Reference'))
                reference = req.headers.get('Reference');

            param.Identifier = reference;

            if (req.params.containsKey('type'))
                paramMap.put('type', RestContext.request.params.get('type'));

            if (req.params.containsKey('endpoint'))
                paramMap.put('endpoint', RestContext.request.params.get('endpoint'));

            if (req.params.containsKey('ip'))
                paramMap.put('ip', RestContext.request.params.get('ip'));

            param.payload = JSON.serialize(paramMap);

            ActionResult result = ContactService.requestVerifyEmail(param);
            RestContext.response.addHeader('Content-Type', 'application/json');

            if (result.Status == ResultStatus.SUCCESS)
            {
                RestContext.response.responseBody = Blob.valueOf(JSON.serialize('OK'));
                RestContext.response.statusCode = 200;
            }
            else 
            {
                Map<String, String> errorMap = new Map<String, String>
                {
                    'Message' => result.Message,
                    'Details' => ''
                };

                RestContext.response.responseBody = Blob.valueOf(JSON.serialize(errorMap));
                RestContext.response.statusCode = 400;
            }
        }
        catch(Exception ex)
        {
            Map<String, String> errorMap = new Map<String, String>
            {
                'Message' => ex.getMessage(),
                'Details' => ex.getStackTraceString() 
            };

            RestContext.response.addHeader('Content-Type', 'application/json');
            RestContext.response.responseBody = Blob.valueOf(JSON.serialize(errorMap));
            RestContext.response.statusCode = 400;
        }
    }
}