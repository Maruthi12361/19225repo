public class SubjectEnrolmentSendEmailCommand extends BaseQueueableCommand
{
    private static final String templateName = 'Workforce Management - Work Instructions';

    public SubjectEnrolmentSendEmailCommand()
    {
        super(1);
    }

    public override List<SObject> validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        EmailTemplate emailTemplate = EmailService.getEmailTemplate(templateName);
        List<hed__Course_Enrollment__c> enrolments = new List<hed__Course_Enrollment__c>();

        if (emailTemplate == null)
        {
            String message = String.format('No email template found for {0}', new List<String> { templateName });
            Log.error(this.className, 'validate', message);
            throw new Exceptions.ApplicationException(message);
        }

        for (hed__Course_Enrollment__c enrolment : (List<hed__Course_Enrollment__c>) items)
        {
            if (enrolment.hed__Contact__c == null)
            {
                Log.info(this.className, 'validate', 'Subject enrolment {0} missing contact', new List<String> { enrolment.Id });
                continue;
            }

            enrolments.add(enrolment);
        }

        return enrolments;
    }

    public override ActionResult command(List<SObject> items, Id jobId)
    {
        EmailTemplate emailTemplate = EmailService.getEmailTemplate(templateName);
        List<hed__Course_Enrollment__c> subjectEnrolments = getSubjectEnrolments(SObjectUtils.getIDs(items, 'ID'));
        List<hed__Course_Offering__c> subjectOfferings = getSubjectOfferings(SObjectUtils.getIDs(subjectEnrolments, 'hed__Course_Offering__c'));

        for (hed__Course_Enrollment__c enrolment : subjectEnrolments)
        {
            String body;
            String subject;
            Messaging.SingleEmailMessage mail;
            String link = Label.OAFBaseURL + Label.WorkInstructions_Link;
            hed__Course_Offering__c subjectOffering = getSubjectOffering(enrolment.hed__Course_Offering__c, subjectOfferings);

            if (enrolment.hed__Contact__r == null)
            {
                Log.info(this.className, 'command', 'Work instruction email not sent as subject enrolment ({0}) does not have contact',
                    new List<String> { enrolment.Id});
                continue;
            }
            else if (subjectOffering == null)
            {
                Log.info(this.className, 'command', 'Work instruction email not sent as subject offering ({0}) for subject enrolment was not found ({1})',
                    new List<String> { enrolment.hed__Course_Offering__c, enrolment.Id});
                continue;
            }

            mail = Messaging.renderStoredEmailTemplate(emailTemplate.Id, enrolment.hed__Contact__r.Id, null);
            body =  mail.getHtmlBody();
            subject = mail.getSubject();

            body = body.replace('{Contact.FirstName}', enrolment.hed__Contact__r.FirstName);
            body = body.replace('{SubjectOffering.StartDate}', subjectOffering.hed__Start_Date__c != null ? subjectOffering.hed__Start_Date__c.format() : '');
            body = body.replace('{SubjectOffering.EndDate}', subjectOffering.hed__End_Date__c != null ? subjectOffering.hed__End_Date__c.format() : '');

            subject = subject.replace('{Term.TermName}', subjectOffering.hed__Term__r.Name);
            subject = subject.replace('{SubjectVersion.SubjectName}', subjectOffering.hed__Course__r.Name);

            if (subjectOffering.hed__Term__r != null)
                body = body.replace('{Term.TermName}', subjectOffering.hed__Term__r.Name);

            if (subjectOffering.hed__Course__r != null)
                body = body.replace('{SubjectVersion.SubjectName}', subjectOffering.hed__Course__r.Name);

            link = link.replace('{SubjectEnrolment.Id}', enrolment.Id);
            link = link.replace('{Contact.EncryptedId}', enrolment.hed__Contact__r.EncryptedId__c);

            body = body.replace('{AvailableLink}', link.replace('{IsAvailable}', 'true'));
            body = body.replace('{NotAvailableLink}', link.replace('{IsAvailable}', 'false'));

            sendEmail(enrolment.hed__Contact__r.Email, enrolment.hed__Contact__r.Name, subject, body, Label.WorkInstructions_ReplyEmail, Label.WorkInstructions_ReplyName);
        }

        return new ActionResult(ResultStatus.SUCCESS);
    }

    @Future(callout=true)
    public static void sendEmail(String email, String name, String subject, String body, String replyEmail, String replyName)
    {
        ActionResult result = EmailService.sendToRecipient(Constants.APIPROVIDER_DEFAULT, email, name, subject, replyEmail, replyName, null, body, null);

        if (result.isError)
            Log.error('SubjectEnrolmentSendEmailCommand', 'sendEmail', 'Failed to send Work instructions email to {0}<{1}> due to: {2}.', new List<String>{ name, email, result.message });
        else
            Log.info('SubjectEnrolmentSendEmailCommand', 'sendEmail', 'Successfully sent Work instructions email to {0}<{1}>.', new List<String>{ name, email });
    }

    private List<hed__Course_Enrollment__c> getSubjectEnrolments(Set<ID> ids)
    {
        return
        [
            SELECT
                Id, hed__Course_Offering__c, hed__Contact__c, hed__Contact__r.Id, hed__Contact__r.Name, hed__Contact__r.FirstName, hed__Contact__r.Email,
                hed__Contact__r.EncryptedId__c
            FROM hed__Course_Enrollment__c
            WHERE Id IN :ids
        ];
    }

    private List<hed__Course_Offering__c> getSubjectOfferings(Set<Id> ids)
    {
        return
        [
            SELECT Id, Name, hed__Term__c, hed__Term__r.Name, hed__Course__r.Name, hed__Start_Date__c, hed__End_Date__c
            FROM hed__Course_Offering__c
            WHERE Id = :ids
        ];
    }

    private hed__Course_Offering__c getSubjectOffering(Id subjectOfferingId, List<hed__Course_Offering__c> subjectOfferings)
    {
        for (hed__Course_Offering__c subjectOffering : subjectOfferings)
            if (subjectOffering.Id == subjectOfferingId)
                return subjectOffering;

        return null;
    }
}