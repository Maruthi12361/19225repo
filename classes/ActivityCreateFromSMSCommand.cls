public class ActivityCreateFromSMSCommand extends BaseTriggerCommand
{
    public ActivityCreateFromSMSCommand()
    {
        super(5);
    }

    public override List<SObject> validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        List<SObject> filteredList = new List<SObject>();
        String objType;

        if (items[0] instanceof smagicinteract__Incoming_SMS__c)
            objType = 'InboundSMS';
        else if (items[0] instanceof smagicinteract__smsMagic__c)
            objType = 'OutboundSMS';

        for (SObject item : items)
        {
            if (triggerContext == Enums.TriggerContext.AFTER_INSERT)
            {
                if (objType == 'InboundSMS')
                {
                    smagicinteract__Incoming_SMS__c newValue = (smagicinteract__Incoming_SMS__c)item;
                    
                    if (newValue.smagicinteract__Mobile_Number__c != null)
                        filteredList.add(newValue);
                }
                else if (objType == 'OutboundSMS')
                {
                    smagicinteract__smsMagic__c newValue = (smagicinteract__smsMagic__c)item;
                    
                    if (newValue.smagicinteract__PhoneNumber__c != null)
                        filteredList.add(newValue); 
                }
            }
            else if (triggerContext == Enums.TriggerContext.AFTER_UPDATE)
            {
                if (objType == 'InboundSMS')
                {
                    smagicinteract__Incoming_SMS__c newValue = (smagicinteract__Incoming_SMS__c)item;
                    smagicinteract__Incoming_SMS__c oldValue = (smagicinteract__Incoming_SMS__c)oldMap.get(item.Id);
                    
                    if (newValue.smagicinteract__Contact__c != null && newValue.smagicinteract__Mobile_Number__c != null && 
                        (newValue.smagicinteract__Contact__c != oldValue.smagicinteract__Contact__c || newValue.smagicinteract__Mobile_Number__c != oldValue.smagicinteract__Mobile_Number__c))
                    {
                        filteredList.add(newValue);
                    }
                }
                else if (objType == 'OutboundSMS')
                {
                    smagicinteract__smsMagic__c newValue = (smagicinteract__smsMagic__c)item;
                    smagicinteract__smsMagic__c oldValue = (smagicinteract__smsMagic__c)oldMap.get(item.Id);

                    if (newValue.smagicinteract__Contact__c != null && newValue.smagicinteract__PhoneNumber__c != null && 
                        (newValue.smagicinteract__Contact__c != oldValue.smagicinteract__Contact__c || newValue.smagicinteract__PhoneNumber__c != oldValue.smagicinteract__PhoneNumber__c))
                    {
                        filteredList.add(newValue);
                    }
                }
            }
        }

        return filteredList;
    }

    public override ActionResult command(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        String objType;
        Set<Id> idSet;
        Map<Id, Task> taskMap = new Map<Id, Task>();
        Map<Id, User> userMap = new Map<Id, User>();
        List<Task> tasks;

        if (items[0] instanceof smagicinteract__Incoming_SMS__c)
            objType = 'InboundSMS';
        else if (items[0] instanceof smagicinteract__smsMagic__c)
            objType = 'OutboundSMS';

        idSet = new Map<Id, SObject>(items).keySet();

        tasks =
            [
                SELECT Id, WhoId, WhatId, OwnerId, Subject, Type
                FROM Task
                WHERE RecordType.DeveloperName = 'Client_Contact' AND Type = 'SMS' AND WhatId IN : idSet
            ];

        for (Task task : tasks)
        {
            taskMap.put(task.WhatId, task);
        }

        idSet = new Set<Id>();

        for (SObject item : items)
        {
            if (objType == 'InboundSMS')
                idSet.add(((smagicinteract__Incoming_SMS__c)item).OwnerId);
            else if (objType == 'OutboundSMS')
                idSet.add(((smagicinteract__smsMagic__c)item).OwnerId);
        }

        // Query the owners of SMS objects. UserService only returns the active users.
        userMap = new Map<Id, User>(UserService.get(idSet));

        for (SObject item : items)
        {
            Task smsTask = taskMap.get(item.Id);
            Boolean isDirty = false;

            if (objType == 'InboundSMS')
            {
                smagicinteract__Incoming_SMS__c newValue = (smagicinteract__Incoming_SMS__c)item;
                
                if (smsTask != null && smsTask.WhoId != newValue.smagicinteract__Contact__c)
                {
                    smsTask.WhoId = newValue.smagicinteract__Contact__c;
                    isDirty = true;
                }
                else
                    smsTask = createSMSTask(newValue, objType, userMap);
            }
            else if (objType == 'OutboundSMS')
            {
                smagicinteract__smsMagic__c newValue = (smagicinteract__smsMagic__c)item;
                
                if (smsTask != null && smsTask.WhoId != newValue.smagicinteract__Contact__c)
                {
                    smsTask.WhoId = newValue.smagicinteract__Contact__c;
                    isDirty = true;
                }
                else
                    smsTask = createSMSTask(newValue, objType, userMap);
            }

            if (smsTask != null)
            {
                if (smsTask.Id == null)
                {
                    uow.registerNew(smsTask);
                    Log.info(this.className, 'command', 'Creating new Task: ' + smsTask);
                }
                else if (isDirty)
                {
                    uow.registerDirty(smsTask);
                    Log.info(this.className, 'command', 'Updating existing Task: ' + smsTask);
                }
            }
        }

        return new ActionResult(ResultStatus.SUCCESS);
    }

    private Task createSMSTask(SObject sms, String smsType, Map<Id, User> userMap)
    {
        Task smsTask;
        smagicinteract__Incoming_SMS__c inSMS;
        smagicinteract__smsMagic__c outSMS;
        String subject;
        Id contactId;
        Id ownerId;

        if (smsType == 'InboundSMS')
        {
            inSMS = (smagicinteract__Incoming_SMS__c)sms;
            subject = 'Inbound SMS from ' + inSMS.smagicinteract__Mobile_Number__c;
            ownerId = inSMS.OwnerId;

            // If the owner of SMS object is inactive, the current user will be used as the new task owner.
            if (!userMap.containsKey(ownerId))
                ownerId = UserInfo.getUserId();

            smsTask = createSMSTask(inSMS.smagicinteract__Contact__c, inSMS.Id, ownerId, subject, inSMS.CreatedDate.date());
        }
        else if (smsType == 'OutboundSMS')
        {
            outSMS = (smagicinteract__smsMagic__c)sms;
            subject = 'Outbound SMS to ' + outSMS.smagicinteract__PhoneNumber__c;
            ownerId = outSMS.OwnerId;

            // If the owner of SMS object is inactive, the current user will be used as the new task owner.
            if (!userMap.containsKey(ownerId))
                ownerId = UserInfo.getUserId();

            smsTask = createSMSTask(outSMS.smagicinteract__Contact__c, outSMS.Id, ownerId, subject, outSMS.CreatedDate.date());
        }

        return smsTask;
    }

    private Task createSMSTask(Id whoId, Id whatId, Id ownerId, String subject, Date dueDate)
    {
        Task smsTask = new Task(Type = 'SMS', Status = 'Completed', ActivityDate = dueDate);
        smsTask.Subject = subject;
        smsTask.WhoId = whoId;
        smsTask.WhatId = whatId;
        smsTask.OwnerId = ownerId;
        smsTask.RecordTypeId = RecordTypeUtils.getRecordTypeId(Constants.RECORDTYPE_TASK_CLIENTCONTACT, SObjectType.Task);

        return smsTask;
    }
}