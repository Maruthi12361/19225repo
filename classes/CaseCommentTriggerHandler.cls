public class CaseCommentTriggerHandler extends BaseTriggerHandler
{
    public override void afterInsert()
    {
        ActionResult result;

        result = CaseService.notifyOfNewComment(Trigger.new, Trigger.oldMap, Enums.TriggerContext.AFTER_INSERT);
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);
    }
}