@isTest
public class SubjectServiceTests
{
    @testSetup 
    private static void testSetup()
    {
        TestUtilities.setupHEDA();
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Account account = TestDataAccountMock.createDepartmentBusiness(user);
        TestDataSubjectMock.createDefinitionLEAD(user);
    }

    @isTest
    static void NewSubjectDefinition_CreatesDefaultSubjectVersion()
    {
        // Arrange
        hed__Course__c subject =
            [
                SELECT Id,Name,RecordTypeId,hed__Course_ID__c,hed__Credit_Hours__c,hed__Account__c,hed__Description__c,
                    hed__Extended_Description__c,DisciplineCode__c,CerebroID__c,InternalNotes__c,SubjectYearLongIndicator__c,
                    TCSIUID__c,EFTSLValue__c,AQFLevel__c,EffectiveFromDate__c,EffectiveToDate__c
                FROM hed__Course__c
                WHERE Name = 'Leadership'
            ];

        // Act
        Test.startTest();
        ActionResult result = SubjectService.createVersion(new List<hed__Course__c> { subject }, new Map<Id, SObject>(), Enums.TriggerContext.AFTER_INSERT);
        Test.stopTest();

        hed__Course__c subjectVersion =
            [
                SELECT Id,Name,RecordType.DeveloperName,hed__Course_ID__c,hed__Credit_Hours__c,hed__Account__c,hed__Description__c,
                    hed__Extended_Description__c,DisciplineCode__c,CerebroID__c,InternalNotes__c,SubjectYearLongIndicator__c,
                    TCSIUID__c,VersionNumber__c,EFTSLValue__c,IsPrimary__c,AQFLevel__c,EffectiveFromDate__c,EffectiveToDate__c
                FROM hed__Course__c
                WHERE SubjectDefinition__c = : subject.Id
            ];

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.Status);
        System.assertEquals(Constants.RECORDTYPE_SUBJECT_VERSION, subjectVersion.RecordType.DeveloperName);
        System.assertEquals(subject.hed__Course_ID__c, subjectVersion.hed__Course_ID__c);
        System.assertEquals(subject.hed__Credit_Hours__c, subjectVersion.hed__Credit_Hours__c);
        System.assertEquals(subject.hed__Account__c, subjectVersion.hed__Account__c);
        System.assertEquals(subject.hed__Description__c, subjectVersion.hed__Description__c);
        System.assertEquals(subject.hed__Extended_Description__c, subjectVersion.hed__Extended_Description__c);
        System.assertEquals(subject.DisciplineCode__c, subjectVersion.DisciplineCode__c);
        System.assertEquals(subject.AQFLevel__c, subjectVersion.AQFLevel__c);
        System.assertEquals(subject.CerebroID__c, subjectVersion.CerebroID__c);
        System.assertEquals(subject.InternalNotes__c, subjectVersion.InternalNotes__c);
        System.assertEquals(subject.TCSIUID__c, subjectVersion.TCSIUID__c);
        System.assertEquals(1, subjectVersion.VersionNumber__c);
        System.assertEquals(subject.EFTSLValue__c, subjectVersion.EFTSLValue__c);
        System.assertEquals(subject.SubjectYearLongIndicator__c, subjectVersion.SubjectYearLongIndicator__c);
        System.assertEquals(subject.EffectiveFromDate__c, subjectVersion.EffectiveFromDate__c);
        System.assertEquals(subject.EffectiveToDate__c, subjectVersion.EffectiveToDate__c);
        System.assertEquals(True, subjectVersion.IsPrimary__c);
    }

    @isTest
    static void UpdateSubjectDefinition_CreatesDefaultSubjectVersionIfNoneExists()
    {
        // Arrange
        hed__Course__c subject =
            [
                SELECT Id,Name,RecordTypeId,hed__Course_ID__c,hed__Credit_Hours__c,hed__Account__c,hed__Description__c,
                    hed__Extended_Description__c,DisciplineCode__c,CerebroID__c,InternalNotes__c,SubjectYearLongIndicator__c,
                    TCSIUID__c,EFTSLValue__c,AQFLevel__c,EffectiveFromDate__c,EffectiveToDate__c
                FROM hed__Course__c
                WHERE Name = 'Leadership'
            ];
        Map<Id, hed__Course__c> oldMap = new Map<Id, hed__Course__c>();
        oldMap.put(subject.Id, subject);
        hed__Course__c newSubject = subject.clone(true, false, true, true);

        // Act
        Test.startTest();
        ActionResult result = SubjectService.createVersion(new List<hed__Course__c> { newSubject }, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        result = SubjectService.createVersion(new List<hed__Course__c> { subject }, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        Test.stopTest();

        List<hed__Course__c> subjectVersions =
            [
                SELECT Id,Name,RecordType.DeveloperName,hed__Course_ID__c,hed__Credit_Hours__c,hed__Account__c,hed__Description__c,
                    hed__Extended_Description__c,DisciplineCode__c,CerebroID__c,InternalNotes__c,SubjectYearLongIndicator__c,
                    TCSIUID__c,VersionNumber__c,EFTSLValue__c,IsPrimary__c,AQFLevel__c,EffectiveFromDate__c,EffectiveToDate__c
                FROM hed__Course__c
                WHERE SubjectDefinition__c = : subject.Id
            ];

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.Status);
        System.assertEquals(1, subjectVersions.size());
        System.assertEquals(Constants.RECORDTYPE_SUBJECT_VERSION, subjectVersions[0].RecordType.DeveloperName);
        System.assertEquals(subject.hed__Course_ID__c, subjectVersions[0].hed__Course_ID__c);
        System.assertEquals(subject.hed__Credit_Hours__c, subjectVersions[0].hed__Credit_Hours__c);
        System.assertEquals(subject.hed__Account__c, subjectVersions[0].hed__Account__c);
        System.assertEquals(subject.hed__Description__c, subjectVersions[0].hed__Description__c);
        System.assertEquals(subject.hed__Extended_Description__c, subjectVersions[0].hed__Extended_Description__c);
        System.assertEquals(subject.DisciplineCode__c, subjectVersions[0].DisciplineCode__c);
        System.assertEquals(subject.AQFLevel__c, subjectVersions[0].AQFLevel__c);
        System.assertEquals(subject.CerebroID__c, subjectVersions[0].CerebroID__c);
        System.assertEquals(subject.InternalNotes__c, subjectVersions[0].InternalNotes__c);
        System.assertEquals(subject.TCSIUID__c, subjectVersions[0].TCSIUID__c);
        System.assertEquals(1, subjectVersions[0].VersionNumber__c);
        System.assertEquals(subject.EFTSLValue__c, subjectVersions[0].EFTSLValue__c);
        System.assertEquals(subject.SubjectYearLongIndicator__c, subjectVersions[0].SubjectYearLongIndicator__c);
        System.assertEquals(subject.EffectiveFromDate__c, subjectVersions[0].EffectiveFromDate__c);
        System.assertEquals(subject.EffectiveToDate__c, subjectVersions[0].EffectiveToDate__c);
        System.assertEquals(True, subjectVersions[0].IsPrimary__c);
    }
    
    @isTest
    static void NewPrimarySubjectVersion_UnticksPrimaryForSiblingSubjectVersions()
    {
        // Arrange
        hed__Course__c subject =
            [
                SELECT Id,Name,RecordTypeId,hed__Course_ID__c,hed__Credit_Hours__c,hed__Account__c,hed__Description__c,
                    hed__Extended_Description__c,DisciplineCode__c,CerebroID__c,InternalNotes__c,SubjectYearLongIndicator__c,
                    TCSIUID__c,EFTSLValue__c,AQFLevel__c,EffectiveFromDate__c,EffectiveToDate__c
                FROM hed__Course__c
                WHERE Name = 'Leadership'
            ];
        SubjectService.createVersion(new List<hed__Course__c> { subject }, new Map<Id, SObject>(), Enums.TriggerContext.AFTER_INSERT);
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        hed__Course__c subjectVersion = TestDataSubjectMock.createVersionLEAD(user, true, 2);

        // Act
        Test.startTest();
        ActionResult result = SubjectService.updatePrimaryVersion(new List<hed__Course__c> { subjectVersion }, new Map<Id, SObject>(), Enums.TriggerContext.AFTER_INSERT);
        Test.stopTest();

        List<hed__Course__c> subjectVersions =
            [
                SELECT Id,IsPrimary__c,VersionNumber__c
                FROM hed__Course__c
                WHERE SubjectDefinition__c = : subject.Id AND RecordType.DeveloperName =: Constants.RECORDTYPE_SUBJECT_VERSION
                ORDER BY VersionNumber__c
            ];

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.Status);
        System.assertEquals(2, subjectVersions.size());
        System.assertEquals(1, subjectVersions[0].VersionNumber__c);
        System.assertEquals(false, subjectVersions[0].IsPrimary__c);
        System.assertEquals(subjectVersion.Id, subjectVersions[1].Id);
        System.assertEquals(2, subjectVersions[1].VersionNumber__c);
        System.assertEquals(true, subjectVersions[1].IsPrimary__c);
    }
    
    @isTest
    static void SetSubjectVersionAsPrimary_UnticksPrimaryForSiblingSubjectVersions()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        hed__Course__c subject =
            [
                SELECT Id,Name,RecordTypeId,hed__Course_ID__c,hed__Credit_Hours__c,hed__Account__c,hed__Description__c,
                    hed__Extended_Description__c,DisciplineCode__c,CerebroID__c,InternalNotes__c,SubjectYearLongIndicator__c,
                    TCSIUID__c,EFTSLValue__c,AQFLevel__c,EffectiveFromDate__c,EffectiveToDate__c
                FROM hed__Course__c
                WHERE Name = 'Leadership'
            ];

        SubjectService.createVersion(new List<hed__Course__c> { subject }, new Map<Id, SObject>(), Enums.TriggerContext.AFTER_INSERT);

        hed__Course__c subjectVersion = TestDataSubjectMock.createVersionLEAD(user, false, 2);

        Map<Id, hed__Course__c> oldMap = new Map<Id, hed__Course__c>();
        oldMap.put(subjectVersion.Id, subjectVersion);
        hed__Course__c newSubjectVersion = subjectVersion.clone(true, false, true, true);
        newSubjectVersion.IsPrimary__c = true;

        // Act
        Test.startTest();
        ActionResult result = SubjectService.updatePrimaryVersion(new List<hed__Course__c> { newSubjectVersion }, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        Test.stopTest();

        List<hed__Course__c> subjectVersions =
            [
                SELECT Id,IsPrimary__c,VersionNumber__c
                FROM hed__Course__c
                WHERE SubjectDefinition__c = : subject.Id AND RecordType.DeveloperName =: Constants.RECORDTYPE_SUBJECT_VERSION
                ORDER BY VersionNumber__c
            ];

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.Status);
        System.assertEquals(2, subjectVersions.size());
        System.assertEquals(1, subjectVersions[0].VersionNumber__c);
        System.assertEquals(false, subjectVersions[0].IsPrimary__c);
        System.assertEquals(newSubjectVersion.Id, subjectVersions[1].Id);
    }
    
    @isTest
    static void LinkPrimarySubjectVersionWithSubjectDefinition_UnticksPrimaryForSiblingSubjectVersions()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        hed__Course__c subject =
            [
                SELECT Id,Name,RecordTypeId,hed__Course_ID__c,hed__Credit_Hours__c,hed__Account__c,hed__Description__c,
                    hed__Extended_Description__c,DisciplineCode__c,CerebroID__c,InternalNotes__c,SubjectYearLongIndicator__c,
                    TCSIUID__c,EFTSLValue__c,AQFLevel__c,EffectiveFromDate__c,EffectiveToDate__c
                FROM hed__Course__c
                WHERE Name = 'Leadership'
            ];

        SubjectService.createVersion(new List<hed__Course__c> { subject }, new Map<Id, SObject>(), Enums.TriggerContext.AFTER_INSERT);
        hed__Course__c subjectVersion = TestDataSubjectMock.createVersionMMGT(user, true, 2);
        
        Map<Id, hed__Course__c> oldMap = new Map<Id, hed__Course__c>();
        oldMap.put(subjectVersion.Id, subjectVersion);
        hed__Course__c newSubjectVersion = subjectVersion.clone(true, false, true, true);
        newSubjectVersion.SubjectDefinition__c = subject.Id;

        // Act
        Test.startTest();
        ActionResult result = SubjectService.updatePrimaryVersion(new List<hed__Course__c> { newSubjectVersion }, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        Test.stopTest();

        List<hed__Course__c> subjectVersions =
            [
                SELECT Id,IsPrimary__c,VersionNumber__c
                FROM hed__Course__c
                WHERE SubjectDefinition__c = : subject.Id AND RecordType.DeveloperName =: Constants.RECORDTYPE_SUBJECT_VERSION
                ORDER BY VersionNumber__c
            ];

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.Status);
        System.assertEquals(1, subjectVersions.size());
        System.assertEquals(1, subjectVersions[0].VersionNumber__c);
        System.assertEquals(false, subjectVersions[0].IsPrimary__c);
    }
}