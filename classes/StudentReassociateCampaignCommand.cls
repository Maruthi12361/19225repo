public class StudentReassociateCampaignCommand extends BaseTriggerCommand
{
    private static List<Campaign> allCampaignList;

    public override List<SObject> validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        List<Contact> contactList = new List<Contact>();

        if (triggerContext == Enums.TriggerContext.AFTER_UPDATE)
        {
            for (Contact newContact : (List<Contact>) items)
            {
                Contact oldContact = (Contact) oldMap.get(newContact.Id);

                if (oldContact.Rollover_Followup_Date__c != newContact.Rollover_Followup_Date__c && newContact.Current_Course_Status__c == RolloverConstantsDefinition.CS_ONGOING)
                {
                    contactList.add(newContact);
                }
            }
        }

        return contactList;
    }

    public override ActionResult command(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        Map<Id, Contact> contactMap = new Map<Id, Contact>((List<Contact>)items);

        List<Contact> contactList =
            [
                SELECT Id,Current_Rollover_Campaign__c,Rollover_Status__c,Rollover_Call_Count__c,Rollover_Email_Count__c,Rollover_Contact_History__c,
                    Rollover_Lost_Count__c,Rollover_Deferred_Reason__c,Rollover_Deferred_Reason_Other__c,Rollover_Count__c,
                (
                    SELECT Id,Status,CampaignId,Campaign.Type,Campaign.StartDate,Campaign.Status
                    FROM CampaignMembers
                    WHERE Campaign.Type = 'Rollover'
                    ORDER BY CreatedDate desc
                ),
                (
                    SELECT Id, ContactId, Rollover_Campaign__c
                    FROM Cases
                    WHERE RecordType.DeveloperName =: Constants.RECORDTYPE_CASE_ROLLOVER AND Status NOT IN ('Closed')
                )
                FROM Contact
                WHERE Id IN : contactMap.keySet()
            ];

        List<CampaignMember> updatedCampaignMemberList = new List<CampaignMember>();
        List<CampaignMember> newCampaignMemberList = new List<CampaignMember>();
        List<CampaignMember> deletedCampaignMemberList = new List<CampaignMember>();
        List<Contact> updatedContactList = new List<Contact>();

        for (Contact fullContact : contactList)
        {
            Boolean isDirty = false;
            Contact newContact = contactMap.get(fullContact.Id);
            Date lastSubjectStartDate;

            Log.info(this.className, 'command', 'Current CampaignId: ' + newContact.Current_Rollover_Campaign__c);

            CampaignMember currentCampaignMember;
            CampaignMember newCampaignMember;

            for (CampaignMember campaignMember : fullContact.CampaignMembers)
            {
                if (newContact.Current_Rollover_Campaign__c == campaignMember.CampaignId)
                {
                    currentCampaignMember = campaignMember;
                    Log.info(this.className, 'command', 'Found current CampaignMember: ' + currentCampaignMember);
                }
            }

            if (newContact.Rollover_Followup_Date__c != null)
            {
                lastSubjectStartDate = newContact.Next_Followup_Date__c.addDays(Integer.valueOf(Label.RolloverStartDateOffset));
                newCampaignMember = createNewCampaignMember(fullContact.Id, newContact.Student_Type__c, newContact.Rollover_Followup_Date__c, lastSubjectStartDate);
            }

            if (currentCampaignMember != null && (newCampaignMember == null || newCampaignMember.CampaignId != currentCampaignMember.CampaignId))
            {
                if (currentCampaignMember.Status == RolloverConstantsDefinition.ST_OPEN && (currentCampaignMember.Campaign.StartDate > Date.today() ||
                  newContact.Next_Followup_Date__c == newContact.Rollover_Followup_Date__c))
                {
                    Log.info(this.className, 'command', 'Delete the current CM if it has not started: ' + currentCampaignMember);
                    deletedCampaignMemberList.add(currentCampaignMember);

                    if (fullContact.Rollover_Count__c != null && fullContact.Rollover_Count__c > 0)
                    {
                        fullContact.Rollover_Count__c--;
                        isDirty = true;
                    }
                }
                else if (currentCampaignMember.Status == RolloverConstantsDefinition.ST_OPEN ||
                        currentCampaignMember.Status == RolloverConstantsDefinition.ST_MASSEMAIL ||
                        currentCampaignMember.Status == RolloverConstantsDefinition.ST_CONTACTING)
                {
                    currentCampaignMember.Status = RolloverConstantsDefinition.ST_CLOSED_LOST;

                    if (fullContact.Rollover_Lost_Count__c == null)
                        fullContact.Rollover_Lost_Count__c = 1;
                    else
                        fullContact.Rollover_Lost_Count__c++;

                    isDirty = true;
                    updatedCampaignMemberList.add(currentCampaignMember);
                    Log.info(this.className, 'command', 'Set the current CampaignMember to Closed Lost if it is active but no case created: ' + currentCampaignMember);
                }
            }

            if (newCampaignMember != null)
            {
                for (CampaignMember campaignMember : fullContact.CampaignMembers)
                {
                    if (campaignMember.CampaignId == newCampaignMember.CampaignId)
                    {
                        newCampaignMember = campaignMember;
                        Log.info(this.className, 'command', 'Found existing CampaignMember: ' + newCampaignMember);
                    }
                }

                if (fullContact.Current_Rollover_Campaign__c != newCampaignMember.CampaignId)
                {
                    fullContact.Current_Rollover_Campaign__c = newCampaignMember.CampaignId;
                    fullContact.Rollover_Call_Count__c = 0;
                    fullContact.Rollover_Email_Count__c = 0;
                    fullContact.Rollover_Contact_History__c = null;

                    if (newCampaignMember.Campaign.StartDate > Date.today())
                    {
                        fullContact.Rollover_Status__c = RolloverConstantsDefinition.RO_DEFERRED;
                        fullContact.Rollover_Deferred_Reason__c = newContact.Rollover_Deferred_Reason__c;
                        
                        if (newContact.Next_Followup_Date__c != null && lastSubjectStartDate > Date.today() && 
                           (newContact.Next_Followup_Date__c >= newContact.Manual_Followup_Date__c || newContact.Manual_Followup_Date__c == null))
                        {
                            fullContact.Rollover_Deferred_Reason__c = RolloverConstantsDefinition.RO_DF_ACTIVEENT;
                        }
                        else
                        {
                            if (fullContact.Rollover_Deferred_Reason__c == null || fullContact.Rollover_Deferred_Reason__c == RolloverConstantsDefinition.RO_DF_NOTDEF ||
                               fullContact.Rollover_Deferred_Reason__c == RolloverConstantsDefinition.RO_DF_WITHDRAW || 
                               fullContact.Rollover_Deferred_Reason__c == RolloverConstantsDefinition.RO_DF_ACTIVEENT)
                            {
                                fullContact.Rollover_Deferred_Reason__c = RolloverConstantsDefinition.RO_DF_SCDEFERRED;
                            }
                        }
                        
                        Log.info(this.className, 'command', 'Determine the deferred reason: ' + fullContact.Rollover_Deferred_Reason__c); 
                    }
                    else
                    {
                        fullContact.Rollover_Status__c = RolloverConstantsDefinition.ST_OPEN;
                        fullContact.Rollover_Deferred_Reason__c = null;
                        fullContact.Rollover_Deferred_Reason_Other__c = null;
                    }

                    isDirty = true;
                    Log.info(this.className, 'command', 'Replace the current CampaignMember with new one and reset Rollover status: ' + newCampaignMember);
                }
                else
                {
                    if (fullContact.Rollover_Status__c == RolloverConstantsDefinition.RO_DEFERRED)
                    {
                        if (newCampaignMember.Campaign.StartDate <= Date.today())
                        {
                            fullContact.Rollover_Status__c = RolloverConstantsDefinition.ST_OPEN;
                            fullContact.Rollover_Deferred_Reason__c = null;
                            fullContact.Rollover_Deferred_Reason_Other__c = null;
                            isDirty = true;
                            Log.info(this.className, 'command', 'Set Contact Rollover Status to Open when Campaign becomes In Progress');
                        }
                    }
                    else if (fullContact.Rollover_Status__c == RolloverConstantsDefinition.ST_OPEN)
                    {
                        // This may only happen when Campaign StatDate is changed.
                        if (newCampaignMember.Campaign.StartDate > Date.today())
                        {
                            fullContact.Rollover_Status__c = RolloverConstantsDefinition.RO_DEFERRED;

                            if (fullContact.Rollover_Deferred_Reason__c == null)
                            {
                                fullContact.Rollover_Deferred_Reason__c = RolloverConstantsDefinition.RO_DF_SCDEFERRED;

                                if (newContact.Next_Followup_Date__c != null && lastSubjectStartDate > Date.today() && 
                                   (newContact.Next_Followup_Date__c >= newContact.Manual_Followup_Date__c || newContact.Manual_Followup_Date__c == null))
                                {
                                    fullContact.Rollover_Deferred_Reason__c = RolloverConstantsDefinition.RO_DF_ACTIVEENT;
                                }
                            }

                            isDirty = true;
                            Log.info(this.className, 'command', 'Set Contact Rollover Status to Deferred when Campaign becomes Planned');
                        }
                    }
                }

                for (Case rolloverCase : fullContact.Cases)
                {
                    if (rolloverCase.Rollover_Campaign__c == newCampaignMember.CampaignId)
                    {
                        fullContact.Rollover_Status__c = RolloverConstantsDefinition.ST_ENROLLING;
                        fullContact.Rollover_Deferred_Reason__c = null;
                        newCampaignMember.Status = RolloverConstantsDefinition.ST_ENROLLING;

                        if (newCampaignMember.Id != null)
                            updatedCampaignMemberList.add(newCampaignMember);

                        isDirty = true;
                        Log.info(this.className, 'command', 'Found existing rollover case, set RolloverStatus to Enrolling');
                        break;
                    }
                }
            }
            else
            {
                if (newContact.Rollover_Followup_Date__c != null)
                {
                    if (newContact.Rollover_Followup_Date__c > Date.today())
                    {
                        fullContact.Rollover_Status__c = RolloverConstantsDefinition.RO_DEFERRED;

                        if (fullContact.Rollover_Deferred_Reason__c == null || fullContact.Rollover_Deferred_Reason__c == RolloverConstantsDefinition.RO_DF_NOTDEF ||
                           newContact.Rollover_Followup_Date__c == newContact.Next_Followup_Date__c)
                        {
                            fullContact.Rollover_Deferred_Reason__c = RolloverConstantsDefinition.RO_DF_TBD;
                        }
                    }
                    else
                    {
                        fullContact.Rollover_Status__c = null;
                        fullContact.Rollover_Deferred_Reason__c = null;
                    }
                }
                else
                {
                    if (newContact.Rollover_Status__c == RolloverConstantsDefinition.RO_NOENROLMENTS)
                        fullContact.Rollover_Deferred_Reason__c = RolloverConstantsDefinition.RO_DF_WITHDRAW;
                }

                fullContact.Current_Rollover_Campaign__c = null;
                fullContact.Rollover_Call_Count__c = 0;
                fullContact.Rollover_Email_Count__c = 0;
                fullContact.Rollover_Contact_History__c = null;
                Log.info(this.className, 'command', 'No future campaign available for the contact');
                isDirty = true;
            }

            if (newCampaignMember != null && newCampaignMember.Id == null)
            {
                Log.info(this.className, 'command', 'Add new CampaignMember: ' + newCampaignMember);
                newCampaignMemberList.add(newCampaignMember);

                if (fullContact.Rollover_Count__c == null)
                    fullContact.Rollover_Count__c = 1;
                else
                    fullContact.Rollover_Count__c++;
            }

            if (isDirty) 
            {
                updatedContactList.add(fullContact);
            }
        }

        if (updatedCampaignMemberList.size() > 0)
            uow.registerDirty(updatedCampaignMemberList);

        if (newCampaignMemberList.size() > 0)
            uow.registerNew(newCampaignMemberList);

        if (deletedCampaignMemberList.size() > 0)
            uow.registerDeleted(deletedCampaignMemberList);

        if (updatedContactList.size() > 0)
            uow.registerDirty(updatedContactList);

        return new ActionResult(ResultStatus.SUCCESS);
    }

    private CampaignMember createNewCampaignMember(Id contactId, String studentType, Date rolloverFollowupDate, Date lastSubjectStartDate)
    {
        CampaignMember newCampaignMember;

        // The default student type is DMBA
        String portfolio = 'Domestic';
        String programme = 'MBA';

        if (studentType == 'OLI')
        {
            portfolio = 'International';
        }

        Campaign assignedCampaign;
        Campaign prevCampaign;
        Campaign lastCampaign;
        
        if (allCampaignList == null)
        {
            allCampaignList = 
                [
                    SELECT Id, Name, StartDate, EndDate, TermStartDate__c, Portfolio__c, Programme__c, Status
                    FROM Campaign
                    WHERE Type = 'Rollover' AND Status IN : Constants.CAMPAIGN_ACTIVE_STATUS
                    ORDER BY StartDate DESC
                ];
        }

        for (Campaign campaign : allCampaignList)
        {
            if (campaign.Portfolio__c == portfolio && campaign.Programme__c == programme)
            {
                if (rolloverFollowupDate >= campaign.StartDate && assignedCampaign == null)
                {
                    if (lastSubjectStartDate == campaign.TermStartDate__c)
                        assignedCampaign = prevCampaign;
                    else
                        assignedCampaign = campaign;
                }

                if (lastCampaign == null)
                    lastCampaign = campaign;
                else
                {
                    if (campaign.EndDate > lastCampaign.EndDate)
                        lastCampaign = campaign;
                }

                prevCampaign = campaign;
            }
        }

        // Create campaign member
        if (assignedCampaign != null && lastCampaign != null && rolloverFollowupDate < lastCampaign.EndDate)
        {
            newCampaignMember = New CampaignMember();
            newCampaignMember.CampaignId = assignedCampaign.Id;
            newCampaignMember.Campaign = assignedCampaign;
            newCampaignMember.ContactId = ContactId;
            newCampaignMember.Status = RolloverConstantsDefinition.ST_OPEN;
            Log.info(this.className, 'createNewCampaignMember', 'Match RolloverFollowupDate: ' + rolloverFollowupDate + ' to New Campaign: ' + assignedCampaign);
        }

        return newCampaignMember;
    }
}