public class ContactTriggerHandler extends BaseTriggerHandler
{
    public override void beforeInsert()
    {
        ActionResult result;

        result = ContactService.encryptData(Trigger.new, Trigger.oldMap, Enums.TriggerContext.BEFORE_INSERT);
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);

        result = ContactService.formatPhoneNumbers(Trigger.new, Trigger.oldMap, Enums.TriggerContext.BEFORE_INSERT);
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);

        result = ContactService.updateStatus(Trigger.new, Trigger.oldMap, Enums.TriggerContext.BEFORE_INSERT);
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);

        result = ContactService.setCountryFieldsValue(Trigger.new, Trigger.oldMap, Enums.TriggerContext.BEFORE_INSERT);
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);

        result = ContactService.updateOwnerFromAutoProcToCA(Trigger.new, Trigger.oldMap, Enums.TriggerContext.BEFORE_INSERT);
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);
    }

    public override void afterInsert()
    {
        ActionResult result;

        result = ContactService.validateAccount(Trigger.new, Trigger.oldMap, Enums.TriggerContext.AFTER_INSERT);
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);
        
        result = ContactService.sendWelcomeEmail(Trigger.new, Trigger.oldMap, Enums.TriggerContext.AFTER_INSERT);
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);
        
        result = ContactService.sendWelcomeSMS(Trigger.new, Trigger.oldMap, Enums.TriggerContext.AFTER_INSERT);
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);

        result = ContactService.reassociateWarmCampaign(Trigger.new, Trigger.oldMap, Enums.TriggerContext.AFTER_INSERT);
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);

        result = ContactService.reassociateAcquisitionCampaign(Trigger.new, Trigger.oldMap, Enums.TriggerContext.AFTER_INSERT);
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);

        result = ContactService.trackStatusHistory(Trigger.new, Trigger.oldMap, Enums.TriggerContext.AFTER_INSERT);
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);

        result = ContactService.createOpportunity(Trigger.new, Trigger.oldMap, Enums.TriggerContext.AFTER_INSERT);
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);
    }

    public override void beforeUpdate()
    {
        ActionResult result;

        result = ContactService.formatPhoneNumbers(Trigger.new, Trigger.oldMap, Enums.TriggerContext.BEFORE_UPDATE);
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);

        result = ContactService.updateDoNotCall(Trigger.new, Trigger.oldMap, Enums.TriggerContext.BEFORE_UPDATE);
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);

        result = ContactService.updateStatus(Trigger.new, Trigger.oldMap, Enums.TriggerContext.BEFORE_UPDATE);
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message); 

        result = ContactService.setCountryFieldsValue(Trigger.new, Trigger.oldMap, Enums.TriggerContext.BEFORE_UPDATE);
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);

        result = ContactService.setFinalCourseConsultant(Trigger.new, Trigger.oldMap, Enums.TriggerContext.BEFORE_UPDATE);
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);
    }

    public override void beforeDelete()
    {
        ActionResult result;

        result = ContactService.validateDeletion(Trigger.old, Trigger.oldMap, Enums.TriggerContext.BEFORE_DELETE);
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);
    }

    public override void afterUpdate()
    {
        ActionResult result;

        result = ContactService.validateAccount(Trigger.new, Trigger.oldMap, Enums.TriggerContext.AFTER_UPDATE);
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);

        result = ContactService.sendWelcomeEmail(Trigger.new, Trigger.oldMap, Enums.TriggerContext.AFTER_UPDATE);
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);

        result = ContactService.sendWelcomeSMS(Trigger.new, Trigger.oldMap, Enums.TriggerContext.AFTER_UPDATE);
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);

        result = ContactService.reassociateWarmCampaign(Trigger.new, Trigger.oldMap, Enums.TriggerContext.AFTER_UPDATE);
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);

        result = ContactService.reassociateAcquisitionCampaign(Trigger.new, Trigger.oldMap, Enums.TriggerContext.AFTER_UPDATE);
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);

        result = ContactService.reassociateRolloverCampaign(Trigger.new, Trigger.oldMap, Enums.TriggerContext.AFTER_UPDATE);
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);

        result = ContactService.syncRolloverStatusToCampaign(Trigger.new, Trigger.oldMap, Enums.TriggerContext.AFTER_UPDATE);
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);

        result = ContactService.trackStatusHistory(Trigger.new, Trigger.oldMap, Enums.TriggerContext.AFTER_UPDATE);
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);

        result = CerebroService.syncApplication(Trigger.new, Trigger.oldMap, Enums.TriggerContext.AFTER_UPDATE);
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);

        result = ContactService.identifyDuplicates(Trigger.new, Trigger.oldMap, Enums.TriggerContext.AFTER_UPDATE); 
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);

        result = ContactService.createOpportunity(Trigger.new, Trigger.oldMap, Enums.TriggerContext.AFTER_UPDATE);
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);
        
        result = ContactService.updateOpportunity(Trigger.new, Trigger.oldMap, Enums.TriggerContext.AFTER_UPDATE);
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);

        result = ContactService.reenableSSO(Trigger.new, Trigger.oldMap, Enums.TriggerContext.AFTER_UPDATE);
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);

        result = ContactService.saveMilestone(Trigger.new, Trigger.oldMap, Enums.TriggerContext.AFTER_UPDATE);
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);

        result = ContactService.updateUserOnChange(Trigger.new, Trigger.oldMap, Enums.TriggerContext.AFTER_UPDATE);
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);

        result = SObjectService.sendConfirmationEmail(Trigger.new, Trigger.oldMap, Enums.TriggerContext.AFTER_UPDATE);
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);
    }
}