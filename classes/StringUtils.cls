public class StringUtils
{
    public static String getExceptionMessage(Exception ex)
    {
        return ex.getMessage() + '\n' +
            'Line number: ' + ex.getLineNumber() + '\n' +
            'Stack trace: ' + ex.getStackTraceString();
    }

    public static String concat(String stringOne, String stringTwo, String separator)
    {
        String result = stringOne;

        if (String.isBlank(result))
        {
            result = stringTwo;
        }
        else
        {
            result = result.trim();
            if (String.isNotBlank(stringTwo))
            {
                result = result + separator + stringTwo;
            }
        }

        if (String.isNotBlank(result))
        {
            result = result.trim();
        }

        return result;
    }

    public static Boolean equal(String stringOne, String stringTwo)
    {
        if (String.isBlank(stringOne))
        {
            return String.isBlank(stringTwo);
        }
        else
        {
            if (String.isBlank(stringTwo))
                return false;
            else
            {
                return stringOne.trim().equals(stringTwo.trim());
            }
        }
    }

    public static Boolean equalIgnoreCase(String stringOne, String stringTwo)
    {
        if (String.isBlank(stringOne))
        {
            return String.isBlank(stringTwo);
        }
        else
        {
            if (String.isBlank(stringTwo))
                return false;
            else
            {
                return stringOne.trim().equalsIgnoreCase(stringTwo.trim());
            }
        }
    }

    public static Boolean isDifferentPhoneNumber(String source, String target)
    {
        if (String.isBlank(source))
        {
            return !String.isBlank(target);
        }
        else
        {
            if (String.isBlank(target))
                return true;
            else
            {
                return !normalisePhoneNumber(target).endsWith(normalisePhoneNumber(source));
            }
        }
    }

    public static String normalisePhoneNumber(String phoneNumber)
    {
        String result = phoneNumber;

        if (String.isNotBlank(result))
        {
            result = result.trim().replaceAll('[^0-9]', ' ').deleteWhitespace();
            if (result.startsWith('0'))
            {
                result = result.removeStart('0');
            }

            if (String.isNotBlank(result) && result.length() > 40)
                result = result.left(40);
        }

        return result;
    }

    public static Boolean isValidEmail(String email)
    {
        if (String.isBlank(email))
            return false;

        String emailString = email.trim();

        if (emailString.length() > 80)
            return false;

        String emailRegEx = '^([a-zA-Z0-9\\\'_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,20}|[0-9]{1,3})(\\]?)$';
        Pattern emailPattern = Pattern.compile(emailRegEx);
        Matcher emailMatcher = emailPattern.matcher(emailString);

        return emailMatcher.matches();
    }

    public static String copyLeft(String stringParam, Integer length)
    {
        if (String.isBlank(stringParam) || stringParam.length() <= length)
            return stringParam;
        else
            return stringParam.left(length);
    }

    public static String formatDate(Date dateValue)
    {
        if (dateValue == null)
            return null;

        Integer year = dateValue.year();
        Integer month = dateValue.month();
        Integer day = dateValue.day();

        return (day < 10 ? '0' : '') + String.valueOf(day) + '/' + (month < 10 ? '0' : '') + String.valueOf(month) + '/' + String.valueOf(year);
    }

    public static Boolean isLogicalStatementTrue(String operator, String leftOperand, String rightOperand)
    {
        if (operator == String.valueOf(Enums.Operator.EQUAL))
        {
            if (equalIgnoreCase(leftOperand, rightOperand))
                return true;

            return false;
        }
        else if (operator == String.valueOf(Enums.Operator.NOT_EQUAL))
        {
            if (!equalIgnoreCase(leftOperand, rightOperand))
                return true;

            return false;
        }
        else if (operator == String.valueOf(Enums.Operator.LESS_THAN))
        {
            if (!isNumeric(leftOperand))
            {
                Log.info('StringUtils', 'isLogicalStatementTrue', 'Operator.LESS_THAN: Left operand is not a valid number: {0}', new List<String>{ leftOperand });
                return false;
            }

            if (!isNumeric(rightOperand))
            {
                Log.info('StringUtils', 'isLogicalStatementTrue', 'Operator.LESS_THAN: Right operand is not a valid number: {0}', new List<String>{ rightOperand });
                return false;
            }

            if (Decimal.valueOf(leftOperand) < Decimal.valueOf(rightOperand))
                return true;

            return false;
        }
        else if (operator == String.valueOf(Enums.Operator.LESS_THAN_OR_EQUAL))
        {
            if (!isNumeric(leftOperand))
            {
                Log.info('StringUtils', 'isLogicalStatementTrue', 'Operator.LESS_THAN_OR_EQUAL: Left operand is not a valid number: {0}', new List<String>{ leftOperand });
                return false;
            }

            if (!isNumeric(rightOperand))
            {
                Log.info('StringUtils', 'isLogicalStatementTrue', 'Operator.LESS_THAN_OR_EQUAL: Right operand is not a valid number: {0}', new List<String>{ rightOperand });
                return false;
            }

            if (Decimal.valueOf(leftOperand) <= Decimal.valueOf(rightOperand))
                return true;

            return false;
        }
        else if (operator == String.valueOf(Enums.Operator.GREATER_THAN))
        {
            if (!isNumeric(leftOperand))
            {
                Log.info('StringUtils', 'isLogicalStatementTrue', 'Operator.GREATER_THAN: Left operand is not a valid number: {0}', new List<String>{ leftOperand });
                return false;
            }

            if (!isNumeric(rightOperand))
            {
                Log.info('StringUtils', 'isLogicalStatementTrue', 'Operator.GREATER_THAN: Right operand is not a valid number: {0}', new List<String>{ rightOperand });
                return false;
            }

            if (Decimal.valueOf(leftOperand) > Decimal.valueOf(rightOperand))
                return true;

            return false;
        }
        else if (operator == String.valueOf(Enums.Operator.GREATER_THAN_OR_EQUAL))
        {
            if (!isNumeric(leftOperand))
            {
                Log.info('StringUtils', 'isLogicalStatementTrue', 'Operator.GREATER_THAN_OR_EQUAL: Left operand is not a valid number: {0}', new List<String>{ leftOperand });
                return false;
            }

            if (!isNumeric(rightOperand))
            {
                Log.info('StringUtils', 'isLogicalStatementTrue', 'Operator.GREATER_THAN_OR_EQUAL: Right operand is not a valid number: {0}', new List<String>{ rightOperand });
                return false;
            }

            if (Decimal.valueOf(leftOperand) >= Decimal.valueOf(rightOperand))
                return true;

            return false;
        }
        else if (operator == String.valueOf(Enums.Operator.CONTAINS))
        {
            if (rightOperand == null || String.isEmpty(rightOperand.trim()))
            {
                Log.info('StringUtils', 'isLogicalStatementTrue', 'Operator.CONTAINS: Right operand value cannot be null or blank');
                return false;
            }

            for (String value : rightOperand.split(','))
            {
                if (leftOperand != null && leftOperand.trim().containsIgnoreCase(value.trim()))
                    return true;
            }

            return false;
        }
        else if(operator == String.valueOf(Enums.Operator.NOT_CONTAINS))
        {
            if (rightOperand == null || String.isEmpty(rightOperand.trim()))
            {
                Log.info('StringUtils', 'isLogicalStatementTrue', 'Operator.NOT_CONTAINS: Right operand value cannot be null or blank');
                return true;
            }

            for (String value : rightOperand.split(','))
            {
                if (leftOperand != null && leftOperand.trim().containsIgnoreCase(value.trim()))
                    return false;
            }

            return true;
        }
        else if (operator == String.valueOf(Enums.Operator.EMPTY))
        {
            if (String.isEmpty(leftOperand))
                return true;

            return false;
        }
        else if (operator == String.valueOf(Enums.Operator.NOT_EMPTY))
        {
            if (!String.isEmpty(leftOperand))
                return true;

            return false;
        }
        else if (operator == String.valueOf(Enums.Operator.ANY_VALUE_IN))
        {
            if (rightOperand == null || String.isEmpty(rightOperand.trim()))
            {
                Log.info('StringUtils', 'isLogicalStatementTrue', 'Operator.ANY_VALUE_IN: Right operand value cannot be null or blank');
                return false;
            }

            Set<String> values = new Set<String>(rightOperand.replaceAll(' ', '').toLowerCase().split(','));

            if (leftOperand != null && values.contains(leftOperand.toLowerCase().trim()))
                return true;

            return false;
        }
        else if (operator == String.valueOf(Enums.Operator.RANGE))
        {
            if (!isNumeric(leftOperand))
            {
                Log.info('StringUtils', 'isLogicalStatementTrue', 'Operator.RANGE: Left operand is not a valid number: {0}', new List<String>{ leftOperand });
                return false;
            }

            if (rightOperand == null || String.isEmpty(rightOperand.trim()))
            {
                Log.info('StringUtils', 'isLogicalStatementTrue', 'Operator.RANGE: Right operand value cannot be null or blank');
                return false;
            }

            List<String> values = rightOperand.replaceAll(' ', '').toLowerCase().split(',');

            if (values.size() != 2)
            {
                Log.info('StringUtils', 'isLogicalStatementTrue', 'Operator.RANGE: Unexpected number in range values - {0}', new List<String>{ String.join(values, ',') });
                return false;
            }

            if (!isNumeric(values[0]))
            {
                Log.info('StringUtils', 'isLogicalStatementTrue', 'Operator.RANGE: Min range value is not a valid number: ' + values[0]);
                return false;
            }

            if (!isNumeric(values[1]))
            {
                Log.info('StringUtils', 'isLogicalStatementTrue', 'Operator.RANGE: Max range is not a valid number: ' + values[1]);
                return false;
            }

            if (Decimal.valueOf(leftOperand) >= Decimal.valueOf(values[0]) && Decimal.valueOf(leftOperand) <= Decimal.valueOf(values[1]))
                return true;

            return false;
        }
        else if (operator == String.valueOf(Enums.Operator.STARTS_WITH))
        {
            if (leftOperand == null || String.isEmpty(leftOperand.trim()))
            {
                Log.info('StringUtils', 'isLogicalStatementTrue', 'Operator.STARTS_WITH: Left operand value cannot be null or blank');
                return false;
            }

            if (rightOperand == null || String.isEmpty(rightOperand.trim()))
            {
                Log.info('StringUtils', 'isLogicalStatementTrue', 'Operator.STARTS_WITH: Right operand value cannot be null or blank');
                return false;
            }

            for (String value : rightOperand.split(','))
            {
                if (leftOperand.trim().startsWithIgnoreCase(value.trim()))
                    return true;
            }

            return false;
        }
        else if (operator == String.valueOf(Enums.Operator.ENDS_WITH))
        {
            if (leftOperand == null || String.isEmpty(leftOperand.trim()))
            {
                Log.info('StringUtils', 'isLogicalStatementTrue', 'Operator.ENDS_WITH: Left operand value cannot be null or blank');
                return false;
            }

            if (rightOperand == null || String.isEmpty(rightOperand.trim()))
            {
                Log.info('StringUtils', 'isLogicalStatementTrue', 'Operator.ENDS_WITH: Left operand value cannot be null or blank');
                return false;
            }

            for (String value : rightOperand.split(','))
            {
                if (leftOperand.trim().endsWithIgnoreCase(value.trim()))
                    return true;
            }

            return false;
        }
        else
        {
            Log.info('StringUtils', 'isLogicalStatementTrue', 'Unknown operator: {0}', new List<String> { operator });
            return false;
        }
    }

    private static Boolean isNumeric(String stringNum)
    {
         if (stringNum == null)
            return false;

        if (String.isEmpty(stringNum.trim()))
            return false;

        if (!stringNum.isNumeric())
            return false;

        return true;
    }

    public static Boolean containsInList(String source, List<String> stringList, Integer matchType)
    {
        if (String.isBlank(source) || stringList == null)
            return false;

        source = source.trim();

        for(String item : stringList)
        {
            item = item.trim();
            
            if (matchType == 0 && source.equals(item) || matchType == 1 && source.contains(item) || matchType == 2 && source.startsWith(item))
                return true;
        }

        return false;
    }

    public static Boolean containsInListIgnoreCase(String source, List<String> stringList, Integer matchType)
    {
        if (String.isBlank(source) || stringList == null)
            return false;

        source = source.trim();

        for(String item : stringList)
        {
            item = item.trim();

            if (matchType == 0 && source.equalsIgnoreCase(item) || matchType == 1 && source.containsIgnoreCase(item) || matchType == 2 && source.startsWithIgnoreCase(item))
                return true;
        }

        return false;
    }

    public static String generateRandomString()
    {
        Blob blobKey = crypto.generateAesKey(128);
        String key = EncodingUtil.convertToHex(blobKey);
        
        return key.substring(0,30);
    }

    public static Set<Id> toIdSet(List<String> stringIds)
    {
        Set<Id> ids = new Set<Id>();

        for(String element : stringIds)
        {
            if (element instanceof ID)
            {
                ids.add(element);
            }
        }

        return ids;
    }

    public static String serializeMail(ViewModelsMail.Mail mail)
    {
        String fullBody = System.JSon.serialize(mail, true);
        fullBody = fullBody.replace('"from_x":', '"from":');
        return fullBody;
    }
}