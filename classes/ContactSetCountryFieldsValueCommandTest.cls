@isTest
public class ContactSetCountryFieldsValueCommandTest 
{
    @testSetup 
    public static void setup()
    {
        TestUtilities.setupHEDA();
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        TestDataCountryPhoneCodeMock.create(user, 'Australia', '61', 'AU');
        TestDataCountryPhoneCodeMock.create(user, 'Japan', '11', 'JP');
    }

    @isTest
    static void NewContactNoCountryFieldsSet_ContactCreatedWithoutCountryFieldValueSet() 
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
            
        // Act
        Contact contact = TestDataContactMock.createProspect(user, '');
        Contact updateContact = [SELECT Id, InferredCountry__c, MobilePhoneCountry__c, PhoneCountry__c, RefereeOneMobileCountry__c, RefereeTwoMobileCountry__c, MailingCountry, OtherCountry FROM Contact WHERE Id = :contact.Id];
        
        // Assert
        System.assertEquals(null, updateContact.InferredCountry__c, 'Unexpected Inferred Country value');
        System.assertEquals(null, updateContact.MobilePhoneCountry__c, 'Unexpected Mobile Phone Country value');
        System.assertEquals(null, updateContact.PhoneCountry__c, 'Unexpected Inferred Phone Country value');
        System.assertEquals(null, updateContact.RefereeOneMobileCountry__c, 'Unexpected Referee One Mobile Country value');
        System.assertEquals(null, updateContact.RefereeTwoMobileCountry__c, 'Unexpected Referee Two Mobile Country value');
        System.assertEquals(null, updateContact.MailingCountry, 'Unexpected Mailing Country value');
        System.assertEquals(null, updateContact.OtherCountry, 'Unexpected Other Country value');
    }

    @isTest
    static void NewContactWithMailingAdressSetToCode_ContactCreatedWithMailingCountrySetToCountryFullName() 
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
            
        // Act
        Contact contact = TestDataContactMock.createProspect(user, 'au');
        Contact updateContact = [SELECT Id, InferredCountry__c, MobilePhoneCountry__c, PhoneCountry__c, RefereeOneMobileCountry__c, RefereeTwoMobileCountry__c, MailingCountry, OtherCountry FROM Contact WHERE Id = :contact.Id];

        // Assert
        System.assertEquals('Australia', updateContact.MailingCountry, 'Unexpected Mailing Country value');
    }

    @isTest
    static void ExistingContactUpdatingCountryFieldWithCodeValues_ContactCountryFieldsSetToFullCountryName() 
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact contact = TestDataContactMock.createProspect(user, '');
        contact.InferredCountry__c = 'AU';
        contact.MobilePhoneCountry__c = 'AU';
        contact.PhoneCountry__c = 'au';
        contact.RefereeOneMobileCountry__c = 'AU';
        contact.RefereeTwoMobileCountry__c = 'aU';
        contact.MailingCountry = 'Au';
        contact.OtherCountry = 'JP';
            
        // Act
        update contact;
        Contact updateContact = [SELECT Id, InferredCountry__c, MobilePhoneCountry__c, PhoneCountry__c, RefereeOneMobileCountry__c, RefereeTwoMobileCountry__c, MailingCountry, OtherCountry FROM Contact WHERE Id = :contact.Id];
        
        // Assert
        System.assertEquals('Australia', updateContact.InferredCountry__c, 'Unexpected Inferred Country value');
        System.assertEquals('Australia', updateContact.MobilePhoneCountry__c, 'Unexpected Mobile Phone Country value');
        System.assertEquals('Australia', updateContact.PhoneCountry__c, 'Unexpected Inferred Phone Country value');
        System.assertEquals('Australia', updateContact.RefereeOneMobileCountry__c, 'Unexpected Referee One Mobile Country value');
        System.assertEquals('Australia', updateContact.RefereeTwoMobileCountry__c, 'Unexpected Referee Two Mobile Country value');
        System.assertEquals('Australia', updateContact.MailingCountry, 'Unexpected Mailing Country value');
        System.assertEquals('Japan', updateContact.OtherCountry, 'Unexpected Other Country value');
    }

    @isTest
    static void ExistingContactUpdatingCountryFieldWithNonExistantCodeValues_ContactCountryFieldsValuesNotUpdated() 
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact contact = TestDataContactMock.createProspect(user, '');
        contact.InferredCountry__c = 'AUS';
        contact.MobilePhoneCountry__c = 'AUT';
        contact.PhoneCountry__c = 'Random';
        contact.RefereeOneMobileCountry__c = 'A';
        contact.RefereeTwoMobileCountry__c = 'three';
        contact.MailingCountry = 'ndl';
        contact.OtherCountry = 'BEL';
            
        // Act
        update contact;
        Contact updateContact = [SELECT Id, InferredCountry__c, MobilePhoneCountry__c, PhoneCountry__c, RefereeOneMobileCountry__c, RefereeTwoMobileCountry__c, MailingCountry, OtherCountry FROM Contact WHERE Id = :contact.Id];
        
        // Assert
        System.assertEquals('AUS', updateContact.InferredCountry__c, 'Unexpected Inferred Country value');
        System.assertEquals('AUT', updateContact.MobilePhoneCountry__c, 'Unexpected Mobile Phone Country value');
        System.assertEquals('Random', updateContact.PhoneCountry__c, 'Unexpected Inferred Phone Country value');
        System.assertEquals('A', updateContact.RefereeOneMobileCountry__c, 'Unexpected Referee One Mobile Country value');
        System.assertEquals('three', updateContact.RefereeTwoMobileCountry__c, 'Unexpected Referee Two Mobile Country value');
        System.assertEquals('ndl', updateContact.MailingCountry, 'Unexpected Mailing Country value');
        System.assertEquals('BEL', updateContact.OtherCountry, 'Unexpected Other Country value');
    }
}