@isTest
public class ContactUpdateStatusCommandTest
{
    @testSetup
    static void setup()
    {
        TestUtilities.setupHEDA();
    }

    @isTest
    static void CreateColdContact_ContactStatusCold()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact contact = TestDataContactMock.createProspectCold(user, '04669789789');
        Contact updatedContact = [SELECT Id, Status__c FROM Contact WHERE Id = :contact.Id];

        // Assert
        System.assertNotEquals(null, updatedContact);
        System.assertEquals(Constants.STATUS_COLD, updatedContact.Status__c, 'Incorrect status set');
    } 

    @isTest
    static void CreateWarmContact_ContactStatusWarm()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact contact = TestDataContactMock.createProspectWarm(user);
        Contact updatedContact = [SELECT Id, Status__c FROM Contact WHERE Id = :contact.Id];

        // Assert
        System.assertNotEquals(null, updatedContact);
        System.assertEquals(Constants.STATUS_WARM, updatedContact.Status__c, 'Incorrect status set');
    }

    @isTest
    static void DeleteWorkExperienceValueFromWarmContact_ContactStatusCold()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact contact = TestDataContactMock.createProspectWarm(user);
        contact.WorkExperienceYears__c = 0;
        contact.ManagementExperienceYears__c = 0;

        // Act
        update contact;
        Contact updatedContact = [SELECT Id, Status__c FROM Contact WHERE Id = :contact.Id];

        // Assert
        System.assertNotEquals(null, updatedContact);
        System.assertEquals(Constants.STATUS_COLD, updatedContact.Status__c, 'Incorrect status set');
    }

    @isTest
    static void WarmContactUpdateHighestTertiaryLevelDegreeToDegree_ContactStatusWarm()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact contact = TestDataContactMock.createProspectWarm(user);
        contact.HighestTertiaryLevelDegree__c = 'Associate Degree, Advanced Diploma or equivalent';
        contact.WorkExperienceYears__c = 4;
        contact.ManagementExperienceYears__c = 4;

        // Act
        update contact;
        Contact updatedContact = [SELECT Id, Status__c FROM Contact WHERE Id = :contact.Id];

        // Assert
        System.assertNotEquals(null, updatedContact);
        System.assertEquals(Constants.STATUS_WARM, updatedContact.Status__c, 'Incorrect status set');
    }

    @isTest
    static void WarmContactUpdateHighestTertiaryLevelDegreeToAssociateDegree_ContactStatusWarm()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact contact = TestDataContactMock.createProspectWarm(user);
        contact.HighestTertiaryLevelDegree__c = 'Associate Degree, Advanced Diploma or equivalent';
        contact.WorkExperienceYears__c = null;
        contact.ManagementExperienceYears__c = 4;

        // Act
        update contact;
        Contact updatedContact = [SELECT Id, Status__c FROM Contact WHERE Id = :contact.Id];

        // Assert
        System.assertNotEquals(null, updatedContact);
        System.assertEquals(Constants.STATUS_WARM, updatedContact.Status__c, 'Incorrect status set');
    }

    @isTest
    static void WarmContactUpdateHighestTertiaryLevelDegreeToHighSchool_ContactStatusWarm()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact contact = TestDataContactMock.createProspectWarm(user);
        contact.HighestTertiaryLevelDegree__c = 'High school, Certificate III, IV or Diploma';
        contact.WorkExperienceYears__c = 5;
        contact.ManagementExperienceYears__c = 3;

        // Act
        update contact;
        Contact updatedContact = [SELECT Id, Status__c FROM Contact WHERE Id = :contact.Id];

        // Assert
        System.assertNotEquals(null, updatedContact);
        System.assertEquals(Constants.STATUS_WARM, updatedContact.Status__c, 'Incorrect status set');
    }

    @isTest
    static void WarmContactUpdateHighestTertiaryLevelDegreeToMaster_ContactStatusWarm()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact contact = TestDataContactMock.createProspectWarm(user);
        contact.HighestTertiaryLevelDegree__c = 'Master\'s degree, post graduate degree or higher';
        contact.WorkExperienceYears__c = 4;
        contact.ManagementExperienceYears__c = 4;

        // Act
        update contact;
        Contact updatedContact = [SELECT Id, Status__c FROM Contact WHERE Id = :contact.Id];

        // Assert
        System.assertNotEquals(null, updatedContact);
        System.assertEquals(Constants.STATUS_WARM, updatedContact.Status__c, 'Incorrect status set');
    }

    @isTest
    static void CreateHotContact_ContactStatusHot()
    {
        // Arrange
        TestUtilities.disableComponent('CerebroSyncApplicationCommand');
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact contact = TestDataContactMock.createProspectHot(user);
        Contact updatedContact = [SELECT Id, Status__c FROM Contact WHERE Id = :contact.Id];

        // Assert
        System.assertNotEquals(null, updatedContact);
        System.assertEquals(Constants.STATUS_HOT, updatedContact.Status__c, 'Incorrect status set');
    }

    @isTest
    static void CreateWarmContact_ContactStatusChangedToHot()
    {
        // Arrange
        TestUtilities.disableComponent('CerebroSyncApplicationCommand');
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact contact = TestDataContactMock.createProspectWarm(user);

        contact.ApplicationCourse__c = 'MBA';
        contact.HighestQualificationName__c = 'Bachelor';
        contact.HighestQualificationInstitution__c = 'Uni SA';
        contact.HighestQualificationLanguage__c = 'English';
        contact.HighestQualificationCompletionYear__c = '2018';
        contact.High_School_Level__c = 'Year 12 (or equivalent)';
        contact.Preferred_Start_Date__c = StringUtils.formatDate(TestDataContactMock.getNextPreferredStartDate());

        // Act
        update contact;
        Contact updatedContact = [SELECT Id, Status__c FROM Contact WHERE Id = :contact.Id];

        // Assert
        System.assertNotEquals(null, updatedContact);
        System.assertEquals(Constants.STATUS_HOT, updatedContact.Status__c, 'Incorrect status set');
    }

    @isTest
    static void ColdStudentContatUpdateWarmField_ContactStatusNoChange()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact contact = TestDataContactMock.createStudent(user);
        contact.HighestTertiaryLevelDegree__c = 'Bachelor\'s Degree or equivalent';
        contact.WorkExperienceYears__c = 4;
        contact.ManagementExperienceYears__c = 4;

        // Act
        update contact;
        Contact updatedContact = [SELECT Id, Status__c FROM Contact WHERE Id = :contact.Id];

        // Assert
        System.assertNotEquals(null, updatedContact);
        System.assertEquals(contact.Status__c, updatedContact.Status__c, 'Incorrect status set');
    }
}