public class TaskUpdateCaseOppDocStatusCommand extends BaseTriggerCommand
{
    @TestVisible
    private static String MIRReasonM2 = 'M-2 Insufficient Evidence of Managerial Experience/Relevant Work Experience';
    @TestVisible
    private static String MIRReasonM6 = 'M-6 POE Error (Not Submitted/Insufficient Evidence)';
    @TestVisible
    private static String MIRReasonM9 = 'M-9 Transcript/Proof of Completion Error (Transcript/Parchment)';

    public override List<SObject> validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        List<Task> taskList = new List<Task>();
        Id mirRecordTypeId = RecordTypeUtils.getRecordTypeId(Constants.RECORDTYPE_TASK_MIR, SObjectType.Task);

        for (Task activity : (List<Task>)items)
            if (activity.WhatId != null && ((String)activity.WhatId).startsWith(Constants.ENTITYID_CASE) && activity.RecordTypeId == mirRecordTypeId)
                taskList.add(activity);

        return taskList;
    }

    public override ActionResult command(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        Set<Id> ids = SObjectUtils.getIDs(items, 'WhatId');
        List<Case> cases = getActivityCases(ids);
        Log.info(this.className, 'command', 'Processing following tasks: ' + JSON.serialize(ids));

        for (Task activity : (List<Task>)items)
        {
            Boolean isDirty = false;
            Case activityCase = getActivityCase(activity.WhatId, cases);

            if (activityCase == null)
            {
                Log.info(this.className, 'command', 'No Application Case {0} found for Task {1}', new List<String>{ activity.WhatId, activity.Id });
                continue;
            }

            if (activity.MIR_Reason__c == MIRReasonM2)
            {
                if (activityCase.Opportunity__r.WorkExperienceDocs__c == Constants.DOCSTATUS_NOTREQUIRED)
                    activityCase.Opportunity__r.WorkExperienceDocs__c = Constants.DOCSTATUS_REQUIRED;
                else
                    activityCase.Opportunity__r.WorkExperienceDocs__c = Constants.DOCSTATUS_REJECTED;

                isDirty = true;
            }
            else if (activity.MIR_Reason__c == MIRReasonM6)
            {
                if (activityCase.Opportunity__r.EnglishProficiencyDocs__c == Constants.DOCSTATUS_NOTREQUIRED)
                    activityCase.Opportunity__r.EnglishProficiencyDocs__c = Constants.DOCSTATUS_REQUIRED;
                else
                    activityCase.Opportunity__r.EnglishProficiencyDocs__c = Constants.DOCSTATUS_REJECTED;

                isDirty = true;
            }
            else if (activity.MIR_Reason__c == MIRReasonM9)
            {
                if (activityCase.Opportunity__r.TranscriptDocs__c == Constants.DOCSTATUS_NOTREQUIRED)
                    activityCase.Opportunity__r.TranscriptDocs__c = Constants.DOCSTATUS_REQUIRED;
                else
                    activityCase.Opportunity__r.TranscriptDocs__c = Constants.DOCSTATUS_REJECTED;

                isDirty = true;
            }

            if (isDirty)
                uow.registerDirty(activityCase.Opportunity__r);
        }

        return new ActionResult(ResultStatus.SUCCESS);
    }

    private List<Case> getActivityCases(Set<ID> ids)
    {
        return
        [
            SELECT Id, Opportunity__r.ApplicationReviewed__c, Opportunity__r.TranscriptDocs__c, Opportunity__r.EnglishProficiencyDocs__c,
                Opportunity__r.WorkExperienceDocs__c
            FROM Case
            WHERE Id IN :ids
        ];
    }

    private Case getActivityCase(Id id, List<Case> cases)
    {
        for (Case activityCase : cases)
            if (activityCase.Id == id)
                return activityCase;

        return null;
    }
}