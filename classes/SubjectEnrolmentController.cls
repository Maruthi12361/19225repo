@RestResource(urlMapping='/subject/enrolment/*')
global class SubjectEnrolmentController
{
    @HttpPost
    global static void save()
    {
        ActionResult result;
        String reference = '';
        RestRequest req = RestContext.request;
        ViewModels.Param reqParam = new ViewModels.Param();
        Map<String, Object> optionMap = new Map<String, Object>();

        if (req.headers.containsKey('Reference'))
            reference = req.headers.get('Reference');

        reqParam.Identifier = reference;
        reqParam.Payload =  req.requestBody.toString();

        Log.info('SubjectEnrolmentController', 'save', 'Identifier value {0} \n Payload: {1}', new List<String>{ reqParam.Identifier, reqParam.Payload });

        for (Integer i = 0; i < 5; i++)
        {
            result = ContactService.updateFacultyEnrolmentStatus(reqParam);

            if (result.Status == ResultStatus.SUCCESS || (!result.message.contains('UNABLE_TO_LOCK_ROW') && !result.message.contains('Please try again')))
                break;

            Log.info('SubjectEnrolmentController', 'save', 'Unable to lock row, remaining retry {0}', new List<String>{ String.valueOf(4 - i) });
        }

        RestContext.response.addHeader('Content-Type', 'application/json');
        RestContext.response.statusCode = 200;
    }
}