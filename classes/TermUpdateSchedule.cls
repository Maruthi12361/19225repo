global class TermUpdateSchedule implements Schedulable
{
    global void execute(SchedulableContext sc)
    {
        database.executeBatch(new TermUpdateDatesBatch());
    }
}