public class CaseTrackStatusHistoryCommand extends BaseTriggerCommand
{
    static Map<Id, Status_Tracking_History__c> closedHistoryMap = new Map<Id, Status_Tracking_History__c>();
    static Map<Id, Status_Tracking_History__c> activeHistoryMap = new Map<Id, Status_Tracking_History__c>();
    static DateTime now = DateTime.now();

    public override List<SObject> validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        List<Case> filteredList;

        if (triggerContext == Enums.TriggerContext.AFTER_INSERT)
        {
            filteredList = items;
        }
        else if (triggerContext == Enums.TriggerContext.AFTER_UPDATE)
        {
            filteredList = new List<Case>();

            for (Case newValue: (List<Case>)items)
            {
                Case oldValue = (Case) oldMap.get(newValue.Id);

                if (newValue.Status != oldValue.Status || newValue.OwnerId != oldValue.OwnerId || newValue.Department__c != oldValue.Department__c)
                    filteredList.add(newValue);
            }
        }

        return filteredList;
    }

    public override ActionResult command(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        Map<Id, String> nameMap = queryCaseOwnerNames((List<Case>)items);

        if (triggerContext == Enums.TriggerContext.AFTER_INSERT)
        {
            for (Case newCase : (List<Case>)items)
            {
                Status_Tracking_History__c history = createStatusTrackingHistory(newCase, nameMap.get(newCase.OwnerId));
                uow.registerNew(history);
                Log.info(this.className, 'command', 'Inserting New Status Tracking History for case:' + history);
            }
        }
        else if (triggerContext == Enums.TriggerContext.AFTER_UPDATE)
        {
            List<Case> casesWithoutInTransitHistory = new List<Case>();
            List<Case> casesWithInTransitHistory = new List<Case>();

            for (Case newCase : (List<Case>)items)
            {
                Case oldCase = (Case) oldMap.get(newCase.Id);

                if (activeHistoryMap.containsKey(newCase.Id))
                {
                    Status_Tracking_History__c activeHistory = activeHistoryMap.get(newCase.Id);

                    if (activeHistory != null && activeHistory.Status_Stage__c == newCase.StatusDepartment__c && activeHistory.User__c == nameMap.get(newCase.OwnerId))
                    {
                        Log.info(this.className, 'command', 'Skipping ' + newCase.Id + ' because there exists an active history:' + activeHistory);
                        continue;
                    }

                    casesWithInTransitHistory.add(newCase);
                }
                else
                    casesWithoutInTransitHistory.add(newCase);
            }

            if (casesWithoutInTransitHistory.size() > 0)
            {
                Map<Id, Case> caseMap = new Map<Id, Case>(casesWithoutInTransitHistory);

                List<Status_Tracking_History__c> histories =
                [
                    SELECT Id, Status_Stage__c, CaseId__c, EndDateTime__c, Next_Status_Stage__c, Next_User__c, Type__c
                    FROM Status_Tracking_History__c
                    WHERE (EndDateTime__c = NULL OR (EndDateTime__c <> NULL AND Next_Status_Stage__c = NULL AND Status_Stage__c LIKE '%Closed'))
                        AND (Type__c = 'State Change' OR Type__c = 'User Reassignment') AND CaseId__c IN : caseMap.keySet()
                ];

                for (Status_Tracking_History__c history : histories)
                {
                    Case newCase = caseMap.get(history.CaseId__c);

                    history.Direction__c = getDirection(history.Status_Stage__c, newCase.StatusDepartment__c);
                    history.EndDateTime__c = now;
                    history.Next_Status_Stage__c = newCase.StatusDepartment__c;
                    history.Next_User__c = nameMap.get(newCase.OwnerId);
                    history.Type__c = history.Status_Stage__c == newCase.StatusDepartment__c ? 'User Reassignment' : 'State Change';

                    uow.registerDirty(history);
                    closedHistoryMap.put(history.CaseId__c, history);
                    Log.info(this.className, 'command', 'Closing Old Status Tracking History for case:' + history);
                }

                for (Case newCase : casesWithoutInTransitHistory)
                {
                    Status_Tracking_History__c history = createStatusTrackingHistory(newCase, nameMap.get(newCase.OwnerId));
                    uow.registerNew(history);
                    activeHistoryMap.put(history.CaseId__c, history);
                    Log.info(this.className, 'command', 'Creating New Status Tracking History for case:' + history);
                }
            }

            if (casesWithInTransitHistory.size() > 0)
            {
                for (Case newCase : casesWithInTransitHistory)
                {
                    Status_Tracking_History__c closedHistory = closedHistoryMap.get(newCase.Id);

                    if (closedHistory != null)
                    {
                        closedHistory.Direction__c = getDirection(closedHistory.Status_Stage__c, newCase.StatusDepartment__c);
                        closedHistory.Next_Status_Stage__c = newCase.StatusDepartment__c;
                        closedHistory.Next_User__c = nameMap.get(newCase.OwnerId);
                        closedHistory.Type__c = 'State Change';

                        uow.registerDirty(closedHistory);
                        closedHistoryMap.put(newCase.Id, closedHistory);
                        Log.info(this.className, 'command', 'Updating Closed Status Tracking History for case:' + closedHistory);
                    }

                    Status_Tracking_History__c activeHistory = activeHistoryMap.get(newCase.Id);

                    if (activeHistory != null)
                    {
                        activeHistory.Department__c = newCase.Department__c;
                        activeHistory.Status_Stage__c = newCase.StatusDepartment__c;
                        activeHistory.User__c = nameMap.get(newCase.OwnerId);

                        if (!newCase.Status.equals(Constants.CASE_STATUS_CLOSED))
                            activeHistory.EndDateTime__c = null;

                        uow.registerDirty(activeHistory);
                        activeHistoryMap.put(newCase.Id, activeHistory);
                        Log.info(this.className, 'command', 'Updating Active Status Tracking History for case:' + activeHistory);
                    }
                }
            }
        }

        return new ActionResult(ResultStatus.SUCCESS);
    }

    private Status_Tracking_History__c createStatusTrackingHistory(Case newCase, String userName)
    {
        Status_Tracking_History__c history = new Status_Tracking_History__c();
        history.CaseId__c = newCase.Id;
        history.Contact_ID__c = newCase.ContactId;
        history.Department__c = newCase.Department__c;
        history.StartDateTime__c = now;
        history.Status_Stage__c = newCase.StatusDepartment__c;
        history.Type__c = 'State Change';
        history.User__c = userName;

        if (newCase.Status.equals(Constants.CASE_STATUS_CLOSED))
            history.EndDateTime__c = now;

        return history;
    }

    private Integer getStateValue(String status, String otherStatus)
    {
        Integer value = 3;

        if (!String.isBlank(status) && !String.isBlank(otherStatus))
        {
            if (status.contains(Constants.CASE_STATUS_NEW) && !otherStatus.contains(Constants.CASE_STATUS_NEW))
                value = 1;
            else if (status.contains(Constants.CASE_STATUS_CLOSED) && !otherStatus.contains(Constants.CASE_STATUS_CLOSED))
                value = 6;
            else if (status.contains(Constants.CASE_DEPT_ACQUISITIONS) || status.contains(Constants.CASE_DEPT_PROGRESSIONS))
                value = 2;
            else if (status.contains(Constants.CASE_DEPT_ADMISSIONS) || status.contains(Constants.CASE_DEPT_STUDENT_CENTRAL))
                value = 3;
            else if (status.contains(Constants.CASE_DEPT_FINANCE))
                value = 4;
            else if (status.contains(Constants.CASE_DEPT_TIMETABLING))
                value = 5;
        }

        return value;
    }

    private String getDirection(String oldStatus, String newStatus)
    {
        String result = 'None';
        Integer oldStateValue = getStateValue(oldStatus, newStatus);
        Integer newStateValue = getStateValue(newStatus, oldStatus);

        if (newStateValue > oldStateValue)
            result = 'Forward';
        else if (newStateValue < oldStateValue)
            result = 'Backward';

        return result;
    }

    private Map<Id, String> queryCaseOwnerNames(List<Case> items)
    {
        Map<Id, String> nameMap = new Map<Id, String>();
        Set<Id> userIds = new Set<Id>();
        Set<Id> queueIds = new Set<Id>();

        for (Case newCase : (List<Case>)items)
        {
            if (newCase.OwnerId != null)
            {
                if (String.valueOf(newCase.OwnerId).startsWith(Constants.ENTITYID_USER))
                {
                    if (!userIds.contains(newCase.OwnerId))
                        userIds.add(newCase.OwnerId);
                }
                else
                {
                    if (!queueIds.contains(newCase.OwnerId))
                        queueIds.add(newCase.OwnerId);
                }
            }
        }

        if (userIds.size() > 0)
        {
            List<User> users = UserService.get(userIds);

            for (User user : users)
            {
                nameMap.put(user.Id, user.Name);
            }
        }

        if (queueIds.size() > 0)
        {
            List<Group> queues = GroupService.get(queueIds);

            for (Group queue : queues)
            {
                nameMap.put(queue.Id, queue.Name);
            }
        }

        return nameMap;
    }
}