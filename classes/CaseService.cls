public class CaseService
{
    public static ActionResult calculateFirstEmailResponseTime(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        return new CaseCalculateFirstEmailResponseCommand().execute(items, oldMap, triggerContext);
    }

    public static ActionResult calculateLastInteraction(List<Case> cases)
    {
        return new CaseCalculateLastInteractionCommand().execute((List<SObject>)cases);
    }

    public static ActionResult trackStatusHistory(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        return new CaseTrackStatusHistoryCommand().execute(items, oldMap, triggerContext);
    }
    
    public static ActionResult updateAwardAddress(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        return new CaseUpdateAwardAddressCommand().execute(items, oldMap, triggerContext);
    }

    public static ActionResult validateFileFlags(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        return new CaseFileFlagsValidate().execute(items, oldMap, triggerContext);
    }

    public static ActionResult updateSSOStatus(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        return new CaseUpdateSSOStatusCommand().execute(items, oldMap, triggerContext);
    }
    
    public static ActionResult notifyOfNewComment(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        return new CaseNotifyOfNewCommentCommand().execute(items, oldMap, triggerContext);
    }
}