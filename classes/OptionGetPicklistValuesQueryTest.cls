@isTest
public class OptionGetPicklistValuesQueryTest
{
    @testSetup
    static void setup()
    {
        TestUtilities.setupHEDA();
    }

    @isTest
    static void ContactListNamesWithRecordTypeProvided_SuccessfullyReturnedListOfRecords()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        ViewModels.Param param = new ViewModels.Param();
        Map<String, Object> optionMap = new Map<String, Object>
        {
            'list' => 'countries, preferredStartDates',
            'type' => Constants.RECORDTYPE_CONTACT_PROSPECT.toLowerCase()
        };
        param.Payload = JSON.serialize(optionMap);

        // Act
        test.startTest();
        ViewModels.PicklistOptions option = new OptionGetPicklistValuesQuery().query(param);
        test.stopTest();

        // Assert
        System.assertNotEquals(null, option, 'Option should not be null');
        System.assertNotEquals(null, option.Data, 'Option data value should not be null');
        System.assertEquals(true, String.valueOf(option.Data).contains('Countries'), 'Expected to see a Countries list');
    }

    @isTest
    static void OppPicklistListWithRecordTypeProvided_SuccessfullyReturnedListOfRecords()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        ViewModels.Param param = new ViewModels.Param();
        Map<String, Object> optionMap = new Map<String, Object>
        {
            'list' => 'PrimaryLocationDuringStudies',
            'type' => Constants.RECORDTYPE_CONTACT_PROSPECT.toLowerCase()
        };
        param.Payload = JSON.serialize(optionMap);

        // Act
        test.startTest();
        ViewModels.PicklistOptions option = new OptionGetPicklistValuesQuery().query(param);
        test.stopTest();

        // Assert
        System.assertNotEquals(null, option, 'Option should not be null');
        System.assertNotEquals(null, option.Data, 'Option data value should not be null');
        System.assertEquals(true, String.valueOf(option.Data).contains('PrimaryLocationDuringStudies'), 'Expected to see a PrimaryLocationDuringStudies list');
    }

    @isTest 
    static void ParameterNullValuePassed_DefaultListsReturned()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());

        // Act
        test.startTest();
        ViewModels.PicklistOptions option = new OptionGetPicklistValuesQuery().query(null);
        test.stopTest();

        // Assert
        System.assertNotEquals(null, option, 'Option should not be null');
        System.assertNotEquals(null, option.Data, 'Option data value should not be null');
        System.assertEquals(true, String.valueOf(option.Data).contains('Countries'), 'Expected to see a Countries list');
    }

    @isTest
    static void ParameterPayloadNullValuePassed_DefaultListsReturned()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        ViewModels.Param param = new ViewModels.Param();
        param.Payload = null;

        // Act
        test.startTest();
        ViewModels.PicklistOptions option = new OptionGetPicklistValuesQuery().query(param);
        test.stopTest();

        // Assert
        System.assertNotEquals(null, option, 'Option should not be null');
        System.assertNotEquals(null, option.Data, 'Option data value should not be null');
        System.assertEquals(true, String.valueOf(option.Data).contains('Countries'), 'Expected to see a Countries list');
    }

    @isTest
    static void EmptyListParameter_EmptyListErrorReturned()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        ViewModels.Param param = new ViewModels.Param();
        Map<String, Object> optionMap = new Map<String, Object>
        {
            'list' => ''
        };
        param.Payload = JSON.serialize(optionMap);
        String errorMessage = '';

        // Act
        try
        {
            test.startTest();
            ViewModels.PicklistOptions option = new OptionGetPicklistValuesQuery().query(param);
            test.stopTest();
        }
        catch (Exception ex)
        {
           errorMessage = ex.getMessage();
        }

        // Assert
        System.assert(errorMessage.contains('List name not supported'), 'Unexpected error message');
    }

    @isTest
    static void UnsupportedListInParameter_UnsupportedListErrorReturned()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        ViewModels.Param param = new ViewModels.Param();
        Map<String, Object> optionMap = new Map<String, Object>
        {
            'list' => 'someTstList'
        };
        param.Payload = JSON.serialize(optionMap);
        String errorMessage = '';

        // Act
        try
        {
            test.startTest();
            ViewModels.PicklistOptions option = new OptionGetPicklistValuesQuery().query(param);
            test.stopTest();
        }
        catch (Exception ex)
        {
           errorMessage = ex.getMessage();
        }

        // Assert
        System.assert(errorMessage.contains('List name not supported'), 'Unexpected error message');
    }

    @isTest
    static void LightningModelSubjectEnrolmentListRequested_ListOfRecordsSuccessfullyReturned()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        ViewModels.Param param = new ViewModels.Param();
        Map<String, Object> optionMap = new Map<String, Object>
        {
            'list' => 'subjectEnrolmentRoles, subjectEnrolmentStatuses',
            'type' => 'faculty',
            'isLightningModel' => true
        };
        param.Payload = JSON.serialize(optionMap);

        // Act
        test.startTest();
        ViewModels.PicklistOptions option = new OptionGetPicklistValuesQuery().query(param);
        test.stopTest();

        // Assert
        System.assertNotEquals(null, option, 'Option should not be null');
        System.assertNotEquals(null, option.Data, 'Option data value should not be null');
    }
}