@isTest
public class ContactBatchRolloverDetailScheduleTests
{
    private static User user = UserService.getByAlias(Constants.USER_TESTSETUP);
    private static String cerebroID = 'A001641336';
    
    @testSetup 
    static void setup() 
    {
        // Arrange
        TestUtilities.setupHEDA();
        TestDataContactMock.createRolloverStudent(user, cerebroID);
    }
    
    @isTest 
    static void ExecuteJobWithoutParameter_ResetsAllStudents() 
    {
        // Arrange
        Datetime runTime = System.now().addMinutes(15); 
        String cron = runTime.second() + ' ' + runTime.minute() + ' ' + runTime.hour()
            + ' ' + runTime.day() + ' ' + runTime.month() + ' ? ' + runTime.year();
        
        // Act
        Test.startTest();        
        ContactBatchRolloverDetailSchedule job = new ContactBatchRolloverDetailSchedule();
        String jobId = System.schedule('Test_ContactBatchRolloverDetailSchedule', cron, job);
        job.execute(null);
        Test.stopTest();        
		
        // Assert
        Contact contact = 
            [
                SELECT Id,Current_Course_Status__c,Passed_Subjects__c,Incomplete_Subjects__c,Required_Subjects__c,Rollover_Status__c,
                    MC_Subjects__c,AtRisk__c,Course_Start_Date__c,Next_Followup_Date__c,Manual_Followup_Date__c,MBA_Course_Name__c,FeeHelp_Eligibility__c,
                    Last_Used_Funding_Source__c,FeeSet__c,FeeHelp_Approved__c 
                FROM Contact
                WHERE Cerebro_Student_ID__c = : cerebroID
            ];
        
        system.assertEquals(null, contact.Current_Course_Status__c, 'Wrong value for Current_Course_Status__c');
    }
    
    @isTest 
    static void ExecuteJobWithParameter_ResetsIdentifiedStudents() 
    {
        // Arrange
        Datetime runTime = System.now().addMinutes(15); 
        String cron = runTime.second() + ' ' + runTime.minute() + ' ' + runTime.hour()
            + ' ' + runTime.day() + ' ' + runTime.month() + ' ? ' + runTime.year();
        
        // Act
        Test.startTest();
        ContactBatchRolloverDetailSchedule job = new ContactBatchRolloverDetailSchedule('\'' + cerebroID + '\'');
        String jobId = System.schedule('Test_ContactBatchRolloverDetailSchedule', cron, job);
        job.execute(null);
        Test.stopTest();        
		
        // Assert
        Contact contact = 
            [
                SELECT Id,Current_Course_Status__c,Passed_Subjects__c,Incomplete_Subjects__c,Required_Subjects__c,Rollover_Status__c,
                    MC_Subjects__c,AtRisk__c,Course_Start_Date__c,Next_Followup_Date__c,Manual_Followup_Date__c,MBA_Course_Name__c,FeeHelp_Eligibility__c,
                    Last_Used_Funding_Source__c,FeeSet__c,FeeHelp_Approved__c 
                FROM Contact 
                WHERE Cerebro_Student_ID__c = : cerebroID
            ];
        
        system.assertEquals(null, contact.Current_Course_Status__c, 'Wrong value for Current_Course_Status__c');
    }
}