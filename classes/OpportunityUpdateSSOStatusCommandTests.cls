@isTest
public class OpportunityUpdateSSOStatusCommandTests 
{
    @testSetup
    static void setup()
    {
        TestUtilities.setupHEDA();
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        TestUtilities.disableComponent('CerebroSyncApplicationCommand');
        User user = UserService.getByAlias(Constants.USER_COURSEADVISOR);
        Contact contact = TestDataContactMock.createProspectHot(user, 'A0019545878');
    }

    @isTest
    static void ProspectAcceptOffer_SSOStatusUpdatedSuccessfully()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        TestUtilities.disableComponent('ContactSendSSOLoginDetailsCommand');
        TestUtilities.disableComponent('OppUpdateContactSSOStatusCommand');
        Contact contact = [SELECT Id, Cerebro_Student_ID__c, SingleSignOnStatus__c FROM Contact LIMIT 1];
        Opportunity opp = [SELECT Id, Contact__c, OfferAcceptedDate__c, RecordTypeId FROM Opportunity LIMIT 1];
        Opportunity cloneOpp = opp.clone(true, true);
        
        opp.OfferAcceptedDate__c = System.now();
        update contact;

        Map<Id, SOBject> oldMap = new Map<Id,SObject>();
        oldMap.put(cloneOpp.Id, cloneOpp);

        // Act
        Test.startTest();
        ActionResult result = OpportunityService.updateSSOStatus(new List<Opportunity> { opp }, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        Test.stopTest();

        Contact updatedContact = [SELECT Id, SingleSignOnStatus__c, SSO_ID__c, SingleSignOnStatusLastUpdated__c FROM Contact WHERE Id = :contact.Id];

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.status, 'Unexpected result status');
        System.assertEquals(Constants.CONTACT_SINGLESIGNONSTATUS_PROSPECTPROVISIONING, updatedContact.SingleSignOnStatus__c, 'Unexpected single sign on status');
    }
}