public class SObjectIdentifyAllDuplicatesCommand extends BaseCommand 
{
    public override List<SObject> validate(List<SObject> items)
    {
        return items;
    }

    public override ActionResult command(List<SObject> items)
    {
        ActionResult result = new ActionResult();
        try
        {
            List<DuplicateRule> rules = getDuplicateRules();

            for(SObject s : items)
            {
                Datacloud.FindDuplicatesResult findDuplicateResult = Datacloud.FindDuplicates.findDuplicates(new List<SObject>{s})[0];

                for (Datacloud.DuplicateResult duplicateResult : findDuplicateResult.getDuplicateResults()) 
                {
                    for (Datacloud.MatchResult matchResult : duplicateResult.getMatchResults())
                    {
                        if(matchResult.getSize() == 0)
                            continue;

                        Set<Id> duplicateIds = new Set<Id>();
                        for (Datacloud.MatchRecord matchRecord : matchResult.getMatchRecords()) 
                            duplicateIds.add(matchRecord.getRecord().Id);
                        
                        Log.info(this.className, 'command', 'Following duplicate records found for {0} record using {1} duplicate rule: {2}',
                        new List<String>{s.Id, duplicateResult.getDuplicateRule(), JSON.serializePretty(duplicateIds)});

                        if(doesDuplicateRecordSetExists(s.id, duplicateIds))
                            continue;

                        duplicateIds.add(s.Id); // Add parent id
                        createDuplicateRecordSet(getDuplicateRuleId(rules,duplicateResult.getDuplicateRule()), duplicateIds);
                    }
                }
            }

            return result;
        }
        catch(Exception e)
        {
            String errorMsg = 'Failed to execute command. ' + e.getMessage();
            Log.error(this.className, 'command', errorMsg, e);
            result.status = ResultStatus.ERROR;
            result.messages.add(errorMsg);

            return result;
        }
    }

    public List<DuplicateRule> getDuplicateRules()
    {
        return [SELECT Id, SobjectType, MasterLabel, DeveloperName, IsActive FROM DuplicateRule WHERE IsActive = true AND IsDeleted = false];
    }

    public Id getDuplicateRuleId(List<DuplicateRule> rules, String name)
    {
        for(DuplicateRule rule : rules)
        {
            if(rule.DeveloperName == name)
                return rule.Id;
        }
        return null;
    }

    private Boolean doesDuplicateRecordSetExists(Id parentId, Set<Id> duplicateIds)
    {
        try
        {
            DuplicateRecordItem parentRecordItem; 
            List<DuplicateRecordItem> parentList = [SELECT Id, DuplicateRecordSetId, RecordId FROM DuplicateRecordItem WHERE RecordId = :parentId];
            
            if(parentList.size() == 0)
                return false;// No duplicate record item exists for parent contact

            parentRecordItem = parentList[0];
            List<DuplicateRecordItem> recordItems = 
            [
                SELECT Id, DuplicateRecordSetId, RecordId 
                FROM DuplicateRecordItem 
                WHERE RecordId in :duplicateIds AND DuplicateRecordSetId = :parentRecordItem.DuplicateRecordSetId
            ];

            if(recordItems.size() >= duplicateIds.size())
                return true; // All of the duplicates already exist in the same duplicate record set
            
            return false;
        }
        catch(Exception e)
        {
            Log.error(this.className, 'doesDuplicateRecordSetExists', 'Failed to retrive duplicates. ' + e.getMessage());
            throw e;
        }
    }

    private void createDuplicateRecordSet(Id duplicateRuleId, Set<Id> duplicateIds)
    {
        List<DuplicateRecordItem> duplicteRecordItems = new List<DuplicateRecordItem>();
        DuplicateRecordSet drs = new DuplicateRecordSet(duplicateRuleId = duplicateRuleId);
        Database.insert(drs); // DuplicateRecordSet not supported by uow

        for(Id recordId : duplicateIds)
        {
            DuplicateRecordItem recordItem = new DuplicateRecordItem
            (
                DuplicateRecordSetId = drs.Id,
                RecordId = recordId
            );
            duplicteRecordItems.add(recordItem); 
        }

        Database.insert(duplicteRecordItems); // DuplicateRecordItem not supported by uow
    }
}