public class OpportunityCheckReopenCommand extends BaseTriggerCommand
{
    public override List<SObject> validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        List<Opportunity> result = new List<Opportunity>();
        Id acquisitionRecordTypeId = RecordTypeUtils.getRecordTypeId(Constants.RECORDTYPE_OPPORTUNITY_ACQUISITION, SObjectType.Opportunity);

        for (Opportunity opp : (List<Opportunity>)items)
        {
            if (opp.RecordTypeId == acquisitionRecordTypeId && opp.Contact__c != null && triggerContext == Enums.TriggerContext.BEFORE_UPDATE)
            {
                Opportunity oldValue = (Opportunity) oldMap.get(opp.Id);

                if (!opp.IsClosed && oldValue.IsClosed)
                    result.add(opp);
            }
        }

        return result;
    }

    public override ActionResult command(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        Set<Id> contactIds = SObjectUtils.getIds(items, 'Contact__c');
        Set<Id> idSet = lookupOpenOpportunities(contactIds);

        for (Opportunity opp : (List<Opportunity>)items)
        {
            String errorMsg = '';

            if (idSet.contains(opp.Contact__c))
            {
                errorMsg = 'You cannot reopen this opportunity as there is other open acquisition opportunity associated with the same Contact.';
                Log.error(this.className, 'command', 'Opportunity: ' + opp.Id + ', ' +  errorMsg);
                opp.StageName.addError(errorMsg);
            }
        }

        return new ActionResult(ResultStatus.SUCCESS);
    }

    private Set<Id> lookupOpenOpportunities(Set<Id> contactIds)
    {
        Set<Id> idSet = new Set<Id>();
        List<AggregateResult> results =
            [
                SELECT Contact__c, COUNT(Id)
                FROM Opportunity
                WHERE Contact__c IN : contactIds AND RecordType.DeveloperName = :Constants.RECORDTYPE_OPPORTUNITY_ACQUISITION AND IsClosed = false
                GROUP BY Contact__c
                HAVING COUNT(Id) > 0
            ];

        for (AggregateResult result : results)
            idSet.add((Id)result.get('Contact__c'));

        return idSet;
    }
}