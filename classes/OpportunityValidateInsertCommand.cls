public class OpportunityValidateInsertCommand extends BaseTriggerCommand 
{
    static final string allowedProfile = 'System Administrator';

    public override List<SObject> validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        String profileName = ProfileUtilities.getCurrentUserProfileName();
        Set<Id> contactIds = SObjectUtils.getIds(items, 'Contact__c');
        Id acquisitionRecordTypeId = RecordTypeUtils.getRecordTypeId(Constants.RECORDTYPE_OPPORTUNITY_ACQUISITION, SObjectType.Opportunity);

        List<Contact> contacts = getContacts(contactIds);
        List<Opportunity> openOpps = getOpenOpportunities(contactIds);

        for (Opportunity opp : (List<Opportunity>)items)
        {
            if (opp.RecordTypeId == acquisitionRecordTypeId && profileName != allowedProfile)
            {
                String errorMsg = '';
                Contact c = getContact(opp.Contact__c, contacts);

                if (opp.Contact__c == null || c == null)
                {
                    errorMsg = 'Opportunity is not associated with a contact or contact does not exists';
                    Log.error(this.className, 'validate', 'RecordType: ' + opp.RecordTypeId + ' Profile: ' + profileName + ' ' +  errorMsg);
                    opp.addError(errorMsg);
                    throw new Exceptions.ApplicationException(errorMsg);
                }

                if (c.Status__c != Constants.STATUS_HOT)
                {
                    errorMsg = 'Associated contact not in a Hot status';
                    Log.error(this.className, 'validate', 'RecordType: ' + opp.RecordTypeId + ' Profile: ' + profileName + ' ' +  errorMsg);
                    opp.addError(errorMsg);
                    throw new Exceptions.ApplicationException(errorMsg);
                }

                List<Opportunity> contactOpps = getContactOpportunities(opp.Contact__c, openOpps);
                if (contactOpps.size() > 0)
                {
                    errorMsg = 'There is at least one open acquisition opportunity associated with Contact';
                    Log.error(this.className, 'validate', 'RecordType: ' + opp.RecordTypeId + ' Profile: ' + profileName + ' ' +  errorMsg);
                    opp.addError(errorMsg);
                    throw new Exceptions.ApplicationException(errorMsg);
                }   
            }    
        }

        return items;
    }

    public override ActionResult command(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        return new ActionResult(ResultStatus.SUCCESS);
    }

    private List<Contact> getContacts(Set<Id> ids)
    {
        List<Contact> contacts = 
            [
                SELECT Id, Status__c 
                FROM Contact 
                WHERE Id in :ids
            ];
        
        return contacts;
    }

    private Contact getContact(Id contactId, List<Contact> contacts)
    {
        for (Contact c : contacts)
            if (c.Id == contactId)
                return c;
        
        return null;
    }

    private List<Opportunity> getOpenOpportunities(Set<Id> contactIds)
    {
        List<Opportunity> opps = 
            [
                SELECT Id, RecordType.DeveloperName, StageName, Contact__c
                FROM Opportunity 
                WHERE RecordType.DeveloperName = :Constants.RECORDTYPE_OPPORTUNITY_ACQUISITION AND Contact__c in :contactIds AND StageName NOT IN :Constants.OPPORTUNITY_ACQUISITION_CLOSEDSTAGES
            ];

        return opps;
    }

    private List<Opportunity> getContactOpportunities(Id contactId, List<Opportunity> opps)
    {
        List<Opportunity> contactOpps = new List<Opportunity>();

        for (Opportunity o : opps)
            if (o.Contact__c == contactId)
                contactOpps.add(o);

        return contactOpps;
    }
}