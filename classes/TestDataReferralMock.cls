@isTest
public class TestDataReferralMock 
{
    public static Referral__c create(User user, Id referrerId, Id refereeId, Boolean saveRecord)
    {
        Referral__c referral = new Referral__c
        (
            Referrer_Contact__c = referrerId,
            Referee_Contact__c = refereeId
        );

        if (saveRecord)
        {
            System.RunAs(user)
            {
                insert referral;
            }
        }

        return referral;
    }
}