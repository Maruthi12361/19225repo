/* To be replaced by a duplicate rule once Salesforce allows matching rule mapping to editable for the same object */
public class ContactIdentifyDuplicatesCommand extends BaseQueueableCommand 
{
    static final List<String> contactFields = new List<String>
    {
        'Email', 'PersonalEmail__c', 'hed__WorkEmail__c', 'hed__UniversityEmail__c', 'hed__AlternateEmail__c'
    };

    public ContactIdentifyDuplicatesCommand()
    {
        super(1);
    }

    public override List<SObject> validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        List<Contact> result = new List<Contact>();

        if (!(items[0] instanceof Contact))
            throw new Exceptions.InvalidParameterException(this.className + ' accepts only Contact objects as parameter');

        for (Contact contact : (List<Contact>) items)
        {
            Contact oldContact = (Contact) oldMap.get(contact.Id);
            if (!SObjectUtils.hasAnyPropertyChangedIgnoreCase(contact, oldContact, contactFields))
                continue; // No email fields modified

            result.add(contact);
        }

        return result;
    }

    public override ActionResult command(List<SObject> items, Id jobId)
    {
        ActionResult result = new ActionResult();

        try
        {
            List<Contact> contacts = getContacts(SObjectUtils.getIds(items, 'Id'));

            for (Contact contact : contacts)
            {
                String searchQuery;
                List<String> emailsToSearch = getEmailsToSearch(contact);
                List<Contact> potentialDuplicates = new List<Contact>();

                if (emailsToSearch.size() == 0)
                    continue; // All of the emails values are blank or null

                searchQuery = String.join(emailsToSearch, ' OR ');
                Log.info(this.className, 'command', 'Searching for following emails: ' + emailsToSearch);
                List<Contact> searchResult = (List<Contact>) [FIND :searchQuery IN EMAIL FIELDS RETURNING Contact(Id, Email, DuplicateKey__c WHERE Id != :contact.Id)][0];

                if (searchResult.size() == 0)
                    continue; // No duplicates found for contact

                for (Contact potentialDuplicate : searchResult)
                {
                    if (contact.Email == potentialDuplicate.Email)
                        continue; // New Matching rule should handle Email to Email duplicates

                    potentialDuplicates.add(potentialDuplicate);
                }

                if (potentialDuplicates.size() == 0)
                    continue; // No duplicates found for contact

                Set<Id> duplicateRecordIds = SObjectUtils.getIds(potentialDuplicates, 'Id');

                if(duplicateRecordIds.size() == 0 || doesDuplicateRecordSetExists(contact.Id, duplicateRecordIds))
                    continue; // Duplicate record set already exists or no duplicate records found

                String duplicateKey = StringUtils.generateRandomString();
                potentialDuplicates.add(contact);

                for (Contact potentialDuplicate : potentialDuplicates)
                    potentialDuplicate.DuplicateKey__c = duplicateKey;

                uow.registerDirty(potentialDuplicates);
            }

            return result;
        }
        catch(Exception e)
        {
            result.status = ResultStatus.ERROR;
            result.messages.add(e.getMessage());
            Log.error(this.className, 'command', 'An exception occured while executing command', e);
            return result;
        }
    }

    private List<Contact> getContacts(Set<Id> ids)
    {
        String query = 
            'SELECT ' + 
                'Id, DuplicateKey__c, ' + String.join(contactFields, ', ') + ' ' +
            'FROM Contact ' + 
            'WHERE Id in :ids';

        try
        {
            return Database.query(query);
        }
        catch(Exception exp)
        {
            Log.error(this.className, 'getContacts', String.format('Failed to retrive following contacts: {0}',
                new String[] { JSON.serializePretty(ids) }), exp);
            throw exp;
        }
    }

    private List<String> getEmailsToSearch(Contact contact)
    {
        List<String> emails = new List<String>();

        if (String.IsNotEmpty(contact.Email))
            emails.add(contact.Email);

        if(String.IsNotEmpty(contact.PersonalEmail__c))
            emails.add(contact.PersonalEmail__c);

        if(String.IsNotEmpty(contact.hed__WorkEmail__c))
            emails.add(contact.hed__WorkEmail__c);

        if(String.IsNotEmpty(contact.hed__UniversityEmail__c))
            emails.add(contact.hed__UniversityEmail__c);

        if(String.IsNotEmpty(contact.hed__AlternateEmail__c))
            emails.add(contact.hed__AlternateEmail__c);

        return emails;
    }

    private Boolean doesDuplicateRecordSetExists(Id parentId, Set<Id> duplicateIds)
    {
        try
        {
            DuplicateRecordItem parentRecordItem; 
            List<DuplicateRecordItem> parentList = [SELECT Id, DuplicateRecordSetId, RecordId FROM DuplicateRecordItem WHERE RecordId = :parentId];

            if (parentList.size() == 0)
                return false;// No duplicate record item exists for parent contact

            parentRecordItem = parentList[0];
            List<DuplicateRecordItem> recordItems = 
            [
                SELECT Id, DuplicateRecordSetId, RecordId 
                FROM DuplicateRecordItem 
                WHERE RecordId in :duplicateIds AND DuplicateRecordSetId = :parentRecordItem.DuplicateRecordSetId
            ];

            if (recordItems.size() >= duplicateIds.size())
                return true; // All of the duplicates already exist in the same duplicate record set

            return false;
        }
        catch(Exception exp)
        {
            Log.error(this.className, 'doesDuplicateRecordSetExists', 'Failed to retrive duplicates. ' + exp.getMessage());
            throw exp;
        }
    }
}