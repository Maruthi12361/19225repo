@isTest
public class TestDataContactReferrerMock 
{
    public static ContactReferrer__c create(User user, Id contactId) 
    {
        return create(user, contactId, 'facebook', 'facebook', 'paid-social');
    }

    public static ContactReferrer__c create(User user, Id contactId, String source, String content, String medium) 
    {
        return create(user, contactId, source, content, medium, '', '', '', '', '');
    }

    public static ContactReferrer__c create(User user, Id contactId, String source, String content, String medium, 
        String campaing, String gclid, String keyword, String term, String referrerSource) 
    {
        ContactReferrer__c referrer = new ContactReferrer__c
        (
            ContactID__c = contactId,
            utm_campaign__c = campaing,
            utm_gclid__c = gclid,
            utm_keyword__c = keyword,
            utm_content__c = content, 
            utm_source__c = source, 
            utm_medium__c = medium,
            utm_term__c = term,
            utm_referralSource__c = referrerSource
        );
        
        System.RunAs(user)
        {
            insert referrer;
        }

        return referrer;
    }
}