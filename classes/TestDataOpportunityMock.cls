@isTest
public class TestDataOpportunityMock
{
    public static Opportunity create(User user)
    {
        return create(user, null, null, true);
    }

    public static Opportunity create(User user, Contact contact, String stageName, Boolean save)
    {
        return create(user, contact, stageName, null, save);
    }

    public static Opportunity create(User user, Contact contact, String stageName, Campaign campaign, Boolean save)
    {
        Opportunity opp = new Opportunity
        (
            OwnerId = user.Id,

            OAF_Submitted__c = false,
            CloseDate = System.today(),
            Name = 'Test Opp ' + System.now(),

            Has_Special_Needs__c = 'No',
            Special_Needs_Advice_Required__c = 'No',

            Payment_Method__c = 'Self Funded',
            Parent_1_Highest_Educational_Attainment__c = 'Bachelor degree',
            Parent_2_Highest_Educational_Attainment__c = 'Completed Year 12 schooling or equivalent',
            RecordTypeId = RecordTypeUtils.getRecordTypeId(Constants.RECORDTYPE_OPPORTUNITY_ACQUISITION, SObjectType.Opportunity)
        );

        if (String.isEmpty(stageName))
            opp.StageName = 'Applying';
        else
            opp.StageName = stageName;

        if (contact != null)
            opp.Contact__c = contact.Id;

        if (campaign != null)
        {
            opp.CampaignId = campaign.Id;
            opp.CloseDate = campaign.TermCensusDate__c;
        }

        if (save)
        {
            System.RunAs(user)
            {
                insert opp;
            }
        }

        return opp;
    }

    public static Opportunity createApplyingAcquisition(User user, Contact contact)
    {
        return create(user, contact, 'Applying', true);
    }

    public static Opportunity createCommencing(User user, Contact contact)
    {
        Opportunity opp =  create(user, contact, 'Commencing', true);

        return opp;
    }

    public static Opportunity createProgression(User user, Contact contact, Campaign campaign)
    {
        Opportunity opp = new Opportunity
        (
            OwnerId = user.Id,

            OAF_Submitted__c = false,
            CloseDate = System.today(),
            Name = 'Test Opp ' + System.now(),
            Payment_Method__c = 'Self Funded',
            StageName = 'Applying',

            RecordTypeId = RecordTypeUtils.getRecordTypeId(Constants.RECORDTYPE_OPPORTUNITY_PROGRESSION, SObjectType.Opportunity)
        );

        if (campaign == null)
            campaign = TestDataCampaignMock.create(user, Date.today().addDays(56), 'Domestic', 'MBA', true);

        opp.CampaignId = campaign.Id;
        opp.CloseDate = campaign.TermCensusDate__c;

        if (contact != null)
            opp.Contact__c = contact.Id;

        System.RunAs(user)
        {
            insert opp;
        }

        return opp;
    }
}