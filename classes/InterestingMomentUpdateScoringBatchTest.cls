@isTest
public class InterestingMomentUpdateScoringBatchTest 
{
    @testSetup 
    static void setup()
    {
        TestUtilities.setupHEDA();
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact contact = TestDataContactMock.createProspectCold(user, '040411111122');
        SalesScoreConfig__c config = TestDataSalesScoreConfigMock.create(user);
    }

    @isTest 
    static void NoSalesScoreConfigData_BatchJobNotRun() 
    {
        // Act
        List<SalesScoreConfig__c> configs = [SELECT Id FROM SalesScoreConfig__c];

        Test.startTest();
        delete configs;

        InterestingMomentUpdateScoringBatch batch = new InterestingMomentUpdateScoringBatch();
        ID batchProcessId = Database.executeBatch(batch, 5);
        Test.stopTest();

        // Assert
        System.assertNotEquals(null, batchProcessId, 'Unexpected batch process ID');
    }

    @isTest 
    static void SalesScoreConfigDataChanged_InterestingMomentBehaviourScoreUpdated() 
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact contact = [SELECT Id FROM Contact LIMIT 1];
        SalesScoreConfig__c config = [SELECT Id, ScoreValue__c FROM SalesScoreConfig__c LIMIT 1];
        config.ScoreValue__c = 50;
        InterestingMoment__c moment = TestDataInterestingMomentMock.create(user, contact);

        // Act
        update config;
        
        Test.startTest();
        InterestingMomentUpdateScoringBatch batch = new InterestingMomentUpdateScoringBatch();
        ID batchProcessId = Database.executeBatch(batch, 5);
        Test.stopTest();

        InterestingMoment__c updatedMoment = [SELECT Id, BehaviourScore__c FROM InterestingMoment__c WHERE Id = :moment.Id];

        // Assert
        System.assertEquals(50, updatedMoment.BehaviourScore__c, 'Unexpected behaviour score value');
    }

    @isTest 
    static void SalesScoreConfigDataNotChanged_InterestingMomentBehaviourScoreNotUpdated() 
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact contact = [SELECT Id FROM Contact LIMIT 1];
        
        InterestingMoment__c createdMoment = TestDataInterestingMomentMock.create(user, contact);
        createdMoment.BehaviourScore__c = 5;
        
        // Act
        update createdMoment;

        Test.startTest();
        InterestingMomentUpdateScoringBatch batch = new InterestingMomentUpdateScoringBatch();
        ID batchProcessId = Database.executeBatch(batch, 5);
        Test.stopTest();

        InterestingMoment__c moment = [SELECT Id, BehaviourScore__c FROM InterestingMoment__c WHERE Id = :createdMoment.Id];

        // Assert
        System.assertEquals(createdMoment.BehaviourScore__c, moment.BehaviourScore__c, 'Unexpected behaviour score value');
    }
}