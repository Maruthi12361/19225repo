@isTest
public class CaseLastInteractionBatchTest
{
    @testSetup
    static void setup()
    {
        TestUtilities.setupHEDA();
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createProspectHot(user);
    }

    @isTest
    static void CaseLastInteractionBatch_LastInteractionForClosedCases()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = [SELECT id FROM Contact LIMIT 1];
        Case newCase = TestDataCaseMock.createStudentSupportCase(user, contact, false);
        Datetime dateNowGMT = System.now(); // GMT timezone
        Timezone tz = UserInfo.getTimeZone();
        Datetime local = dateNowGMT.addSeconds(Integer.valueOf(DateTimeUtils.getUTCOffsetHours(tz, dateNowGMT) * 60 * 60)); //local timezone
        newCase.Latest_Interaction_Date_Time__c = local.addDays(-1);
        newCase.Status = 'Closed';
        insert newCase;

        // Act
        test.startTest();
        String query = 'SELECT Id, Latest_Interaction_Date_Time__c, ClosedDate, Last_Interaction_Till_Yesterday__c FROM Case WHERE Status=\'Closed\' ';
        ID batchprocessid = Database.executeBatch(new CaseLastInteractionBatch(query));
        Decimal expected = DateTimeUtils.getTimeAsHours(DateTimeUtils.getBusinessElapsedTime(local.addDays(-1), System.now()));
        test.stopTest();

        // Assert
        Case actual = [SELECT Id, Latest_Interaction_Date_Time__c, ClosedDate, Last_Interaction_Till_Yesterday__c FROM Case WHERE Id =: newCase.Id];
        System.assertNotEquals(null, actual.Last_Interaction_Till_Yesterday__c, 'No calculation of the Business Hours for the Last_Interaction_Till_Yesterday__c has occured.');
        // Intermittently, the value of expected won't be exactly the same as actual.Last_Interaction_Till_Yesterday__c because expected is calculated based on the returned value of System.now()
        // after executing the batch. Therefore, we cannot assume expected is always equal to actual.Last_Interaction_Till_Yesterday__c. Instead, we just check if the delta is within an acceptable range.
        Decimal delta = Math.abs(expected - actual.Last_Interaction_Till_Yesterday__c);
        System.assertEquals(true, (delta >= 0 && delta < 0.05), 'Incorrect calculation of Last_Interaction_Till_Yesterday__c, the delta is ' + delta);
    }

    @isTest
    static void CaseLastInteractionBatch_LastInteractionForOpenCases()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = [SELECT id FROM Contact LIMIT 1];
        Case newCase = TestDataCaseMock.createStudentSupportCase(user, contact, false);
        Datetime dateNowGMT = System.now(); // GMT timezone
        Timezone tz = UserInfo.getTimeZone();
        Datetime local = dateNowGMT.addSeconds(Integer.valueOf(DateTimeUtils.getUTCOffsetHours(tz, dateNowGMT) * 60 * 60)); //local timezone
        newCase.Latest_Interaction_Date_Time__c = local.addDays(-1);
        insert newCase;

        // Act
        test.startTest();
        String query = 'SELECT Id, Latest_Interaction_Date_Time__c, ClosedDate, Last_Interaction_Till_Yesterday__c FROM Case WHERE Status != \'Closed\' ';
        ID batchprocessid = Database.executeBatch(new CaseLastInteractionBatch(query));
        Decimal expected = DateTimeUtils.getTimeAsHours(DateTimeUtils.getBusinessElapsedTime(local.addDays(-1), System.now()));
        test.stopTest();

        // Assert
        Case actual = [SELECT Id, Latest_Interaction_Date_Time__c, ClosedDate, Last_Interaction_Till_Yesterday__c FROM Case WHERE Id =: newCase.Id];
        System.assertNotEquals(null, actual.Last_Interaction_Till_Yesterday__c, 'No calculation of the Business Hours for the Last_Interaction_Till_Yesterday__c occur has occured.');
        // Intermittently, the value of expected won't be exactly the same as actual.Last_Interaction_Till_Yesterday__c because expected is calculated based on the returned value of System.now()
        // after executing the batch. Therefore, we cannot assume expected is always equal to actual.Last_Interaction_Till_Yesterday__c. Instead, we just check if the delta is within an acceptable range.
        Decimal delta = Math.abs(expected - actual.Last_Interaction_Till_Yesterday__c);
        System.assertEquals(true, (delta >= 0 && delta < 0.05), 'Incorrect calculation of Last_Interaction_Till_Yesterday__c, the delta is ' + delta);
    }
}