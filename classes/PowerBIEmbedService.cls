public with sharing class PowerBIEmbedService
{
    // Org Cache Partition details
    private final String PARTITION = 'local.OrgCache';
    private final String ACCESS_TOKEN = 'AccessToken';
    private final String EXPIRES_ON = 'ExpiresOn';

    // Always points to Power BI Production, in production and sandboxes
    // it is read-only permission to access Power BI reports
    private final String PBI_OAUTH = 'Production';

    public String accessToken {get; set;}
    public String expiresOn {get; set;}
    public String mappedObject {get; set;}
    public Id reportFilterId {get; set;}
    public String embedURL {get; set;}
    public String filterTable {get;set;}
    public String filterColumn {get;set;}
    public String defaultPage {get;set;}
    public String selectedReport {get;set;}
    public List<SelectOption> reportList {get;set;}
    public Boolean isReportSelected {get;set;}
    public String vfPageName {get;set;}

    @TestVisible
    private Cache.OrgPartition orgCache
    {
        get
        {
            if (orgCache == null)
            {
                orgCache = Cache.Org.getPartition(PARTITION);
            }
            return orgCache;
        }
        set;
    }

    public PowerBIEmbedOAuth__mdt pbiOauth
    {
        get
        {
            if (pbiOauth == null)
            {
                pbiOauth = [SELECT Id
                        , DeveloperName
                        , ClientID__c
                        , ClientSecret__c
                        , ResourceURI__c
                        FROM PowerBIEmbedOAuth__mdt
                        WHERE DeveloperName = :PBI_OAUTH];
            }
            return pbiOauth;
        }
        set;
    }

    public Map<String, PowerBIEmbedMapping__mdt> pbiMapping
    {
        get
        {
            if (pbiMapping == null && mappedObject != null)
            {
                pbiMapping = new Map<String, PowerBIEmbedMapping__mdt>();
                for (PowerBIEmbedMapping__mdt m : [SELECT Id
                                                        , Label
                                                        , DeveloperName
                                                        , GroupID__c
                                                        , ReportID__c
                                                        , EmbedURLFormat__c
                                                        , VisualforcePageName__c
                                                        , Default_Page__c
                                                        , Filter_Table__c
                                                        , Filter_Column__c
                                                        FROM PowerBIEmbedMapping__mdt
                                                        WHERE ObjectAPI__c = :mappedObject])
                {
                    pbiMapping.put(m.DeveloperName, m);
                }
            }
            return pbiMapping;
        }
        set;
    }

    public PowerBIEmbedService(ApexPages.StandardController stdController)
    {
        reportFilterId = (Id) stdController.getRecord().Id;
        mappedObject = (String) stdController.getRecord().getSObjectType().getDescribe().getName();
        vfPageName = ApexPages.currentPage().getUrl().substringBetween('apex/', '?');
        isReportSelected = false;
        loadReportList();
    }

    public void loadReportList()
    {
        reportList = new List<SelectOption>();
        reportList.add(new SelectOption('Select', '-- Select --'));

        List<String> embedVfPageList = new List<String>();

        for (String reportName : pbiMapping.keySet())
        {
            if (pbiMapping.get(reportName).VisualforcePageName__c != null)
            {
                embedVfPageList = pbiMapping.get(reportName).VisualforcePageName__c.split(';');

                if (!embedVfPageList.isEmpty())
                {
                    for (String embedVfPage : embedVfPageList)
                    {
                        if (vfPageName == embedVfPage)
                        {
                            reportList.add(new SelectOption(reportName, pbiMapping.get(reportName).Label));
                        }
                    }
                }
            }
        }
    }

    public void loadSelectedReport()
    {
        if (selectedReport != 'Select')
        {
            isReportSelected = true;
            PowerBIEmbedMapping__mdt selectedBiReport = pbiMapping.get(selectedReport);
            embedURL = String.format(selectedBiReport.EmbedURLFormat__c, new String[] {selectedBiReport.GroupID__c, selectedBiReport.ReportID__c});
            defaultPage = selectedBiReport.Default_Page__c;
            filterTable = selectedBiReport.Filter_Table__c;
            filterColumn = selectedBiReport.Filter_Column__c;
        }
    }

    public void storeToken()
    {
        // Store expiresOn value and check if token expired
        Long expOn = 0;

        if (orgCache.get(EXPIRES_ON) != null)
        {
            expOn = Integer.valueOf(orgCache.get(EXPIRES_ON));
        }

        // Retrieve access token for first access or when token expired
        if ((expOn - 2000) < DateTime.now().getTime()/1000)
        {
            retrieveToken();
        }

        if (orgCache.get(ACCESS_TOKEN) != null && orgCache.get(EXPIRES_ON) != null)
        {
            accessToken = (String) orgCache.get(ACCESS_TOKEN);
            expiresOn = (String) orgCache.get(EXPIRES_ON);
        }
    }

    public void retrieveToken()
    {
        List<String> urlParams = new List<String>
        {
            'grant_type=password',
            'scope=openid',
            'client_id=' + EncodingUtil.urlEncode(pbiOauth.ClientID__c, 'UTF-8'),
            'client_secret=' + EncodingUtil.urlEncode(pbiOauth.ClientSecret__c, 'UTF-8'),
            'resource=' + EncodingUtil.urlEncode(pbiOauth.ResourceURI__c, 'UTF-8'),
            'username=' + '{!$Credential.UserName}',
            'password=' + '{!$Credential.Password}'
        };
        String body = String.join(urlParams, '&');

        Http http = new Http();
        HttpRequest request = new HttpRequest();
        HttpResponse response = null;

        // Callout endpoint's URL and authentication settings from Named Credential
        request.setEndpoint('callout:PowerBIEmbed');
        request.setMethod(Constants.HTTPMETHOD_POST);
        request.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        request.setHeader('Accept', 'application/json');
        request.setTimeout(120000);
        request.setBody(body);

        Log.info('PowerBIEmbedService', 'retrieveToken', 'Retrieving access token from Power BI.', request, new String[] {'Accept', 'Content-Type'});

        response = http.send(request);

        if (response.getStatusCode() != 200)
        {
            Log.error('PowerBIEmbedService', 'retrieveToken', 'Error retrieving access token. Error Code: ' + response.getStatusCode());
            return;
        }

        Map<String, Object> oauthResult = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());

        accessToken = (String) oauthResult.get('access_token');
        expiresOn = (String) oauthResult.get('expires_on');

        orgCache.put(ACCESS_TOKEN, accessToken);
        orgCache.put(EXPIRES_ON, expiresOn);
    }
}