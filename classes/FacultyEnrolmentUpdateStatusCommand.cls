public class FacultyEnrolmentUpdateStatusCommand extends BaseComponent
{
    private Boolean validate(Object parameter)
    {
        ViewModels.Param param = (ViewModels.Param) parameter;

        if (String.isEmpty(param.Identifier))
        {
            Log.error(this.className, 'validate', 'Identifier parameter is null');
            throw new Exceptions.InvalidParameterException('Identifier parameter cannot be empty or null');
        }
        else if (String.isEmpty(param.Payload))
        {
            Log.error(this.className, 'validate', 'Payload parameter is null');
            throw new Exceptions.InvalidParameterException('Subject Enrolment payload cannot be empty or null');
        }
        else
        {
            Map<String, Object> fieldsMap = (Map<String, Object>)JSON.deserializeUntyped(param.Payload);

            if (!fieldsMap.containsKey('Id'))
            {
                Log.error(this.className, 'validate', 'Missing Id field in payload');
                throw new Exceptions.InvalidParameterException('Missing Id field');
            }
            else if (!fieldsMap.containsKey('IsAvailable'))
            {
                Log.error(this.className, 'validate', 'Missing IsAvailable field in payload');
                throw new Exceptions.InvalidParameterException('Missing IsAvailable field');
            }
            else
            {
                String subjectEnrolmentId = (String) fieldsMap.get('Id');
                Contact contact = getFacultyContact(param.Identifier, subjectEnrolmentId);

                if (contact == null)
                {
                    Log.error(this.className, 'validate', 'No faculty contact found with {0} encrypted ID.', new List<String> { param.Identifier });
                    throw new Exceptions.InvalidParameterException('No faculty contact found');
                }
                else if (contact.hed__Student_Course_Enrollments__r.size() != 1)
                {
                    Log.error(this.className, 'validate', 'No subject enrolment {0} found for contact {1}', new List<String> { subjectEnrolmentId, param.Identifier } );
                    throw new Exceptions.InvalidParameterException('Subject enrolment not found');
                }
            }
        }

        return true;
    }

    public ActionResult command(Object parameter)
    {
        validate(parameter);

        ViewModels.Param param = (ViewModels.Param) parameter;
        Map<String, Object> fieldsMap = (Map<String, Object>)JSON.deserializeUntyped(param.Payload);
        String subjectEnrolmentId = (String) fieldsMap.get('Id');
        hed__Course_Enrollment__c subjectEnrolment = getSubjectEnrolment(subjectEnrolmentId);

        if (subjectEnrolment.hed__Status__c != 'Requested')
        {
            Log.info(this.className, 'command', 'Enrolment status not in Requested, status not updated');
            return new ActionResult(ResultStatus.SUCCESS);
        }

        Boolean isAvailable = Boolean.valueOf(fieldsMap.get('IsAvailable'));

        if (isAvailable)
            subjectEnrolment.hed__Status__c = 'Enrolled';
        else
            subjectEnrolment.hed__Status__c = 'Rejected';

        Database.update(subjectEnrolment);

        return new ActionResult(ResultStatus.SUCCESS);
    }

    private Contact getFacultyContact(String encryptedId, String subjectEnrolmentId)
    {
        List<Contact> contacts = 
        [
            SELECT
                Id, Name, EncryptedId__c, (SELECT Id FROM hed__Student_Course_Enrollments__r WHERE Id = :subjectEnrolmentId)
            FROM Contact
            WHERE EncryptedId__c = :encryptedId AND RecordType.DeveloperName = :Constants.RECORDTYPE_CONTACT_FACULTY
        ];

        if (contacts.size() > 0)
            return contacts[0];

        return null;
    }

    private hed__Course_Enrollment__c getSubjectEnrolment(String Id)
    {
        return
        [
            SELECT 
                Id, hed__Status__c
            FROM hed__Course_Enrollment__c
            WHERE Id = :Id
        ];
    }
}