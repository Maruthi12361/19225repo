global class ContactBatchRolloverDetailSchedule implements Schedulable
{
    global final String query;
    
    global ContactBatchRolloverDetailSchedule() 
    {        
    }
    
    global ContactBatchRolloverDetailSchedule(String stringParam) 
    {
        if (string.isNotBlank(stringParam)) 
            query = stringParam;
    }
    
    global void execute(SchedulableContext sc) 
	{
		ContactResetRolloverDetailBatch batch;
        
        if (query == null || String.isBlank(query))
            batch = new ContactResetRolloverDetailBatch();
        else
            batch = new ContactResetRolloverDetailBatch(query);
		database.executebatch(batch);
	}
}