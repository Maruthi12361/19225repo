public class NVMCallSummaryService
{
    public static ActionResult setTaskLookup(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        return new NVMCallSummarySetTaskLookupCommand().execute(items, oldMap, triggerContext);
    }
}