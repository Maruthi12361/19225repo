public class SystemService
{
    public static ActionResult runSystemHealthCheck(List<QueryResult> items)
    {
        return new SystemHealthCheckCommand().execute(items);
    }
}