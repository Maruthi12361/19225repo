@IsTest
public class ContactSaveInterestingMomentCommandTest
{
    @testSetup
    public static void setup()
    {
        TestUtilities.setupHEDA();
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact contact = TestDataContactMock.createProspectCold(user, '0455548889');
    }

    @isTest
    static void BlankIdentifier_EmptyIdentifierError()
    {
        // Arrange
        String errorMessage = '';
        ViewModels.Param param = new ViewModels.Param();
        param.Identifier = '';
        param.Payload = null;

        // Act
        try
        {
           ActionResult result = new ContactSaveInterestingMomentCommand().command(param);
        }
        catch (Exception ex)
        {
           errorMessage = ex.getMessage();
        }

        // Assert
        System.assertEquals(String.format(Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Identifier' }), errorMessage, 'Unexpected error message');
    }

    @isTest
    static void BlankPayload_EmptyPayloadError()
    {
        // Arrange
        String errorMessage = '';
        ViewModels.Param param = new ViewModels.Param();
        param.Identifier = 'myemail@email.com';
        param.Payload = null;

        // Act
        try
        {
           ActionResult result = new ContactSaveInterestingMomentCommand().command(param);
        }
        catch (Exception ex)
        {
           errorMessage = ex.getMessage();
        }

        // Assert
        System.assertEquals(String.format(Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Payload' }), errorMessage, 'Unexpected error message');
    }

    @isTest
    static void NonExistantContact_NoContactError()
    {
        // Arrange
        String errorMessage = '';
        Map<String, Object> momentMap = new Map<String, Object>
        {
            'Source__c' => 'Google',
            'Type__c' => 'Web Page Access',
            'EventDate__c' => '2018-08-18T14:30:00.000Z'
        };

        ViewModels.Param param = new ViewModels.Param();
        param.Identifier = 'myemail@email.com';
        param.Payload = JSON.serialize(momentMap);

        // Act
        try
        {
           ActionResult result = new ContactSaveInterestingMomentCommand().command(param);
        }
        catch (Exception ex)
        {
           errorMessage = ex.getMessage();
        }

        // Assert
        System.assert(errorMessage.contains('Contact does not exist'), 'Unexpected error message');
    }

    @isTest
    static void NewInterestingMoment_CreatedSuccessfully()
    {
        // Arrange
        List<Object> payload = new List<Object>();
        Contact contact = [SELECT Id, PersonalEmail__c FROM Contact LIMIT 1];
        Map<String, Object> momentMap = new Map<String, Object>
        {
            'Source__c' => 'Google',
            'Type__c' => 'Web Page Access',
            'EventDate__c' => '2018-08-18T14:30:00.000Z'
        };
        payload.add((Object) JSON.deserializeUntyped(JSON.serialize(momentMap)));

        ViewModels.Param param = new ViewModels.Param();
        param.Identifier = contact.PersonalEmail__c;
        param.Payload = JSON.serialize(payload);

        // Act
        ActionResult result = new ContactSaveInterestingMomentCommand().command(param);
        InterestingMoment__c moment = [SELECT Id, Source__c, Type__c, ContactID__c FROM InterestingMoment__c WHERE ContactID__c = :contact.Id];

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Unexpected result status value');
        System.assertEquals(contact.Id, moment.ContactID__c, 'Unexpected contact id');
        System.assertEquals('Google', moment.Source__c, 'Unexpected interesting moment source');
        System.assertEquals('Web Page Access', moment.Type__c, 'Unexpected interesting moment type');
    }

    @isTest
    static void NewInterestingMomentEcryptedId_CreatedSuccessfully()
    {
        // Arrange
        List<Object> payload = new List<Object>();
        Contact contact = [SELECT Id FROM Contact LIMIT 1];
        Map<String, Object> momentMap = new Map<String, Object>
        {
            'Source__c' => 'Google',
            'Type__c' => 'Web Page Access',
            'EventDate__c' => '2018-08-18T14:30:00.000Z'
        };
        payload.add((Object) JSON.deserializeUntyped(JSON.serialize(momentMap)));

        ViewModels.Param param = new ViewModels.Param();
        param.Identifier = EncryptionUtil.Encrypt(contact.Id);
        param.Payload =  JSON.serialize(payload);

        // Act
        ActionResult result = new ContactSaveInterestingMomentCommand().command(param);
        InterestingMoment__c moment = [SELECT Id, Source__c, Type__c, ContactID__c FROM InterestingMoment__c WHERE ContactID__c = :contact.Id];

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Unexpected result status value');
        System.assertEquals(contact.Id, moment.ContactID__c, 'Unexpected contact id');
        System.assertEquals('Google', moment.Source__c, 'Unexpected interesting moment source');
        System.assertEquals('Web Page Access', moment.Type__c, 'Unexpected interesting moment type');
    }

    @isTest
    static void NewInterestingInvalidField_InvalidFieldError()
    {
        // Arrange
        String errorMsg = '';
        List<Object> payload = new List<Object>();
        Contact contact = [SELECT Id, PersonalEmail__c FROM Contact LIMIT 1];
        Map<String, Object> momentMap = new Map<String, Object>
        {
            'Source__c' => 'Google',
            'Typppe__c' => 'Web Page Access',
            'EventDate__c' => '2018-08-18T14:30:00.000Z'
        };
        payload.add((Object) JSON.deserializeUntyped(JSON.serialize(momentMap)));

        ViewModels.Param param = new ViewModels.Param();
        param.Identifier = contact.PersonalEmail__c;
        param.Payload = JSON.serialize(payload);

        // Act
        try
        {
            ActionResult result = new ContactSaveInterestingMomentCommand().command(param);
        }
        catch(Exception ex)
        {
            errorMsg = ex.getMessage();
        }

        // Assert
        System.assertNotEquals('', errorMsg, 'Unexpected error message');
    }

    @isTest
    static void NewInterestingMomentNoValues_InterestingMomentNotCreated()
    {
        // Arrange
        String errorMsg = '';
        List<Object> payload = new List<Object>();
        Contact contact = [SELECT Id, PersonalEmail__c FROM Contact LIMIT 1];
        Map<String, Object> momentMap = new Map<String, Object>
        {
            'Source__c' => '',
            'Type__c' => '',
            'EventDate__c' => ''
        };
        payload.add((Object) JSON.deserializeUntyped(JSON.serialize(momentMap)));

        ViewModels.Param param = new ViewModels.Param();
        param.Identifier = contact.PersonalEmail__c;
        param.Payload = JSON.serialize(payload);

        // Act
        ActionResult result = new ContactSaveInterestingMomentCommand().command(param);
        List<InterestingMoment__c> moments = [SELECT Id FROM InterestingMoment__c WHERE ContactID__c = :contact.Id];

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Unexpected result status value');
        System.assertEquals(0, moments.size(), 'Interesting moment should not be created');
    }

    @isTest
    static void NewInterestingMomentWithSalesScoreRuleMatch_CreatedSuccessfullyWithBehaviourScoreSet()
    {
        // Arrange
        List<Object> payload = new List<Object>();
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        SalesScoreConfig__c config = TestDataSalesScoreConfigMock.create(user);
        Contact contact = [SELECT Id, PersonalEmail__c FROM Contact LIMIT 1];
        Map<String, Object> momentMap = new Map<String, Object>
        {
            'Source__c' => 'Google',
            'Type__c' => 'Web Page Access',
            'EventDate__c' => '2018-08-18T14:30:00.000Z'
        };
        payload.add((Object) JSON.deserializeUntyped(JSON.serialize(momentMap)));

        ViewModels.Param param = new ViewModels.Param();
        param.Identifier = contact.PersonalEmail__c;
        param.Payload = JSON.serialize(payload);

        // Act
        ActionResult result = new ContactSaveInterestingMomentCommand().command(param);
        InterestingMoment__c moment = [SELECT Id, Source__c, Type__c, ContactID__c, BehaviourScore__c FROM InterestingMoment__c WHERE ContactID__c = :contact.Id];

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Unexpected result status value');
        System.assertEquals(contact.Id, moment.ContactID__c, 'Unexpected contact id');
        System.assertEquals('Google', moment.Source__c, 'Unexpected interesting moment source');
        System.assertEquals('Web Page Access', moment.Type__c, 'Unexpected interesting moment type');
        System.assertEquals(config.ScoreValue__c, moment.BehaviourScore__c, 'Unexpected BehaviourScore__c value');
    }
}