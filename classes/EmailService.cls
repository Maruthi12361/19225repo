public class EmailService
{
    public static void send(String toAddress, String subject, String body)
    {
        send(new String[] { toAddress }, subject, body);
    }

    public static void send(String[] toAddresses, String subject, String body)
    {
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();

        if (UnitOfWork.current == null)
            throw new Exceptions.ApplicationException('A Unit of Work has not been established to send this message through');

        Log.info('EmailService', 'send', 'Queuing email with subject line: ' + subject);

        email.setToAddresses(toAddresses);
        email.setSubject(subject);
        email.setPlainTextBody(body);

        UnitOfWork.current.registerEmail(email);
    }

    public static void send(String toAddress, String subject, String body, String senderName, String replyAddress)
    {
        send(new String[] { toAddress }, subject, body, senderName, replyAddress);
    }

    public static void send(String[] toAddresses, String subject, String body, String senderName, String replyAddress)
    {
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();

        if (UnitOfWork.current == null)
            throw new Exceptions.ApplicationException('A Unit of Work has not been established to send this message through');

        Log.info('EmailService', 'send', 'Queuing email with subject line: ' + subject);

        email.setToAddresses(toAddresses);
        email.setReplyTo(replyAddress);
        email.setSenderDisplayName(senderName);
        email.setSubject(subject);
        email.setBccSender(false);
        email.setUseSignature(false);
        email.setHtmlBody(body);

        UnitOfWork.current.registerEmail(email);
    }

    public static void sendLegacy(String toAddress, String subject, String body)
    {
        Boolean uowScope = false;

        if (UnitOfWork.current == null)
        {
            uowScope = true;
            UnitOfWork.current = new UnitOfWork(new Schema.SObjectType[] {});
        }

        send(new String[] { toAddress }, subject, body);

        if (uowScope)
        {
            UnitOfWork.current.commitWork();
            UnitOfWork.current = null;
            uowScope = false;
        }
    }

    public static void sendLegacy(String toAddress, String subject, String body, String senderName, String replyAddress)
    {
        Boolean uowScope = false;

        if (UnitOfWork.current == null)
        {
            uowScope = true;
            UnitOfWork.current = new UnitOfWork(new Schema.SObjectType[] {});
        }

        send(new String[] { toAddress }, subject, body, senderName, replyAddress);

        if (uowScope)
        {
            UnitOfWork.current.commitWork();
            UnitOfWork.current = null;
            uowScope = false;
        }
    }

    public static EmailTemplate getEmailTemplate(String templateName)
    {
        return new EmailTemplateGetQuery().query(templateName);
    }

    public static ActionResult sendToRecipient(String emailProvider, String toAddress, String toName, String subject, String fromAddress, String fromName, String bodyText, String bodyHtml, List<ViewModelsMail.Recipient> ccEmail)
    {
        String provider = emailProvider;

        if (provider == Constants.APIPROVIDER_DEFAULT && String.isNotBlank(Label.EmailProvider))
            provider = Label.EmailProvider;

        if (provider == Constants.APIPROVIDER_SENDGRID)
            return sendViaSendGrid(toAddress, toName, subject, fromAddress, fromName, bodyText, bodyHtml, ccEmail);
        else if (provider == Constants.APIPROVIDER_MARKETINGCLOUD)
            return sendViaMarketingCloud(toAddress, toName, subject, fromAddress, fromName, bodyText, bodyHtml, ccEmail);

        return sendViaSendGrid(toAddress, toName, subject, fromAddress, fromName, bodyText, bodyHtml, ccEmail);
    }

    private static ActionResult sendViaSendGrid(string toAddress, String toName, String subject, String fromAddress, String fromName, String bodyText, String bodyHtml, List<ViewModelsMail.Recipient> ccEmail )
    {
        ViewModelsMail.Mail mail = new ViewModelsMail.Mail(toAddress, toName, subject, fromAddress, fromName, bodyText, bodyHtml, ccEmail);

        return new EmailSendViaSendGridCommand().execute(new List<ViewModelsMail.Mail> {mail});
    }

    private static ActionResult sendViaMarketingCloud(String toAddress, String toName, String subject, String fromAddress, String fromName, String bodyText, String bodyHtml,  List<ViewModelsMail.Recipient> ccEmail)
    {
        ViewModelsMail.Email mail = new ViewModelsMail.Email(null, subject, bodyText, bodyHtml, toAddress, toName, fromName, fromAddress, ccEmail);

        return new EmailSendViaMarketingCloudCommand().execute(new List<ViewModelsMail.Email> {mail}); 
    }
}