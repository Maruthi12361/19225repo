public class InterestingMomentUpdateStatusCommand extends BaseTriggerCommand
{
    protected override List<SObject> validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        List<InterestingMoment__c> result = new List<InterestingMoment__c>();

        for (InterestingMoment__c item : (List<InterestingMoment__c>)items)
        {
            if (triggerContext == Enums.TriggerContext.AFTER_INSERT && item.ContactID__c != null && item.Type__c != null && item.Source__c != null)
            {
                if (item.Type__c == 'Form Completed' && item.Source__c == 'Online Application Form')
                    result.add(item);
            }
        }

        return result;
    }

    protected override ActionResult command(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        Set<Id> contactIds = new Set<Id>();

        for (InterestingMoment__c item : (List<InterestingMoment__c>)items)
        {
            contactIds.add(item.ContactID__c);
        }

        if (contactIds.size() > 0)
        {
            Map<Id, Opportunity> opportunityMap = new Map<Id, Opportunity>();
            List<Opportunity> opportunities =
                [
                    SELECT Id, Contact__c, StageName
                    FROM Opportunity
                    WHERE Contact__c IN : contactIds AND IsClosed = false AND RecordType.DeveloperName =: Constants.RECORDTYPE_OPPORTUNITY_ACQUISITION AND StageName IN ('Applying')
                    ORDER BY Contact__c, LastModifiedDate DESC
                ];

            for (Opportunity opportunity : opportunities)
            {
                if (opportunityMap.containsKey(opportunity.Contact__c))
                    continue;

                opportunity.StageName = 'Vetting';
                opportunityMap.put(opportunity.Contact__c, opportunity);
            }

            if (opportunityMap.size() > 0)
                uow.registerDirty(opportunityMap.values());
        }

        return new ActionResult();
    }
}