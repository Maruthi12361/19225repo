public class CampaignUpdateStatusBatch extends BaseBatchCommand
{
    String query = 'SELECT Id, Type, Program_Portfolio__c, Status, TermStartDate__c, TermLookup__r.Status__c ' +
                   'FROM Campaign ' +
                   'WHERE RecordType.DeveloperName = \'TermIntake\' AND Status NOT IN (\'Aborted\', \'Completed\') AND TermLookup__c != null AND TermStartDate__c != null AND IsActive = True ' +
                   'ORDER BY Type, Program_Portfolio__c, TermStartDate__c';

    public CampaignUpdateStatusBatch()
    {
    }

    public override Database.QueryLocator query(Database.BatchableContext context)
    {
        return Database.getQueryLocator(query);
    }

    public override void command(Database.BatchableContext context, List<sObject> scope)
    {
        List<Campaign> updatedCampaigns = new List<Campaign>();
        Map<String, List<Campaign>> campMap = new Map<String, List<Campaign>>();

        for (Campaign camp : (List<Campaign>) scope)
        {
            String key = camp.Type + camp.Program_Portfolio__c;
            List<Campaign> camps = campMap.get(key);

            if (camps == null)
            {
                camps = new List<Campaign>();
                campMap.put(key, camps);
            }

            camps.add(camp);
        }

        for (String key : campMap.keySet())
        {
            List<Campaign> camps = campMap.get(key);
            Boolean isFirstFutureTerm = true;

            for (Campaign camp : camps)
            {
                String status;

                if (camp.TermLookup__r.Status__c == 'Future')
                {
                    if (isFirstFutureTerm)
                    {
                        isFirstFutureTerm = false;
                        status = 'Next';
                    }
                    else
                        status = 'Planned';
                }
                else if (camp.TermLookup__r.Status__c == 'Next')
                    status = 'In Progress';
                else if (camp.TermLookup__r.Status__c == 'Current')
                    status = 'Previous';
                 if (camp.TermLookup__r.Status__c == 'Previous' || camp.TermLookup__r.Status__c == 'Past')
                    status = 'Completed';

                if (status != camp.Status)
                {
                    camp.Status = status;
                    updatedCampaigns.add(camp);
                }
            }
        }

        if (updatedCampaigns.size() > 0)
            update updatedCampaigns;
    }
}