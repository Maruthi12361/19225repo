@isTest
public class SubjectEnrolmentCalculateCommandTests
{
    @testSetup
    static void setup()
    {
        TestUtilities.setupHEDA();
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Account account = TestDataAccountMock.create(user, 'Test AIB Account');
        hed__Term__c term = TestDataTermMock.create(user, account.Id, Date.today());
        hed__Course__c subjectDefinition = TestDataSubjectMock.createDefinitionCGOV(user);
        hed__Course__c subjectVersion = TestDataSubjectMock.createVersionCGOV(user);

        TestDataContactMock.createFacultyContact(user);
        TestDataSubjectOfferingMock.createOffering(user, subjectVersion, term);
        TestDataAccountMock.createCourseMBA(user);
        TestDataContactMock.createStudent(user, 'A001648586', 'A001648586@study.aib.edu.au', true, false);
    }

    @isTest
    static void StudentSubjectEnrolmentCreated_SubjectOfferingValuesCalculatedSuccessfully()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact student =  [SELECT Id, Cerebro_Student_ID__c FROM Contact WHERE Cerebro_Student_ID__c = 'A001648586' LIMIT 1];
        Account mbaCourse = [SELECT Id, Program__c FROM Account WHERE Program__c = 'MBA'];
        hed__Course_Offering__c subjectOffering = [SELECT Id FROM hed__Course_Offering__c LIMIT 1];
        hed__Program_Enrollment__c courseEnrolment = TestDataCourseEnrolmentMock.create(user, mbaCourse.Id, student.Id, 'Ongoing');

        // Act
        test.startTest();
        TestDataSubjectEnrolmentMock.create(user, subjectOffering, courseEnrolment, 'Enrolled', 'Standard', null, null);
        hed__Course_Offering__c updatedSubjectOffering =
        [
            SELECT Id, ActualStudents__c, ProjectedStudents__c, hed__Capacity__c, ProjectedCapacity__c, MaximumCapacity__c
            FROM hed__Course_Offering__c
            WHERE Id = :subjectOffering.Id
        ];
        test.stopTest();

        // Assert
        System.assertEquals(1, updatedSubjectOffering.ActualStudents__c, 'Unexpected actual student value');
        System.assertEquals(1, updatedSubjectOffering.ProjectedStudents__c, 'Unexpected projected student value');
    }

    @isTest
    static void FacultySubjectEnrolmentCreated_SubjectOfferingValuesCalculatedSuccessfully()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact coordinator = TestDataContactMock.createFacultyContact(user);
        hed__Course_Offering__c subjectOffering = [SELECT Id FROM hed__Course_Offering__c LIMIT 1];

        // Act
        test.startTest();
        TestDataSubjectEnrolmentMock.createFacultyEnrolment(user, subjectOffering, coordinator, 'Enrolled', 'Subject Coordinator');
        hed__Course_Offering__c updatedSubjectOffering =
        [
            SELECT Id, ActualStudents__c, ProjectedStudents__c, hed__Capacity__c, ProjectedCapacity__c, MaximumCapacity__c
            FROM hed__Course_Offering__c
            WHERE Id = :subjectOffering.Id
        ];
        test.stopTest();

        // Assert
        System.assertEquals(20, updatedSubjectOffering.hed__Capacity__c, 'Unexpected capacity value');
        System.assertEquals(20, updatedSubjectOffering.ProjectedCapacity__c, 'Unexpected projected capacity value');
    }

    @isTest
    static void CoordinatorAndFacilitatorSubjectEnrolmentCreated_SubjectOfferingValuesCalculatedSuccessfully()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact coordinator = TestDataContactMock.createFacultyContact(user);
        Contact firstFacilitator = TestDataContactMock.createFacultyContact(user);
        Contact secondFacilitator = TestDataContactMock.createFacultyContact(user);
        hed__Course_Offering__c subjectOffering = [SELECT Id FROM hed__Course_Offering__c LIMIT 1];

        // Act
        test.startTest();
        TestDataSubjectEnrolmentMock.createFacultyEnrolment(user, subjectOffering, coordinator, 'Enrolled', 'Subject Coordinator');
        TestDataSubjectEnrolmentMock.createFacultyEnrolment(user, subjectOffering, firstFacilitator, 'Enrolled', 'Online Facilitator');
        TestDataSubjectEnrolmentMock.createFacultyEnrolment(user, subjectOffering, secondFacilitator, 'Requested', 'Online Facilitator');

        hed__Course_Offering__c updatedSubjectOffering =
        [
            SELECT Id, ActualStudents__c, ProjectedStudents__c, hed__Capacity__c, ProjectedCapacity__c, MaximumCapacity__c
            FROM hed__Course_Offering__c
            WHERE Id = :subjectOffering.Id
        ];
        test.stopTest();

        // Assert
        System.assertEquals(62, updatedSubjectOffering.hed__Capacity__c, 'Unexpected capacity value');
        System.assertEquals(104, updatedSubjectOffering.ProjectedCapacity__c, 'Unexpected projected capacity value');
    }

    @isTest
    static void ProvisionalAndStudentSubjectEnrolmentCreated_SubjectOfferingValuesCalculatedSuccessfully()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact student =  [SELECT Id, Cerebro_Student_ID__c FROM Contact WHERE Cerebro_Student_ID__c = 'A001648586' LIMIT 1];
        Contact secondStudent = TestDataContactMock.createStudent(user, 'A001648514', 'A001648514@study.aib.edu.au', true, false);
        hed__Course_Offering__c subjectOffering = [SELECT Id FROM hed__Course_Offering__c LIMIT 1];
        Account mbaCourse = [SELECT Id, Program__c FROM Account WHERE Program__c = 'MBA'];
        hed__Program_Enrollment__c courseEnrolment = TestDataCourseEnrolmentMock.create(user, mbaCourse.Id, student.Id, 'Ongoing');

        // Act
        test.startTest();
        TestDataSubjectEnrolmentMock.create(user, subjectOffering, courseEnrolment, Constants.SUBJECTENROLMENT_STATUS_ENROLLED, 'Standard', null, null);
        TestDataSubjectEnrolmentMock.createProvisionalEnrolment(user, subjectOffering, secondStudent, Constants.SUBJECTENROLMENT_STATUS_ENROLLING);

        hed__Course_Offering__c updatedSubjectOffering =
        [
            SELECT Id, ActualStudents__c, ProjectedStudents__c, hed__Capacity__c, ProjectedCapacity__c, MaximumCapacity__c
            FROM hed__Course_Offering__c
            WHERE Id = :subjectOffering.Id
        ];
        test.stopTest();

        // Assert
        System.assertEquals(1, updatedSubjectOffering.ActualStudents__c, 'Unexpected actual student value');
        System.assertEquals(2, updatedSubjectOffering.ProjectedStudents__c, 'Unexpected projected student value');
    }

    @isTest
    static void StudentsAndAProvisionalSubjectEnrolmentWithVariousStatusesCreated_SubjectOfferingValuesCalculatedSuccessfully()
    {
        // Arrange
        User testUser = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact student =  [SELECT Id, Cerebro_Student_ID__c FROM Contact WHERE Cerebro_Student_ID__c = 'A001648586' LIMIT 1];
        Contact secondStudent = TestDataContactMock.createStudent(testUser, 'A001648528', 'A001648528@study.aib.edu.au', true, false);
        Contact thirdStudent = TestDataContactMock.createStudent(testUser, 'A001648587', 'A001648587@study.aib.edu.au', true, false);
        Contact fourthStudent = TestDataContactMock.createStudent(testUser, 'A001648556', 'A001648556@study.aib.edu.au', true, false);
        Contact fifthStudent = TestDataContactMock.createProspectHot(testUser);
        hed__Course_Offering__c subjectOffering = [SELECT Id FROM hed__Course_Offering__c LIMIT 1];
        Account mbaCourse = [SELECT Id, Program__c FROM Account WHERE Program__c = 'MBA'];
        hed__Program_Enrollment__c courseEnrolment = TestDataCourseEnrolmentMock.create(testUser, mbaCourse.Id, student.Id, 'Ongoing');
        hed__Program_Enrollment__c secondStudentcourseEnrolment = TestDataCourseEnrolmentMock.create(testUser, mbaCourse.Id, secondStudent.Id, 'Ongoing');
        hed__Program_Enrollment__c thirdStudentcourseEnrolment = TestDataCourseEnrolmentMock.create(testUser, mbaCourse.Id, thirdStudent.Id, 'Ongoing');
        hed__Program_Enrollment__c fourthStudentcourseEnrolment = TestDataCourseEnrolmentMock.create(testUser, mbaCourse.Id, fourthStudent.Id, 'Ongoing');
        hed__Program_Enrollment__c fifthStudentcourseEnrolment = TestDataCourseEnrolmentMock.create(testUser, mbaCourse.Id, fifthStudent.Id, 'Ongoing');
        User adminUser = UserService.getByAlias(Constants.USER_SYSTEMADMIN);

        // Act
        test.startTest();
        TestDataSubjectEnrolmentMock.create(adminUser, subjectOffering, courseEnrolment, Constants.SUBJECTENROLMENT_STATUS_STUDYING, 'Standard', null, null);
        TestDataSubjectEnrolmentMock.create(adminUser, subjectOffering, secondStudentcourseEnrolment, Constants.SUBJECTENROLMENT_STATUS_ENROLLED, 'Standard', null, null);
        TestDataSubjectEnrolmentMock.create(adminUser, subjectOffering, thirdStudentcourseEnrolment, Constants.SUBJECTENROLMENT_STATUS_COMPLETED, 'Standard', null, null);
        TestDataSubjectEnrolmentMock.create(adminUser, subjectOffering, fourthStudentcourseEnrolment, Constants.SUBJECTENROLMENT_STATUS_WITHDRAWING, 'Standard', null, null);
        TestDataSubjectEnrolmentMock.createProvisionalEnrolment(adminUser, subjectOffering, fifthStudent, Constants.SUBJECTENROLMENT_STATUS_ENROLLING, 'Standard', fifthStudentcourseEnrolment);

        hed__Course_Offering__c updatedSubjectOffering =
        [
            SELECT Id, ActualStudents__c, ProjectedStudents__c, hed__Capacity__c, ProjectedCapacity__c, MaximumCapacity__c
            FROM hed__Course_Offering__c
            WHERE Id = :subjectOffering.Id
        ];
        test.stopTest();

        // Assert
        System.assertEquals(3, updatedSubjectOffering.ActualStudents__c, 'Unexpected actual student value');
        System.assertEquals(4, updatedSubjectOffering.ProjectedStudents__c, 'Unexpected projected student value');
    }
}