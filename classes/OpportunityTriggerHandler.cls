public class OpportunityTriggerHandler extends BaseTriggerHandler
{
    public override void beforeInsert()
    {
        OpportunityService.validateInsert(Trigger.new, Trigger.oldMap, Enums.TriggerContext.BEFORE_INSERT);
    }

    public override void afterInsert()
    {
        ActionResult result;

        result = OpportunityService.sendCLOOEmail(Trigger.new, Trigger.oldMap, Enums.TriggerContext.AFTER_INSERT);
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);

        result = OpportunityService.syncRolloverStatusToCampaign(Trigger.new, Trigger.oldMap, Enums.TriggerContext.AFTER_INSERT);        
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);
        
        result = OpportunityService.trackStatusHistory(Trigger.new, Trigger.oldMap, Enums.TriggerContext.AFTER_INSERT);
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);

        result = OpportunityService.assignCampaign(Trigger.new, Trigger.oldMap, Enums.TriggerContext.AFTER_INSERT);
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);
    }

    public override void beforeUpdate()
    {
        ActionResult result;

        result = OpportunityService.checkReopen(Trigger.new, Trigger.oldMap, Enums.TriggerContext.BEFORE_UPDATE);
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);
    }

    public override void afterUpdate()
    {
        ActionResult result;
        
        result = OpportunityService.sendCLOOEmail(Trigger.new, Trigger.oldMap, Enums.TriggerContext.AFTER_UPDATE);
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);
        
        result = OpportunityService.updateSSOStatus(Trigger.new, Trigger.oldMap, Enums.TriggerContext.AFTER_UPDATE);
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);
        
        result = OpportunityService.syncRolloverStatusToCampaign(Trigger.new, Trigger.oldMap, Enums.TriggerContext.AFTER_UPDATE);        
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);

        result = CerebroService.syncApplication(Trigger.new, Trigger.oldMap, Enums.TriggerContext.AFTER_UPDATE);
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);

        result = OpportunityService.trackStatusHistory(Trigger.new, Trigger.oldMap, Enums.TriggerContext.AFTER_UPDATE);
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);

        result = SObjectService.sendConfirmationEmail(Trigger.new, Trigger.oldMap, Enums.TriggerContext.AFTER_UPDATE);
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);
    }
}