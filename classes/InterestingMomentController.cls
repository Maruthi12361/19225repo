@RestResource(urlMapping='/moment/*')
global class InterestingMomentController 
{
    @HttpPost
    global static void save() 
    {        
        String reference = '';
        RestRequest req = RestContext.request;
        ViewModels.Param reqParam = new ViewModels.Param();
        Map<String, Object> optionMap = new Map<String, Object>();
        
        if(req.headers.containsKey('Reference'))
            reference = req.headers.get('Reference');
        
        reqParam.Identifier = reference;    
        reqParam.Payload =  req.requestBody.toString();
        
        Log.info('InterestingMomentController', 'save', 'Identifier value {0} \n Payload: {1}', new List<String>{ reqParam.Identifier, reqParam.Payload });
        
        ActionResult result;
        
        for (Integer i = 0; i < 20; i++)
        {
            result = ContactService.saveInterestingMoment(reqParam);
            
            if (result.Status == ResultStatus.SUCCESS || !result.message.contains('UNABLE_TO_LOCK_ROW'))
                break;
            
            Log.info('InterestingMomentController', 'save', 'Unable to lock row, remaining retry {0}', new List<String>{ String.valueOf(4 - i) });
        }
        
        if(result.Status == ResultStatus.SUCCESS)
        {
            RestContext.response.addHeader('Content-Type', 'application/json');
            RestContext.response.statusCode = 201;
        }
        else 
        {
            Log.error('InterestingMomentController', 'save', result.message);
            throw new Exceptions.ApplicationException(result.message);
        }
    }
}