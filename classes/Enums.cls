public class Enums
{
    public enum TriggerContext
    {
        BEFORE_INSERT,
        BEFORE_UPDATE,
        BEFORE_DELETE,
        AFTER_INSERT,
        AFTER_UPDATE,
        AFTER_DELETE,
        AFTER_UNDELETE
    }

    public enum TermStatus
    {
        CURRENT_TERM,
        NEXT_TERM,
        FUTURE_TERM,
        PAST_TERM
    }

    public enum Operator
    {
        EQUAL,
        NOT_EQUAL,
        LESS_THAN,
        LESS_THAN_OR_EQUAL,
        GREATER_THAN,
        GREATER_THAN_OR_EQUAL,
        CONTAINS,
        NOT_CONTAINS,
        EMPTY,
        NOT_EMPTY,
        ANY_VALUE_IN,
        RANGE,
        STARTS_WITH,
        ENDS_WITH
    }

    public enum DataUpdateAction
    {
        MOVE,
        WRITE,
        TRANSFORM
    }

    public enum SSOTokenType
    {
        ACCESS,
        APPLICATION
    }

    public enum DatabaseAction
    {
        ADD,
        NONE,
        REMOVE,
        UNKNOWN,
        EDIT
    }
}