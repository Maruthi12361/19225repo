@isTest
public class TestDataCampaignMock
{
    private static Map<String, Campaign> campaignMap;

    public static void createRolloverCampaignMemberStatus(User user)
    {
        List<Campaign> campaignList =
            [
                SELECT Id
                FROM Campaign
                WHERE Type = 'Rollover'
            ];

        System.RunAs(user)
        {
            for (Campaign campaign : campaignList)
            {
                List<CampaignMemberStatus> statusList = new List<CampaignMemberStatus>();

                CampaignMemberStatus status = new CampaignMemberStatus(CampaignId=campaign.Id, HasResponded=true, Label=RolloverConstantsDefinition.ST_OPEN, SortOrder=3);
                statusList.add(status);

                status = new CampaignMemberStatus(CampaignId=campaign.Id, HasResponded=true, Label=RolloverConstantsDefinition.ST_MASSEMAIL, SortOrder=4);
                statusList.add(status);

                status = new CampaignMemberStatus(CampaignId=campaign.Id, HasResponded=false, Label=RolloverConstantsDefinition.ST_CONTACTING, SortOrder=5);
                statusList.add(status);

                status = new CampaignMemberStatus(CampaignId=campaign.Id, HasResponded=false, Label=RolloverConstantsDefinition.ST_ENROLLING, SortOrder=6);
                statusList.add(status);

                status = new CampaignMemberStatus(CampaignId=campaign.Id, HasResponded=false, Label=RolloverConstantsDefinition.ST_CLOSED_LOST, SortOrder=7);
                statusList.add(status);

                status = new CampaignMemberStatus(CampaignId=campaign.Id, HasResponded=false, Label=RolloverConstantsDefinition.ST_CLOSED_WON, SortOrder=8);
                statusList.add(status);

                insert statusList;
            }
        }
    }

    public static void createAcquisitionCampaignMemberStatus(User user)
    {
        List<Campaign> campaignList =
            [
                SELECT Id
                FROM Campaign
                WHERE Type = 'Acquisition'
            ];

        System.RunAs(user)
        {
            for (Campaign campaign : campaignList)
            {
                List<CampaignMemberStatus> statusList = new List<CampaignMemberStatus>();

                CampaignMemberStatus status = new CampaignMemberStatus(CampaignId=campaign.Id, HasResponded=false, Label='Moved', SortOrder=3);
                statusList.add(status);

                insert statusList;
            }
        }
    }

    public static List<Campaign> createWarmCampaigns(User user)
    {
        List<Campaign> campaignList =
            [
                SELECT Id
                FROM Campaign
                WHERE Type = 'Warm'
            ];

        if (campaignList.size() == 0)
        {
            campaignList = new List<Campaign>();

            Campaign item = new Campaign(Name = 'ASAP', IsActive = true, Status = 'In Progress', Type = 'Warm');
            campaignList.add(item);

            item = new Campaign(Name = '3 - 6 Months', IsActive = true, Status = 'In Progress', Type = 'Warm');
            campaignList.add(item);

            item = new Campaign(Name = '6 - 12 Months', IsActive = true, Status = 'In Progress', Type = 'Warm');
            campaignList.add(item);

            item = new Campaign(Name = '12+ Months', IsActive = true, Status = 'In Progress', Type = 'Warm');
            campaignList.add(item);

            item = new Campaign(Name = 'Undecided', IsActive = true, Status = 'In Progress', Type = 'Warm');
            campaignList.add(item);

            System.RunAs(user)
            {
                insert campaignList;
            }
        }

        return campaignList;
    }

    // TODO: Technical Debt - Remove
    public static Campaign getWarmCampaign(User user, String name)
    {
        List<Campaign> warmCampaigns = createWarmCampaigns(user);

        for (Campaign camp : warmCampaigns)
        {
            if (camp.Name == name)
                return camp;
        }

        return null;
    }

    public static Campaign create(User user, String portfolio, String program, Boolean isRollover)
    {
        return create(user, getCurrentCourseStartDate(), portfolio, program, isRollover);
    }

    public static Campaign create(User user, String portfolio, String program, Boolean isRollover, Boolean bulkCreation)
    {
        return create(user, getCurrentCourseStartDate(), portfolio, program, isRollover, bulkCreation);
    }

    public static Campaign create(User user, Date startDate, String portfolio, String program, Boolean isRollover, Boolean bulkCreation)
    {
        return create(user, startDate, portfolio, program, isRollover, bulkCreation, null);
    }

    public static Campaign create(User user, Date startDate, String portfolio, String program, Boolean isRollover, Boolean bulkCreation, hed__Term__c term)
    {
        Campaign campaign;
        List<Campaign> campaignList;
        String campaignType = (isRollover ? 'Rollover' : 'Acquisition');
        String key = campaignType + portfolio.left(1) + program + startDate.format();

        if (campaignMap == null)
        {
            campaignMap = new Map<String, Campaign>();
            List<Campaign> campaigns =
                [
                    SELECT Id, Type, Programme__c, Portfolio__c, Program_Portfolio__c, TermLookup__c, ApplicationCutoffDate__c, StartDate, EndDate,
                        TermAdministrationDate__c, TermCensusDate__c, TermStartDate__c, Year_Term__c
                    FROM Campaign
                    WHERE RecordType.DeveloperName = 'TermIntake'
                ];

            for (Campaign item : campaigns)
            {
                campaignMap.put(item.Type + item.Program_Portfolio__c + item.TermStartDate__c.format(), item);
            }
        }

        campaign = campaignMap.get(key);

        if (campaign == null)
        {
            campaign = new Campaign(Name = 'Test Campaign ' + portfolio.left(1) + program + ' ' + startDate.format(), IsActive = true);

            if (term == null)
            {
                Account account = TestDataAccountMock.createDepartmentBusiness(user);
                term = TestDataTermMock.create(user, account.Id, startDate);
            }

            campaign.TermLookup__c = term.Id;
            campaign.EndDate = startDate.addDays(11);
            campaign.Portfolio__c = portfolio;
            campaign.Programme__c = program;
            campaign.StartDate = startDate.addDays(-56);
            campaign.Type = campaignType;
            campaign.RecordTypeId = RecordTypeUtils.getRecordTypeId(Constants.RECORDTYPE_CAMPAIGN_TERMINTAKE, SObjectType.Campaign);

            if (campaign.StartDate > Date.today())
                campaign.Status = 'Planned';
            else
            {
                if (campaign.EndDate < Date.today())
                    campaign.Status = 'Completed';
                else
                    campaign.Status = 'In Progress';
            }

            System.RunAs(user)
            {
                insert campaign;

                // If a campaign is created individually, we need to query it from database for those cross-object formula fields.
                // Otherwise, these formula fields are null and the campaign object may not be usable for Opportunity related test cases.
                // If a campaign is created as part of bulk creation, we don't need to query it here as the test cases should do the query
                // for any campaign they need. It can also avoid hitting SOQL limit.
                if (!bulkCreation)
                {
                    campaign =
                        [
                            SELECT Id, Type,Programme__c, Portfolio__c, TermLookup__c, ApplicationCutoffDate__c, StartDate, EndDate,
                            TermAdministrationDate__c, TermCensusDate__c, TermStartDate__c, Year_Term__c
                            FROM Campaign
                            WHERE Id = : campaign.Id
                        ];
                }
            }

            campaignMap.put(key, campaign);
        }

        return campaign;
    }

    public static Campaign create(User user, Date startDate, String portfolio, String program, Boolean isRollover)
    {
        return create(user, startDate, portfolio, program, isRollover, false);
    }

    public static void createSeries(User user, Date courseStartDate, Integer occurrence, Boolean isRollover)
    {
        Account account = TestDataAccountMock.createDepartmentBusiness(user);

        for (integer i = 0; i < occurrence; i++)
        {
            Date startDate = courseStartDate.addDays(i * 56);
            hed__Term__c term = TestDataTermMock.create(user, account.Id, startDate);
            create(user, startDate, 'Domestic', 'MBA', isRollover, true, term);
            create(user, startDate, 'International', 'MBA', isRollover, true, term);
        }
    }

    public static void createSeries(User user, Integer occurrence, Boolean isRollover)
    {
        createSeries(user, getCurrentCourseStartDate(), occurrence, isRollover);
    }

    public static Date getCurrentCourseStartDate()
    {
        Date startDate = Date.today();

        return startDate;
    }

    public static String getCampaignStatus(String termStatus)
    {
        String status = 'Planned';

        if (termStatus == 'Past')
            status = 'Completed';
        else if (termStatus == 'Previous' || termStatus == 'Next')
            status = termStatus;
        else if (termStatus == 'Current')
            status = 'In Progress';
        else if (termStatus == 'Invalid')
            status = 'Aborted';

        return status;
    }
}