public class ViewModelsWorkforce
{
    
    public class SubjectOfferingList implements Interfaces.IViewModel
    {
        public List<SubjectOfferingListItem> items { get; set; }

        public SubjectOfferingList()
        {
            items = new List<SubjectOfferingListItem>();
        }
    }

    public class SubjectOfferingListItem implements Interfaces.IViewModel
    {
        @AuraEnabled
        public String Id { get; set; }
        @AuraEnabled
        public String Code { get; set; }
        @AuraEnabled
        public String Name { get; set; }
        @AuraEnabled
        public String AQFLevel { get; set; }
        @AuraEnabled
        public String HeadOfDiscipline { get; set; }
        @AuraEnabled
        public String StudentsTaughtPerOLF { get; set; }
        @AuraEnabled
        public String StudentsTaughtPerSC { get; set; }
        @AuraEnabled
        public String StudentsCoordinatedPerSC { get; set; }
        @AuraEnabled
        public String TotalSkilledContacts { get; set; }
        @AuraEnabled
        public String ActualStudents { get; set; }
        @AuraEnabled
        public Boolean IsValid { get; set; }
        @AuraEnabled
        public String RecordUrl { get; set; }
    }

    public class SubjectOfferingStaffDetails implements Interfaces.IViewModel
    {
        @AuraEnabled
        public List<SubjectOfferingStaffDetailsItem> SubjectOfferings { get; set; }
        @AuraEnabled
        public ViewModels.PicklistOptions Option { get; set; }

        public SubjectOfferingStaffDetails()
        {
            SubjectOfferings = new List<SubjectOfferingStaffDetailsItem>();
        }
    }

    public class SubjectOfferingStaffDetailsItem implements Interfaces.IViewModel
    {
        @AuraEnabled
        public String SubjectOfferingId { get; set; }
        @AuraEnabled
        public String SubjectCode { get; set; }
        @AuraEnabled
        public String SubjectName { get; set; }
        @AuraEnabled
        public Date StartDate { get; set; }
        @AuraEnabled
        public Date EndDate { get; set; }
        @AuraEnabled
        public Integer ActualStudents { get; set; }
        @AuraEnabled
        public Integer ProjectedStudents { get; set; }
        @AuraEnabled
        public Integer Capacity { get; set; }
        @AuraEnabled
        public Integer ProjectedCapacity { get; set; }
        @AuraEnabled
        public String HealthStatus { get; set; }
        @AuraEnabled
        public List<SubjectEnrolmentStaffDetailsItem> SubjectEnrolments { get; set; }
        @AuraEnabled
        public List<RoleStudentCapacity> RoleStudentCapacities { get; set; }

        public SubjectOfferingStaffDetailsItem()
        {
            SubjectEnrolments = new List<SubjectEnrolmentStaffDetailsItem>();
            RoleStudentCapacities = new List<RoleStudentCapacity>();
        }
    }

    public class SubjectEnrolmentStaffDetailsItem implements Interfaces.IViewModel
    {
        @AuraEnabled
        public String SubjectEnrolmentId { get; set; }
        @AuraEnabled
        public String RoleName { get; set; }
        @AuraEnabled
        public String StaffId { get; set; }
        @AuraEnabled
        public String StaffName { get; set; }
        @AuraEnabled
        public Boolean StaffPrerequisitesCompleted { get; set; }
        @AuraEnabled
        public Integer AllocatedStudents { get; set; }
        @AuraEnabled
        public Integer MaxCapacity { get; set; }
        @AuraEnabled
        public String Status { get; set; }
        @AuraEnabled
        public Boolean IsDeleted { get; set; }
    }

    public class FacultyStaffPlanOffering implements Interfaces.IViewModel
    {
        @AuraEnabled
        public String Id { get; set; }
        @AuraEnabled
        public String Name { get; set; }
        @AuraEnabled
        public String Role { get; set; }
        @AuraEnabled
        public Boolean PrerequisitesCompleted { get; set; }
    }

    public class RoleStudentCapacity implements Interfaces.IViewModel
    {
        @AuraEnabled
        public String RoleName { get; set; }
        @AuraEnabled
        public String Capacity { get; set; }
    }
}