public class LogDeletionBatch extends BaseBatchCommand 
{
    protected override Database.QueryLocator query(Database.BatchableContext context)
    {
        Integer deleteThreshold = 14;//Days
        Datetime dateThreshold = Datetime.now();
        dateThreshold = dateThreshold.addDays(-deleteThreshold);

        String sql ='SELECT Id ' +
                    'FROM AppLog__c ' +
                    'WHERE CreatedDate <  :dateThreshold';

        return Database.getQueryLocator(sql);
    }
    
    public override void command(Database.BatchableContext BC, List<sObject> scope) 
    {
        Log.info('LogDeletionBatch', 'command', 'Number of logs: ' + scope.size());
        Log.deleteLogs(scope);
    }
}