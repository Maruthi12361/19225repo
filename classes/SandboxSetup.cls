global without sharing class SandboxSetup implements SandboxPostCopy
{
    @TestVisible private Id orgId = null;
    @TestVisible private Id sandboxId = null;
    @TestVisible private String sandboxName = '';
    @TestVisible private String baseURLLabel = '';

    global void runApexClass(SandboxContext context)
    {
        //Setup consants initially
        orgId = context.organizationId();
        sandboxId = context.sandboxId();
        sandboxName = context.sandboxName();

        Log.info('SandboxSetup', 'runApexClass', 'Org ID: ' + orgId + ', Sandbox ID: ' + sandboxId + ', Sandbox Name: ' + sandboxName);

        //Now perform the individual post-create/refresh steps
        setupRolloverCampaignStatusDefaults();
        setupAcquisitionCampaignStatusDefaults();
        setupCampaigns();
        scheduleJobs();

        //This currently cannot be done through Apex without using the open source MetaData API (https://github.com/financialforcedev/apex-mdapi) to call the SOAP API.
        //setupLabels(Url.getSalesforceBaseUrl().getHost().toLowerCase());
    }

    private void setupRolloverCampaignStatusDefaults()
    {
        List<AAKCS__Campaign_Status_Default__c> rollovers = [SELECT Id FROM AAKCS__Campaign_Status_Default__c WHERE Name = 'Rollover'];
        if (rollovers.size() == 0)
        {
            AAKCS__Campaign_Status_Default__c campaign = new AAKCS__Campaign_Status_Default__c(name='Rollover', aakcs__action__c='Replace All', aakcs__active__c=true);
            insert campaign;

            List<AAKCS__Campaign_Status_Default_Value__c> vals = new List<AAKCS__Campaign_Status_Default_Value__c>();
            vals.add(new AAKCS__Campaign_Status_Default_Value__c(name='Open - Not Contacted', AAKCS__Campaign_Status_Default__c=campaign.id, AAKCS__Default__c=true, AAKCS__Responded__c=false, AAKCS__Sequence__c=1));
            vals.add(new AAKCS__Campaign_Status_Default_Value__c(name='Mass Email Sent', AAKCS__Campaign_Status_Default__c=campaign.id, AAKCS__Default__c=false, AAKCS__Responded__c=false, AAKCS__Sequence__c=2));
            vals.add(new AAKCS__Campaign_Status_Default_Value__c(name='Contacting', AAKCS__Campaign_Status_Default__c=campaign.id, AAKCS__Default__c=false, AAKCS__Responded__c=false, AAKCS__Sequence__c=3));
            vals.add(new AAKCS__Campaign_Status_Default_Value__c(name='Enrolling', AAKCS__Campaign_Status_Default__c=campaign.id, AAKCS__Default__c=false, AAKCS__Responded__c=true, AAKCS__Sequence__c=4));
            vals.add(new AAKCS__Campaign_Status_Default_Value__c(name='Closed Won', AAKCS__Campaign_Status_Default__c=campaign.id, AAKCS__Default__c=false, AAKCS__Responded__c=true, AAKCS__Sequence__c=5));
            vals.add(new AAKCS__Campaign_Status_Default_Value__c(name='Closed Lost', AAKCS__Campaign_Status_Default__c=campaign.id, AAKCS__Default__c=false, AAKCS__Responded__c=false, AAKCS__Sequence__c=6));
            insert vals;
        }
    }

    private void setupAcquisitionCampaignStatusDefaults()
    {
        List<AAKCS__Campaign_Status_Default__c> acquisitions = [SELECT Id FROM AAKCS__Campaign_Status_Default__c WHERE Name = 'Acquisition'];
        if (acquisitions.size() == 0)
        {
            AAKCS__Campaign_Status_Default__c campDefault = new AAKCS__Campaign_Status_Default__c(Name='Acquisition', AAKCS__Active__c=true, AAKCS__Action__c='Replace All');
            insert campDefault;

            List<AAKCS__Campaign_Status_Default_Value__c> vals = new List<AAKCS__Campaign_Status_Default_Value__c>();
            vals.add(new AAKCS__Campaign_Status_Default_Value__c(Name='Sent', AAKCS__Campaign_Status_Default__c=campDefault.Id, AAKCS__Default__c=true, AAKCS__Responded__c=false, AAKCS__Sequence__c=1));
            vals.add(new AAKCS__Campaign_Status_Default_Value__c(Name='Responded', AAKCS__Campaign_Status_Default__c=campDefault.Id, AAKCS__Default__c=false, AAKCS__Responded__c=true, AAKCS__Sequence__c=2));
            vals.add(new AAKCS__Campaign_Status_Default_Value__c(Name='Moved', AAKCS__Campaign_Status_Default__c=campDefault.Id, AAKCS__Default__c=false, AAKCS__Responded__c=false, AAKCS__Sequence__c=3));
            insert vals;
        }
    }

    private void setupCampaigns()
    {
        List<Campaign> lists = [SELECT Id FROM Campaign WHERE Type = 'Warm'];
        if (lists.size() == 0)
        {
            lists = new List<Campaign>();
            lists.add(new Campaign(Name = 'ASAP', IsActive = true, Status = 'In Progress', Type = 'Warm'));
            lists.add(new Campaign(Name = '3 - 6 Months', IsActive = true, Status = 'In Progress', Type = 'Warm'));
            lists.add(new Campaign(Name = '6 - 12 Months', IsActive = true, Status = 'In Progress', Type = 'Warm'));
            lists.add(new Campaign(Name = '12+ Months', IsActive = true, Status = 'In Progress', Type = 'Warm'));
            lists.add(new Campaign(Name = 'Undecided', IsActive = true, Status = 'In Progress', Type = 'Warm'));
            insert lists;
        }
    }

    private void scheduleJobs()
    {
        String schedule09PM = '0 0 21 * * ?';
        String schedule11PM = '0 0 23 * * ?';
        String schedule01AM = '0 0  1 * * ?';
        String schedule02AM = '0 0  2 * * ?';
        String schedule03AM = '0 0  3 * * ?';
        String schedule04AM = '0 0  4 * * ?';
        String schedule06AM = '0 0  6 * * ?';
        String scheduleHrly = '0 0  * * * ?';
        String schedule2Hrly = '0 0  */2 * * ?';
        String schedule01AM_SAT_WEEKLY = '0 0  1 ? * SAT';
        String schedule04AM_WED_WEEKLY = '0 0  4 ? * WED';

        String caseLastInteractionSchedule = 'CaseLastInteractionSchedule-Daily';
        String contactBatchRolloverDetailSchedule = 'ContactBatchRolloverDetailSchedule-Daily';
        String contactCalcPassRateSchedule = 'ContactCalcPassRateSchedule-Daily';
        String contactExpressConsentSchedule = 'ContactExpressConsentSchedule-Daily';
        String contactReferrerSetLeadSourceSchedule = 'ContactReferrerSetLeadSourceSchedule-Daily';
        String contactScoreDemographicsSchedule = 'ContactScoreDemographicsSchedule-Daily';
        String contactSumBehaviourDecaySchedule = 'ContactSumBehaviourDecaySchedule-Daily';
        String decsOnDScheduledCleanExecutionRecords = 'DecsOnD_ScheduledCleanExecutionRecords_7-Weekly';
        String interestingMomentUpdateScoringSchedule = 'InterestingMomentUpdateScoringSchedule-Daily';
        String logDeletionSchedulable = 'LogDeletionSchedulable-Daily';
        String opportunityCloseSchedule = 'OpportunityCloseSchedule-Daily';
        String opportunityMoveTermSchedule = 'OpportunityMoveTermSchedule-Weekly';
        String statusTrackingHistoryCalcAgeSchedule = 'StatusTrackingHistoryCalcAgeSchedule-2Hourly';
        String systemHealthCheckSchedule = 'SystemHealthCheckSchedule-Daily';
        String termUpdateSchedule = 'TermUpdateSchedule-Daily'; //calls TermUpdateDateBatch --> TermUpdateStatusBatch --> CampaignUpdateStatusBatch
        String userUpdateLightningPreferenceSchedule = 'UserUpdateLightningPreferenceSchedule-Hourly';
        String userUpdatePhotoUrl = 'UserUpdatePhotoURL-Daily';

        //Need to check the job hasnt already been scheduled before attempting to schedule
        if ([SELECT Id, Name FROM CronJobDetail WHERE Name = :caseLastInteractionSchedule].size() == 0)
        {
            System.schedule(caseLastInteractionSchedule, schedule06AM, new CaseLastInteractionSchedule());
        }
        if ([SELECT Id, Name FROM CronJobDetail WHERE Name = :contactBatchRolloverDetailSchedule].size() == 0)
        {
            System.schedule(contactBatchRolloverDetailSchedule, schedule06AM, new ContactBatchRolloverDetailSchedule());
        }
        if ([SELECT Id, Name FROM CronJobDetail WHERE Name = :contactCalcPassRateSchedule].size() == 0)
        {
            System.schedule(contactCalcPassRateSchedule, schedule03AM, new ContactCalcPassRateSchedule());
        }
        if ([SELECT Id, Name FROM CronJobDetail WHERE Name = :contactExpressConsentSchedule].size() == 0)
        {
            System.schedule(contactExpressConsentSchedule, schedule09PM, new ContactExpressConsentSchedule());
        }
        if ([SELECT Id, Name FROM CronJobDetail WHERE Name = :contactReferrerSetLeadSourceSchedule].size() == 0)
        {
            System.schedule(contactReferrerSetLeadSourceSchedule, schedule02AM, new ContactReferrerSetLeadSourceSchedule());
        }
        if ([SELECT Id, Name FROM CronJobDetail WHERE Name = :contactScoreDemographicsSchedule].size() == 0)
        {
            System.schedule(contactScoreDemographicsSchedule, schedule01AM, new ContactScoreDemographicsSchedule());
        }
        if ([SELECT Id, Name FROM CronJobDetail WHERE Name = :contactSumBehaviourDecaySchedule].size() == 0)
        {
            System.schedule(contactSumBehaviourDecaySchedule, schedule04AM, new ContactSumBehaviourDecaySchedule());
        }
        if ([SELECT Id, Name FROM CronJobDetail WHERE Name = :decsOnDScheduledCleanExecutionRecords].size() == 0)
        {
            System.schedule(decsOnDScheduledCleanExecutionRecords, schedule01AM_SAT_WEEKLY, new DecsOnD.ScheduledCleanExecutionRecords_7());
        }
        if ([SELECT Id, Name FROM CronJobDetail WHERE Name = :interestingMomentUpdateScoringSchedule].size() == 0)
        {
            System.schedule(interestingMomentUpdateScoringSchedule, schedule03AM, new InterestingMomentUpdateScoringSchedule());
        }
        if ([SELECT Id, Name FROM CronJobDetail WHERE Name = :logDeletionSchedulable].size() == 0)
        {
            System.schedule(logDeletionSchedulable, schedule02AM, new LogDeletionSchedulable());
        }
        if ([SELECT Id, Name FROM CronJobDetail WHERE Name = :opportunityCloseSchedule].size() == 0)
        {
            System.schedule(opportunityCloseSchedule, schedule02AM, new OpportunityCloseSchedule());
        }
        if ([SELECT Id, Name FROM CronJobDetail WHERE Name = :opportunityMoveTermSchedule].size() == 0)
        {
            System.schedule(opportunityMoveTermSchedule, schedule04AM_WED_WEEKLY, new OpportunityMoveTermSchedule());
        }
        if ([SELECT Id, Name FROM CronJobDetail WHERE Name = :statusTrackingHistoryCalcAgeSchedule].size() == 0)
        {
            System.schedule(statusTrackingHistoryCalcAgeSchedule, schedule2Hrly, new StatusTrackingHistoryCalcAgeSchedule());
        }
        if ([SELECT Id, Name FROM CronJobDetail WHERE Name = :systemHealthCheckSchedule].size() == 0)
        {
            System.schedule(systemHealthCheckSchedule, schedule06AM, new SystemHealthCheckSchedule());
        }
        if ([SELECT Id, Name FROM CronJobDetail WHERE Name = :termUpdateSchedule].size() == 0)
        {
            System.schedule(termUpdateSchedule, schedule01AM, new TermUpdateSchedule());
        }
        if ([SELECT Id, Name FROM CronJobDetail WHERE Name = :userUpdateLightningPreferenceSchedule].size() == 0)
        {
            System.schedule(userUpdateLightningPreferenceSchedule, scheduleHrly, new UserUpdateLightningPreferenceSchedule());
        }
        if ([SELECT Id, Name FROM CronJobDetail WHERE Name = :userUpdatePhotoUrl].size() == 0)
        {
            System.schedule(userUpdatePhotoUrl, schedule11PM, new UserUpdatePhotoUrl());
        }
    }

    //At some stage change this over to use the MetaData API & CustomMetadataTypes instead of labels or use the Andy Fawcett Metadata Wraper package install to access the custom labels via SOAP API Calls.
    @TestVisible private void setupLabels(String hostURL)
    {
        Log.info('SandboxSetup', 'setupLabels', 'Initially for hostURL:' + hostURL + ', label values are:');
        Log.info('SandboxSetup', 'setupLabels', 'Label-OAFBaseURL:' + System.Label.OAFBaseURL);

        List<String> splits = hostURL.split('\\.');
        String host = splits.get(0);
        String sName = '';
        String server = 'secure';

        //if host is one of the sandboxes rather than PROD
        if (host.startsWithIgnoreCase('aib--'))
        {
            sName = (host.substring('aib--'.length()) + '-').toLowerCase();
            server = splits.get(1);
        }

        baseURLLabel = 'https://' + sName + 'aib-app.' + server + '.force.com/';

        //Can't do this directly as it causes an error
        //System.Label.OAFBaseURL = baseURLLabel;

        Log.info('SandboxSetup', 'setupLabels', 'Post Setup for hostURL:' + hostURL + ', label values are:');
        Log.info('SandboxSetup', 'setupLabels', 'Label-OAFBaseURL:' + System.Label.OAFBaseURL);
    }
}