public class ContentVersionTriggerHandler extends BaseTriggerHandler
{
    public override void beforeInsert()
    {
        ActionResult result;

        result = ContentDocumentService.updateType(Trigger.new, Trigger.oldMap, Enums.TriggerContext.BEFORE_INSERT);

        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);
    }

    public override void afterInsert()
    {
        ActionResult result;

        result = ContentDocumentService.shareWithAllUsers(Trigger.new, Trigger.oldMap, Enums.TriggerContext.AFTER_INSERT);

        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);

        result = ContentDocumentService.updateEntityFlags(Trigger.new, Trigger.oldMap, Enums.TriggerContext.AFTER_INSERT);

        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);

        result = ContentDocumentService.updateOpportunityDocStatuses(Trigger.new, Trigger.oldMap, Enums.TriggerContext.AFTER_INSERT);

        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);
    }

    public override void afterUpdate()
    {
        ActionResult result;

        result = ContentDocumentService.updateOpportunityDocStatuses(Trigger.new, Trigger.oldMap, Enums.TriggerContext.AFTER_UPDATE);

        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);
    }
}