@isTest
private class BaseIdQueryTests
{
    @testSetup
    static void setup()
    {
        TestUtilities.setupHEDA();
    }

    @isTest
    static void BaseQueryWithIds_FiltersAndReturnsResultsMatchingIds()
    {
        // Arrange
        TestBaseIdQuery testQuery = new TestBaseIdQuery();
        List<Case> cases1 = new List<Case>();
        List<Case> cases2 = new List<Case>();
        
        for (Integer i = 0; i < 10; i++)
        {
            Case newCase = new Case();
            newCase.Subject = 'Test BaseIdQuery';
            newCase.Description = 'Test BaseIdQuery';

            if (i < 5)
                cases1.add(newCase);
            else
                cases2.add(newCase);
        }

        insert cases1;
        insert cases2;

        Set<Id> caseIds = (new Map<Id,SObject>(cases1)).keySet();

        // Act
        List<SObject> results = testQuery.query(caseIds);

        // Assert
        System.assertNotEquals(null, results, 'Request should return a valid collection of items');
        System.assertEquals(5, results.size(), 'Request should return only those items identified by the ids being passed through');
    }

    class TestBaseIdQuery extends BaseIdQuery
    {
        public override List<SObject> query(Set<Id> caseIds)
        {
            String qry = 'SELECT Id FROM Case WHERE Id IN :caseIds';
            return Database.query(qry);
        }
    }
}