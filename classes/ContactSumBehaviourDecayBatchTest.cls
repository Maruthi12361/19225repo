@isTest
public class ContactSumBehaviourDecayBatchTest 
{
    @testSetup 
    static void setup()
    {
        TestUtilities.setupHEDA();
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        TestDataSalesScoreConfigMock.create(user);
        Contact contact = TestDataContactMock.createProspectCold(user, '040411111122');
    }

    @isTest 
    static void ContactWithNoInterestingMoments_BehaviourDecaySumNotUpdated() 
    {
        // Act
        Test.startTest();
        ContactSumBehaviourDecayBatch batch = new ContactSumBehaviourDecayBatch();
        ID batchProcessId = Database.executeBatch(batch, 5);
        Test.stopTest();
        
        Contact updatedContact = [SELECT Id, BehaviourDecaySum__c FROM Contact LIMIT 1];

        // Assert
        System.assertEquals(null, updatedContact.BehaviourDecaySum__c, 'Unexpected Behaviour Decay Sum value');
    }

    @isTest 
    static void ContactWithSingleInterestingMoment_BehaviourDecayUpdatedCorrectly() 
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact contact = [SELECT Id FROM Contact LIMIT 1];
        InterestingMoment__c moment = TestDataInterestingMomentMock.create(user, contact);

        // Act
        Test.startTest();
        ContactSumBehaviourDecayBatch batch = new ContactSumBehaviourDecayBatch();
        ID batchProcessId = Database.executeBatch(batch, 5);
        Test.stopTest();

        Contact updatedContact = [SELECT Id, BehaviourDecaySum__c FROM Contact WHERE Id = :contact.Id];
        InterestingMoment__c insertedMoment = [SELECT Id, BehaviourDecay__c, ContactID__c FROM InterestingMoment__c WHERE ContactID__c = :updatedContact.Id LIMIT 1];

        // Assert
        System.assertEquals(insertedMoment.BehaviourDecay__c, updatedContact.BehaviourDecaySum__c, 'Unexpected Behaviour Decay Sum value');
    }

    @isTest 
    static void ContactWithMultipleInterestingMoments_BehaviourDecayUpdatedCorrectly() 
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact contact = [SELECT Id FROM Contact LIMIT 1];
        TestDataInterestingMomentMock.create(user, contact);
        TestDataInterestingMomentMock.create(user, contact, Datetime.now().addDays(-10));

        // Act
        Test.startTest();
        ContactSumBehaviourDecayBatch batch = new ContactSumBehaviourDecayBatch();
        ID batchProcessId = Database.executeBatch(batch, 5);
        Test.stopTest();

        Contact updatedContact = [SELECT Id, BehaviourDecaySum__c FROM Contact WHERE Id = :contact.Id];
        AggregateResult[] result = [SELECT SUM(BehaviourDecay__c) TotalDecay, ContactID__c FROM InterestingMoment__c WHERE ContactID__c = :updatedContact.Id GROUP by ContactID__c];

        // Assert
        System.assertEquals(result[0].get('TotalDecay'), updatedContact.BehaviourDecaySum__c, 'Unexpected Behaviour Decay Sum value');
    }
}