@isTest
public class TestDataContentVersionMock
{
    public static ContentVersion create(User user, Id parentId)
    {
        return create(user, 'Test File.txt', 'Test File', parentId);
    }

    public static ContentVersion create(User user, String filename, String title, Id parentId)
    {
        return create(user, filename, title, parentId, null);
    }

    public static ContentVersion create(User user, String filename, String title, Id parentId, String type)
    {
        String fileContent = 'Test Test and Test\nTest';
        Blob data = Blob.valueOf(fileContent);

        ContentVersion version = new ContentVersion
        (
            ContentLocation = Constants.FILE_CONTENT_LOCATION_SALESFORCE,
            VersionData = data,
            Title = title,
            Description = 'File description',
            PathOnClient = filename,
            FirstPublishLocationId = parentId,
            Origin = Constants.FILE_VERSION_ORIGIN_CONTENT,
            Type__c = type
        );

        System.runAs(user)
        {
            insert version;
        }

        return version;
    }
}