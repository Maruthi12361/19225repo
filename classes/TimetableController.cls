@RestResource(urlMapping='/timetable/*')
global class TimetableController
{
    @HttpGet 
    global static void get()
    {
        try
        {
            String reference = '';
            RestRequest req = RestContext.request;
            ViewModels.Param param = new ViewModels.Param();

            if (!req.headers.containsKey('Reference'))
            {
                RestContext.response.responseBody = Blob.valueOf('Unauthenticated Request');
                RestContext.response.statusCode = 400;
                return;
            }

            reference = req.headers.get('Reference');
            param.Identifier = reference;
            param.IsAuthenticated = reference == '' || reference.contains('@') ? false : true;

            RestContext.response.addHeader('Content-Type', 'application/json');
            RestContext.response.responseBody = Blob.valueOf(JSON.serialize(TimetableService.getTimetable(param)));
            RestContext.response.statusCode = 200;
        }
        catch(Exception ex)
        {
            Map<String, String> errorMap = new Map<String, String>
            {
                'Message' => ex.getMessage(),
                'Details' => ex.getStackTraceString() 
            };

            RestContext.response.addHeader('Content-Type', 'application/json');
            RestContext.response.responseBody = Blob.valueOf(JSON.serialize(errorMap));
            RestContext.response.statusCode = 400;
        }
    }

    @HttpPost
    global static void save()
    {
        String reference = '';
        RestRequest req = RestContext.request;
        String payload = req.requestBody.toString();

        if (!req.headers.containsKey('Reference'))
        {
            RestContext.response.responseBody = Blob.valueOf('Unauthenticated Request');
            RestContext.response.statusCode = 400;
            return;
        }

        reference = req.headers.get('Reference');
        ViewModelsTimetable.EnrolmentDetails enrolmentDetails = (ViewModelsTimetable.EnrolmentDetails)JSON.deserialize(payload, ViewModelsTimetable.EnrolmentDetails.class);
        enrolmentDetails.Reference = reference;

        ActionResult result = TimetableService.saveTimetable(enrolmentDetails);

        if (result.Status == ResultStatus.SUCCESS)
        {
            ViewModels.Param param = new ViewModels.Param();
            param.Identifier = reference;
            param.IsAuthenticated = reference == '' || reference.contains('@') ? false : true;

            RestContext.response.addHeader('Content-Type', 'application/json');
            RestContext.response.responseBody = Blob.valueOf(JSON.serialize(TimetableService.getTimetable(param)));
            RestContext.response.statusCode = 200;
        }
        else
        {
            Log.error('TimetableController', 'save', result.message);
            throw new Exceptions.ApplicationException(result.message);
        }
    }
}