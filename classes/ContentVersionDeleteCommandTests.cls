@isTest
public class ContentVersionDeleteCommandTests 
{
    @testSetup 
    public static void setup()
    {
        TestUtilities.setupHEDA();
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact contact = TestDataContactMock.createProspectCold(user, '04045566889');
        TestDataContentVersionMock.create(user, contact.Id);
    }

    @isTest
    static void DeleteFile_DeleteFileAllRelatedObjects()
    {
        // Arrange
        Id documentId = [SELECT ContentDocumentId FROM ContentVersion LIMIT 1].ContentDocumentId;

        // Act
        test.startTest();
        ActionResult result = ContentDocumentService.removeFile(documentId);
        test.stopTest();

        // Assert
        List<ContentVersion> files = [SELECT Id FROM ContentVersion WHERE ContentDocumentId = : documentId];
        List<ContentDocument> documents = [SELECT Id FROM ContentDocument WHERE Id = : documentId];
        List<ContentDocumentLink> links = [SELECT Id FROM ContentDocumentLink WHERE ContentDocumentId = : documentId];

        system.assertEquals(0, files.size());
        system.assertEquals(0, documents.size());
        system.assertEquals(0, links.size());
    }
}