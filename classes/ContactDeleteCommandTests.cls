@IsTest
public class ContactDeleteCommandTests
{
    @testSetup
    static void setup()
    {
        TestUtilities.setupHEDA();
    }

    @isTest
    static void CreateContactWithAccount_DeleteContactSuccessfully()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createStudent(user);

        // Act
        Test.startTest();
        ActionResult result = new ContactDeleteCommand().execute(new Set<Id> { contact.Id });
        Test.stopTest();
        List<Contact> contacts = [SELECT Id FROM Contact WHERE Id = :contact.Id];

        // Assert
        System.assertNotEquals(null, result);
        System.assertEquals(false, result.IsError, 'There should be no errors on deletion');
        System.assertEquals(0, contacts.size(), 'Contact should not exist');
    }
    
    @isTest
    static void CreateContactWithCase_DeleteContactAndCasesSuccessfully()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createStudent(user);
        Case contactCase = TestDataCaseMock.createAwardCase(user, contact);

        // Act
        Test.startTest();
        ActionResult result = new ContactDeleteCommand().execute(new Set<Id> { contact.Id });
        Test.stopTest();
        List<Contact> contacts = [SELECT Id FROM Contact WHERE Id = :contact.Id];
        List<Case> contactCases = [SELECT Id FROM Case WHERE Id = :contactCase.Id];

        // Assert
        System.assertNotEquals(null, result);
        System.assertEquals(false, result.IsError, 'There should be no errors on deletion');
        System.assertEquals(0, contacts.size(), 'Contact should not exist');
        System.assertEquals(0, contactCases.size(), 'Case should not exist');
    } 

    @isTest
    static void CreateContactWithOpportunity_DeleteContactAndCasesSuccessfully()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createStudent(user);
        Opportunity opp =  TestDataOpportunityMock.createCommencing(user, contact);

        // Act
        Test.startTest();
        ActionResult result = new ContactDeleteCommand().execute(new Set<Id> { contact.Id });
        Test.stopTest(); 
        List<Contact> contacts = [SELECT Id FROM Contact WHERE Id = :contact.Id];
        List<Opportunity> opps = [SELECT Id FROM Opportunity WHERE Id = :opp.Id];

        // Assert
        System.assertNotEquals(null, result);
        System.assertEquals(false, result.IsError, 'There should be no errors on deletion');
        System.assertEquals(0, contacts.size(), 'Contact should not exist');
        System.assertEquals(0, opps.size(), 'Opportunity should not exist');
    }

    @isTest
    static void CreateContactWithContactReferrer_DeleteContactAndContactReferrerSuccessfully()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createStudent(user);
        ContactReferrer__c referrer = TestDataContactReferrerMock.create(user, contact.id);

        // Act
        Test.startTest();
        ActionResult result = new ContactDeleteCommand().execute(new Set<Id> { contact.Id });
        Test.stopTest();
        List<Contact> contacts = [SELECT Id FROM Contact WHERE Id = :contact.Id];
        List<ContactReferrer__c> referrers = [SELECT Id, ContactID__c FROM ContactReferrer__c WHERE ContactID__c = :contact.Id];

        // Assert
        System.assertNotEquals(null, result);
        System.assertEquals(false, result.IsError, 'There should be no errors on deletion');
        System.assertEquals(0, contacts.size(), 'Contact should not exist');
        System.assertEquals(0, referrers.size(), 'Contact Referrer should not exist');
    }

}