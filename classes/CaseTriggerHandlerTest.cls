@isTest
private class CaseTriggerHandlerTest
{
    @testSetup
    public static void setup()
    {
        TestUtilities.setupHEDA();
    }

    // Test before insert trigger to calculate the Business Hours for the tFirst Email Response Time
    @isTest static void BeforeInsert_UpdateFirstEmailResponseTime()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createProspectHot(user);
        Case newCase = TestDataCaseMock.createStudentSupportCase(user, contact, false);
        newCase.First_Email_Student_Date_Time__c = Datetime.valueOf('2017-05-15 10:00:00');
        newCase.First_Email_Date_Time__c = Datetime.valueOf('2017-05-16 10:00:00');
        Decimal expect = DateTimeUtils.getTimeAsHours(DateTimeUtils.getBusinessElapsedTime(newCase.First_Email_Student_Date_Time__c, newCase.First_Email_Date_Time__c));

        // Act
        insert newCase;
        List<Case> result = [SELECT Id, First_Email_Response_Time__c FROM Case WHERE Id =: newCase.id];

        // Assert
        System.assertEquals(1, result.size(), 'One case should be created by insert');
        System.assertEquals(expect, result[0].First_Email_Response_Time__c, 'Incorrect calculation of the Business Hours for the First_Email_Response_Time__c ');
    }

    // Test before update trigger to calculate the Business Hours for the First Email Response Time
    @isTest static void BeforeUpdate_UpdateFirstEmailResponseTime()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createProspectHot(user);
        Case newCase = TestDataCaseMock.createStudentSupportCase(user, contact, false);
        newCase.First_Email_Student_Date_Time__c = Datetime.valueOf('2017-05-15 10:00:00');
        newCase.First_Email_Date_Time__c = Datetime.valueOf('2017-05-16 10:00:00');
        insert newCase;

        // Act
        newCase.First_Email_Date_Time__c = Datetime.valueOf('2017-05-17 10:00:00');
        Decimal expect = DateTimeUtils.getTimeAsHours(DateTimeUtils.getBusinessElapsedTime(newCase.First_Email_Student_Date_Time__c, newCase.First_Email_Date_Time__c));
        update newCase;
        List<Case> result = [SELECT Id, First_Email_Response_Time__c FROM Case WHERE Id =: newCase.id];

        // Assert
        System.assertEquals(1, result.size(), 'Only one case should be updated');
        System.assertEquals(expect, result[0].First_Email_Response_Time__c, 'Incorrect calculation of the Business Hours for the First_Email_Response_Time__c');
    }
}