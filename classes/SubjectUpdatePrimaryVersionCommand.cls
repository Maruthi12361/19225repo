public class SubjectUpdatePrimaryVersionCommand extends BaseTriggerCommand
{
    static Id subjectVersionRecordTypeId = RecordTypeUtils.getRecordTypeId(Constants.RECORDTYPE_SUBJECT_VERSION, SObjectType.hed__Course__c);

    public override List<SObject> validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        List<hed__Course__c> result = new List<hed__Course__c>();

        for (hed__Course__c subject : (List<hed__Course__c>)items)
        {
            if (subject.RecordTypeId == subjectVersionRecordTypeId)
            {
                if (triggerContext == Enums.TriggerContext.AFTER_INSERT)
                {
                    if (subject.IsPrimary__c && subject.SubjectDefinition__c != null)
                        result.add(subject);
                }
                else if (triggerContext == Enums.TriggerContext.AFTER_UPDATE)
                {
                    hed__Course__c oldValue = (hed__Course__c) oldMap.get(subject.Id);

                    if (subject.IsPrimary__c && subject.SubjectDefinition__c != null &&
                        (subject.IsPrimary__c != oldValue.IsPrimary__c ||
                         subject.SubjectDefinition__c != oldValue.SubjectDefinition__c))
                        result.add(subject);
                }
            }
        }

        return result;
    }
    
    public override ActionResult command(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {        
        Set<Id> subjectDefSet = new Set<Id>();
        Set<Id> subjectVerSet = new Set<Id>();

        for (hed__Course__c subject : (List<hed__Course__c>)items)
        {
            subjectDefSet.add(subject.SubjectDefinition__c);
            subjectVerSet.add(subject.Id);
        }

        List<hed__Course__c> subjects =
            [
                SELECT Id, IsPrimary__c
                FROM hed__Course__c
                WHERE Id NOT IN : subjectVerSet AND SubjectDefinition__c IN : subjectDefSet AND IsPrimary__c = True AND RecordTypeId = : subjectVersionRecordTypeId
            ];
        
        for (hed__Course__c subject : subjects)
        {
            subject.IsPrimary__c = false;
            uow.registerDirty(subject);
        }
        
        return new ActionResult();
    }
}