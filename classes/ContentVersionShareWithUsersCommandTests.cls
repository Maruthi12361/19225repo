@isTest
public class ContentVersionShareWithUsersCommandTests
{
    @testSetup
    public static void setup()
    {
        TestUtilities.setupHEDA();
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createProspectHot(user);
        TestDataOpportunityMock.createApplyingAcquisition(user, contact);
        TestDataContentVersionMock.create(UserService.getByAlias(Constants.USER_SYSTEMADMIN), contact.Id);
    }

    @isTest
    static void CreateNewFile_SharesFileWithAllUsers()
    {
        // Arrange
        Id contactId = [SELECT Id FROM Contact LIMIT 1].Id;
        List<ContentVersion> files = [SELECT Id, ContentDocumentId FROM ContentVersion LIMIT 1];
        Id fileId = files[0].ContentDocumentId;

        // Act
        ActionResult result = ContentDocumentService.shareWithAllUsers(files, new Map<Id, ContentVersion>(), Enums.TriggerContext.AFTER_INSERT);

        // Assert
        List<ContentDocumentLink> links =
            [
                SELECT Id, LinkedEntityId, ShareType, Visibility
                FROM ContentDocumentLink
                WHERE ContentDocumentId = : fileId AND LinkedEntityId = : UserInfo.getOrganizationId()
            ];
        System.assertEquals(ResultStatus.SUCCESS, result.Status);
        System.assertEquals(1, links.size());
        System.assertEquals(Constants.FILE_CONTENT_SHARETYPE_COLLABORTOR, links[0].ShareType);
        System.assertEquals(Constants.FILE_CONTENT_VISIBILITY_ALL, links[0].Visibility);
    }

    @isTest
    static void CreateFileAlreadySharedWithAllUsers_DontShareFileAgain()
    {
        // Arrange
        Id contactId = [SELECT Id FROM Contact LIMIT 1].Id;
        List<ContentVersion> files = [SELECT Id, ContentDocumentId FROM ContentVersion LIMIT 1];
        Id fileId = files[0].ContentDocumentId;

        // Act
        ActionResult result = ContentDocumentService.shareWithAllUsers(files, new Map<Id, ContentVersion>(), Enums.TriggerContext.AFTER_INSERT);
        result = ContentDocumentService.shareWithAllUsers(files, new Map<Id, ContentVersion>(), Enums.TriggerContext.AFTER_INSERT);

        // Assert
        List<ContentDocumentLink> links =
            [
                SELECT Id, LinkedEntityId, ShareType, Visibility
                FROM ContentDocumentLink
                WHERE ContentDocumentId = : fileId AND LinkedEntityId = : UserInfo.getOrganizationId()
            ];
        System.assertEquals(ResultStatus.SUCCESS, result.Status);
        System.assertEquals(1, links.size());
        System.assertEquals(Constants.FILE_CONTENT_SHARETYPE_COLLABORTOR, links[0].ShareType);
        System.assertEquals(Constants.FILE_CONTENT_VISIBILITY_ALL, links[0].Visibility);
    }
}