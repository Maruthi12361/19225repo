@isTest
private class ContactExpressConsentBatchTest
{
    private static final String query = ContactExpressConsentBatch.DEFAULT_QUERY + ' LIMIT 99';
    private static final Date EXPIRED_TEN_DAYS_AGO = System.today().addDays(-10);
    private static final Date EXPIRED_TODAY = System.today();

    @testSetup
    public static void setup()
    {
        TestUtilities.setupHEDA();
        User user = UserService.getByAlias('data');
        TestDataContactMock.createProspectHot(user, true);
    }

    @isTest
    static void UpdateContactWithinExpressConsentWindow_SetsExpressConsentFlagToTrue()
    {
        // Arrange

        // Act
        Test.startTest();
        ID batchProcessId = Database.executeBatch(new ContactExpressConsentBatch(query), 99);
        Test.stopTest();

        // Assert
        Contact actual = ContactService.get([SELECT Id FROM Contact LIMIT 1].Id);
        System.assertEquals(true, actual.Express_Consent_Exists__c);
    }

    @isTest
    static void UpdateContactOutsideExpressConsentWindow_SetsExpressConsentFlagToFalse()
    {
        // Arrange
        Contact con = [SELECT Id, Express_Consent_Expires__c, Express_Consent_Exists__c FROM Contact LIMIT 1];
        con.Express_Consent_Exists__c = true;
        con.Express_Consent_Expires__c = EXPIRED_TODAY;
        update con;

        // Act
        Test.startTest();
        ID batchProcessId = Database.executeBatch(new ContactExpressConsentBatch(query), 99);
        Test.stopTest();

        // Assert
        Contact actual = ContactService.get(con.Id);
        System.assertEquals(false, actual.Express_Consent_Exists__c);
    }

    @isTest
    static void UpdateContactWithDoNotCall_SetOKToCallToNOTOKOnAnyNumber()
    {
        // Arrange
        Contact con = [SELECT Id, DoNotCall, dncau__Phone_DNC_Status__c, dncau__Phone_DNC_Checked__c, dncau__MobilePhone_DNC_Status__c, dncau__MobilePhone_DNC_Checked__c,
                          dncau__OtherPhone_DNC_Status__c , dncau__OtherPhone_DNC_Checked__c, dncau__HomePhone_DNC_Status__c, dncau__HomePhone_DNC_Checked__c, 
                          WorkPhone_DNC_Status__c, WorkPhone_DNC_Checked__c, Express_Consent_Expires__c, Express_Consent_Exists__c
                       FROM Contact LIMIT 1];

        con.DoNotCall = true;
        update con;

        // Act
        Test.startTest();
        ID batchProcessId = Database.executeBatch(new ContactExpressConsentBatch(query), 99);
        Test.stopTest();

        // Assert
        Contact actual = ContactService.get(con.Id);
        System.assertEquals(Constants.DNC_NOT_OK_ANY_NUMBER, actual.OK_To_Call__c);
    }

    @isTest
    static void UpdateContactWithDNCCheckWithin28DaysForAllPhones_SetsOKToCallToOKAnyNumber()
    {
        // Arrange
        Contact con = [SELECT Id, DoNotCall, dncau__Phone_DNC_Status__c, dncau__Phone_DNC_Checked__c, dncau__MobilePhone_DNC_Status__c, dncau__MobilePhone_DNC_Checked__c,
                          dncau__OtherPhone_DNC_Status__c , dncau__OtherPhone_DNC_Checked__c, dncau__HomePhone_DNC_Status__c, dncau__HomePhone_DNC_Checked__c, 
                          WorkPhone_DNC_Status__c, WorkPhone_DNC_Checked__c, Express_Consent_Expires__c, Express_Consent_Exists__c
                       FROM Contact LIMIT 1];

        con.Express_Consent_Exists__c = true;
        con.Express_Consent_Expires__c = EXPIRED_TODAY;
        con.dncau__MobilePhone_DNC_Status__c = Constants.DNC_CAN_CALL_STATUS;
        con.dncau__Phone_DNC_Status__c = Constants.DNC_CAN_CALL_STATUS;
        con.dncau__OtherPhone_DNC_Status__c = Constants.DNC_CAN_CALL_STATUS;
        con.dncau__HomePhone_DNC_Status__c = Constants.DNC_CAN_CALL_STATUS;
        con.WorkPhone_DNC_Status__c = Constants.DNC_CAN_CALL_STATUS;
        con.dncau__Phone_DNC_Checked__c = EXPIRED_TEN_DAYS_AGO;
        con.dncau__MobilePhone_DNC_Checked__c = EXPIRED_TEN_DAYS_AGO;
        con.dncau__OtherPhone_DNC_Checked__c = EXPIRED_TEN_DAYS_AGO;
        con.dncau__HomePhone_DNC_Checked__c = EXPIRED_TEN_DAYS_AGO;
        con.WorkPhone_DNC_Checked__c = EXPIRED_TEN_DAYS_AGO;
        update con;

        // Act
        Test.startTest();
        ID batchProcessId = Database.executeBatch(new ContactExpressConsentBatch(query), 99);
        Test.stopTest();

        // Assert
        Contact actual = ContactService.get(con.Id);
        System.assertEquals(Constants.DNC_OK_ANY_NUMBER, actual.OK_To_Call__c);
    }

    @isTest
    static void UpdateContactWithDNCCheckWithin28DaysForMobileAndPrimaryPhone_SetsOKToCallToOKMobileAndPrimaryPhone()
    {
        // Arrange
        Contact con = [SELECT Id, DoNotCall, dncau__Phone_DNC_Status__c, dncau__Phone_DNC_Checked__c, dncau__MobilePhone_DNC_Status__c, dncau__MobilePhone_DNC_Checked__c,
                          dncau__OtherPhone_DNC_Status__c , dncau__OtherPhone_DNC_Checked__c, dncau__HomePhone_DNC_Status__c, dncau__HomePhone_DNC_Checked__c, 
                          WorkPhone_DNC_Status__c, WorkPhone_DNC_Checked__c, Express_Consent_Expires__c, Express_Consent_Exists__c
                       FROM Contact LIMIT 1];

        con.Express_Consent_Exists__c = true;
        con.Express_Consent_Expires__c = EXPIRED_TODAY;
        con.dncau__MobilePhone_DNC_Status__c = Constants.DNC_CAN_CALL_STATUS;
        con.dncau__Phone_DNC_Status__c = Constants.DNC_CAN_CALL_STATUS;
        con.dncau__Phone_DNC_Checked__c = EXPIRED_TEN_DAYS_AGO;
        con.dncau__MobilePhone_DNC_Checked__c = EXPIRED_TEN_DAYS_AGO;
        update con;

        // Act
        Test.startTest();
        ID batchProcessId = Database.executeBatch(new ContactExpressConsentBatch(query), 99);
        Test.stopTest();

        // Assert
        Contact actual = ContactService.get(con.Id);
        System.assertEquals('OK - Phone, Mobile Phone', actual.OK_To_Call__c);
    }

    @isTest 
    static void UpdateContactWithDNCCheckWithin28DaysForMobileAndOther_SetsOKToCallToOKMobileAndWork()
    {
        // Arrange
        Contact con = [SELECT Id, DoNotCall, dncau__Phone_DNC_Status__c, dncau__Phone_DNC_Checked__c, dncau__MobilePhone_DNC_Status__c, dncau__MobilePhone_DNC_Checked__c,
                          dncau__OtherPhone_DNC_Status__c , dncau__OtherPhone_DNC_Checked__c, dncau__HomePhone_DNC_Status__c, dncau__HomePhone_DNC_Checked__c, 
                          WorkPhone_DNC_Status__c, WorkPhone_DNC_Checked__c, Express_Consent_Expires__c, Express_Consent_Exists__c
                       FROM Contact LIMIT 1];

        con.Express_Consent_Exists__c = true;
        con.Express_Consent_Expires__c = EXPIRED_TODAY;
        con.dncau__MobilePhone_DNC_Status__c = Constants.DNC_CAN_CALL_STATUS;
        con.WorkPhone_DNC_Status__c = Constants.DNC_CAN_CALL_STATUS;
        con.dncau__MobilePhone_DNC_Checked__c = EXPIRED_TEN_DAYS_AGO;
        con.WorkPhone_DNC_Checked__c = EXPIRED_TEN_DAYS_AGO;
        update con;

        // Act
        Test.startTest();
        ID batchProcessId = Database.executeBatch(new ContactExpressConsentBatch(query), 99);
        Test.stopTest();

       // Assert
        Contact actual = ContactService.get(con.Id);
        System.assertEquals('OK - Mobile Phone, Work Phone', actual.OK_To_Call__c);
    }

    @isTest
    static void UpdateContactWithDNCCheckWithin28DaysForPrimaryAndOther_SetsOKToCallToOKPrimaryAndHome()
    {
        // Arrange
        Contact con = [SELECT Id, DoNotCall, dncau__Phone_DNC_Status__c, dncau__Phone_DNC_Checked__c, dncau__MobilePhone_DNC_Status__c, dncau__MobilePhone_DNC_Checked__c,
                          dncau__OtherPhone_DNC_Status__c , dncau__OtherPhone_DNC_Checked__c, dncau__HomePhone_DNC_Status__c, dncau__HomePhone_DNC_Checked__c, 
                          WorkPhone_DNC_Status__c, WorkPhone_DNC_Checked__c, Express_Consent_Expires__c, Express_Consent_Exists__c
                       FROM Contact LIMIT 1];

        con.Express_Consent_Exists__c = true;
        con.Express_Consent_Expires__c = EXPIRED_TODAY;
        con.dncau__Phone_DNC_Status__c = Constants.DNC_CAN_CALL_STATUS;
        con.dncau__HomePhone_DNC_Status__c = Constants.DNC_CAN_CALL_STATUS;
        con.dncau__Phone_DNC_Checked__c = EXPIRED_TEN_DAYS_AGO;
        con.dncau__HomePhone_DNC_Checked__c = EXPIRED_TEN_DAYS_AGO;
        update con;

        // Act
        Test.startTest();
        ID batchProcessId = Database.executeBatch(new ContactExpressConsentBatch(query), 99);
        Test.stopTest();

        // Assert
        Contact actual = ContactService.get(con.Id);
        System.assertEquals('OK - Phone, Home Phone', actual.OK_To_Call__c);
    }

    @isTest
    static void UpdateContactWithDNCCheckWithin28DaysForMobileOnly_SetsOKToCallToOKMobileOnly()
    {
        // Arrange
        Contact con = [SELECT Id, DoNotCall, dncau__Phone_DNC_Status__c, dncau__Phone_DNC_Checked__c, dncau__MobilePhone_DNC_Status__c, dncau__MobilePhone_DNC_Checked__c,
                          dncau__OtherPhone_DNC_Status__c , dncau__OtherPhone_DNC_Checked__c, dncau__HomePhone_DNC_Status__c, dncau__HomePhone_DNC_Checked__c, 
                          WorkPhone_DNC_Status__c, WorkPhone_DNC_Checked__c, Express_Consent_Expires__c, Express_Consent_Exists__c
                       FROM Contact LIMIT 1];

        con.Express_Consent_Exists__c = true;
        con.Express_Consent_Expires__c = EXPIRED_TODAY;
        con.dncau__MobilePhone_DNC_Status__c = Constants.DNC_CAN_CALL_STATUS;
        con.dncau__MobilePhone_DNC_Checked__c = EXPIRED_TEN_DAYS_AGO;
        update con;

        // Act
        Test.startTest();
        ID batchProcessId = Database.executeBatch(new ContactExpressConsentBatch(query), 99);
        Test.stopTest();

        // Assert
        Contact actual = ContactService.get(con.Id);
        System.assertEquals('OK - Mobile Phone', actual.OK_To_Call__c);
    }

    @isTest 
    static void UpdateContactWithDNCCheckWithin28DaysForPrimaryPhoneOnly_SetsOKToCallToOKPrimaryPhoneOnly()
    {
        // Arrange
        Contact con = [SELECT Id, DoNotCall, dncau__Phone_DNC_Status__c, dncau__Phone_DNC_Checked__c, dncau__MobilePhone_DNC_Status__c, dncau__MobilePhone_DNC_Checked__c,
                          dncau__OtherPhone_DNC_Status__c , dncau__OtherPhone_DNC_Checked__c, dncau__HomePhone_DNC_Status__c, dncau__HomePhone_DNC_Checked__c, 
                          WorkPhone_DNC_Status__c, WorkPhone_DNC_Checked__c, Express_Consent_Expires__c, Express_Consent_Exists__c
                       FROM Contact LIMIT 1];

        con.Express_Consent_Exists__c = true;
        con.Express_Consent_Expires__c = EXPIRED_TODAY;
        con.dncau__Phone_DNC_Status__c = Constants.DNC_CAN_CALL_STATUS;
        con.dncau__Phone_DNC_Checked__c = EXPIRED_TEN_DAYS_AGO;
        update con;

        // Act
        Test.startTest();
        ID batchProcessId = Database.executeBatch(new ContactExpressConsentBatch(query), 99);
        Test.stopTest();

       // Assert
        Contact actual = ContactService.get(con.Id);
        System.assertEquals('OK - Phone', actual.OK_To_Call__c);
    }

    @isTest
    static void UpdateContactWithDNCCheckWithin28DaysForOtherPhoneOnly_SetsOKToCallToOKOtherPhoneOnly()
    {
        // Arrange
        Contact con = [SELECT Id, DoNotCall, dncau__Phone_DNC_Status__c, dncau__Phone_DNC_Checked__c, dncau__MobilePhone_DNC_Status__c, dncau__MobilePhone_DNC_Checked__c,
                          dncau__OtherPhone_DNC_Status__c , dncau__OtherPhone_DNC_Checked__c, dncau__HomePhone_DNC_Status__c, dncau__HomePhone_DNC_Checked__c, 
                          WorkPhone_DNC_Status__c, WorkPhone_DNC_Checked__c, Express_Consent_Expires__c, Express_Consent_Exists__c
                       FROM Contact LIMIT 1];

        con.Express_Consent_Exists__c = true;
        con.Express_Consent_Expires__c = EXPIRED_TODAY;
        con.dncau__OtherPhone_DNC_Status__c = Constants.DNC_CAN_CALL_STATUS;
        con.dncau__OtherPhone_DNC_Checked__c = EXPIRED_TEN_DAYS_AGO;
        update con;

        // Act
        Test.startTest();
        ID batchProcessId = Database.executeBatch(new ContactExpressConsentBatch(query), 99);
        Test.stopTest();

        // Assert
        Contact actual = ContactService.get(con.Id);
        System.assertEquals('OK - Other Phone', actual.OK_To_Call__c);
    }

    @isTest
    static void UpdateContactWithDNCCheckOutside28DaysForAllPhones_SetsOKToCallToNOTOKOnAnyNumber()
    {
        // Arrange
        Contact con = [SELECT Id, DoNotCall, dncau__Phone_DNC_Status__c, dncau__Phone_DNC_Checked__c, dncau__MobilePhone_DNC_Status__c, dncau__MobilePhone_DNC_Checked__c,
                          dncau__OtherPhone_DNC_Status__c , dncau__OtherPhone_DNC_Checked__c, dncau__HomePhone_DNC_Status__c, dncau__HomePhone_DNC_Checked__c, 
                          WorkPhone_DNC_Status__c, WorkPhone_DNC_Checked__c, Express_Consent_Expires__c, Express_Consent_Exists__c
                       FROM Contact LIMIT 1];

        con.Express_Consent_Exists__c = true;
        con.Express_Consent_Expires__c = EXPIRED_TODAY;
        con.dncau__MobilePhone_DNC_Status__c = Constants.DNC_CAN_CALL_STATUS;
        con.dncau__Phone_DNC_Status__c = Constants.DNC_CAN_CALL_STATUS;
        con.dncau__OtherPhone_DNC_Status__c = Constants.DNC_CAN_CALL_STATUS;
        con.dncau__HomePhone_DNC_Status__c = Constants.DNC_CAN_CALL_STATUS;
        con.WorkPhone_DNC_Status__c = Constants.DNC_CAN_CALL_STATUS;
        Date expire = System.today().addDays(-31);
        con.dncau__Phone_DNC_Checked__c = expire;
        con.dncau__MobilePhone_DNC_Checked__c = expire;
        con.dncau__OtherPhone_DNC_Checked__c = expire;
        con.dncau__HomePhone_DNC_Checked__c = expire;
        con.WorkPhone_DNC_Checked__c = expire;
        update con;

        // Act
        Test.startTest();
        ID batchProcessId = Database.executeBatch(new ContactExpressConsentBatch(query), 99);
        Test.stopTest();

        // Assert
        Contact actual = ContactService.get(con.Id);
        System.assertEquals(Constants.DNC_NOT_OK_ANY_NUMBER, actual.OK_To_Call__c);
    }
}