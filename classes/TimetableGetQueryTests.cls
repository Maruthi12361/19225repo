@isTest
public class TimetableGetQueryTests
{
    @testSetup
    public static void setup()
    {
        TestUtilities.setupHEDA();
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact contact = TestDataContactMock.createProspectCold(user, '0455548889');
        ViewModelsTestData.Data mockData = TestDataAcademicConfigMock.create(user);
    }

    @isTest
    public static void NoParamIdentifer_NoDataReturnedFromService()
    {
        // Arrange
        ViewModels.Param param = new ViewModels.Param();
        param.Identifier = '';
        param.IsAuthenticated = true;

        // Act
        test.startTest();
        ViewModelsTimetable.Data data = TimetableService.getTimetable(param);
        test.stopTest();

        // Assert
        System.assertEquals(null, data, 'Unexpected data being returned');
    }

    @isTest
    public static void IsAuthenticatedSetToFalse_NoDataReturnedFromService()
    {
        // Arrange
        ViewModels.Param param = new ViewModels.Param();
        param.Identifier = 'some.email@test.com';
        param.IsAuthenticated = false;

        // Act
        test.startTest();
        ViewModelsTimetable.Data data = TimetableService.getTimetable(param);
        test.stopTest();

        // Assert
        System.assertEquals(null, data, 'Unexpected data being returned');
    }

    @isTest
    public static void ContactNotEnrolledInAnyCourse_PersonalTimetableFieldValuesReturnedSuccessfully()
    {
        // Arrange
        Contact contact = [SELECT Id, FirstName, LastName, EncryptedId__c, RecordType.Name FROM Contact LIMIT 1];
        ViewModels.Param param = new ViewModels.Param();
        param.Identifier = contact.EncryptedId__c;
        param.IsAuthenticated = true;

        // Act
        test.startTest();
        ViewModelsTimetable.Data data = TimetableService.getTimetable(param);
        test.stopTest();

        // Assert
        System.assertEquals(contact.FirstName, data.Details.FirstName, 'Unexpected FirstName value');
        System.assertEquals(contact.LastName, data.Details.LastName, 'Unexpected LastName value');
        System.assertEquals(true, data.Details.FeeHelpEligible, 'Unexpected FeeHelpEligible value');
        System.assertEquals(0.00, data.Details.FeesInCredit, 'Unexpected FeesInCredit value');
        System.assertEquals(0.00, data.Details.FeeHelpCredit, 'Unexpected FeeHelpCredit value');
        System.assertEquals(contact.RecordType.Name, data.Details.RecordType, 'Unexpected RecordType value');
        System.assertEquals(0, data.Courses.size(), 'Unexpected number of courses');
        System.assertEquals(0, data.Subjects.size(), 'Unexpected number of subjects');
    }

    @isTest
    public static void ContactEnrolledInSingleCourseNoSubjects_PersonalAncCourseTimetableFieldValuesReturnedSuccessfully()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact contact = [SELECT Id, FirstName, LastName, EncryptedId__c, RecordType.Name FROM Contact LIMIT 1];
        Account course = [SELECT Id, ProductName__c FROM Account WHERE ProductName__c = 'MBA' LIMIT 1];
        hed__Program_Plan__c coursePlan = [SELECT Id, Name FROM hed__Program_Plan__c WHERE Name = 'MBA v1' LIMIT 1];

        TestDataCourseEnrolmentMock.create(user, course.Id, contact.Id, 'Approved', coursePlan.Id);

        ViewModels.Param param = new ViewModels.Param();
        param.Identifier = contact.EncryptedId__c;
        param.IsAuthenticated = true;

        // Act
        test.startTest();
        ViewModelsTimetable.Data data = TimetableService.getTimetable(param);
        test.stopTest();

        // Assert
        System.assertEquals(contact.FirstName, data.Details.FirstName, 'Unexpected FirstName value');
        System.assertEquals(contact.LastName, data.Details.LastName, 'Unexpected LastName value');
        System.assertEquals(true, data.Details.FeeHelpEligible, 'Unexpected FeeHelpEligible value');
        System.assertEquals(0.00, data.Details.FeesInCredit, 'Unexpected FeesInCredit value');
        System.assertEquals(0.00, data.Details.FeeHelpCredit, 'Unexpected FeeHelpCredit value');
        System.assertEquals(contact.RecordType.Name, data.Details.RecordType, 'Unexpected RecordType value');
        System.assertEquals(1, data.Courses.size(), 'Unexpected number of courses');
        System.assertEquals(0, data.Subjects.size(), 'Unexpected number of subjects');

        ViewModelsTimetable.Course  contactCourse = data.Courses[0];
        System.assertEquals(course.Id, contactCourse.CourseId, 'Unexpected course identifier');
        System.assertEquals(coursePlan.Id, contactCourse.CoursePlanId, 'Unexpected course plan identifier');
        System.assertEquals(null, contactCourse.CoursePathway, 'Unexpected course plan identifier');
        System.assertEquals('Approved', contactCourse.Status, 'Unexpected course plan identifier');
        System.assertEquals(Date.today(), contactCourse.Commenced, 'Unexpected commenced date');
    }

    @isTest
    public static void ContactEnrolledInSingleCourseWithSubject_TimetableFieldValuesReturnedSuccessfully()
    {
        // Arrange
        Date today = Date.today();
        Date earliestDateCanEnroll = today.addDays(42);
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact contact = [SELECT Id, FirstName, LastName, EncryptedId__c, RecordType.Name FROM Contact LIMIT 1];
        Account course = [SELECT Id, ProductName__c FROM Account WHERE ProductName__c = 'MBA' LIMIT 1];
        hed__Program_Plan__c coursePlan = [SELECT Id, Name FROM hed__Program_Plan__c WHERE Name = 'MBA v1' LIMIT 1];

        hed__Plan_Requirement__c planRequirement =
        [
            SELECT Id, hed__Course__c, hed__Program_Plan__c, hed__Plan_Requirement__r.hed__Program_Plan__c
            FROM hed__Plan_Requirement__c
            WHERE hed__Plan_Requirement__r.hed__Program_Plan__c = :coursePlan.id AND hed__Course__c != null
                AND hed__Plan_Requirement__r.RecordType.DeveloperName = 'CoursePlanRequirement'
            LIMIT 1
        ];
        hed__Course_Offering__c subjectOffering =
        [
            SELECT Id, hed__Course__r.SubjectDefinition__c
            FROM hed__Course_Offering__c
            WHERE hed__Course__r.SubjectDefinition__c = :planRequirement.hed__Course__c LIMIT 1
        ];

        hed__Program_Enrollment__c courseEnrolment = TestDataCourseEnrolmentMock.create(user, course.Id, contact.Id, 'Approved', coursePlan.Id);
        hed__Course_Enrollment__c subjectEnrolment = TestDataSubjectEnrolmentMock.create(user, subjectOffering, courseEnrolment, 'Enrolled', 'Standard', null, null);
        Date subjectEnrolmentStartdate = [SELECT Id, SubjectOfferingStartDate__c FROM hed__Course_Enrollment__c WHERE Id = :subjectEnrolment.Id].SubjectOfferingStartDate__c;
        Boolean canEnroll =  today < subjectEnrolmentStartdate && subjectEnrolmentStartdate < earliestDateCanEnroll;

        ViewModels.Param param = new ViewModels.Param();
        param.Identifier = contact.EncryptedId__c;
        param.IsAuthenticated = true;

        // Act
        test.startTest();
        ViewModelsTimetable.Data data = TimetableService.getTimetable(param);
        test.stopTest();

        // Assert
        System.assertEquals(contact.FirstName, data.Details.FirstName, 'Unexpected FirstName value');
        System.assertEquals(contact.LastName, data.Details.LastName, 'Unexpected LastName value');
        System.assertEquals(true, data.Details.FeeHelpEligible, 'Unexpected FeeHelpEligible value');
        System.assertEquals(0.00, data.Details.FeesInCredit, 'Unexpected FeesInCredit value');
        System.assertEquals(0.00, data.Details.FeeHelpCredit, 'Unexpected FeeHelpCredit value');
        System.assertEquals(contact.RecordType.Name, data.Details.RecordType, 'Unexpected RecordType value');
        System.assertEquals(1, data.Courses.size(), 'Unexpected number of courses');
        System.assertEquals(1, data.Subjects.size(), 'Unexpected number of subjects');

        ViewModelsTimetable.Course  contactCourse = data.Courses[0];
        System.assertEquals(course.Id, contactCourse.CourseId, 'Unexpected course identifier');
        System.assertEquals(coursePlan.Id, contactCourse.CoursePlanId, 'Unexpected course plan identifier');
        System.assertEquals(null, contactCourse.CoursePathway, 'Unexpected course plan identifier');
        System.assertEquals('Approved', contactCourse.Status, 'Unexpected course plan identifier');
        System.assertEquals(Date.today(), contactCourse.Commenced, 'Unexpected commenced date');

        ViewModelsTimetable.Subject subject = data.Subjects[0];
        System.assertNotEquals(null, subject.Id, 'Unexpected subject id value');
        System.assertNotEquals(null, subject.TermId, 'Unexpected term id value');
        System.assertEquals(null, subject.Grade, 'Unexpected grade value');
        System.assertEquals('Enrolled', subject.Status, 'Unexpected status value');
        System.assertEquals('Standard', subject.Type, 'Unexpected type value');
        System.assertEquals(2725.00, subject.Price, 'Unexpected price value');
        System.assertEquals(0.00, subject.Discount, 'Unexpected discount value');
        System.assertEquals(canEnroll, subject.CanEnrol, 'Unexpected can enrol value');
    }

    @isTest
    public static void ContactEnrolledInSingleCourseWithVariousSubjectTypes_TimetableFieldValuesReturnedSuccessfully()
    {
        // Arrange
        Date today = Date.today();
        Boolean studentSubjectCanEnrol = false;
        Date earliestDateCanEnroll = today.addDays(42);
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact contact = [SELECT Id, FirstName, LastName, EncryptedId__c, RecordType.Name FROM Contact LIMIT 1];
        Account course = [SELECT Id, ProductName__c FROM Account WHERE ProductName__c = 'MBA' LIMIT 1];
        hed__Program_Plan__c coursePlan = [SELECT Id, Name FROM hed__Program_Plan__c WHERE Name = 'MBA v1' LIMIT 1];

        List<hed__Plan_Requirement__c> planRequirements =
        [
            SELECT Id, hed__Course__c, hed__Program_Plan__c, hed__Plan_Requirement__r.hed__Program_Plan__c
            FROM hed__Plan_Requirement__c
            WHERE hed__Plan_Requirement__r.hed__Program_Plan__c = :coursePlan.id AND hed__Course__c != null
                AND hed__Plan_Requirement__r.RecordType.DeveloperName = 'CoursePlanRequirement'
            LIMIT 2
        ];
        Set<Id> planRequirementSubjectDefinitionIds = SObjectUtils.getIds(planRequirements, 'hed__Course__c');
        List<hed__Course_Offering__c> subjectOfferings =
        [
            SELECT Id, hed__Course__r.SubjectDefinition__c
            FROM hed__Course_Offering__c
            WHERE hed__Course__r.SubjectDefinition__c in :planRequirementSubjectDefinitionIds
            LIMIT 2
        ];

        hed__Program_Enrollment__c courseEnrolment = TestDataCourseEnrolmentMock.create(user, course.Id, contact.Id, 'Approved', coursePlan.Id);
        hed__Course_Enrollment__c studentSubjectEnrolment = TestDataSubjectEnrolmentMock.create(user, subjectOfferings[0], courseEnrolment, 'Enrolled', 'Standard', null, null);
        hed__Course_Enrollment__c provisionalSubjectEnrolment = TestDataSubjectEnrolmentMock.createProvisionalEnrolment(user, subjectOfferings[1], contact, 'Interested', 'Virtual', courseEnrolment);

        hed__Course__c breakSubjectVersion = TestDataSubjectMock.createVersionBreak(user, true, 1);
        hed__Term__c breakTerm = [SELECT Id, FirstSubjectStartDate__c FROM hed__Term__c LIMIT 1];
        hed__Course_Offering__c breakSubjectOffering = TestDataSubjectOfferingMock.createOffering(user, breakSubjectVersion, breakTerm, Constants.SUBJECTOFFERING_TYPE_BREAK);
        hed__Course_Enrollment__c breakSubjectEnrolment = TestDataSubjectEnrolmentMock.createProvisionalEnrolment(user, breakSubjectOffering, contact, 'Interested', 'Break', courseEnrolment);

        Set<Id> enrolmentIds = new Set<Id> { studentSubjectEnrolment.Id, provisionalSubjectEnrolment.Id, breakSubjectEnrolment.Id };
        List<hed__Course_Enrollment__c> enrolments = [SELECT Id, SubjectOfferingStartDate__c FROM hed__Course_Enrollment__c WHERE Id IN :enrolmentIds];

        for (hed__Course_Enrollment__c enrolment : enrolments)
        {
            Boolean canEnrol = today < enrolment.SubjectOfferingStartDate__c && enrolment.SubjectOfferingStartDate__c < earliestDateCanEnroll;
            if (enrolment.Id == studentSubjectEnrolment.Id & canEnrol)
                studentSubjectCanEnrol = true;
        }

        ViewModels.Param param = new ViewModels.Param();
        param.Identifier = contact.EncryptedId__c;
        param.IsAuthenticated = true;

        // Act
        test.startTest();
        ViewModelsTimetable.Data data = TimetableService.getTimetable(param);
        test.stopTest();

        // Assert
        System.assertEquals(contact.FirstName, data.Details.FirstName, 'Unexpected FirstName value');
        System.assertEquals(contact.LastName, data.Details.LastName, 'Unexpected LastName value');
        System.assertEquals(true, data.Details.FeeHelpEligible, 'Unexpected FeeHelpEligible value');
        System.assertEquals(0.00, data.Details.FeesInCredit, 'Unexpected FeesInCredit value');
        System.assertEquals(0.00, data.Details.FeeHelpCredit, 'Unexpected FeeHelpCredit value');
        System.assertEquals(contact.RecordType.Name, data.Details.RecordType, 'Unexpected RecordType value');
        System.assertEquals(1, data.Courses.size(), 'Unexpected number of courses');
        System.assertEquals(3, data.Subjects.size(), 'Unexpected number of subjects');

        ViewModelsTimetable.Course  contactCourse = data.Courses[0];
        System.assertEquals(course.Id, contactCourse.CourseId, 'Unexpected course identifier');
        System.assertEquals(coursePlan.Id, contactCourse.CoursePlanId, 'Unexpected course plan identifier');
        System.assertEquals(null, contactCourse.CoursePathway, 'Unexpected course plan identifier');
        System.assertEquals('Approved', contactCourse.Status, 'Unexpected course plan identifier');
        System.assertEquals(Date.today(), contactCourse.Commenced, 'Unexpected commenced date');

        for (ViewModelsTimetable.Subject subject : data.Subjects)
        {
            if (subject.Id == studentSubjectEnrolment.Id)
            {
                System.assertNotEquals(null, subject.TermId, 'Unexpected student subject term id value');
                System.assertEquals(null, subject.Grade, 'Unexpected student subject grade value');
                System.assertEquals('Enrolled', subject.Status, 'Unexpected student subject status value');
                System.assertEquals('Standard', subject.Type, 'Unexpected student subject type value');
                System.assertEquals(2725.00, subject.Price, 'Unexpected student subject price value');
                System.assertEquals(0.00, subject.Discount, 'Unexpected student subject discount value');
                System.assertEquals(studentSubjectCanEnrol, subject.CanEnrol, 'Unexpected student subject can enrol value');
            }
            else if (subject.Id == provisionalSubjectEnrolment.Id)
            {
                System.assertNotEquals(null, subject.TermId, 'Unexpected virtual subject term id value');
                System.assertEquals(null, subject.Grade, 'Unexpected virtual subject grade value');
                System.assertEquals('Interested', subject.Status, 'Unexpected virtual subject status value');
                System.assertEquals('Virtual', subject.Type, 'Unexpected virtual subject type value');
                System.assertEquals(null, subject.Price, 'Unexpected virtual subject price value');
                System.assertEquals(null, subject.Discount, 'Unexpected virtual subject discount value');
                System.assertEquals(false, subject.CanEnrol, 'Unexpected virtual subject can enrol value');
            }
            else
            {
                System.assertEquals(breakSubjectEnrolment.Id, subject.Id, 'Unexpected break subject id value');
                System.assertNotEquals(null, subject.TermId, 'Unexpected break subject term id value');
                System.assertEquals(null, subject.Grade, 'Unexpected break subject grade value');
                System.assertEquals('Interested', subject.Status, 'Unexpected break subject status value');
                System.assertEquals('Break', subject.Type, 'Unexpected break subject type value');
                System.assertEquals(null, subject.Price, 'Unexpected break subject price value');
                System.assertEquals(null, subject.Discount, 'Unexpected break subject discount value');
                System.assertEquals(false, subject.CanEnrol, 'Unexpected break subject can enrol value');
            }
        }
    }
}