public class PlanRequirementService
{
    public static ActionResult updateParent(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        return new PlanRequirementUpdateParentCommand().execute(items, oldMap, triggerContext);
    }
}