@isTest
public class AttachmentDeleteCommandTest 
{
    @testSetup 
    public static void setup()
    {
        TestUtilities.setupHEDA();
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact contact = TestDataContactMock.createProspectCold(user, '04045566889');
        TestDataAttachmentMock.create(user, contact.Id);
    }

    @isTest 
    static void DeletetingExistingAttachment_AttachmentSuccessfullyDeleted() 
    {
        // Arrange 
        Attachment attachment = [SELECT Id FROM Attachment LIMIT 1];

        // Act
        test.startTest();
        ActionResult result = new AttachmentDeleteCommand().execute(new Set<ID>{ attachment.id });
        test.stopTest();

        List<Attachment> attachments = [SELECT Id FROM Attachment WHERE Id = :attachment.Id];

        // Assert
        System.assertEquals(true, result.isSuccess, 'Unexpected status result');
        System.assertEquals(0, attachments.size(), 'Attachment is suppose to be delete it');
    }
}