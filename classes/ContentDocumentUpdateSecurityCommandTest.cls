@isTest
public class ContentDocumentUpdateSecurityCommandTest
{
    @testSetup
    public static void setup()
    {
        TestUtilities.setupHEDA();
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createProspectHot(user);
        TestDataOpportunityMock.createApplyingAcquisition(user, contact);
        TestDataCaseMock.createStudentSupportCase(user, contact);
    }

    @isTest
    static void CreateNewFile_SharesFileWithAllCaseUsers()
    {
        // Arrange
        ActionResult result;
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Case newCase = [SELECT Id FROM Case LIMIT 1];
        Id parentId = newCase.Id;
        String fileName = 'Test File.txt';
        String type = 'Proof of ID';
        String fileContent = 'Test Test and Test\nTest';
        Blob data = Blob.valueOf(fileContent);
        Id versionId;

        System.RunAs(user)
        {
            result = ContentDocumentService.createFile(fileName, type, data, parentId, null, null);
        }

        List<ContentVersion> versions = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id =: result.resultId];
        List<ContentDocumentLink> links = [SELECT Id, ContentDocumentId, LinkedEntityId, ShareType, Visibility FROM ContentDocumentLink WHERE ContentDocumentId =: versions[0].ContentDocumentId AND LinkedEntityId =: parentId];

        // Act
        result = ContentDocumentService.updateSecurity(links, new Map<Id, ContentDocumentLink>(), Enums.TriggerContext.BEFORE_INSERT);

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.Status);
        System.assertEquals(1, links.size());
        System.assertEquals(Constants.FILE_CONTENT_SHARETYPE_INFERRED, links[0].ShareType);
        System.assertEquals(Constants.FILE_CONTENT_VISIBILITY_ALL, links[0].Visibility);
    }

    @isTest
    static void CreateNewFile_ShareWithStudentPortalLoginProfileUserInCommunity()
    {
        // Arrange
        ActionResult result;
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact con = [SELECT Id FROM Contact LIMIT 1];
        Case newCase = [SELECT Id FROM Case LIMIT 1];
        Id parentId = newCase.Id;
        Id communityId = [SELECT Id FROM network WHERE name = 'My AIB' LIMIT 1].Id;
        String fileName = 'Test File.txt';
        String type = 'Proof of ID';
        String fileContent = 'Test Test and Test\nTest';
        Blob data = Blob.valueOf(fileContent);
        User studentPortalProfileUser = TestDataUserMock.createStudentPortalLoginProfileUser(user, con.Id);

        System.RunAs(studentPortalProfileUser)
        {
            result = ContentDocumentService.createFile(fileName, type, data, parentId, studentPortalProfileUser.Id, communityId);
        }

        List<ContentVersion> versions = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id =: result.resultId];
        List<ContentDocumentLink> links = [SELECT Id, ContentDocumentId, LinkedEntityId, ShareType, Visibility FROM ContentDocumentLink WHERE ContentDocumentId =: versions[0].ContentDocumentId AND LinkedEntityId =: parentId];

        // Act
        result = ContentDocumentService.updateSecurity(links, new Map<Id, ContentDocumentLink>(), Enums.TriggerContext.BEFORE_INSERT);

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.Status);
        System.assertEquals(1, links.size());
        System.assertEquals(Constants.FILE_CONTENT_SHARETYPE_INFERRED, links[0].ShareType);
        System.assertEquals(Constants.FILE_CONTENT_VISIBILITY_ALL, links[0].Visibility);
    }

    @isTest
    static void OppTimetableFile_ShareFileWithEveryone()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        TestUtilities.disableComponent('SObjectSendConfirmationEmailCommand');
        ActionResult result;
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact con = [SELECT Id FROM Contact LIMIT 1];
        Opportunity opportunity = [SELECT Id FROM Opportunity LIMIT 1];

        result = ContentDocumentService.createFile('Timetable File.txt', 'Timetable', Blob.valueOf('File content'), opportunity.Id, user.Id, null);

        List<ContentVersion> versions = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id = :result.resultId];
        List<ContentDocumentLink> links = [SELECT Id, ContentDocumentId, LinkedEntityId, ShareType, Visibility FROM ContentDocumentLink WHERE ContentDocumentId =: versions[0].ContentDocumentId AND LinkedEntityId = :opportunity.Id];

        // Act
        Test.startTest();
        result = ContentDocumentService.updateSecurity(links, new Map<Id, ContentDocumentLink>(), Enums.TriggerContext.BEFORE_INSERT);
        Test.stopTest();

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.Status);
        System.assertEquals(1, links.size());
        System.assertEquals(Constants.FILE_CONTENT_SHARETYPE_INFERRED, links[0].ShareType);
        System.assertEquals(Constants.FILE_CONTENT_VISIBILITY_ALL, links[0].Visibility);
    }

    @isTest
    static void OppExemptionsFile_FileNotSharedWithEveryone()
    {
        // Arrange
        ActionResult result;
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact con = [SELECT Id FROM Contact LIMIT 1];
        Opportunity opportunity = [SELECT Id FROM Opportunity LIMIT 1];

        result = ContentDocumentService.createFile('Exemptions File.txt', 'Exemptions', Blob.valueOf('File content'), opportunity.Id, user.Id, null);

        List<ContentVersion> versions = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id = :result.resultId];
        List<ContentDocumentLink> links = [SELECT Id, ContentDocumentId, LinkedEntityId, ShareType, Visibility FROM ContentDocumentLink WHERE ContentDocumentId =: versions[0].ContentDocumentId AND LinkedEntityId = :opportunity.Id];

        // Act
        result = ContentDocumentService.updateSecurity(links, new Map<Id, ContentDocumentLink>(), Enums.TriggerContext.BEFORE_INSERT);

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.Status);
        System.assertEquals(1, links.size());
        System.assertNotEquals(Constants.FILE_CONTENT_SHARETYPE_INFERRED, links[0].ShareType);
        System.assertNotEquals(Constants.FILE_CONTENT_VISIBILITY_ALL, links[0].Visibility);
    }
}