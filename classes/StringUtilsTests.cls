@isTest
public class StringUtilsTests
{
    @testSetup
    static void setup()
    {
        TestUtilities.setupHEDA();
    }

    @isTest
    static void ThrowException_MessageHasLineNumberAndStackTrace()
    {
        // Arrange
        String result;
        Exception tempException;

        try
        {
            result.toLowerCase();
        }
        catch(Exception ex)
        {
            tempException = ex;
        }

        // Act
        result = StringUtils.getExceptionMessage(tempException);

        // Assert
        system.assertEquals(true, result.contains('Line number:'), 'Line number not found');
        system.assertEquals(true, result.contains('Stack trace:'), 'Stack trace not found');
    }

    @isTest
    static void ConcatenateStringsWithNullValueAndSpace_ExcludeNullValueAndSpace()
    {
        // Arrange
        String string1;
        String string2 = ' Test';
        String string3;
        String string4 = 'Test ';

        // Act
        String result = StringUtils.concat(string1, string2, '-');
        result = StringUtils.concat(result, string3, ' ');
        result = StringUtils.concat(result, string4, ' ');

        // Assert
        system.assertEquals('Test Test', result, 'Wrong value for result');
    }

    @isTest
    static void CompareStrings_MatchBothCharsAndCase()
    {
        // Arrange
        String string1 = 'TeSt';
        String string2 = 'TEST';
        String string3 = 'TeSt';
        String string4;

        // Act
        Boolean result1 = StringUtils.equal(string1, string2);
        Boolean result2 = StringUtils.equal(string1, string3);
        Boolean result3 = StringUtils.equal(string1, string4);
        Boolean result4 = StringUtils.equal(string4, string1);

        // Assert
        system.assertEquals(false, result1, 'Wrong value for result1');
        system.assertEquals(true, result2, 'Wrong value for result2');
        system.assertEquals(false, result3, 'Wrong value for result3');
        system.assertEquals(false, result4, 'Wrong value for result4');
    }

    @isTest
    static void CompareStrings_MatchIgnoreCase()
    {
        // Arrange
        String string1 = 'TeSt';
        String string2 = 'TEST';
        String string3 = 'TESt';
        String string4;

        // Act
        Boolean result1 = StringUtils.equalIgnoreCase(string1, string2);
        Boolean result2 = StringUtils.equalIgnoreCase(string1, string3);
        Boolean result3 = StringUtils.equalIgnoreCase(string1, string4);
        Boolean result4 = StringUtils.equalIgnoreCase(string4, string1);

        // Assert
        system.assertEquals(true, result1, 'Wrong value for result1');
        system.assertEquals(true, result2, 'Wrong value for result2');
        system.assertEquals(false, result3, 'Wrong value for result3');
        system.assertEquals(false, result4, 'Wrong value for result4');
    }

    @isTest
    static void NormalisePhoneNumber_GetJustDigit()
    {
        // Arrange
        String phoneNumber1 = '(08) 7089-1212';
        String phoneNumber2 = '087.089.1212';
        String phoneNumber3 = '087.089.1212.087.089.1212.087.089.1212.087.089-1212.087.089.1212.087.089.1212.087.089 1212.087.089.1212.087.089.1212';

        // Act
        String result1 = StringUtils.normalisePhoneNumber(phoneNumber1);
        String result2 = StringUtils.normalisePhoneNumber(phoneNumber2);
        String result3 = StringUtils.normalisePhoneNumber(phoneNumber3);

        // Assert
        system.assertEquals('870891212', result1, 'Wrong value for result1');
        system.assertEquals('870891212', result2, 'Wrong value for result2');
        system.assertEquals('8708912120870891212087089121208708912120', result3, 'Wrong value for result3');
    }

    @isTest
    static void ComparePhoneNumbers_MatchPhoneNumberIngoreFormatAndCountryCode()
    {
        // Arrange
        String phoneNumber1 = '+61 8 7089 1212';
        String phoneNumber2 = '(08) 7089-1212';
        String phoneNumber3 = '08 70891212';
        String phoneNumber4;

        // Act
        Boolean result1 = StringUtils.isDifferentPhoneNumber(phoneNumber2, phoneNumber1);
        Boolean result2 = StringUtils.isDifferentPhoneNumber(phoneNumber3, phoneNumber1);
        Boolean result3 = StringUtils.isDifferentPhoneNumber(phoneNumber1, phoneNumber4);
        Boolean result4 = StringUtils.isDifferentPhoneNumber(phoneNumber4, phoneNumber1);

        // Assert
        system.assertEquals(false, result1, 'Wrong value for result1');
        system.assertEquals(false, result2, 'Wrong value for result2');
        system.assertEquals(true, result3, 'Wrong value for result3');
        system.assertEquals(true, result4, 'Wrong value for result4');
    }

    @isTest
    static void CheckEmails_IndicatesCorrectEmails()
    {
        // Arrange
        String correctEmail1 = 'feng.lian@aib.edu.au';
        String correctEmail2 = 'feng_lian@aib.com.au';
        String wrongEmail1 = 'feng.lian@aib.edu.aib.edu.aib.edu.aib.edu.aib.edu.aib.edu.aib.edu.aib.edu.au.aib.edu.aib.edu.aib.edu.au';
        String wrongEmail2 = 'feng.lian@aib';
        String wrongEmail3 = 'feng.lian#aib';
        String wrongEmail4 = 'feng.lian@aib_edu.au';
        String wrongEmail5 = '@aib_edu.au';
        String wrongEmail6 = 'aib_edu.au@';
        String wrongEmail7;

        // Act
        Boolean result1 = StringUtils.isValidEmail(correctEmail1);
        Boolean result2 = StringUtils.isValidEmail(correctEmail2);
        Boolean result3 = StringUtils.isValidEmail(wrongEmail1);
        Boolean result4 = StringUtils.isValidEmail(wrongEmail2);
        Boolean result5 = StringUtils.isValidEmail(wrongEmail3);
        Boolean result6 = StringUtils.isValidEmail(wrongEmail4);
        Boolean result7 = StringUtils.isValidEmail(wrongEmail5);
        Boolean result8 = StringUtils.isValidEmail(wrongEmail6);
        Boolean result9 = StringUtils.isValidEmail(wrongEmail7);

        // Assert
        system.assertEquals(true, result1, 'Wrong value for result1');
        system.assertEquals(true, result2, 'Wrong value for result2');
        system.assertEquals(false, result3, 'Wrong value for result3');
        system.assertEquals(false, result4, 'Wrong value for result4');
        system.assertEquals(false, result5, 'Wrong value for result5');
        system.assertEquals(false, result6, 'Wrong value for result6');
        system.assertEquals(false, result7, 'Wrong value for result7');
        system.assertEquals(false, result8, 'Wrong value for result8');
        system.assertEquals(false, result9, 'Wrong value for result9');
    }

    @isTest
    static void CopyLeftStringsWithNullValue_TruncatesStringWithoutException()
    {
        // Arrange
        String string1;
        String string2 = '0123456789';
        String string3;
        String string4;

        // Act
        string3 = StringUtils.copyLeft(string1, 5);
        string1 = StringUtils.copyLeft(string2, 5);
        string4 = StringUtils.copyLeft(string2, 50);

        // Assert
        system.assertEquals(null, string3, 'Wrong value for string3');
        system.assertEquals(string2.left(5), string1, 'Wrong value for string1');
        system.assertEquals(string2, string4, 'Wrong value for string4');
    }

    @isTest
    static void FormatNullDate_ReturnsNullString()
    {
        // Arrange
        Date date1 = null;

        // Act 
        String string1 = StringUtils.formatDate(date1);

        // Assert
        system.assertEquals(null, string1);
    }

    @isTest
    static void FormatDateInDifferentValues_ReturnsStringWithDoubleDigitDayMonth()
    {
        // Arrange
        Date date1 = Date.valueOf('2018-1-2');
        Date date2 = Date.valueOf('2018-1-12');
        Date date3 = Date.valueOf('2018-11-2');
        Date date4 = Date.valueOf('2018-11-12');

        // Act
        String string1 = StringUtils.formatDate(date1);
        String string2 = StringUtils.formatDate(date2);
        String string3 = StringUtils.formatDate(date3);
        String string4 = StringUtils.formatDate(date4);

        // Assert
        system.assertEquals('02/01/2018', string1);
        system.assertEquals('12/01/2018', string2);
        system.assertEquals('02/11/2018', string3);
        system.assertEquals('12/11/2018', string4);
    }

    @isTest
    static void IsStatementTrueEqualOperatorValidValues_StatementsEvaluatedCorrectly()
    {
        // Arrange
        String strOne = 'Test';
        String strTwo = 'More Tests';
        String strThree = '10';
        String strFour = '15';

        // Act
        Boolean stringSame = StringUtils.isLogicalStatementTrue(String.valueOf(Enums.Operator.EQUAL), strOne, strOne);
        Boolean stringDifferent = StringUtils.isLogicalStatementTrue(String.valueOf(Enums.Operator.EQUAL), strOne, strTwo);
        Boolean numSame = StringUtils.isLogicalStatementTrue(String.valueOf(Enums.Operator.EQUAL), strThree, strThree);
        Boolean numDifferent = StringUtils.isLogicalStatementTrue(String.valueOf(Enums.Operator.EQUAL), strThree, strFour);

        // Assert
        System.assertEquals(true, stringSame, 'Unexpected value for same string');
        System.assertEquals(false, stringDifferent, 'Unexpected value for different string');
        System.assertEquals(true, numSame, 'Unexpected value for same number');
        System.assertEquals(false, numDifferent, 'Unexpected value for different number');
    }

    @isTest
    static void IsStatementTrueNotEqualOperatorValidValues_StatementsEvaluatedCorrectly()
    {
        // Arrange
        String strOne = 'Test';
        String strTwo = 'More Tests';
        String strThree = '10';
        String strFour = '15';

        // Act
        Boolean stringSame = StringUtils.isLogicalStatementTrue(String.valueOf(Enums.Operator.NOT_EQUAL), strOne, strOne);
        Boolean stringDifferent = StringUtils.isLogicalStatementTrue(String.valueOf(Enums.Operator.NOT_EQUAL), strOne, strTwo);
        Boolean numSame = StringUtils.isLogicalStatementTrue(String.valueOf(Enums.Operator.NOT_EQUAL), strThree, strThree);
        Boolean numDifferent = StringUtils.isLogicalStatementTrue(String.valueOf(Enums.Operator.NOT_EQUAL), strThree, strFour);

        // Assert
        System.assertEquals(false, stringSame, 'Unexpected value for same string');
        System.assertEquals(true, stringDifferent, 'Unexpected value for different string');
        System.assertEquals(false, numSame, 'Unexpected value for same number');
        System.assertEquals(true, numDifferent, 'Unexpected value for different number');
    }

    @isTest
    static void IsStatementTrueLessThanOperatorValidValues_StatementsEvaluatedCorrectly()
    {
        // Arrange
        String strOne = '10';
        String strTwo = '20';

        // Act
        Boolean stringEqual = StringUtils.isLogicalStatementTrue(String.valueOf(Enums.Operator.LESS_THAN), strOne, strOne);
        Boolean stringLess = StringUtils.isLogicalStatementTrue(String.valueOf(Enums.Operator.LESS_THAN), strOne, strTwo);
        Boolean stringGreater = StringUtils.isLogicalStatementTrue(String.valueOf(Enums.Operator.LESS_THAN), strTwo, strOne);

        // Assert
        System.assertEquals(false, stringEqual, 'Unexpected value for same string value');
        System.assertEquals(true, stringLess, 'Unexpected value for less then string value');
        System.assertEquals(false, stringGreater, 'Unexpected value for greater than string value');
    }

    @isTest
    static void IsStatementTrueLessThanOperatorInvalidLeftOperand_StatementEvaluatedCorrectly()
    {
        // Arrange
        String strOne = 'bd20';

        // Act
        Boolean stringEqual = StringUtils.isLogicalStatementTrue(String.valueOf(Enums.Operator.LESS_THAN), strOne, strOne);

        // Assert
        System.assertEquals(false, stringEqual, 'Unexpected result value');
    }

    @isTest
    static void IsStatementTrueLessThanOperatorInvalidRightOperand_StatementEvaluatedCorrectly()
    {
        // Arrange
        String strOne = '10';
        String strTwo = '';

        // Act 
        Boolean stringEqual = StringUtils.isLogicalStatementTrue(String.valueOf(Enums.Operator.LESS_THAN), strOne, strTwo);

        // Assert
        System.assertEquals(false, stringEqual, 'Unexpected result value');
    }

    @isTest
    static void IsStatementTrueLessThanOrEqualOperatorValidValues_StatementsEvaluatedCorrectly()
    {
        // Arrange
        String strOne = '10';
        String strTwo = '20';

        // Act
        Boolean stringEqual = StringUtils.isLogicalStatementTrue(String.valueOf(Enums.Operator.LESS_THAN_OR_EQUAL), strOne, strOne);
        Boolean stringLess = StringUtils.isLogicalStatementTrue(String.valueOf(Enums.Operator.LESS_THAN_OR_EQUAL), strOne, strTwo);
        Boolean stringGreater = StringUtils.isLogicalStatementTrue(String.valueOf(Enums.Operator.LESS_THAN_OR_EQUAL), strTwo, strOne);

        // Assert
        System.assertEquals(true, stringEqual, 'Unexpected value for same string value');
        System.assertEquals(true, stringLess, 'Unexpected value for less then string value');
        System.assertEquals(false, stringGreater, 'Unexpected value for greater than string value');
    }

    @isTest
    static void IsStatementTrueLessThanOrEqualOperatorInvalidLeftOperand_StatementEvaluatedCorrectly()
    {
        // Arrange
        String strOne = 'bd20';

        // Act
        Boolean stringEqual = StringUtils.isLogicalStatementTrue(String.valueOf(Enums.Operator.LESS_THAN_OR_EQUAL), strOne, strOne);

        // Assert
        System.assertEquals(false, stringEqual, 'Unexpected result value');
    }

    @isTest
    static void IsStatementTrueLessThanOrEqualOperatorInvalidRightOperand_StatementEvaluatedCorrectly()
    {
        // Arrange
        String strOne = '10';
        String strTwo = '';

        // Act
        Boolean stringEqual = StringUtils.isLogicalStatementTrue(String.valueOf(Enums.Operator.LESS_THAN_OR_EQUAL), strOne, strTwo);

        // Assert
        System.assertEquals(false, stringEqual, 'Unexpected result value');
    }

    @isTest
    static void IsStatementTrueGreaterThanOperatorValidValues_StatementsEvaluatedCorrectly()
    {
        // Arrange
        String strOne = '20';
        String strTwo = '10';

        // Act
        Boolean stringEqual = StringUtils.isLogicalStatementTrue(String.valueOf(Enums.Operator.GREATER_THAN), strOne, strOne);
        Boolean stringGreater = StringUtils.isLogicalStatementTrue(String.valueOf(Enums.Operator.GREATER_THAN), strOne, strTwo);
        Boolean stringLess = StringUtils.isLogicalStatementTrue(String.valueOf(Enums.Operator.GREATER_THAN), strTwo, strOne);

        // Assert
        System.assertEquals(false, stringEqual, 'Unexpected value for same string value');
        System.assertEquals(true, stringGreater, 'Unexpected value for greater then string value');
        System.assertEquals(false, stringLess, 'Unexpected value for less than string value');
    }

    @isTest
    static void IsStatementTrueGreaterThanOperatorInvalidLeftOperand_StatementEvaluatedCorrectly()
    {
        // Arrange
        String strOne = 'bd20';

        // Act
        Boolean stringEqual = StringUtils.isLogicalStatementTrue(String.valueOf(Enums.Operator.GREATER_THAN), strOne, strOne);

        // Assert
        System.assertEquals(false, stringEqual, 'Unexpected result value');
    }

    @isTest
    static void IsStatementTrueGreaterThanOperatorInvalidRightOperand_StatementEvaluatedCorrectly()
    {
        // Arrange
        String strOne = '10';
        String strTwo = '';

        // Act
        Boolean stringEqual = StringUtils.isLogicalStatementTrue(String.valueOf(Enums.Operator.GREATER_THAN), strOne, strTwo);

        // Assert
        System.assertEquals(false, stringEqual, 'Unexpected result value');
    }

    @isTest
    static void IsStatementTrueGreaterThanOrEqualOperatorValidValues_StatementsEvaluatedCorrectly()
    {
        // Arrange
        String strOne = '20';
        String strTwo = '10';

        // Act
        Boolean stringEqual = StringUtils.isLogicalStatementTrue(String.valueOf(Enums.Operator.GREATER_THAN_OR_EQUAL), strOne, strOne);
        Boolean stringGreater = StringUtils.isLogicalStatementTrue(String.valueOf(Enums.Operator.GREATER_THAN_OR_EQUAL), strOne, strTwo);
        Boolean stringLess = StringUtils.isLogicalStatementTrue(String.valueOf(Enums.Operator.GREATER_THAN_OR_EQUAL), strTwo, strOne);

        // Assert
        System.assertEquals(true, stringEqual, 'Unexpected value for same string value');
        System.assertEquals(true, stringGreater, 'Unexpected value for greater then string value');
        System.assertEquals(false, stringLess, 'Unexpected value for less than string value');
    }

    @isTest
    static void IsStatementTrueGreaterThanOrEqualOperatorLeftOperand_StatementEvaluatedCorrectly()
    {
        // Arrange
        String strOne = 'bd20';

        // Act
        Boolean stringEqual = StringUtils.isLogicalStatementTrue(String.valueOf(Enums.Operator.GREATER_THAN_OR_EQUAL), strOne, strOne);

        // Assert
        System.assertEquals(false, stringEqual, 'Unexpected result value');
    }

    @isTest
    static void IsStatementTrueGreaterThanOrEqualOperatorInvalidRightOperand_StatementEvaluatedCorrectly()
    {
        // Arrange
        String strOne = '10';
        String strTwo = '';

        // Act
        Boolean stringEqual = StringUtils.isLogicalStatementTrue(String.valueOf(Enums.Operator.GREATER_THAN_OR_EQUAL), strOne, strTwo);

        // Assert
        System.assertEquals(false, stringEqual, 'Unexpected result value');
    }

    @isTest
    static void IsStatementTrueContainsOperatorValidValues_StatementsEvaluatedCorrectly()
    {
        // Arrange
        String firstValue = 'test';
        String secondValue = 'random';
        String csv = 'somevalue, test, anothervalue';

        // Act
        Boolean exists = StringUtils.isLogicalStatementTrue(String.valueOf(Enums.Operator.CONTAINS), firstValue, csv);
        Boolean notExists = StringUtils.isLogicalStatementTrue(String.valueOf(Enums.Operator.CONTAINS), secondValue, csv);

        // Assert
        System.assertEquals(true, exists, 'Unexpected exists value');
        System.assertEquals(false, notExists, 'Unexpected  "Not Exists" value');
    }

    @isTest
    static void IsStatementTrueContainsOperatorInvalidRightOperant_StatementEvaluatedCorrectly()
    {
        // Arrange
        String value = 'test';
        String csv = '';

        // Act
        Boolean stringEqual = StringUtils.isLogicalStatementTrue(String.valueOf(Enums.Operator.CONTAINS), value, csv);

        // Assert
        System.assertEquals(false, stringEqual, 'Unexpected result value');
    }

    @isTest
    static void IsStatementTrueNotContainsOperatorValidValues_StatementsEvaluatedCorrectly()
    {
        // Arrange
        String firstValue = 'test';
        String secondValue = 'random';
        String csv = 'somevalue, test, anothervalue';

        // Act
        Boolean notContains = StringUtils.isLogicalStatementTrue(String.valueOf(Enums.Operator.NOT_CONTAINS), secondValue, csv);
        Boolean contains = StringUtils.isLogicalStatementTrue(String.valueOf(Enums.Operator.NOT_CONTAINS), firstValue, csv);

        // Assert
        System.assertEquals(true, notContains, 'Unexpected "Not Contains" value');
        System.assertEquals(false, contains, 'Unexpected  "Not Exists" value');
    }

    @isTest
    static void IsStatementTrueNotContainsOperatorInvalidCSV_StatementEvaluatedCorrectly()
    {
        // Arrange
        String value = 'test';
        String csv = '';

        // Act
        Boolean stringEqual = StringUtils.isLogicalStatementTrue(String.valueOf(Enums.Operator.NOT_CONTAINS), value, csv);

        // Assert
        System.assertEquals(true, stringEqual, 'Unexpected result value');
    }

    @isTest
    static void IsStatementTrueEmptyOperatorValidValues_StatementsEvaluatedCorrectly()
    {
        // Arrange
        String strOne = '';
        String strTwo = 'Tests';

        // Act
        Boolean empty = StringUtils.isLogicalStatementTrue(String.valueOf(Enums.Operator.EMPTY), strOne, strOne);
        Boolean notEmpty = StringUtils.isLogicalStatementTrue(String.valueOf(Enums.Operator.EMPTY), strTwo, strOne);

        // Assert
        System.assertEquals(true, empty, 'Unexpected value for empty string');
        System.assertEquals(false, notEmpty, 'Unexpected value for "Not Empty" string');
    }

    @isTest 
    static void IsStatementTrueNotEmptyOperatorValidValues_StatementsEvaluatedCorrectly()
    {
        // Arrange
        String strOne = '';
        String strTwo = 'Tests';

        // Act
        Boolean empty = StringUtils.isLogicalStatementTrue(String.valueOf(Enums.Operator.NOT_EMPTY), strOne, strOne);
        Boolean notEmpty = StringUtils.isLogicalStatementTrue(String.valueOf(Enums.Operator.NOT_EMPTY), strTwo, strOne);

        // Assert
        System.assertEquals(false, empty, 'Unexpected value for empty string');
        System.assertEquals(true, notEmpty, 'Unexpected value for "Not Empty" string');
    }

    @isTest
    static void IsStatementTrueAnyValueInOperatorValidValues_StatementsEvaluatedCorrectly()
    {
        // Arrange
        String firstValue = 'test';
        String secondValue = 'random';
        String csv = 'somevalue, Test, anothervalue';

        // Act
        Boolean valueIn = StringUtils.isLogicalStatementTrue(String.valueOf(Enums.Operator.ANY_VALUE_IN), firstValue, csv);
        Boolean valueNotIn = StringUtils.isLogicalStatementTrue(String.valueOf(Enums.Operator.ANY_VALUE_IN), secondValue, csv);

        // Assert
        System.assertEquals(true, valueIn, 'Unexpected value for "Value In" string');
        System.assertEquals(false, valueNotIn, 'Unexpected value for "Value Not In" string');
    }

    @isTest
    static void IsStatementTrueRangeOperatorValidValues_StatementsEvaluatedCorrectly()
    {
        // Arrange
        String value = '10';
        String range = '5,15';

        // Act
        Boolean inRange = StringUtils.isLogicalStatementTrue(String.valueOf(Enums.Operator.RANGE), value, range);
        
        // Assert
        System.assertEquals(true, inRange, 'Unexpected value for "In Range" string');
    }

    @isTest 
    static void IsStatementTrueRangeOperatorInvalidRightOperand_StatementEvaluatedCorrectly()
    {
        // Arrange
        String value = '10';
        String range = '';

        // Act
        Boolean stringEqual = StringUtils.isLogicalStatementTrue(String.valueOf(Enums.Operator.RANGE), value, range);

        // Assert
        System.assertEquals(false, stringEqual, 'Unexpected result value');
    }

    @isTest
    static void IsStatementTrueRangeOperatorInvalidRange_StatementEvaluatedCorrectly()
    {
        // Arrange
        String value = '10';
        String range = '5';

        // Act
        Boolean stringEqual = StringUtils.isLogicalStatementTrue(String.valueOf(Enums.Operator.RANGE), value, range);

        // Assert
        System.assertEquals(false, stringEqual, 'Unexpected result value');
    }

    @isTest
    static void IsStatementTrueRangeOperatorBlankLeftOperand_StatementsEvaluatedCorrectly()
    {
        // Arrange
        String value = '';
        String range = '5,15';

        // Act
        Boolean inRange = StringUtils.isLogicalStatementTrue(String.valueOf(Enums.Operator.RANGE), value, range);

        // Assert
        System.assertEquals(false, inRange, 'Unexpected value for "In Range" string');
    }

    @isTest
    static void IsStatementTrueRangeOperatorInvalidMinRangeValue_StatementEvaluatedCorrectly()
    {
        // Arrange
        String value = '10';
        String range = 'bb,15';

        // Act
        Boolean stringEqual = StringUtils.isLogicalStatementTrue(String.valueOf(Enums.Operator.RANGE), value, range);
 
        // Assert
        System.assertEquals(false, stringEqual, 'Unexpected result value');
    }

    @isTest
    static void IsStatementTrueRangeOperatorInvalidMaxRangeValue_StatementEvaluatedCorrectly()
    {
        // Arrange
        String value = '10';
        String range = '5,bb';

        // Act
        Boolean stringEqual = StringUtils.isLogicalStatementTrue(String.valueOf(Enums.Operator.RANGE), value, range);

        // Assert
        System.assertEquals(false, stringEqual, 'Unexpected result value');
    }

    @isTest
    static void IsStatementTrueStartWithOperatorInvalidLeftOperandValue_StatementEvaluatedCorrectly()
    {
        // Arrange
        String value = '';
        String prefix = 'tes';
        
        // Act
        Boolean stringEqual = StringUtils.isLogicalStatementTrue(String.valueOf(Enums.Operator.STARTS_WITH), value, prefix);

        // Assert
        System.assertEquals(false, stringEqual, 'Unexpected result value');
    }

    @isTest
    static void IsStatementTrueStartWithOperatorInvalidRightOperandValue_StatementEvaluatedCorrectly()
    {
        // Arrange
        String value = 'Test';
        String prefix = '';

        // Act
        Boolean stringEqual = StringUtils.isLogicalStatementTrue(String.valueOf(Enums.Operator.STARTS_WITH), value, prefix);

        // Assert
        System.assertEquals(false, stringEqual, 'Unexpected result value');
    }

    @isTest
    static void IsStatementTrueStartWithOperatorValidValues_StatementsEvaluatedCorrectly()
    {
        // Arrange
        String value = 'Testing';
        String prefix = 'Tes';
        String prefixCsv = 'Set, Tes';
        String nonExistantPrefix = 'Set';

        // Act
        Boolean resultTrue = StringUtils.isLogicalStatementTrue(String.valueOf(Enums.Operator.STARTS_WITH), value, prefix);
        Boolean resultFalse = StringUtils.isLogicalStatementTrue(String.valueOf(Enums.Operator.STARTS_WITH), value, nonExistantPrefix);
        Boolean resultTrueList  = StringUtils.isLogicalStatementTrue(String.valueOf(Enums.Operator.STARTS_WITH), value, prefixCsv);

        // Assert
        System.assertEquals(true, resultTrue, 'Unexpected resultTrue value');
        System.assertEquals(false, resultFalse, 'Unexpected resultFalse value');
        System.assertEquals(true, resultTrueList, 'Unexpected resultTrueList value');
    }

    @isTest
    static void IsStatementTrueEndsWithOperatorInvalidLeftOperandValue_StatementEvaluatedCorrectly()
    {
        // Arrange
        String value = '';
        String suffix = 'tes';

        // Act
        Boolean stringEqual = StringUtils.isLogicalStatementTrue(String.valueOf(Enums.Operator.ENDS_WITH), value, suffix);

        // Assert
        System.assertEquals(false, stringEqual, 'Unexpected result value');
    }

    @isTest
    static void IsStatementTrueEndsWithOperatorInvalidRightOperandValue_StatementEvaluatedCorrectly()
    {
        // Arrange
        String value = 'Test';
        String suffix = '';

        // Act
        Boolean stringEqual = StringUtils.isLogicalStatementTrue(String.valueOf(Enums.Operator.ENDS_WITH), value, suffix);

        // Assert
        System.assertEquals(false, stringEqual, 'Unexpected result value');
    }

    @isTest
    static void IsStatementTrueEndsWithOperatorValidValues_StatementsEvaluatedCorrectly()
    {
        // Arrange
        String value = 'Testing';
        String suffix = 'ing';
        String suffixCsv = 'gni, ing';
        String nonExistantSuffix = 'gni';

        // Act
        Boolean resultTrue = StringUtils.isLogicalStatementTrue(String.valueOf(Enums.Operator.ENDS_WITH), value, suffix);
        Boolean resultFalse = StringUtils.isLogicalStatementTrue(String.valueOf(Enums.Operator.ENDS_WITH), value, nonExistantSuffix);
        Boolean resultTrueList = StringUtils.isLogicalStatementTrue(String.valueOf(Enums.Operator.ENDS_WITH), value, suffixCsv);

        // Assert
        System.assertEquals(true, resultTrue, 'Unexpected resultTrue value');
        System.assertEquals(false, resultFalse, 'Unexpected resulFalse value');
        System.assertEquals(true, resultTrueList, 'Unexpected resultTrueList value');
    }

    @isTest
    static void CompareStringWithList_MatchExact()
    {
        // Arrange
        String string1 = 'Reserve Your Spot';
        String string2 = 'reserve your Spot';
        String string3 = 'Reserve Your Spot (Ineligible)';
        String string4 = 'Reserve Your Spot (Eligible, Start 04/01/2019)';
        List<String> stringList = new List<String> {'Test One', 'Reserve Your Spot', 'Test Two'};

        // Act
        Boolean result1 = StringUtils.containsInList(string1, stringList, 0);
        Boolean result2 = StringUtils.containsInList(string2, stringList, 0);
        Boolean result3 = StringUtils.containsInList(string3, stringList, 0);
        Boolean result4 = StringUtils.containsInList(string4, stringList, 0);

        // Assert
        System.assertEquals(true, result1);
        System.assertEquals(false, result2);
        System.assertEquals(false, result3);
        System.assertEquals(false, result4);
    }

    @isTest
    static void CompareStringWithList_MatchSubString()
    {
        // Arrange
        String string1 = 'Reserve Your Spot';
        String string2 = 'reserve your Spot';
        String string3 = 'Reserve Your Spot (Ineligible)';
        String string4 = 'Reserve Your Spot (Eligible, Start 04/01/2019)';
        List<String> stringList = new List<String> {'Test One', 'Reserve Your Spot', 'Test Two'};

        // Act
        Boolean result1 = StringUtils.containsInList(string1, stringList, 1);
        Boolean result2 = StringUtils.containsInList(string2, stringList, 1);
        Boolean result3 = StringUtils.containsInList(string3, stringList, 1);
        Boolean result4 = StringUtils.containsInList(string4, stringList, 1);

        // Assert
        System.assertEquals(true, result1);
        System.assertEquals(false, result2);
        System.assertEquals(true, result3);
        System.assertEquals(true, result4);
    }

    @isTest
    static void CompareStringWithList_MatchBeginWith()
    {
        // Arrange
        String string1 = 'Reserve Your Spot';
        String string2 = 'reserve your Spot';
        String string3 = 'Reserve Your Spot (Ineligible)';
        String string4 = 'Reserve Your Spot (Eligible, Start 04/01/2019)';
        List<String> stringList = new List<String> {'Test One', 'Reserve Your Spot (Eligible', 'Test Two'};

        // Act
        Boolean result1 = StringUtils.containsInList(string1, stringList, 2);
        Boolean result2 = StringUtils.containsInList(string2, stringList, 2);
        Boolean result3 = StringUtils.containsInList(string3, stringList, 2);
        Boolean result4 = StringUtils.containsInList(string4, stringList, 2);

        // Assert
        System.assertEquals(false, result1);
        System.assertEquals(false, result2);
        System.assertEquals(false, result3);
        System.assertEquals(true, result4);
    }

    @isTest
    static void CompareStringWithList_MatchExactIgnoreCase()
    {
        // Arrange
        String string1 = 'Reserve Your Spot';
        String string2 = 'reserve your Spot';
        String string3 = 'Reserve Your Spot (Ineligible)';
        String string4 = 'Reserve Your Spot (Eligible, Start 04/01/2019)';
        List<String> stringList = new List<String> {'Test One', 'Reserve Your Spot', 'Test Two'};

        // Act
        Boolean result1 = StringUtils.containsInListIgnoreCase(string1, stringList, 0);
        Boolean result2 = StringUtils.containsInListIgnoreCase(string2, stringList, 0);
        Boolean result3 = StringUtils.containsInListIgnoreCase(string3, stringList, 0);
        Boolean result4 = StringUtils.containsInListIgnoreCase(string4, stringList, 0);

        // Assert
        System.assertEquals(true, result1);
        System.assertEquals(true, result2);
        System.assertEquals(false, result3);
        System.assertEquals(false, result4);
    }

    @isTest
    static void CompareStringWithList_MatchSubStringIgnoreCase()
    {
        // Arrange
        String string1 = 'Reserve Your Spot';
        String string2 = 'reserve your Spot';
        String string3 = 'Reserve Your Spot (Ineligible)';
        String string4 = 'Reserve Your Spot (Eligible, Start 04/01/2019)';
        List<String> stringList = new List<String> {'Test One', 'Reserve Your Spot', 'Test Two'};

        // Act
        Boolean result1 = StringUtils.containsInListIgnoreCase(string1, stringList, 1);
        Boolean result2 = StringUtils.containsInListIgnoreCase(string2, stringList, 1);
        Boolean result3 = StringUtils.containsInListIgnoreCase(string3, stringList, 1);
        Boolean result4 = StringUtils.containsInListIgnoreCase(string4, stringList, 1);

        // Assert
        System.assertEquals(true, result1);
        System.assertEquals(true, result2);
        System.assertEquals(true, result3);
        System.assertEquals(true, result4);
    }

    @isTest
    static void CompareStringWithList_MatchBeginWithIgnoreCase()
    {
        // Arrange
        String string1 = 'Reserve Your Spot';
        String string2 = 'reserve your Spot';
        String string3 = 'Reserve Your Spot (Ineligible)';
        String string4 = 'reserve Your Spot (Eligible, Start 04/01/2019)';
        List<String> stringList = new List<String> {'Test One', 'Reserve Your Spot (Eligible', 'Test Two'};

        // Act
        Boolean result1 = StringUtils.containsInListIgnoreCase(string1, stringList, 2);
        Boolean result2 = StringUtils.containsInListIgnoreCase(string2, stringList, 2);
        Boolean result3 = StringUtils.containsInListIgnoreCase(string3, stringList, 2);
        Boolean result4 = StringUtils.containsInListIgnoreCase(string4, stringList, 2);

        // Assert
        System.assertEquals(false, result1);
        System.assertEquals(false, result2);
        System.assertEquals(false, result3);
        System.assertEquals(true, result4);
    }

    @isTest
    static void GenerateRandomString_StringSuccessfullyGenerated()
    {
        // Arrange
        String randomStringOne = '';
        String randomStringTwo = '';

        // Act
        randomStringOne = StringUtils.generateRandomString();
        randomStringTwo = StringUtils.generateRandomString();

        // Assert
        System.assert(!String.isEmpty(randomStringOne), 'Expecting value in random string one');
        System.assert(!String.isEmpty(randomStringTwo), 'Expecting value in random string two');
        System.assertNotEquals(randomStringOne, randomStringTwo, 'Expecting two random strings to be different');
    }
}