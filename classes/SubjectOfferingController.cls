public class SubjectOfferingController 
{
    @AuraEnabled(cacheable=true)
    public static Object getList(String termId)
    {
        if (String.isEmpty(termId))
            throw new Exceptions.InvalidParameterException('Parameter value cannot be blank');

        ViewModels.Param param = new ViewModels.Param();
        param.Identifier = termId;

        return JSON.serialize(SubjectOfferingService.getStaffPlanSubjects(param));
    }

    @AuraEnabled(cacheable=true)
    public static Object getStaffPlanDetails(String termId)
    {
        if (String.isEmpty(termId))
            throw new Exceptions.InvalidParameterException('Parameter value cannot be blank');
        
        ViewModels.Param param = new ViewModels.Param();
        param.Identifier = termId;

        ViewModelsWorkforce.SubjectOfferingStaffDetails subjectDetails = SubjectOfferingService.getStaffPlanSubjectsDetails(param);

        ViewModels.Param optionParam = new ViewModels.Param();
        Map<String, Object> optionMap = new Map<String, Object>
        {
            'list' => 'subjectEnrolmentRoles, subjectEnrolmentStatuses',
            'type' => 'faculty',
            'isLightningModel' => true
        };
        optionParam.Payload = JSON.serialize(optionMap);
        subjectDetails.Option = OptionService.getPickListValues(optionParam);

        return JSON.serialize(subjectDetails);
    }

    @AuraEnabled()
    public static Object saveEnrolments(String offeringId, String enrolments)
    {
        if (String.isEmpty(offeringId))
            throw new Exceptions.InvalidParameterException('OfferingId parameter cannot be blank');

        if (String.isEmpty(enrolments))
            throw new Exceptions.InvalidParameterException('Enrolments parameter cannot be blank');
        
        ViewModels.Param param = new ViewModels.Param();
        param.Identifier = offeringId;
        param.Payload = enrolments;

        ActionResult result = SubjectOfferingService.saveEnrolments(param);

        if (result.isSuccess)
        {
            ViewModels.Param queryParam = new ViewModels.Param();
            queryParam.Identifier = offeringId;

            return JSON.serialize(SubjectOfferingService.getStaffPlanSubjectDetails(queryParam));
        }
        else
        {
            Log.error('SubjectOfferingController', 'saveEnrolments', result.message);
            throw new Exceptions.ApplicationException('Failed to save data.');
        }
    }
}