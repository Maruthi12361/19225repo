public class SubjectEnrolmentCalculateCommand extends BaseQueueableCommand
{
    Id facultyRecordTypeId = RecordTypeUtils.getRecordTypeId(Constants.RECORDTYPE_COURSEENROLLMENT_FACULTY, SObjectType.hed__Course_Enrollment__c);
    Id studentRecordTypeId = RecordTypeUtils.getRecordTypeId(Constants.RECORDTYPE_COURSEENROLLMENT_STUDENT, SObjectType.hed__Course_Enrollment__c);
    Id provisionalRecordTypeId = RecordTypeUtils.getRecordTypeId(Constants.RECORDTYPE_COURSEENROLLMENT_PROVISIONAL, SObjectType.hed__Course_Enrollment__c);

    public SubjectEnrolmentCalculateCommand()
    {
        super(50);
    }

    public override List<SObject> validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        List<hed__Course_Enrollment__c> enrolments = new List<hed__Course_Enrollment__c>();

        for (hed__Course_Enrollment__c enrolment : (List<hed__Course_Enrollment__c>) items)
        {
            if (enrolment.hed__Course_Offering__c != null)
                enrolments.add(enrolment);
        }

        return enrolments;
    }

    public override ActionResult command(List<SObject> items, Id jobId)
    {
        Set<Id> subjectOfferingIds = SObjectUtils.getIDs(items, 'hed__Course_Offering__c');
        List<hed__Course_Offering__c> subjectOfferings = getSubjectOfferings(subjectOfferingIds);
        List<hed__Course_Enrollment__c> subjectEnrolments = getSubjectEnrolments(subjectOfferingIds);
        List<hed__Course_Offering__c > subjectOfferingsToUpdate = new List<hed__Course_Offering__c >();

        for (hed__Course_Enrollment__c enrolment : (List<hed__Course_Enrollment__c>) items)
        {
            hed__Course_Offering__c subjectOffering = getSubjectOffering(enrolment.hed__Course_Offering__c, subjectOfferings);
            List<hed__Course_Enrollment__c> subjectOfferingEnrolments = getOfferingSubjectEnrolments(enrolment.hed__Course_Offering__c, subjectEnrolments);

            if (subjectOffering == null)
            {
                Log.info(this.className, 'command', 'Failed to find subject offering {0}. Not updating any subject offering value', new List<String> { enrolment.hed__Course_Offering__c });
                continue;
            }

            OfferingInfo offeringInfo = getOfferingCalValues(subjectOffering, subjectOfferingEnrolments);

            if (offeringInfo != null)
            {
                subjectOffering.ActualStudents__c = offeringInfo.ActualStudents;
                subjectOffering.ProjectedStudents__c = offeringInfo.ProjectedStudents;
                subjectOffering.hed__Capacity__c = offeringInfo.Capacity;
                subjectOffering.ProjectedCapacity__c = offeringInfo.ProjectedCapacity;
                subjectOffering.MaximumCapacity__c = offeringInfo.MaximumCapacity;

                subjectOfferingsToUpdate.add(subjectOffering);
            }
        }

        if (subjectOfferingsToUpdate.size() > 0)
            uow.registerDirty(subjectOfferingsToUpdate);

        return new ActionResult(ResultStatus.SUCCESS);
    }

    private List<hed__Course_Offering__c> getSubjectOfferings(Set<Id> ids)
    {
        List<hed__Course_Offering__c> offerings =
        [
            SELECT ActualStudents__c, Id, PlanningHealth__c, ProjectedCapacity__c, ProjectedStudents__c, hed__Capacity__c,
                hed__Course__r.StudentsCoordinatedPerSC__c, hed__Course__r.StudentsTaughtPerOLF__c, hed__Course__r.StudentsTaughtPerSC__c
            FROM hed__Course_Offering__c
            WHERE Id IN :ids
        ];

        return offerings;
    }

    private hed__Course_Offering__c getSubjectOffering(Id subjectOfferingId, List<hed__Course_Offering__c> subjectOfferings)
    {
        for (hed__Course_Offering__c subjectOffering : subjectOfferings)
            if (subjectOffering.Id == subjectOfferingId)
                return subjectOffering;

        return null;
    }

    private List<hed__Course_Enrollment__c> getSubjectEnrolments(Set<Id> subjectOfferingIds)
    {
        return
        [
            SELECT Id, IsDeleted, RecordTypeId, Role__c, StudentsAllocated__c, hed__Course_Offering__c, hed__Status__c
            FROM hed__Course_Enrollment__c
            WHERE hed__Course_Offering__c IN :subjectOfferingIds AND IsDeleted = false
        ];
    }

    private List<hed__Course_Enrollment__c> getOfferingSubjectEnrolments(Id subjectOfferingId, List<hed__Course_Enrollment__c> subjectEnrolments)
    {
        List<hed__Course_Enrollment__c> offeringEnrolments = new List<hed__Course_Enrollment__c>();

        for (hed__Course_Enrollment__c subjectEnrolment : subjectEnrolments)
            if (subjectEnrolment.hed__Course_Offering__c == subjectOfferingId)
                offeringEnrolments.add(subjectEnrolment);

        return offeringEnrolments;
    }

    private OfferingInfo getOfferingCalValues(hed__Course_Offering__c subjectOffering, List<hed__Course_Enrollment__c> subjectEnrolments)
    {
        OfferingInfo info = new OfferingInfo();

        if (subjectEnrolments == null)
            return null;

        for (hed__Course_Enrollment__c enrolment : subjectEnrolments)
        {
            if (enrolment.RecordTypeId == studentRecordTypeId)
            {
                if (enrolment.hed__Status__c == Constants.SUBJECTENROLMENT_STATUS_ENROLLED
                    || enrolment.hed__Status__c == Constants.SUBJECTENROLMENT_STATUS_STUDYING
                    || enrolment.hed__Status__c == Constants.SUBJECTENROLMENT_STATUS_COMPLETED)
                {
                    info.ActualStudents++;
                    info.ProjectedStudents++;
                }
            }
            else if (enrolment.RecordTypeId == provisionalRecordTypeId)
            {
                if (enrolment.hed__Status__c == Constants.SUBJECTENROLMENT_STATUS_ENROLLING
                    || enrolment.hed__Status__c == Constants.SUBJECTENROLMENT_STATUS_INTERESTED)
                    info.ProjectedStudents++;
            }
            else if (enrolment.RecordTypeId == facultyRecordTypeId)
            {
                if (enrolment.hed__Status__c == Constants.SUBJECTENROLMENT_STATUS_ENROLLED)
                    info.Capacity += enrolment.StudentsAllocated__c != null ? (Integer) enrolment.StudentsAllocated__c : 0;

                if (enrolment.hed__Status__c == Constants.SUBJECTENROLMENT_STATUS_ENROLLED || enrolment.hed__Status__c == Constants.SUBJECTENROLMENT_STATUS_REQUESTED)
                    info.ProjectedCapacity += enrolment.StudentsAllocated__c != null ? (Integer) enrolment.StudentsAllocated__c : 0;

                if (enrolment.Role__c == 'Subject Coordinator')
                    info.MaximumCapacity += subjectOffering.hed__Course__r.StudentsTaughtPerSC__c != null ? 
                        (Integer) subjectOffering.hed__Course__r.StudentsTaughtPerSC__c : 0;
                else if (enrolment.Role__c == 'Online Facilitator' || enrolment.Role__c == 'Online Facilitator (SC)')
                    info.MaximumCapacity += subjectOffering.hed__Course__r.StudentsTaughtPerOLF__c != null ? (Integer) subjectOffering.hed__Course__r.StudentsTaughtPerOLF__c : 0;
            }
        }

        return info;
    }

    public class OfferingInfo
    {
        public Integer ActualStudents { get; set; }
        public Integer ProjectedStudents { get; set; }
        public Integer Capacity { get; set; }
        public Integer ProjectedCapacity { get; set; }
        public Integer MaximumCapacity { get; set; }

        public OfferingInfo()
        {
            ActualStudents = 0;
            ProjectedStudents = 0;
            Capacity = 0;
            ProjectedCapacity = 0;
            MaximumCapacity = 0;
        }
    }
}