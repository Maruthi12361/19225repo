@isTest
public class TaskSyncCallCountToContactCommandTests 
{
    private static String cerebroID = 'A001641336';
    
    @testSetup
    static void setup()
    {        
        TestUtilities.setupHEDA();
    	User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        TestDataCampaignMock.createSeries(user, 4, true);
        TestDataCampaignMock.createRolloverCampaignMemberStatus(user);
        
        Contact student = TestDataContactMock.createRolloverStudent(user, cerebroID);
        student.Manual_Followup_Date__c = Date.today().addDays(1);
        update student;        
    }

    @isTest 
    static void NewCallTask_SyncsCallCountToContact() 
    {        
         // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact student = 
            [
                SELECT Id,Current_Rollover_Campaign__c,Rollover_Status__c
                FROM Contact
                WHERE Cerebro_Student_ID__c = : cerebroID
            ];
        
        Task task = TestDataTaskMock.createRolloverCallTask(user, student.Id, student.Current_Rollover_Campaign__c, 'Phone Off', 'Phone is off, call another time.', false);        
        List<Task> items = new List<Task>{ task };
        Map<Id, Task> oldMap = new Map<Id, Task>();
        
        // Act
        ActionResult result = TaskService.syncCallCountToContact(items, oldMap, Enums.TriggerContext.AFTER_INSERT);
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Executing a command should return a successful result to the caller.');
        
        // Assert        
        student = 
            [
                SELECT Id,Rollover_Status__c,Rollover_Call_Count__c,Rollover_Email_Count__c,Rollover_Contact_History__c
                FROM Contact
                WHERE Id = : student.Id
            ];
        String contactHistory = 'Call ' + student.Rollover_Call_Count__c + ' - ' + task.LastModifiedDate.format() + ' - Phone Off - Phone is off, call another time.';        
        
        System.assertEquals(RolloverConstantsDefinition.ST_CONTACTING, student.Rollover_Status__c, 'Contact Rollover Status should be Contacting when new task is created');
        System.assertEquals(1, student.Rollover_Call_Count__c, 'Contact Rollover Call Count should be increased by 1 when new task is created');        
        System.assertEquals(contactHistory, student.Rollover_Contact_History__c, 'Contact Rollover Contact History is not updated correctly.');
    }
    
    @isTest 
    static void UpdatedEmailTask_SyncsEmailCountToContact() 
    {        
         // Arrange
        Contact student = 
            [
                SELECT Id,Current_Rollover_Campaign__c,Rollover_Status__c
                FROM Contact
                WHERE Cerebro_Student_ID__c = : cerebroID
            ];
        
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Task task = TestDataTaskMock.createRolloverEmailTask(user, student.Id, null, 'Test Subject', true);
        
        Task newTask = task.clone(true, false, true, true);        
        newTask.WhatId = student.Current_Rollover_Campaign__c;
        List<Task> items = new List<Task>{ newTask };
        Map<Id, Task> oldMap = new Map<Id, Task>();
        oldMap.put(task.Id, task);
        // Act
        ActionResult result = TaskService.syncCallCountToContact(items, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Executing a command should return a successful result to the caller.');
        
        // Assert        
        student = 
            [
                SELECT Id,Rollover_Status__c,Rollover_Call_Count__c,Rollover_Email_Count__c,Rollover_Contact_History__c
                FROM Contact
                WHERE Id = : student.Id
            ];
        String contactHistory = 'Email ' + student.Rollover_Email_Count__c + ' - ' + newTask.LastModifiedDate.format() + ' - ' + 'Test Subject';       
        
        System.assertEquals(RolloverConstantsDefinition.ST_CONTACTING, student.Rollover_Status__c, 'Contact Rollover Status should be Contacting when new task is created');
        System.assertEquals(1, student.Rollover_Email_Count__c, 'Contact Rollover Call Count should be increased by 1 when new task is created');        
        System.assertEquals(contactHistory, student.Rollover_Contact_History__c, 'Contact Rollover Contact History is not updated correctly.');
    }
}