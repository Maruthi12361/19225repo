public class AttachmentService
{
    public static ActionResult create(Id parentId, String attName, String description, String contentType, Blob fileBlob)
    {
        Attachment att = new Attachment
        (
            ParentId = parentId,
            Name = attName,
            Description = description,
            ContentType = contentType,
            Body = fileBlob
        );

        return new AttachmentCreateCommand().execute(new List<Attachment>{ att });
    }

    public static ActionResult create( Id parentId, String attName, String description, Blob fileBlob)
    {
        Attachment att = new Attachment
        (
            ParentId = parentId,
            Name = attName,
            Description = description,
            Body = fileBlob
        );

        return new AttachmentCreateCommand().execute(new List<Attachment>{ att });
    }

    public static ActionResult remove(String id)
    {
        return new AttachmentDeleteCommand().execute(new Set<ID>{ id });
    }
}