public abstract class BaseStringQuery 
{
    protected abstract List<SObject> query(Set<String> values);
}