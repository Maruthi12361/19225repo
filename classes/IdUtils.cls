public class IdUtils 
{
    public static Boolean contains(List<Id> ids, Id id)
    {
        for (Id itemId : ids)
        {
            if (itemId == id)
                return true;
        }

        return false;
    }

    public static Id toId(String idString)
    {
        if (idString.length() == 18)
            return Id.valueOf(idString);

        return null;
    }

    public static String generateGUID()
    {
        Blob blobValue = Crypto.GenerateAESKey(128);
        String hex = EncodingUtil.ConvertTohex(blobValue);

        return hex.SubString(0,8)+ '-' + hex.SubString(8,12) + '-' + hex.SubString(12,16) + '-' + hex.SubString(16,20) + '-' + hex.substring(20);
    }
}