public class CaseEnrolmentController
{
    @AuraEnabled
    public static Case getCaseInfo(Id caseId)
    {
        Case record = [SELECT RecordType.DeveloperName, IsClosed FROM Case WHERE Id = : caseId];
        return record;
    }

    @AuraEnabled
    public static ViewModelsCaseEnrolment.Payload getSubjects(Id caseId)
    {
        return CaseEnrolmentService.querySubjectsByCase(caseId);
    }

    @AuraEnabled
    public static List<Interfaces.IViewModel> getSubjectsByPortalUser()
    {
        return CaseEnrolmentService.querySubjectsByPortalUser(UserInfo.getUserId());
    }

    @AuraEnabled
    public static void createCaseEnrolments(Id caseId, List<Id> param)
    {
        ActionResult result;

        if (caseId != null && param != null && param.size() > 0)
        {
            Case record = [SELECT Id, IsClosed, RecordType.DeveloperName, ContactId FROM Case WHERE Id = : caseId];
            
            if (record.IsClosed || record.RecordType.DeveloperName == Constants.RECORDTYPE_CASE_APPLICATION || record.RecordType.DeveloperName == Constants.RECORDTYPE_CASE_ROLLOVER)
            {            
                throw new Exceptions.ApplicationException('This interface is disabled for closed or Application and Rollover types of cases.');
            }
            else if (record.RecordType.DeveloperName == Constants.RECORDTYPE_CASE_EXEMPTIONS)
            {
                result = CaseEnrolmentService.createExemptionEnrolments(caseId, new Set<Id>(param));
            }
            else
            {
                result = CaseEnrolmentService.create(caseId, new Set<Id>(param));
            }
            
            if (result.Status != ResultStatus.SUCCESS)        
            {
                Log.error('CaseEnrolmentController', 'createCaseEnrolments', result.message);
                throw new Exceptions.ApplicationException(result.message);
            }
        }
    }
}