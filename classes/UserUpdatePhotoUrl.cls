global class UserUpdatePhotoUrl implements Schedulable 
{
    global void execute(SchedulableContext ctx)
    {
        List<User> updates = new List<User>();

        // CPQ Integration User is a Read-Only user installed by SF and so cannot be editted as part of this update
        List<User> users = [SELECT Id, SmallPhotoUrl, Small_Photo_URL__c FROM User WHERE Profile.Name <> :Constants.PROFILE_CPQINTEGRATION];

        for (User user : users)
        {
            if (user.SmallPhotoUrl != user.Small_Photo_URL__c)
            {
                user.Small_Photo_URL__c = user.SmallPhotoUrl;
                updates.add(user);
            }
        }
        
        if (updates.size() > 0)
	        update updates;
    }
}