public class ContactEncryptDataCommand extends BaseTriggerCommand
{
    public override List<SObject> validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        List<Contact> contacts = new List<Contact>();

        for (Contact c : (List<Contact>)items)
        {
            if (String.isEmpty(c.EncryptedId__c))
                contacts.add(c);
        }

        return contacts;
    }

    public override ActionResult command(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        for (Contact c : (List<Contact>)items)
        {
            String guid = IdUtils.generateGUID();
            c.EncryptedId__c = EncryptionUtil.Encrypt(guid);
            Log.info(this.className, 'command', 'Encrypted GUID from {0} to {1}', new List<String>{ guid, c.EncryptedId__c });
        }

        return new ActionResult(ResultStatus.SUCCESS);
    }
}