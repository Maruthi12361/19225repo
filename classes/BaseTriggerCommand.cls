public abstract class BaseTriggerCommand extends BaseComponent
{
    protected UnitOfWork uow
    {
        get
        {
            return UnitOfWork.current;
        }
    }

    public BaseTriggerCommand()
    {
        super();
    }

    public BaseTriggerCommand(Integer retry)
    {
        super(retry);
    }

    protected abstract List<SObject> validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext);

    protected abstract ActionResult command(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext);

    public ActionResult execute(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        Boolean isValid = true;
        ActionResult result = new ActionResult(ResultStatus.SUCCESS);
        List<SObject> validItems;

        if (items == null)
            throw new InvalidParameterValueException('items', 'Command execution was requested but was provided with no information to act on');

        if (oldMap == null && (triggerContext == Enums.TriggerContext.BEFORE_UPDATE || triggerContext == Enums.TriggerContext.AFTER_UPDATE))
            throw new InvalidParameterValueException('oldMap', 'Command execution was requested but was provided with no old map of objects to act on');

        // Validation
        try
        {
            Log.info(this.className, 'execute', 'Validating command.');
            validItems = validate(items, oldMap, triggerContext);

            if (validItems.size() == 0)
            {
                Log.info(this.className, 'execute', 'Validation: No objects meeting conditions to process.');
                isValid = false;
            }
        }
        catch (Exception ex)
        {
            exc = ex;
            isValid = false;
            Log.error(this.className, 'execute', 'Error validating command: ', ex);
            result = new ActionResult(ResultStatus.ERROR, 'Error validating command ' + this.className + ': ' + StringUtils.getExceptionMessage(ex));
        }

        if (isValid)
        {
            Log.info(this.className, 'execute', 'Executing command.');

            // Command
            for (Integer i = 0 ; i <= retry; i++)
            {
                result = executeTrigger(validItems, oldMap, triggerContext);

                if (result.Status == ResultStatus.SUCCESS || (!result.message.contains('UNABLE_TO_LOCK_ROW') && !result.message.contains('Please try again')))
                    break;

                Log.info(this.className, 'execute', 'Unable to lock row, remaining retry {0}', new List<String>{ String.valueOf(retry - i) });
            }
        }

        if (!trigger.isExecuting && this.saveLogs)
            Log.save();

        if (exc != null && Test.isRunningTest() && TestUtilities.throwOnException)
            throw exc;

        return result;
    }

    private ActionResult executeTrigger(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        ActionResult result;
        Boolean uowScope = false;

        if (UnitOfWork.current == null)
        {
            uowScope = true;
            UnitOfWork.current = new UnitOfWork();
        }

        try
        {
            result = command(items, oldMap, triggerContext);

            if (uowScope)
            {
                UnitOfWork.current.commitWork();
                uowScope = false;
            }
        }
        catch (Exception ex)
        {
            exc = ex;
            UnitOfWork.current = null;
            uowScope = false;
            Log.error(this.className, 'execute', 'Error executing command: ', ex);
            result = new ActionResult(ResultStatus.ERROR, 'Error executing command ' + this.className + ': ' + StringUtils.getExceptionMessage(ex));
        }

        return result;
    }
}