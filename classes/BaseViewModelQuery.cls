public abstract class BaseViewModelQuery 
{
    public abstract List<Interfaces.IViewModel> query(Set<Id> ids);
}