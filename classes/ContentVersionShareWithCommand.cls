public class ContentVersionShareWithCommand extends BaseViewModelCommand
{
    public override List<Interfaces.IViewModel> validate(List<Interfaces.IViewModel> viewModels)
    {
        ViewModelsFile.DocumentLink viewModel = (ViewModelsFile.DocumentLink)viewModels[0];
        List<Interfaces.IViewModel> result = new List<Interfaces.IViewModel>();
        
        List<ContentDocumentLink> files = 
        [
            SELECT Id, LinkedEntityId, ContentDocumentId, Visibility, ShareType
            FROM ContentDocumentLink
            WHERE ContentDocumentId =: viewModel.DocumentIds AND LinkedEntityId =: viewModel.ParentId
        ];

        if (files.size() == 0)
            result.add(viewModel);

        return result;
    }

    public override ActionResult command(List<Interfaces.IViewModel> viewModels)
    {
        ViewModelsFile.DocumentLink viewModel = (ViewModelsFile.DocumentLink)viewModels[0];

        for (Id id : viewModel.DocumentIds)
        {            
            List<ContentDocumentLink> links = 
            [
                SELECT Id, LinkedEntityId, ContentDocumentId, Visibility, ShareType
                FROM ContentDocumentLink
                WHERE ContentDocumentId =: id
            ];

            for (ContentDocumentLink link: links)
            {
                ContentDocumentLink newLink = link.clone();
                newLink.LinkedEntityId = viewModel.ParentId;
                insert newLink;
            }    
        }

        return new ActionResult();
    }
}