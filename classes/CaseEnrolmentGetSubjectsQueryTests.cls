@isTest
public class CaseEnrolmentGetSubjectsQueryTests
{
    @testSetup
    static void setup()
    {
        // Arrange
        TestUtilities.setupHEDA();

        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        ViewModelsTestData.Data data = TestDataAcademicConfigMock.create(user);
        Contact student = TestDataContactMock.createStudent(user, 'A001641336');
        hed__Program_Enrollment__c courseEnrolment = TestDataCourseEnrolmentMock.create(user, data.course('MBA').Id, student.Id, 'Ongoing', data.coursePlan('MBA v1').Id);

        TestDataSubjectEnrolmentMock.create(user, data.subjectOffering(Date.today().addDays(59), '9050PROJ'), courseEnrolment, 'In Progress', 'Standard', null, null);
        TestDataSubjectEnrolmentMock.createProvisionalEnrolment(user, data.subjectOffering(Date.today().addDays(115), '8001LEAD'), student, 'Enrolling', 'Standard', courseEnrolment);
        TestDataSubjectEnrolmentMock.create(user, data.subjectOffering(Date.today().addDays(-53), '8002MMGT'), courseEnrolment, 'Complete', 'Standard', 'Pass 2', 50);
        TestDataSubjectEnrolmentMock.createExemption(user, '8003SHRM', 'Strategic Human Resource Management', courseEnrolment);
        TestDataSubjectEnrolmentMock.createExemption(user, '8006FMGT', 'Financial Management', courseEnrolment, true);        

        TestDataCaseMock.createStudentSupportCase(user, student);
        TestDataCaseMock.createExemptionCase(user, student);
    }
    
    @isTest
    static void QuerySubjectsByPortalUser_ReturnsSubjectEnrolments()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact student = [SELECT Id FROM Contact WHERE Cerebro_Student_ID__c = 'A001641336'];
        User portalUser = TestDataUserMock.createStudentPortalLoginProfileUser(user, student.Id);
        List<hed__Course_Enrollment__c> enrolments =
            [
                SELECT Id, SubjectCode__c, SubjectName__c, SubjectOfferingAdminDate__c, SubjectOfferingStartDate__c, SubjectOfferingTerm__c
                FROM hed__Course_Enrollment__c
                WHERE hed__Contact__c = : student.Id AND (RecordType.DeveloperName != : Constants.RECORDTYPE_COURSEENROLLMENT_PROVISIONAL OR hed__Status__c = : Constants.SUBJECTENROLMENT_STATUS_ENROLLING)
                ORDER BY SubjectOfferingAdminDate__c DESC
            ];

        // Act
        test.startTest();
        List<ViewModelsCaseEnrolment.EnrolmentDetails> subjects = (List<ViewModelsCaseEnrolment.EnrolmentDetails>)CaseEnrolmentService.querySubjectsByPortalUser(portalUser.Id);
        test.stopTest();

        // Assert
        System.assertEquals(enrolments.size(), subjects.size());

        for (Integer i = 0; i < subjects.size(); i++)
        {
            System.assertEquals(enrolments[i].SubjectName__c, subjects[i].Name);
            System.assertEquals(enrolments[i].SubjectCode__c, subjects[i].Code);
            System.assertEquals(enrolments[i].SubjectOfferingAdminDate__c, subjects[i].AdminDate);
            System.assertEquals(enrolments[i].SubjectOfferingStartDate__c, subjects[i].StartDate);
            System.assertEquals(enrolments[i].SubjectOfferingTerm__c, subjects[i].Term);
        }
    }

    @isTest
    static void QuerySubjectsByExemptionCase_ReturnsSubjectsFromCoursePlan()
    {
        // Arrange
        Case exemptionCase = [SELECT Id, ContactId FROM Case WHERE RecordType.DeveloperName = : Constants.RECORDTYPE_CASE_EXEMPTIONS];
        List<hed__Plan_Requirement__c> requirements =
            [
                SELECT Name, SubjectCode__c
                FROM hed__Plan_Requirement__c
                WHERE RecordType.DeveloperName = : Constants.RECORDTYPE_PLANREQUIREMENT_DEPENDENCYITEM AND
                    hed__Plan_Requirement__r.hed__Program_Plan__r.Name = 'MBA v1' AND SubjectCode__c IN ('8005CGOV')
            ];
        Set<Id> idSet = new Map<Id, hed__Plan_Requirement__c>(requirements).keySet();
        CaseEnrolmentService.createExemptionEnrolments(exemptionCase.Id, idSet);
        List<hed__Course_Enrollment__c> enrolments =
            [
                SELECT SubjectCode__c
                FROM hed__Course_Enrollment__c
                WHERE SubjectCode__c = '8005CGOV' OR (hed__Contact__c = : exemptionCase.ContactId AND
                    RecordType.DeveloperName = : Constants.RECORDTYPE_COURSEENROLLMENT_STUDENT AND
                    hed__Status__c != : Constants.SUBJECTENROLMENT_STATUS_WITHDRAWN)
            ];
        Set<String> subjectCodeSet = new Set<String>();

        for (hed__Course_Enrollment__c enrolment : enrolments)
            subjectCodeSet.add(enrolment.SubjectCode__c);

        requirements =
            [
                SELECT hed__Plan_Requirement__r.Name, Name, SubjectCode__c
                FROM hed__Plan_Requirement__c
                WHERE RecordType.DeveloperName = : Constants.RECORDTYPE_PLANREQUIREMENT_DEPENDENCYITEM AND
                    hed__Plan_Requirement__r.hed__Program_Plan__r.Name = 'MBA v1' AND SubjectCode__c NOT IN : subjectCodeSet
                ORDER BY hed__Plan_Requirement__r.Name, Name
            ];

        // Act
        test.startTest();
        ViewModelsCaseEnrolment.Payload payload = CaseEnrolmentService.querySubjectsByCase(exemptionCase.Id);
        test.stopTest();

        // Assert
        System.assertEquals(requirements.size(), payload.data.size());

        for (Integer i = 0; i < payload.data.size(); i++)
        {
            ViewModelsCaseEnrolment.PlanRequirementDetails detail = (ViewModelsCaseEnrolment.PlanRequirementDetails)payload.data[i];
            System.assertEquals(requirements[i].hed__Plan_Requirement__r.Name, detail.Category);
            System.assertEquals(requirements[i].Name, detail.SubjectName);
            System.assertEquals(requirements[i].SubjectCode__c, detail.SubjectCode);
        }
    }

    @isTest
    static void QuerySubjectsBySupportCase_ReturnsSubjectEnrolmentsNotLinkedWithThisCase()
    {
        // Arrange
        Case supportCase = [SELECT Id, ContactId FROM Case WHERE RecordType.DeveloperName = : Constants.RECORDTYPE_CASE_STUDENT_SUPPORT];
        List<hed__Course_Enrollment__c> enrolments = [SELECT Id FROM hed__Course_Enrollment__c WHERE SubjectCode__c = '9050PROJ' AND hed__Contact__c = : supportCase.ContactId];
        Set<Id> idSet = new Map<Id, hed__Course_Enrollment__c>(enrolments).keySet();
        CaseEnrolmentService.create(supportCase.Id, idSet);
        enrolments =
            [
                SELECT SubjectCode__c
                FROM hed__Course_Enrollment__c
                WHERE hed__Contact__c = : supportCase.ContactId
                AND SubjectCode__c != '9050PROJ' AND (RecordType.DeveloperName != : Constants.RECORDTYPE_COURSEENROLLMENT_PROVISIONAL OR hed__Status__c = : Constants.SUBJECTENROLMENT_STATUS_ENROLLING)
                ORDER BY SubjectOfferingAdminDate__c DESC
            ];

        // Act
        test.startTest();
        ViewModelsCaseEnrolment.Payload payload = CaseEnrolmentService.querySubjectsByCase(supportCase.Id);
        test.stopTest();

        // Assert
        System.assertEquals(enrolments.size(), payload.data.size());

        for (Integer i = 0; i < payload.data.size(); i++)
        {
            ViewModelsCaseEnrolment.EnrolmentDetails detail = (ViewModelsCaseEnrolment.EnrolmentDetails)payload.data[i];
            System.assertEquals(enrolments[i].Id, detail.Id);
        }
    }

    @isTest
    static void QuerySubjectsByAwardCase_ReturnsSubjectEnrolmentsWithPassGrades()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact student = [SELECT Id FROM Contact WHERE Cerebro_Student_ID__c = 'A001641336'];
        Case awardCase = TestDataCaseMock.createAwardCase(user, student);
        List<String> passGrades = new List<String> {'High Distinction', 'Distinction', 'Credit', 'Pass 1', 'Pass 2'};
        List<hed__Course_Enrollment__c> enrolments = [SELECT Id FROM hed__Course_Enrollment__c WHERE hed__Contact__c = : awardCase.ContactId AND Grade__c IN : passGrades ORDER BY SubjectOfferingAdminDate__c DESC];

        // Act
        test.startTest();
        ViewModelsCaseEnrolment.Payload payload = CaseEnrolmentService.querySubjectsByCase(awardCase.Id);
        test.stopTest();

        // Assert
        System.assertEquals(enrolments.size(), payload.data.size());

        for (Integer i = 0; i < payload.data.size(); i++)
        {
            ViewModelsCaseEnrolment.EnrolmentDetails detail = (ViewModelsCaseEnrolment.EnrolmentDetails)payload.data[i];
            System.assertEquals(enrolments[i].Id, detail.Id);
        }
    }
    
    @isTest
    static void QuerySubjectsByWithdrawalCase_ReturnsInProgressSubjectEnrolments()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact student = [SELECT Id FROM Contact WHERE Cerebro_Student_ID__c = 'A001641336'];
        Case withdrawalCase = TestDataCaseMock.createStudentSupportCase(user, student, 'Withdrawal', 'Subjects', true);
        List<String> statuses = new List<String> {Constants.SUBJECTENROLMENT_STATUS_ENROLLED, Constants.SUBJECTENROLMENT_STATUS_STUDYING};
        List<hed__Course_Enrollment__c> enrolments = [SELECT Id FROM hed__Course_Enrollment__c WHERE hed__Contact__c = : withdrawalCase.ContactId AND hed__Status__c IN : statuses ORDER BY SubjectOfferingAdminDate__c DESC];

        // Act
        test.startTest();
        ViewModelsCaseEnrolment.Payload payload = CaseEnrolmentService.querySubjectsByCase(withdrawalCase.Id);
        test.stopTest();

        // Assert
        System.assertEquals(enrolments.size(), payload.data.size());

        for (Integer i = 0; i < payload.data.size(); i++)
        {
            ViewModelsCaseEnrolment.EnrolmentDetails detail = (ViewModelsCaseEnrolment.EnrolmentDetails)payload.data[i];
            System.assertEquals(enrolments[i].Id, detail.Id);
        }
    }
    
    @isTest
    static void QuerySubjectsByAppealCase_ReturnsCompletedOrWithdrawnSubjectEnrolments()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact student = [SELECT Id FROM Contact WHERE Cerebro_Student_ID__c = 'A001641336'];
        Case appealCase = TestDataCaseMock.createStudentSupportCase(user, student, 'Formal Grievance/Appeal', null, true);
        List<String> statuses = new List<String> {Constants.SUBJECTENROLMENT_STATUS_COMPLETED, Constants.SUBJECTENROLMENT_STATUS_WITHDRAWN};
        List<hed__Course_Enrollment__c> enrolments = [SELECT Id FROM hed__Course_Enrollment__c WHERE hed__Contact__c = : appealCase.ContactId AND hed__Status__c IN : statuses ORDER BY SubjectOfferingAdminDate__c DESC];

        // Act
        test.startTest();
        ViewModelsCaseEnrolment.Payload payload = CaseEnrolmentService.querySubjectsByCase(appealCase.Id);
        test.stopTest();

        // Assert
        System.assertEquals(enrolments.size(), payload.data.size());

        for (Integer i = 0; i < payload.data.size(); i++)
        {
            ViewModelsCaseEnrolment.EnrolmentDetails detail = (ViewModelsCaseEnrolment.EnrolmentDetails)payload.data[i];
            System.assertEquals(enrolments[i].Id, detail.Id);
        }
    }

    @isTest
    static void QuerySubjectsByExemptionCaseForProspect_ReturnsSubjectsFromCurrentAndFutureSubjectOfferings()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact prospect = TestDataContactMock.createProspectCold(user, '0405558798');
        Case exemptionCase = TestDataCaseMock.createExemptionCase(user, prospect);
        List<AggregateResult> subjects =
            [
                SELECT hed__Course__r.SubjectDefinition__c SubjectId
                FROM hed__Course_Offering__c
                WHERE hed__End_Date__c >= TODAY
                GROUP BY hed__Course__r.SubjectDefinition__c
            ];
        Set<Id> subjectIds = new Set<Id>();

        for(AggregateResult result : subjects)
        {
            subjectIds.add((Id)result.get('SubjectId'));
        }

        // Act
        test.startTest();
        ViewModelsCaseEnrolment.Payload payload = CaseEnrolmentService.querySubjectsByCase(exemptionCase.Id);
        test.stopTest();

        // Assert
        System.assertEquals(subjectIds.size(), payload.data.size());

        for (Integer i = 0; i < payload.data.size(); i++)
        {
            ViewModelsCaseEnrolment.SubjectDetails detail = (ViewModelsCaseEnrolment.SubjectDetails)payload.data[i];
            System.assertEquals(true, subjectIds.contains(detail.Id));
        }
    }
}