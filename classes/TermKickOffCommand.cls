public class TermKickOffCommand extends BaseIdCommand
{
    public override Set<ID> validate(Set<ID> items)
    {
       return items;
    }

    public override ActionResult command(Set<ID> items)
    {
        List<hed__Term__c> terms = getTerms(items);
        List<hed__Course_Offering__c> termsSubjectOfferings = getTermsSubjectOfferings(SObjectUtils.getIds(terms, 'Id'));
        List<hed__Course_Enrollment__c> facultySubjectEnrolments = getFacultySubjectEnrolments(SObjectUtils.getIds(termsSubjectOfferings, 'Id'));

        for (hed__Term__c term : terms)
        {
            Log.info(this.className, 'command', 'Kicking off term {0}', new List<String> { term.Id });

            if (term.PlanningStatus__c != 'In Planning')
                term.PlanningStatus__c = 'In Planning';

            createFacultyEnrolments(term.Id, termsSubjectOfferings, facultySubjectEnrolments);
        }

        uow.registerDirty(terms);

        return TermService.sendKickOffEmail(terms);
    }

    private void createFacultyEnrolments(Id termId, List<hed__Course_Offering__c> termsSubjectOfferings, List<hed__Course_Enrollment__c> facultySubjectEnrolments)
    {
        Id facultyRecordTypeId = RecordTypeUtils.getRecordTypeId(Constants.RECORDTYPE_COURSEENROLLMENT_FACULTY,SObjectType.hed__Course_Enrollment__c);
        List<hed__Course_Offering__c> subjectOfferings = getTermSubjectOfferings(termId, termsSubjectOfferings);

        for (hed__Course_Offering__c subjectOffering : subjectOfferings)
        {
            Integer allocatedStudents = 0;
            List<hed__Course_Enrollment__c> newEnrolments = new List<hed__Course_Enrollment__c>();
            List<hed__Course_Enrollment__c> facultyEnrolments = getFacultyEnrolments(subjectOffering.Id, facultySubjectEnrolments);

            if (facultyEnrolments.size() > 0)
            {
                Log.info(this.className, 'createFacultyEnrolments', 'There are already faculty enrolments for subject offering {0}. Not creating any additional one',
                    new List<String> { subjectOffering.Id });

                continue;
            }

            if (subjectOffering.ActualStudents__c == null)
            {
                Log.info(this.className, 'createFacultyEnrolments', 'Actual student value for subject offering {0} is null. Not creating subject enrolments',
                    new List<String> { subjectOffering.Id });

                continue;
            }

            if (subjectOffering.hed__Course__r.StudentsCoordinatedPerSC__c != null)
            {
                Integer numberOfCoordinatorsRequired = (Integer) (subjectOffering.ActualStudents__c / subjectOffering.hed__Course__r.StudentsCoordinatedPerSC__c).round(System.RoundingMode.CEILING);

                for (Integer i = 0; i < numberOfCoordinatorsRequired; i++)
                {
                    Integer numberOfStudentsToAllocate = (Integer) subjectOffering.hed__Course__r.StudentsTaughtPerSC__c;
                    allocatedStudents =  allocatedStudents + numberOfStudentsToAllocate;

                    newEnrolments.add(createSubjectEnrolment(subjectOffering.Id, facultyRecordTypeId, 'Subject Coordinator', 'Recommended', numberOfStudentsToAllocate));
                }
            }
            else
                Log.info(this.className, 'createFacultyEnrolments', 'There is no value in Coordinated Students Per Subject field in Subject Offering. Not adding Subject Coordinators');

            if (subjectOffering.hed__Course__r.StudentsTaughtPerOLF__c != null)
            {
                Integer numberOfFacilitatorsRequired = (Integer) ((subjectOffering.ActualStudents__c - allocatedStudents) / subjectOffering.hed__Course__r.StudentsTaughtPerOLF__c).round(System.RoundingMode.CEILING);

                for (Integer i = numberOfFacilitatorsRequired; i > 0; i--)
                {
                    Integer numberOfStudentsToAllocate = (Integer) ((subjectOffering.ActualStudents__c - allocatedStudents) / i).round(System.RoundingMode.CEILING);
                    allocatedStudents = allocatedStudents + numberOfStudentsToAllocate;

                    newEnrolments.add(createSubjectEnrolment(subjectOffering.Id, facultyRecordTypeId, 'Online Facilitator', 'Recommended', numberOfStudentsToAllocate));
                }
            }
            else
                Log.info(this.className, 'createFacultyEnrolments', 'There is no value in Students Per OLF field in Subject Offering. Not adding Online Facilitators');

            if (newEnrolments.size() > 0)
                uow.registerNew(newEnrolments);
        }
    }

    private static List<hed__Term__c> getTerms(Set<ID> itemIds)
    {
        return [SELECT Id, Name, PlanningStatus__c FROM hed__Term__c WHERE Id IN :itemIds];
    }

    private List<hed__Course_Offering__c> getTermsSubjectOfferings(Set<Id> termIds)
    {
        List<hed__Course_Offering__c> offerings =
        [
            SELECT ActualStudents__c, Id, Name, SubjectCode__c, hed__Course__r.AQFLevel__c, hed__Course__r.HeadOfDiscipline__c,
                hed__Course__r.hed__Course_ID__c, hed__Course__r.Name, hed__Course__r.StudentsCoordinatedPerSC__c, hed__Course__r.StudentsTaughtPerOLF__c,
                hed__Course__r.StudentsTaughtPerSC__c, hed__End_Date__c, hed__Start_Date__c, hed__Term__c
            FROM hed__Course_Offering__c 
            WHERE hed__Term__c IN :termIds AND ActualStudents__c > 0
        ];

        return offerings;
    }

    private static List<hed__Course_Enrollment__c> getFacultySubjectEnrolments(Set<ID> subjectOfferingIds)
    {
        List<hed__Course_Enrollment__c> subjectEnrolments =
        [
            SELECT Id, hed__Course_Offering__c
            FROM hed__Course_Enrollment__c
            WHERE hed__Course_Offering__c IN :subjectOfferingIds AND  RecordType.DeveloperName = 'Faculty'
        ];

        return subjectEnrolments;
    }

    private static  List<hed__Course_Enrollment__c> getFacultyEnrolments(Id subjectOfferingId, List<hed__Course_Enrollment__c> facultySubjectEnrolments)
    {
        List<hed__Course_Enrollment__c> facultyEnrolments = new List<hed__Course_Enrollment__c>();

        for (hed__Course_Enrollment__c enrolment : facultySubjectEnrolments)
        {
            if (enrolment.hed__Course_Offering__c == subjectOfferingId)
                facultyEnrolments.add(enrolment);
        }

        return facultyEnrolments;
    }

    private List<hed__Course_Offering__c> getTermSubjectOfferings(Id termId, List<hed__Course_Offering__c> termsSubjectOfferings)
    {
        List<hed__Course_Offering__c> termSubjectOfferings = new LIst<hed__Course_Offering__c>();

        for (hed__Course_Offering__c subjectOffering : termsSubjectOfferings)
        {
            if (subjectOffering.hed__Term__c == termId)
                termSubjectOfferings.add(subjectOffering);
        }

        return termSubjectOfferings;
    }

    private static hed__Course_Enrollment__c createSubjectEnrolment(Id subjectOfferingId, Id recordTypeId, String role, String status, Integer studentsAllocated)
    {
        hed__Course_Enrollment__c enrolment = new hed__Course_Enrollment__c
        (
            hed__Course_Offering__c = subjectOfferingId,
            RecordTypeId = recordTypeId,
            Role__c = role,
            hed__Status__c = status,
            StudentsAllocated__c = studentsAllocated
        );

        return enrolment;
    }
}