@isTest
public class ContactUpdateUserOnChangeCommandTests
{
    private static String cerebroID = 'A001641336';

    @testSetup
    static void setup()
    {
        TestUtilities.setupHEDA();
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact student = TestDataContactMock.createRolloverStudent(user, cerebroID);
        TestDataUserMock.createStudentPortalLoginProfileUser(user, student.Id);
    }

    @isTest
    static void ChangeStudentNames_SyncsNamesToPortalUser()
    {
        // Arrange
        Contact student =
            [
                SELECT Id, HasPortalUser__c, FirstName, MiddleName, LastName
                FROM Contact
                WHERE Cerebro_Student_ID__c = : cerebroID
            ];
        Contact newStudent = student.clone(true, false, true, true);
        newStudent.HasPortalUser__c = true;
        newStudent.MiddleName = 'Bee';

        // Act
        ActionResult result = ContactService.updateUserOnChange(new List<Contact>{ newStudent }, new Map<Id, Contact>{ student.Id => student }, Enums.TriggerContext.AFTER_UPDATE);
        System.assertEquals(ResultStatus.SUCCESS, result.Status);

        // Assert
        User portalUser =
            [
                SELECT Id, FirstName, MiddleName, LastName
                FROM User
                WHERE ContactId = : student.Id
            ];
        System.assertEquals(portalUser.FirstName, newStudent.FirstName);
        System.assertEquals(portalUser.MiddleName, newStudent.MiddleName);
        System.assertEquals(portalUser.LastName, newStudent.LastName);
    }
}