@isTest
public class ContactUpdateOpportunityCommandTests 
{    
    @testSetup
    static void setup()
    {
        TestUtilities.setupHEDA();        
    }

    @isTest
    static void ContactUpdateProgram_OpportuniyProgramUpdated()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);        
        Contact contact = TestDataContactMock.createProspectHot(user);        
        Contact updatedContact = contact.clone(true, false, true, true);
        
        // Act
        Test.startTest();                
        updatedContact.Program__c = 'GCM';
        update updatedContact;
        Test.stopTest();
        
        Opportunity opp = [SELECT Id, Program__c, Contact__c FROM Opportunity WHERE Contact__c = :updatedContact.Id];
        
        // Assert
        System.assertNotEquals(null, opp);
        System.assertEquals('GCM', opp.Program__c);
    }
    
    @isTest
    static void ContactUpdatePortfolio_OpportunityPortfolioUpdated()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);        
        Contact contact = TestDataContactMock.createProspectHot(user);        
        Contact updatedContact = contact.clone(true, false, true, true);       
                
        // Act
        Test.startTest();        
        updatedContact.Portfolio__c = 'International (Other)';
        update updatedContact;
        Test.stopTest();
        
        Opportunity opp = [SELECT Id, Portfolio__c, Contact__c FROM Opportunity WHERE Contact__c = :updatedContact.Id];
        
        // Assert
        System.assertNotEquals(null, opp);
        System.assertEquals('International (Other)', opp.Portfolio__c);
    }
}