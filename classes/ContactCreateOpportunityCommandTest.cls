@IsTest
public class ContactCreateOpportunityCommandTest
{
    @testSetup
    static void setup()
    {
        TestUtilities.setupHEDA();
    }

    @isTest
    static void UpdateProspectContactStatusToHotWithoutAnyOpenOpportunities_AcquisitionOpportunityCreated()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());

        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Campaign campaign = TestDataCampaignMock.create(user, 'Domestic', 'MBA', false);
        Contact contact = TestDataContactMock.createProspectWarm(user, true);

        Contact updated = contact.clone(true, false, true, true);
        updated.ApplicationCourse__c = 'MBA';
        updated.HighestQualificationName__c = 'Bachelor';
        updated.HighestQualificationInstitution__c = 'Uni SA';
        updated.HighestQualificationLanguage__c = 'English';
        updated.HighestQualificationCompletionYear__c = '2018';
        updated.High_School_Level__c = 'Year 12 (or equivalent)';
        updated.Preferred_Start_Date__c = StringUtils.formatDate(campaign.TermStartDate__c);
        updated.Status__c = Constants.STATUS_HOT;
        updated.OwnerId = user.Id;
        updated.PreferredCampaign__c = campaign.Id;

        // Act
        Test.startTest();
        update updated;
        Contact updatedContact = [SELECT Id, AccountId, Preferred_Start_Date__c, OwnerId, ProgramPortfolio__c FROM Contact WHERE Id = :updated.Id];
        Test.stopTest(); 

        Opportunity createdOpportunity = 
        [
            SELECT Id, Name, CloseDate, StageName, AccountId, Contact__c, OwnerId, RecordTypeId, CampaignId
            FROM Opportunity 
            WHERE Contact__c = :contact.Id
        ];

        // Assert
        System.assertNotEquals(null, createdOpportunity);
        System.assertEquals('Applying', createdOpportunity.StageName, 'Incorrect StageName');
        System.assertEquals(updatedContact.AccountId, createdOpportunity.AccountId, 'Incorrect account Id');
        System.assertEquals(campaign.TermCensusDate__c, createdOpportunity.CloseDate, 'Incorrect close date');
        System.assertEquals(updatedContact.OwnerId, createdOpportunity.OwnerId, 'Incorrect owner id');
        System.assertEquals(System.Label.ID_RecordType_Opportunity_Acquisition, createdOpportunity.RecordTypeId, 'Incorrect record type');
        System.assertEquals(campaign.Id, createdOpportunity.CampaignId, 'Incorrect Primary Campaign Source');
    }

    @isTest
    static void UpdateProspectContactStatusToHotWithOpenAcquisitionOpportunity_AcquisitionOpportunityCreated()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Campaign campaign = TestDataCampaignMock.create(user, 'Domestic', 'MBA', false);
        Contact contact = TestDataContactMock.createProspectWarm(user, true);
        TestDataOpportunityMock.createApplyingAcquisition(user, contact);
        Contact updated = contact.clone(true, false, true, true);
        updated.ApplicationCourse__c = 'MBA';
        updated.HighestQualificationName__c = 'Bachelor';
        updated.HighestQualificationInstitution__c = 'Uni SA';
        updated.HighestQualificationLanguage__c = 'English';
        updated.HighestQualificationCompletionYear__c = '2018';
        updated.High_School_Level__c = 'Year 12 (or equivalent)';
        updated.Preferred_Start_Date__c = StringUtils.formatDate(campaign.TermStartDate__c);
        updated.Status__c = Constants.STATUS_HOT;
        updated.OwnerId = user.Id;
        updated.PreferredCampaign__c = campaign.Id;

        // Act
        Test.startTest();
        ContactService.createOpportunity(new List<SObject> { updated }, new Map<Id, SObject>(new List<Contact> { contact }), Enums.TriggerContext.AFTER_UPDATE);
        Test.stopTest(); 

        List<Opportunity> createdOpportunities = 
        [
            SELECT Id, Name, CloseDate, StageName, AccountId, Contact__c, OwnerId, RecordTypeId 
            FROM Opportunity 
            WHERE Contact__c = :contact.Id and RecordTypeId = :System.Label.ID_RecordType_Opportunity_Acquisition
        ];

        // Assert
        System.assertEquals(1, createdOpportunities.size(), 'Incorrect number of opportunities');
    }

    @isTest
    static void UpdateProspectContactStatusToHotWithOpenAcquisitionOpportunities_NoNewOpportunityAcquisitionCreated()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact contact = TestDataContactMock.createProspectWarm(user, true);
        Opportunity opp = TestDataOpportunityMock.createApplyingAcquisition(user, contact);
        contact.Preferred_Start_Date__c = StringUtils.formatDate(TestDataContactMock.getNextPreferredStartDate());
        contact.Status__c = Constants.STATUS_HOT;

        // Act
        Test.startTest();
        update contact;
        Test.stopTest(); 

        Contact updatedContact = 
        [
            SELECT Id, AccountId, Name, OwnerId, ProgramPortfolio__c, RecordTypeId, Preferred_Start_Date__c 
            FROM Contact 
            WHERE Id = :contact.Id
        ];
        List<Opportunity> createdOpportunities = 
        [
            SELECT Id, Name, CloseDate, StageName, AccountId, Contact__c, OwnerId, RecordTypeId 
            FROM Opportunity 
            WHERE Contact__c = :contact.Id and RecordTypeId = :System.Label.ID_RecordType_Opportunity_Acquisition
        ];

        // Assert
        System.assertEquals(1, createdOpportunities.size(), 'Incorrect number of Acqusition opportunities');
        System.assertEquals(opp.AccountId, createdOpportunities[0].AccountId, 'Incorrect account Id');
        System.assertEquals(opp.CloseDate, createdOpportunities[0].CloseDate, 'Incorrect close date');
        System.assertEquals(System.Label.ID_RecordType_Opportunity_Acquisition, createdOpportunities[0].RecordTypeId, 'Incorrect record type');
    }

    @isTest
    static void UpdateStudentContactStatusToHotWithoutOpenOpportunities_NoNewOpportunityAcquisitionCreated()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact contact = TestDataContactMock.createStudent(user);
        contact.Preferred_Start_Date__c = StringUtils.formatDate(TestDataContactMock.getNextPreferredStartDate());
        contact.Status__c = Constants.STATUS_HOT;

        // Act
        Test.startTest();
        update contact;
        Test.stopTest(); 

        List<Opportunity> createdOpportunities = 
        [
            SELECT Id, Name, CloseDate, StageName, AccountId, Contact__c, OwnerId, RecordTypeId 
            FROM Opportunity 
            WHERE Contact__c = :contact.Id and RecordTypeId = :System.Label.ID_RecordType_Opportunity_Acquisition
        ];

        // Assert
        System.assertEquals(0, createdOpportunities.size(), 'Incorrect number of Acquisition opportunities');
    }
}