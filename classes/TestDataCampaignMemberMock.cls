@isTest
public class TestDataCampaignMemberMock {

	public static CampaignMember createCampaingMember(User user, Contact contact, Campaign campaign)
	{
		CampaignMember cm = new CampaignMember
		(
			CampaignId = campaign.Id,
			ContactId = contact.Id
		);

        System.RunAs(user)
        {
            insert cm;
        }
        
        return cm;
	}
}