@isTest
public class ContactReferrerSetLeadSourceCommandTest
{
    @testSetup
    static void setup()
    {
        TestUtilities.setupHEDA();
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        TestDataContactMock.createProspectCold(user, '040411111122');
        TestDataLeadSourceConfigMock.create(user);
    }

    @isTest
    static void NewContactReferrerCreated_LeadSourceValueSet()
    {
        // Arrange
        Contact contact = [SELECT Id FROM Contact limit 1];
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        ContactReferrer__c referrer = TestDataContactReferrerMock.create(user, contact.Id);

        // Act
        Test.startTest();
        ContactReferrer__c updatedReferrer = [SELECT Id, LeadSource__c FROM ContactReferrer__c WHERE Id = :referrer.Id];
        Test.stopTest(); 

        // Assert
        System.assertEquals('Facebook (Paid)', updatedReferrer.LeadSource__c, 'Unexpected lead source');

    } 

    @isTest
    static void NewContactReferrerCreated_LeadSourceValueSetSuccessfully()
    {
        // Arrange
        Contact contact = [SELECT Id FROM Contact limit 1];
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        TestDataLeadSourceConfigMock.create(user, 'eqr323121', 'TestSource', 'TestContent', 'TestMedium', '', '', 'TestCampaign', 'TestKeyword', 'TestTerm', 'Test (Free)');
        ContactReferrer__c referrer = TestDataContactReferrerMock.create(user, contact.Id, 'TestSource', 'TestContent', 'TestMedium', 'TestCampaign', 'eqr323121', 'TestKeyword', 'TestTerm', 'TestReferrerSource');

        // Act
        Test.startTest();
        ContactReferrer__c updatedReferrer = [SELECT Id, LeadSource__c FROM ContactReferrer__c WHERE Id = :referrer.Id];
        Test.stopTest(); 

        // Assert
        System.assertEquals('Test (Free)', updatedReferrer.LeadSource__c, 'Unexpected lead source');
    }

    @isTest
    static void NewContactReferrerWithOriginalReferrer_LeadSourceValueSetSuccessfully()
    {
        // Arrange
        Contact contact = [SELECT Id, PersonalEmail__c, OriginalReferrerSource__c FROM Contact limit 1];
        contact.OriginalReferrerSource__c = 'Test Class';
        update contact;

        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        TestDataLeadSourceConfigMock.create(user, 'eqr323121', 'TestSource', 'TestContent', 'TestMedium', contact.PersonalEmail__c, 'Test Class', 'TestCampaign', 'TestKeyword', 'TestTerm', 'Test (Free)');
        ContactReferrer__c referrer = TestDataContactReferrerMock.create(user, contact.Id, 'TestSource', 'TestContent', 'TestMedium', 'TestCampaign', 'eqr323121', 'TestKeyword', 'TestTerm', 'TestReferrerSource');

        // Act
        Test.startTest();
        ContactReferrer__c updatedReferrer = [SELECT Id, LeadSource__c FROM ContactReferrer__c WHERE Id = :referrer.Id];
        Test.stopTest(); 

        // Assert
        System.assertEquals('Test (Free)', updatedReferrer.LeadSource__c, 'Unexpected lead source');
    }

    @isTest
    static void NewContactReferrerWithOriginalReferrerAndLeadSourceAsCommaSeparatedList_LeadSourceValueSetSuccessfully()
    {
        // Arrange
        Contact contact = [SELECT Id, PersonalEmail__c, OriginalReferrerSource__c FROM Contact limit 1];
        contact.OriginalReferrerSource__c = 'Test Class';
        update contact;

        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        TestDataLeadSourceConfigMock.create(user, 'eqr323121', 'TestSourceOne,TestSourceTwo', 'TestContent', 'TestMedium', contact.PersonalEmail__c, 'Test Class', 'TestCampaign', 'TestKeyword', 'TestTerm', 'Test (Free)');
        ContactReferrer__c referrer = TestDataContactReferrerMock.create(user, contact.Id, 'TestSourceOne', 'TestContent', 'TestMedium', 'TestCampaign', 'eqr323121', 'TestKeyword', 'TestTerm', 'TestReferrerSource');

        // Act
        Test.startTest();
        ContactReferrer__c updatedReferrer = [SELECT Id, LeadSource__c FROM ContactReferrer__c WHERE Id = :referrer.Id];
        Test.stopTest(); 

        // Assert
        System.assertEquals('Test (Free)', updatedReferrer.LeadSource__c, 'Unexpected lead source');
    }
}