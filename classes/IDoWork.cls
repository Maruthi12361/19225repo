/**
 * Interface describes work to be performed during the commitWork method
 **/
public interface IDoWork
{
    void doWork();
}