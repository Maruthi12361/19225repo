@isTest
private class UserUpdatePhotoUrlTest 
{
    @testSetup
    static void setup()
    {
        TestUtilities.setupHEDA();
    }

    @isTest
    public static void testScheduleJob()
    {
        // Arrange
        Integer year = Date.today().year() + 1;        
        
        // Act
        Test.startTest();
        String jobId = System.schedule('UserUpdatePhotoUrlTest', '0 0 0 1 7 ? ' + String.valueOf(year), new UserUpdatePhotoUrl());
        Test.stopTest();

        // Assert
        User dbUser = [SELECT Id, SmallPhotoUrl, Small_Photo_URL__c FROM User WHERE Profile.Name <> :Constants.PROFILE_CPQINTEGRATION][0];
        System.assertEquals(dbUser.Small_Photo_URL__c,dbUser.SmallPhotoURL);
    }
}