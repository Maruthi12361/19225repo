@isTest
public class TestDataSMSMock
{
    public static SObject create(User user, Id contactId, Id leadId, String phone, String smsText, Boolean inboundSMS)
    {
        return create(user, contactId, leadId, user.Id, phone, smsText, inboundSMS);
    }
    
    public static SObject create(User user, Id contactId, Id leadId, Id ownerId, String phone, String smsText, Boolean inboundSMS)
    {
        SObject sms;
        
        if (inboundSMS)
        {
            sms = new smagicinteract__Incoming_SMS__c
                (
                    smagicinteract__Contact__c = contactId,
                    smagicinteract__Lead__c = leadId,
                    smagicinteract__Mobile_Number__c = phone,
                    smagicinteract__SMS_Text__c = smsText,
                    smagicinteract__external_field__c = 'bc1fcf2194ae942b440500b5f87d11e0',
                    OwnerId = ownerId
                );
        }
        else
        {
            sms = new smagicinteract__smsMagic__c
                (
                    smagicinteract__Contact__c = contactId,
                    smagicinteract__Lead__c = leadId,
                    smagicinteract__PhoneNumber__c = phone,
                    smagicinteract__SMSText__c = smsText,
                    smagicinteract__external_field__c = 'ee7529ea283edee8a0e0d2403b289397',
                    OwnerId = ownerId
                );
        }
        
        System.RunAs(user)
        {
            insert sms;
        }
        
        if (inboundSMS)
        {
            return 
                [
                    SELECT Id,OwnerId,CreatedDate,smagicinteract__Contact__c,smagicinteract__Lead__c,smagicinteract__Mobile_Number__c,smagicinteract__SMS_Text__c 
                    FROM smagicinteract__Incoming_SMS__c
                    WHERE Id = : sms.Id
                ];
        }
        else
        {
            return 
                [
                    SELECT Id,OwnerId,CreatedDate,smagicinteract__Contact__c,smagicinteract__Lead__c,smagicinteract__PhoneNumber__c,smagicinteract__SMSText__c 
                    FROM smagicinteract__smsMagic__c
                    WHERE Id = : sms.Id
                ];
        }
    }
}