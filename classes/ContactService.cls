public class ContactService 
{
    public static Contact get(String identifier)
    {
        return new ContactGetQuery().query(identifier, Constants.CONTACT_FIELDS, false);
    }

    public static Contact get(String identifier, String fields, Boolean forUpdate)
    {
        return new ContactGetQuery().query(identifier, fields, forUpdate);
    }

    public static List<Contact> getMany(Set<Id> ids)
    {
        return new ContactGetManyQuery().query(ids, Constants.CONTACT_FIELDS, false);
    }

    public static List<Contact> getMany(Set<Id> ids, String fields, Boolean forUpdate)
    {
        return new ContactGetManyQuery().query(ids, fields, forUpdate);
    }

    public static Contact getByName(String name)
    {
        List<Contact> items = new ContactGetByNameQuery().query(new Set<String> { name });

        return items.size() == 1 ? items[0] : null;
    }

    public static List<Contact> getByName(Set<String> names)
    {
        return new ContactGetByNameQuery().query(names);
    }

    public static ActionResult reassociateRolloverCampaign(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        return new StudentReassociateCampaignCommand().execute(items, oldMap, triggerContext);
    }

    public static ActionResult syncRolloverStatusToCampaign(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        return new ContactSyncRolloverStatusCommand().execute(items, oldMap, triggerContext);
    }

    public static ActionResult trackStatusHistory(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        return new ContactTrackStatusHistoryCommand().execute(items, oldMap, triggerContext);
    }

    public static ActionResult formatPhoneNumbers(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        return new ContactFormatPhoneNumbersCommand().execute(items, oldMap, triggerContext);
    }

    public static ActionResult reassociateWarmCampaign(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        return new ProspectReassociateCampaignCommand().execute(items, oldMap, triggerContext);
    }

    public static ActionResult handleRemovalCampaignMember(Set<Id> ids)
    {
        return new ContactCampaignMemberDeletionCommand().execute(ids);
    }

    public static ActionResult updateDoNotCall(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        return new DoNoCallContactCommand().execute(items, oldMap, triggerContext);
    }

    @InvocableMethod(label='Handle Contact missed calls' description='A method which handles missed first attmept count')
    public static void handleMissedCall(List<Task> params)
    {
        // TODO: Delete post Release 41
    }

    public static ActionResult updateRolloverDetails(List<SObject> items)
    {
        return new ContactUpdateRolloverDetailsCommand().execute(items);
    }

    public static ActionResult reassociateAcquisitionCampaign(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        return new ContactReassociateHotCampaignCommand().execute(items, oldMap, triggerContext);
    }

    public static ActionResult identifyDuplicates(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        return new ContactIdentifyDuplicatesCommand().execute(items, oldMap, triggerContext); 
    }

    public static ActionResult validateDeletion(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        return new ContactDeleteValidation().execute(items, oldMap, triggerContext); 
    }

    public static ActionResult createOpportunity(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        return new ContactCreateOpportunityCommand().execute(items, oldMap, triggerContext);
    }
    
    public static ActionResult updateOpportunity(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        return new ContactUpdateOpportunityCommand().execute(items, oldMap, triggerContext);
    }

    public static ActionResult updateStatus(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        return new ContactUpdateStatusCommand().execute(items, oldMap, triggerContext);
    }

    public static ActionResult deleteContact(Id contactId)
    {
        return new ContactDeleteCommand().execute(new Set<Id> { contactId });
    }

    public static ActionResult deleteContacts(Set<Id> ids)
    {
        return new ContactDeleteCommand().execute(ids);
    }

    public static ActionResult encryptData (List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        return new ContactEncryptDataCommand().execute(items, oldMap, triggerContext);
    }

    public static ViewModelsAcquisition.Prospect getProspect(ViewModels.Param param)
    {
        return new ContactGetProspectQuery().query(param);
    }

    public static ActionResult saveProspect(ViewModels.Param param)
    {
        return new ContactSaveProspectCommand().command(param);
    }

    public static ActionResult saveInterestingMoment(ViewModels.Param param)
    {
        return new ContactSaveInterestingMomentCommand().command(param);
    }

    public static ActionResult uploadAttachment(ViewModelsFile.Upload param)
    {
        return new ContactUploadAttachmentCommand().execute(new List<ViewModelsFile.Upload> {param});
    }

    public static ActionResult requestVerifyEmail(ViewModels.Param param)
    {
        return new ContactRequestVerifyEmailCommand().command(param);
    }

    public static ActionResult sendEmailVerification(List<Contact> contacts)
    {
        return new ContactSendEmailVerificationCommand().execute(contacts, null, null);
    }

    public static ActionResult saveMilestone(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        return new ProspectSaveMilestoneCommand().execute(items, oldMap, triggerContext);
    }

    public static ActionResult setCountryFieldsValue(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        return new ContactSetCountryFieldsValueCommand().execute(items, oldMap, triggerContext);
    }

    public static ActionResult setFinalCourseConsultant(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        return new ContactSetCourseConsultantCommand().execute(items, oldMap, triggerContext);
    }

    public static ActionResult createReferral(ViewModels.Param param)
    {
        return new ContactCreateReferralCommand().command(param);
    }

    public static ActionResult sendCalculatorEmail(ViewModels.Param param)
    {
        return new ContactSendCalculatorEmailCommand().command(param);
    }

    public static ActionResult validateAccount(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        return new ContactCreateValidation().execute(items, oldMap, triggerContext);
    }

    public static ViewModelsAcquisition.Prospect getProspectFiles(ViewModels.Param param)
    {
        return new ContactGetProspectQuery().queryFiles(param);
    }

    public static ActionResult updateOwnerFromAutoProcToCA(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        return new ContactUpdateOwnerCommand().execute(items, oldMap, triggerContext);
    }

    public static List<ViewModelsWorkforce.FacultyStaffPlanOffering> getSubjectOfferingFaculties(ViewModels.Param param)
    {
        return new ContactGetOfferingFacultiesQuery().query(param);
    }

    public static ActionResult updateFacultyEnrolmentStatus(ViewModels.Param param)
    {
        return new FacultyEnrolmentUpdateStatusCommand().command(param);
    }

    public static ActionResult sendSSOEmail(ViewModelsSSO.EmailLoginDetails param)
    {
        return new ContactSendSSOLoginDetailsCommand().command(param);
    }

    public static ActionResult enableSSO(List<Contact> contacts)
    {
        return new ContactEnableSSOCommand().execute(contacts, null, null);
    }

    public static ActionResult reenableSSO(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        return new ContactReenableSSOCommand().execute(items, oldMap, triggerContext);
    }

    public static ActionResult disableSSO(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        return new ContactDisableSSOCommand().execute(items, oldMap, triggerContext);
    }

    public static ActionResult resetSSOPassword(Contact contact)
    {
        return new ContactResetSSOPasswordCommand().execute(new List<Contact> { contact }, null, null);
    }

    public static ActionResult resetSSOPassword(ViewModels.Param param)
    {
        return new ContactResetSSOPasswordCommand().execute(new List<Contact>{ get(param.Identifier) }, null, null);
    }

    public static ActionResult sendSSOResetPasswordEmail(ViewModelsSSO.EmailLoginDetails param)
    {
        return new ContactSendSSOResetPassEmailCommand().command(param);
    }

    public static ActionResult sendPasswordResetQueuedEmail(Contact contact)
    {
        return new ContactSendPassResetQueuedCommand().execute(new List<Contact> { contact }, null, null);
    }
    
    public static ActionResult sendWelcomeEmail(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        return new ContactSendWelcomeEmailCommand().execute(items, oldMap, triggerContext);
    }
    
    public static ActionResult sendWelcomeSMS(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        return new ContactSendWelcomeSMSCommand().execute(items, oldMap, triggerContext);
    }

    public static ActionResult updateUserOnChange(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        return new ContactUpdateUserOnChangeCommand().execute(items, oldMap, triggerContext);
    }

    public static ActionResult unsubscribe(Viewmodels.Param param)
    {
        Contact contact = get(param.Identifier, 'Id, RecordType.DeveloperName', false);

        if (contact.RecordType.DeveloperName == Constants.RECORDTYPE_CONTACT_PROSPECT)
            return new ContactSaveProspectCommand().command(param);
        else
            return new ContactSaveCommand().command(param);
    }
}