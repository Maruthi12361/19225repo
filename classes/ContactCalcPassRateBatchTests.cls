@isTest
public class ContactCalcPassRateBatchTests
{
    private static List<String> cerebroIDs = new List<String> {'A001641336', 'A001641337'};

    @testSetup
    static void setup()
    {
        // Arrange
        TestUtilities.setupHEDA();

        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Account courseMBA = TestDataAccountMock.createCourseMBA(user);
        Account courseGCM = TestDataAccountMock.createCourseGCM(user);

        // MBA Stage By Stage Student
        Contact student = TestDataContactMock.createStudent(user, cerebroIDs[0]);
        TestDataCourseEnrolmentMock.create(user, courseGCM.Id, student.Id, 'Transferred');
        TestDataCourseEnrolmentMock.create(user, courseMBA.Id, student.Id, 'Ongoing');

        // MBA Direct Entry Student
        student = TestDataContactMock.createStudent(user, cerebroIDs[1]);
        TestDataCourseEnrolmentMock.create(user, courseMBA.Id, student.Id, 'Ongoing');
    }

    @isTest
    static void StudentsWithoutEnrolments_PassRateIsEmpty()
    {
        // Act
        Test.startTest();
        Database.executeBatch(new ContactCalcPassRateBatch());
        Test.stopTest();

        // Assert
        List<Contact> contacts =
            [
                SELECT Id,FailedSubjects__c,PassRate__c
                FROM Contact
                WHERE Cerebro_Student_ID__c IN : cerebroIDs
                ORDER BY Cerebro_Student_ID__c
            ];

        System.assertEquals(0, contacts[0].FailedSubjects__c);
        System.assertEquals(null, contacts[0].PassRate__c);
        System.assertEquals(0, contacts[1].FailedSubjects__c);
        System.assertEquals(null, contacts[1].PassRate__c);
    }

    @isTest
    static void StudentWithEnrolments_PassRateIsCalculated()
    {
        // Arrange
        List<hed__Program_Enrollment__c> courseEnrolments =
            [
                SELECT Id,hed__Contact__c,hed__Account__c
                FROM hed__Program_Enrollment__c
                WHERE hed__Contact__r.Cerebro_Student_ID__c = : cerebroIDs[0]
                ORDER BY hed__Account__r.Program__c
            ];
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        TestDataSubjectEnrolmentMock.create(user, courseEnrolments[0], 'TEST001', 'Test Subject 1', Date.today().addDays(-100), 'Transferred', 'Standard', 'Credit', 65);
        TestDataSubjectEnrolmentMock.create(user, courseEnrolments[0], 'TEST002', 'Test Subject 2', Date.today().addDays(-100), 'Transferred', 'Standard', 'Distinction', 75);
        TestDataSubjectEnrolmentMock.create(user, courseEnrolments[0], 'TEST003', 'Test Subject 3', Date.today().addDays(-100), 'Transferred', 'Standard', 'Pass 1', 55);
        TestDataSubjectEnrolmentMock.create(user, courseEnrolments[0], 'TEST004', 'Test Subject 4', Date.today().addDays(-100), 'Transferred', 'Standard', 'Pass 2', 50);
        TestDataSubjectEnrolmentMock.create(user, courseEnrolments[1], 'TEST005', 'Test Subject 5', Date.today().addDays(-50), 'Completed', 'Standard', 'Pass 2', 50);
        TestDataSubjectEnrolmentMock.create(user, courseEnrolments[1], 'TEST006', 'Test Subject 6', Date.today().addDays(-50), 'Medical Compassionate', 'Standard', 'Medical / Compassionate', null);
        TestDataSubjectEnrolmentMock.create(user, courseEnrolments[1], 'TEST007', 'Test Subject 7', Date.today().addDays(-50), 'Withdraw Fail', 'Standard', 'Withdraw Fail', null);
        TestDataSubjectEnrolmentMock.create(user, courseEnrolments[1], 'TEST008', 'Test Subject 8', Date.today().addDays(-50), 'Completed', 'Standard', 'Fail', 25);
        TestDataSubjectEnrolmentMock.create(user, courseEnrolments[1], 'TEST009', 'Test Subject 9', Date.today().addDays(-50), 'Withdraw Not Fail', 'Standard', 'Withdraw Not Fail', null);

        // Act
        Test.startTest();
        Database.executeBatch(new ContactCalcPassRateBatch());
        Test.stopTest();

        // Assert
        List<Contact> contacts =
            [
                SELECT Id,FailedSubjects__c,PassRate__c
                FROM Contact
                WHERE Cerebro_Student_ID__c IN : cerebroIDs
                ORDER BY Cerebro_Student_ID__c
            ];

        System.assertEquals(2, contacts[0].FailedSubjects__c);
        System.assertEquals(5 * 100 / 7, contacts[0].PassRate__c);
        System.assertEquals(0, contacts[1].FailedSubjects__c);
        System.assertEquals(null, contacts[1].PassRate__c);
    }
}