@isTest
private class ProcessBuilderLogTest {

    @testSetup
    static void setup()
    {
        TestUtilities.setupHEDA();
    }

    @isTest static void test_PrintToLog()
    {
        // Arrange        
        ProcessBuilderLog.ProcessBuilderParameter parameter = new ProcessBuilderLog.ProcessBuilderParameter();        
        parameter.processName = 'Test Process';
        parameter.actionName = 'Test Action';
        List<ProcessBuilderLog.ProcessBuilderParameter> params = new List<ProcessBuilderLog.ProcessBuilderParameter>();
        params.add(parameter);
        
        // Act    
        ProcessBuilderLog.PrintToLog(params);
        
    }
}