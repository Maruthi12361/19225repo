public class SubjectCreateVersionCommand extends BaseTriggerCommand
{
    static Id subjectDefinitionRecordTypeId = RecordTypeUtils.getRecordTypeId(Constants.RECORDTYPE_SUBJECT_DEFINITION, SObjectType.hed__Course__c);
    static Id subjectVersionRecordTypeId = RecordTypeUtils.getRecordTypeId(Constants.RECORDTYPE_SUBJECT_VERSION, SObjectType.hed__Course__c);
    static Set<Id> activeRecordSet = new Set<Id>();

    public override List<SObject> validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        List<hed__Course__c> result = new List<hed__Course__c>();

        for (hed__Course__c subject : (List<hed__Course__c>)items)
        {
            if ((triggerContext == Enums.TriggerContext.AFTER_INSERT || triggerContext == Enums.TriggerContext.AFTER_UPDATE) && subject.RecordTypeId == subjectDefinitionRecordTypeId)
            {
                if (activeRecordSet.contains(subject.Id))
                {
                    Log.info(this.className, 'validate', 'Skipped creating SubjectVersion for ' + subject);
                    continue;
                }

                result.add(subject);
            }
        }

        return result;
    }
    
    public override ActionResult command(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {        
        Map<Id, hed__Course__c> subjectMap = new Map<Id, hed__Course__c>((List<hed__Course__c>)items);

        if (triggerContext == Enums.TriggerContext.AFTER_UPDATE)
        {
            List<hed__Course__c> subjects =
                [
                    SELECT Id, (SELECT Id FROM Subjects__r LIMIT 1) 
                    FROM hed__Course__c
                    WHERE Id IN : subjectMap.keySet()
                ];
            
            for (hed__Course__c subject : subjects)
            {
                if (subject.Subjects__r.size() == 0)
                {
                    uow.registerNew(createSubjectVersion(subjectMap.get(subject.Id)));
                    activeRecordSet.add(subject.Id);
                }
            }
        }
        else if (triggerContext == Enums.TriggerContext.AFTER_INSERT)
        {
            for (hed__Course__c subject : (List<hed__Course__c>)items)
            {
                uow.registerNew(createSubjectVersion(subject));
                activeRecordSet.add(subject.Id);
            }
        }
        
        return new ActionResult();
    }
    
    private hed__Course__c createSubjectVersion(hed__Course__c parent)
    {
        hed__Course__c subjectVersion = new hed__Course__c(Name = parent.Name, RecordTypeId = subjectVersionRecordTypeId);
        subjectVersion.SubjectDefinition__c = parent.Id;
        subjectVersion.hed__Course_ID__c = parent.hed__Course_ID__c;
        subjectVersion.hed__Credit_Hours__c = parent.hed__Credit_Hours__c;
        subjectVersion.hed__Account__c = parent.hed__Account__c;
        subjectVersion.hed__Description__c = parent.hed__Description__c;
        subjectVersion.hed__Extended_Description__c = parent.hed__Extended_Description__c;
        subjectVersion.DisciplineCode__c = parent.DisciplineCode__c;
        subjectVersion.AQFLevel__c = parent.AQFLevel__c;
        subjectVersion.CerebroID__c = parent.CerebroID__c;
        subjectVersion.InternalNotes__c = parent.InternalNotes__c;
        subjectVersion.TCSIUID__c = parent.TCSIUID__c;
        subjectVersion.VersionNumber__c = 1;
        subjectVersion.IsPrimary__c = True;
        subjectVersion.EFTSLValue__c = parent.EFTSLValue__c;
        subjectVersion.EffectiveFromDate__c = parent.EffectiveFromDate__c;
        subjectVersion.EffectiveToDate__c = parent.EffectiveToDate__c;
        subjectVersion.SubjectYearLongIndicator__c = parent.SubjectYearLongIndicator__c;
        Log.info(this.className, 'createSubjectVersion', 'Created SubjectVersion for ' + parent);

        return subjectVersion;
    }
}