@isTest
public class AIBGovernorLimitsTest
{
    @testSetup static void setup()
    {
        TestUtilities.setupHEDA();
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createProspectHot(user);
        TestDataCaseMock.createStudentSupportCase(user, contact);
    }

    @isTest
    static void UpdateUnchangedCase_DMLCountIncreased()
    {
        AIBGovernorLimits govLimits = new AIBGovernorLimits();
        Test.startTest();

        System.assertEquals(10, govLimits.soqlCount);
        System.assertEquals(15, govLimits.dmlCount);
        System.assertEquals(2, govLimits.soslCount);
        System.assertEquals(1000, govLimits.cpuTime);

        govLimits.initialiseGLCounts();

        System.assertEquals(0, govLimits.soqlCount);
        System.assertEquals(0, govLimits.dmlCount);
        System.assertEquals(0, govLimits.soslCount);
        System.assertEquals(0, govLimits.cpuTime);

        System.assertEquals(Limits.getLimitQueries(), govLimits.soqlCountTotal);
        System.assertEquals(Limits.getLimitDmlStatements(), govLimits.dmlCountTotal);
        System.assertEquals(Limits.getLimitSoslQueries(), govLimits.soslCountTotal);
        System.assertEquals(limits.getLimitCpuTime(), govLimits.cpuTimeTotal);

        Case c = [SELECT Id, Status FROM Case Limit 1];
        govLimits.trackGLCounts();

        System.assertEquals(1, govLimits.soqlCount);  // From the SELECT of Case above
        System.assertEquals(0, govLimits.dmlCount);
        System.assertEquals(0, govLimits.soslCount);
        System.assertNotEquals(0, govLimits.cpuTime);

        // Note: by NOT actually making any changes to the Case object we can limit the DML to a single update statement. Otherwise DML count in the assertion
        //       below will change based on the number of commands triggered from CaseTriggerHandler by the update, which changes as we write new functionality.
        update c;
        govLimits.trackGLCounts();
        Test.stopTest();

        System.assertNotEquals(0, govLimits.soqlCountDelta);
        System.assert(govLimits.soqlCountDelta < govLimits.soqlCount);
        System.assertEquals(1, govLimits.dmlCountDelta);   // From the single UPDATE 
        System.assertEquals(0, govLimits.soslCountDelta);
        System.assert(govLimits.cpuTimeDelta < govLimits.cpuTime);
    }
}