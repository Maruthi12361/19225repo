global class OpportunityMoveTermSchedule implements Schedulable
{
    global void execute(SchedulableContext sc) 
    {
        database.executebatch(new OpportunityMoveTermBatch(), 25);
    }
}