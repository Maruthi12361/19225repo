public class ActionResult
{
    public ResultStatus status { get; set; }

    public Id resultId { get; set; }

    public List<String> messages { get; set; }

    public Interfaces.IViewModel resultInfo { get; set; }

    public String message 
    {
        get
        {
            return String.join(this.messages, ', ').trim();
        }
    }

    public Boolean isSuccess
    {
        get
        {
            return (status == ResultStatus.SUCCESS);
        }
    }

    public Boolean isError
    {
        get
        {
            return (status == ResultStatus.ERROR);
        }
    }

    public ActionResult()
    {
        status = ResultStatus.SUCCESS;
        messages = new List<String>();
    }

    public ActionResult(Id id)
    {
        status = ResultStatus.SUCCESS;
        messages = new List<String>();
        resultId = id;
    }

    public ActionResult(ResultStatus status)
    {
        this.messages = new List<String>();
        this.status = status;
    }

    public ActionResult(ResultStatus status, List<String> messages)
    {
        this.messages = messages;
        this.status = status;
    }

    public ActionResult(ResultStatus status, Interfaces.IViewModel resultInfo)
    {
        this.status = status;
        this.messages = new List<String>();
        this.resultInfo = resultInfo;
    }

    public ActionResult(ResultStatus status, String message)
    {
        this.messages = new List<String> { message };
        this.status = status;
    }
}