public class ContactGetQuery
{
    public Contact query(String identifier, String fields, Boolean forUpdate)
    {
        List<Contact> contacts;
        String query = 'SELECT ' + fields + ' FROM Contact ';

        if (String.isBlank(identifier))
            return null;
        else if (identifier.startsWith(Constants.ENTITYID_CONTACT))
        {
            query += 'WHERE Id = :identifier';
        }
        else if (identifier.contains('@'))
        {
            query += 'WHERE PersonalEmail__c = :identifier OR hed__WorkEmail__c = :identifier OR hed__UniversityEmail__c = :identifier OR hed__AlternateEmail__c = :identifier';
        }
        else
        {
            query += 'WHERE EncryptedId__c = :identifier';

            if (forUpdate)
                query += ' FOR UPDATE';

            contacts = Database.query(query);

            if (contacts.size() > 0)
                return contacts[0];

            Id contactId;
            String recordId = EncryptionUtil.Decrypt(identifier);

            if (recordId.startsWith(Constants.ENTITYID_CONTACT))
                contactId = recordId;
            else if (recordId.startsWith(Constants.ENTITYID_OPPORTUNITY))
                contactId = getContactId(recordId);
            else
            {
                Log.error('ContactGetQuery', 'queryByParam', 'Identifier does not belong to contact or opportunity: ' + recordId);
                throw new Exceptions.ApplicationException('Unable to retrive record data');
            }

            query = 'SELECT ' + fields + ' FROM Contact WHERE Id = :contactId';
        }

        if (forUpdate)
            query += ' FOR UPDATE';

        contacts = Database.query(query);

        if (contacts.size() > 0)
            return contacts[0];
        else
            return null;
    }

    private static Id getContactId(Id oppId)
    {
        List<Opportunity> opps = [SELECT Id, Contact__c FROM Opportunity WHERE Id = :oppId];

        if (opps.size() == 0)
        {
            Log.error('ContactGetQuery', 'getContactId', 'Opportunity does not exists in db - ' + oppId);
            throw new Exceptions.ApplicationException('Unable to retrive record data');
        }

        if (opps[0].Contact__c == null)
        {
            Log.error('ContactGetQuery', 'getContactId', 'Opportunity does not have contact reference ' + oppId);
            throw new Exceptions.ApplicationException('Unable to retrive record data');
        }

        return opps[0].Contact__c;
    }
}