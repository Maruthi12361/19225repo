@isTest
public class ContactDisableSSOCommandTests
{
    @isTest
    static void ProspectSSOUserDoesNotExist_SSOStatusSetToNone()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        TestUtilities.disableComponent('ContactSendSSOLoginDetailsCommand');
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createProspectCold(user, 'John', 'Test','61408815278', 'john.test@yahoo.com', 'A0000000000');
        contact.SingleSignOnStatus__c = Constants.CONTACT_SINGLESIGNONSTATUS_PROSPECTENABLED;
        update contact;

        // Act
        Test.startTest();
        ActionResult result = ContactService.disableSSO(new List<Contact> { contact }, new Map<Id, SObject>(), Enums.TriggerContext.AFTER_UPDATE);
        Test.stopTest();

        Contact updatedContact = [SELECT Id, SingleSignOnStatus__c, SingleSignOnStatusLastUpdated__c FROM Contact WHERE Id = :contact.Id];

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.status, 'Unexpected result status');
        System.assertEquals(Constants.CONTACT_SINGLESIGNONSTATUS_NONE, updatedContact.SingleSignOnStatus__c, 'Unexpected single sign on status');
        System.assert(updatedContact.SingleSignOnStatusLastUpdated__c != null, 'Expected single sign on status last update date to have value');
    }

    @isTest
    static void ProspectSSOEnabled_SuccessfullyDisabledSSO()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        TestUtilities.disableComponent('ContactSendSSOLoginDetailsCommand');
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createProspectCold(user, 'John', 'Test','61408815278', 'john.test@yahoo.com', 'A0019875478');
        contact.SingleSignOnStatus__c = Constants.CONTACT_SINGLESIGNONSTATUS_PROSPECTENABLED;
        update contact;

        // Act
        Test.startTest();
        ActionResult result = ContactService.disableSSO(new List<Contact> { contact }, new Map<Id, SObject>(), Enums.TriggerContext.AFTER_UPDATE);
        Test.stopTest();

        Contact updatedContact = [SELECT Id, SingleSignOnStatus__c, SingleSignOnStatusLastUpdated__c FROM Contact WHERE Id = :contact.Id];

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.status, 'Unexpected result status');
        System.assertEquals(Constants.CONTACT_SINGLESIGNONSTATUS_DISABLED, updatedContact.SingleSignOnStatus__c, 'Unexpected single sign on status');
        System.assert(updatedContact.SingleSignOnStatusLastUpdated__c != null, 'Expected single sign on status last update date to have value');
    }
}