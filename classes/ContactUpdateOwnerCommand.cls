public class ContactUpdateOwnerCommand extends BaseTriggerCommand
{
    protected override List<SObject> validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        List<Contact> result = new List<Contact>();

        for (Contact contact : (List<Contact>)items)
            if (contact.OwnerId == Constants.USERID_AUTOPROC && contact.RecordTypeId == System.Label.ID_RecordType_Contact_Prospect)
                result.add(contact);

        return result;
    }

    protected override ActionResult command(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        for (Contact contact : (List<Contact>)items)
            if (contact.OwnerId == Constants.USERID_AUTOPROC && contact.RecordTypeId == System.Label.ID_RecordType_Contact_Prospect)
                contact.OwnerId = Constants.USERID_OAF;

        return new ActionResult(ResultStatus.SUCCESS);
    }
}