public class PhoneNumberUtilities
{
    private static List<Country_Phone_Code__c> countryCodes;
    private static Map<String, String> countryNameToCodes;
    private static Map<String, String> countryCodeToNames;

    public static String cleanNumber(String phoneNumber)
    {
        if (String.isBlank(phoneNumber)) 
            return '';

        String resultNumber = (phoneNUmber.trim().startsWith('+') ? '+' : '') + phoneNumber.replaceAll('[^0-9]+', '');

        Log.info('PhoneNumberUtilities', 'cleanNumber', 'Cleaning invalid characters from phone number: "{0}" to "{1}"', new List<String> { phoneNumber, resultNumber }); 

        return resultNumber;
    }

    private static void loadCountryCodes()
    {
        if (countryCodes == null || countryNameToCodes == null|| countryCodeToNames == null)
        {
            countryCodes = [SELECT Name, Phone_Code__c, CountryCode__c from Country_Phone_Code__c ORDER BY Name];
            countryNameToCodes = new Map<String, String>();
            countryCodeToNames = new Map<String, String>();

            for (Country_Phone_Code__c item : countryCodes)
            {
                countryNameToCodes.put(item.Name.toLowerCase().capitalize(), String.isBlank(item.CountryCode__c) ? '' : item.CountryCode__c.toUpperCase());
                if (!String.isBlank(item.CountryCode__c))
                    countryCodeToNames.put(item.CountryCode__c.toUpperCase(), item.Name.toLowerCase().capitalize());
            }
        }
    }

    public static String getCountryCode(String countryName)
    {
        loadCountryCodes();

        return String.isBlank(countryName) ? null : countryNameToCodes.get(countryName.trim().toLowerCase().capitalize());
    }

    public static String getCountryName(String countryCode)
    {
        loadCountryCodes();

        return String.isBlank(countryCode) ? null : countryCodeToNames.get(countryCode.trim().toUpperCase());
    }
}