@IsTest
public class ContactSendCalculatorEmailCommandTest
{
    @testSetup
    public static void setup()
    {
        TestUtilities.setupHEDA();
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact contact = TestDataContactMock.createProspectCold(user, '0455548889');
    }

    @isTest
    static void BlankIdentifier_EmptyParameterError()
    {
        // Arrange
        String errorMessage = '';
        ViewModels.Param param = new ViewModels.Param();
        param.Identifier = '';
        param.Payload = null;

        // Act
        try
        {
           ActionResult result = new ContactSendCalculatorEmailCommand().command(param);
        }
        catch (Exception ex)
        {
           errorMessage = ex.getMessage();
        }

        // Assert
        System.assertEquals(String.format(Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Identifier' }), errorMessage, 'Unexpected error message');
    }

    @isTest
    static void BlankPayload_EmptyPayloadError()
    {
        // Arrange
        String errorMessage = '';
        ViewModels.Param param = new ViewModels.Param();
        param.Identifier = 'myemail@email.com';
        param.Payload = null;

        // Act
        try
        {
           ActionResult result = new ContactSendCalculatorEmailCommand().command(param);
        }
        catch (Exception ex)
        {
           errorMessage = ex.getMessage();
        }

        // Assert
        System.assertEquals(String.format(Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Calculator' }), errorMessage, 'Unexpected error message');
    }

    @isTest
    static void SendCalculatorEmail_EmailSuccessfullySent()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        Contact contact = [SELECT Id FROM Contact LIMIT 1];

        Map<String, Object> emailMap = new Map<String, Object>
        {
            'EmailName' => 'Calculator_FeeHelp'
        };

        ViewModels.Param param = new ViewModels.Param();
        param.Identifier = contact.Id;
        param.Payload = JSON.serialize(emailMap);

        // Act
        Test.startTest();
        Integer callCount = Limits.getFutureCalls();
        ActionResult result = new ContactSendCalculatorEmailCommand().command(param);
        callCount = Limits.getFutureCalls() - callCount;
        Test.stopTest();

        // Assert
        System.assertEquals(true, result.isSuccess);
        System.assertEquals(2, callCount);
    }

    @isTest
    static void SendCalculatorEmail_EmailAndImageCacheSuccessfullySent()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        Contact contact = [SELECT Id, CalculatorPayPeriod__c, CalculatorSalaryPerAnnum__c FROM Contact LIMIT 1];
        contact.CalculatorSalaryPerAnnum__c = 78000;
        contact.CalculatorPayPeriod__c = 'Weekly';

        Map<String, Object> emailMap = new Map<String, Object>
        {
            'EmailName' => 'Calculator_FeeHelp'
        };

        ViewModels.Param param = new ViewModels.Param();
        param.Identifier = contact.Id;
        param.Payload = JSON.serialize(emailMap);

        // Act
        update contact;
        Test.startTest();
        Integer callCount = Limits.getFutureCalls();
        ActionResult result = new ContactSendCalculatorEmailCommand().command(param);
        callCount = Limits.getFutureCalls() - callCount;
        Test.stopTest();

        // Assert
        System.assertEquals(true, result.isSuccess);
        System.assertEquals(2, callCount);
    }
}