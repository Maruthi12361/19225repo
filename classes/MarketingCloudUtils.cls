public class MarketingCloudUtils
{
    private static String accessToken;

    public static ActionResult sendRequest(Interfaces.IViewModel model)
    {
 		ActionResult result = new ActionResult(ResultStatus.SUCCESS, new List<String>{'Message is successfully sent via MarketingCloud'});
        HTTPResponse response = getResponse('POST', Constants.MARKETINGCLOUD_SEND_ENDPOINT, 'Bearer '+ getAccessToken(), JSON.serialize(model));
        List<Object> errors;

        if (response.getStatusCode() != 201)
        {
            Log.error('MarketingCloudUtils', 'sendRequest', 'Could not send message. Error Code: {0}, Error Body: {1}', new String[]{ String.valueOf(response.getStatusCode()), response.getBody() });

            result.status = ResultStatus.ERROR;
            result.messages = new List<String>{String.valueOf(response.getStatusCode()), response.getBody()};

            return result;
        }

        Log.info( 'MarketingCloudUtils', 'sendMessage', 'Successfully send message through Marketing Cloud API. Response from server: {0}.', new String[]{ response.getBody() });

        return result;
    }
    
    public static ActionResult sendEmail(ViewModelsMail.Email mail)
    {
        ViewModelsMail.MarketingCloudBody data = null;

        if (mail.CcAddress != null)
        {
            String ccAddress='';

            for (ViewModelsMail.Recipient ccMail : mail.CcAddress)
                ccAddress += ccMail.email + ';';

            data = new ViewModelsMail.MarketingCloudBody(String.valueOf(DateTime.now().getTime()), 'AU', null, mail.ToAddress, mail.Subject, mail.OwnerName, mail.OwnerEmail, mail.BodyText, mail.BodyHtml, null, ccAddress, null);
        }
        else
            data = new ViewModelsMail.MarketingCloudBody(String.valueOf(DateTime.now().getTime()), 'AU', null, mail.ToAddress, mail.Subject, mail.OwnerName, mail.OwnerEmail, mail.BodyText, mail.BodyHtml, null, null, null);

        ViewModelsMail.MarketingCloudPayload payload = new ViewModelsMail.MarketingCloudPayload(String.valueOf(DateTime.now().getTime()), Constants.MARKETINGCLOUD_SEND_EVENT, data);
        
        if (String.isBlank(((ViewModelsMail.MarketingCloudBody)payload.Data).EmailBodyText))
            ((ViewModelsMail.MarketingCloudBody)payload.Data).EmailBodyText = null;

        if (String.isBlank(((ViewModelsMail.MarketingCloudBody)payload.Data).EmailBodyHtml))
            ((ViewModelsMail.MarketingCloudBody)payload.Data).EmailBodyHtml = null;

        return sendRequest(payload);
    }
    
    public static ActionResult sendSMS(ViewModelsSMS.Details details)
    {
        ViewModelsSMS.MarketingCloudBody data = new ViewModelsSMS.MarketingCloudBody(String.valueOf(DateTime.now().getTime()), 'AU', '', details.ToPhoneNumber, details.Message);
        ViewModelsSMS.MarketingCloudPayload payload = new ViewModelsSMS.MarketingCloudPayload(String.valueOf(DateTime.now().getTime()), Constants.MARKETINGCLOUD_SEND_EVENT, data);
        
        return sendRequest(payload);
    }
    
    private static String getAccessToken()
    {
        if (accessToken != null)
            return accessToken;

        String body = '{ "grant_type" : "' + Constants.MARKETINGCLOUD_AUTH_GRANTTYPE
                        + '", "client_id" : "' + Constants.MARKETINGCLOUD_AUTH_CLIENTID
                        + '", "client_secret" : "' +  Constants.MARKETINGCLOUD_AUTH_CLIENTSECRET
                        + '", "account_id" : "' + Constants.MARKETINGCLOUD_AUTH_ACCOUNTID 
                        + '" }';

        HttpResponse response = getResponse('POST', Constants.MARKETINGCLOUD_AUTH_ENDPOINT, null, body);

        if (response.getStatusCode() != 200)
        {
            Log.error('MarketingCloudUtils', 'getAccessToken', 'Error in retrieving access token. Error Code: ' + response.getStatusCode());

            return null;
        }

        Map<String, Object> m = (Map<String, Object>)JSON.deserializeUntyped(response.getBody());
        accessToken = (String) m.get('access_token');

        Log.info('MarketingCloudUtils', 'getAccessToken', 'Retrieved access token: ' + accessToken);

        return accessToken;
    }

    private static HttpResponse getResponse(String method, String endpoint, String authorization, String payload)
    {
        Http http = new Http();
        HttpRequest request = new HttpRequest();

        request.setEndpoint(endpoint);
        request.setMethod(method);
        request.setHeader('Cache-Control', 'no-cache');
        request.setHeader('Content-Type', 'application/json');
        request.setHeader('Accept', 'application/json');
        request.setTimeout(120000);
        request.setBody(payload);

        if (authorization != null)
            request.setHeader('Authorization', authorization);

        return http.send(request);
    }    
}