public class CurrentUserProfileController {

      @AuraEnabled
      public static User getUser() {
             User user = [select FirstName, LastName, Title, Department, EmployeeNumber, FullPhotoUrl 
                                from User 
                                where Id =: Userinfo.getUserId()];
             return user;
       }
}