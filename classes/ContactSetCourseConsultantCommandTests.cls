@isTest
public class ContactSetCourseConsultantCommandTests
{
    @testSetup
    static void setup()
    {
        TestUtilities.setupHEDA();
    }

    @isTest
    static void ContactOwnerChangesFromCCToCA_SetFinalCourseConsultantField()
    {
        // Arrange
        GroupMember gmCC = [SELECT userOrGroupId FROM GroupMember WHERE Group.DeveloperName =: Constants.GROUP_COURSE_CONSULTANTS LIMIT 1];
        GroupMember gmCA = [SELECT userOrGroupId FROM GroupMember WHERE Group.DeveloperName =: Constants.GROUP_COURSE_ADVISORS LIMIT 1];
        User userCC = UserService.get(gmCC.UserOrGroupId);
        Contact contact = TestDataContactMock.createProspectWarm(userCC);
        Map<Id, Contact> oldMap = new Map<Id, Contact>{ contact.Id => contact };
        Contact newContact = contact.clone(true, false, true, true);
        newContact.OwnerId = gmCA.UserOrGroupId;
        List<Contact> items = new List<Contact>{ newContact };

        // Act
        Test.startTest();
        ActionResult result = ContactService.setFinalCourseConsultant(items, oldMap, Enums.TriggerContext.BEFORE_UPDATE);
        Test.stopTest();

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.status, 'Unexpected result status');
        System.assertEquals(null, contact.FinalCourseConsultant__c, 'Before update the FinalCourseConsultant field should have initially been blank.');
        System.assertEquals(userCC.Id, newContact.FinalCourseConsultant__c, 'FinalCourseConsultant field should have been set to the courseConsultant');
    }

    @isTest
    static void ContactOwnerChangesFromNonCCToCA_DoNotSetFinalCourseConsultantField()
    {
        // Arrange
        GroupMember gmCA = [SELECT userOrGroupId FROM GroupMember WHERE Group.DeveloperName =: Constants.GROUP_COURSE_ADVISORS LIMIT 1];
        User userNotCC = UserService.getbyAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createProspectWarm(userNotCC);
        Map<Id, Contact> oldMap = new Map<Id, Contact>{ contact.Id => contact };
        Contact newContact = contact.clone(true, false, true, true);
        newContact.OwnerId = gmCA.UserOrGroupId;
        List<Contact> items = new List<Contact>{ newContact };

        // Act
        Test.startTest();
        ActionResult result = ContactService.setFinalCourseConsultant(items, oldMap, Enums.TriggerContext.BEFORE_UPDATE);
        Test.stopTest();

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.status, 'Unexpected result status');
        System.assertEquals(null, contact.FinalCourseConsultant__c, 'Before update the FinalCourseConsultant field should have initially been blank.');
        System.assertEquals(null, newContact.FinalCourseConsultant__c, 'After update the FinalCourseConsultant field should still be blank as we didnt have a CourseConsultant owner initially.');
    }
}