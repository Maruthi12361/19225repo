@isTest
public class TestDataSingleSignOnConfigurationMock
{
    public static SingleSignOnConfiguration__mdt create()
    {
        SingleSignOnConfiguration__mdt singleSignOnConfig = new SingleSignOnConfiguration__mdt();
        singleSignOnConfig.AzureAPIEndpoint__c = 'https://graph.windows.net/aibtest.com.au';
        singleSignOnConfig.AzureAPIID__c = 'cb82e551-f0fd-49ef-89c3-azureId';
        singleSignOnConfig.AzureAPISecret__c = 'secretDBTj3VZjaq64hLqU4M/t+TfCojgTQX01GxLZB8=';
        singleSignOnConfig.AzurePassword__c = 'by4YcD~3!azure';
        singleSignOnConfig.AzureUsername__c = 'svc-salesforce-azure-sync@aibtest.edu.au';
        singleSignOnConfig.CerebroAPIEndpoint__c = 'https://api.aibtest.edu.au';
        singleSignOnConfig.CerebroResource__c = 'https://testaibcorpoutlook.onmicrosoft.com/74880207-2d83-4ed4-9a1e-3ef1af68d124';
        singleSignOnConfig.DeveloperName = ConfigUtility.getCurrentEnvironment();
        singleSignOnConfig.Label = ConfigUtility.getCurrentEnvironment();
        singleSignOnConfig.QualifiedApiName = ConfigUtility.getCurrentEnvironment();
        singleSignOnConfig.StudentDomain__c = '@study.aibtest.edu.au';

        return singleSignOnConfig;
    }
}