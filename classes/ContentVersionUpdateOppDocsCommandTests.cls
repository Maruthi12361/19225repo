@isTest
public class ContentVersionUpdateOppDocsCommandTests
{
    @testSetup
    public static void setup()
    {
        TestUtilities.setupHEDA();
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createProspectHot(user);
        TestDataOpportunityMock.createApplyingAcquisition(user, contact);
    }

    @isTest
    static void ProofOfIdOpportunityFileCreated_ProofOfIdDocsValueSetSuccessfully()
    {
        // Arrange
        Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];

        // Act
        Test.startTest();
        ContentVersion contentVersion = TestDataContentVersionMock.create(UserService.getByAlias(Constants.USER_SYSTEMADMIN), 'Id.txt', 'Id', opp.Id, Constants.CONTENTVERSION_TYPE_PROOFOFID);
        Test.stopTest();
        Opportunity updatedOpportunity = [SELECT Id, ProofOfIDDocs__c FROM Opportunity WHERE Id = :opp.Id];

        // Assert
        System.assertEquals(Constants.DOCSTATUS_REQUIRESREVIEW, updatedOpportunity.ProofOfIDDocs__c, 'Unexpected Proof of Id docs value');
    }

    @isTest
    static void TranscriptOpportunityFileCreated_TranscriptDocsValueSetSuccessfully()
    {
        // Arrange
        Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];

        // Act
        Test.startTest();
        ContentVersion contentVersion = TestDataContentVersionMock.create(UserService.getByAlias(Constants.USER_SYSTEMADMIN), 'Transcript.txt', 'Transcript', opp.Id, Constants.CONTENTVERSION_TYPE_TRANSCRIPT);
        Test.stopTest();
        Opportunity updatedOpportunity = [SELECT Id, TranscriptDocs__c FROM Opportunity WHERE Id = :opp.Id];

        // Assert
        System.assertEquals(Constants.DOCSTATUS_REQUIRESREVIEW, updatedOpportunity.TranscriptDocs__c, 'Unexpected Transcript docs value');
    }

    @isTest
    static void EnglishProficiencyOpportunityFileCreated_TranscriptDocsValueSetSuccessfully()
    {
        // Arrange
        Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];

        // Act
        Test.startTest();
        ContentVersion contentVersion = TestDataContentVersionMock.create(UserService.getByAlias(Constants.USER_SYSTEMADMIN), 'EnglishProficiency.txt', 'EnglishProficiency', opp.Id, Constants.CONTENTVERSION_TYPE_ENGLISHPROFICIENCY);
        Test.stopTest();
        Opportunity updatedOpportunity = [SELECT Id, EnglishProficiencyDocs__c FROM Opportunity WHERE Id = :opp.Id];

        // Assert
        System.assertEquals(Constants.DOCSTATUS_REQUIRESREVIEW, updatedOpportunity.EnglishProficiencyDocs__c, 'Unexpected english proficiency docs value');
    }

    @isTest
    static void WorkExperienceOpportunityFileCreated_TranscriptDocsValueSetSuccessfully()
    {
        // Arrange
        Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];

        // Act
        Test.startTest();
        ContentVersion contentVersion = TestDataContentVersionMock.create(UserService.getByAlias(Constants.USER_SYSTEMADMIN), 'WorkExperience.txt', 'WorkExperience', opp.Id, Constants.CONTENTVERSION_TYPE_WORKEXPERIENCE);
        Test.stopTest();
        Opportunity updatedOpportunity = [SELECT Id, WorkExperienceDocs__c FROM Opportunity WHERE Id = :opp.Id];

        // Assert
        System.assertEquals(Constants.DOCSTATUS_REQUIRESREVIEW, updatedOpportunity.WorkExperienceDocs__c, 'Unexpected work experience docs value');
    }

    @isTest
    static void SpecialNeedsOpportunityFileCreated_SpecialNeedsDocsValueSetSuccessfully()
    {
        // Arrange
        Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];

        // Act
        Test.startTest();
        ContentVersion contentVersion = TestDataContentVersionMock.create(UserService.getByAlias(Constants.USER_SYSTEMADMIN), 'SpecialNeeds.txt', 'SpecialNeeds', opp.Id, Constants.CONTENTVERSION_TYPE_SPECIALNEEDS);
        Test.stopTest();
        Opportunity updatedOpportunity = [SELECT Id, SpecialNeedsDocs__c FROM Opportunity WHERE Id = :opp.Id];

        // Assert
        System.assertEquals(Constants.DOCSTATUS_REQUIRESREVIEW, updatedOpportunity.SpecialNeedsDocs__c, 'Unexpected special needs docs value');
    }
}