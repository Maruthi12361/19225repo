public class EmailSendViaSendGridCommand extends BaseViewModelCommand 
{
    public override List<Interfaces.IViewModel> validate(List<Interfaces.IViewModel> params)
    {
        return params;
    }

    public override ActionResult command(List<Interfaces.IViewModel> params)
    {
        ActionResult result = new ActionResult(ResultStatus.SUCCESS, new List<String>{'Email is successfully sent via SendGrid'});

        for (ViewModelsMail.Mail param: (List<ViewModelsMail.Mail>) params)
        {
            if (ConfigUtility.isSandbox() && !Test.isRunningTest())
            {
                param = overrideEmailForSandbox(param);
            }

            Http http = new Http();
            HttpRequest request = new HttpRequest();
    
            request.setEndpoint(Constants.SENDGRID_SEND_ENDPOINT);
            request.setMethod('POST');
            request.setHeader('Authorization', 'Bearer ' + Label.SendGridAPIKey);
            request.setHeader('Content-Type', 'application/json');
            request.setHeader('Cache-Control', 'no-cache');
            request.setHeader('Accept', 'text/plain; charset=utf-8');
            request.setBody(StringUtils.serializeMail(param));
            HttpResponse response = http.send(request);

            List<Object> errors;
            List<String> messages = new List<String>();

            if (response.getStatusCode() != 202)
            {
                Map<String, Object> responseBody = (Map<String, Object>)JSON.deserializeUntyped(response.getBody());
                errors = (List<Object>) responseBody.get('errors');

                for (Object error : errors)
                {
                    messages.add(String.valueOf(error));
                }

                Log.error(this.className, 'command', 'Could not send message. Error Code: {0}, Error Body: {1}', new String []{ String.valueOf(response.getStatusCode()), response.getBody()});

                result.status = ResultStatus.ERROR;
                result.messages = messages;
            }
        }

        return result;
    }

    private ViewModelsMail.Mail overrideEmailForSandbox(ViewModelsMail.Mail mail)
    {
        if (mail.personalizations != null && mail.personalizations.size() > 0)
        {
            ViewModelsMail.Personalization personalization = mail.personalizations[0];
            List<ViewModelsMail.Recipient> toAddresses = personalization.to;
            Integer counter = 1;
            String mailTrap = Label.SMTPOverrideEmail;
            String emailName = mailTrap.substringBefore('@') + '+';

            for (ViewModelsMail.Recipient toEmail: toAddresses)
            {
                toEmail.email = emailName + counter + '@inbox.mailtrap.io';
                counter++;
            }

            emailName = emailName + 'cc';
            counter = 1;

            if (personalization.cc != null)
            {
                for (ViewModelsMail.Recipient ccEmail: personalization.cc)
                {
                    ccEmail.email = emailName + counter + '@inbox.mailtrap.io';
                    counter++;
                }
            }
        }

        return mail;
    }
}