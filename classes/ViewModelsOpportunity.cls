public class ViewModelsOpportunity
{
    public class MoveTermCampaign
    {
        public MoveTermCampaign()
        {
            Campaigns = new List<ViewModels.LightningCode>();
        }

        public Object Options { get; set; }
        public List<ViewModels.LightningCode> Campaigns { get; set; }
    }
}