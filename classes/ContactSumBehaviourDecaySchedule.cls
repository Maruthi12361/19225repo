public class ContactSumBehaviourDecaySchedule implements Schedulable 
{
    public void execute(SchedulableContext sc)
    {
        Database.executebatch(new ContactSumBehaviourDecayBatch(), 50);
    }
}