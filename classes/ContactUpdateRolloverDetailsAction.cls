global class ContactUpdateRolloverDetailsAction 
{
    WebService static String updateRolloverDetails(String contactId)
    {
        ActionResult result = ContactService.updateRolloverDetails(new List<Contact> {new Contact(Id = contactId)});
        
        if (result.isError)
            return result.message;
        
        return 'Rollover details are being refreshed.';
    }
}