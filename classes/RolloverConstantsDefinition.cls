// Contains all the status constants for Rollover process
global class RolloverConstantsDefinition
{
    // Campaign Member Statuses
    global static final String ST_OPEN = 'Open - Not Contacted';
    global static final String ST_MASSEMAIL = 'Mass Email Sent';
    global static final String ST_CONTACTING = 'Contacting';
    global static final String ST_ENROLLING = 'Enrolling';
    global static final String ST_CLOSED_LOST = 'Closed Lost';
    global static final String ST_CLOSED_WON = 'Closed Won';

    // Contact Rollover Status
    global static final String RO_DEFERRED = 'Deferred';
    global static final String RO_NOENROLMENTS = 'No Future Enrollments';
    global static final String RO_ALLSTAGE = 'All Stages Scheduled';

    // Contact Rollover Deferred Reason
    global static final String RO_DF_ACTIVEENT = 'Active Enrollment';
    global static final String RO_DF_TBD = 'To Be Determined';
    global static final String RO_DF_SCDEFERRED = 'Student Central Deferred';
    global static final String RO_DF_UNCONTACT = 'Uncontactable';
    global static final String RO_DF_TABOTHER = 'Taking a Break - Other';
    global static final String RO_DF_NOTDEF = 'Not Deferred';
    global static final String RO_DF_WITHDRAW = 'Withdrawing';

    // Course Status
    global static final String CS_ONGOING = 'Ongoing';

    // CodeFeeHelpElgibilityType
    global static final String FEEHELP_FHE = 'Fee HELP Eligible';
    global static final String FEEHELP_NFHE = 'Non-Fee HELP Eligible';
    global static final String FEEHELP_OFHE = 'Old Fee HELP Eligible';
    global static final String FEEHELP_ONFHE = 'Old Non-Fee HELP Eligible';
    global static final String FEEHELP_NFHEU = 'Non-Fee HELP Eligible - Upfront';
    global static final String FEEHELP_NFHEO = 'Non-Fee HELP Eligible Overseas';
    global static final String FEEHELP_NFHEO_NZ = 'Non-Fee HELP Eligible Overseas – New Zealand';
    global static final String FEEHELP_NFHEO_CL = 'Non-Fee HELP Eligible Overseas - Can Learn';

    // CodeStudentStageStatus
    global static final String STAGE_ENROLLED = 'ENROLLED';
    global static final String STAGE_ATRISK = 'AtRisk';
}