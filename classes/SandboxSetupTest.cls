@isTest
public class SandboxSetupTest
{
    @testSetup
    static void setup()
    {
        TestUtilities.setupHEDA();
    }

    @isTest
    static void SandboxSetup_SetsCampaignsJobsAndCampaignStatusDefaults()
    {
        // Arrange - Use any Ids as orgId and sandboxId for test
        Id myOrgId = Id.valueOf(UserInfo.getOrganizationId());
        Id mySandboxId = Id.valueOf('001xa000003DIlo');
        String mySandboxName = 'MySandboxName';
        SandboxSetup sandboxSetup = new SandboxSetup();

        // Act
        Test.startTest();
        Test.testSandboxPostCopyScript(sandboxSetup, myOrgId, mySandboxId, mySandboxName);
        Test.stopTest();

        // Assert
        System.assertEquals(myOrgId, sandboxSetup.orgId, 'MyOrgId:' + myOrgId + ':' + sandboxSetup.orgId);
        System.assertEquals(mySandboxId, sandboxSetup.sandboxId, 'OrgId:' + mySandboxId + ':' + sandboxSetup.sandboxId);
        System.assertEquals(mySandboxName, sandboxSetup.sandboxName, 'SandboxName:' + mySandboxName + ':' + sandboxSetup.sandboxName);

        List<AAKCS__Campaign_Status_Default__c> rolloverCSD = [SELECT id, Name, AAKCS__Action__c, AAKCS__Active__c FROM AAKCS__Campaign_Status_Default__c WHERE name='Rollover' AND AAKCS__Action__c='Replace All' AND AAKCS__Active__c=true];
        System.assertEquals(1, rolloverCSD.size(), 'Expected a single Rollover AAKCS__Campaign_Status_Default__c but got:' + rolloverCSD.size());
        
        List<AAKCS__Campaign_Status_Default_Value__c> vals = [SELECT id from AAKCS__Campaign_Status_Default_Value__c WHERE AAKCS__Campaign_Status_Default__c = :rolloverCSD.get(0).Id];
        System.assertEquals(6, vals.size(), 'Expected 6 Rollover parent CampaignStatusDefaults, got:' + vals.size());

        List<AAKCS__Campaign_Status_Default__c> acquisitionCSD = [SELECT id, Name, AAKCS__Action__c, AAKCS__Active__c FROM AAKCS__Campaign_Status_Default__c WHERE name='Acquisition' AND AAKCS__Action__c='Replace All' AND AAKCS__Active__c=true];
        System.assertEquals(1, acquisitionCSD.size(), 'Expected a single Acquisition AAKCS__Campaign_Status_Default__c but got:' + acquisitionCSD.size());
 
        vals = [SELECT id from AAKCS__Campaign_Status_Default_Value__c WHERE AAKCS__Campaign_Status_Default__c = :acquisitionCSD.get(0).Id];
        System.assertEquals(3, vals.size(), 'Expected 3 Acquisition parent CampaignStatusDefaults, got:' + vals.size());

        List<Campaign> warmCampaigns = [SELECT Id FROM Campaign WHERE Type = 'Warm'];
        System.assertEquals(5, warmCampaigns.size(), 'Expected 5 Warm Campaigns for use in List Views Filters, got:' + warmCampaigns.size());
    }

    @isTest
    static void SandboxSetup_SetsLabels()
    {
        // Arrange
        SandboxSetup prodSandboxSetup = new SandboxSetup();
        SandboxSetup devSandboxSetup = new SandboxSetup();
        String prodHostURL = 'aib.my.salesforce.com';
        String devHostURL = 'aib--devmarty.cs20.my.salesforce.com';

        // Act
        prodSandboxSetup.setupLabels(prodHostURL);
        devSandboxSetup.setupLabels(devHostURL);

        // Assert
        System.assertEquals('https://aib-app.secure.force.com/', prodSandboxSetup.baseURLLabel);
        System.assertEquals('https://devmarty-aib-app.cs20.force.com/', devSandboxSetup.baseURLLabel);
    }
}