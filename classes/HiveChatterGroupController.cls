/* Controller class used for the ChaterGroupList component used in HIVE to display the public chatter groups */
public class HiveChatterGroupController {
    @AuraEnabled
    public static List<CollaborationGroup> getList() {
        return [SELECT id, name, description, smallPhotoUrl, memberCount FROM CollaborationGroup WHERE networkId <> null and collaborationType = 'Public'];
    }
}