public class TaskSyncCallCountToContactCommand extends BaseTriggerCommand
{
    public TaskSyncCallCountToContactCommand()
    {
        super(5);
    }

    public override List<SObject> validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        List<Task> taskList = new List<Task>();

        for (Task newTask: (List<Task>)items)
        {
            if (triggerContext == Enums.TriggerContext.AFTER_INSERT)
            {
                if (newTask.WhatId != null && newTask.WhoId != null && ((String)newTask.WhatId).startsWith('701') && ((String)newTask.WhoId).startsWith('003'))
                {
                    taskList.add(newTask);
                }
            }
            else if (triggerContext == Enums.TriggerContext.AFTER_UPDATE)
            {
                Task oldTask = (Task) oldMap.get(newTask.Id);
                
                if ((oldTask.WhatId == null || oldTask.WhoId == null) && newTask.WhatId != null && newTask.WhoId != null && ((String)newTask.WhatId).startsWith('701') &&
                    ((String)newTask.WhoId).startsWith('003'))
                {
                    taskList.add(newTask);
                }
            }
        }

        return taskList;
    }

    public override ActionResult command(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        Map<String, Task> taskMap = new Map<String, Task>();
        Set<Id> contactIdSet = new Set<Id>();
        Set<Id> campaignIdSet = new Set<Id>();

        for (Task newTask : (List<Task>) items)
        {
            contactIdSet.add(newTask.WhoId);
            campaignIdSet.add(newTask.WhatId);
            taskMap.put((String)newTask.WhatId + (String)newTask.WhoId, newTask);
        }

        List<Contact> contacts = 
            [
                SELECT Id, Rollover_Status__c, Rollover_Call_Count__c, Rollover_Email_Count__c, Campaign_Contact_ID__c
                FROM Contact
                WHERE RecordType.DeveloperName =: Constants.RECORDTYPE_CONTACT_STUDENT
                AND Id IN : contactIdSet 
                AND Current_Rollover_Campaign__c IN : campaignIdSet
            ];

        for (Contact contact : contacts) 
        {
            Boolean isDirty = false;
            String history;

            Task task = taskMap.get(contact.Campaign_Contact_ID__c);
            if (task == null)
                continue;

            if (task.TaskSubtype == 'Email') 
            {
                contact.Rollover_Email_Count__c = contact.Rollover_Email_Count__c == null ? 1 : contact.Rollover_Email_Count__c + 1;
                history = 'Email ' + contact.Rollover_Email_Count__c + ' - ' + task.LastModifiedDate.format();
                history = StringUtils.concat(history, task.Subject, ' - ');
                contact.Rollover_Contact_History__c = StringUtils.copyLeft(history, 255);
                isDirty = true;
            }
            else if (task.Type == 'Call') 
            {
                contact.Rollover_Call_Count__c = contact.Rollover_Call_Count__c == null ? 1 : contact.Rollover_Call_Count__c + 1;
                history = 'Call ' + contact.Rollover_Call_Count__c + ' - ' + task.LastModifiedDate.format();
                history = StringUtils.concat(history, task.Call_Outcome__c, ' - ');
                history = StringUtils.concat(history, task.Description, ' - ');
                contact.Rollover_Contact_History__c = StringUtils.copyLeft(history, 255);
                isDirty = true;
            }

            if (contact.Rollover_Status__c == RolloverConstantsDefinition.ST_OPEN || contact.Rollover_Status__c == RolloverConstantsDefinition.ST_MASSEMAIL || contact.Rollover_Status__c == RolloverConstantsDefinition.RO_DEFERRED)
            {
                contact.Rollover_Status__c = RolloverConstantsDefinition.ST_CONTACTING;
                isDirty = true;
            }

            if (isDirty) 
            {
                uow.registerDirty(contact);
            }
        }

        return new ActionResult(ResultStatus.SUCCESS);
    }
}