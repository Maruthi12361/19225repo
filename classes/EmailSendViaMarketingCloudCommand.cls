public class EmailSendViaMarketingCloudCommand extends BaseViewModelCommand
{
    public override List<Interfaces.IViewModel> validate(List<Interfaces.IViewModel> params)
    {
        return params;
    }
    
    public override ActionResult command(List<Interfaces.IViewModel> params)
    {        
        for (ViewModelsMail.Email payload: (List<ViewModelsMail.Email>) params)
        {
            if (ConfigUtility.isSandbox() && !Test.isRunningTest())
                payload = overrideEmailForSandbox(payload);

            return MarketingCloudUtils.sendEmail(payload);
        }

        return new ActionResult();
    }
    
    private ViewModelsMail.Email overrideEmailForSandbox(ViewModelsMail.Email payload)
    {
        payload.ToAddress = Label.SMTPOverrideEmail;
        List<ViewModelsMail.Recipient> emails = payload.CcAddress;

        if (emails != null)
        {
            String mailTrap = Label.SMTPOverrideEmail;
            String mailTrapName = mailTrap.substringBefore('@') + '+cc';
            Integer counter = 1;

            for (ViewModelsMail.Recipient ccEmail : emails)
            {
                ccEmail.email = mailTrapname + counter + '@inbox.mailtrap.io';
                counter++;
            }

            payload.CcAddress = emails;
        }

        return payload;
    }
}