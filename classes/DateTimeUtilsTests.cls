@isTest
public class DateTimeUtilsTests
{
    @testSetup
    static void setup()
    {
        TestUtilities.setupHEDA();
    }

    @isTest
    static void GetBusinessHours_ReturnsBusinessHoursInitialisedCorrectly()
    {
        // Act
        BusinessHours bh = DateTimeUtils.getBusinessHours();

        // Assert
        System.assertNotEquals(null, bh); 
        System.assertNotEquals(null, bh.Id);
    }

    @isTest
    static void GetBusinessStartTimes_ReturnsBusinessStartTimesForEachDayInWeek()
    {
        // Arrange
        List<Time> expectStartTimes = new Time[7];
        expectStartTimes[0] = null;                         // Sunday
        expectStartTimes[1] = Time.newInstance(9, 0, 0, 0); // Monday
        expectStartTimes[2] = Time.newInstance(9, 0, 0, 0); // Tuesday
        expectStartTimes[3] = Time.newInstance(9, 0, 0, 0); // Wednesday
        expectStartTimes[4] = Time.newInstance(9, 0, 0, 0); // Thursday
        expectStartTimes[5] = Time.newInstance(9, 0, 0, 0); // Friday
        expectStartTimes[6] = null;                         // Saturday

        // Act
        List<Time> actualStartTimes = DateTimeUtils.getBusinessStartTimes();

        // Assert
        System.assertNotEquals(null, actualStartTimes);
        System.assertEquals(7, actualStartTimes.size());
        for (Integer i = 0; i < 7; i++)
            System.assertEquals(expectStartTimes[i], actualStartTimes[i], 'Incorrect setup for Business Start Time');
    }

    @isTest
    static void GetBusinessEndTimes_ReturnsBusinessEndTimesForEachDayInWeek()
    {
        // Arrange
        List<Time> expectEndTimes = new Time[7];
        expectEndTimes[0] = null;                           // Sunday
        expectEndTimes[1] = Time.newInstance(17, 30, 0, 0); // Monday
        expectEndTimes[2] = Time.newInstance(17, 30, 0, 0); // Tuesday
        expectEndTimes[3] = Time.newInstance(17, 30, 0, 0); // Wednesday
        expectEndTimes[4] = Time.newInstance(17, 30, 0, 0); // Thursday
        expectEndTimes[5] = Time.newInstance(17, 30, 0, 0); // Friday
        expectEndTimes[6] = null;                           // Saturday

        // Act
        List<Time> actualEndTimes = DateTimeUtils.getBusinessEndTimes();

        // Assert
        System.assertNotEquals(null, actualEndTimes);
        System.assertEquals(7, actualEndTimes.size());
        for (Integer i = 0; i < 7; i++)
            System.assertEquals(expectEndTimes[i], actualEndTimes[i], 'Incorrect setup for Business End Times');
    }

    @isTest
    static void GetBusinessDays_ReturnsWhetherBusinessDayForEadDayInWeek()
    {
        // Arrange
        List<Boolean> expectBusinessDays = new Boolean[7];
        expectBusinessDays[0] = false; // Sunday
        expectBusinessDays[1] = true;  // Monday
        expectBusinessDays[2] = true;  // Tuesday
        expectBusinessDays[3] = true;  // Wednesday
        expectBusinessDays[4] = true;  // Thursday
        expectBusinessDays[5] = true;  // Friday
        expectBusinessDays[6] = false; // Saturday

        // Act
        List<Boolean> actualBusinessDays = DateTimeUtils.getBusinessDays();

        // Assert
        System.assertNotEquals(null, actualBusinessDays);
        System.assertEquals(7, actualBusinessDays.size());
        for (Integer i = 0; i < 7; i++)
            System.assertEquals(expectBusinessDays[i], actualBusinessDays[i], 'Incorrect setup for Business Days');
    }

    @isTest
    static void GetElapsedTimeForTwoVariousDates_ReturnDurationInMillisAsLong()
    {
        // Arrange
        long expect = 24 * DateTimeUtils.MILLISECONDS_PER_HOUR;
        Datetime startDate  = Datetime.valueOf('2017-06-07 10:00:00'); 
        Datetime endDate = startDate.addDays(1);
        Datetime nowMinusOneSec = System.now().addSeconds(-1);

        // Act
        long actualOverSingleDay = DateTimeUtils.getElapsedTime(startDate, endDate);
        long actualNullStartTime = DateTimeUtils.getElapsedTime(null, endDate);
        long actualNullEndTime = DateTimeUtils.getElapsedTime(nowMinusOneSec, null);

        // Assert
        System.assertEquals(expect, actualOverSingleDay, 'Incorrect calculation of the Business Hours between two dates 1 day apart');
        System.assertEquals(0, actualNullStartTime, 'Incorrect calculation of the Business Hours between two dates when start time is null');
        System.assert(actualNullEndTime > 0, 'Incorrect calculation of the Business Hours between two dates when end time is null');
    }

    @isTest
    static void GetBusinessElapsedTimeForTwoVariousDates_ReturnDurationInBusinessMillisAsLong()
    {
        // Arrange
        Datetime startDate  = Datetime.valueOf('2017-06-07 10:00:00'); 
        Datetime endDate = startDate.addDays(1);

        // Act
        Decimal actualOverSingleDay = DateTimeUtils.getTimeAsHours(DateTimeUtils.getBusinessElapsedTime(startDate, endDate));
        Decimal actualNullStartTime = DateTimeUtils.getTimeAsHours(DateTimeUtils.getBusinessElapsedTime(null, endDate));
        Decimal actualNullEndTime = DateTimeUtils.getTimeAsHours(DateTimeUtils.getBusinessElapsedTime(startDate, null));

        // Assert
        System.assertEquals(DateTimeUtils.BUSINESS_HOURS_PER_DAY, actualOverSingleDay, 'Incorrect calculation of the Business Hours between two dates 1 day apart');
        System.assertEquals(0, actualNullStartTime, 'Incorrect calculation of the Business Hours between two dates when start time is null');
        System.assert(actualNullEndTime > 0, 'Incorrect calculation of the Business Hours between two dates when end time is null');
    }

    @isTest
    static void GetTimeAsVariousDurations_ReturnMillisConvertedToEquivalentDurations()
    {
        // Arrange
        Decimal expectedSeconds = 36;
        Decimal expectedMinutes = 5; 
        Decimal expectedHours = 5;
        Decimal expectedDays = 2;
        long secondsInMillis = expectedSeconds.longValue() * 1000; 
        long minuteInMillis = expectedMinutes.longValue() * DateTimeUtils.MILLISECONDS_PER_MINUTE;
        long hourInMillis = expectedHours.longValue() * DateTimeUtils.MILLISECONDS_PER_HOUR;
        long daysInMillis = expectedDays.longValue() * DateTimeUtils.MILLISECONDS_PER_DAY;

        // Act
        Decimal actualSeconds = DateTimeUtils.getTimeAs(secondsInMillis, DateTimeUtils.Duration.SECONDS);
        Decimal actualMinutes = DateTimeUtils.getTimeAs(minuteInMillis, DateTimeUtils.Duration.MINUTES);
        Decimal actualHours = DateTimeUtils.getTimeAs(hourInMillis, DateTimeUtils.Duration.HOURS);
        Decimal actualdays = DateTimeUtils.getTimeAs(daysInMillis, DateTimeUtils.Duration.DAYS);
        Decimal actualNull = DateTimeUtils.getTimeAs(minuteInMillis, null);

        // Assert
        System.assertEquals(expectedSeconds, actualSeconds);
        System.assertEquals(expectedMinutes, actualMinutes);
        System.assertEquals(expectedHours, actualHours);
        System.assertEquals(null, actualNull);
    }

    @isTest
    static void GetBusinessWorkingHoursForGivenDay_ReturnsBusinessHoursForDate()
    {
        // Arrange
        Date workingDay = Date.newInstance(2019, 8, 5);    //Monday, working day
        Date nonWorkingDay = Date.newInstance(2019, 8, 4); //Sunday, non-working day
        Decimal expectedWorkingDayHours = DateTimeUtils.BUSINESS_HOURS_PER_DAY;
        Decimal expectedNonWorkingDayHours = 0;

        // Act
        Decimal actualWorkingDay = DateTimeUtils.getBusinessWorkHours(workingDay);
        Decimal actualNonWorkingDay = DateTimeUtils.getBusinessWorkHours(nonWorkingDay);

        // Assert
        System.assertEquals(expectedWorkingDayHours, actualWorkingDay, 'Incorrect calculation of Daily Working Hours for Weekday');
        System.assertEquals(expectedNonWorkingDayHours, actualNonWorkingDay, 'Incorrect calculation of Daily Working Hours for Weekend');
    }

    @isTest
    static void GetWorkingStartTime_ReturnsBusinessDayStartTimeForDate()
    {
        // Arrange
        Time expect = Time.newInstance(9, 0, 0, 0);
        Datetime day = Datetime.newInstanceGmt(2017, 7, 10); // Monday

        // Act
        Time actual = DateTimeUtils.getWorkingStartTime(day.date());

        // Assert
        System.assertEquals(expect, actual, 'Incorrect Business Start Time');
    }

    @isTest
    static void GetWorkingEndTime_ReturnsBusinessDayEndTimeForDate()
    {
        // Arrange
        Time expect = Time.newInstance(17, 30, 0, 0);
        Datetime day = Datetime.newInstance(2017, 7, 10); // Monday

        // Act
        Time actual = DateTimeUtils.getWorkingEndTime(day.date());

        // Assert
        System.assertEquals(expect, actual, 'Incorrect Business End Time');
    }

    @isTest
    static void IsBusinessDayForVariousDays_ReturnsWhetherGivenDateIsBusinesDay()
    {
        // Arrange
        Datetime workDay = Datetime.newInstance(2017, 6, 30);       // 30 Jun 2017 is a workday 
        Datetime weekend = Datetime.newInstance(2019, 8, 4);        // 4 Aug 2019 is a Sunday
        Datetime publicHoliday = Datetime.newInstance(2017, 10, 2); // 2 Oct 2017 is public holiday

        // Act
        Boolean actualWorkDay = DateTimeUtils.isBusinessDay(workDay.date());
        Boolean actualWeekend = DateTimeUtils.isBusinessDay(weekend.date());
        Boolean actualPublicHol = DateTimeUtils.isBusinessDay(publicHoliday.date());

        // Assert
        System.assertEquals(true, actualWorkDay, 'Incorrect verification of a business day');
        System.assertEquals(false, actualWeekend, 'Incorrect verification of a business day');
        System.assertEquals(false, actualPublicHol, 'Incorrect verification of a business day');
    }

    @isTest
    static void TimezoneOffsetForLocalUserAtVariousTimes_CalculatesTimezoneOffsetForGivenDays()
    {
        // Arrange
        TimeZone tz = UserInfo.getTimeZone();
        Datetime preDayLightSaving = Datetime.newInstance(2019, 4, 4);
        Datetime postDaylightSaving = Datetime.newInstance(2019, 4, 8);
        Decimal expectedOffsetInHoursNow = DateTimeUtils.getTimeAsHours(tz.getOffset(System.now()));
        Decimal expectedOffsetInHoursPre = 10.5;
        Decimal expectedOffsetInHoursPost = 9.5;

        // Act
        Decimal actualOffsetNow = DateTimeUtils.getUTCOffsetHours(tz, System.now());
        Decimal actualOffsetNull = DateTimeUtils.getUTCOffsetHours(tz, null);
        Decimal actualOffestPre = DateTimeUtils.getUTCOffsetHours(tz, preDayLightSaving);
        Decimal actualOffestPost = DateTimeUtils.getUTCOffsetHours(tz, postDayLightSaving);

        // Assert
        System.assertEquals(expectedOffsetInHoursNow, actualOffsetNow, 'Incorrect TimeZone Offset for time now');
        System.assertEquals(expectedOffsetInHoursNow, actualOffsetNull, 'Incorrect TimeZone Offset for no time provided should be same as now');
        System.assertEquals(expectedOffsetInHoursPre, actualOffestPre, 'Incorrect TimeZone Offset before daylight saving');
        System.assertEquals(expectedOffsetInHoursPost, actualOffestPost, 'Incorrect TimeZone Offset after daylight saving');
    }
}