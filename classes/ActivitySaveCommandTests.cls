@isTest
public class ActivitySaveCommandTests 
{
    @testSetup 
    static void setup()
    {
        TestUtilities.setupHEDA();
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        TestDataContactMock.createProspect(user, 'Jane', 'Eyre', '', '+61455789698', 'jane.eyre@gmail.com');
    }

    @isTest
    static void NewContact_FollowUpTaskCreated()
    {
        // Arrange
        Id followUp = RecordTypeUtils.getRecordTypeId(Constants.RECORDTYPE_TASK_FOLLOWUP, SObjectType.Task);
        Contact contact = [SELECT Id, OwnerId, AccountId FROM Contact WHERE Email = 'jane.eyre@gmail.com'];

        ViewModelsAcquisition.Activity activity = new ViewModelsAcquisition.Activity();

        String howCanWeHelp = 'Application advice';
        String comments = 'What advice can you provide for MBA course?';
        String description = 'How can we help?\r\n' + howCanWeHelp + '\r\n \r\n' + 'Comments provided: ' + comments;
        activity.IsNewContact = false;
        activity.AssignedTo = contact.OwnerId;
        activity.RelatedTo = contact.AccountId;
        activity.Name = contact.Id;
        activity.TaskType = 'Call';
        activity.TaskRecordType = followUp;
        activity.Priority = 'High';
        activity.Status = 'Open';
        activity.DueDate = System.today().addDays(1);
        activity.Subject = Constants.TASKSUBJECT_FOR_NEW_CONTACT;
        activity.Comments = description;
        activity.NotifyOwner = false;

        // Act
        test.startTest();
        ActionResult result = ActivityService.createActivity(activity);
        Contact updatedContact = [SELECT Id FROM Contact WHERE Email = 'jane.eyre@gmail.com'];
        Task task = [SELECT Id, Subject, Type, Status, ActivityDate, Description FROM Task WHERE WhoId = :updatedContact.Id];
        test.stopTest();

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Unexpected result status value');
        System.assertEquals(Constants.TASKSUBJECT_FOR_NEW_CONTACT, task.Subject, 'Unexpected task subject value');
        System.assertEquals('Call', task.Type, 'Unexpected task type value');
        System.assertEquals('Open', task.Status, 'Unexpected task status value');
        System.assertEquals(System.today().addDays(1), task.ActivityDate, 'Unexpected task activity date');
        System.assertEquals(description, task.Description, 'Unexpected task description');
    }
}