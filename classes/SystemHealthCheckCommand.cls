public class SystemHealthCheckCommand
{
    public ViewModelsSystem.SystemHealthCheck resultInfo = new ViewModelsSystem.SystemHealthCheck();

    public ActionResult execute(List<QueryResult> items)
    {
        QueryResult.QueryResultListWrapper resultWrapper = new QueryResult.QueryResultListWrapper(items);
        resultInfo.FailedChecks = resultWrapper.resultList.size();
        resultInfo.TotalChecks = resultWrapper.totalResultsCount;

        Log.info('SystemHealthCheckCommand', 'execute', 'Failed Checks:' + resultWrapper.resultList.size() + ', Total Checks:' + resultWrapper.totalResultsCount + ', resultWrapper:' + resultWrapper);

        ActionResult result = new ActionResult();
        if (resultInfo.FailedChecks > 0)
            generateAndSendEmail(resultWrapper);

        result.resultInfo = resultInfo;

        return result;
    }

    private void generateAndSendEmail(QueryResult.QueryResultListWrapper wrapper)
    {
        Map<String, String> emailUsers = new Map<String, String>();
        EmailTemplate emailTemplate = EmailService.getEmailTemplate('Environment Health Check');
        Messaging.SingleEmailMessage mail = Messaging.renderStoredEmailTemplate(emailTemplate.Id, null, null);
        String subject = mail.getSubject().replace('{dateAndTime}', System.now().format());
        String body = mail.getHtmlBody();
        body = body.replace('{dateAndTime}', System.now().format());
        body = body.replace('{failedChecks}', String.valueOf(resultInfo.FailedChecks));
        body = body.replace('{totalChecks}', String.valueOf(resultInfo.TotalChecks));
        body = body.replace('{section.detailedFailures}', getFailureSummarySection(wrapper.resultList));

        List<User> users = [SELECT Id, email, Name FROM User WHERE isActive = true AND Id IN (SELECT userOrGroupId FROM groupMember WHERE group.DeveloperName =: Constants.GROUP_HEALTH_CHECK_TEAM)];
        resultInfo.UserEmailsToSend = users.size();

        Log.info('SystemHealthCheckCommand', 'generateAndSendEmail', 'Email being sent - Subject: {0}, to {1} Users in the {2} group.', new List<String>{ subject, String.valueOf(users.size()), Constants.GROUP_HEALTH_CHECK_TEAM });
        for (User usr : users)
            emailUsers.put(usr.Name, String.valueOf(usr.email));

        sendEmail(emailUsers, subject, body);
    }

    @Future(callout=true)
    private static void sendEmail(Map<String, String> emailUsers, String subject, String body)
    {
        String toName = null;
        String toEmail = null;
        List<ViewModelsMail.Recipient> ccEmails = new List<ViewModelsMail.Recipient>();
        for (String name : emailUsers.keySet())
        {
            if (toName == null && toEmail == null)
            {
                toName = name;
                toEmail = emailUsers.get(name);
            }
            else
                ccEmails.add(new ViewModelsMail.Recipient(name, emailUsers.get(name)));
        }

        // SendGrid fails if a CC empty list is sent through so nulling out if no CC attached.
        if (ccEmails.size() == 0)
            ccEmails = null;

        ActionResult result = EmailService.sendToRecipient(Constants.APIPROVIDER_DEFAULT, toEmail, toName, subject, 'software@aib.edu.au', 'Salesforce Admin', null, body, ccEmails);
        if (result.isError)
            Log.error('SystemHealthCheckCommand', 'sendEmail', 'Failed to send SystemHealthCheck email to: {0} at {1}, due to: {2}', new List<String> { String.valueOf(emailUsers.keySet()), String.valueOf(emailUsers.values()), String.valueOf(result.messages) });
    }

    private String getFailureSummarySection(List<QueryResult> results)
    {
        String htmlForSection = '';

        for (QueryResult qresult : results)
            htmlForSection +=
                '<div style="font-size: 14px;"><b><u>' + qresult.queryName + '</u></b></div><br />' +
                '<div style="font-size: 12px;">' + qresult.queryDescription + '</div><br />' +
                '<div style="font-size: 12px;">' + qResult.getAsHTMLTable() + '</div><br />';

        return htmlForSection;
    }
}