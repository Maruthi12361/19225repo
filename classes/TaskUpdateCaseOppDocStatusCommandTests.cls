@isTest
public class TaskUpdateCaseOppDocStatusCommandTests
{
    @testSetup
    static void setup()
    {        
        TestUtilities.setupHEDA();
        TestUtilities.disableComponent('CerebroSyncApplicationCommand');
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        TestDataContactMock.createProspectHot(user);
    }

    @isTest
    static void NewMIRTaskWithM2ReasonOppDocDefaultStatus_OppWorkExperienceDocStatusUpdated()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact contact = [SELECT Id FROM Contact LIMIT 1];
        Opportunity opp = [SELECT Id, Application_Case__c FROM Opportunity LIMIT 1];

        // Act
        Test.startTest();
        TestDataTaskMock.createMIRTask(user, contact.Id, opp.Application_Case__c, TaskUpdateCaseOppDocStatusCommand.MIRReasonM2, 
            TaskUpdateCaseOppDocStatusCommand.MIRReasonM2);
        Test.stopTest();
        Opportunity updatedOpportunity = [SELECT Id, WorkExperienceDocs__c FROM Opportunity WHERE Id = :opp.Id];

        // Assert
        System.assertEquals(Constants.DOCSTATUS_REQUIRED, updatedOpportunity.WorkExperienceDocs__c, 'Unexpected Work experience doc status');
    }

    @isTest
    static void NewMIRTaskWithM2ReasonOppDocNotRequiredStatus_OppWorkExperienceDocStatusUpdated()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact contact = [SELECT Id FROM Contact LIMIT 1];
        Opportunity opp = [SELECT Id, Application_Case__c, WorkExperienceDocs__c FROM Opportunity LIMIT 1];
        opp.WorkExperienceDocs__c = Constants.DOCSTATUS_NOTREQUIRED;
        update opp;

        // Act
        Test.startTest();
        TestDataTaskMock.createMIRTask(user, contact.Id, opp.Application_Case__c, TaskUpdateCaseOppDocStatusCommand.MIRReasonM2, 
            TaskUpdateCaseOppDocStatusCommand.MIRReasonM2);
        Test.stopTest();
        Opportunity updatedOpportunity = [SELECT Id, WorkExperienceDocs__c FROM Opportunity WHERE Id = :opp.Id];

        // Assert
        System.assertEquals(Constants.DOCSTATUS_REQUIRED, updatedOpportunity.WorkExperienceDocs__c, 'Unexpected Work experience doc status');
    }

    @isTest
    static void NewMIRTaskWithM2ReasonOppDocAcceptedStatus_OppWorkExperienceDocStatusUpdated()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact contact = [SELECT Id FROM Contact LIMIT 1];
        Opportunity opp = [SELECT Id, Application_Case__c, WorkExperienceDocs__c FROM Opportunity LIMIT 1];
        opp.WorkExperienceDocs__c = Constants.DOCSTATUS_ACCEPTED;
        update opp;

        // Act
        Test.startTest();
        TestDataTaskMock.createMIRTask(user, contact.Id, opp.Application_Case__c, TaskUpdateCaseOppDocStatusCommand.MIRReasonM2, 
            TaskUpdateCaseOppDocStatusCommand.MIRReasonM2);
        Test.stopTest();
        Opportunity updatedOpportunity = [SELECT Id, WorkExperienceDocs__c FROM Opportunity WHERE Id = :opp.Id];

        // Assert
        System.assertEquals(Constants.DOCSTATUS_REJECTED, updatedOpportunity.WorkExperienceDocs__c, 'Unexpected Work experience doc status');
    }

    @isTest
    static void NewMIRTaskWithM6ReasonOppDocDefaultStatus_OppEnglishProficiencyDocStatusUpdated()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact contact = [SELECT Id FROM Contact LIMIT 1];
        Opportunity opp = [SELECT Id, Application_Case__c FROM Opportunity LIMIT 1];

        // Act
        Test.startTest();
        TestDataTaskMock.createMIRTask(user, contact.Id, opp.Application_Case__c, TaskUpdateCaseOppDocStatusCommand.MIRReasonM6, 
            TaskUpdateCaseOppDocStatusCommand.MIRReasonM6);
        Test.stopTest();
        Opportunity updatedOpportunity = [SELECT Id, EnglishProficiencyDocs__c FROM Opportunity WHERE Id = :opp.Id];

        // Assert
        System.assertEquals(Constants.DOCSTATUS_REQUIRED, updatedOpportunity.EnglishProficiencyDocs__c, 'Unexpected Work experience doc status');
    }

    @isTest
    static void NewMIRTaskWithM6ReasonOppDocNotRequiredStatus_OppEnglishProficiencyDocStatusUpdated()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact contact = [SELECT Id FROM Contact LIMIT 1];
        Opportunity opp = [SELECT Id, Application_Case__c, EnglishProficiencyDocs__c FROM Opportunity LIMIT 1];
        opp.EnglishProficiencyDocs__c = Constants.DOCSTATUS_NOTREQUIRED;
        update opp;

        // Act
        Test.startTest();
        TestDataTaskMock.createMIRTask(user, contact.Id, opp.Application_Case__c, TaskUpdateCaseOppDocStatusCommand.MIRReasonM6, 
            TaskUpdateCaseOppDocStatusCommand.MIRReasonM6);
        Test.stopTest();
        Opportunity updatedOpportunity = [SELECT Id, EnglishProficiencyDocs__c FROM Opportunity WHERE Id = :opp.Id];

        // Assert
        System.assertEquals(Constants.DOCSTATUS_REQUIRED, updatedOpportunity.EnglishProficiencyDocs__c, 'Unexpected Work experience doc status');
    }

    @isTest
    static void NewMIRTaskWithM6ReasonOppDocAcceptedStatus_OppEnglishProficiencyDocStatusUpdated()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact contact = [SELECT Id FROM Contact LIMIT 1];
        Opportunity opp = [SELECT Id, Application_Case__c, EnglishProficiencyDocs__c FROM Opportunity LIMIT 1];
        opp.EnglishProficiencyDocs__c = Constants.DOCSTATUS_ACCEPTED;
        update opp;

        // Act
        Test.startTest();
        TestDataTaskMock.createMIRTask(user, contact.Id, opp.Application_Case__c, TaskUpdateCaseOppDocStatusCommand.MIRReasonM6, 
            TaskUpdateCaseOppDocStatusCommand.MIRReasonM6);
        Test.stopTest();
        Opportunity updatedOpportunity = [SELECT Id, EnglishProficiencyDocs__c FROM Opportunity WHERE Id = :opp.Id];

        // Assert
        System.assertEquals(Constants.DOCSTATUS_REJECTED, updatedOpportunity.EnglishProficiencyDocs__c, 'Unexpected Work experience doc status');
    }

    @isTest
    static void NewMIRTaskWithM9ReasonOppDocNotRequiredStatus_OppTranscriptDocStatusUpdated()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact contact = [SELECT Id FROM Contact LIMIT 1];
        Opportunity opp = [SELECT Id, Application_Case__c, TranscriptDocs__c FROM Opportunity LIMIT 1];
        opp.TranscriptDocs__c = Constants.DOCSTATUS_NOTREQUIRED;
        update opp;

        // Act
        Test.startTest();
        TestDataTaskMock.createMIRTask(user, contact.Id, opp.Application_Case__c, TaskUpdateCaseOppDocStatusCommand.MIRReasonM9, 
            TaskUpdateCaseOppDocStatusCommand.MIRReasonM9);
        Test.stopTest();
        Opportunity updatedOpportunity = [SELECT Id, TranscriptDocs__c FROM Opportunity WHERE Id = :opp.Id];

        // Assert
        System.assertEquals(Constants.DOCSTATUS_REQUIRED, updatedOpportunity.TranscriptDocs__c, 'Unexpected Work experience doc status');
    }

    @isTest
    static void NewMIRTaskWithM9ReasonOppDocAcceptedStatus_OppTranscriptDocStatusUpdated()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact contact = [SELECT Id FROM Contact LIMIT 1];
        Opportunity opp = [SELECT Id, Application_Case__c, TranscriptDocs__c FROM Opportunity LIMIT 1];
        opp.TranscriptDocs__c = Constants.DOCSTATUS_ACCEPTED;
        update opp;

        // Act
        Test.startTest();
        TestDataTaskMock.createMIRTask(user, contact.Id, opp.Application_Case__c, TaskUpdateCaseOppDocStatusCommand.MIRReasonM9, 
            TaskUpdateCaseOppDocStatusCommand.MIRReasonM9);
        Test.stopTest();
        Opportunity updatedOpportunity = [SELECT Id, TranscriptDocs__c FROM Opportunity WHERE Id = :opp.Id];

        // Assert
        System.assertEquals(Constants.DOCSTATUS_REJECTED, updatedOpportunity.TranscriptDocs__c, 'Unexpected Work experience doc status');
    }

    @isTest
    static void NewMIRTaskWithM9ReasonOppDocDefaultStatus_OppTranscriptDocStatusUpdated()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact contact = [SELECT Id FROM Contact LIMIT 1];
        Opportunity opp = [SELECT Id, Application_Case__c FROM Opportunity LIMIT 1];

        // Act
        Test.startTest();
        TestDataTaskMock.createMIRTask(user, contact.Id, opp.Application_Case__c, TaskUpdateCaseOppDocStatusCommand.MIRReasonM9, 
            TaskUpdateCaseOppDocStatusCommand.MIRReasonM9);
        Test.stopTest();
        Opportunity updatedOpportunity = [SELECT Id, TranscriptDocs__c FROM Opportunity WHERE Id = :opp.Id];

        // Assert
        System.assertEquals(Constants.DOCSTATUS_REQUIRED, updatedOpportunity.TranscriptDocs__c, 'Unexpected Work experience doc status');
    }
}