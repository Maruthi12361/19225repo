@isTest
public  class ReferralSendEmailsCommandTest
{
    @testSetup
    static void setup()
    {
        TestUtilities.setupHEDA();
        TestUtilities.disableComponent('ReferralSendEmailsCommand');
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact contact = TestDataContactMock.createStudent(user);
        Contact referrer = TestDataContactMock.createStudent(user);
        Contact referee = TestDataContactMock.createProspectCold(user, '0455566787');
        Referral__c referral = TestDataReferralMock.create(user, referrer.id, referee.Id, true);
    }
        
    @isTest
    static void SendNewReferralEmails_SendsWelcomeAndThankYouEmailsSuccessfully()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        Referral__c referral = [SELECT Id,Referrer_Contact__c, Referee_Contact__c FROM Referral__c LIMIT 1];
       
        // Act
        Test.startTest();
        Integer callCount = Limits.getFutureCalls();
        ActionResult result = ReferralService.sendNewReferralEmails(new List<Referral__c> { referral }, null, Enums.TriggerContext.AFTER_INSERT);
        callCount = Limits.getFutureCalls() - callCount;
        Test.stopTest();
        
        // Assert      
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Unexpected action result status');
        System.assertEquals(2, callCount, 'Unexpected number of emails sent');
    }

    @isTest
    static void SendNewReferralEmailsWithoutRefereeId_InvalidParameterExceptionThrown()
    {     
        // Arrange
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact referrer = [SELECT Id FROM Contact LIMIT 1];
        Referral__c referral = TestDataReferralMock.create(user, referrer.id, null, false);
        String errorMsg = '';
        
        // Act
        try
        {
            ActionResult result = ReferralService.sendNewReferralEmails(new List<Referral__c> { referral }, null, Enums.TriggerContext.AFTER_INSERT);
        }
        catch (Exception ex)
        {
            errorMsg = ex.getMessage();
        }

        // Assert
        System.assertEquals(true, errorMsg.contains('Referrer Contact and Referree Contact field values cannot be null'), 'Unexpected exception message');
    }

    @isTest
    static void SendNewReferralEmailsWithoutReferrerId_InvalidParameterExceptionThrown()
    {     
        // Arrange
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact referee = [SELECT Id FROM Contact LIMIT 1];
        Referral__c referral = TestDataReferralMock.create(user, null, referee.Id, false);
        String errorMsg = '';
        
        // Act
        try
        {
            ActionResult result = ReferralService.sendNewReferralEmails(new List<Referral__c> { referral }, null, Enums.TriggerContext.AFTER_INSERT);
        }
        catch (Exception ex)
        {
            errorMsg = ex.getMessage();
        }

        // Assert
        System.assertEquals(true, errorMsg.contains('Referrer Contact and Referree Contact field values cannot be null'), 'Unexpected exception message');
    }
}