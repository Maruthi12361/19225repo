//This schedule will run daily at 6AM to calculate the elapsed Business Hours since the last AIB interaction with Student via Email
global class CaseLastInteractionSchedule implements Schedulable
{
    String query;

    public CaseLastInteractionSchedule()
    {
        this('SELECT Id, Latest_Interaction_Date_Time__c, ClosedDate, Last_Interaction_Till_Yesterday__c ' +
             'FROM Case ' +
             'WHERE Latest_Interaction_Date_Time__c != null ' +
             '    AND (Status != \'Closed\' OR (Status = \'Closed\' and DAY_ONLY(convertTimezone(ClosedDate)) >= YESTERDAY))');
    }

    public CaseLastInteractionSchedule(String qry)
    {
        this.query = qry;
    }

    global void execute(SchedulableContext sc) 
    {
        Database.executebatch(new CaseLastInteractionBatch(query), 99);
    }
}