@isTest
public class TestDataSkillSetMock
{
    public static SkillSet__c createFaculty(User user, Contact facultyContact, hed__Course__c subjectDefinition)
    {
        SkillSet__c skillSet = new SkillSet__c
        (
            RecordTypeId = RecordTypeUtils.getRecordTypeId(Constants.RECORDTYPE_SKILLSET_FACULTY, SObjectType.SkillSet__c),
            Contact__c = facultyContact.Id,
            SubjectDefinition__c = subjectDefinition.Id,
            Type__c = 'Teaching'
        );

        System.RunAs(user)
        {
            insert skillSet;
        }

        return skillSet;
    }

    public static SkillSet__c createStaff(User user, User staff, String type, String subType)
    {
        return createStaff(user, staff, type, subType, true);
    }

    public static SkillSet__c createStaff(User user, User staff, String type, String subType, boolean saveDB)
    {
        return createStaff(user, staff, type, subType, Constants.SKILLSET_WEIGHTING_ALWAYS, saveDB);
    }

    public static SkillSet__c createStaff(User user, User staff, String type, String subType, String weighting, boolean saveDB)
    {
        SkillSet__c skillSet = new SkillSet__c
        (
            RecordTypeId = RecordTypeUtils.getRecordTypeId(Constants.RECORDTYPE_SKILLSET_STAFF, SObjectType.SkillSet__c),
            User__c = staff.Id,
            User__r = staff,
            Type__c = type,
            SubType__c = subType,
            Weighting__c = weighting
        );

        if (saveDB)
        {
            System.RunAs(user)
            {
                insert skillSet;
            }
        }

        return skillSet;
    }
}