public class ContentDocumentLinkTriggerHandler extends BaseTriggerHandler
{
    public override void beforeInsert()
    {
        ActionResult result;

        result = ContentDocumentService.updateSecurity(Trigger.new, Trigger.oldMap, Enums.TriggerContext.BEFORE_INSERT);
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);
    }
}