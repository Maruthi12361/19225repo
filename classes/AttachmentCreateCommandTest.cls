@isTest 
public class AttachmentCreateCommandTest 
{
    @testSetup 
    public static void setup()
    {
        TestUtilities.setupHEDA();
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact contact = TestDataContactMock.createProspectCold(user, '0455548889');
    }

    @isTest 
    static void BlankParentId_Error() 
    {
        // Arrange 
        String errMsg = '';
        Attachment att = new Attachment();

        // Act
        try 
        {
           ActionResult result = new AttachmentCreateCommand().execute(new List<Attachment>{ att });
        } 
        catch (Exception ex) 
        {
           errMsg = ex.getMessage();
        }
        
        // Assert
        System.assert(errMsg.contains('Attachment parent id parameter cannot be null'), 'Unexpected error message'); 
    }

    @isTest 
    static void BlankName_Error() 
    {
        // Arrange 
        String errMsg = '';
        Contact contact = [SELECT Id FROM Contact Limit 1];
        Attachment att = new Attachment();
        att.ParentId = contact.Id;

        // Act
        try 
        {
           ActionResult result = new AttachmentCreateCommand().execute(new List<Attachment>{ att });
        } 
        catch (Exception ex) 
        {
           errMsg = ex.getMessage();
        }
        
        // Assert
        System.assert(errMsg.contains('Attachment name parameter cannot be null or empty'), 'Unexpected error message'); 
    }

    @isTest 
    static void BlankBody_Error() 
    {
        // Arrange 
        String errMsg = '';
        Contact contact = [SELECT Id FROM Contact Limit 1];
        Attachment att = new Attachment();
        att.ParentId = contact.Id;
        att.Name = 'Test.txt';

        // Act
        try 
        {
           ActionResult result = new AttachmentCreateCommand().execute(new List<Attachment>{ att });
        } 
        catch (Exception ex) 
        {
           errMsg = ex.getMessage();
        }
        
        // Assert
        System.assert(errMsg.contains('Attachment body parameter cannot be null or empty'), 'Unexpected error message'); 
    }

    @isTest 
    static void NewAttachment_CreatedSuccessfully() 
    {
        // Arrange 
        String errMsg = '';
        Contact contact = [SELECT Id FROM Contact Limit 1];
        Attachment att = new Attachment();
        att.ParentId = contact.Id;
        att.Name = 'Test.txt';
        att.Description = 'Test File';
        att.Body = Blob.valueOf('My test file value');

        // Act
        ActionResult result = new AttachmentCreateCommand().execute(new List<Attachment>{ att });
        Attachment createdAtt = [SELECT Id, Name, ParentId, Description FROM Attachment Limit 1];

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Unexpected status value'); 
        System.assertEquals(contact.Id, createdAtt.ParentId, 'Unexpected parent id value'); 
        System.assertEquals('Test.txt', createdAtt.Name, 'Unexpected name value'); 
        System.assertEquals('Test File', createdAtt.Description, 'Unexpected description value'); 
    }
}