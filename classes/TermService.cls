public class TermService
{
    public static ActionResult kickOff(Id termId)
    {
        return new TermKickOffCommand().execute(termId);
    }

    public static ActionResult sendKickOffEmail(List<SObject> items)
    {
        return new TermSendKickOffEmailCommand().execute(items, null, null);
    }

    public static ActionResult updateStatus(List<SObject> items)
    {
        return new TermUpdateStatusCommand().execute(items);
    }
}