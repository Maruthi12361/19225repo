public class SubjectOfferingGetPlanSubjectsQuery
{
    public List<ViewModelsWorkforce.SubjectOfferingListItem> query(Object param)
    {
        return getSubjects((ViewModels.Param) param);
    }

    public List<ViewModelsWorkforce.SubjectOfferingListItem> getSubjects(ViewModels.Param param)
    {
        List<ViewModelsWorkforce.SubjectOfferingListItem> subjectListItems = new List<ViewModelsWorkforce.SubjectOfferingListItem>();
        List<hed__Course_Offering__c> courseOfferings = getSubjectOfferings(param.Identifier);

        for (hed__Course_Offering__c offering : courseOfferings)
        {
            ViewModelsWorkforce.SubjectOfferingListItem item = new ViewModelsWorkforce.SubjectOfferingListItem();

            item.Id = offering.Id;
            item.Code = offering.hed__Course__r.hed__Course_ID__c;
            item.Name = offering.hed__Course__r.Name;
            item.AQFLevel = offering.hed__Course__r.AQFLevel__c;
            item.RecordUrl = '/' + offering.Id;
            item.StudentsTaughtPerOLF = String.valueOf(offering.StudentsTaughtPerOLF__c);
            item.StudentsTaughtPerSC = String.valueOf(offering.StudentsTaughtPerSC__c);
            item.StudentsCoordinatedPerSC = String.valueOf(offering.StudentsCoordinatedPerSC__c);
            item.HeadOfDiscipline = String.format('{0} {1}', new List<String> 
            { 
                offering.hed__Course__r.HeadOfDiscipline__r.FirstName != null ? offering.hed__Course__r.HeadOfDiscipline__r.FirstName : '' , 
                offering.hed__Course__r.HeadOfDiscipline__r.LastName != null ? offering.hed__Course__r.HeadOfDiscipline__r.LastName : ''
            });
            item.TotalSkilledContacts = String.valueOf(offering.hed__Course__r.SubjectDefinition__r.NumberOfSkilledContacts__c != null ? offering.hed__Course__r.SubjectDefinition__r.NumberOfSkilledContacts__c : 0);
            item.IsValid = item.StudentsTaughtPerOLF != null && item.StudentsTaughtPerSC != null && item.StudentsCoordinatedPerSC != null;
            item.ActualStudents = String.valueOf(offering.ActualStudents__c);

            subjectListItems.add(item);
        }

        return subjectListItems;
    }

    private List<hed__Course_Offering__c> getSubjectOfferings(String termId)
    {
        List<hed__Course_Offering__c> offerings =
        [
            SELECT ActualStudents__c, Id, Name, StudentsCoordinatedPerSC__c, StudentsTaughtPerOLF__c, StudentsTaughtPerSC__c, SubjectCode__c,
                hed__Course__c, hed__Course__r.AQFLevel__c, hed__Course__r.HeadOfDiscipline__c, hed__Course__r.HeadOfDiscipline__r.FirstName, 
                hed__Course__r.HeadOfDiscipline__r.LastName, hed__Course__r.hed__Course_ID__c, hed__Course__r.Name, hed__Course__r.SubjectDefinition__c,
                hed__Course__r.SubjectDefinition__r.NumberOfSkilledContacts__c, hed__Term__c
            FROM hed__Course_Offering__c
            WHERE hed__Term__c = :termId AND ActualStudents__c > 0
            ORDER BY hed__Course__r.Name
        ];

        return offerings;
    }
}