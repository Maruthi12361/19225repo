@RestResource(urlMapping='/option/*')
global class OptionController 
{
    @HttpGet
    global static void get() 
    {
        try
        {
            RestRequest req = RestContext.request;
            ViewModels.Param param = new ViewModels.Param();
            Map<String, Object> paramMap = new Map<String, Object>();

            if (req.params.containsKey('list'))
                paramMap.put('list', RestContext.request.params.get('list'));

            if (req.params.containsKey('type'))
                paramMap.put('type', RestContext.request.params.get('type'));

            param.payload = JSON.serialize(paramMap);
  
            RestContext.response.addHeader('Content-Type', 'application/json');
            RestContext.response.responseBody = Blob.valueOf(JSON.serialize(OptionService.getPickListValues(param)));
            RestContext.response.statusCode = 200;
        }
        catch(Exception ex)
        {
            Map<String, String> errorMap = new Map<String, String>
            {
                'Message' => ex.getMessage(),
                'Details' => ex.getStackTraceString() 
            };

            RestContext.response.addHeader('Content-Type', 'application/json');
            RestContext.response.responseBody = Blob.valueOf(JSON.serialize(errorMap));
            RestContext.response.statusCode = 400;
        }
    }
}