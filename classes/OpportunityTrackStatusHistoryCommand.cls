public class OpportunityTrackStatusHistoryCommand extends BaseTriggerCommand
{
    static Map<Id, Status_Tracking_History__c> activeHistoryMap = new Map<Id, Status_Tracking_History__c>();
    static DateTime now = DateTime.now();

    public override List<SObject> validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        List<Opportunity> filteredList;

        if (triggerContext == Enums.TriggerContext.AFTER_INSERT || oldMap == null)
            filteredList = items;
        else if (triggerContext == Enums.TriggerContext.AFTER_UPDATE)
        {
            filteredList = new List<Opportunity>();

            for (Opportunity newValue: (List<Opportunity>)items)
            {
                Opportunity oldValue = (Opportunity) oldMap.get(newValue.Id);
                Status_Tracking_History__c activeHistory = activeHistoryMap.get(newValue.Id);

                if (newValue.StageName != oldValue.StageName)
                {   
                    if (activeHistory != null && activeHistory.Status_Stage__c == newValue.StageName)
                    {
                        Log.info(this.className, 'command', 'Skipping ' + newValue.Id + ' because there exists an active history ' + activeHistory);
                        continue;
                    }

                    filteredList.add(newValue);
                }
                else if (newValue.CampaignId != null && oldValue.CampaignId == null)
                {
                    if (activeHistory != null && activeHistory.Status_Stage__c == newValue.StageName && activeHistory.Campaign__c == null)
                    {
                        filteredList.add(newValue);
                    }
                }
            }
        }

        return filteredList;
    }

    public override ActionResult command(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        List<Status_Tracking_History__c> history;
        Status_Tracking_History__c current;
        Status_Tracking_History__c previous;

        if (triggerContext == Enums.TriggerContext.AFTER_UPDATE)
        {
            history =
            [
                SELECT Id, Type__c, Contact_ID__c, Opportunity_ID__c, Status_Stage__c, StartDateTime__c, EndDateTime__c, Campaign__c
                FROM Status_Tracking_History__c
                WHERE Opportunity_ID__c = :(new Map<Id, SObject>(items).keySet()) AND Type__c = 'State Change'
                    AND (EndDateTime__c = NULL OR (EndDateTime__c <> NULL AND Next_Status_Stage__c = NULL AND Status_Stage__c in : Constants.OPPORTUNITY_ACQUISITION_CLOSEDSTAGES))
                ORDER BY CreatedDate DESC
            ];
        }

        for (Opportunity item: (List<Opportunity>)items)
        {
            if (triggerContext == Enums.TriggerContext.AFTER_UPDATE)
            {
                Opportunity oldValue = (Opportunity) oldMap.get(item.Id);
                Status_Tracking_History__c activeHistory = activeHistoryMap.get(item.Id);

                if (item.CampaignId != null && oldValue.CampaignId == null && item.StageName == oldValue.StageName)
                {
                    activeHistory.Campaign__c = item.CampaignId;
                    uow.registerDirty(activeHistory);
                    Log.info(this.className, 'command', 'Updating Campaign__c of Status Tracking History for new opportunity ' + item.Id);
                    continue;
                }

                // Find the previous history entry for this contact
                for (Status_Tracking_History__c historyItem: history)
                {
                    if (historyItem.Opportunity_ID__c == item.Id)
                    {
                        previous = historyItem;
                        continue;
                    }
                }

                // Don't capture updates where there is no state change
                if (previous != null && previous.Status_Stage__c == item.StageName)
                {
                    Log.info(this.className, 'command', 'Skipping opportunity ' + item.Id + ', Status (' + item.StageName + ') has not changed.');
                    continue;
                }

                // Update previous item
                if (previous != null)
                {
                    Log.info(this.className, 'command', 'Updating previous Status Tracking History end date for opportunity ' + item.Id);
                    previous.EndDateTime__c = now;
                    previous.Next_Status_Stage__c = item.StageName;
                    previous.Next_User__c = UserInfo.getName();

                    if (previous.Campaign__c == null && item.CampaignId != null)
                        previous.Campaign__c = item.CampaignId;

                    if (!String.isBlank(previous.Status_Stage__c) && !String.isBlank(item.StageName))
                    {
                        previous.Direction__c = getDirection(previous.Status_Stage__c, item.StageName);
                    }

                    uow.registerDirty(previous);
                }
            }

            // Create new item
            current = new Status_Tracking_History__c();
            current.OwnerId = item.OwnerId;
            current.Opportunity_ID__c = item.Id;
            current.User__c = UserInfo.getName();
            current.Contact_ID__c = item.Contact_ID__c;
            current.StartDateTime__c = now;
            current.Status_Stage__c = item.StageName;
            current.Type__c = 'State Change';
            current.Department__c = 'Sales';
            current.Campaign__c = item.CampaignId;

            if (item.stageName.contains('Closed'))
                current.EndDateTime__c = now;

            uow.registerNew(current);
            activeHistoryMap.put(current.Opportunity_ID__c, current);
            Log.info(this.className, 'command', 'Creating new Status Tracking History item for opportunity ' + item.Id + ', ' + item.StageName);
        }

        return new ActionResult(ResultStatus.SUCCESS);
    }

    private Integer getStateValue(String state)
    {
        Integer value = 2;

        if (!String.isBlank(state))
        {
            if (state == 'Closed Lost')
                value = 1;
            else if (state == 'Student Central' || state == 'Finance' || state == 'Timetable and Handover')
                value = 2;
            else if (state == 'Applying')
                value = 3;
            else if (state == 'Vetting')
                value = 5;
            else if (state == 'Enrolling')
                value = 6;
            else if (state == 'Commencing')
                value = 7;
            else if (state == 'Closed Won')
                value = 8;
        }

        return value;
    }

    private String getDirection(String oldStatus, String newStatus)
    {
        String result = 'None';
        Integer oldStageValue = getStateValue(oldStatus);
        Integer newStageValue = getStateValue(newStatus);

        if (newStageValue > oldStageValue)
            result = 'Forward';
        else if (newStageValue < oldStageValue)
            result = 'Backward';

        return result;
    }
}