@isTest
public class ProspectReassociateCampaignCommandTests
{
    @testSetup
    static void setup()
    {
        TestUtilities.setupHEDA();
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        TestDataContactMock.createProspectCold(user, '0444444444');
        TestDataCampaignMock.createWarmCampaigns(user);
    }

    @isTest
    static void ProspectWithoutCampaignHistory_CreatesCampaignMember()
    {
        // Arrange
        Contact prospect = 
            [
                SELECT Id, Status__c, PreferredTimeframe__c, PreferredTimeframeLastModified__c
                  FROM Contact
                 LIMIT 1
            ];
        prospect.PreferredTimeframe__c = 'ASAP';
        Contact newProspect = prospect.clone(true, false, true, true);

        newProspect.Status__c = Constants.STATUS_WARM;
        newProspect.recalculateFormulas();

        List<Contact> items = new List<Contact>{ newProspect };
        Map<Id, Contact> oldMap = new Map<Id, Contact>();
        oldMap.put(prospect.Id, prospect);

        // Act
        ActionResult result = ContactService.reassociateWarmCampaign(items, oldMap, Enums.TriggerContext.AFTER_INSERT);
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Executing a command should return a successful result to the caller.');

        // Assert
        prospect = 
            [
                SELECT Id, Status__c, PreferredTimeframeLastModified__c,
                (
                    SELECT Id, Campaign.Name
                      FROM CampaignMembers
                     WHERE Campaign.Type = 'Warm'
                )
                FROM Contact
                WHERE Id = : prospect.Id
            ];
        CampaignMember campaignMember = prospect.CampaignMembers;

        System.assertEquals(1, prospect.CampaignMembers.size(), 'Incorrect CampaignMembers.size');
        System.assertNotEquals(null, prospect.PreferredTimeframeLastModified__c, 'PreferredTimeframeLastModified__c is not correct');
        System.assertEquals('ASAP', campaignMember.Campaign.Name, 'Prospect is incorrectly assigned to campaign');
    }

    @isTest
    static void ProspectWithCampaignHistory_CreatesNewCampaignMemberAfterDeletingOld()
    {
        // Arrange
        Contact prospect = 
            [
                SELECT Id, Status__c, PreferredTimeframe__c, PreferredTimeframeLastModified__c
                  FROM Contact
                 LIMIT 1
            ];
        prospect.PreferredTimeframe__c = 'ASAP';
        prospect.HighestTertiaryLevelDegree__c = 'Bachelor\'s Degree or equivalent';
        prospect.WorkExperienceYears__c = 3;
        prospect.ManagementExperienceYears__c = 4;
        prospect.Status__c = Constants.STATUS_WARM;
        Date preferredTimeframeLastModified = Date.today().addDays(-100);
        prospect.PreferredTimeframeLastModified__c = preferredTimeframeLastModified;
        update prospect;

        Contact newProspect = prospect.clone(true, false, true, true);

        newProspect.PreferredTimeframe__c = null;
        newProspect.recalculateFormulas();

        List<Contact> items = new List<Contact>{ newProspect };
        Map<Id, Contact> oldMap = new Map<Id, Contact>();
        oldMap.put(prospect.Id, prospect);

        // Act
        ActionResult result = ContactService.reassociateWarmCampaign(items, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Executing a command should return a successful result to the caller.');

        // Assert
        prospect = 
            [
                SELECT Id, Status__c, PreferredTimeframeLastModified__c,
                (
                    SELECT Id, Campaign.Name
                      FROM CampaignMembers
                     WHERE Campaign.Type = 'Warm'
                )
                FROM Contact
                WHERE Id = : prospect.Id
            ];
        CampaignMember campaignMember = prospect.CampaignMembers;

        System.assertEquals(1, prospect.CampaignMembers.size(), 'Incorrect CampaignMembers.size');
        System.assertEquals(Date.today(), prospect.PreferredTimeframeLastModified__c, 'PreferredTimeframeLastModified__c is not correct');
        System.assertEquals('Undecided', campaignMember.Campaign.Name, 'Prospect is incorrectly assigned to campaign');
    }

    @isTest
    static void ProspectChangesFromWarmToNotWarm_DeletesExistingCampaignMember()
    {
        // Arrange
        Contact prospect = 
            [
                SELECT Id, LastModifiedDate, Status__c, PreferredTimeframe__c, PreferredTimeframeLastModified__c, HighestTertiaryLevelDegree__c, WorkExperienceYears__c, ManagementExperienceYears__c
                  FROM Contact
                 LIMIT 1
            ];
        prospect.HighestTertiaryLevelDegree__c = 'Bachelor\'s Degree or equivalent';
        prospect.WorkExperienceYears__c = 4;
        prospect.ManagementExperienceYears__c = 4;
        prospect.PreferredTimeframe__c = '6 - 12 Months';
        prospect.Status__c = Constants.STATUS_WARM;
        update prospect;

        Contact newProspect = prospect.clone(true, false, true, true);

        newProspect.Status__c = Constants.STATUS_HOT;
        //newProspect.recalculateFormulas();

        List<Contact> items = new List<Contact>{ newProspect };
        Map<Id, Contact> oldMap = new Map<Id, Contact>();
        oldMap.put(prospect.Id, prospect);

        // Act
        ActionResult result = ContactService.reassociateWarmCampaign(items, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Executing a command should return a successful result to the caller.');

        // Assert
        prospect = 
            [
                SELECT Id, Status__c, PreferredTimeframeLastModified__c,
                (
                    SELECT Id, Campaign.Name
                      FROM CampaignMembers
                     WHERE Campaign.Type = 'Warm'
                )
                FROM Contact
                WHERE Id = : prospect.Id
            ];
        System.assertEquals(0, prospect.CampaignMembers.size(), 'Incorrect CampaignMembers.size');
    }

    @isTest
    static void ProspectChangesPreferredTimeFrame_SavesPreferredTimeFrameModifiedDate()
    {
        // Arrange
        Contact prospect = 
            [
                SELECT Id, Status__c, PreferredTimeframe__c, PreferredTimeframeLastModified__c
                  FROM Contact
                 LIMIT 1
            ];
        prospect.PreferredTimeframe__c = 'ASAP';
        Contact newProspect = prospect.clone(true, false, true, true);

        newProspect.PreferredTimeframe__c = '3 - 6 Months';
        newProspect.PreferredTimeframeLastModified__c = null;
        newProspect.recalculateFormulas();

        List<Contact> items = new List<Contact>{ newProspect };
        Map<Id, Contact> oldMap = new Map<Id, Contact>();
        oldMap.put(prospect.Id, prospect);

        // Act
        ActionResult result = ContactService.reassociateWarmCampaign(items, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Executing a command should return a successful result to the caller.');

        // Assert
        prospect = 
            [
                SELECT Id, Status__c, PreferredTimeframeLastModified__c,
                (
                    SELECT Id, Campaign.Name
                      FROM CampaignMembers
                     WHERE Campaign.Type = 'Warm'
                )
                FROM Contact
                WHERE Id = : prospect.Id
            ];

        System.assertEquals(0, prospect.CampaignMembers.size(), 'Incorrect CampaignMembers.size');
        System.assertEquals(Date.today(), prospect.PreferredTimeframeLastModified__c, 'PreferredTimeframeLastModified__c is not correct');
    }

    @isTest
    static void ProspectBecomesWarm_CreatesCampaignMemberBasedOnPreferredTimeframe()
    {
        // Arrange
        Contact prospect = 
            [
                SELECT Id, Status__c, PreferredTimeframe__c, PreferredTimeframeLastModified__c
                  FROM Contact
                 LIMIT 1
            ];
        prospect.PreferredTimeframe__c = 'Greater than 12 Months';
        update prospect;

        Contact newProspect = prospect.clone(true, false, true, true);
        Date preferredTimeframeLastModified = Date.today().addDays(-30);
        newProspect.Status__c = Constants.STATUS_WARM;
        newProspect.PreferredTimeframeLastModified__c = preferredTimeframeLastModified;
        newProspect.recalculateFormulas();

        List<Contact> items = new List<Contact>{ newProspect };
        Map<Id, Contact> oldMap = new Map<Id, Contact>();
        oldMap.put(prospect.Id, prospect);

        // Act
        ActionResult result = ContactService.reassociateWarmCampaign(items, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Executing a command should return a successful result to the caller.');

        // Assert
        prospect = 
            [
                SELECT Id, Status__c, PreferredTimeframeLastModified__c,
                (
                    SELECT Id, Campaign.Name
                      FROM CampaignMembers
                     WHERE Campaign.Type = 'Warm'
                )
                FROM Contact
                WHERE Id = : prospect.Id
            ];

        CampaignMember campaignMember = prospect.CampaignMembers;

        System.assertEquals(1, prospect.CampaignMembers.size(), 'Incorrect CampaignMembers.size');
        System.assertEquals('6 - 12 Months', campaignMember.Campaign.Name, 'Prospect is incorrectly assigned to campaign');
    }

    @isTest
    static void ProspectWith12MonthsPlusPreferredTimeframe_CreatesCampaignMemberIn12PlusMonthsCampaign()
    {
        // Arrange
        Contact prospect = 
            [
                SELECT Id, Status__c, PreferredTimeframe__c, PreferredTimeframeLastModified__c
                  FROM Contact
                 LIMIT 1
            ];
        prospect.PreferredTimeframe__c = 'ASAP';
        Contact newProspect = prospect.clone(true, false, true, true);

        newProspect.Status__c = Constants.STATUS_WARM;
        newProspect.PreferredTimeframe__c = 'Greater than 12 Months';
        newProspect.recalculateFormulas();

        List<Contact> items = new List<Contact>{ newProspect };
        Map<Id, Contact> oldMap = new Map<Id, Contact>();
        oldMap.put(prospect.Id, prospect);

        // Act
        ActionResult result = ContactService.reassociateWarmCampaign(items, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Executing a command should return a successful result to the caller.');

        // Assert
        prospect = 
            [
                SELECT Id, Status__c, PreferredTimeframeLastModified__c,
                (
                    SELECT Id, Campaign.Name
                      FROM CampaignMembers
                     WHERE Campaign.Type = 'Warm'
                )
                FROM Contact
                WHERE Id = : prospect.Id
            ];
        CampaignMember campaignMember = prospect.CampaignMembers;

        System.assertEquals(1, prospect.CampaignMembers.size(), 'Incorrect CampaignMembers.size');
        System.assertEquals('12+ Months', campaignMember.Campaign.Name, 'Prospect is incorrectly assigned to campaign');
    }
}