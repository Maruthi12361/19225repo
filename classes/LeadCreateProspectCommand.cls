public class LeadCreateProspectCommand extends BaseQueueableCommand 
{
    public LeadCreateProspectCommand()
    {
        super(1);
    }

    public override List<SObject>  validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        return items;
    }

	public override ActionResult command(List<SObject> items, Id jobId)
    {
        for (Lead lead : (List<Lead>) items)
            createProspect(lead);

        return new ActionResult(ResultStatus.Success);
    }

    private void createProspect(Lead lead)
    {
        ViewModels.Param param = new ViewModels.Param();
        Map<String, Object> prospectObjectMap = new Map<String, Object>
        {
            'Cnt_Salutation' => lead.Salutation,
            'Cnt_FirstName' => lead.FirstName,
            'Cnt_MiddleName' => lead.MiddleName,
            'Cnt_LastName' => lead.LastName,
            'Cnt_Email' => lead.Email,
            'Cnt_HomePhone' => lead.Phone,
            'Cnt_MobilePhone' => lead.MobilePhone,
            'Cnt_MailingCountry' => lead.Country
        };

        Log.info(this.className, 'createProspect', 'Creating new Prospect from Lead: {0}', new List<String> { JSON.serialize(prospectObjectMap) });

        param.Identifier = lead.Email;
        param.Payload = JSON.serialize(prospectObjectMap);

        ContactService.saveProspect(param);
    }
}