public class ViewModelsAcquisition
{
    public class Prospect implements Interfaces.IViewModel
    {
        public Details details { get; set; }
        public List<ViewModelsFile.Details> files { get; set; }
    }

    public class Details implements Interfaces.IViewModel
    {
        // Contact fields
        public String Id { get; set; }
        public String Cnt_Id { get; set; }
        public String Opp_Id { get; set; }

        public String Cnt_RecordType { get; set; }
        public String Cnt_Status_c { get; set; }
        public String Cnt_Salutation { get; set; }
        public String Cnt_FirstName { get; set; }
        public String Cnt_MiddleName { get; set; }
        public String Cnt_LastName { get; set; }
        public String Cnt_Email { get; set; }
        public Date Cnt_Birthdate { get; set; }
        public String Cnt_hed_Gender_c { get; set; }
        public Boolean Cnt_HasOptedOutOfEmail { get; set;}
        public String Cnt_LeadSource { get; set; }

        public Integer Cnt_WorkExperienceYears_c { get; set; }
        public Integer Cnt_ManagementExperienceYears_c{ get; set; }
        public String Cnt_Highest_Qualification_c { get; set; }
        public String Cnt_PreferredTimeframe_c { get; set; }
        public String Cnt_PrimaryMotivations_c  { get; set; }
        public String Cnt_ApplicationCourse_c { get; set; }
        public String Cnt_Preferred_Start_Date_c { get; set; }
        public String Cnt_LinkedIn_Profile_c { get; set; }
        public String Cnt_OtherAddressType_c { get; set; }
        public String Cnt_OtherStreet { get; set; }
        public String Cnt_OtherCity { get; set; }
        public String Cnt_OtherPostalCode { get; set; }
        public String Cnt_OtherState { get; set; }
        public String Cnt_OtherCountry { get; set; }
        public String Cnt_MailingStreet { get; set; }
        public String Cnt_MailingCity { get; set; }
        public String Cnt_MailingPostalCode { get; set; }
        public String Cnt_MailingState { get; set; }
        public String Cnt_MailingCountry { get; set; }
        public String Cnt_PermanentAddressType_c { get; set; }
        public String Cnt_Permanent_Home_Address_c { get; set; }
        public String Cnt_Permanent_Home_Suburb_Town_c { get; set; }
        public String Cnt_Permanent_Home_Postcode_c { get; set; }
        public String Cnt_Permanent_Home_State_Province_Region_c { get; set; }
        public String Cnt_Permanent_Home_Country_c { get; set; }
        public String Cnt_Program_c { get; set; }
        public String Cnt_Portfolio_c { get; set; }
        public String Cnt_CustomInformationPack_c { get; set;}

        public String Cnt_hed_Dual_Citizenship_c { get; set; }
        public String Cnt_hed_Citizenship_c { get; set; }
        public String Cnt_hed_Country_of_Origin_c { get; set; }
        public Integer Cnt_YearOfArrivalInAustralia_c { get; set; }
        public String Cnt_Passport_Number_c { get; set; }
        public String Cnt_Visa_Type_c { get; set; }
        public Date Cnt_Visa_Expiry_Date_c { get; set; }

        public String Cnt_Impairments_c { get; set; }
        public String Cnt_Special_Needs_Details_c { get; set; }
        public String Cnt_High_School_Level_c { get; set; }
        public Integer Cnt_YearOfLeavingHighSchool_c { get; set; }
        public String Cnt_High_School_State_Territory_c { get; set; }
        public String Cnt_EnglishLevel_c { get; set; }

        public String Cnt_HighestQualificationName_c { get; set; }
        public String Cnt_HighestQualificationInstitution_c { get; set; }
        public String Cnt_HighestQualificationLanguage_c { get; set; }
        public Integer Cnt_HighestQualificationCompletionYear_c { get; set; }
        public String Cnt_HighestTertiaryLevelDegree_c { get; set; }

        public String Cnt_RefereeOneFullName_c { get; set; }
        public String Cnt_RefereeOneJobTitle_c { get; set; }
        public String Cnt_RefereeOneEmployer_c { get; set; }
        public String Cnt_RefereeOneWorkEmail_c { get; set; }
        public String Cnt_RefereeOneMobile_c { get; set; }
        public String Cnt_RefereeOneMobileCountry_c { get; set; }

        public String Cnt_RefereeTwoFullName_c { get; set; }
        public String Cnt_RefereeTwoJobTitle_c { get; set; }
        public String Cnt_RefereeTwoEmployer_c { get; set; }
        public String Cnt_RefereeTwoWorkEmail_c { get; set; }
        public String Cnt_RefereeTwoMobile_c { get; set; }
        public String Cnt_RefereeTwoMobileCountry_c { get; set; }

        public String Cnt_Aboriginal_or_Torres_Strait_Islander_Des_c { get; set; }
        public Boolean Cnt_Indigenous_Support_Services_c { get; set; }
        public String Cnt_Language_Spoken_at_Home_c { get; set; }
        public String Cnt_CHESSN_Number_c { get; set; }
        public String Cnt_InferredCountry_c { get; set; }
        public String Cnt_InferredCity_c { get; set; }
        public String Cnt_InferredCompany_c { get; set; }
        public String Cnt_InferredMetropolitanArea_c { get; set; }
        public String Cnt_InferredPhoneAreaCode_c { get; set; }
        public String Cnt_InferredPostalCode_c { get; set; }
        public String Cnt_InferredStateRegion_c { get; set; }
        public String Cnt_AustralianVisaHolder_c { get; set; }
        public Boolean Cnt_EmailVerified_c { get; set; }
        public String Cnt_OriginalReferrerSource_c { get; set; }

        public String Cnt_CalculatorAgeRange_c { get; set; }
        public String Cnt_CalculatorRemainingWorkYears_c { get; set; }
        public Integer Cnt_CalculatorSalaryPerAnnum_c { get; set; }
        public Integer Cnt_CalculatorStudyMorning_c { get; set; }
        public Integer Cnt_CalculatorStudyEvening_c { get; set; }
        public Integer Cnt_CalculatorStudyCommuting_c { get; set; }
        public Integer Cnt_CalculatorStudyWork_c { get; set; }
        public Integer Cnt_CalculatorStudyWeekend_c { get; set; }
        public String Cnt_CalculatorPayPeriod_c { get; set; }
        public String Cnt_IPOverride_c { get; set; }

        // Phones
        public string Cnt_hed_PreferredPhone_c { get; set; }

        public String Cnt_MobilePhone { get; set; }
        public String Cnt_MobilePhoneCountry_c { get; set; }
        public String Cnt_HomePhone { get; set; }
        public String Cnt_HomePhoneCountry_c { get; set; }
        public String Cnt_hed_WorkPhone_c { get; set; }
        public String Cnt_WorkPhoneCountry_c { get; set; }
        public String Cnt_OtherPhone { get; set; }
        public String Cnt_OtherPhoneCountry_c { get; set; }

        public String Cnt_SingleSignOnStatus_c { get; set; }
        public Boolean Cnt_SingleSignOnStatusViewed_c { get; set; }
        public Boolean Cnt_MyAIBPortalAccessed_c { get; set; }
        public Boolean Cnt_LearningPortalAccessed_c { get; set; }

        public String Cnt_OwnerID { get; set; }
        public Boolean Cnt_WelcomeProspectEmailSent_c { get; set; }

        // Opportunity fields
        public String Opp_Studying_in_Australia_c { get; set; }
        public String Opp_Primary_Location_During_Studies_c { get; set; }
        public Boolean Opp_IsExpatDeclared_c { get; set; }
        public String Opp_Financial_Support_Source_c { get; set; }
        public String Opp_Invoice_Recipient_Name_c { get; set; }
        public String Opp_Invoice_Recipient_Address_c { get; set; }
        public String Opp_Invoice_Recipient_Telephone_c { get; set; }
        public String Opp_Invoice_Recipient_Fax_c { get; set; }
        public String Opp_Invoice_Recipient_Mobile_c { get; set; }

        public String Opp_FEEHELP_Previous_Lastname_c { get; set; }
        public String Opp_FEEHELP_Previous_Firstname_c { get; set; }
        public String Opp_FEEHELP_Previous_Names_c { get; set; }
        public String Opp_Signature_c { get; set; }
        public String Opp_CreditTransferRequested_c { get; set; }
        public String Opp_Make_RPL_claim_c { get; set; }
        public Boolean Opp_Terms_And_Condition_Agreed_c { get; set; }
        public String Opp_OAF_Agreed_Terms_And_Condition_Version_c { get; set; }
        public Boolean Opp_Course_Info_Read_c { get; set; }
        public String Opp_OAF_Agreed_Course_Information_Version_c { get; set; }

        public String Opp_Last_Year_of_Participation_c { get; set; }
        public String Opp_Parent_Education_Options_c { get; set; }
        public String Opp_Parent_1_Highest_Educational_Attainment_c { get; set; }
        public String Opp_Parent_2_Highest_Educational_Attainment_c { get; set; }
        public String Opp_English_Proficiency_c { get; set; }
        public String Opp_English_Proficiency_Test_c { get; set; }
        public String Opp_English_Proficiency_Details_c { get; set; }
        public String Opp_Special_Needs_Advice_Required_c { get; set; }
        public String Opp_Has_Special_Needs_c { get; set; }

        public String Opp_Consent_Full_Name_c { get; set; }
        public Integer Opp_OAF_Submit_Count_c { get; set; }
        public Boolean Opp_OAF_Submitted_c { get; set; }
        public Boolean Opp_ConfirmedDetails_c { get; set; }
        public Boolean Opp_AuthorisesEnquiries_c { get; set ; }
        public Boolean Opp_RespondFurtherRequests_c { get; set; }

        public String Opp_ProofOfIDDocs_c { get; set; }
        public String Opp_TranscriptDocs_c { get; set; }
        public String Opp_EnglishProficiencyDocs_c { get; set; }
        public String Opp_WorkExperienceDocs_c { get; set; }
        public String Opp_SpecialNeedsDocs_c { get; set; }
        public Boolean Opp_ApplicationReviewed_c { get; set; }

        public String Opp_StudyGoals_c { get; set; }
        public Boolean Opp_RefereesSubmitted_c { get; set; }
        public Boolean Opp_Offer_Documentation_Sent_c { get; set; }
        public Datetime Opp_OfferAcceptedDate_c { get; set; }
        public Boolean Opp_TimetableAccessed_c { get; set; }
        public Boolean Opp_TimetableSent_c { get; set; }
        public Boolean Opp_TimetableAccepted_c { get; set; }
        public Decimal Opp_OAFPercentComplete_c { get; set; }

        public Boolean Opp_Remittance_Received_c { get; set; }
        public Boolean Opp_FEE_HELP_Forms_Submitted_c { get; set; }
        public Boolean Opp_IsLost { get; set; }

        // Case fields
        public String Case_Status { get; set; }
        public Boolean Case_Invoice_Generated_c { get; set; }
        public Boolean Case_Invoice_Required_c { get; set; }
        public Boolean Case_Invoice_Processed_c { get; set; }
        public Boolean Case_FeeHelpDetailsRequired_c { get; set; }
        public Boolean Case_FEE_HELP_Processed_c { get; set; }
        public Boolean Case_Vetting_Complete_c { get; set; }
        public Boolean Case_Timetable_Processed_c { get; set; }
        public Boolean Case_Welcome_Letter_Sent_c { get; set; }
        public Boolean Case_WelcomeLetterViewed_c { get; set; }

        // Owner fields
        public String Owner_FirstName { get; set; }
        public String Owner_LastName { get; set; }
        public String Owner_Email { get; set; }
        public String Owner_Phone { get; set; }
        public String Owner_LinkedInURL_c { get; set; }

        public String IPAddress { get; set; }
        public Date NextIntakeCloseDate { get; set; }
        public String OAFTermsAndConditionsVersion { get; set; }
        public String SendEmail { get; set; }
        public String LeadSourceReferenceId { get; set; }
        public String LeadSourceTests { get; set; }

        // Activity fields
        public String Act_HowCanWeHelp_c { get; set; }
        public String Act_Comments_c { get; set; }
    }

    public class InterestingMomentAlert
    {
        public String MomentType;
        public List<String> MomentSources;
        public String EmailTemplate;
        public Integer MatchMode;
        public Boolean CustomLogic;

        public InterestingMomentAlert(String type, List<String> sources, String template, Integer mode, Boolean custom)
        {
            MomentType = type;
            MomentSources = sources;
            EmailTemplate = template;
            MatchMode = mode;
            CustomLogic = custom;
        }
    }

    public class ContactReferrer implements Interfaces.IViewModel
    {
        public Id ContactID { get; set; }
        public String LeadSource { get; set; }
        public String utm_campaign { get; set; }
        public String utm_content { get; set; }
        public String utm_gclid { get; set; }
        public String utm_keyword { get; set; }
        public String utm_medium { get; set; }
        public String utm_referralSource { get; set; }
        public String utm_source { get; set; }
        public String utm_term { get; set; }
    }

    public class CalculatorEmail implements Interfaces.IViewModel
    {
        public String EmailName { get; set; }
    }

    public class Activity implements Interfaces.IViewModel
    {
        public Boolean IsNewContact { get; set; }
        public Id ContactId { get; set; }
        public Id AssignedTo { get; set; }
        public Id RelatedTo { get; set; }
        public Id Name { get; set; }
        public String TaskType { get; set; }
        public Id TaskRecordType { get; set; }
        public String Priority { get; set; }
        public String Subject { get; set; }
        public String Status { get; set; }
        public Date DueDate { get; set; }
        public String Comments { get; set; }
        public Boolean NotifyOwner { get; set; }
    }
}