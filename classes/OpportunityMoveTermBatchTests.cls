@isTest
public class OpportunityMoveTermBatchTests
{
    @testSetup
    static void setup()
    {
        // Arrange
        TestUtilities.setupHEDA();
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createProspectHot(user);
        TestDataCampaignMock.createSeries(user, 2, false);
        Campaign oldCampaign = TestDataCampaignMock.create(user, Date.valueOf('2018-05-04'), 'Domestic', 'MBA', false);
        TestDataCampaignMock.createAcquisitionCampaignMemberStatus(user);
        TestDataOpportunityMock.create(user, contact, 'Applying', oldCampaign, true);
        TestDataOpportunityMock.create(user, contact, 'Applying', true);
    }

    @isTest
    static void OpportunityPastCloseDate_MovesToNextTerm()
    {
        // Arrange
        TestUtilities.disableComponent('CerebroSyncApplicationCommand');
        Opportunity opportunity =
            [
                SELECT Id, Name, CampaignId, Campaign.TermStartDate__c, CloseDate
                FROM Opportunity
                WHERE CampaignId <> null
                LIMIT 1
            ];

        // Act
        Test.startTest();
        Database.executeBatch(new OpportunityMoveTermBatch('\'' + opportunity.Id + '\''));
        Test.stopTest();

        // Assert
        Campaign campaign =
            [
                SELECT Id, Name, TermStartDate__c
                FROM Campaign
                WHERE TermStartDate__c > TODAY
                AND Portfolio__c = 'Domestic'
                ORDER BY TermStartDate__c
                LIMIT 1
            ];

        opportunity =
            [
                SELECT Id, Name, CampaignId, Campaign.Name, CloseDate, TermMoveCount__c
                FROM Opportunity
                WHERE Id = : opportunity.Id
            ];

        System.assertEquals(1, opportunity.TermMoveCount__c);
        System.assertEquals(campaign.Id, opportunity.CampaignId);
    }

    @isTest
    static void OpportunityWithoutIntendedStartDatePastCloseDate_MovesToNextTerm()
    {
        // Arrange
        TestUtilities.disableComponent('CerebroSyncApplicationCommand');
        Opportunity opportunity =
            [
                SELECT Id, Name, TermMoveCount__c, CloseDate
                FROM Opportunity
                WHERE CampaignId = null
                LIMIT 1
            ];
        opportunity.TermMoveCount__c = 1;
        opportunity.CloseDate = Date.today().addDays(-1);
        update opportunity;

        // Act
        Test.startTest();
        Database.executeBatch(new OpportunityMoveTermBatch('\'' + opportunity.Id + '\''));
        Test.stopTest();

        // Assert
        Campaign campaign =
            [
                SELECT Id, Name, TermStartDate__c
                FROM Campaign
                WHERE TermStartDate__c > TODAY
                AND Portfolio__c = 'Domestic'
                ORDER BY TermStartDate__c
                LIMIT 1
            ];

        opportunity =
            [
                SELECT Id, Name, CampaignId, Campaign.Name, CloseDate, TermMoveCount__c
                FROM Opportunity
                WHERE Id = : opportunity.Id
            ];

        System.assertEquals(2, opportunity.TermMoveCount__c);
        System.assertEquals(campaign.Id, opportunity.CampaignId);
    }

    @isTest
    static void OpportunityPastCloseDateThirdTime_TermMoveAutoClosed()
    {
        // Arrange
        TestUtilities.disableComponent('CerebroSyncApplicationCommand');
        Opportunity opportunity =
            [
                SELECT Id, TermMoveCount__c
                FROM Opportunity
                WHERE CampaignId <> null
                LIMIT 1
            ];
        opportunity.TermMoveCount__c = 2;
        update opportunity;

        // Act
        Test.startTest();
        Database.executeBatch(new OpportunityMoveTermBatch());
        Test.stopTest();

        // Assert
        opportunity =
            [
                SELECT Id, Name, IsClosed, Lost_Reason__c, TermMoveCount__c
                FROM Opportunity
                WHERE Id = : opportunity.Id
            ];

        System.assertEquals(2, opportunity.TermMoveCount__c);
        System.assertEquals(true, opportunity.IsClosed);
        System.assertEquals('Term Move Auto Closed', opportunity.Lost_Reason__c);
    }
}