public class OpportunityGetQuery extends BaseIdQuery
{
    public override List<SObject> query(Set<Id> ids)
    {
        List<Opportunity> items =
        [
            SELECT
                Id,
                Name,
                StageName,
                OwnerId,
                Contact__c,
                Contact__r.Id,
                Contact__r.FirstName,
                Contact__r.LastName,
                CloseDate,
                RecordTypeId,
                RecordType.DeveloperName,

                OAF_Submitted__c,
                OnlineApplicationURL__c,
                Custom_Timetable_Required__c,

                Remittance_Received__c,
                Has_Special_Needs__c,
                Special_Needs_Advice_Required__c,
                Last_Year_of_Participation__c,

                FEE_HELP_Forms_Submitted__c,
                FEEHELP_Previous_Firstname__c,
                FEEHELP_Previous_Lastname__c,
                FEEHELP_Previous_Names__c,

                Parent_1_Highest_Educational_Attainment__c,
                Parent_2_Highest_Educational_Attainment__c

            FROM Opportunity
            WHERE Id = :ids
        ];

        return items;
    }
}