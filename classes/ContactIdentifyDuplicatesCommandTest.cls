@IsTest
public class ContactIdentifyDuplicatesCommandTest
{
    @testSetup
    public static void setup()
    {
        TestUtilities.setupHEDA();
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact contact = TestDataContactMock.createProspectCold(user, 'Cold', 'Prospect', '04045566889', 'cold.prospect@email.com', 'A0015588648');
    }

    @isTest
    static void ContactAllUniqueEmails_NoDuplicatesFound() 
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact contact = TestDataContactMock.createProspectCold(user, 'Cold', 'Prospect Two', '0404778975', 'newcold.prospect@email.com', 'A0015577898');
        contact.PersonalEmail__c = 'newcold.prospectTwo@email.com';
        contact.hed__WorkEmail__c = 'newcold.prospectThree@email.com';
        contact.hed__UniversityEmail__c = 'newcold.prospectFour@email.com';
        contact.hed__AlternateEmail__c = 'newcold.prospectFive@email.com';

        // Act
        test.startTest();
        update contact;
        List<Contact> contactItems = [SELECT Id, DuplicateKey__c FROM Contact WHERE DuplicateKey__c <> ''];
        test.stopTest();

        // Assert
        System.assertEquals(0, contactItems.size(), 'No contact should have duplicate key value');
    }

    @isTest
    static void ContactThirdEmailSameAsOtherContactPrimaryEmail_DuplicateFound()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Id setupContactId = [SELECT Id FROM Contact limit 1].Id;
        Contact contact = TestDataContactMock.createProspectCold(user, 'Cold', 'Prospect Two', '0404441975', 'newcold.prospect@email.com', 'A0015333898');
        contact.hed__WorkEmail__c = 'cold.prospect@email.com';

        Id[] fixedSearchResults= new Id[]{ setupContactId };
        Test.setFixedSearchResults(fixedSearchResults);

        // Act
        test.startTest();
        update contact;
        List<Contact> contactItems = [SELECT Id, DuplicateKey__c FROM Contact WHERE DuplicateKey__c <> ''];
        test.stopTest();

        // Assert
        System.assertEquals(2, contactItems.size(), 'There should be two contacts with Duplicate Key value');
    }

    @isTest
    static void ContactFourthEmailSameAsOtherContactPrimaryEmail_DuplicateFound()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Id setupContactId = [SELECT Id FROM Contact limit 1].Id;
        Contact contact = TestDataContactMock.createProspectCold(user, 'Cold', 'Prospect Two', '0404441975', 'newcold.prospect@email.com', 'A0015333898');
        contact.hed__UniversityEmail__c = 'cold.prospect@email.com';

        Id[] fixedSearchResults= new Id[]{ setupContactId };
        Test.setFixedSearchResults(fixedSearchResults);

        // Act
        test.startTest();
        update contact;
        List<Contact> contactItems = [SELECT Id, DuplicateKey__c FROM Contact WHERE DuplicateKey__c <> ''];
        test.stopTest();

        // Assert
        System.assertEquals(2, contactItems.size(), 'There should be two contacts with Duplicate Key value');
    }
}