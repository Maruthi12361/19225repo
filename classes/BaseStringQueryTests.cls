@isTest
private class BaseStringQueryTests
{
    @testSetup
    static void setup()
    {
        TestUtilities.setupHEDA();
    }

    @isTest
    static void BaseQueryWithStrings_FiltersAndReturnsResultsMatchingStrings()
    {
        // Arrange
        TestBaseStringQuery testQuery = new TestBaseStringQuery();
        List<Case> cases = new List<Case>();

        for (Integer i = 0; i < 10; i++)
        {
            Case newCase = new Case();
            newCase.Subject = (i < 5 ? 'Test' : 'No Test');
            newCase.Description = 'Test BaseStringQuery';

            cases.add(newCase);
        }

        insert cases;

        Set<String> values = new Set<String> { 'Test' };

        // Act
        List<SObject> results = testQuery.query(values);

        // Assert
        System.assertNotEquals(null, results, 'Request should return a valid collection of items');
        System.assertEquals(5, results.size(), 'Request should return only those items identified by the strings being passed through');
    }

    class TestBaseStringQuery extends BaseStringQuery
    {
        public override List<SObject> query(Set<String> values)
        {
            String qry = 'SELECT Id FROM Case WHERE Subject IN :values';
            return Database.query(qry);
        }
    }
}