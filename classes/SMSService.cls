public class SMSService 
{
    public static ActionResult sendToRecipient(String smsProvider, String fromName, String toPhoneNumber, String message) 
    {
        String provider = smsProvider;

        if (provider == Constants.APIPROVIDER_DEFAULT)
            provider = Label.SMSProvider;

        if (provider == Constants.APIPROVIDER_MARKETINGCLOUD )
            return sendViaMarketingCloud(fromName, toPhoneNumber, message);
        else if (provider == Constants.APIPROVIDER_SMSMAGIC)
            return sendViaSMSMagic(fromName, toPhoneNumber, message);

        return sendViaSMSMagic(fromName, toPhoneNumber, message);
    }

    private static ActionResult sendViaMarketingCloud(String fromName, String toPhoneNumber, String message)
    {
        ViewModelsSMS.Details details = new ViewModelsSMS.Details(fromName, toPhoneNumber, message);
        
        return new SMSSendViaMarketingCloudCommand().execute( new List<ViewModelsSMS.Details> { details });
    }

    private static ActionResult sendViaSMSMagic(String fromName, String toPhoneNumber, String message)
    {
        ViewModelsSMS.Details payload = new ViewModelsSMS.Details(fromName, toPhoneNumber, message);

        return new SMSSendViaSMSMagicCommand().execute(new List<ViewModelsSMS.Details> {payload});
    }
}