@isTest
public class ContentVersionUpdateFlagsCommandTests
{
    @testSetup
    public static void setup()
    {
        TestUtilities.setupHEDA();
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createProspectHot(user);
        TestDataOpportunityMock.createApplyingAcquisition(user, contact);
    }

    @isTest
    static void OpportunityTimetableFileUploaded_TimetableSentFlagSetSuccessfully()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        Opportunity opportunity = [SELECT Id FROM Opportunity LIMIT 1];

        // Act
        Test.startTest();
        ContentVersion contentVersion = TestDataContentVersionMock.create(UserService.getByAlias(Constants.USER_SYSTEMADMIN), 'John Smith Timetable.txt', 'John Smith Timetable', opportunity.Id);
        Opportunity updatedOpportunity = [SELECT Id, TimetableSent__c FROM Opportunity WHERE Id = :opportunity.Id];
        Test.stopTest();

        // Assert
        System.assertEquals(true, updatedOpportunity.TimetableSent__c, 'Unexpected Timetable Sent field value');
    }

    @isTest
    static void OpportunityNonTimetableFileUploaded_TimetableSentFlagNotSet()
    {
        // Arrange
        Opportunity opportunity = [SELECT Id FROM Opportunity LIMIT 1];

        // Act
        Test.startTest();
        ContentVersion contentVersion = TestDataContentVersionMock.create(UserService.getByAlias(Constants.USER_SYSTEMADMIN), 'John INV Smith.txt', 'John INV Smith', opportunity.Id);
        Opportunity updatedOpportunity = [SELECT Id, TimetableSent__c FROM Opportunity WHERE Id = :opportunity.Id];
        Test.stopTest();

        // Assert
        System.assertEquals(false, updatedOpportunity.TimetableSent__c, 'Unexpected Timetable Sent field value');
    }

    @isTest
    static void CaseLettOfOfferFileUploaded_OfferGeneratedDateSetSuccessfully()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        Contact contact = [SELECT Id FROM Contact LIMIT 1];
        Case createdCase = TestDataCaseMock.createApplication(UserService.getByAlias(Constants.USER_SYSTEMADMIN), contact);

        // Act
        Test.startTest();
        ContentVersion contentVersion = TestDataContentVersionMock.create(UserService.getByAlias(Constants.USER_SYSTEMADMIN), 'John Smith Letter of Offer.txt', 'John Smith Letter of Offer', createdCase.Id);
        Case updatedCase = [SELECT Id, OfferGeneratedDate__c FROM Case WHERE Id = :createdCase.Id];
        Test.stopTest();

        // Assert
        System.assertNotEquals(null, updatedCase.OfferGeneratedDate__c, 'Unexpected Offer Generated Date field value');
    }

    @isTest
    static void CaseNonLettOfOfferFileUploaded_OfferGeneratedDateNotSet()
    {
        // Arrange
        Contact contact = [SELECT Id FROM Contact LIMIT 1];
        Case createdCase = TestDataCaseMock.createApplication(UserService.getByAlias(Constants.USER_SYSTEMADMIN), contact);

        // Act
        Test.startTest();
        ContentVersion contentVersion = TestDataContentVersionMock.create(UserService.getByAlias(Constants.USER_SYSTEMADMIN), 'John Random Smith.txt', 'John Random Smith', createdCase.Id);
        Case updatedCase = [SELECT Id, OfferGeneratedDate__c FROM Case WHERE Id = :createdCase.Id];
        Test.stopTest();

        // Assert
        System.assertEquals(null, updatedCase.OfferGeneratedDate__c, 'Unexpected Offer Generated Date field value');
    }

    @isTest
    static void CaseInvoiceFileUploaded_InvoiceGeneratedSetSuccessfully()
    {
        // Arrange
        Contact contact = [SELECT Id FROM Contact LIMIT 1];
        Case createdCase = TestDataCaseMock.createApplication(UserService.getByAlias(Constants.USER_SYSTEMADMIN), contact);

        // Act
        Test.startTest();
        ContentVersion contentVersion = TestDataContentVersionMock.create(UserService.getByAlias(Constants.USER_SYSTEMADMIN), 'John INV Smith.txt', 'John INV Smith', createdCase.Id);
        Case updatedCase = [SELECT Id, Invoice_Generated__c FROM Case WHERE Id = :createdCase.Id];
        Test.stopTest();

        // Assert
        System.assertEquals(true, updatedCase.Invoice_Generated__c, 'Unexpected Invoice generated field value');
    }
}