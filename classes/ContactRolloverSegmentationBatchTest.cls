@isTest
public class ContactRolloverSegmentationBatchTest
{
    @testSetup
    static void setup()
    {
        TestUtilities.setupHEDA();
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        TestDataCampaignMock.createSeries(user, 6, false);
        Database.executeBatch(new TermUpdateStatusBatch());
        Date dt = Date.today();

        // Create Contact
        Contact student = TestDataContactMock.createStudent(user, 'A001648585', 'A001648585@study.aib.edu.au', true, false);
        TestDataContactMock.createStudent(user, 'A001648586', 'A001648586@study.aib.edu.au', true, false);

        // Create Enrolments
        Account course = TestDataAccountMock.createCourseMBA(user);
        hed__Program_Enrollment__c courseEnrolment = TestDataCourseEnrolmentMock.create(user, course.Id, student.Id, 'Ongoing');
        TestDataSubjectEnrolmentMock.create(user, courseEnrolment, 'TEST001', 'Test Subject One', dt);
        TestDataSubjectEnrolmentMock.create(user, courseEnrolment, 'TEST002', 'Test Subject Two', dt.addDays(56));
        TestDataSubjectEnrolmentMock.create(user, courseEnrolment, 'TEST003', 'Test Subject Three', dt.addDays(112));
    }

    @isTest
    static void ExecuteBatchWithoutParameter_UpdatesAllStudents()
    {
        // Arrange
        ContactRolloverSegmentationBatch rolloverBatch = new ContactRolloverSegmentationBatch();

        // Act
        Test.startTest();
        Database.executeBatch(rolloverBatch);
        Test.stopTest();

        // Assert
        List<Contact> contacts =
            [
                SELECT Id, Cerebro_Student_ID__c, IsEnroledCurrentTerm__c, IsEnroledNextTerm__c, IsEnroledFutureTerm__c
                FROM Contact
                WHERE Cerebro_Student_ID__c IN ('A001648585', 'A001648586')
            ];

        system.assertEquals(true, contacts[0].IsEnroledCurrentTerm__c);
        system.assertEquals(true, contacts[0].IsEnroledNextTerm__c);
        system.assertEquals(true, contacts[0].IsEnroledFutureTerm__c);
        system.assertEquals(false, contacts[1].IsEnroledCurrentTerm__c);
        system.assertEquals(false, contacts[1].IsEnroledNextTerm__c);
        system.assertEquals(false, contacts[1].IsEnroledFutureTerm__c);
    }

    @isTest
    static void ExecuteBatchWithParameter_UpdatesIdentifiedStudents()
    {
        // Arrange
        ContactRolloverSegmentationBatch rolloverBatch = new ContactRolloverSegmentationBatch('\'A001648585\'');

        // Act
        Test.startTest();
        Database.executeBatch(rolloverBatch);
        Test.stopTest();

        // Assert
        List<Contact> contacts =
            [
                SELECT Id, Cerebro_Student_ID__c, IsEnroledCurrentTerm__c, IsEnroledNextTerm__c, IsEnroledFutureTerm__c
                FROM Contact
                WHERE Cerebro_Student_ID__c = 'A001648585'
            ];

        system.assertEquals(true, contacts[0].IsEnroledCurrentTerm__c);
        system.assertEquals(true, contacts[0].IsEnroledNextTerm__c);
        system.assertEquals(true, contacts[0].IsEnroledFutureTerm__c);
    }
}