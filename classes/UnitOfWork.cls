//
// Unit of Work scope for managing database updates, commits and rollbacks on error.
// Related documentation:
//   https://developer.salesforce.com/page/Apex_Enterprise_Patterns_-_Separation_of_Concerns
//   https://developer.salesforce.com/page/Apex_Enterprise_Patterns_-_Service_Layer
// Maintained at:
//   https://github.com/financialforcedev/df12-apex-enterprise-patterns
//   https://github.com/financialforcedev/fflib-apex-common
//
public virtual class UnitOfWork implements IUnitOfWork
{
    public static UnitOfWork current;
    
    protected List<Schema.SObjectType> sobjTypes = new List<Schema.SObjectType>();

    protected Map<String, List<SObject>> newListByType = new Map<String, List<SObject>>();

    protected Map<String, Map<Id, SObject>> dirtyMapByType = new Map<String, Map<Id, SObject>>();

    protected Map<String, Map<Id, SObject>> deletedMapByType = new Map<String, Map<Id, SObject>>();

    protected Map<String, Relationships> mapRelationships = new Map<String, Relationships>();

    protected List<IDoWork> workList = new List<IDoWork>();

    protected SendEmailWork emailWork = new SendEmailWork();

    protected IDML dml;
    
    public static Schema.SObjectType[] defaultTypes 
    { 
        get
        {
            return new Schema.SObjectType[]
            {
                Account.SObjectType,
                AppLog__c.SObjectType,
                Campaign.SObjectType,
                CampaignMember.SObjectType,
                Case.SObjectType,
                Contact.SObjectType,
                ContentDocument.SObjectType,
                ContentDocumentLink.SObjectType,
                ContentDocumentLink.SObjectType,
                ContentVersion.SObjectType,
                InterestingMoment__c.SObjectType,
                Lead.SObjectType,
                Opportunity.SObjectType,
                Status_Tracking_History__c.SObjectType,
                Task.SObjectType,
                User.SObjectType,
                hed__Affiliation__c.SObjectType,
                hed__Course__c.SObjectType,
                hed__Course_Enrollment__c.SObjectType,
                hed__Course_Offering__c.SObjectType,
                hed__Plan_Requirement__c.SObjectType,
                hed__Term__c.SObjectType
            };
        }
    }
    
    public Integer emailCount
    { 
        get 
        {
            return emailWork.emails.size();
        }
    }
    
    public class SimpleDML implements IDML
    {
        public void dmlInsert(List<SObject> objList)
        {
            insert objList;
        }
        
        public void dmlUpdate(List<SObject> objList)
        {
            update objList;
        }
        
        public void dmlDelete(List<SObject> objList)
        {
            delete objList;
        }
    }

    /**
     * Constructs a new UnitOfWork to support work against the given object list
     *
     * @param sObjectList A list of objects given in dependency order (least dependent first)
     */
    public UnitOfWork()
    {
        this(UnitOfWork.defaultTypes, new SimpleDML());
    }
    
    public UnitOfWork(List<Schema.SObjectType> sObjectTypes)
    {
        this(sObjectTypes, new SimpleDML());
    }

    public UnitOfWork(List<Schema.SObjectType> sObjectTypes, IDML pdml)
    {
        sobjTypes = sObjectTypes.clone();

        for(Schema.SObjectType sObjectType : sobjTypes)
        {
            // register the type
            handleRegisterType(sObjectType);
        }

        mapRelationships.put( Messaging.SingleEmailMessage.class.getName(), new Relationships());

        workList.add(emailWork);

        this.dml = pdml;
    }

    // default implementations for commitWork events
    public virtual void onRegisterType(Schema.SObjectType sObjectType) {}
    public virtual void onCommitWorkStarting() {}
    public virtual void onDMLStarting() {}
    public virtual void onDMLFinished() {}
    public virtual void onDoWorkStarting() {}
    public virtual void onDoWorkFinished() {}
    public virtual void onCommitWorkFinishing() {}
    public virtual void onCommitWorkFinished(Boolean wasSuccessful) {}

    /**
     * Registers the type to be used for DML operations
     *
     * @param sObjectType - The type to register
     *
     */
    private void handleRegisterType(Schema.SObjectType sObjectType)
    {
        // add type to dml operation tracking
        newListByType.put(sObjectType.getDescribe().getName(), new List<SObject>());
        dirtyMapByType.put(sObjectType.getDescribe().getName(), new Map<Id, SObject>());
        deletedMapByType.put(sObjectType.getDescribe().getName(), new Map<Id, SObject>());
        mapRelationships.put(sObjectType.getDescribe().getName(), new Relationships());

        // give derived class opportunity to register the type
        onRegisterType(sObjectType);
    }

    /**
     * Register a generic peace of work to be invoked during the commitWork phase
     **/
    public void registerWork(IDoWork work)
    {
        workList.add(work);
    }

    /**
     * Registers the given email to be sent during the commitWork
     **/
    public void registerEmail(Messaging.Email email)
    {
        emailWork.registerEmail(email);
    }

    /**
     * Register a newly created SObject instance to be inserted when commitWork is called
     *
     * @param record A newly created SObject instance to be inserted during commitWork
     **/
    public void registerNew(SObject record)
    {
        registerNew(record, null, null);
    }

    /**
     * Register a list of newly created SObject instances to be inserted when commitWork is called
     *
     * @param records A list of newly created SObject instances to be inserted during commitWork
     **/
    public void registerNew(List<SObject> records)
    {
        for(SObject record : records)
        {
            registerNew(record, null, null);
        }
    }

    /**
     * Register a newly created SObject instance to be inserted when commitWork is called,
     *   you may also provide a reference to the parent record instance (should also be registered as new separatly)
     *
     * @param record A newly created SObject instance to be inserted during commitWork
     * @param relatedToParentField A SObjectField reference to the child field that associates the child record with its parent
     * @param relatedToParentRecord A SObject instance of the parent record (should also be registered as new separatly)
     **/
    public void registerNew(SObject record, Schema.sObjectField relatedToParentField, SObject relatedToParentRecord)
    {
        if(record.Id != null)
            throw new UnitOfWorkException('Only new records can be registered as new');

        String sObjectType = record.getSObjectType().getDescribe().getName();
        if(!newListByType.containsKey(sObjectType))
            throw new UnitOfWorkException(String.format('SObject type {0} is not supported by this unit of work', new String[] { sObjectType }));

        newListByType.get(sObjectType).add(record);
        if(relatedToParentRecord!=null && relatedToParentField!=null)
            registerRelationship(record, relatedToParentField, relatedToParentRecord);
    }

    /**
     * Register a relationship between two records that have yet to be inserted to the database. This information will be
     *  used during the commitWork phase to make the references only when related records have been inserted to the database.
     *
     * @param record An existing or newly created record
     * @param relatedToField A SObjectField referene to the lookup field that relates the two records together
     * @param relatedTo A SOBject instance (yet to be commited to the database)
     */
    public void registerRelationship(SObject record, Schema.sObjectField relatedToField, SObject relatedTo)
    {
        String sObjectType = record.getSObjectType().getDescribe().getName();
        if(!newListByType.containsKey(sObjectType))
            throw new UnitOfWorkException(String.format('SObject type {0} is not supported by this unit of work', new String[] { sObjectType }));

        mapRelationships.get(sObjectType).add(record, relatedToField, relatedTo);
    }

    /**
     * Registers a relationship between a record and a Messaging.Email where the record has yet to be inserted
     *  to the database.  This information will be
     *  used during the commitWork phase to make the references only when related records have been inserted to the database.
     *
     * @param a single email message instance
     * @param relatedTo A SOBject instance (yet to be commited to the database)
     */
    public void registerRelationship( Messaging.SingleEmailMessage email, SObject relatedTo )
    {
        mapRelationships.get( Messaging.SingleEmailMessage.class.getName() ).add(email, relatedTo);
    }

    /**
     * Register an existing record to be updated during the commitWork method
     *
     * @param record An existing record
     **/
    public void registerDirty(SObject record)
    {
        if(record.Id == null)
            throw new UnitOfWorkException('New records cannot be registered as dirty');

        String sObjectType = record.getSObjectType().getDescribe().getName();
        if(!dirtyMapByType.containsKey(sObjectType))
            throw new UnitOfWorkException(String.format('SObject type {0} is not supported by this unit of work', new String[] { sObjectType }));

        dirtyMapByType.get(sObjectType).put(record.Id, record);
    }

    /**
     * Register an existing record to be updated when commitWork is called,
     *   you may also provide a reference to the parent record instance (should also be registered as new separatly)
     *
     * @param record A newly created SObject instance to be inserted during commitWork
     * @param relatedToParentField A SObjectField reference to the child field that associates the child record with its parent
     * @param relatedToParentRecord A SObject instance of the parent record (should also be registered as new separatly)
     **/
    public void registerDirty(SObject record, Schema.sObjectField relatedToParentField, SObject relatedToParentRecord)
    {
        if(record.Id == null)
            throw new UnitOfWorkException('New records cannot be registered as dirty');

        String sObjectType = record.getSObjectType().getDescribe().getName();
        if(!dirtyMapByType.containsKey(sObjectType))
            throw new UnitOfWorkException(String.format('SObject type {0} is not supported by this unit of work', new String[] { sObjectType }));

        dirtyMapByType.get(sObjectType).put(record.Id, record);
        if(relatedToParentRecord!=null && relatedToParentField!=null)
            registerRelationship(record, relatedToParentField, relatedToParentRecord);
    }

    /**
     * Register a list of existing records to be updated during the commitWork method
     *
     * @param records A list of existing records
     **/
    public void registerDirty(List<SObject> records)
    {
        for(SObject record : records)
        {
            this.registerDirty(record);
        }
    }

    /**
     * Register an existing record to be deleted during the commitWork method
     *
     * @param record An existing record
     **/
    public void registerDeleted(SObject record)
    {
        if(record.Id == null)
            throw new UnitOfWorkException('New records cannot be registered for deletion');

        String sObjectType = record.getSObjectType().getDescribe().getName();
        if(!deletedMapByType.containsKey(sObjectType))
            throw new UnitOfWorkException(String.format('SObject type {0} is not supported by this unit of work', new String[] { sObjectType }));
            
        deletedMapByType.get(sObjectType).put(record.Id, record);
    }

    /**
     * Register a list of existing records to be deleted during the commitWork method
     *
     * @param records A list of existing records
     **/
    public void registerDeleted(List<SObject> records)
    {
        for(SObject record : records)
        {
            this.registerDeleted(record);
        }
    }

    /**
     * Takes all the work that has been registered with the UnitOfWork and commits it to the database
     **/
    public void commitWork()
    {
        current = null;
        
        // notify we're starting the commit work
        onCommitWorkStarting();

        // Wrap the work in its own transaction
        Savepoint sp;
        if(!Test.isRunningTest())
        	sp= Database.setSavePoint();
        
        Boolean wasSuccessful = false;
        try
        {
            // notify we're starting the DML operations
            onDMLStarting();
            // Insert by type
            for(Schema.SObjectType sObjectType : sobjTypes)
            {
                mapRelationships.get(sObjectType.getDescribe().getName()).resolve();
                dml.dmlInsert(newListByType.get(sObjectType.getDescribe().getName()));
            }
            // Update by type
            for(Schema.SObjectType sObjectType : sobjTypes)
                dml.dmlUpdate(dirtyMapByType.get(sObjectType.getDescribe().getName()).values());
            // Delete by type (in reverse dependency order)
            Integer objectIdx = sobjTypes.size() - 1;
            while(objectIdx>=0)
                dml.dmlDelete(deletedMapByType.get(sobjTypes[objectIdx--].getDescribe().getName()).values());

            // manage any record relationships to emails that need to be resolved.
            mapRelationships.get( Messaging.SingleEmailMessage.class.getName() ).resolve();

            // notify we're done with DML
            onDMLFinished();

            // notify we're starting to process registered work
            onDoWorkStarting();
            // Generic work
            for(IDoWork work : workList)
                work.doWork();
            // notify we've completed processing registered work
            onDoWorkFinished();

            // notify we've completed all steps and are in the final stage of completing
            onCommitWorkFinishing();

            // mark tracker to indicate success
            wasSuccessful = true;
        }
        catch (Exception e)
        {
            // Rollback
            if(!Test.isRunningTest())
            	Database.rollback(sp);
            
            // Throw exception on to caller
            throw e;
        }
        finally
        {
            // notify we're done with commit work
            onCommitWorkFinished(wasSuccessful);
        }
    }

    private class Relationships
    {
        private List<IRelationship> mapRelationships = new List<IRelationship>();

        public void resolve()
        {
            // Resolve relationships
            for(IRelationship relationship : mapRelationships)
            {
                //relationship.Record.put(relationship.RelatedToField, relationship.RelatedTo.Id);
                relationship.resolve();
            }

        }

        public void add(SObject record, Schema.sObjectField relatedToField, SObject relatedTo)
        {
            // Relationship to resolve
            Relationship relationship = new Relationship();
            relationship.Record = record;
            relationship.RelatedToField = relatedToField;
            relationship.RelatedTo = relatedTo;
            mapRelationships.add(relationship);
        }

        public void add(Messaging.SingleEmailMessage email, SObject relatedTo)
        {
            EmailRelationship emailRelationship = new EmailRelationship();
            emailRelationship.email = email;
            emailRelationship.relatedTo = relatedTo;
            mapRelationships.add(emailRelationship);
        }
    }

    private interface IRelationship
    {
        void resolve();
    }

    private class Relationship implements IRelationship
    {
        public SObject Record;
        public Schema.sObjectField RelatedToField;
        public SObject RelatedTo;

        public void resolve()
        {
            this.Record.put( this.RelatedToField, this.RelatedTo.Id);
        }
    }

    private class EmailRelationship implements IRelationship
    {
        public Messaging.SingleEmailMessage email;
        public SObject relatedTo;

        public void resolve()
        {
            this.email.setWhatId( this.RelatedTo.Id );
        }
    }

    /**
     * UnitOfWork Exception
     **/
    public class UnitOfWorkException extends Exception {}

    /**
     * Internal implementation of Messaging.sendEmail, see outer class registerEmail method
     **/
    private class SendEmailWork implements IDoWork
    {
        private List<Messaging.Email> emails;
        
        public SendEmailWork()
        {
            this.emails = new List<Messaging.Email>();            
        }

        public void registerEmail(Messaging.Email email)
        {                      
            this.emails.add(email);
        }
        
        public void doWork()
        {
            if(!Test.isRunningTest() && emails.size() > 0) 
                Messaging.sendEmail(emails);

            emails.clear();
        }
    }
}