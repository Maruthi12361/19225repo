public class GroupService
{
    public static Group get(Id id)
    {
        List<Group> items = new GroupGetQuery().query(new Set<Id> { id });
        
        return items.size() == 1 ? items[0] : null;
    }
    
    public static List<Group> get(Set<Id> ids)
    {
        return new GroupGetQuery().query(ids);
    }
}