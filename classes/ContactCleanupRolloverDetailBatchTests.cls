@isTest
public class ContactCleanupRolloverDetailBatchTests
{
    private static string cerebroID = 'A001641336';
    
    @testSetup 
    static void setup() 
    {
        // Arrange
        TestUtilities.setupHEDA();
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        TestDataCampaignMock.create(user, Date.today().addDays(56), 'Domestic', 'MBA', true);
        TestDataCampaignMock.create(user, Date.today().addDays(112), 'Domestic', 'MBA', true);
        TestDataCampaignMock.createRolloverCampaignMemberStatus(user);
        
        Contact contact = TestDataContactMock.createRolloverStudent(user, cerebroID);
        contact.Next_Followup_Date__c = Date.today().addDays(1);
        update contact;
    }
    
    @isTest
    static void StudentWithOpenStatusInCurrentCampaign_SetsCampaignClosedLost() 
    {
        // Arrange
        Contact contact = 
            [
                SELECT Id,Current_Course_Status__c 
                FROM Contact
                WHERE Cerebro_Student_ID__c = : cerebroID
            ];
        contact.Current_Course_Status__c = null;
        update contact;
        
        // Act
        Test.startTest();        
        Database.executeBatch(new ContactCleanupRolloverDetailBatch());
        Test.stopTest();        
        
        // Assert
        contact = 
            [
                SELECT Id,Current_Course_Status__c,Rollover_Status__c,Manual_Followup_Date__c,Current_Rollover_Campaign__c,
                (
                    SELECT Id,Status,CampaignId,Campaign.Type 
                    FROM CampaignMembers 
                    WHERE Campaign.Type = 'Rollover' 
                    ORDER BY CreatedDate DESC
                )
                FROM Contact 
                WHERE Cerebro_Student_ID__c = : cerebroID
            ];
        
        system.assertEquals(null, contact.Current_Course_Status__c, 'Wrong value for Current_Course_Status__c');
        system.assertEquals(null, contact.Rollover_Status__c, 'Wrong value for Rollover_Status__c');
        system.assertEquals(null, contact.Current_Rollover_Campaign__c, 'Wrong value for Current_Rollover_Campaign__c');
        system.assertEquals(1, contact.CampaignMembers.size(), 'Wrong value for CampaignMembers');
    }
    
    @isTest
    static void StudentWithOpenStatusInFutureCampaign_DeletesCampaign()
    {
        // Arrange
        Contact contact = 
            [
                SELECT Id,Current_Course_Status__c,Next_Followup_Date__c,Rollover_Count__c 
                FROM Contact
                WHERE Cerebro_Student_ID__c = : cerebroID
            ];
        contact.Next_Followup_Date__c = Date.today().addDays(56);
        update contact;
        
        contact.Current_Course_Status__c = null;
        contact.Rollover_Count__c = 2;
        update contact;
        
        // Act
        Test.startTest();        
        Database.executeBatch(new ContactCleanupRolloverDetailBatch('\'' + cerebroID + '\''));
        Test.stopTest();        
        
        // Assert
        contact = 
            [
                SELECT Id,Current_Course_Status__c,Rollover_Status__c,Manual_Followup_Date__c,Current_Rollover_Campaign__c,Rollover_Count__c,
                (
                    SELECT Id,Status,CampaignId,Campaign.Type 
                    FROM CampaignMembers 
                    WHERE Campaign.Type = 'Rollover' 
                    ORDER BY CreatedDate DESC
                )
                FROM Contact 
                WHERE Cerebro_Student_ID__c = : cerebroID
            ];
        
        system.assertEquals(null, contact.Current_Course_Status__c, 'Wrong value for Current_Course_Status__c');        
        system.assertEquals(null, contact.Current_Rollover_Campaign__c, 'Wrong value for Current_Rollover_Campaign__c');
        system.assertEquals(1, contact.Rollover_Count__c, 'Wrong value for Rollover_Count__c');
        system.assertEquals(0, contact.CampaignMembers.size(), 'Wrong value for CampaignMembers');
        system.assertEquals(null, contact.Rollover_Status__c, 'Wrong value for Rollover_Status__c');
    }
    
    @isTest 
    static void StudentWithMassEmailStatus_SetsCampaignClosedLost() 
    {
        // Arrange
        Contact contact = 
            [
                SELECT Id,Rollover_Lost_Count__c,Current_Course_Status__c 
                FROM Contact
                WHERE Cerebro_Student_ID__c = : cerebroID
            ];
        contact.Rollover_Lost_Count__c = 0;
        contact.Current_Course_Status__c = null;
        update contact;
        
        CampaignMember campaignMember = 
            [
                SELECT Id, Status 
                FROM CampaignMember 
                WHERE ContactId = :contact.Id
            ];
        campaignMember.Status = RolloverConstantsDefinition.ST_MASSEMAIL;
        update campaignMember;
        
        // Act
        Test.startTest();        
        Database.executeBatch(new ContactCleanupRolloverDetailBatch());
        Test.stopTest();        
        
        // Assert
        contact = 
            [
                SELECT Id,Current_Course_Status__c,Rollover_Status__c,Manual_Followup_Date__c,Current_Rollover_Campaign__c,Rollover_Count__c,Rollover_Lost_Count__c,
                (
                    SELECT Id,Status,CampaignId,Campaign.Type 
                    FROM CampaignMembers 
                    WHERE Campaign.Type = 'Rollover' 
                    ORDER BY CreatedDate DESC
                )
                FROM Contact 
                WHERE Cerebro_Student_ID__c = : cerebroID
            ];
        campaignMember = 
            [
                SELECT Id, Status 
                FROM CampaignMember 
                WHERE ContactId = : contact.Id
            ];
        
        system.assertEquals(null, contact.Current_Course_Status__c, 'Wrong value for Current_Course_Status__c');
        system.assertEquals(null, contact.Rollover_Status__c, 'Wrong value for Rollover_Status__c');
        system.assertEquals(null, contact.Current_Rollover_Campaign__c, 'Wrong value for Current_Rollover_Campaign__c');
        system.assertEquals(1, contact.Rollover_Lost_Count__c, 'Wrong value for Rollover_Lost_Count__c');
        system.assertEquals(1, contact.CampaignMembers.size(), 'Wrong value for CampaignMembers');
        system.assertEquals(RolloverConstantsDefinition.ST_CLOSED_LOST, campaignMember.Status, 'Wrong value for CampaignMember Status');
    }
    
    @isTest 
    static void StudentWithContactingStatus_SetsCampaignClosedLost() 
    {
        // Arrange
        Contact contact = 
            [
                SELECT Id,Rollover_Lost_Count__c,Current_Course_Status__c 
                FROM Contact
                WHERE Cerebro_Student_ID__c = : cerebroID
            ];
        contact.Rollover_Lost_Count__c = 0;
        contact.Current_Course_Status__c = null;
        update contact;
        
        CampaignMember campaignMember = 
            [
                SELECT Id, Status 
                FROM CampaignMember 
                WHERE ContactId = :contact.Id
            ];
        campaignMember.Status = RolloverConstantsDefinition.ST_CONTACTING;
        update campaignMember;
        
        // Act
        Test.startTest();        
        Database.executeBatch(new ContactCleanupRolloverDetailBatch());
        Test.stopTest();        
        
        // Assert
        contact = 
            [
                SELECT Id,Current_Course_Status__c,Rollover_Status__c,Manual_Followup_Date__c,Current_Rollover_Campaign__c,Rollover_Count__c,Rollover_Lost_Count__c,
                (
                    SELECT Id,Status,CampaignId,Campaign.Type 
                    FROM CampaignMembers 
                    WHERE Campaign.Type = 'Rollover' 
                    ORDER BY CreatedDate DESC
                )
                FROM Contact 
                WHERE Cerebro_Student_ID__c = : cerebroID
            ];
        campaignMember = 
            [
                SELECT Id, Status 
                FROM CampaignMember 
                WHERE ContactId = : contact.Id
            ];
        
        system.assertEquals(null, contact.Current_Course_Status__c, 'Wrong value for Current_Course_Status__c');
        system.assertEquals(null, contact.Rollover_Status__c, 'Wrong value for Rollover_Status__c');
        system.assertEquals(null, contact.Current_Rollover_Campaign__c, 'Wrong value for Current_Rollover_Campaign__c');
        system.assertEquals(1, contact.Rollover_Lost_Count__c, 'Wrong value for Rollover_Lost_Count__c');
        system.assertEquals(1, contact.CampaignMembers.size(), 'Wrong value for CampaignMembers');
        system.assertEquals(RolloverConstantsDefinition.ST_CLOSED_LOST, campaignMember.Status, 'Wrong value for CampaignMember Status');         
    }
}