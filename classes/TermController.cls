public class TermController 
{
    @AuraEnabled()
    public static Object kickOff(String termId)
    {
        ActionResult result = TermService.kickOff(termId);

        if (result.isError)
            throw new AuraHandledException(result.message);

        return JSON.serialize(result);
    }
}