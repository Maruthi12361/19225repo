public class ContentDocumentUpdateSecurityCommand extends BaseTriggerCommand
{
    public override List<SObject> validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        List<ContentDocumentLink> result = new List<ContentDocumentLink>();
        List<ContentDocument> documents = getContentDocuments(SObjectUtils.getIDs(items, 'ContentDocumentId'));

        for (ContentDocumentLink cdl: (List<ContentDocumentLink>)items)
        {
            ContentDocument document = getContentDocument(cdl.ContentDocumentId, documents);

            // Since shareType is not editable we only want to update on Insert and only if not already a higher sharing level
            if (triggerContext == Enums.TriggerContext.BEFORE_INSERT && cdl.ShareType == Constants.FILE_CONTENT_SHARETYPE_VIEWER &&
                ((String.valueOf(cdl.LinkedEntityId).startsWith(Constants.ENTITYID_CASE) || 
                    UserInfo.getProfileId() == System.Label.IDProfileAIBStudentPortalLoginUser || 
                    UserInfo.getProfileId() == System.Label.IDProfileAIBStudentPortalUser) ||
                    (document != null && String.valueOf(cdl.LinkedEntityId).startsWith(Constants.ENTITYID_OPPORTUNITY) &&
                        document.LatestPublishedVersion.Type__c == 'Timetable')))
            {
                result.add(cdl);
            }
        }

        return result;
    }

    public override ActionResult command(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        for (ContentDocumentLink cdl: (List<ContentDocumentLink>)items)
        {
            cdl.ShareType = Constants.FILE_CONTENT_SHARETYPE_INFERRED;
            cdl.Visibility = Constants.FILE_CONTENT_VISIBILITY_ALL;
        }
        return new ActionResult(ResultStatus.SUCCESS);
    }

    private List<ContentDocument> getContentDocuments(Set<Id> ids)
    {
        return
        [
            SELECT Id, LatestPublishedVersionId, LatestPublishedVersion.Type__c
            FROM ContentDocument
            WHERE Id = :ids
        ];
    }

    private ContentDocument getContentDocument(Id id, List<ContentDocument> contentDocuments)
    {
        for (ContentDocument doc : contentDocuments)
            if (doc.Id == id)
                return doc;
        
        return null;
    }
}