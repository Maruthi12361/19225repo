public class CaseUpdateAwardAddressCommand extends BaseTriggerCommand
{
    public override List<SObject> validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        List<Case> result = new List<Case>();
        Id recordTypeId = RecordTypeUtils.getRecordTypeId(Constants.RECORDTYPE_CASE_AWARD, SObjectType.Case);

        for (Case newCase: (List<Case>)items)
        {
            if (newCase.RecordTypeId == recordTypeId)
            {
                if (triggerContext == Enums.TriggerContext.BEFORE_UPDATE)
                {
                    Case oldCase = (Case) oldMap.get(newCase.Id);

                    if (newCase.Teaching_Centre__c != null &&
                        (newCase.Teaching_Centre__c != oldCase.Teaching_Centre__c ||
                         ((newCase.Teaching_Centre__c == Constants.AIB_DL || newCase.Teaching_Centre__c == Constants.AIB_LOCAL) && newCase.ContactId != oldCase.ContactId))
                       )
                        result.add(newCase);
                }
                else if (triggerContext == Enums.TriggerContext.BEFORE_INSERT)
                {
                    if (newCase.Teaching_Centre__c != null)
                        result.add(newCase);
                }
            }
        }

        return result;
    }

    public override ActionResult command(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        Set<Id> contactIds = new Set<Id>();
        Set<String> tcNames = new Set<String>();

        for (Case c : (List<Case>)items)
        {
            if (c.Teaching_Centre__c == Constants.AIB_DL || c.Teaching_Centre__c == Constants.AIB_LOCAL)
            {
                if (c.ContactId != null)
                    contactIds.add(c.ContactId);
            }
            else
                tcNames.add(c.Teaching_Centre__c);
        }

        Map<Id, Contact> contactMap = new Map<Id, Contact>();

        if (contactIds.size() > 0)
        {
            contactMap.putAll(
                [
                    SELECT Id, MailingStreet, MailingCity, MailingState, MailingCountry, MailingPostalCode
                    FROM Contact
                    WHERE Id IN : contactIds
                ]);
        }

        Map<String, Account> tcMap = new Map<String, Account>();

        if (tcNames.size() > 0)
        {
            List<Account> tcs =
                [
                    SELECT Id, Name, BillingStreet, BillingCity, BillingState, BillingCountry, BillingPostalCode
                    FROM Account
                    WHERE Name IN : tcNames AND RecordType.DeveloperName = 'Educational_Institution'
                ];

            for (Account tc : tcs)
                tcMap.put(tc.Name, tc);
        }

        for (Case c : (List<Case>)items)
        {
            if (c.Teaching_Centre__c == Constants.AIB_DL || c.Teaching_Centre__c == Constants.AIB_LOCAL)
            {
                Contact contact = contactMap.get(c.ContactId);

                if (contact != null)
                {
                    c.AwardAddressStreet__c = (contact.MailingStreet != null ? contact.MailingStreet: '');
                    c.AwardAddressSuburb__c = (contact.MailingCity != null ? contact.MailingCity : '');
                    c.AwardAddressState__c = (contact.MailingState != null ? contact.MailingState : '');
                    c.AwardAddressPostcode__c = (contact.MailingPostalCode != null ? contact.MailingPostalCode  : '');
                    c.AwardAddressCountry__c = (contact.MailingCountry != null ? contact.MailingCountry : '');
                }
            }
            else
            {
                Account tc = tcMap.get(c.Teaching_Centre__c);

                if (tc != null)
                {
                    c.AwardAddressStreet__c = (tc.BillingStreet != null ? tc.BillingStreet: '');
                    c.AwardAddressSuburb__c = (tc.BillingCity != null ? tc.BillingCity : '');
                    c.AwardAddressState__c = (tc.BillingState != null ? tc.BillingState : '');
                    c.AwardAddressPostcode__c = (tc.BillingPostalCode != null ? tc.BillingPostalCode  : '');
                    c.AwardAddressCountry__c = (tc.BillingCountry != null ? tc.BillingCountry : '');
                }
            }
        }

        return new ActionResult(ResultStatus.SUCCESS);
    }
}