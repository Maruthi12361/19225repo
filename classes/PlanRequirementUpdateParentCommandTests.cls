@isTest
public class PlanRequirementUpdateParentCommandTests
{
    @testSetup
    static void setup()
    {
        TestUtilities.setupHEDA();
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        TestDataPlanRequirementMock.createCoursePlanRequirement(user, 'Core', null);
        TestDataPlanRequirementMock.createCoursePlanRequirement(user, 'Electives', null);
    }

    @isTest
    static void InsertDependencyItem_IncreasesNumberOfSubject()
    {
        // Arrange
        Map<Id, hed__Plan_Requirement__c> oldMap = new Map<Id, hed__Plan_Requirement__c>();
        List<hed__Plan_Requirement__c> items = new List<hed__Plan_Requirement__c>();
        List<hed__Plan_Requirement__c> requirements =
            [
                SELECT Id, NumberOfSubjects__c
                FROM hed__Plan_Requirement__c
                WHERE Name = 'Core'
            ];
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        hed__Course__c subject = TestDataSubjectMock.createDefinitionLEAD(user);
        hed__Plan_Requirement__c item = TestDataPlanRequirementMock.createDependencyItem(user, 'Leadership', requirements[0].Id, subject.Id);

        items.add(item);

        // Act
        Test.startTest();
        ActionResult result = PlanRequirementService.updateParent(items, oldMap, Enums.TriggerContext.AFTER_INSERT);
        Test.stopTest();

        // Assert
        List<hed__Plan_Requirement__c> updatedRequirements =
            [
                SELECT Id, NumberOfSubjects__c
                FROM hed__Plan_Requirement__c
                WHERE Name = 'Core'
            ];
        System.assertEquals(ResultStatus.SUCCESS, result.Status);
        System.assertEquals(requirements[0].NumberOfSubjects__c + 1, updatedRequirements[0].NumberOfSubjects__c);
    }

    @isTest
    static void MoveDependencyItem_AdjustsNumberOfSubjectInOldAndNewParents()
    {
        // Arrange
        List<hed__Plan_Requirement__c> items = new List<hed__Plan_Requirement__c>();
        List<hed__Plan_Requirement__c> requirements =
            [
                SELECT Id, NumberOfSubjects__c
                FROM hed__Plan_Requirement__c
                ORDER BY NAME
            ];
        requirements[0].NumberOfSubjects__c = 1;
        update requirements[0];
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        hed__Course__c subject = TestDataSubjectMock.createDefinitionLEAD(user);
        hed__Plan_Requirement__c item = TestDataPlanRequirementMock.createDependencyItem(user, 'Marketing Management', requirements[0].Id, subject.Id);
        Map<Id, hed__Plan_Requirement__c> oldMap = new Map<Id, hed__Plan_Requirement__c>{ item.Id => item };
        hed__Plan_Requirement__c newItem = item.clone(true, false, true, true);
        newItem.hed__Plan_Requirement__c = requirements[1].Id;
        items.add(newItem);
        System.RunAs(user)
        {
            update newItem;
        }

        // Act
        Test.startTest();
        ActionResult result = PlanRequirementService.updateParent(items, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        Test.stopTest();

        // Assert
        List<hed__Plan_Requirement__c> updatedRequirements =
            [
                SELECT Id, NumberOfSubjects__c
                FROM hed__Plan_Requirement__c
                ORDER BY NAME
            ];
        System.assertEquals(ResultStatus.SUCCESS, result.Status);
        System.assertEquals(requirements[0].NumberOfSubjects__c - 1, updatedRequirements[0].NumberOfSubjects__c);
        System.assertEquals(requirements[1].NumberOfSubjects__c + 1, updatedRequirements[1].NumberOfSubjects__c);
    }

    @isTest
    static void DeleteDependencyItem_DecreasesNumberOfSubject()
    {
        // Arrange
        Map<Id, hed__Plan_Requirement__c> oldMap = new Map<Id, hed__Plan_Requirement__c>();
        List<hed__Plan_Requirement__c> items = new List<hed__Plan_Requirement__c>();
        List<hed__Plan_Requirement__c> requirements =
            [
                SELECT Id, NumberOfSubjects__c
                FROM hed__Plan_Requirement__c
                WHERE Name = 'Core'
            ];
        requirements[0].NumberOfSubjects__c = 1;
        update requirements[0];
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        hed__Course__c subject = TestDataSubjectMock.createDefinitionLEAD(user);
        hed__Plan_Requirement__c item = TestDataPlanRequirementMock.createDependencyItem(user, 'Leadership', requirements[0].Id, subject.Id);
        items.add(item);
        System.RunAs(user)
        {
            delete item;
        }

        // Act
        Test.startTest();
        ActionResult result = PlanRequirementService.updateParent(items, oldMap, Enums.TriggerContext.AFTER_DELETE);
        Test.stopTest();

        // Assert
        List<hed__Plan_Requirement__c> updatedRequirements =
            [
                SELECT Id, NumberOfSubjects__c
                FROM hed__Plan_Requirement__c
                WHERE Name = 'Core'
            ];
        System.assertEquals(ResultStatus.SUCCESS, result.Status);
        System.assertEquals(requirements[0].NumberOfSubjects__c - 1, updatedRequirements[0].NumberOfSubjects__c);
    }
}