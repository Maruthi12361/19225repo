public class ConfigUtility
{
    private static String CurrentSFEnvironment = null;
    private static Boolean isSandbox = null;

    public static String getCurrentEnvironment()
    {
        if (CurrentSFEnvironment == null)
            CurrentSFEnvironment = (isSandbox()) ? UserInfo.getUserName().substringAfterLast('.') : 'production';

        return CurrentSFEnvironment;
    }

    public static Boolean isSandbox()
    {
        if (isSandbox == null)
            isSandbox = [SELECT IsSandbox FROM Organization LIMIT 1].IsSandbox;

        return isSandbox;
    }
}