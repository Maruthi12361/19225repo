public class ContactBackfillMobileLocaleBatch extends BaseBatchCommand
{
    private String query = 
        'SELECT ' +
        	'Id,MobileLocale__c,MobilePhoneCountry__c,InferredCountry__c,MailingCountry,Permanent_Home_Country__c ' +
        'FROM Contact ' +
        'WHERE MobileLocale__c = null AND (MobilePhoneCountry__c != null OR InferredCountry__c != null OR MailingCountry != null OR Permanent_Home_Country__c != null)';
    
    private String contactIds;
    
    public ContactBackfillMobileLocaleBatch()
    {
    }
    
    public ContactBackfillMobileLocaleBatch(String stringParam)
    {
        if (stringParam != null && String.isNotBlank(stringParam))
            contactIds = stringParam;
    }
    
    public override Database.QueryLocator query(Database.BatchableContext context)
    {
        if (contactIds != null)
        {
            query += ' AND Id IN (' + contactIds + ')';
        }

        return Database.getQueryLocator(query);
    }
    
    public override void command(Database.BatchableContext context, List<sObject> scope)
    {
        List<Contact> updates = new List<Contact>();

        for (Contact contact : (List<Contact>) scope)
        {
            String code = PhoneNumberUtilities.getCountryCode(contact.MobilePhoneCountry__c);
            
            if (String.isEmpty(code))
                code = PhoneNumberUtilities.getCountryCode(contact.InferredCountry__c);
            
            if (String.isEmpty(code))
                code = PhoneNumberUtilities.getCountryCode(contact.MailingCountry);

            if (String.isEmpty(code))
                code = PhoneNumberUtilities.getCountryCode(contact.Permanent_Home_Country__c);

            if (!String.isEmpty(code))
            {
                contact.MobileLocale__c = code;
                updates.add(contact);
            }
        }
        
        if (updates.size() > 0)
            update updates;
    }
}