@isTest
public class InterestingMomentAlertCACommandTests
{
    @testSetup
    static void setup()
    {
        TestUtilities.setupHEDA();
    }

    @isTest
    static void EnrolESSNow_SendsCAAlert()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createProspectCold(user, '0455566787');
        InterestingMoment__c moment = TestDataInterestingMomentMock.create(user, contact, 'Email Link Clicked', '/ess-thanks-enrol/', 25);
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());

        // Act
        Test.startTest();
        Integer callCount = Limits.getFutureCalls();
        ActionResult result = InterestingMomentService.sendCAAlert(new List<InterestingMoment__c> { moment }, new Map<Id, InterestingMoment__c>(), Enums.TriggerContext.AFTER_INSERT);
        callCount = Limits.getFutureCalls() - callCount;
        Test.stopTest();

        // Assert
        System.assertEquals(true, result.isSuccess);
        System.assertEquals(1, callCount);
    }

    @isTest
    static void InterestedESS_SendsCAAlert()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createProspectCold(user, '0455566787');
        InterestingMoment__c moment = TestDataInterestingMomentMock.create(user, contact, 'Email Link Clicked', '/ess-thanks/', 25);
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());

        // Act
        Test.startTest();
        Integer callCount = Limits.getFutureCalls();
        ActionResult result = InterestingMomentService.sendCAAlert(new List<InterestingMoment__c> { moment }, new Map<Id, InterestingMoment__c>(), Enums.TriggerContext.AFTER_INSERT);
        callCount = Limits.getFutureCalls() - callCount;
        Test.stopTest();

        // Assert
        System.assertEquals(true, result.isSuccess);
        System.assertEquals(1, callCount);
    }
    
    @isTest
    static void InterestedPortalTour_SendsCAAlert()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createProspectCold(user, '0455566787');
        InterestingMoment__c moment = TestDataInterestingMomentMock.create(user, contact, 'Email Link Clicked', '/online-portal-tour/', 25);
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());

        // Act
        Test.startTest();
        Integer callCount = Limits.getFutureCalls();
        ActionResult result = InterestingMomentService.sendCAAlert(new List<InterestingMoment__c> { moment }, new Map<Id, InterestingMoment__c>(), Enums.TriggerContext.AFTER_INSERT);
        callCount = Limits.getFutureCalls() - callCount;
        Test.stopTest();

        // Assert
        System.assertEquals(true, result.isSuccess);
        System.assertEquals(1, callCount);
    }
    
    @isTest
    static void KeepTalking_SendsCAAlert()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createProspectCold(user, '0455566787');
        InterestingMoment__c moment = TestDataInterestingMomentMock.create(user, contact, 'Form Completed', 'Need Help', 25);
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());

        // Act
        Test.startTest();
        Integer callCount = Limits.getFutureCalls();
        ActionResult result = InterestingMomentService.sendCAAlert(new List<InterestingMoment__c> { moment }, new Map<Id, InterestingMoment__c>(), Enums.TriggerContext.AFTER_INSERT);
        callCount = Limits.getFutureCalls() - callCount;
        Test.stopTest();

        // Assert
        System.assertEquals(true, result.isSuccess);
        System.assertEquals(1, callCount);
    }

    @isTest
    static void ReserveYourSpot_SendsCAAlert()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createProspectCold(user, '0455566787');
        InterestingMoment__c moment = TestDataInterestingMomentMock.create(user, contact, 'Form Completed', 'Reserve Your Spot', 25);
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());

        // Act
        Test.startTest();
        Integer callCount = Limits.getFutureCalls();
        ActionResult result = InterestingMomentService.sendCAAlert(new List<InterestingMoment__c> { moment }, new Map<Id, InterestingMoment__c>(), Enums.TriggerContext.AFTER_INSERT);
        callCount = Limits.getFutureCalls() - callCount;
        Test.stopTest();

        // Assert
        System.assertEquals(true, result.isSuccess);
        System.assertEquals(1, callCount);
    }

    @isTest
    static void LeadScoreOver300_SendsCAAlert()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createProspectCold(user, '0455566787');
        InterestingMoment__c moment = TestDataInterestingMomentMock.create(user, contact, 'Milestone', 'Lead Score Over 300', 50);
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());

        // Act
        Test.startTest();
        Integer callCount = Limits.getFutureCalls();
        ActionResult result = InterestingMomentService.sendCAAlert(new List<InterestingMoment__c> { moment }, new Map<Id, InterestingMoment__c>(), Enums.TriggerContext.AFTER_INSERT);
        callCount = Limits.getFutureCalls() - callCount;
        Test.stopTest();

        // Assert
        System.assertEquals(true, result.isSuccess);
        System.assertEquals(1, callCount);
    }

    @isTest
    static void ProspectFillsFirstFormAgain_SendsCAAlert()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createProspectCold(user, '0455566787');
        contact.ActualCreateDate__c = DateTime.now() - Integer.valueOf(Label.DaysToSkipForExistingProspectFillInFormAlert) - 1;
        contact.HasOptedOutOfEmail = true;
        update contact;

        InterestingMoment__c moment = TestDataInterestingMomentMock.create(user, contact, 'Form Completed', 'Reserve Your Spot', 25);
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());

        // Act
        Test.startTest();
        Integer callCount = Limits.getFutureCalls();
        ActionResult result = InterestingMomentService.sendCAAlert(new List<InterestingMoment__c> { moment }, new Map<Id, InterestingMoment__c>(), Enums.TriggerContext.AFTER_INSERT);
        callCount = Limits.getFutureCalls() - callCount;
        Test.stopTest();

        // Assert
        contact =
            [
                SELECT Id, HasOptedOutOfEmail
                FROM Contact
                WHERE Id = : contact.Id
            ];
        System.assertEquals(true, result.isSuccess);
        System.assertEquals(2, callCount);
        System.assertEquals(false, contact.HasOptedOutOfEmail);
    }

    @isTest
    static void ProspectFillsMoreFormAgain_NoCAAlert()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createProspectCold(user, '0455566787');
        contact.ActualCreateDate__c = DateTime.now() - Integer.valueOf(Label.DaysToSkipForExistingProspectFillInFormAlert) - 1;
        contact.HasOptedOutOfEmail = true;
        update contact;

        InterestingMoment__c moment = TestDataInterestingMomentMock.create(user, contact, 'Form Completed', 'Reserve Your Spot', 25);
        moment = TestDataInterestingMomentMock.create(user, contact, 'Form Completed', 'Enquire Now', 25);
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());

        // Act
        Test.startTest();
        Integer callCount = Limits.getFutureCalls();
        ActionResult result = InterestingMomentService.sendCAAlert(new List<InterestingMoment__c> { moment }, new Map<Id, InterestingMoment__c>(), Enums.TriggerContext.AFTER_INSERT);
        callCount = Limits.getFutureCalls() - callCount;
        Test.stopTest();

        // Assert
        contact =
            [
                SELECT Id, HasOptedOutOfEmail
                FROM Contact
                WHERE Id = : contact.Id
            ];
        System.assertEquals(true, result.isSuccess);
        System.assertNotEquals(2, callCount);
        System.assertEquals(false, contact.HasOptedOutOfEmail);
    }

    @isTest
    static void NotEligible_SendsCAAlert()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createProspectCold(user, '0455566787');
        InterestingMoment__c moment = TestDataInterestingMomentMock.create(user, contact, 'Form Completed', 'Check Your Eligibility (Ineligible', 25);
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());

        // Act
        Test.startTest();
        Integer callCount = Limits.getFutureCalls();
        ActionResult result = InterestingMomentService.sendCAAlert(new List<InterestingMoment__c> { moment }, new Map<Id, InterestingMoment__c>(), Enums.TriggerContext.AFTER_INSERT);
        callCount = Limits.getFutureCalls() - callCount;
        Test.stopTest();

        // Assert
        System.assertEquals(true, result.isSuccess);
        System.assertEquals(1, callCount);
    }

    @isTest
    static void OAFSubmitted_SendsCAAlert()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createProspectHot(user);
        Opportunity opportunity = TestDataOpportunityMock.createApplyingAcquisition(user, contact);
        InterestingMoment__c moment = TestDataInterestingMomentMock.create(user, contact, 'Form Completed', 'Online Application Form', 25);
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());

        // Act
        Test.startTest();
        Integer callCount = Limits.getFutureCalls();
        ActionResult result = InterestingMomentService.sendCAAlert(new List<InterestingMoment__c> { moment }, new Map<Id, InterestingMoment__c>(), Enums.TriggerContext.AFTER_INSERT);
        callCount = Limits.getFutureCalls() - callCount;
        Test.stopTest();

        // Assert
        System.assertEquals(true, result.isSuccess);
        System.assertEquals(1, callCount);
    }
}