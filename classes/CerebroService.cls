public class CerebroService
{
    public static ActionResult syncApplication(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        return new CerebroSyncApplicationCommand().execute(items, oldMap, triggerContext);
    }
}