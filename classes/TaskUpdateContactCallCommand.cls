public class TaskUpdateContactCallCommand extends BaseTriggerCommand
{
    public override List<SObject> validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        List<Task> tasks = new List<Task>();

        if (triggerContext == Enums.TriggerContext.AFTER_INSERT || triggerContext == Enums.TriggerContext.AFTER_UPDATE)
        {
            for (Task task : (List<Task>) items)
            {
                if (task.TaskSubtype == 'Call' && String.valueOf(task.WhoId).startsWith(Constants.ENTITYID_CONTACT) && !String.isEmpty(task.Call_Outcome__c) &&
                    task.NVMCallSummary__c != null)
                    tasks.add(task);
            }
        }

        return tasks;
    }

    public override ActionResult command(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        List<Task> tasksToUpdate = new List<Task>();
        List<Contact> contactsToUpdate = new List<Contact>();
        List<Task> tasks = getTasks(SObjectUtils.getIds(items, 'Id'));
        Map<Id, Contact> contactMap = getContactMap(SObjectUtils.getIDs(items, 'WhoId'));

        for (Task task : tasks)
        {
            Contact taskContact = contactMap.get(task.WhoId);

            if (taskContact == null)
            {
                String errorMessage = String.format('Failed to retrieve contact({0}) from db', new String[] { task.WhoId });
                Log.error(this.className, 'command', errorMessage);
                return new ActionResult(ResultStatus.ERROR, errorMessage);
            }

            if (taskContact.Tasks.size() == 0)
            {
                Log.info(this.className, 'command', 'Expecting at least one call task with call outcome for contact {0}', new List<String> { task.WhoId });
                continue;
            }

            Task latestContactCallTask = taskContact.Tasks[0];

            if (task.Id != latestContactCallTask.Id)
            {
                Log.info(this.className, 'command', 'Task ({0}) is not the latest. Contact not updated. Currently latest call task is {1}',
                    new List<String>{ task.Id, latestContactCallTask.Id });
                continue;
            }

            if (task.Call_Outcome__c != taskContact.LastCallOutcome__c)
            {
                Log.info(this.className, 'command', 'Updating contact ({0}) call fields', new List<String>{ taskContact.Id });
                taskContact.LastCallOutcome__c = task.Call_Outcome__c;
                taskContact.LastCallDateTime__c = Datetime.now();
                taskContact.NVMConnect__NextContactTime__c = Datetime.now().addHours(9);

                task.CallDisposition = task.Call_Outcome__c;

                tasksToUpdate.add(task);
                contactsToUpdate.add(taskContact);
            }
        }

        if (contactsToUpdate.size() > 0)
            uow.registerDirty(contactsToUpdate);

        if (tasksToUpdate.size() > 0)
            uow.registerDirty(tasksToUpdate);

        return new ActionResult(ResultStatus.SUCCESS);
    }

    private Map<Id, Contact> getContactMap(Set<Id> contactIds)
    {
        return new Map<Id, Contact>
        ([
            SELECT Id, LastCallDateTime__c, LastCallOutcome__c, NVMConnect__NextContactTime__c,
                (
                    SELECT Id, Call_Outcome__c, CreatedDate, TaskSubtype
                    FROM Tasks
                    WHERE TaskSubtype = 'Call' AND Call_Outcome__c <> ''
                    ORDER BY CreatedDate DESC
                )
            FROM Contact
            WHERE Id IN :contactIds
        ]);
    }

    private List<Task> getTasks(Set<Id> taskIds)
    {
        return [SELECT Id, Call_Outcome__c, CallDisposition, WhoId FROM Task WHERE Id IN :taskIds];
    }
}