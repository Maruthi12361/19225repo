@isTest
private class BaseTriggerCommandTests
{
    @testSetup
    static void setup()
    {
        TestUtilities.setupHEDA();
    }

    @isTest
    static void ExecuteCommandWithNullParameters_RaisesExeceptionToCaller()
    {
        // Arrange
        List<SObject> nullObj;
        TestCommand command = new TestCommand();

        // Act
        try
        {
            command.execute(nullObj, new Map<Id, SObject>(), Enums.TriggerContext.AFTER_INSERT);
        }
        catch(Exception e)
        {
            //Assert
            System.assert(e instanceof InvalidParameterValueException, 'Executing a command with a null parameter should raise an exception for the developer.');
        }
    }

    @isTest
    static void ExecuteCommandWithNullOldMap_RaisesExeceptionToCaller()
    {
        // Arrange
        TestCommand command = new TestCommand();

        // Act
        try
        {
            command.execute(new List<SObject>(), null, Enums.TriggerContext.AFTER_INSERT);
        }
        catch(Exception e)
        {
            //Assert
            System.assert(e instanceof InvalidParameterValueException, 'Executing a command with a null parameter should raise an exception for the developer.');
        }
    }

    @isTest
    static void ExcecuteCommandWithEmptySetOfParameters_ReturnsSuccessResultToCaller()
    {
        // Arrange
        List<SObject> items = new List<SObject>();
        TestCommand command = new TestCommand();

        // Act
        ActionResult result = command.execute(items, new Map<Id, SObject>(), Enums.TriggerContext.AFTER_INSERT);

        // Arrange
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Executing a command without any items to action against should return a successful result to the caller.'); 
    }

    @isTest
    static void ExecuteCommandAfterSuccessfulValidationAndExecution_ReturnsSuccessToCaller()
    {
        // Arrange
        List<Case> items = new List<SObject> { new Case() };
        TestCommand command = new TestCommand();

        // Act
        ActionResult result = command.execute(items, new Map<Id, SObject>(), Enums.TriggerContext.AFTER_INSERT);

        //Assert
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Executing a command with valid items, validating and executing successfully, should return a successful result to the caller.'); 
    }

    class TestValidationExeceptionCommand extends BaseTriggerCommand
    {
        public override List<SObject> validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
        {
            throw new SObjectException('Unexpected exception during validation action');
        }

        public override ActionResult command(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
        {
            return new ActionResult();
        }
    }

    class TestCommandExceptionCommand extends BaseTriggerCommand
    {
        public override List<SObject> validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
        {
            return items;
        }

        public override ActionResult command(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
        {
            throw new SObjectException('Unexpected exception during command action');
        }
    }

    class TestCommand extends BaseTriggerCommand
    {
        public override List<SObject> validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
        {
            return items;
        }

        public override ActionResult command(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
        {
            return new ActionResult();
        }
    }
}