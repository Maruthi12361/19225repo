public class TestDataCoursePlanMock
{
    private static Map<String, hed__Program_Plan__c> planMap = new Map<String, hed__Program_Plan__c>();

    public static hed__Program_Plan__c create(User user, String name, Id courseId)
    {
        return create(user, name, courseId, '1', true);
    }

    public static hed__Program_Plan__c create(User user, String name, Id courseId, String version, Boolean isPrimary)
    {
        return create(user, new List<ViewModelsTestData.CoursePlan> { new ViewModelsTestData.CoursePlan(name, courseId, version, isPrimary) })[0];
    }

    public static List<hed__Program_Plan__c> create(User user, List<ViewModelsTestData.CoursePlan> coursePlans)
    {
        List<hed__Program_Plan__c> result = new List<hed__Program_Plan__c>();
        List<hed__Program_Plan__c> inserts = new List<hed__Program_Plan__c>();

        for (ViewModelsTestData.CoursePlan coursePlan: coursePlans)
        {
            String key = coursePlan.Name + coursePlan.courseId;

            if (planMap.containsKey(key))
            {
                result.add(planMap.get(key));
                continue;
            }

            if (planMap.size() == 0)
            {
                List<hed__Program_Plan__c> items = 
                    [
                        SELECT Name, hed__Account__c, hed__Version__c, hed__Is_Primary__c
                        FROM hed__Program_Plan__c
                    ];

                for (hed__Program_Plan__c item: items)
                    planMap.put(item.Name + item.hed__Account__c, item);
            }
        
            if (planMap.containsKey(key))
            {
                result.add(planMap.get(key));
                continue;
            }
        
            hed__Program_Plan__c newItem = new hed__Program_Plan__c
            (
                Name = coursePlan.Name,
                hed__Account__c = coursePlan.CourseId,
                hed__Version__c = coursePlan.Version,
                hed__Is_Primary__c = coursePlan.IsPrimary
            );

            planMap.put(key, newItem);
            inserts.add(newItem);
            result.add(newItem);
        }

        if (inserts.size() > 0)
        {
            if (user  != null)
            {
                System.RunAs(user)
                {
                    insert inserts;
                }
            }
            else
            {
                insert inserts;
            }
        }

        return result;
    }
}