@isTest
public class TaskCreateCommandTest 
{
    @testSetup 
    public static void setup()
    {
        TestUtilities.setupHEDA();
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact contact = TestDataContactMock.createProspectCold(user, '0455548889');
    }

    @isTest 
    static void NewTask_CreatedSuccessfully() 
    {
        // Arrange 
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact contact = [SELECT Id FROM Contact Limit 1];
        Task task = TestDataTaskMock.createRolloverEmailTask(user, contact.Id, null, 'Test Create Command', false);
        task.LastModifiedDate = null;

        // Act
        ActionResult result = new TaskCreateCommand().execute(new List<Task>{ task });
        Task createdTask = [SELECT Id, WhoId, Subject FROM Task Limit 1];

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Unexpected status value'); 
        System.assertEquals(contact.Id, createdTask.WhoId, 'Unexpected who id value'); 
        System.assertEquals('Test Create Command', createdTask.Subject, 'Unexpected subject value'); 
    }
}