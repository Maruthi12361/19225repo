@isTest
private class OpportunityTriggerHandlerTests
{
    @testSetup 
    private static void testSetup()
    {
        TestUtilities.setupHEDA();
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createStudent(user);
    }

    @isTest
    public static void InsertOpportunityAcquisition_ExecutesTriggerWithoutException()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = ContactService.GetByName('Forrest Gump');

        // Act
        test.startTest();  
        TestDataOpportunityMock.createApplyingAcquisition(user, contact);
        test.stopTest();

        // Assert
        System.assert(true);
    }

    @isTest
    public static void UpdateOpportunityAcquisition_ExecutesTriggerWithoutException()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = ContactService.GetByName('Forrest Gump');
        Opportunity opp = TestDataOpportunityMock.createApplyingAcquisition(user, contact); 

        // Act
        test.startTest();
        opp.Name = 'Update Opportunity Test';
        update opp;
        test.stopTest();

        // Assert
        System.assert(true);
    }
}