@isTest
public class SMSSendViaSMSMagicCommandTests 
{
    @testSetup
    static void setup()
    {
        TestUtilities.setupHEDA();
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createProspect(user, 'John', 'Smith', '', '0455566755', 'john.smith@gmail.com', Constants.STATUS_COLD, 'A001588400', 'Australia', 'Website (Organic)');
    }

    @isTest
    private static void SendToRecipientWithSMSMagicProvider_SendSMSViaSMSMagic()
    {
        // Arrange
        Contact contact = [SELECT Id, Email, FirstName, LastName, MobilePhone FROM Contact LIMIT 1];

        // Act
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        ActionResult result = SMSService.sendToRecipient(Constants.APIPROVIDER_SMSMAGIC, 'SMSServiceTest', contact.MobilePhone, 'SendToRecipient_SendSMSBySMSMagic');
        Test.stopTest();

        // Assert
        System.assert(result.isSuccess, 'Fail to send SMS by SMS Magic API');
    }
}