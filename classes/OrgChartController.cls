public class OrgChartController {

      @AuraEnabled
      public static List<User> getUsers() {
          List<Id> managerIds = new List<Id>();
          for(User u : [select ManagerId from User where Include_On_Org_Chart__c = true]) {
              managerIds.add(u.ManagerId);
          }
          
          List<User> users = [select 
                              	Id, Name, ManagerId, Title, Department, Extension, SmallPhotoUrl, FullPhotoUrl, Fire_Warden__c, First_Aid_Officer__c, Contact_Officer__c
                              from User
                              where Include_On_Org_Chart__c = true or Id in :managerIds
                              order by ManagerId];
          return users;
       }
}