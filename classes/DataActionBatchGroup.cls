public class DataActionBatchGroup extends BaseComponent
{
    private List<DataActionBatch> batches { get; set; }

    public DataActionBatchGroup(List<DataActionBatch> actionBatches)
    {
        this.batches = actionBatches;
    }

    public void executeBatches(Integer batchSize)
    {
        if (batches.size() > 0 )
        {
            for (Integer i=0; i< batches.size(); i++)
            {
                DataActionBatch batch = batches[i];
                batch.nextSize = batchSize;

                if ( i < batches.size() - 1)
                    batch.next = batches[i+1];
                else
                    batch.next = null;
            }

            Database.executeBatch(batches[0], batchSize);
        }
    }
}