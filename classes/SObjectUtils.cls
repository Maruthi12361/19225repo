public class SObjectUtils
{
    private static String accessToken = '';
    private static Map<String, Map<String, Schema.sObjectField>> sObjectFieldsMap = new Map<String, Map<String, Schema.sObjectField>>();

    // Provides an index on which to base a fake Id to allow uniqueness for temporary references between objects before insertion into the DB
    private static Integer fakeSubjectOfferingIndex = 1;

    public static Boolean hasPropertyChanged(SObject newObject, SObject oldObject, String field)
    {
        return hasPropertyChanged(newObject, oldObject, field, true, false);
    }

    public static Boolean hasPropertyChangedIgnoreCase(SObject newObject, SObject oldObject, String field)
    {
        return hasPropertyChanged(newObject, oldObject, field, false, false);
    }

    private static Boolean hasPropertyChangedIgnoreLineBreak(SObject newObject, SObject oldObject, String field)
    {
        return hasPropertyChanged(newObject, oldObject, field, true, true);
    }

    private static Boolean hasPropertyChanged(SObject newObject, SObject oldObject, String field, Boolean isCaseSensitive, Boolean ignoreLineBreak)
    {
        if (newObject == null && oldObject != null)
        {
            Log.info('SObjectUtils', 'hasPropertyChanged', 'Testing field {0}. Different. New object is null, old is not.', new String[] { field });
            return true;
        }

        if (newObject != null && oldObject == null)
        {
            Log.info('SObjectUtils', 'hasPropertyChanged', 'Testing field {0}. Different. Old object is null, new is not.', new String[] { field });
            return true;
        }

        if (newObject == null && oldObject == null)
        {
            Log.info('SObjectUtils', 'hasPropertyChanged', 'Testing field {0}. Same. Old object is null, new is null.', new String[] { field });
            return false;
        }

        Object newValue = newObject.get(field);
        Object oldValue = oldObject.get(field);
        
        if (newValue == null && oldValue != null)
        {
            Log.info('SObjectUtils', 'hasPropertyChanged', 'Testing field {0}. Different. New value: {1}, Old value: {2}.', new String[] { field, String.valueOf(newValue), String.valueOf(oldValue) });
            return true;
        }

        if (newValue != null && oldValue == null)
        {
            Log.info('SObjectUtils', 'hasPropertyChanged', 'Testing field {0}. Different. New value: {1}, Old value: {2}.', new String[] { field, String.valueOf(newValue), String.valueOf(oldValue) });
            return true;
        }

        if (newValue == null && oldValue == null)
        {
            return false;
        }

        if (ignoreLineBreak && getFieldType(newObject, field) == Schema.DisplayType.TEXTAREA)
        {
            String stringValue = (String) newValue;
            newValue = stringValue.normalizeSpace();
            stringValue = (String) oldValue;
            oldValue = stringValue.normalizeSpace();
        }

        if (isCaseSensitive)
        {
            if (newValue instanceof String && oldValue instanceof String)
            {
                Boolean isDifferent = !StringUtils.equal((String)newValue, (String)oldValue);

                if (isDifferent)
                    Log.info('SObjectUtils', 'hasPropertyChanged', 'Testing field {0}. {1}. New value: {2}, Old value: {3}.', new String[] { field, (isDifferent ? 'Different' : 'Same'), String.valueOf(newValue), String.valueOf(oldValue) });

                return isDifferent;
            }
        }

        if (newValue != oldValue)
        {
            Log.info('SObjectUtils', 'hasPropertyChanged', 'Testing field {0}. Different. New value: {1}, Old value: {2}.', new String[] { field, String.valueOf(newValue), String.valueOf(oldValue) });
            return true;
        }

        return false;
    }

    public static Boolean hasAnyPropertyChanged(SObject newObject, SObject oldObject, List<String> fields)
    {
        for (String field : fields)
        {
            if (hasPropertyChanged(newObject, oldObject, field))
                return true;
        }

        return false;
    }

    public static Boolean hasAnyPropertyChangedIgnoreLineBreak(SObject newObject, SObject oldObject, List<String> fields)
    {
        for (String field : fields)
        {
            if (hasPropertyChangedIgnoreLineBreak(newObject, oldObject, field))
                return true;
        }

        return false;
    }

    public static Boolean hasAnyPropertyChangedIgnoreCase(SObject newObject, SObject oldObject, List<String> fields)
    {
        for (String field : fields)
        {
            if (hasPropertyChangedIgnoreCase(newObject, oldObject, field))
                return true;
        }

        return false;
    }

    public static Set<Id> getIDs(List<SObject> items, String fieldName)
    {
        Set<ID> ids = new Set<ID>();

        for (SObject item: items)
        {
            if (item.get(fieldName) != null)
                ids.add(Id.valueOf((String)item.get(fieldName)));
        }

        return ids;
    }

    public static List<ViewModels.IdWrap> getIdWraps(List<SObject> items)
    {
        ViewModels.IdWrap idWrap;
        List<ViewModels.IdWrap> ids = new List<ViewModels.IdWrap>();

        for (SObject item: items)
        {
            if (item.ID != null)
            {
                idWrap = new ViewModels.IdWrap();
                idWrap.ID = item.ID;
                ids.add(idWrap);
            }
        }

        return ids;
    }

    public static Set<Id> unwrapIds(List<ViewModels.IdWrap> items)
    {
        Set<ID> ids = new Set<ID>();

        for (ViewModels.IdWrap item: items)
            ids.add(item.ID);

        return ids;
    }

    public static Map<String, String> getPicklistValues(String objectName, String fieldName, String recordTypeName)
    {

        Map<String, String> resultMap = new Map<String, String>();

        if (String.isEmpty(recordTypeName))
        {
            Schema.DescribeSobjectResult[] results = Schema.describeSObjects(new String[]{ objectName });

            for(Schema.DescribeSobjectResult res : results) 
            {
                for (Schema.PicklistEntry entry : res.fields.getMap().get(fieldName).getDescribe().getPicklistValues()) 
                {
                    if (entry.isActive()) 
                        resultMap.put(entry.getLabel(), entry.getValue()); 
                }
            }
        }
        else
        {
            String authBearer = '';
            String recordTypeId = String.valueOf(RecordTypeUtils.getRecordTypeId(recordTypeName, objectName));
            String endpoint = String.format('/services/data/v46.0/ui-api/object-info/{0}/picklist-values/{1}/{2}', new List<String>{ objectName, recordTypeId, fieldName });
            String url = URL.getSalesforceBaseUrl().toExternalForm() + endpoint;

            if (String.isEmpty(accessToken))
            {
                if (Test.isRunningTest())
                    accessToken = UserInfo.getSessionId();
                else
                    accessToken = getSessionId();

                authBearer = accessToken;
            }
            else
                authBearer = accessToken;

            Http http = new Http();
            HttpRequest request = new HttpRequest();
            HttpResponse response = null;

            Log.info('SObjectUitls', 'getPicklistValues', 'Request url: {0}', new List<String> { url });

            request.setEndpoint(url);
            request.setMethod('GET');
            request.setHeader('Accept', 'application/json');
            request.setHeader('Content-Type', 'application/json');
            request.setHeader('Authorization', 'Bearer ' + authBearer);

            response = http.send(request);

            if (response.getStatusCode() != 200)
            {
                Log.error('SObjectUtils', 'getPicklistValues', 'Error retrieving picklist values. Object Name: ' + objectName +
                    'FieldName: ' + fieldName + ' Error Code: ' + response.getStatusCode() + ' ' + response.getBody());
                throw new Exceptions.ApplicationException('Failed to retrive picklist value for ' + fieldName);
            }

            Map<String, Object> responseMap = (Map<String, Object>)JSON.deserializeUntyped(response.getBody());

            if (responseMap.get('values') == null)
            {
                Log.error('SObjectUtils', 'getPicklistValues', 'No values in API response. Object Name: ' + objectName + 'FieldName: ' +
                    fieldName + ' Error Code: ' + response.getStatusCode() + ' Response Body: ' + response.getBody());
                throw new Exceptions.ApplicationException('Failed to retrive picklist value for ' + fieldName);
            }
            
            List<ViewModels.PicklistValue> picklistValues =  (List<ViewModels.PicklistValue>) JSON.deserialize(JSON.serialize(responseMap.get('values')), List<ViewModels.PicklistValue>.class);

            for (ViewModels.PicklistValue picklist : picklistValues)
                resultMap.put(picklist.label, picklist.value);

        }

        return resultMap;
    }

    private static Schema.DescribeFieldResult getFieldDescribe(SObject objectInstance, String fieldName)
    {
        Schema.SObjectType sObjectType = objectInstance.getSObjectType();
        Schema.DescribeSObjectResult objectDescripteResult = sObjectType.getDescribe();
        String key = objectDescripteResult.getName();
        Map<String, Schema.sObjectField> fieldsMap;

        if (!sObjectFieldsMap.containsKey(key))
        {
            fieldsMap = objectDescripteResult.fields.getMap();
            sObjectFieldsMap.put(key, fieldsMap);
        }
        else
            fieldsMap = sObjectFieldsMap.get(key);

        return fieldsMap.get(fieldName).getDescribe();
    }

    public static Schema.DisplayType getFieldType(SObject objectInstance, String fieldName)
    {
        return getFieldDescribe(objectInstance, fieldName).getType();
    }
    
    public static Boolean isStringType(SObject objectInstance, String fieldName)
    {
        return getFieldDescribe(objectInstance, fieldName).getSOAPType() == Schema.SOAPType.STRING;
    }
    
    public static Integer getMaxStringLength(SObject objectInstance, String fieldName)
    {
        return getFieldDescribe(objectInstance, fieldName).getLength();
    }

    public static Boolean isFieldOfType(Schema.DisplayType fieldType, SObject objectInstance, String fieldName)
    {
        return getFieldDescribe(objectInstance, fieldName).getType() == fieldType;
    }

    public static String getFakeId(String prefixSchemaObjectType)
    {
        return prefixSchemaObjectType + String.valueOf(fakeSubjectOfferingIndex++).leftPad(12, '0');
    }

    private static String getSessionId()
    {
        String NS_SF = 'urn:partner.soap.sforce.com';
        String NS_SOAP = 'http://schemas.xmlsoap.org/soap/envelope/';
        String userPwdClientSecret = 'AIB_Sync@2016' + 'wRsx28yyXqPnMSaNmohYBvfu8';
        
        User syncUser = UserService.getByAlias(Constants.USER_SYNC); 

        Http http = new Http();
        HttpRequest request = new HttpRequest();
        HttpResponse response = null;

        request.setMethod('POST');  
        request.setEndpoint(URL.getSalesforceBaseUrl().toExternalForm() + '/services/Soap/u/43.0');
        request.setHeader('Content-Type', 'text/xml;charset=UTF-8');
        request.setHeader('SOAPAction', '""');
        request.setBody
        (
            '<Envelope xmlns="http://schemas.xmlsoap.org/soap/envelope/"> ' +
                '<Header/> ' +
                '<Body> ' +
                    '<login xmlns="urn:partner.soap.sforce.com"> ' +
                        '<username>' + syncUser.Username + '</username> ' +
                        '<password>' + userPwdClientSecret + '</password> ' +
                    '</login> ' + 
                '</Body> ' + 
            '</Envelope>'
        );

        response = http.send(request);

        if (response.getStatusCode() != 200)
        {
            Dom.Document responseDocument = response.getBodyDocument();
            Dom.Xmlnode rootElm = responseDocument.getRootElement(); // soapenv:Envelope
            Dom.Xmlnode bodyElm = rootElm.getChildElement('Body', NS_SOAP); // soapenv:Body
            Dom.Xmlnode faultElm = bodyElm.getChildElement('Fault', NS_SOAP); // soapenv:Fault
            Dom.Xmlnode faultStringElm = faultElm.getChildElement('faultstring', null); // faultstring
            throw new Exceptions.ApplicationException(faultStringElm.getText()); 
        }

        Dom.Document responseDocument = response.getBodyDocument();
        Dom.Xmlnode rootElm = responseDocument.getRootElement(); // soapenv:Envelope
        Dom.Xmlnode bodyElm = rootElm.getChildElement('Body', NS_SOAP); // soapenv:Body
        Dom.Xmlnode loginResponseElm = bodyElm.getChildElement('loginResponse', NS_SF); // loginResponse
        Dom.Xmlnode resultElm = loginResponseElm.getChildElement('result', NS_SF); // result
        Dom.Xmlnode sessionIdElm = resultElm.getChildElement('sessionId', NS_SF); // sessionId

        return sessionIdElm.getText();
    }
}