public class ViewModelsCaseEnrolment
{
    public class Payload
    {
        @AuraEnabled
        public String Type { get; set; }
        @AuraEnabled
        public String SubType { get; set; }
        @AuraEnabled
        public String Message { get; set; }
        @AuraEnabled
        public List<Interfaces.IViewModel> Data { get; set; }

        public Payload(String caseType, String dataType, List<Interfaces.IViewModel> dataList, String stringMessage)
        {
            Type = caseType;
            SubType = dataType;
            Data = dataList;
            Message = stringMessage;
        }
    }

    public class Details implements Interfaces.IViewModel
    {
        public Id Id { get; set; }
        public Id caseId { get; set; }
        public Id subjectEnrolmentId { get; set; }
    }

    public class ExemptionDetails implements Interfaces.IViewModel
    {
        public Id caseId { get; set; }
        public Id recordId { get; set; }
    }

    public class SubjectDetails implements Interfaces.IViewModel
    {
        @AuraEnabled
        public String Id { get; set; }
        @AuraEnabled
        public String SubjectName { get; set; }
        @AuraEnabled
        public String SubjectCode { get; set; }
        @AuraEnabled
        public Integer Units { get; set; }
        @AuraEnabled
        public String AQF { get; set; }
    }

    public class PlanRequirementDetails implements Interfaces.IViewModel
    {
        @AuraEnabled
        public String Id { get; set; }
        @AuraEnabled
        public String Course { get; set; }
        @AuraEnabled
        public String Category { get; set; }
        @AuraEnabled
        public String SubjectName { get; set; }
        @AuraEnabled
        public String SubjectCode { get; set; }
        @AuraEnabled
        public Integer Units { get; set; }
        @AuraEnabled
        public String AQF { get; set; }
    }

    public static PlanRequirementDetails convertPlanRequirement(hed__Plan_Requirement__c requirement, Boolean isForPortal)
    {
        PlanRequirementDetails item = new PlanRequirementDetails();
        item.Course = requirement.hed__Plan_Requirement__r.hed__Program_Plan__r.Name;
        item.Category = requirement.hed__Plan_Requirement__r.Name;
        SubjectDetails subject = convertSubject(requirement.hed__Course__r, isForPortal);
        item.SubjectName = subject.SubjectName;
        item.SubjectCode = subject.SubjectCode;
        item.Units = Integer.valueOf(subject.Units);
        item.AQF = subject.AQF;

        if (!isForPortal)
        {
            item.Id = requirement.Id;
        }

        return item;
    }

    public static SubjectDetails convertSubject(hed__Course__c subject, Boolean isForPortal)
    {
        SubjectDetails item = new SubjectDetails();
        item.SubjectName = subject.Name;
        item.SubjectCode = subject.hed__Course_ID__c;
        item.Units = Integer.valueOf(subject.hed__Credit_Hours__c);
        item.AQF = subject.AQFLevel__c;

        if (!isForPortal)
        {
            item.Id = subject.Id;
        }

        return item;
    }

    public class EnrolmentDetails implements Interfaces.IViewModel
    {
        @AuraEnabled
        public String Id { get; set; }
        @AuraEnabled
        public String EnrolmentType { get; set; }
        @AuraEnabled
        public String Name { get; set; }
        @AuraEnabled
        public String Code { get; set; }
        @AuraEnabled
        public String SubjectOffering { get; set; }
        @AuraEnabled
        public String SubjectOfferingId { get; set; }
        @AuraEnabled
        public String Status { get; set; }
        @AuraEnabled
        public String Grade { get; set; }
        @AuraEnabled
        public Date AdminDate { get; set; }
        @AuraEnabled
        public String Contact { get; set; }
        @AuraEnabled
        public String ContactId { get; set; }
        @AuraEnabled
        public String CourseEnrolment { get; set; }
        @AuraEnabled
        public String CourseEnrolmentId { get; set; }
        @AuraEnabled
        public String Term { get; set; }
        @AuraEnabled
        public Date StartDate { get; set; }
    }

    public static EnrolmentDetails convertEnrolment(hed__Course_Enrollment__c enrolment, Boolean isForPortal)
    {
        EnrolmentDetails item = new EnrolmentDetails();
        item.Id = enrolment.Id;
        item.EnrolmentType = enrolment.Type__c;
        item.Name = enrolment.SubjectName__c;
        item.Code = enrolment.SubjectCode__c;
        item.SubjectOffering = enrolment.hed__Course_Offering__c == null ? item.Code + ' ' + item.Name : enrolment.hed__Course_Offering__r.Name;
        item.Status = enrolment.hed__Status__c;
        item.Grade = enrolment.Grade__c;
        item.AdminDate = enrolment.SubjectOfferingAdminDate__c;
        item.Contact = enrolment.hed__Contact__r.Name;
        item.CourseEnrolment = enrolment.hed__Program_Enrollment__r.hed__Program_Plan__r.Name;

        if (!isForPortal)
        {
            item.SubjectOfferingId = enrolment.hed__Course_Offering__c;
            item.ContactId = enrolment.hed__Contact__c;
            item.CourseEnrolmentId = enrolment.hed__Program_Enrollment__c;
        }
        else
        {
            item.Term = enrolment.SubjectOfferingTerm__c;
            item.StartDate = enrolment.SubjectOfferingStartDate__c;
        }

        return item;
    }
}