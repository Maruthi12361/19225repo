public class ContactSyncRolloverStatusCommand extends BaseTriggerCommand
{
    public override List<SObject> validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        List<Contact> contactList = new List<Contact>();
        
        if (triggerContext == Enums.TriggerContext.AFTER_UPDATE)
        {
            for (Contact newContact : (List<Contact>) items)
            {
                Contact oldContact = (Contact) oldMap.get(newContact.Id);           
                
                if (oldContact.Rollover_Status__c == newContact.Rollover_Status__c)
                {
                    Log.info(this.className, 'validate', 'Skipping contact ' + newContact.Id + '. Status has not changed.');
                    continue;
                }
                
                Log.info(this.className, 'validate', 'Old Rollover Status: ' + oldContact.Rollover_Status__c + '; new Rollover Status: ' + newContact.Rollover_Status__c);
                
                if (newContact.Current_Rollover_Campaign__c == null)
                {
                    Log.info(this.className, 'validate', 'Skipping contact ' + newContact.Id + '. Contact has no associated rollover campaign.');
                    continue;
                }
                
                if (!(newContact.Rollover_Status__c == RolloverConstantsDefinition.ST_CONTACTING 
                      || newContact.Rollover_Status__c == RolloverConstantsDefinition.ST_ENROLLING))
                {
                    Log.info(this.className, 'validate', 'Skipping contact ' + newContact.Id + '. Contact has a manual status already applied.');
                    continue;
                }
                
                contactList.add(newContact);
            }
        }
        
        return contactList;
    }
    
    public override ActionResult command(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        Map<string, Contact> contactMap = new Map<string, Contact>();
        List<CampaignMember> updates = new List<CampaignMember>();
        List<Id> campaignIds = new List<Id>();
        List<Id> contactIds = new List<Id>();
        
        for (Contact newContact : (List<Contact>)items)
        {
            contactIds.add(newContact.Id);
            campaignIds.add(newContact.Current_Rollover_Campaign__c);
            contactMap.put(newContact.Campaign_Contact_Id__c, newContact);
        }
        
        List<CampaignMember> members = 
            [
                SELECT Id, Status, Campaign_Contact_Id__c
                FROM CampaignMember
                WHERE CampaignId IN :campaignIds
                AND ContactId IN :contactIds
            ];
        
        
        // Find and update corresponding campaign members
        for (CampaignMember member : members) 
        { 
            Contact contact = contactMap.get(member.Campaign_Contact_Id__c);
            
            if (contact == null) 
                continue;
            
            Log.info(this.className, 'command', 'Current CampaignMember status: ' + member.Status);
            
            // Non-Closed status
            if (member.Status != contact.Rollover_Status__c && contact.Rollover_Status__c != RolloverConstantsDefinition.RO_DEFERRED 
                && contact.Rollover_Status__c != RolloverConstantsDefinition.RO_NOENROLMENTS)
            {
                Log.info(this.className, 'command', 'Updating campaign member to contact status: ' + contact.Rollover_Status__c);
                
                member.Status = contact.Rollover_Status__c;
                updates.add(member);
            }
        }
        
        if (updates.size() > 0)
            uow.registerDirty(updates);
        
        return new ActionResult(ResultStatus.SUCCESS);
    }
}