public class TimetableGetQuery
{
    public Interfaces.IViewModel query(Object param)
    {
        ViewModels.Param queryParam = (ViewModels.Param) param;
        ViewModelsTimetable.Data data = new ViewModelsTimetable.Data();

        Boolean isAuthenticated = !queryParam.IsAuthenticated || queryParam.Identifier == '' || queryParam.Identifier.contains('@') ? false : true;

        if (!isAuthenticated)
            return null;

        Contact contact = ContactService.get(queryParam.Identifier);

        data.Details = getDetails(contact);
        data.Courses = getCourses(contact);
        data.Subjects = getSubjects(contact);

        return data;
    }

    private ViewModelsTimetable.Details getDetails(Contact contact)
    {
        ViewModelsTimetable.Details details = new ViewModelsTimetable.Details();

        details.FirstName = contact.FirstName;
        details.LastName = contact.LastName;
        details.LastName = contact.LastName;
        details.FeeHelpEligible = true;
        details.FeesInCredit = 0.00;
        details.FeeHelpCredit = 0.00;
        details.StudyPersona = contact.StudyPersona__c;
        details.RecordType = contact.RecordType.Name;

        return details;
    }

    private List<ViewModelsTimetable.Course> getCourses(Contact contact)
    {
        List<ViewModelsTimetable.Course> result = new List<ViewModelsTimetable.Course>();
        List<hed__Program_Enrollment__c> courses =
        [
            SELECT Id, CoursePathway2__c, Status__c, hed__Account__c, hed__Program_Plan__c, hed__Start_Date__c
            FROM hed__Program_Enrollment__c
            WHERE hed__Contact__c = : contact.Id
        ];

        for (hed__Program_Enrollment__c course : courses)
        {
            ViewModelsTimetable.Course item = new ViewModelsTimetable.Course();

            item.CourseId = course.hed__Account__c;
            item.CoursePlanId = course.hed__Program_Plan__c;
            item.CoursePathway = course.CoursePathway2__c;
            item.Status = course.Status__c;
            item.Commenced = course.hed__Start_Date__c;

            result.add(item);
        }

        return result;
    }

    private List<ViewModelsTimetable.Subject> getSubjects(Contact contact)
    {
        List<ViewModelsTimetable.Subject> result = new List<ViewModelsTimetable.Subject>();
        List<hed__Course_Enrollment__c> subjects = 
        [
            SELECT Id, SubjectOfferingTerm__c, hed__Course_Offering__c, SubjectOfferingAdminDate__c, SubjectOfferingCensusDate__c,
                SubjectOfferingEndDate__c, SubjectOfferingStartDate__c, Grade__c, hed__Status__c, RecordTypeId, hed__Contact__c,
                hed__Course_Offering__r.hed__Term__c, hed__Course_Offering__r.hed__Term__r.hed__Grading_Period_Sequence__c,
                hed__Course_Offering__r.hed__Start_Date__c, hed__Course_Offering__r.hed__Course__r.SubjectDefinition__c,
                RecordType.Name, Type__c, hed__Program_Enrollment__c
            FROM hed__Course_Enrollment__c
            WHERE hed__Contact__c =: contact.Id
        ];

        for (hed__Course_Enrollment__c subject : subjects)
        {
            ViewModelsTimetable.Subject item = new ViewModelsTimetable.Subject();
            Date today = Date.today();
            Date startDate = subject.SubjectOfferingStartDate__c;

            item.Id = subject.Id;
            item.Grade = subject.Grade__c;
            item.Status = subject.hed__Status__c;
            item.Type = subject.Type__c;
            item.CanEnrol = subject.Type__c == 'Standard' && (contact.Portfolio__c.startsWith('International') || (today < startDate && startDate < today.addDays(224)));

            if (subject.Type__c != 'Break')
                item.SubjectId = subject.hed__Course_Offering__r.hed__Course__r.SubjectDefinition__c;

            if (subject.Type__c == 'Virtual')
                item.TermId = String.valueOf(subject.hed__Course_Offering__r.hed__Term__r.hed__Grading_Period_Sequence__c);
            else
                item.TermId = subject.hed__Course_Offering__r.hed__Term__c;

            if (item.CanEnrol)
            {
                item.Price = 2725.00;
                item.Discount = 0.00;
            }

            result.add(item);
        }

        return result;
    }
}