public class NVMCallSummarySetTaskLookupCommand extends BaseTriggerCommand
{
    protected override List<SObject> validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        List<NVMStatsSF__NVM_Call_Summary__c> result = new List<NVMStatsSF__NVM_Call_Summary__c>();

        for (NVMStatsSF__NVM_Call_Summary__c summary : (List<NVMStatsSF__NVM_Call_Summary__c>) items)
        {
            if ((triggerContext == Enums.TriggerContext.AFTER_INSERT && summary.NVMStatsSF__TaskID__c != null) ||
                (triggerContext == Enums.TriggerContext.AFTER_UPDATE && SObjectUtils.hasPropertyChanged(summary, oldMap.get(summary.Id), 'NVMStatsSF__TaskID__c')))
            {
                result.add(summary);
            }
        }

        return result;
    }

    public override ActionResult command(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        List<NVMStatsSF__NVM_Call_Summary__c> callSummaries = (List<NVMStatsSF__NVM_Call_Summary__c>) items;
        Map<Id, Task> taskMap = new Map<Id, Task>([SELECT Id, NVMCallSummary__c FROM Task WHERE Id IN :SObjectUtils.getIDs(callSummaries, 'NVMStatsSF__TaskID__c')]);

        for (NVMStatsSF__NVM_Call_Summary__c callSummary : callSummaries)
        {
            Task callSummaryTask = taskMap.get(callSummary.NVMStatsSF__TaskID__c);

            if (callSummaryTask != null && callSummaryTask.NVMCallSummary__c == null)
            {
                callSummaryTask.NVMCallSummary__c = callSummary.Id;
                uow.registerDirty(callSummaryTask);
            }
        }

        return new ActionResult(ResultStatus.SUCCESS);
    }
}