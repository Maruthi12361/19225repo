@isTest
public class DoNotCallContactCommandTest
{
    @testSetup
    static void setup()
    {
        // Arrange
        TestUtilities.setupHEDA();
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact prospectContact = TestDataContactMock.createProspectCold(user, '+61415036101');
        prospectContact.Phone = '+61293534846';
        prospectContact.MobilePhone = '+61417015944';
        prospectContact.HomePhone = '+61417015977';
        prospectContact.OtherPhone = '+61417015978';
        prospectContact.hed__WorkPhone__c = '+61417015999';
        prospectContact.dncau__Phone_DNC_Status__c = Constants.DNC_CAN_CALL_STATUS;
        prospectContact.dncau__Phone_DNC_Checked__c = DateTime.newInstance(2018, 02, 15, 12, 0, 0);
        prospectContact.dncau__MobilePhone_DNC_Status__c = Constants.DNC_DO_NOT_CALL_STATUS;
        prospectContact.dncau__MobilePhone_DNC_Checked__c = DateTime.newInstance(2018, 02, 15, 12, 0, 0);
        prospectContact.dncau__HomePhone_DNC_Status__c = Constants.DNC_CAN_CALL_STATUS;
        prospectContact.dncau__HomePhone_DNC_Checked__c = DateTime.newInstance(2018, 02, 15, 12, 0, 0);
        prospectContact.dncau__OtherPhone_DNC_Status__c = Constants.DNC_CAN_CALL_STATUS;
        prospectContact.dncau__OtherPhone_DNC_Checked__c = DateTime.newInstance(2018, 02, 15, 12, 0, 0);
        prospectContact.WorkPhone_DNC_Status__c = Constants.DNC_CAN_CALL_STATUS;
        prospectContact.WorkPhone_DNC_Checked__c = DateTime.newInstance(2018, 02, 15, 12, 0, 0);
        update prospectContact;

        Contact student = TestDataContactMock.createStudent(user);
        student.Phone = '+61295623332';
        student.MobilePhone = '+61295623333';
        student.HomePhone = '+61295623344';
        student.OtherPhone = '+61295623355';
        student.hed__WorkPhone__c = '+61295623366';
        student.dncau__Phone_DNC_Status__c = Constants.DNC_CAN_CALL_STATUS;
        student.dncau__Phone_DNC_Checked__c = DateTime.newInstance(2018, 02, 15, 12, 0, 0);
        student.dncau__MobilePhone_DNC_Status__c = Constants.DNC_DO_NOT_CALL_STATUS;
        student.dncau__MobilePhone_DNC_Checked__c = DateTime.newInstance(2018, 02, 15, 12, 0, 0);
        student.dncau__HomePhone_DNC_Status__c = Constants.DNC_CAN_CALL_STATUS;
        student.dncau__HomePhone_DNC_Checked__c = DateTime.newInstance(2018, 02, 15, 12, 0, 0);
        student.dncau__OtherPhone_DNC_Status__c = Constants.DNC_CAN_CALL_STATUS;
        student.dncau__OtherPhone_DNC_Checked__c = DateTime.newInstance(2018, 02, 15, 12, 0, 0);
        student.WorkPhone_DNC_Status__c = Constants.DNC_CAN_CALL_STATUS;
        student.WorkPhone_DNC_Checked__c = DateTime.newInstance(2018, 02, 15, 12, 0, 0);
        update student;
    }

    @isTest
    static void UpdateContactPhone_ResetDNCPhoneToNull()
    {
        // Arrange
        Contact prospect = [SELECT Id, FirstName, LastName, Phone, dncau__Phone_DNC_Status__c,dncau__Phone_DNC_Checked__c 
                            FROM Contact
                            WHERE RecordType.DeveloperName =: Constants.RECORDTYPE_CONTACT_PROSPECT LIMIT 1];
        // Act
        Test.startTest();
        prospect.Phone = '+61295623332';
        update prospect;
        Test.stopTest();

        // Assert
        Contact result = [SELECT Id, FirstName, LastName, Phone,dncau__Phone_DNC_Status__c,dncau__Phone_DNC_Checked__c 
                          FROM Contact
                          WHERE RecordType.DeveloperName =: Constants.RECORDTYPE_CONTACT_PROSPECT LIMIT 1];
        System.assertEquals(null, result.dncau__Phone_DNC_Status__c, 'Incorrect value in dncau__Phone_DNC_Status__c field');
        System.assertEquals(null, result.dncau__Phone_DNC_Checked__c, 'Incorrect value in dncau__Phone_DNC_Checked__c field');
    }

    @isTest
    static void UpdateContactMobile_ResetDNCMobileToNull()
    {
        // Arrange
        Contact student = [SELECT Id, FirstName, LastName, MobilePhone, dncau__MobilePhone_DNC_Status__c,dncau__MobilePhone_DNC_Checked__c 
                           FROM Contact
                           WHERE RecordType.DeveloperName =: Constants.RECORDTYPE_CONTACT_STUDENT LIMIT 1];
        // Act
        Test.startTest();
        student.MobilePhone = '+61497799315';
        update student;
        Test.stopTest();

        // Assert
        Contact result = [SELECT Id, FirstName, LastName, MobilePhone, dncau__MobilePhone_DNC_Status__c,dncau__MobilePhone_DNC_Checked__c 
                          FROM Contact
                          WHERE RecordType.DeveloperName =: Constants.RECORDTYPE_CONTACT_STUDENT LIMIT 1];
        System.assertEquals(null, result.dncau__MobilePhone_DNC_Status__c, 'Incorrect value in dncau__MobilePhone_DNC_Status__c field');
        System.assertEquals(null, result.dncau__MobilePhone_DNC_Checked__c, 'Incorrect value in dncau__MobilePhone_DNC_Checked__c field');
    }

    @isTest
    static void UpdateContactOtherPhone_ResetDNCOtherPhoneToNull()
    {
        // Arrange
        Contact prospect = [SELECT Id, FirstName, LastName, OtherPhone, dncau__OtherPhone_DNC_Status__c, dncau__OtherPhone_DNC_Checked__c 
                            FROM Contact
                            WHERE RecordType.DeveloperName =: Constants.RECORDTYPE_CONTACT_PROSPECT LIMIT 1];
        // Act
        Test.startTest();
        prospect.OtherPhone = '+61497799315';
        update prospect;
        Test.stopTest();

        // Assert
        Contact result = [SELECT Id, FirstName, LastName, OtherPhone, dncau__OtherPhone_DNC_Status__c, dncau__OtherPhone_DNC_Checked__c 
                          FROM Contact
                          WHERE RecordType.DeveloperName =: Constants.RECORDTYPE_CONTACT_PROSPECT LIMIT 1];
        System.assertEquals(null, result.dncau__OtherPhone_DNC_Status__c, 'Incorrect value in dncau__OtherPhone_DNC_Status__c field');
        System.assertEquals(null, result.dncau__OtherPhone_DNC_Checked__c, 'Incorrect value in dncau__OtherPhone_DNC_Checked__c field');
    }

    @isTest
    static void UpdateContactHomePhone_ResetDNCHomePhoneToNull()
    {
        // Arrange
        Contact prospect = [SELECT Id, FirstName, LastName, HomePhone, dncau__HomePhone_DNC_Status__c, dncau__HomePhone_DNC_Checked__c 
                            FROM Contact
                            WHERE RecordType.DeveloperName =: Constants.RECORDTYPE_CONTACT_PROSPECT LIMIT 1];
        // Act
        Test.startTest();
        prospect.HomePhone = '+61497799315';
        update prospect;
        Test.stopTest();

        // Assert
        Contact result = [SELECT Id, FirstName, LastName, HomePhone, dncau__HomePhone_DNC_Status__c, dncau__HomePhone_DNC_Checked__c 
                          FROM Contact
                          WHERE RecordType.DeveloperName =: Constants.RECORDTYPE_CONTACT_PROSPECT LIMIT 1];
        System.assertEquals(null, result.dncau__HomePhone_DNC_Status__c, 'Incorrect value in dncau__HomePhone_DNC_Status__c field');
        System.assertEquals(null, result.dncau__HomePhone_DNC_Checked__c, 'Incorrect value in dncau__HomePhone_DNC_Checked__c field');
    }

    @isTest
    static void UpdateContactWorkPhone_ResetDNCWorkPhoneToNull()
    {
        // Arrange
        Contact prospect = [SELECT Id, FirstName, LastName, hed__WorkPhone__c, WorkPhone_DNC_Status__c, WorkPhone_DNC_Checked__c 
                            FROM Contact
                            WHERE RecordType.DeveloperName =: Constants.RECORDTYPE_CONTACT_PROSPECT LIMIT 1];
        // Act
        Test.startTest();
        prospect.hed__WorkPhone__c = '+61497799315';
        update prospect;
        Test.stopTest();

        // Assert
        Contact result = [SELECT Id, FirstName, LastName, HomePhone, WorkPhone_DNC_Status__c, WorkPhone_DNC_Checked__c 
                          FROM Contact
                          WHERE RecordType.DeveloperName =: Constants.RECORDTYPE_CONTACT_PROSPECT LIMIT 1];
        System.assertEquals(null, result.WorkPhone_DNC_Status__c, 'Incorrect value in WorkPhone_DNC_Status__c field');
        System.assertEquals(null, result.WorkPhone_DNC_Checked__c, 'Incorrect value in WorkPhone_DNC_Checked__c field');
    }
}