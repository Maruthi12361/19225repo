public class SkillSetService
{
    public static ActionResult calculateNumberSkilledContacts(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        return new SkillSetCalculateContactsCommand().execute(items, oldMap, triggerContext);
    }
    
    public static ActionResult manageSkillSetUsersInGroups(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        return new SkillSetGroupUserManagementCommand().execute(items, oldMap, triggerContext);
    }
}