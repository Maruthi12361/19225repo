public abstract class BaseValidation extends BaseComponent
{
    public BaseValidation()
    {
        super();
    }
    
    protected abstract ActionResult validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext);
    
    public ActionResult execute(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {   
        ActionResult result = new ActionResult(ResultStatus.SUCCESS);

        if (items == null)
            throw new InvalidParameterValueException('items', 'Command execution was requested but was provided with no information to act on');

        if (oldMap == null && triggerContext != Enums.TriggerContext.BEFORE_INSERT && triggerContext != Enums.TriggerContext.AFTER_INSERT)
            throw new InvalidParameterValueException('oldMap', 'Command execution was requested but was provided with no old map of objects to act on');        
        
        try
        {
            Log.info(this.className, 'execute', 'Validating command.');
            result = validate(items, oldMap, triggerContext);
        }
        catch(Exception ex)
        {        
            exc = ex;
            Log.error(this.className, 'execute', 'Error validating command: ', ex);
            result = new ActionResult(ResultStatus.ERROR, 'Error validating command ' + this.className + ': ' + StringUtils.getExceptionMessage(ex));
        }

        if (!trigger.isExecuting && this.saveLogs)
            Log.save();
        
        if (exc != null && Test.isRunningTest() && TestUtilities.throwOnException)
            throw exc;

        return result;
    }
}