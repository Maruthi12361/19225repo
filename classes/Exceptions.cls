public class Exceptions 
{
    //
    // Exception class indicating a generic error has occurred in the application
    //
    public class ApplicationException extends Exception
    {
        
    }
    
    // 
    // Exception class indicating an error has occurred during invocation of a trigger
    //
    public class TriggerHandlerException extends Exception 
    {
        
    }
    
    //
    // Exception class indicating that invalid parameter has been passed to a method
    //
    public class InvalidParameterException extends Exception 
    {

    }

    //
    // Exception class indicating unsupported file extension
    //
    public class UnsupportedFileException extends Exception 
    {

    }

    //
    // Exception class indicating invalid file extension
    //
    public class InvalidFileTypeException extends Exception 
    {

    }

    //
    // Exception class indicating unauthorized request
    //
    public class UnauthorizedException extends Exception 
    {

    }
    
    //
    // Exception class indicating network unavailable
    //  
    public class NetworkException extends Exception
    {
        
    }
}