@isTest
public class AcademicConfigGetQueryTests
{
    @isTest
    static void RequestAcademicConfigWithTerms_ReturnsPayloadIncludingTerms()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        ViewModelsTestData.Data mockData = TestDataAcademicConfigMock.create(user);

        // Act
        test.startTest();
        ViewModelsAcademic.Data data = AcademicConfigService.get();
        test.stopTest();

        // Assert
        System.assertEquals(12, data.Terms.size(), 'All future School of Business terms should be returned by service');
        System.assertEquals(mockData.Terms[0].Id, data.Terms[0].Id, 'Term Id for record not returned by service');
        System.assertEquals(mockData.Terms[0].Name, data.Terms[0].Name, 'Term Name for record not returned by service');
        System.assertEquals(mockData.Terms[0].hed__Start_Date__c, data.Terms[0].StartDate, 'Term StartDate for record not returned by service');
        System.assertEquals(mockData.Terms[0].hed__End_Date__c, data.Terms[0].EndDate, 'Term EndDate for record not returned by service');
        System.assertEquals(mockData.Terms[0].EnrolmentCutoffDate__c, data.Terms[0].EnrolCutoffDate, 'Term EnrolmentCutoffDate for record not returned by service');
        System.assertEquals(mockData.Terms[0].hed__Account__c, data.Terms[0].DepartmentId, 'Term Department Id for record not returned by service');
        System.assert(data.Terms[0].EnrolCutoffDate != null, 'Term Enrolment Cutoff Date for record not returned by service');
        System.assert(data.Terms[0].Year != null, 'Term Year for record not returned by service');
        System.assert(data.Terms[0].TermNumber != null, 'Term Number for record not returned by service');

        for (ViewModelsAcademic.Term term : data.Terms)
        {
            System.assert(term.StartDate >= Datetime.now(), 'All terms returned by service should be in the future');
            System.assert(!term.Name.contains('Virtual'), 'No terms returned by service should be virtual terms');
        }
    }

    @isTest
    static void RequestAcademicConfigWithSubjects_ReturnsPayloadIncludingSubjects()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        ViewModelsTestData.Data mockData = TestDataAcademicConfigMock.create(user);

        // Act
        test.startTest();
        ViewModelsAcademic.Data data = AcademicConfigService.get();
        test.stopTest();

        // Assert
        System.assertEquals(28, data.Subjects.size(), 'All Subjects should returned by service');
        System.assertEquals(mockData.subject('9026BC').Id, data.Subjects[0].Id, 'Subject Id for record not returned by service');
        System.assertEquals(mockData.subject('9026BC').hed__Course_ID__c, data.Subjects[0].Code, 'Subject Code for record not returned by service');
        System.assertEquals(mockData.subject('9026BC').Name, data.Subjects[0].Name, 'Subject Name for record not returned by service');
        System.assertEquals(mockData.subject('9026BC').hed__Extended_Description__c, data.Subjects[0].Description, 'Subject Name for record not returned by service');

        for (ViewModelsAcademic.Subject subject : data.Subjects)
            System.assertNotEquals('BREAK', subject.Code, 'Subjects should not contain the Break subject');
    }

    @isTest
    static void RequestAcademicConfigWithPrerequisiteSubjects_ReturnsPayloadIncludingSubjectPrerequisites()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        ViewModelsTestData.Data mockData = TestDataAcademicConfigMock.create(user);

        // Act
        test.startTest();
        ViewModelsAcademic.Data data = AcademicConfigService.get();
        test.stopTest();

        // Assert
        for (ViewModelsAcademic.Subject subject: data.Subjects)
        {
            if (subject.Name == 'Leadership')
                System.assertEquals(0, subject.PrerequisiteSubjectIds.size(), 'Leadership should have no pre-requisites');

            if (subject.Name == 'Corporate Finance')
            {
                System.assertEquals(1, subject.PrerequisiteSubjectIds.size(), 'Corporate Finance should have one pre-requisite subject listed');
                System.assert(IdUtils.contains(subject.PrerequisiteSubjectIds, mockData.subject('8006FMGT').Id), 'Corporate Finances pre-requisite should be Financial Management');
            }

            if (subject.Name == 'Strategic Management')
            {
                System.assertEquals(4, subject.Prerequisites, 'Strategic Management should require 4 subjects to be completed');
                System.assertEquals(6, subject.PrerequisiteSubjectIds.size(), 'Strategic Management should have 6 pre-requisite subjects listed');
                System.assert(IdUtils.contains(subject.PrerequisiteSubjectIds, mockData.subject('8001LEAD').Id), 'Strategic Management pre-requisites should include Leadership');
                System.assert(IdUtils.contains(subject.PrerequisiteSubjectIds, mockData.subject('8002MMGT').Id), 'Strategic Management pre-requisites should include Marketing Management');
                System.assert(IdUtils.contains(subject.PrerequisiteSubjectIds, mockData.subject('8005CGOV').Id), 'Strategic Management pre-requisites should include Corporate Governance');
                System.assert(IdUtils.contains(subject.PrerequisiteSubjectIds, mockData.subject('8004OMGT').Id), 'Strategic Management pre-requisites should include Operations Management');
                System.assert(IdUtils.contains(subject.PrerequisiteSubjectIds, mockData.subject('8006FMGT').Id), 'Strategic Management pre-requisites should include Financial Management');
                System.assert(IdUtils.contains(subject.PrerequisiteSubjectIds, mockData.subject('8003SHRM').Id), 'Strategic Management pre-requisites should include Strategic Human Resource Management');
            }
        }
    }

    @isTest
    static void RequestAcademicConfigWithCourse_ReturnsPayloadIncludingCourse()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        ViewModelsTestData.Data mockData = TestDataAcademicConfigMock.create(user);

        // Act
        test.startTest();
        ViewModelsAcademic.Data data = AcademicConfigService.get();
        test.stopTest();

        // Assert
        Account mockGCM = mockData.course('GCM');
        hed__Program_Plan__c mockGCMv1 = mockData.coursePlan('GCM v1');
        hed__Plan_Requirement__c mockGCMCore = mockData.coursePlanReqt('GCM v1', 'Core');
        hed__Plan_Requirement__c mockGCMElec = mockData.coursePlanReqt('GCM v1', 'Electives');
        hed__Plan_Requirement__c mockMBACore = mockData.coursePlanReqt('MBA v1', 'Core');
        hed__Plan_Requirement__c mockLEAD = mockData.coursePlanReqtSubject('GCM v1', 'Core', '8001LEAD');
        hed__Plan_Requirement__c mockCGOV = mockData.coursePlanReqtSubject('GCM v1', 'Core', '8005CGOV');

        System.assertEquals(4, data.Courses.size(), 'All Courses should returned by service');

        ViewModelsAcademic.Course dataGCM = data.Courses[1];
        ViewModelsAcademic.Course dataMBA = data.Courses[2];
        System.assertEquals(mockGCM.Id, dataGCM.Id, 'Course Id for GCM record not returned by service');
        System.assertEquals(mockGCM.Name, dataGCM.Name, 'Course Name for GCM record not returned by service');
        System.assertEquals(mockGCM.ProductName__c, dataGCM.ProductName, 'Course Product Name for GCM record not returned by service');
        System.assertEquals(mockGCM.Program__c, dataGCM.Program, 'Course Program for GCM record not returned by service');
        System.assertEquals(mockGCM.ParentId, dataGCM.DepartmentId, 'Course Department Id for GCM record not returned by service');
        System.assertEquals(2, dataGCM.Plans.size(), 'GCM Course should have two Course Plans returned by service');
        System.assertEquals(1, dataMBA.Plans.size(), 'MBA Course should have one Course Plan returned by service');

        ViewModelsAcademic.CoursePlan dataGCMv1 = data.Courses[1].Plans[0];
        ViewModelsAcademic.CoursePlan dataMBAv1 = data.Courses[2].Plans[0];
        System.assertEquals(mockGCMv1.Id, dataGCMv1.Id, 'Course Plan Id for GCM v1 record not returned by service');
        System.assertEquals(mockGCMv1.Name, dataGCMv1.Name, 'Course Plan Name for GCM v1 record not returned by service');
        System.assertEquals(mockGCMv1.hed__Version__c, dataGCMv1.Version, 'Course Plan Version for GCM v1 record not returned by service');
        System.assertEquals(mockGCMv1.hed__Is_Primary__c, dataGCMv1.IsPrimary, 'Course Plan IsPrimary for GCM v1 record not returned by service');
        System.assertEquals(2, dataGCMv1.Requirements.size(), 'GCM Course should have two Course Plan Requirements returned by service');
        System.assertEquals(3, dataMBAv1.Requirements.size(), 'MBA Course should have three Course Plan Requirements returned by service');

        ViewModelsAcademic.PlanRequirement dataGCMCore = data.Courses[1].Plans[0].Requirements[0];
        ViewModelsAcademic.PlanRequirement dataGCMElec = data.Courses[1].Plans[0].Requirements[1];
        ViewModelsAcademic.PlanRequirement dataMBACore = data.Courses[2].Plans[0].Requirements[0];
        System.assertEquals(mockGCMCore.Name, dataGCMCore.Name, 'Course Plan Requirement Name for GCM v1 Core record not returned by service');
        System.assertEquals(mockGCMCore.hed__Sequence__c, dataGCMCore.Sequence, 'Course Plan Requirement Sequence for GCM v1 Core record not returned by service');
        System.assertEquals(mockGCMCore.NumberOfSubjects__c, dataGCMCore.NumberOfSubjects, 'Course Plan Requirement NumberOfSubjects for GCM v1 Core record not returned by service');
        System.assertEquals(mockGCMCore.NumberPassRequirements__c, dataGCMCore.NumberOfPassRequrements, 'Course Plan Requirement NumberOfPassRequrements for GCM v1 Core record not returned by service');
        System.assertEquals(mockGCMCore.NumberOfSubjects__c, dataGCMCore.Subjects.size(), 'GCM v1 Core should have two Subjects returned by service');
        System.assertEquals(mockGCMElec.NumberOfSubjects__c, dataGCMElec.Subjects.size(), 'GCM v1 Elective should have two Subjects returned by service');
        System.assertEquals(mockMBACore.NumberOfSubjects__c, dataMBACore.Subjects.size(), 'MBA v1 Core should have 7 Subjects returned by service');

        ViewModelsAcademic.PlanRequirementSubject dataSubject0 = data.Courses[1].Plans[0].Requirements[0].Subjects[0];
        ViewModelsAcademic.PlanRequirementSubject dataSubject1 = data.Courses[1].Plans[0].Requirements[0].Subjects[1];
        System.assertEquals(mockLEAD.hed__Course__c, dataSubject0.Id, 'Course Plan Requirement LEAD Subject Id for GCM v1 Core record not returned by service');
        System.assertEquals(mockLEAD.hed__Sequence__c, dataSubject0.Sequence, 'Course Plan Requirement LEAD Subject Sequence for GCM v1 Core record not returned by service');
        System.assertEquals(mockLEAD.hed__Sequence__c, dataSubject0.OptimalSequence, 'Course Plan Requirement LEAD Subject Optimal Sequence for GCM v1 Core record not returned by service');
        System.assertEquals(mockCGOV.hed__Course__c, dataSubject1.Id, 'Course Plan Requirement CGOV Subject Id for GCM v1 Core record not returned by service');
        System.assertEquals(mockCGOV.hed__Sequence__c, dataSubject1.Sequence, 'Course Plan Requirement CGOV Subject Sequence for GCM v1 Core record not returned by service');
    }

    @isTest
    static void RequestAcademicConfigWithSubjectOffering_ReturnsPayloadIncludingSubjectOffering()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        ViewModelsTestData.Data mockData = TestDataAcademicConfigMock.create(user);

        // Act
        test.startTest();
        ViewModelsAcademic.Data data = AcademicConfigService.get();
        test.stopTest();

        // Assert
        hed__Course__c subjectBreak = mockData.subject('BREAK');
        System.assert(data.SubjectOfferings.size() > 0, 'All future Subject Offerings for AIB School of Business Subjects should be returned by service');

        for (ViewModelsAcademic.SubjectOffering offering : data.SubjectOfferings)
            System.assert(offering.StartDate >= Datetime.now(), 'All offerings returned by service should be in the future');

        for (hed__Course_Offering__c subjecOffering: mockData.SubjectOfferings)
        {
            if (subjecOffering.Id == data.SubjectOfferings[0].Id)
            {
                System.assertNotEquals(null, data.SubjectOfferings[0].SubjectId, 'Subject Id for record not returned by service');
                System.assertEquals(subjecOffering.Id, data.SubjectOfferings[0].Id, 'Subject Offering Id for record not returned by service');
                System.assertEquals(subjecOffering.hed__Term__c, data.SubjectOfferings[0].TermId, 'Subject Offering TermId for record not returned by service');
                System.assertEquals(subjecOffering.hed__Start_Date__c, data.SubjectOfferings[0].StartDate, 'Subject Offering StartDate for record not returned by service');
                System.assertEquals(subjecOffering.hed__End_Date__c, data.SubjectOfferings[0].EndDate, 'Subject Offering EndDate for record not returned by service');
                System.assertEquals(subjecOffering.CensusDate__c, data.SubjectOfferings[0].CensusDate, 'Subject Offering CensusDate for record not returned by service');
                System.assertEquals(subjecOffering.AdministrationDate__c, data.SubjectOfferings[0].AdminDate, 'Subject Offering AdminDate for record not returned by service');
                return;
            }
        }

        for (ViewModelsAcademic.SubjectOffering subjecOffering: data.SubjectOfferings)
            System.assertNotEquals(subjectBreak.Id, subjecOffering.subjectId, 'Subject Offerings should not contain offerings for the Break subject');
    }
}