@isTest
public class InterestingMomentUpdateStatusCommTests
{
    @testSetup
    static void setup()
    {
        TestUtilities.setupHEDA();
    }

    @isTest
    static void OAFSubmitted_UpdatesOpportunityStageToVetting()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createProspectHot(user);
        Opportunity opportunity = TestDataOpportunityMock.createApplyingAcquisition(user, contact);
        InterestingMoment__c moment = TestDataInterestingMomentMock.create(user, contact, 'Form Completed', 'Online Application Form', 25);
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());

        // Act
        Test.startTest();
        ActionResult result = InterestingMomentService.updateOpportunityStatus(new List<InterestingMoment__c> { moment }, new Map<Id, InterestingMoment__c>(), Enums.TriggerContext.AFTER_INSERT);
        Test.stopTest();

        opportunity =
            [
                SELECT Id, StageName
                FROM Opportunity
                WHERE Id = : opportunity.Id
            ];

        // Assert
        System.assertEquals(true, result.isSuccess);
        System.assertEquals('Vetting', opportunity.StageName);
    }
}