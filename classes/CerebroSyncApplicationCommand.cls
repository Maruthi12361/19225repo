public class CerebroSyncApplicationCommand extends BaseQueueableCommand
{
    static Map<Id, Opportunity> activeRecordMap = new Map<Id, Opportunity>();
    static final String TEACHING_CENTRE_CODE = 'AIB-DL';
    static final String HEARING = 'Hearing';
    static final String LEARNING = 'Learning';
    static final String MOBILITY = 'Mobility';
    static final String VISION = 'Vision';
    static final String MEDICAL = 'Medical';
    static final String OTHER = 'Other';
    static final String INTEL = 'Intellectual';
    static final String MENTAL = 'Mental';
    static final String BRAIN = 'Brain injury';
    static final String NEURO = 'Neurological';
    static final String AUSTRALIA = 'Australia';

    static final List<String> requiredFields = new List<String> { 'FirstName', 'LastName', 'Email' };
    static final List<String> oppFields = new List<String>
    {
        'Has_Special_Needs__c', 'Special_Needs_Advice_Required__c', 'FEEHELP_Previous_Lastname__c', 'FEEHELP_Previous_Firstname__c', 'FEEHELP_Previous_Names__c',
        'Last_Year_of_Participation__c', 'Parent_1_Highest_Educational_Attainment__c', 'Parent_2_Highest_Educational_Attainment__c'
    };

    static final List<String> contactFields = new List<String>
    {
        'Salutation', 'OtherCountry', 'MailingCountry', 'Permanent_Home_Country__c', 'hed__Country_of_Origin__c', 'hed__Gender__c', 'Language_Spoken_at_Home__c', 'Aboriginal_or_Torres_Strait_Islander_Des__c',
        'High_School_Level__c', 'LastName', 'FirstName', 'MiddleName', 'HomePhone', 'MobilePhone', 'PersonalEmail__c', 'BirthDate', 'YearOfArrivalInAustralia__c', 'OtherStreet', 'OtherCity', 'OtherPostalcode', 'OtherState',
        'OtherAddressType__c', 'MailingStreet', 'MailingCity', 'MailingPostalcode', 'MailingState', 'PermanentAddressType__c', 'Permanent_Home_Address__c', 'Permanent_Home_Suburb_Town__c',
        'Permanent_Home_Postcode__c', 'Permanent_Home_State_Province_Region__c', 'High_School_State_Territory__c', 'CHESSN_Number__c', 'Impairments__c', 'hed__Dual_Citizenship__c', 'HighestQualificationName__c',
        'HighestQualificationLanguage__c', 'HighestQualificationInstitution__c', 'HighestQualificationCompletionYear__c', 'ApplicationCourse__c', 'YearOfLeavingHighSchool__c', 'Highest_Qualification__c',
        'hed__WorkEmail__c','hed__UniversityEmail__c','hed__AlternateEmail__c'
    };

    public CerebroSyncApplicationCommand()
    {
        super(50, System.Label.Command_CerebroSyncApplicationCommand);
    }

    public override List<SObject> validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        ID prospectRecordType;
        ID acquisitionRecordType;
        Set<Id> contactIds = new Set<Id> {};
        List<Opportunity> result = new List<Opportunity>();

        if (items.size() == 0)
            return result;

        // Contacts as Source
        if (items[0] instanceof Contact)
        {
            Log.info(this.className, 'validate', 'Originating source of sync request is Contact updates');

            prospectRecordType = RecordTypeUtils.getRecordTypeId(Constants.RECORDTYPE_CONTACT_PROSPECT, SObjectType.Contact);
            acquisitionRecordType = RecordTypeUtils.getRecordTypeId(Constants.RECORDTYPE_OPPORTUNITY_ACQUISITION, SObjectType.Opportunity);

            for (Contact newCont: (List<Contact>)items)
            {
                Contact oldCont = (Contact) oldMap.get(newCont.Id);

                // Skip non Prospect Contacts
                if (newCont.recordTypeId != prospectRecordType)
                {
                    Log.info(this.className, 'validate', 'Skipping Contact {0}. Contact is not a Prospect.', new List<String> { String.valueOf(newCont.Id) });
                    continue;
                }

                // Skip non Hot Contacts
                if (newCont.Status__c != Constants.STATUS_HOT)
                {
                    Log.info(this.className, 'validate', 'Skipping Contact {0}. Contact does not have a Hot status.', new List<String> { String.valueOf(newCont.Id) });
                    continue;
                }

                // Skip if required fields aren't completed
                Boolean missing = false;

                for (String fieldName: requiredFields)
                {
                    if (newCont.get(fieldName) == null || String.isBlank((String)newCont.get(fieldName)))
                    {
                        Log.info(this.className, 'validate', 'Skipping Contact {0}. Required field {1} contains no details.', new List<String> { String.valueOf(newCont.Id), fieldName });
                        missing = true;
                        continue;
                    }
                }

                if (missing)
                    continue;

                // Skip if no fields have changed
                if (!SObjectUtils.hasAnyPropertyChangedIgnoreLineBreak(newCont, oldCont, contactFields))
                {
                    Log.info(this.className, 'validate', 'Skipping Contact {0}. No fields have changed.', new List<String> { String.valueOf(newCont.Id) });
                    continue;
                }

                contactIds.add(newCont.Id);
            }

            Log.info(this.className, 'validate', String.valueOf(contactIds));

            if (contactIds.size() == 0)
                return result;

            List<Opportunity> allItems =
                [
                    SELECT Id, Contact__c, RecordTypeID, StageName
                    FROM Opportunity
                    WHERE Contact__c =: contactIds
                      AND RecordTypeId =: acquisitionRecordType
                      AND StageName IN ('Applying', 'Vetting', 'Enrolling')
                ];

            for (Opportunity newOpp: (List<Opportunity>) allItems)
            {
                if (activeRecordMap.containsKey(newOpp.Id))
                {
                    Log.info(this.className, 'validate', 'Skipping Opportunity {0}. Has already been processed.', new List<String> { String.valueOf(newOpp.Id) });
                    continue;
                }

                // Skip if not in appropriate state
                if (newOpp.StageName != 'Applying' && newOpp.StageName != 'Vetting' && newOpp.StageName != 'Enrolling')
                {
                    Log.info(this.className, 'validate', 'Skipping Opportunity {0}. Not in a stage needing sync with Cerebro.', new List<String> { String.valueOf(newOpp.Id) });
                    continue;
                }

                result.add(newOpp);
            }
        }

         // Opportunities as Source
        if (items[0] instanceof Opportunity)
        {
            Log.info(this.className, 'validate', 'Originating source of sync request is Opportunity updates');

            contactIds = SObjectUtils.getIDs(items, 'Contact__c');
            Id recordTypeId = RecordTypeUtils.getRecordTypeId(Constants.RECORDTYPE_OPPORTUNITY_ACQUISITION, SObjectType.Opportunity);
            Map<ID, SObject> contacts = new Map<Id, SObject>(ContactService.getMany(contactIds));

            for (Opportunity newOpp: (List<Opportunity>) items)
            {
                Opportunity oldOpp = (Opportunity) oldMap.get(newOpp.Id);
                Contact newCont = (Contact)contacts.get(newOpp.Contact__c);

                if (activeRecordMap.containsKey(newOpp.Id))
                {
                    Log.info(this.className, 'validate', 'Skipping Opportunity {0}. Has already been processed.', new List<String> { String.valueOf(newOpp.Id) });
                    continue;
                }

                // Skip Opportunities without valid contacts
                if (newCont == null)
                {
                    Log.info(this.className, 'validate', 'Skipping Opportunity {0}. Isnt associated with a valid Contact.', new List<String> { String.valueOf(newOpp.Id) });
                    continue;
                }

                // Skip non Acquisition opportunities
                if (newOpp.RecordTypeId != recordTypeId)
                {
                    Log.info(this.className, 'validate', 'Skipping Opportunity {0}. Not Aquisition record type.', new List<String> { String.valueOf(newOpp.Id) });
                    continue;
                }

                // Skip if not in appropriate state
                if (newOpp.StageName != 'Applying' && newOpp.StageName != 'Vetting' && newOpp.StageName != 'Enrolling')
                {
                    Log.info(this.className, 'validate', 'Skipping Opportunity {0}. Not in a stage needing sync with Cerebro.', new List<String> { String.valueOf(newOpp.Id) });
                    continue;
                }

                // Skip if required fields aren't completed
                Boolean missing = false;

                for (String fieldName: requiredFields)
                {
                    if (newCont.get(fieldName) == null || String.isBlank((String)newCont.get(fieldName)))
                    {
                        Log.info(this.className, 'validate', 'Skipping Opportunity {0}. Required field {1} on Contact contains no details.', new List<String> { String.valueOf(newOpp.Id), fieldName });
                        missing = true;
                        continue;
                    }
                }

                if (missing)
                    continue;

                // Skip if no fields have changed
                if (!SObjectUtils.hasAnyPropertyChangedIgnoreLineBreak(newOpp, oldOpp, oppFields))
                {
                    Log.info(this.className, 'validate', 'Skipping Opportunity {0}. No fields have changed.', new List<String> { String.valueOf(newOpp.Id) });
                    continue;
                }

                result.add(newOpp);
            }
        }

        Log.info(this.className, 'validate', 'Validated {0} Opportunities for processing.', new List<String> { String.valueOf(result.size()) });
        activeRecordMap.putAll(result);

        return result;
    }

    public override ActionResult command(List<SObject> items, Id jobId)
    {
        ActionResult result = null;
        ViewModelsCerebro.Application application;
        Set<ID> ids = SObjectUtils.getIDs(items, 'ID');

        List<Contact> contacts = getContactsWithOpportunities(ids);

        Log.info(this.className, 'command', 'Processing {0} contacts.', new List<String> { String.valueOf(contacts.size()) });


        for (Contact student: contacts)
        {
            for (Opportunity opp: student.Opportunities__r)
            {
                String payload = System.JSon.serialize(mapViewModel(opp, student), false);
                Log.info(this.className, 'command', 'Sending payload to Cerebro API: ' + payload);

                syncApplication(payload, student.OAF_Application_Id__c, student.Id);
            }
        }

        return new ActionResult();
    }

    private List<Contact> getContactsWithOpportunities(Set<ID> oppIds)
    {
        String sql = 'SELECT ID, OAF_Application_Id__c, Cerebro_Student_Id__c, Salutation, FirstName, LastName, MiddleName, BirthDate, hed__Gender__c, HomePhone, MobilePhone, PersonalEmail__c, ' +
                        'OtherStreet, OtherCity, OtherState, OtherPostalCode,OtherCountry, MailingStreet, MailingCity, MailingState, MailingPostalCode, Email, ' +
                        'MailingCountry, OtherAddressType__c, PermanentAddressType__c, Permanent_Home_Address__c, hed__WorkEmail__c, hed__UniversityEmail__c, hed__AlternateEmail__c, ' +
                        'Permanent_Home_Suburb_Town__c, Permanent_Home_Postcode__c, Permanent_Home_State_Province_Region__c, Permanent_Home_Country__c, hed__Dual_Citizenship__c, ' +
                        'CHESSN_Number__c, Language_Spoken_at_Home__c, Impairments__c, YearOfArrivalInAustralia__c, hed__Country_of_Origin__c, High_School_State_Territory__c, High_School_Level__c, ' +
                        'Aboriginal_or_Torres_Strait_Islander_Des__c, HighestQualificationName__c, HighestQualificationLanguage__c, HighestQualificationInstitution__c, ' +
                        'HighestQualificationCompletionYear__c, ApplicationCourse__c, YearOfLeavingHighSchool__c, Highest_Qualification__c, ' +
                        '(' +
                            'SELECT Id, Has_Special_Needs__c, Special_Needs_Advice_Required__c, Parent_Education_Options__c, ' +
                                'FEEHELP_Previous_Lastname__c, FEEHELP_Previous_Firstname__c, FEEHELP_Previous_Names__c, Last_Year_of_Participation__c, ' +
                                'Parent_1_Highest_Educational_Attainment__c, Parent_2_Highest_Educational_Attainment__c ' +
                            'FROM Opportunities__r ' +
                            'WHERE StageName IN (\'Applying\', \'Vetting\', \'Enrolling\') ' +
                        ') ' +
                    'FROM Contact ' +
                    'WHERE Id In (SELECT Contact__c FROM Opportunity WHERE Id IN :oppIds)';

        return Database.query(sql);
    }

    private ViewModelsCerebro.Application mapViewModel(Opportunity opp, Contact student)
    {
        ViewModelsCerebro.Application application = new ViewModelsCerebro.Application();

        // Application
        application.LeadID = student.Id;
        application.ContactID = student.Id;
        application.OpportunityID = opp.Id;
        application.ApplicationID = student.OAF_Application_Id__c == null ? null : Integer.valueOf(student.OAF_Application_Id__c);
        application.PrimaryDetails = new ViewModelsCerebro.PrimaryDetails();
        application.Qualification = new ViewModelsCerebro.Qualification();
        application.SpecialNeeds = new ViewModelsCerebro.SpecialNeeds();
        application.Statistics = new ViewModelsCerebro.Statistics();
        application.FeeHelp = new ViewModelsCerebro.FeeHelp();
        application.IsSalesForceData = true;
        application.Title = student.Salutation;
        application.CourseName =  Constants.GCM_COURSES.contains(student.ApplicationCourse__c) ? 'GCM' : student.ApplicationCourse__c;
        application.ResidenceCountry = student.OtherCountry;
        application.PostalCountry = student.MailingCountry;
        application.PermanentCountry = student.Permanent_Home_Country__c;
        application.CountryOfBirth = student.hed__Country_of_Origin__c;
        application.Gender = student.hed__Gender__c;
        application.LanguageSpokenAtHome = student.Language_Spoken_at_Home__c;
        application.AboriginalAndTorresStraitIslander = student.Aboriginal_or_Torres_Strait_Islander_Des__c;
        if (opp.Parent_Education_Options__c != null)
        {
            application.HigherEducationAttainedParentOne = opp.Parent_Education_Options__c;
            application.HigherEducationAttainedParentTwo = opp.Parent_Education_Options__c;
        }
        else
        {
            application.HigherEducationAttainedParentOne = opp.Parent_1_Highest_Educational_Attainment__c;
            application.HigherEducationAttainedParentTwo = opp.Parent_2_Highest_Educational_Attainment__c;
        }
        application.HighestQualification = student.Highest_Qualification__c;
        application.HighSchoolLevel = student.High_School_Level__c;
        application.TeachingCentreCode = TEACHING_CENTRE_CODE;

        // Primary Details
        application.PrimaryDetails.AIBStudentID = student.Cerebro_Student_ID__c;
        application.PrimaryDetails.LastName = student.LastName;
        application.PrimaryDetails.FirstName = student.FirstName;
        application.PrimaryDetails.OtherNames = student.MiddleName;
        application.PrimaryDetails.HomePhone = student.HomePhone;
        application.PrimaryDetails.MobilePhone = student.MobilePhone;
        application.PrimaryDetails.Email = (String.isBlank(student.PersonalEmail__c) ? student.Email : student.PersonalEmail__c);
        application.PrimaryDetails.OtherEmail = student.hed__WorkEmail__c;
        application.PrimaryDetails.OtherEmail2 = student.hed__UniversityEmail__c;
        application.PrimaryDetails.OtherEmail3 = student.hed__AlternateEmail__c;
        application.PrimaryDetails.DateOfBirth = student.BirthDate;
        application.PrimaryDetails.ArrivalDate = student.YearOfArrivalInAustralia__c == null ? 0: Integer.valueOf(student.YearOfArrivalInAustralia__c);

        // Addresses
        application.PrimaryDetails.PostalAddressLine1 = student.MailingStreet;
        application.PrimaryDetails.PostalSuburbTown  = student.MailingCity;
        application.PrimaryDetails.PostalPostcode = student.MailingPostalcode;
        application.PrimaryDetails.PostalState = student.MailingState;

        application.PrimaryDetails.AddressLine1 = student.OtherStreet;
        application.PrimaryDetails.SuburbTown = student.OtherCity;
        application.PrimaryDetails.Postcode = student.OtherPostalcode;
        application.PrimaryDetails.State = student.OtherState;
        application.PrimaryDetails.SamePostalAddressTypeID = (student.OtherAddressType__c == 'Same as Mailing Address' ? 2 : null);

        application.PrimaryDetails.PermanentHomeLocationAddressLine1 = student.Permanent_Home_Address__c;
        application.PrimaryDetails.PermanentHomeLocationSuburbTown   = student.Permanent_Home_Suburb_Town__c;
        application.PrimaryDetails.PermanentHomeLocationPostcode = student.Permanent_Home_Postcode__c;
        application.PrimaryDetails.PermanentHomeLocationState = student.Permanent_Home_State_Province_Region__c;

        if (student.PermanentAddressType__c == 'Same as Mailing Address')
        {
            if (student.OtherAddressType__c == 'Same as Mailing Address')
                application.PrimaryDetails.SamePermanentHomeAddressTypeID = 2;
            else
                application.PrimaryDetails.SamePermanentHomeAddressTypeID = 1;
        }
        else if (student.PermanentAddressType__c == 'Same as Other Address')
            application.PrimaryDetails.SamePermanentHomeAddressTypeID = 2;
        else
            application.PrimaryDetails.SamePermanentHomeAddressTypeID = null;

        // Fee Help
        application.FeeHelp.StateTerritory = student.High_School_State_Territory__c;
        application.FeeHelp.CHESSN = student.CHESSN_Number__c;
        application.FeeHelp.PreviousSurname = opp.FEEHELP_Previous_Lastname__c;
        application.FeeHelp.PreviousFirstName = opp.FEEHELP_Previous_Firstname__c;
        application.FeeHelp.PreviousOtherName = opp.FEEHELP_Previous_Names__c;

        // Statistics
        application.Statistics.YearOfCompletionHighestQulificaiton = opp.Last_Year_of_Participation__c == null ? 0 : Integer.valueOf(opp.Last_Year_of_Participation__c);

        // Special Needs
        application.SpecialNeeds.ReceiveAdvice = opp.Special_Needs_Advice_Required__c == 'Yes' ? true : false;
        if (opp.Has_Special_Needs__c == 'Yes')
        {
            application.SpecialNeeds.HasDisability = true;
            if (student.Impairments__c != null)
            {
               String[] options = student.Impairments__c.split(';');
                for(String option: options)
                {
                    if (option.equalsIgnoreCase(HEARING))
                        application.SpecialNeeds.Hearing = true;
                    else if (option.equalsIgnoreCase(LEARNING))
                        application.SpecialNeeds.Learning = true;
                    else if (option.equalsIgnoreCase(MOBILITY))
                        application.SpecialNeeds.Mobility = true;
                    else if (option.equalsIgnoreCase(VISION))
                        application.SpecialNeeds.Vision = true;
                    else if (option.equalsIgnoreCase(MEDICAL))
                        application.SpecialNeeds.Medical = true;
                    else if (option.equalsIgnoreCase(OTHER))
                        application.SpecialNeeds.Other = true;
                    else if (option.equalsIgnoreCase(INTEL))
                        application.SpecialNeeds.IntellectualDisability = true;
                    else if (option.equalsIgnoreCase(MENTAL))
                        application.SpecialNeeds.MentalHealth = true;
                    else if (option.equalsIgnoreCase(BRAIN))
                        application.SpecialNeeds.BrainInjury = true;
                    else if (option.equalsIgnoreCase(NEURO))
                        application.SpecialNeeds.NeurologicalCondition = true;
                }
            }
        }
        else
            application.SpecialNeeds.HasDisability = false;

        application.Qualification.QualificationName = student.HighestQualificationName__c;
        application.QualificationLanguage = student.HighestQualificationLanguage__c;
        application.Qualification.Institution = student.HighestQualificationInstitution__c;
        application.Qualification.DurationTo = DateTime.newInstanceGMT(Integer.valueOf(student.HighestQualificationCompletionYear__c), 1, 1);
        application.CitizenOfCountry = student.hed__Dual_Citizenship__c;

        if (student.YearOfLeavingHighSchool__c > 1900)
            application.YearLeftHighSchool = Integer.valueOf(student.YearOfLeavingHighSchool__c);

        return application;
    }

    @future(callout = true)
    private static void syncApplication(String payload, String applicationId, Id contactId)
    {
        Log.info('CerebroSyncApplicationCommand', 'syncApplication', 'Syncing application');

        String endpoint = SingleSignOnUtilities.getConfiguration().CerebroAPIEndpoint__c;
        String token = SingleSignOnUtilities.getApplicationAccessToken();

        if (token == null)
        {
            Log.error('CerebroSyncApplicationCommand', 'syncApplication', 'Error attempting to send. No Access Token provided to access Cerebro API');
        }

        endpoint += '/applications/save';

        Http http = new Http();
        HttpResponse response;
        HttpRequest request = new HttpRequest();
        request.setEndpoint(endpoint);
        request.setMethod('POST');
        request.setHeader('Authorization', 'Bearer ' + token);
        request.setHeader('Accept', 'application/json');
        request.setHeader('Content-Type', 'application/json');
        request.setHeader('Cache-Control', 'no-cache');
        request.setTimeout(120000);
        request.setBody(payload);
        response = http.send(request);

        Log.info('CerebroSyncApplicationCommand', 'syncApplication', 'Retrieving access token from Azure AD.', request, new String[] { 'Accept', 'Host', 'Cache-Control', 'Content-Type' });

        processResponse(response, contactId);
    }

    private static ActionResult processResponse(HttpResponse response, String contactId)
    {
        Boolean toUpdate = false;
        Boolean isSuccess = false;
        Integer status_code = response.getStatusCode();
        String body = response.getBody();
        Map<String, Object> responseBody = (Map<String, Object>)JSON.deserializeUntyped(body);

        String no_email = 'You tried to create student application, but didn\'t provide required \'Email\'';
        String same_email = 'There is an existing student who has the same \'Email\' you\'ve provided';
        String invalid_status = 'was not updated because application status doesn\'t allow it';
        String message = (String)responseBody.get('Message');

        Contact contact = [SELECT Id, OAF_Application_Id__c, Cerebro_Student_ID__c, CerebroSyncMessage__c FROM Contact WHERE Id=:contactId];

        if (contact.CerebroSyncMessage__c != message)
        {
            contact.CerebroSyncMessage__c = message;
            toUpdate = true;
        }

        // 201: Created
        if (status_code == 201)
        {
            Integer applicationId = (Integer)responseBody.get('ApplicationID');
            String cerebroStudentId = (String)responseBody.get('AIBStudentID');

            // update Contact with ApplicationID and AIBStudentID returned from the Response
            contact.OAF_Application_Id__c = String.valueOf(applicationId);
            contact.Cerebro_Student_ID__c = cerebroStudentId;
            toUpdate = true;
            isSuccess = true;

            Log.info('CerebroSyncApplicationCommand', 'processResponse', 'Successfully created new Application in Cerebro');
        }

        // 200: Updated
        if (status_code == 200)
        {
            isSuccess = true;
            Log.info('CerebroSyncApplicationCommand', 'processResponse', 'Successfully updated existing Application in Cerebro');
        }

        if (toUpdate)
            update contact;

        if (isSuccess)
            return new ActionResult(ResultStatus.SUCCESS);

        // Expected failure scenario
        if (message.equals(no_email) || message.equals(same_email) || message.contains(invalid_status))
        {
            Log.info('CerebroSyncApplicationCommand', 'processResponse', 'Received known error scenario from Cerebro: ' + message);
            return new ActionResult(ResultStatus.SUCCESS, message);
        }

        // Unexpected failure scenario
        Log.error('CerebroSyncApplicationCommand', 'processResponse', 'Received unexpected error scenario from Cerebro: ' + message);
        return new ActionResult(ResultStatus.ERROR, message);
    }
}