@IsTest
public class ContactRequestVerifyEmailCommandTest
{
    @testSetup 
    public static void setup()
    {
        TestUtilities.setupHEDA();
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact contact = TestDataContactMock.createProspectCold(user, '0455548889');
    }

    @isTest
    static void BlankIdentifier_EmptyIdentifierError()
    {
        // Arrange 
        String errorMessage = '';
        ViewModels.Param param = new ViewModels.Param();
        param.Identifier = '';
        param.Payload = null;

        // Act
        try
        {
           ActionResult result = new ContactRequestVerifyEmailCommand().command(param);
        }
        catch (Exception ex)
        {
           errorMessage = ex.getMessage();
        }

        // Assert
        System.assertEquals(String.format(Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Identifier' }), errorMessage, 'Unexpected error message');
    }

    @isTest
    static void NonExistantContact_MissingContactError()
    {
        // Arrange
        String errorMessage = '';
        ViewModels.Param param = new ViewModels.Param();
        param.Identifier = 'myemail@email.com';

        // Act
        try
        {
           ActionResult result = new ContactRequestVerifyEmailCommand().command(param);
        }
        catch (Exception ex)
        {
           errorMessage = ex.getMessage();
        }

        // Assert
        System.assert(errorMessage.contains('Contact does not exist'), 'Unexpected error message');
    }

    @isTest
    static void EmptyPayload_MissingPayloadError()
    {
        // Arrange
        String errorMessage = '';
        Contact contact = [SELECT Id, Email, EncryptedId__c FROM Contact LIMIT 1];
        ViewModels.Param param = new ViewModels.Param();
        param.Identifier = contact.EncryptedId__c;

        // Act
        try
        {
           ActionResult result = new ContactRequestVerifyEmailCommand().command(param);
        }
        catch (Exception ex)
        {
           errorMessage = ex.getMessage();
        }

        // Assert
        System.assert(errorMessage.contains('Missing parameter payload'), 'Unexpected error message');
    }

    @isTest
    static void NoTypeParameter_MissingParameterError()
    {
        // Arrange
        String errorMessage = '';
        Contact contact = [SELECT Id, Email, EncryptedId__c FROM Contact LIMIT 1];

        Map<String, Object> prospectMap = new Map<String, Object>
        {
            'SomeProperty' => ''
        };

        ViewModels.Param param = new ViewModels.Param();
        param.Identifier = contact.EncryptedId__c;
        param.Payload = JSON.serialize(prospectMap);

        // Act
        try
        {
           ActionResult result = new ContactRequestVerifyEmailCommand().command(param);
        }
        catch (Exception ex)
        {
           errorMessage = ex.getMessage();
        }

        // Assert
        System.assert(errorMessage.contains('Missing parameter payload'), 'Unexpected error message');
    }

    @isTest
    static void NoParameterTypeValue_MissingParameterError()
    {
        // Arrange
        String errorMessage = '';
        Contact contact = [SELECT Id, Email, EncryptedId__c FROM Contact LIMIT 1];

        Map<String, Object> prospectMap = new Map<String, Object>
        {
            'type' => ''
        };

        ViewModels.Param param = new ViewModels.Param();
        param.Identifier = contact.EncryptedId__c;
        param.Payload = JSON.serialize(prospectMap);

        // Act
        try
        {
           ActionResult result = new ContactRequestVerifyEmailCommand().command(param);
        }
        catch (Exception ex)
        {
           errorMessage = ex.getMessage();
        }

        // Assert
        System.assert(errorMessage.contains('Missing parameter type value'), 'Unexpected error message');
    }

    @isTest
    static void InvalidParameterTypeValue_InvalidParameterError()
    {
        // Arrange
        String errorMessage = '';
        Contact contact = [SELECT Id, Email, EncryptedId__c FROM Contact LIMIT 1];

        Map<String, Object> prospectMap = new Map<String, Object>
        {
            'type' => 'Test2019'
        };

        ViewModels.Param param = new ViewModels.Param();
        param.Identifier = contact.EncryptedId__c;
        param.Payload = JSON.serialize(prospectMap);

        // Act
        try
        {
           ActionResult result = new ContactRequestVerifyEmailCommand().command(param);
        }
        catch (Exception ex)
        {
           errorMessage = ex.getMessage();
        }

        // Assert
        System.assert(errorMessage.contains('Invalid parameter type value'), 'Unexpected error message');
    }

    @isTest
    static void PayloadContainsEndpoindParamWithNoValue_EndpointParameterError()
    {
        // Arrange
        String errorMessage = '';
        Contact contact = [SELECT Id, Email, EncryptedId__c FROM Contact LIMIT 1];

        Map<String, Object> prospectMap = new Map<String, Object>
        {
            'type' => 'email',
            'endpoint' => ''
        };

        ViewModels.Param param = new ViewModels.Param();
        param.Identifier = contact.EncryptedId__c;
        param.Payload = JSON.serialize(prospectMap);

        // Act
        try
        {
           ActionResult result = new ContactRequestVerifyEmailCommand().command(param);
        }
        catch (Exception ex)
        {
           errorMessage = ex.getMessage();
        }

        // Assert
        System.assert(errorMessage.contains('Endpoint parameter value cannot be null'), 'Unexpected error message');
    }

    @isTest
    static void VerifyEmailRequest_Success()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());

        String errorMessage = '';
        Contact contact = [SELECT Id, Email, EncryptedId__c FROM Contact LIMIT 1];

        Map<String, Object> prospectMap = new Map<String, Object>
        {
            'type' => 'email'
        };

        ViewModels.Param param = new ViewModels.Param();
        param.Identifier = contact.EncryptedId__c;
        param.Payload = JSON.serialize(prospectMap);

        // Act
        test.startTest();
        ActionResult result = new ContactRequestVerifyEmailCommand().command(param);
        test.stopTest();

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Unexpected result status value');
    }

    @isTest
    static void VerifyProspectRequest_Success()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        
        String errorMessage = '';
        Contact contact = [SELECT Id, Email, EncryptedId__c FROM Contact LIMIT 1];

        Map<String, Object> prospectMap = new Map<String, Object>
        {
            'type' => 'prospect',
            'endpoint' => 'reserve-your-spot'
        };

        ViewModels.Param param = new ViewModels.Param();
        param.Identifier = contact.EncryptedId__c;
        param.Payload = JSON.serialize(prospectMap);

        // Act
        test.startTest();
        ActionResult result = new ContactRequestVerifyEmailCommand().command(param);
        test.stopTest();

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Unexpected result status value');
    }
}