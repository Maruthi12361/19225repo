public class ReferralService 
{
    public static ActionResult save(SObject item)
    {
        return new ReferralSaveCommand().execute(item);
    }

    public static ActionResult sendNewReferralEmails(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        return new ReferralSendEmailsCommand().execute(items, oldMap, triggerContext);
    }
}