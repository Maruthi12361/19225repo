public class CaseEnrolmentDeleteValidation extends BaseValidation 
{
    public override ActionResult validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        Set<Id> caseIds;

        if (triggerContext == Enums.TriggerContext.AFTER_DELETE)
        {
            caseIds = new Set<Id>();

            for (CaseEnrolment__c item : (List<CaseEnrolment__c>)items)
            {
                caseIds.add(item.Case__c);            
            }

            Map<Id, Case> caseMap = new Map<Id, Case>([SELECT IsClosed FROM Case WHERE Id IN : caseIds]);

            for (CaseEnrolment__c item : (List<CaseEnrolment__c>)items)
            {
                Case c = caseMap.get(item.Case__c);

                if (c != null && c.IsClosed)
                    item.addError('You cannot delete Case Enrolments if their parent Cases are closed.');
            }
        }

        return new ActionResult(ResultStatus.SUCCESS);
    }
}