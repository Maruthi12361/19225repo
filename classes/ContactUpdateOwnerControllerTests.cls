@isTest
public class ContactUpdateOwnerControllerTests
{
    @isTest
    static void PassStandardProspectToController_UpdateContactOwnerWithRoundRobinUser()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact prospect = TestDataContactMock.createProspectHot(user);
        Apexpages.StandardSetController sc = new Apexpages.StandardSetController(new List<Contact>());
        sc.setSelected([SELECT Id FROM Contact]);
        ContactUpdateOwnerController ext = new ContactUpdateOwnerController(sc);

        // Act
        Test.startTest();
        ext.updateOwnerToRoundRobin();
        Test.stopTest();

        // Assert
        Contact updatedContact = [SELECT Id, OwnerId, Owner.Name FROM Contact LIMIT 1];
        System.assertEquals(Constants.USERID_ROUNDROBIN, updatedContact.OwnerId);
    }

    @isTest
    static void PassDisqualifiedProspectToController_DoesntUpdateContactOwnerWithRoundRobinUser()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact prospect = TestDataContactMock.createProspectHot(user);
        prospect.Status__c = Constants.STATUS_DISQUALIFIED;
        update prospect;
        Apexpages.StandardSetController sc = new Apexpages.StandardSetController(new List<Contact>());
        sc.setSelected([SELECT Id FROM Contact]);
        ContactUpdateOwnerController ext = new ContactUpdateOwnerController(sc);

        // Act
        Test.startTest();
        ext.updateOwnerToRoundRobin();
        Test.stopTest();

        // Assert
        Contact notUpdatedContact = [SELECT Id, OwnerId, Owner.Name FROM Contact LIMIT 1];
        System.assertNotEquals(Constants.USERID_ROUNDROBIN, notUpdatedContact.OwnerId);
    }

    @isTest
    static void PassStandardProspectToControllerAsNonTeamLeader_DoesntUpdateContactOwnerWithRoundRobinUser()
    {
        // Arrange
        TestUtilities.disableComponent('CerebroSyncApplicationCommand');
        User nonTeamLeaderUser = UserService.getByAlias(Constants.USER_COURSEADVISOR);
        Contact prospect = TestDataContactMock.createProspectHot(nonTeamLeaderUser);
        Apexpages.StandardSetController sc = new Apexpages.StandardSetController(new List<Contact>());
        sc.setSelected([SELECT Id FROM Contact]);
        ContactUpdateOwnerController ext = new ContactUpdateOwnerController(sc);

        // Act
        Test.startTest();
        System.runAs(nonTeamLeaderUser)
        {
            ext.updateOwnerToRoundRobin();
        }
        Test.stopTest();

        // Assert
        Contact notUpdatedContact = [SELECT Id, OwnerId, Owner.Name FROM Contact LIMIT 1];
        System.assertNotEquals(Constants.USERID_ROUNDROBIN, notUpdatedContact.OwnerId);
    }
}