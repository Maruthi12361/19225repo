public class TestDataPlanRequirementMock 
{
    private static Map<String, hed__Plan_Requirement__c> coursePlanMap = new Map<String, hed__Plan_Requirement__c>();
    private static Map<String, hed__Plan_Requirement__c> subjectMap = new Map<String, hed__Plan_Requirement__c>();
    private static Map<String, hed__Plan_Requirement__c> dependencyMap = new Map<String, hed__Plan_Requirement__c>();

    public static hed__Plan_Requirement__c createCoursePlanRequirement(User user, String name, Id coursePlanId)
    {
        return createCoursePlanRequirement(user, name, coursePlanId, 0, 0, 0);
    }

    public static hed__Plan_Requirement__c createCoursePlanRequirement(User user, String name, Id coursePlanId, Integer sequence, Integer numberOfSubjects, Integer numberOfPassRequirements)
    {
        return createCoursePlanRequirements(user, new List<ViewModelsTestData.CoursePlanRequirement> { new ViewModelsTestData.CoursePlanRequirement(name, coursePlanId, sequence, numberOfSubjects, numberOfPassRequirements) })[0];
    }

    public static List<hed__Plan_Requirement__c> createCoursePlanRequirements(User user, List<ViewModelsTestData.CoursePlanRequirement> requirements)
    {
        List<hed__Plan_Requirement__c> result = new List<hed__Plan_Requirement__c>();
        List<hed__Plan_Requirement__c> inserts = new List<hed__Plan_Requirement__c>();

        for (ViewModelsTestData.CoursePlanRequirement requirement: requirements)
        {
            String key = requirement.Name + requirement.CoursePlanId;

            if (coursePlanMap.containsKey(key))
            {
                result.add(coursePlanMap.get(key));
                continue;
            }

            if (coursePlanMap.size() == 0)
            {
                List<hed__Plan_Requirement__c> items =
                    [
                        SELECT Name, RecordTypeId, hed__Program_Plan__c, NumberOfSubjects__c, NumberPassRequirements__c
                        FROM hed__Plan_Requirement__c
                    ];

                for (hed__Plan_Requirement__c item: items)
                    coursePlanMap.put(item.Name + item.hed__Program_Plan__c, item);
            }

            if (coursePlanMap.containsKey(key))
            {
                result.add(coursePlanMap.get(key));
                continue;
            }

            hed__Plan_Requirement__c newItem = new hed__Plan_Requirement__c
            (
                Name = requirement.Name,
                RecordTypeId = RecordTypeUtils.getRecordTypeId(Constants.RECORDTYPE_PLANREQUIREMENT_COURSEPLANREQUIREMENT, SObjectType.hed__Plan_Requirement__c),
                hed__Program_Plan__c = requirement.CoursePlanId,
                NumberOfSubjects__c = requirement.NumberOfSubjects,
                NumberPassRequirements__c = requirement.NumberOfPassRequirements,
                hed__Sequence__c = requirement.Sequence
            );

            coursePlanMap.put(key, newItem);
            result.add(newItem);
            inserts.add(newItem);
        }

        if (inserts.size() > 0)
        {
            if (user  != null)
            {
                System.RunAs(user)
                {
                    insert inserts;
                }
            }
            else
            {
                insert inserts;
            }
        }

        return result;
    }

    public static hed__Plan_Requirement__c createSubjectRequirement(User user, String name, Id subjectId)
    {
        return createSubjectRequirements(user, new List<ViewModelsTestData.SubjectRequirement> { new ViewModelsTestData.SubjectRequirement(name, subjectId) })[0];
    }

    public static List<hed__Plan_Requirement__c> createSubjectRequirements(User user, List<ViewModelsTestData.SubjectRequirement> subjectRequirements)
    {
        List<hed__Plan_Requirement__c> result = new List<hed__Plan_Requirement__c>();
        List<hed__Plan_Requirement__c> inserts = new List<hed__Plan_Requirement__c>();

        for (ViewModelsTestData.SubjectRequirement subjectRequirement: subjectRequirements)
        {
            String key = subjectRequirement.Name + subjectRequirement.SubjectId;

            if (subjectMap.containsKey(key))
            {
                result.add(subjectMap.get(key));
                continue;
            }

            if (subjectMap.size() == 0)
            {
                List<hed__Plan_Requirement__c> items =
                    [
                        SELECT Name, RecordTypeId, hed__Course__c, NumberPassRequirements__c
                        FROM hed__Plan_Requirement__c
                    ];

                for (hed__Plan_Requirement__c item: items)
                    subjectMap.put(item.Name + item.hed__Course__c, item);
            }

            if (subjectMap.containsKey(key))
            {
                result.add(subjectMap.get(key));
                continue;
            }

            hed__Plan_Requirement__c newItem = new hed__Plan_Requirement__c
            (
                Name = subjectRequirement.Name,
                RecordTypeId = RecordTypeUtils.getRecordTypeId(Constants.RECORDTYPE_PLANREQUIREMENT_SUBJECTREQUIREMENT, SObjectType.hed__Plan_Requirement__c),
                hed__Course__c = subjectRequirement.SubjectId,
                NumberPassRequirements__c = subjectRequirement.NumberOfPassRequirements
            );

            subjectMap.put(key, newItem);
            result.add(newItem);
            inserts.add(newItem);
        }

        if (inserts.size() > 0)
        {
            if (user  != null)
            {
                System.RunAs(user)
                {
                    insert inserts;
                }
            }
            else
            {
                insert inserts;
            }
        }

        return result;
    }

    public static hed__Plan_Requirement__c createDependencyItem(User user, String name, Id parentId, Id subjectId)
    {
        return createDependencyItem(user, name, parentId, subjectId, null);
    }

    public static hed__Plan_Requirement__c createDependencyItem(User user, String name, Id parentId, Id subjectId, Integer sequence)
    {
        return createDependencyItems(user, new List<ViewModelsTestData.DependencyItem> { new ViewModelsTestData.DependencyItem ( name, parentId, subjectId, sequence ) })[0];
    }

    public static List<hed__Plan_Requirement__c> createDependencyItems(User user, List<ViewModelsTestData.DependencyItem> dependencyItems)
    {
        List<hed__Plan_Requirement__c> inserts = new List<hed__Plan_Requirement__c>();
        List<hed__Plan_Requirement__c> result = new List<hed__Plan_Requirement__c>();

        for (ViewModelsTestData.DependencyItem dependencyItem: dependencyItems)
        {
            String key = dependencyItem.Name +  dependencyItem.RequirementId + dependencyItem.SubjectId;

            if (dependencyMap.containsKey(key))
            {
                result.add(dependencyMap.get(key));
                continue;
            }

            if (dependencyMap.size() == 0)
            {
                List<hed__Plan_Requirement__c> items =
                    [
                        SELECT Name, RecordTypeId, hed__Plan_Requirement__c, hed__Course__c, hed__Sequence__c, OptimalSequence__c, ConcurrentStudy__c
                        FROM hed__Plan_Requirement__c
                    ];

                for (hed__Plan_Requirement__c item: items)
                    dependencyMap.put(item.Name + item.hed__Plan_Requirement__c + item.hed__Course__c, item);
            }

            if (dependencyMap.containsKey(key))
            {
                result.add(dependencyMap.get(key));
                continue;
            }

            hed__Plan_Requirement__c newItem = new hed__Plan_Requirement__c
            (
                Name = dependencyItem.Name,
                RecordTypeId = RecordTypeUtils.getRecordTypeId(Constants.RECORDTYPE_PLANREQUIREMENT_DEPENDENCYITEM, SObjectType.hed__Plan_Requirement__c),
                hed__Plan_Requirement__c = dependencyItem.RequirementId,
                hed__Course__c = dependencyItem.SubjectId,
                hed__Sequence__c = dependencyItem.Sequence,
                OptimalSequence__c = dependencyItem.OptimalSequence,
                ConcurrentStudy__c = dependencyItem.ConcurrentStudy
            );

            dependencyMap.put(key, newItem);
            inserts.add(newItem);
            result.add(newItem);
        }

        if (inserts.size() > 0)
        {
            if (user  != null)
            {
                System.RunAs(user)
                {
                    insert inserts;
                }
            }
            else
            {
                insert inserts;
            }
        }

        return result;
    }
}