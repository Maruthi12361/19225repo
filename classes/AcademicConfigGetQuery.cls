public class AcademicConfigGetQuery extends BaseSingleViewModelQuery
{
    public override Interfaces.IViewModel query()
    {
        List<Object> obj = new List<Object>();
        ViewModelsAcademic.Data academicConfig = new ViewModelsAcademic.Data();

        academicConfig.Terms = getTerms();
        academicConfig.Subjects = getSubjects();
        academicConfig.Courses = getCourses();
        academicConfig.SubjectOfferings = getSubjectOfferings();

        return academicConfig;
    }

    private List<ViewModelsAcademic.Term> getTerms()
    {
        ViewModelsAcademic.Term term;
        List<ViewModelsAcademic.Term> terms = new List<ViewModelsAcademic.Term>();
        List<hed__Term__c> items =
            [
                SELECT Id, Name, hed__Start_Date__c, hed__End_Date__c, EnrolmentCutoffDate__c,
                    hed__Account__c, hed__Account__r.Name, Year__c, hed__Grading_Period_Sequence__c
                FROM hed__Term__c
                WHERE hed__Start_Date__c >= TODAY
                    AND hed__Account__r.Name =: Constants.ACCOUNT_DEPARTMENT_AIBBUSINESS
                ORDER BY hed__Start_Date__c ASC
            ];

        for (hed__Term__c item : items)
        {
            term = new ViewModelsAcademic.Term();

            term.Id = item.Id;
            term.Name = item.Name;
            term.StartDate = item.hed__Start_Date__c;
            term.EndDate = item.hed__End_Date__c;
            term.DepartmentId = item.hed__Account__c;
            term.EnrolCutoffDate = item.EnrolmentCutoffDate__c;
            term.Year = Integer.valueOf(item.Year__c);
            term.TermNumber = (Integer) item.hed__Grading_Period_Sequence__c;

            terms.add(term);
        }

        return terms;
    }

    public List<ViewModelsAcademic.Subject> getSubjects()
    {
        ViewModelsAcademic.Subject subject = new ViewModelsAcademic.Subject();
        List<ViewModelsAcademic.Subject> subjects = new List<ViewModelsAcademic.Subject>();

        List<hed__Course__c> itemSubjects =
            [
                SELECT Id, hed__Course_ID__c, Name, hed__Extended_Description__c
                FROM hed__Course__c
                WHERE RecordType.DeveloperName = 'SubjectDefinition'
                    AND hed__Course_ID__c !=: Constants.SUBJECT_COURSEID_BREAK
                ORDER BY Name ASC
            ];

        List<hed__Plan_Requirement__c> itemPrereqs =
            [
                SELECT Id, hed__Course__r.Id, hed__Course__r.Name, hed__Plan_Requirement__r.hed__Course__r.Id, hed__Plan_Requirement__r.NumberPassRequirements__c
                FROM hed__Plan_Requirement__c
                WHERE RecordType.DeveloperName = 'DependencyItem'
                    AND hed__Plan_Requirement__r.RecordType.DeveloperName = 'SubjectRequirement'
            ];

        for (hed__Course__c item : itemSubjects)
        {
            List<Id> subjectIds = new List<Id>();
            subject = new ViewModelsAcademic.Subject();

            // Subject Prerequisites
            for (hed__Plan_Requirement__c itemPrereq: itemPrereqs)
            {
                if (itemPrereq.hed__Plan_Requirement__r.hed__Course__r.Id == item.Id)
                {
                    subjectIds.add(itemPrereq.hed__Course__r.Id);

                    if (subject.Prerequisites == null)
                        subject.Prerequisites = (Integer) itemPrereq.hed__Plan_Requirement__r.NumberPassRequirements__c;
                }
            }

            subject.Id = item.Id;
            subject.Code = item.hed__Course_ID__c;
            subject.Name = item.Name;
            subject.Description = item.hed__Extended_Description__c;
            subject.PrerequisiteSubjectIds = subjectIds;

            subjects.add(subject);
        }

        return subjects;
    }

    public List<ViewModelsAcademic.Course> getCourses()
    {
        ViewModelsAcademic.Course course;
        List<ViewModelsAcademic.Course> courses = new List<ViewModelsAcademic.Course>();

        List<Account> itemCourses =
            [
                SELECT Id, Name, ProductName__c, Program__c, CourseOfStudyCode__c, ParentId
                FROM Account
                WHERE RecordType.DeveloperName = 'Academic_Program'
                ORDER BY Name ASC
            ];

        List<hed__Program_Plan__c> itemPlans =
            [
                SELECT hed__Account__c, Name, hed__Version__c, hed__Is_Primary__c
                FROM hed__Program_Plan__c
                ORDER BY hed__Version__c
            ];

        List<hed__Plan_Requirement__c> itemReqts =
            [
                SELECT Id, Name, hed__Course__c, NumberPassRequirements__c, NumberOfSubjects__c, hed__Program_Plan__c, hed__Sequence__c
                FROM hed__Plan_Requirement__c
                WHERE RecordType.DeveloperName = 'CoursePlanRequirement'
                ORDER BY hed__Sequence__c
            ];

        List<hed__Plan_Requirement__c> itemDeps =
            [
                SELECT Id, hed__Course__c, hed__Plan_Requirement__c, hed__Sequence__c, OptimalSequence__c, ConcurrentStudy__c
                FROM hed__Plan_Requirement__c
                WHERE RecordType.DeveloperName = 'DependencyItem'
                ORDER BY OptimalSequence__c
            ];


        for (Account itemCourse : itemCourses)
        {
            course = new ViewModelsAcademic.Course();

            course.Id = itemCourse.Id;
            course.Name = itemCourse.Name;
            course.ProductName = itemCourse.ProductName__c;
            course.Program = itemCourse.Program__c;
            course.DepartmentId = itemCourse.ParentId;
            course.Plans = new List<ViewModelsAcademic.CoursePlan>();

            for (hed__Program_Plan__c itemPlan : itemPlans)
            {
                if (itemPlan.hed__Account__c == course.Id)
                {
                    ViewModelsAcademic.CoursePlan coursePlan = new ViewModelsAcademic.CoursePlan();
                    coursePlan.Id = itemPlan.Id;
                    coursePlan.Name = itemPlan.Name;
                    coursePlan.Version = itemPlan.hed__Version__c;
                    coursePlan.IsPrimary = itemPlan.hed__Is_Primary__c;
                    coursePlan.Requirements = new List<ViewModelsAcademic.PlanRequirement>();

                    for (hed__Plan_Requirement__c itemReqt : itemReqts)
                    {
                        if (itemReqt.hed__Program_Plan__c == coursePlan.Id)
                        {
                            ViewModelsAcademic.PlanRequirement requirement = new ViewModelsAcademic.PlanRequirement();
                            requirement.Name = itemReqt.Name;
                            requirement.Sequence = itemReqt.hed__Sequence__c;
                            requirement.NumberOfSubjects = (Integer) itemReqt.NumberOfSubjects__c;
                            requirement.NumberOfPassRequrements = (Integer) itemReqt.NumberPassRequirements__c;
                            requirement.Subjects = new List<ViewModelsAcademic.PlanRequirementSubject>();

                            for (hed__Plan_Requirement__c itemDep : itemDeps)
                            {
                                if (itemDep.hed__Plan_Requirement__c == itemReqt.Id)
                                {
                                    ViewModelsAcademic.PlanRequirementSubject subject = new ViewModelsAcademic.PlanRequirementSubject();
                                    subject.Id = itemDep.hed__Course__c;
                                    subject.Sequence = (Integer) itemDep.hed__Sequence__c;
                                    subject.OptimalSequence = (Integer) itemDep.OptimalSequence__c;
                                    subject.ConcurrentStudy = itemDep.ConcurrentStudy__c;
                                    requirement.Subjects.add(subject);
                                }
                            }

                            coursePlan.Requirements.add(requirement);
                        }
                    }

                    course.Plans.add(coursePlan);
                }
            }

            courses.add(course);
        }

        return courses;
    }

    public List<ViewModelsAcademic.SubjectOffering> getSubjectOfferings()
    {
        ViewModelsAcademic.SubjectOffering offering = new ViewModelsAcademic.SubjectOffering ();
        List<ViewModelsAcademic.SubjectOffering> offerings = new List<ViewModelsAcademic.SubjectOffering>();

        List<hed__Course_Offering__c> items =
            [
                SELECT Id, hed__Term__c, hed__Course__c, hed__Course__r.SubjectDefinition__r.Id, hed__Course__r.SubjectDefinition__r.Name, 
                    hed__Start_Date__c, hed__End_Date__c, CensusDate__c, AdministrationDate__c
                FROM hed__Course_Offering__c
                WHERE hed__Start_Date__c > TODAY
                    AND Type__c =: Constants.SUBJECTOFFERING_TYPE_STANDARD
                ORDER BY hed__Start_Date__c ASC
            ];

        for (hed__Course_Offering__c item : items)
        {
            offering = new ViewModelsAcademic.SubjectOffering();

            offering.Id = item.Id;
            offering.TermId = item.hed__Term__c;
            offering.SubjectId = item.hed__Course__r.SubjectDefinition__r.Id;
            offering.SubjectName = item.hed__Course__r.SubjectDefinition__r.Name;
            offering.StartDate = item.hed__Start_Date__c;
            offering.EndDate = item.hed__End_Date__c;
            offering.CensusDate = item.CensusDate__c;
            offering.AdminDate = item.AdministrationDate__c;

            offerings.add(offering);
        }

        return offerings;
    }
}