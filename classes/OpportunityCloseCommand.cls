public class OpportunityCloseCommand extends BaseCommand
{
    private final static String STAGE_COMMENCING = 'Commencing';
    private final static String STAGE_UNCHANGED = 'Unchanged';
    private final static String STAGE_WON = 'Closed Won';
    private final static String STAGE_LOST = 'Closed Lost';

    public override List<SObject> validate(List<SObject> params)
    {
        List<Opportunity> oppToUpdate = new List<Opportunity>();

        for (Opportunity o : (List<Opportunity>) params)
        {
            if (o.StageName == STAGE_COMMENCING)
                oppToUpdate.add(o);
        }

        return oppToUpdate;
    }

    public override ActionResult command(List<SObject> params)
    {
        List<Opportunity> opps = (List<Opportunity>) params;
        Map<Id, String> result = getStage(opps);

        for (Opportunity o : opps)
        {
            String stageOutcome = result.get(o.Contact__c);

            if (stageOutcome == null || stageOutcome == STAGE_UNCHANGED)
                continue; //Either no data or stage doesn't need to be changed

            if (stageOutcome == STAGE_LOST)
                o.Lost_Reason__c = 'Withdrawn Before Census';

            o.stageName = stageOutcome;
            uow.registerDirty(o);

            Log.info(this.className, 'command', 'Changing opportunity {0} stage to {1}', new List<String>{o.Id, stageOutcome});
        }

        return new ActionResult(ResultStatus.SUCCESS);
    }

    private Map<Id, String> getStage(List<Opportunity> opportunities)
    {
        Set<Id> contactIds = getContactIds(opportunities);
        Map<Id, String> result = new Map<Id, String>();
        Map<Id, List<hed__Program_Enrollment__c>> enrolmentMap = getEnrolments(contactIds);

        for (Id contactId : contactIds)
        {
            boolean hasActiveCourses = false;
            boolean hasInactiveCourses = false;
            List<hed__Program_Enrollment__c> courseEnrolments = enrolmentMap.get(contactId);

            if (courseEnrolments == NULL)
                continue;

            for (hed__Program_Enrollment__c ce : courseEnrolments)
            {
                if (result.containsKey(contactId))
                    continue;

                if (ce.Status__c == 'Ongoing')
                {
                    hasActiveCourses = true;

                    if (ce.hed__Course_Enrollments__r.size() > 0)
                        result.put(contactId, STAGE_WON);
                }
                else if (ce.Status__c == 'Approved' || (ce.Status__c == 'Incomplete' && ce.StatusResultReason__c == 'Excluded'))
                {
                    hasActiveCourses = true;
                }
                else
                {
                    hasInactiveCourses = true;
                }
            }

            if (!result.containsKey(contactId))
            {
                if (!hasActiveCourses && hasInactiveCourses)
                    result.put(contactId, STAGE_LOST);
                else
                    result.put(contactId, STAGE_UNCHANGED);
            }
        }

        return result;
    }

    private Map<Id, List<hed__Program_Enrollment__c>> getEnrolments(Set<Id> contactIds)
    {
        Map<Id, List<hed__Program_Enrollment__c>> enrolmentMap = new Map<Id, List<hed__Program_Enrollment__c>>();
        List<hed__Program_Enrollment__c> allEnrolments =
            [
                SELECT Id,hed__Contact__c,Status__c,StatusResultReason__c,
                (
                    SELECT Id,Grade__c,hed__Status__c,Type__c
                    FROM hed__Course_Enrollments__r
                    WHERE Grade__c NOT IN ('Exemption', 'Withdraw Not Fail') AND RecordType.DeveloperName = 'Student' AND SubjectOfferingCensusDate__c <= TODAY
                )
                FROM hed__Program_Enrollment__c
                WHERE hed__Contact__c IN : contactIds
                ORDER BY hed__Contact__c
            ];

        for (hed__Program_Enrollment__c enrolment : allEnrolments)
        {
            List<hed__Program_Enrollment__c> enrolments = enrolmentMap.get(enrolment.hed__Contact__c);

            if (enrolments == null)
            {
                enrolments = new List<hed__Program_Enrollment__c> { enrolment };
                enrolmentMap.put(enrolment.hed__Contact__c, enrolments);
            }
            else
            {
                enrolments.add(enrolment);
            }
        }

        return enrolmentMap;
    }

    private Set<Id> getContactIds(List<Opportunity> opps)
    {
        Set<Id> ids = new Set<Id>();

        for (Opportunity o : opps)
            ids.add(o.Contact__c);

        return ids;
    }
}