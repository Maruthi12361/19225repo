public class ProspectReassociateCampaignCommand extends BaseTriggerCommand
{
    private static List<Campaign> allCampaignList;

    public override List<SObject> validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        List<Contact> contactList = new List<Contact>();

        if (triggerContext == Enums.TriggerContext.AFTER_UPDATE)
        {
            for (Contact newContact : (List<Contact>) items)
            {
                Contact oldContact = (Contact) oldMap.get(newContact.Id);

                if (newContact.PreferredTimeframe__c != oldContact.PreferredTimeframe__c || 
                    newContact.Status__c == Constants.STATUS_WARM && oldContact.Status__c != Constants.STATUS_WARM || 
                    oldContact.Status__c == Constants.STATUS_WARM && newContact.Status__c != Constants.STATUS_WARM)
                {
                    contactList.add(newContact);
                }
            }
        }
        else if (triggerContext == Enums.TriggerContext.AFTER_INSERT)
        {
            for (Contact newContact : (List<Contact>) items)
            {
                if (newContact.PreferredTimeframe__c != null || newContact.Status__c == Constants.STATUS_WARM)
                {
                    contactList.add(newContact);
                }
            }
        }

        return contactList;
    }

    public override ActionResult command(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        Map<Id, Contact> contactMap = new Map<Id, Contact>((List<Contact>)items);

        List<Contact> contactList = 
            [
                SELECT Id,PreferredTimeframeLastModified__c,
                (
                    SELECT Id,Status,CampaignId
                    FROM CampaignMembers
                    WHERE Campaign.Type = 'Warm'
                )
                FROM Contact
                WHERE RecordType.DeveloperName =: Constants.RECORDTYPE_CONTACT_PROSPECT AND Id IN : contactMap.keySet()
            ];

        List<CampaignMember> newCampaignMemberList = new List<CampaignMember>();
        List<CampaignMember> deletedCampaignMemberList = new List<CampaignMember>();
        List<Contact> updatedContactList = new List<Contact>();

        for (Contact contact : contactList)
        {
            Contact newContact = contactMap.get(contact.Id);
            Date preferredTimeframeLastModified = newContact.PreferredTimeframeLastModified__c;
            Boolean isContactDirty = false;

            if (triggerContext == Enums.TriggerContext.AFTER_UPDATE && newContact.PreferredTimeframe__c != ((Contact) oldMap.get(newContact.Id)).PreferredTimeframe__c)
            {
                preferredTimeframeLastModified = Date.today();
                contact.PreferredTimeframeLastModified__c = preferredTimeframeLastModified;
                isContactDirty = true;
            }

            if (newContact.PreferredTimeframe__c == null && newContact.Status__c == Constants.STATUS_WARM)
            {
                contact.PreferredTimeframe__c = 'Undecided';
                preferredTimeframeLastModified = Date.today();
                contact.PreferredTimeframeLastModified__c = preferredTimeframeLastModified;
                isContactDirty = true;
            }

            if (preferredTimeframeLastModified == null)
            {
                if (triggerContext == Enums.TriggerContext.AFTER_INSERT)
                {
                    preferredTimeframeLastModified = Date.today();
                }
                else
                {
                    preferredTimeframeLastModified = ((Contact) oldMap.get(newContact.Id)).LastModifiedDate.Date();
                }

                contact.PreferredTimeframeLastModified__c = preferredTimeframeLastModified;
                isContactDirty = true;
            }

            if (newContact.PreferredTimeframeLastModified__c != preferredTimeframeLastModified)
                Log.info(this.className, 'command', 'Update PreferredTimeframeLastModified__c to ' + preferredTimeframeLastModified);

            CampaignMember currentCampaignMember;
            CampaignMember newCampaignMember;

            for (CampaignMember campaignMember : contact.CampaignMembers)
            {
                currentCampaignMember = campaignMember;
                Log.info(this.className, 'command', 'Found existing campaign member: ' + campaignMember);
            }

            if (newContact.Status__c == Constants.STATUS_WARM)
            {
                newCampaignMember = createNewCampaignMember(contact.Id, newContact.PreferredTimeframe__c, preferredTimeframeLastModified);

                if (newCampaignMember != null)
                {
                    if (currentCampaignMember == null)
                    {
                        newCampaignMemberList.add(newCampaignMember);
                        Log.info(this.className, 'command', 'Create new campaign member ' + newCampaignMember);
                    }
                    else if (currentCampaignMember.CampaignId != newCampaignMember.CampaignId)
                    {
                        newCampaignMemberList.add(newCampaignMember);
                        deletedCampaignMemberList.add(currentCampaignMember);
                        Log.info(this.className, 'command', 'Delete existing campaign member and create new one');
                    }
                }
            }
            else if (currentCampaignMember != null)
            {
                deletedCampaignMemberList.add(currentCampaignMember);
                Log.info(this.className, 'command', 'Delete existing campaign member ' + currentCampaignMember);
            }

            if (isContactDirty)
                updatedContactList.add(contact);
        }

        if (newCampaignMemberList.size() > 0)
            uow.registerNew(newCampaignMemberList);

        if (deletedCampaignMemberList.size() > 0)
            uow.registerDeleted(deletedCampaignMemberList);

        if (updatedContactList.size() > 0)
            uow.registerDirty(updatedContactList);

        return new ActionResult(ResultStatus.SUCCESS);
    }

    private CampaignMember createNewCampaignMember(Id contactId, String preferredTimeframe, Date preferredTimeframeLastModified)
    {
        CampaignMember newCampaignMember;
        Campaign assignedCampaign;
        Integer resultingTimeFrame;
        String campaignName;

        if (preferredTimeframe == 'ASAP')
        {
            resultingTimeFrame = 90;
        }
        else if (preferredTimeframe == '3 - 6 Months')
        {
            resultingTimeFrame = 182;
        }
        else if (preferredTimeframe == '6 - 12 Months')
        {
            resultingTimeFrame = 365;
        }
        else if (preferredTimeframe == 'Greater than 12 Months')
        {
            resultingTimeFrame = 366;
        }
        else
        {
            resultingTimeFrame = null;
        }

        if (resultingTimeFrame == null)
        {
            campaignName = 'Undecided';
        }
        else
        {
            resultingTimeFrame -= preferredTimeframeLastModified.daysBetween(Date.today());
            if (resultingTimeFrame <= 90)
            {
                campaignName = 'ASAP';
            }
            else if (resultingTimeFrame <= 182)
            {
                campaignName = '3 - 6 Months';
            }
            else if (resultingTimeFrame <= 365)
            {
                campaignName = '6 - 12 Months';
            }
            else
            {
                campaignName = '12+ Months';
            }
        }
        
        if (allCampaignList == null)
        {
            allCampaignList = 
                [
                    SELECT Id, Name
                    FROM Campaign
                    WHERE Type = 'Warm' AND Status IN ('In Progress')
                ];
        }

        for (Campaign campaign : allCampaignList)
        {
            if (campaign.Name == campaignName)
            {
                assignedCampaign = campaign;
                break;
            }
        }

        // Create campaign member
        if (assignedCampaign != null)
        {
            newCampaignMember = New CampaignMember();
            newCampaignMember.CampaignId = assignedCampaign.Id;
            newCampaignMember.Campaign = assignedCampaign;
            newCampaignMember.ContactId = ContactId;
        }
        Log.info(this.className, 'createNewCampaignMember', 'Match PreferredTimeframe: ' + preferredTimeframe + ', preferredTimeframeLastModified ' + preferredTimeframeLastModified + ' to New Campaign: ' + assignedCampaign);

        return newCampaignMember;
    }
}