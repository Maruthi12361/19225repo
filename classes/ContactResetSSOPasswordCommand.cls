public class ContactResetSSOPasswordCommand extends BaseQueueableCommand
{
    @testVisible
    private static final String ERROR_MSG_USERNOTFOUND = 'User not found in Azure AD';
    private static String fields = 'Cerebro_Student_ID__c, Email, FirstName, Id, LastName, MailingCountry, MiddleName, Name, SingleSignOnStatus__c, SSO_ID__c, EnrolmentTrackingURL__c';

    public ContactResetSSOPasswordCommand()
    {
        super(1);
    }

    protected override List<SObject> validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        List<Contact> contacts = new List<Contact>();

        for (Contact contact : (List<Contact>) items)
        {
            if (String.isEmpty(contact.Cerebro_Student_ID__c))
            {
                Log.info(this.className, 'validate', 'Cerebro student Id is null for {0} contact', new List<String> { contact.Id });
                continue;
            }

            if (String.isEmpty(contact.SingleSignOnStatus__c) || contact.SingleSignOnStatus__c == Constants.CONTACT_SINGLESIGNONSTATUS_NONE)
            {
                Log.info(this.className, 'validate', 'Contact {0} is not in right SSO status. Not resetting password. Current status: {1}', new List<String> { contact.Id, contact.SingleSignOnStatus__c });
                continue;
            }

            contacts.add(contact);
        }

        return contacts;
    }

    public override ActionResult command(List<SObject> items, Id jobId)
    {
        List<Contact> contacts = ContactService.getMany(SObjectUtils.getIDs(items, 'ID'), fields, false);
        Log.info(this.className, 'command', 'Resetting SSO passwords for {0} contacts', new List<String> { String.valueOf(contacts.size()) });

        for (Contact contact : contacts)
            resetPassword(contact);

        return new ActionResult(ResultStatus.SUCCESS);
    }

    private void resetPassword(Contact contact)
    {
        Log.info(this.className, 'resetPassword', 'Resetting SSO password for {0}', new List<String> { contact.ID });

        String password =  SingleSignOnUtilities.generatePassword(6, 'aU');
        String signonId = contact.Cerebro_Student_ID__c + SingleSignOnUtilities.getConfiguration().StudentDomain__c;
        String userObjectId = SingleSignOnUtilities.getUserObjectId(signonId);

        if (String.isBlank(userObjectId))
        {
            Log.error(this.className, 'resetPassword', ERROR_MSG_USERNOTFOUND);
            throw new Exceptions.ApplicationException(ERROR_MSG_USERNOTFOUND);
        }

        if (SingleSignOnUtilities.resetPassword(userObjectId, password))
        {
            ContactService.sendSSOResetPasswordEmail(new ViewModelsSSO.EmailLoginDetails(contact.Id, contact.FirstName, contact.LastName, contact.Email, password, contact.EnrolmentTrackingURL__c));
        }
    }
}