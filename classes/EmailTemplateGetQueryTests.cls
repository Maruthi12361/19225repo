@isTest
public class EmailTemplateGetQueryTests
{
    @testSetup
    static void setup()
    {
        TestUtilities.setupHEDA();
    }

    @isTest
    public static void PassEmptyTemplateName_FailValidation()
    {
        // Arrange
        String templateName = null;
        String errorMessage = null;

        // Act
        try
        {
            EmailTemplate template = EmailService.getEmailTemplate(templateName);
        }
        catch (Exception ex)
        {
            errorMessage = ex.getMessage();
        }

        // Assert
        System.assertEquals(String.format(Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Template Name' }), errorMessage);
    }

    @isTest
    static void QueryingNonExistantEmailTemplate_NullValueReturned()
    {
        // Arrange
        String templateName = 'NonExistantEmailTemplate';

        // Act
        test.startTest();
        EmailTemplate template = new EmailTemplateGetQuery().query(templateName);
        test.stopTest();

        // Assert
        System.assertEquals(null, template, 'Email template should not exist.');
    }

    @isTest
    static void QueryingExistingEmailTemplate_EmailTemplateSuccessfullyReturned()
    {
        // Arrange
        String templateName = 'Workforce Management - Term Kickoff';

        // Act
        test.startTest();
        EmailTemplate template = new EmailTemplateGetQuery().query(templateName);
        test.stopTest();

        // Assert
        System.assertNotEquals(null, template, 'Email template should not exist.');
        System.assertEquals(templateName, template.Name, 'Unexpected template name');
    }
}