@isTest
private class CerebroServiceTests
{
    @testSetup
    public static void setup()
    {
        TestUtilities.setupHEDA();
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);

        Contact contactWarm = TestDataContactMock.createProspectWarm(user);

        Contact contactHot = TestDataContactMock.createProspectHot(user);
        Opportunity opp = TestDataOpportunityMock.createApplyingAcquisition(user, contactHot);
    }

    @isTest
    private static void CreateApplicationOAF_SyncsSuccessfullyToCerebroAPI()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        Opportunity opp = OpportunityService.get([SELECT Id FROM Opportunity LIMIT 1].Id);

        Opportunity newOpp = opp.clone(true, false, true, true);
        newOpp.Last_Year_of_Participation__c = '1999';

        // Act
        Test.startTest();
        ActionResult result = CerebroService.syncApplication(new List<Opportunity> { newOpp }, new Map<Id, SObject>(new List<Opportunity> { opp }), Enums.TriggerContext.AFTER_UPDATE);
        Test.stopTest();

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Requests should be successfully queued');
        System.assertEquals(2, TestHttpCalloutMock.requests.size());
        System.assertEquals(Constants.HTTPSTATUS_CREATE, TestHttpCalloutMock.requests[1].response.getStatusCode());
    }

    @isTest
    private static void CreateApplicationOAFWithServiceDisabled_SkipsSyncingToCerebroAPI()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        Opportunity opp = OpportunityService.get([SELECT Id FROM Opportunity LIMIT 1].Id);

        Opportunity newOpp = opp.clone(true, false, true, true);
        newOpp.Last_Year_of_Participation__c = '1999';

        // Act
        Test.startTest();
        TestUtilities.disableComponent('CerebroSyncApplicationCommand');
        ActionResult result = CerebroService.syncApplication(new List<Opportunity> { newOpp }, new Map<Id, SObject>(new List<Opportunity> { opp }), Enums.TriggerContext.AFTER_UPDATE);
        Test.stopTest();

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Requests should be successfully queued');
        System.assertEquals(0, TestHttpCalloutMock.requests.size());
    }

    @isTest
    private static void UpdateApplicationWithNoChange_SkipsSyncingToCerebroAPI()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        Opportunity opp = OpportunityService.get([SELECT Id FROM Opportunity LIMIT 1].Id);

        Opportunity newOpp = opp.clone(true, false, true, true);

        // Act
        Test.startTest();
        ActionResult result = CerebroService.syncApplication(new List<Opportunity> { newOpp }, new Map<Id, SObject>(new List<Opportunity> { opp }), Enums.TriggerContext.AFTER_UPDATE);
        Test.stopTest();

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Requests should be successfully queued');
        System.assertEquals(0, TestHttpCalloutMock.requests.size());
    }

    @isTest
    private static void CreateApplicationOAFWithNonSyncingState_SkipsSyncingToCerebroAPI()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        Opportunity opp = OpportunityService.get([SELECT Id FROM Opportunity LIMIT 1].Id);

        opp.StageName = 'Commencing';
        update opp;

        Opportunity newOpp = opp.clone(true, false, true, true);
        newOpp.Last_Year_of_Participation__c = '1999';

        // Act
        Test.startTest();
        ActionResult result = CerebroService.syncApplication(new List<Opportunity> { newOpp }, new Map<Id, SObject>(new List<Opportunity> { opp }), Enums.TriggerContext.AFTER_UPDATE);
        Test.stopTest();

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Requests should be successfully queued');
        System.assertEquals(0, TestHttpCalloutMock.requests.size());
    }

    @isTest
    private static void CreateApplicationOAFWithNonApplicationRecordType_SkipsSyncingToCerebroAPI()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        Opportunity opp = OpportunityService.get([SELECT Id FROM Opportunity LIMIT 1].Id);

        opp.RecordTypeId = RecordTypeUtils.getRecordTypeId(Constants.RECORDTYPE_OPPORTUNITY_PROGRESSION, SObjectType.Opportunity);
        update opp;

        Opportunity newOpp = opp.clone(true, false, true, true);
        newOpp.Last_Year_of_Participation__c = '1999';

        // Act
        Test.startTest();
        ActionResult result = CerebroService.syncApplication(new List<Opportunity> { newOpp }, new Map<Id, SObject>(new List<Opportunity> { opp }), Enums.TriggerContext.AFTER_UPDATE);
        Test.stopTest();

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Requests should be successfully queued');
        System.assertEquals(0, TestHttpCalloutMock.requests.size());
    }

    @isTest
    private static void CreateApplicationOAFWithoutAnyEmail_SkipsSyncingToCerebroAPI()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        Opportunity opp = OpportunityService.get([SELECT Id FROM Opportunity LIMIT 1].Id);
        Contact contact = ContactService.get([SELECT Id FROM Contact WHERE Status__c =: Constants.STATUS_HOT LIMIT 1].Id);

        contact.PersonalEmail__c = null;
        contact.hed__WorkEmail__c = null;
        contact.hed__Preferred_Email__c = null;
        update contact;

        Opportunity newOpp = opp.clone(true, false, true, true);
        newOpp.Last_Year_of_Participation__c = '1999';

        // Act
        test.startTest();
        ActionResult result = CerebroService.syncApplication(new List<Opportunity> { newOpp }, new Map<Id, SObject>(new List<Opportunity> { opp }), Enums.TriggerContext.AFTER_UPDATE);
        test.stopTest();

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Requests should be successfully queued');
        System.assertEquals(0, TestHttpCalloutMock.requests.size());
    }

    @isTest
    private static void CreateApplicationOAFWithoutPersonalEmail_SyncingToCerebroAPI()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        Opportunity opp = OpportunityService.get([SELECT Id FROM Opportunity LIMIT 1].Id);
        Contact contact = ContactService.get([SELECT Id FROM Contact WHERE Status__c =: Constants.STATUS_HOT LIMIT 1].Id);

        contact.PersonalEmail__c = null;
        contact.hed__Preferred_Email__c = 'Work';
        update contact;

        Opportunity newOpp = opp.clone(true, false, true, true);
        newOpp.Last_Year_of_Participation__c = '1999';

        // Act
        test.startTest();
        ActionResult result = CerebroService.syncApplication(new List<Opportunity> { newOpp }, new Map<Id, SObject>(new List<Opportunity> { opp }), Enums.TriggerContext.AFTER_UPDATE);
        test.stopTest();

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Requests should be successfully queued');
        System.assertEquals(true, TestHttpCalloutMock.requests.size() >= 2);
    }

    @isTest
    private static void CreateApplicationOAFWithoutFirstName_SkipsSyncingToCerebroAPI()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        Opportunity opp = OpportunityService.get([SELECT Id FROM Opportunity LIMIT 1].Id);
        Contact contact = ContactService.get([SELECT Id FROM Contact WHERE Status__c =: Constants.STATUS_HOT LIMIT 1].Id);

        contact.FirstName = null;   
        update contact;

        Opportunity newOpp = opp.clone(true, false, true, true);
        newOpp.Last_Year_of_Participation__c = '1999';

        // Act
        test.startTest();
        ActionResult result = CerebroService.syncApplication(new List<Opportunity> { newOpp }, new Map<Id, SObject>(new List<Opportunity> { opp }), Enums.TriggerContext.AFTER_UPDATE);
        test.stopTest();

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Requests should be successfully queued');
        System.assertEquals(0, TestHttpCalloutMock.requests.size());
    }

    @isTest
    private static void UpdateApplicationOAF_SyncsSuccessfullyToCerebroAPI()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        Opportunity opp = OpportunityService.get([SELECT Id FROM Opportunity LIMIT 1].Id);
        Contact contact = ContactService.get([SELECT Id FROM Contact WHERE Status__c =: Constants.STATUS_HOT LIMIT 1].Id);

        Opportunity newOpp = opp.clone(true, false, true, true);
        newOpp.Special_Needs_Advice_Required__c = 'Yes';
        newOpp.Has_Special_Needs__c = 'Yes';
        newOpp.Last_Year_of_Participation__c = '1999';

        // Act
        test.startTest();
        ActionResult result = CerebroService.syncApplication(new List<Opportunity> { newOpp }, new Map<Id, SObject>(new List<Opportunity> { opp }), Enums.TriggerContext.AFTER_UPDATE);
        test.stopTest();

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Requests should be successfully queued');
        System.assertEquals(2, TestHttpCalloutMock.requests.size());
        System.assertEquals(Constants.HTTPSTATUS_CREATE, TestHttpCalloutMock.requests[1].response.getStatusCode());
    }

    @isTest
    private static void UpdateApplicationOAFWithPaymentExpiredStatus_SyncsWithErrorToCerebroAPI()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        Opportunity opp = OpportunityService.get([SELECT Id FROM Opportunity LIMIT 1].Id);
        Contact contact = ContactService.get([SELECT Id FROM Contact WHERE Status__c =: Constants.STATUS_HOT LIMIT 1].Id);

        contact.Cerebro_Student_ID__c = 'A000000123';
        contact.OAF_Application_Id__c = '466194';
        contact.MiddleName = 'Binh-Minh';
        update contact;

        Opportunity newOpp = opp.clone(true, false, true, true);
        newOpp.Last_Year_of_Participation__c = '1999';

        // Act
        test.startTest();
        ActionResult result = CerebroService.syncApplication(new List<Opportunity> { newOpp }, new Map<Id, SObject>(new List<Opportunity> { opp }), Enums.TriggerContext.AFTER_UPDATE);
        test.stopTest();

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Requests should be successfully queued');
        System.assertEquals(true, TestHttpCalloutMock.requests.size() >= 2);
        System.assertEquals(Constants.HTTPSTATUS_BADREQUEST, TestHttpCalloutMock.requests[1].response.getStatusCode());
    }

    @isTest
    private static void UpdateApplicationOAFWithApprovedExpiredStatus_SyncsWithErrorToCerebroAPI()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        Opportunity opp = OpportunityService.get([SELECT Id FROM Opportunity LIMIT 1].Id);
        Contact contact = ContactService.get([SELECT Id FROM Contact WHERE Status__c =: Constants.STATUS_HOT LIMIT 1].Id);

        contact.Cerebro_Student_ID__c = 'A000000123';
        contact.OAF_Application_Id__c = '466194';
        contact.MiddleName = 'Hoang-Hon';
        update contact;

        Opportunity newOpp = opp.clone(true, false, true, true);
        newOpp.Last_Year_of_Participation__c = '1999';

        // Act
        test.startTest();
        ActionResult result = CerebroService.syncApplication(new List<Opportunity> { newOpp }, new Map<Id, SObject>(new List<Opportunity> { opp }), Enums.TriggerContext.AFTER_UPDATE);
        test.stopTest();

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Requests should be successfully queued');
        System.assertEquals(true, TestHttpCalloutMock.requests.size() >= 2);
        System.assertEquals(Constants.HTTPSTATUS_BADREQUEST, TestHttpCalloutMock.requests[1].response.getStatusCode());
    }

    @isTest
    private static void UpdateApplicationOAFWithNoValidCerebroApplication_SyncsWithErrorToCerebroAPI()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        Opportunity opp = OpportunityService.get([SELECT Id FROM Opportunity LIMIT 1].Id);
        Contact contact = ContactService.get([SELECT Id FROM Contact WHERE Status__c =: Constants.STATUS_HOT LIMIT 1].Id);

        contact.Cerebro_Student_ID__c = 'A000000123';
        contact.OAF_Application_Id__c = '466194000';
        update contact;

        Opportunity newOpp = opp.clone(true, false, true, true);
        newOpp.Last_Year_of_Participation__c = '1999';

        // Act
        test.startTest();
        ActionResult result = CerebroService.syncApplication(new List<Opportunity> { newOpp }, new Map<Id, SObject>(new List<Opportunity> { opp }), Enums.TriggerContext.AFTER_UPDATE);
        test.stopTest();

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Requests should be successfully queued');
        System.assertEquals(true, TestHttpCalloutMock.requests.size() >= 2);
        System.assertEquals(Constants.HTTPSTATUS_BADREQUEST, TestHttpCalloutMock.requests[1].response.getStatusCode());
    }

    @isTest
    private static void UpdateApplicationOAFWithInvalidAibStudentID_SyncsWithErrorToCerebroAPI()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        Opportunity opp = OpportunityService.get([SELECT Id FROM Opportunity LIMIT 1].Id);
        Contact contact = ContactService.get([SELECT Id FROM Contact WHERE Status__c =: Constants.STATUS_HOT LIMIT 1].Id);

        contact.OAF_Application_Id__c = '466194';
        contact.Cerebro_Student_ID__c = '1857991';
        update contact;

        Opportunity newOpp = opp.clone(true, false, true, true);
        newOpp.Last_Year_of_Participation__c = '1999';

        // Act
        test.startTest();
        ActionResult result = CerebroService.syncApplication(new List<Opportunity> { newOpp }, new Map<Id, SObject>(new List<Opportunity> { opp }), Enums.TriggerContext.AFTER_UPDATE);
        test.stopTest();

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Requests should be successfully queued');
        System.assertEquals(true, TestHttpCalloutMock.requests.size() >= 2);
        System.assertEquals(Constants.HTTPSTATUS_BADREQUEST, TestHttpCalloutMock.requests[1].response.getStatusCode());
    }

    @isTest
    private static void UpdateApplicationOAFWithBlankAIBStudentID_SyncsWithErrorToCerebroAPI()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        Opportunity opp = OpportunityService.get([SELECT Id FROM Opportunity LIMIT 1].Id);
        Contact contact = ContactService.get([SELECT Id FROM Contact WHERE Status__c =: Constants.STATUS_HOT LIMIT 1].Id);

        contact.OAF_Application_Id__c = '466194';
        contact.Cerebro_Student_ID__c = '';
        update contact;

        Opportunity newOpp = opp.clone(true, false, true, true);
        newOpp.Last_Year_of_Participation__c = '1999';

        // Act
        test.startTest();
        ActionResult result = CerebroService.syncApplication(new List<Opportunity> { newOpp }, new Map<Id, SObject>(new List<Opportunity> { opp }), Enums.TriggerContext.AFTER_UPDATE);
        test.stopTest();

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Requests should be successfully queued');
    }

    @isTest
    private static void UpdateWarmProspect_DoesntSyncSuccessfullyToCerebroAPI()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        Contact contact = ContactService.get([SELECT Id FROM Contact WHERE Status__c =: Constants.STATUS_WARM LIMIT 1].Id);

        Contact newCont = contact.clone(true, false, true, true);
        newCont.Salutation = 'Dr';

        // Act
        Test.startTest();
        ActionResult result = CerebroService.syncApplication(new List<Contact> { newCont }, new Map<Id, SObject>(new List<Contact> { contact }), Enums.TriggerContext.AFTER_UPDATE);
        Test.stopTest();

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Requests should be successfully queued');
        System.assertEquals(0, TestHttpCalloutMock.requests.size(), 'No requests should be made to Cerebro API for Warm Prospects');
    }

    @isTest
    private static void UpdateHotProspectWithNoChange_DoesntSyncToCerebroAPI()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        Contact contact = ContactService.get([SELECT Id FROM Contact WHERE Status__c =: Constants.STATUS_WARM LIMIT 1].Id);

        Contact newCont = contact.clone(true, false, true, true);

        // Act
        Test.startTest();
        ActionResult result = CerebroService.syncApplication(new List<Contact> { newCont }, new Map<Id, SObject>(new List<Contact> { contact }), Enums.TriggerContext.AFTER_UPDATE);
        Test.stopTest();

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Requests should be successfully queued');
        System.assertEquals(0, TestHttpCalloutMock.requests.size(), 'No requests should be made to Cerebro API for Hot Prospects with no change');
    }

    @isTest
    private static void UpdateHotProspectWithApplication_SyncsSuccessfullyToCerebroAPI()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        Contact contact = ContactService.get([SELECT Id FROM Contact WHERE Status__c =: Constants.STATUS_HOT LIMIT 1].Id);
       
        Contact newCont = contact.clone(true, false, true, true);
        newCont.Salutation = 'Dr';

        // Act
        Test.startTest();
        ActionResult result = CerebroService.syncApplication(new List<Contact> { newCont }, new Map<Id, SObject>(new List<Contact> { contact }), Enums.TriggerContext.AFTER_UPDATE);
        Test.stopTest();

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Requests should be successfully queued');
        System.assertEquals(2, TestHttpCalloutMock.requests.size());
        System.assertEquals(Constants.HTTPSTATUS_CREATE, TestHttpCalloutMock.requests[1].response.getStatusCode());
    }
}