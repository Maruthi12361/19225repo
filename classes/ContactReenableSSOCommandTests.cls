@isTest
public class ContactReenableSSOCommandTests
{
    @testSetup
    static void setup()
    {
        TestUtilities.setupHEDA();
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createProspectCold(user, 'John', 'Test','61408815278', 'john.test@yahoo.com', 'A0019789789');
    }

    @isTest
    static void ProspectNotCreatedInAzureWithSSOProspectProvisioningStatusUpdatedInLastHour_SSOStatusNotUpdated()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        TestUtilities.disableComponent('ContactSendSSOLoginDetailsCommand');
        Contact contact = [SELECT Id, SingleSignOnStatus__c FROM Contact LIMIT 1];
        contact.SingleSignOnStatus__c = Constants.CONTACT_SINGLESIGNONSTATUS_PROSPECTPROVISIONING;

        // Act
        Test.startTest();
        update contact;
        Test.stopTest();

        Contact updatedContact = [SELECT Id, SingleSignOnStatus__c FROM Contact WHERE Id = :contact.Id];

        // Assert
        System.assertEquals(Constants.CONTACT_SINGLESIGNONSTATUS_PROSPECTPROVISIONING, updatedContact.SingleSignOnStatus__c, 'Unexpected single sign on status');
    }

    @isTest
    static void ProspectNotCreatedInAzureWithSSOProspectProvisioningStatusNotUpdatedInLastHour_SSOStatusNotUpdated()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        TestUtilities.disableComponent('ContactSendSSOLoginDetailsCommand');
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = [SELECT Id, SingleSignOnStatus__c, SSO_ID__c, SingleSignOnStatusLastUpdated__c, LastModifiedDate, RecordTypeId, Cerebro_Student_ID__c FROM Contact LIMIT 1];
        contact.SingleSignOnStatus__c = Constants.CONTACT_SINGLESIGNONSTATUS_PROSPECTPROVISIONING;
        contact.SingleSignOnStatusLastUpdated__c = Datetime.now().addHours(-5);

        System.RunAs(user)
        {
            update contact;
        }

        // Act
        Test.startTest();
        ActionResult result = ContactService.reenableSSO(new List<Contact> { contact }, new Map<Id, SObject>(), Enums.TriggerContext.AFTER_UPDATE);
        Test.stopTest();

        Contact updatedContact = [SELECT Id, SingleSignOnStatus__c, SSO_ID__c, SingleSignOnStatusLastUpdated__c FROM Contact WHERE Id = :contact.Id];

        // Assert
        System.assertEquals(Constants.CONTACT_SINGLESIGNONSTATUS_PROSPECTENABLED, updatedContact.SingleSignOnStatus__c, 'Unexpected single sign on status');
        System.assert(String.isNotEmpty(updatedContact.SSO_ID__c), 'Expected SSO ID to have value');
        System.assert(updatedContact.SingleSignOnStatusLastUpdated__c != null, 'Expected single sign on status last update date to have value');
    }

    @isTest
    static void ProspectNotCreatedInAzureWithSSOStudentProvisioningStatusNotUpdatedInLastHour_SSOStatusNotUpdated()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        TestUtilities.disableComponent('ContactSendSSOLoginDetailsCommand');
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = [SELECT Id, SingleSignOnStatus__c, SSO_ID__c, SingleSignOnStatusLastUpdated__c, LastModifiedDate, RecordTypeId, Cerebro_Student_ID__c FROM Contact LIMIT 1];
        contact.SingleSignOnStatus__c = Constants.CONTACT_SINGLESIGNONSTATUS_STUDENTPROVISIONING;
        contact.SingleSignOnStatusLastUpdated__c = Datetime.now().addHours(-5);

        System.RunAs(user)
        {
            update contact;
        }

        // Act
        Test.startTest();
        ActionResult result = ContactService.reenableSSO(new List<Contact> { contact }, new Map<Id, SObject>(), Enums.TriggerContext.AFTER_UPDATE);
        Test.stopTest();

        Contact updatedContact = [SELECT Id, SingleSignOnStatus__c, SSO_ID__c, SingleSignOnStatusLastUpdated__c FROM Contact WHERE Id = :contact.Id];

        // Assert
        System.assertEquals(Constants.CONTACT_SINGLESIGNONSTATUS_STUDENTENABLED, updatedContact.SingleSignOnStatus__c, 'Unexpected single sign on status');
        System.assert(String.isNotEmpty(updatedContact.SSO_ID__c), 'Expected SSO ID to have value');
        System.assert(updatedContact.SingleSignOnStatusLastUpdated__c != null, 'Expected single sign on status last update date to have value');
    }

    @isTest
    static void ProspectNotCreatedInAzureWithSSONoneStatusAndSyncErrorNotUpdatedInLastHour_SSOStatusNotUpdated()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        TestUtilities.disableComponent('ContactSendSSOLoginDetailsCommand');
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = 
        [
            SELECT Id, SingleSignOnStatus__c, SSO_ID__c, SingleSignOnStatusLastUpdated__c, LastModifiedDate, RecordTypeId, Cerebro_Student_ID__c, SingleSignOnSyncMessage__c 
            FROM Contact LIMIT 1
        ];
        contact.SingleSignOnStatus__c = Constants.CONTACT_SINGLESIGNONSTATUS_NONE;
        contact.SingleSignOnStatusLastUpdated__c = Datetime.now().addHours(-5);
        contact.SingleSignOnSyncMessage__c = 'Error message';

        System.RunAs(user)
        {
            update contact;
        }

        // Act
        Test.startTest();
        ActionResult result = ContactService.reenableSSO(new List<Contact> { contact }, new Map<Id, SObject>(), Enums.TriggerContext.AFTER_UPDATE);
        Test.stopTest();

        Contact updatedContact = [SELECT Id, SingleSignOnStatus__c, SSO_ID__c, SingleSignOnStatusLastUpdated__c FROM Contact WHERE Id = :contact.Id];

        // Assert
        System.assertEquals(Constants.CONTACT_SINGLESIGNONSTATUS_PROSPECTENABLED, updatedContact.SingleSignOnStatus__c, 'Unexpected single sign on status');
        System.assert(String.isNotEmpty(updatedContact.SSO_ID__c), 'Expected SSO ID to have value');
        System.assert(updatedContact.SingleSignOnStatusLastUpdated__c != null, 'Expected single sign on status last update date to have value');
    }
}