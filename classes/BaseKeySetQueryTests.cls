@isTest
public class BaseKeySetQueryTests
{
    @testSetup
    static void setup()
    {
        TestUtilities.setupHEDA();
    }

    @isTest
    static void BaseQueryWithKeys_FiltersAndReturnsResultsMatchingKeysInKeySet()
    {
        // Arrange
        TestBaseKeySetQuery testQuery = new TestBaseKeySetQuery();

        Case newCase1 = new Case();
        newCase1.Subject = 'Test';
        newCase1.Description = 'Test Example';
        insert newCase1;

        Case newCase2 = new Case();
        newCase2.Subject = 'Test';
        newCase2.Description = 'Another Example';
        insert newCase2;
        
        Case newCase3 = new Case();
        newCase3.Subject = 'No Test';
        newCase3.Description = 'Test Example';
        insert newCase3;

        // Act
        List<SObject> results = testQuery.query(new Map<String,String> { 'Subject' => 'Test', 'Created' => 'TODAY' });

        // Assert
        System.assertNotEquals(null, results, 'Request should return a valid collection of items');
        System.assertEquals(2, results.size(), 'Request should return only those items identified by the keyset values provided');
    }

    class TestBaseKeySetQuery extends BaseKeySetQuery
    {
        public override List<SObject> query(Map<String, String> keyset)
        {
            String query = 'SELECT Id ' +
                           'FROM Case ' +
                           'WHERE Subject = \'\'{0}\'\' ' +
                           '  AND CreatedDate = {1} ';

            query = String.format(query, new String[]
            {
                keyset.get('Subject'),
                keyset.get('Created')
            });

            return Database.query(query);
        }
    }
}