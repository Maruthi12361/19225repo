@isTest
public class ContactUpdateRolloverDetailBatchTests
{
    private static String cerebroID = 'A001641336';

    @testSetup
    static void setup()
    {
        // Arrange
        TestUtilities.setupHEDA();
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        TestDataCampaignMock.create(user, Date.today().addDays(56), 'Domestic', 'MBA', true);
        Contact student = TestDataContactMock.createStudent(user, cerebroID);
        Account course = TestDataAccountMock.createCourseMBA(user);
        hed__Program_Enrollment__c courseEnrolment = TestDataCourseEnrolmentMock.create(user, course.Id, student.Id, RolloverConstantsDefinition.CS_ONGOING, RolloverConstantsDefinition.STAGE_ENROLLED, Date.today().addDays(1));
    }

    @isTest
    static void StudentWithLaterSSAFollowupDate_SyncsSSAFollowupDateToContact()
    {
        // Act
        Test.startTest();
        Database.executeBatch(new ContactUpdateRolloverDetailBatch());
        Test.stopTest();

        // Assert
        Contact contact =
        [
            SELECT Id, Current_Course_Status__c, Rollover_Status__c, Course_Start_Date__c, Next_Followup_Date__c, FeeHelp_Eligibility__c, StudentExpiryDate__c
            FROM Contact
            WHERE Cerebro_Student_ID__c = : cerebroID
        ];

        System.assertEquals(RolloverConstantsDefinition.CS_ONGOING, contact.Current_Course_Status__c, 'Wrong value for Current_Course_Status__c');
        System.assertEquals(RolloverConstantsDefinition.FEEHELP_FHE, contact.FeeHelp_Eligibility__c, 'Wrong value for FeeHelp_Eligibility__c');
        System.assertEquals(Date.today(), contact.Course_Start_Date__c, 'Wrong value for Course_Start_Date__c');
        System.assertEquals(contact.Course_Start_Date__c.addYears(3), contact.StudentExpiryDate__c, 'Unexpected StudentExpiryDate__c value');
        System.assertEquals(Date.today().addDays(1), contact.Next_Followup_Date__c, 'Wrong value for Next_Followup_Date__c');
    }

    @isTest
    static void StudentWithNullSystemFollowupDate_SetsAllStageScheduled()
    {
        hed__Program_Enrollment__c courseEnrolment =
        [
            SELECT Id
            FROM hed__Program_Enrollment__c
        ];
        courseEnrolment.NextFollowUpDate__c = null;
        update courseEnrolment;

        // Act
        Test.startTest();
        Database.executeBatch(new ContactUpdateRolloverDetailBatch());
        Test.stopTest();

        // Assert
        Contact contact =
        [
            SELECT Id, Current_Course_Status__c, Rollover_Status__c, Course_Start_Date__c, Next_Followup_Date__c, FeeHelp_Eligibility__c, StudentExpiryDate__c
            FROM Contact
            WHERE Cerebro_Student_ID__c = : cerebroID
        ];

        System.assertEquals(RolloverConstantsDefinition.CS_ONGOING, contact.Current_Course_Status__c, 'Wrong value for Current_Course_Status__c');
        System.assertEquals(RolloverConstantsDefinition.FEEHELP_FHE, contact.FeeHelp_Eligibility__c, 'Wrong value for FeeHelp_Eligibility__c');
        System.assertEquals(Date.today(), contact.Course_Start_Date__c, 'Wrong value for Course_Start_Date__c');
        System.assertEquals(contact.Course_Start_Date__c.addYears(3), contact.StudentExpiryDate__c, 'Unexpected StudentExpiryDate__c value');
        System.assertEquals(null, contact.Next_Followup_Date__c, 'Wrong value for Next_Followup_Date__c');
        System.assertEquals(RolloverConstantsDefinition.RO_ALLSTAGE, contact.Rollover_Status__c, 'Wrong value for Rollover_Status__c');
    }
}