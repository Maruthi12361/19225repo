public class ContentVersionUpdateOppDocsCommand extends BaseTriggerCommand 
{
    protected override List<SObject> validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        List<ContentVersion> result = new List<ContentVersion>();

        for (ContentVersion contentVersion : (List<ContentVersion>)items)
        {
            if (triggerContext != Enums.TriggerContext.AFTER_INSERT && triggerContext != Enums.TriggerContext.AFTER_UPDATE)
                continue;

            if (!Constants.CONTENTVERSION_TYPE_OAFDOCTYPES.contains(contentVersion.Type__c))
                continue;

            result.add(contentVersion);
        }

        return result;
    }

    protected override ActionResult command(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        List<Opportunity> opportunitiesToUpdate = new List<Opportunity>();

        for (ContentVersion contentVersion : (List<ContentVersion>)items)
        {
            // Had to query db inside loop as ContentDocumentLink can be filtered only by a single id
            List<ContentDocumentLink> contentDocumentLinks = getDocumentLinks(contentVersion.ContentDocumentId);
            List<Opportunity> opportunities = getOpportunities(getOpportunityIds(contentDocumentLinks));

            for (Opportunity opp : opportunities)
            {
                Boolean updateOpp = false;

                if (contentVersion.Type__c == Constants.CONTENTVERSION_TYPE_PROOFOFID && opp.ProofOfIDDocs__c != Constants.DOCSTATUS_REQUIRESREVIEW)
                {
                    opp.ProofOfIDDocs__c = Constants.DOCSTATUS_REQUIRESREVIEW;
                    updateOpp = true;
                }
                else if (contentVersion.Type__c == Constants.CONTENTVERSION_TYPE_TRANSCRIPT && opp.TranscriptDocs__c != Constants.DOCSTATUS_REQUIRESREVIEW)
                {
                    opp.TranscriptDocs__c = Constants.DOCSTATUS_REQUIRESREVIEW;
                    updateOpp = true;
                }
                else if (contentVersion.Type__c == Constants.CONTENTVERSION_TYPE_ENGLISHPROFICIENCY && opp.EnglishProficiencyDocs__c != Constants.DOCSTATUS_REQUIRESREVIEW)
                {
                    opp.EnglishProficiencyDocs__c = Constants.DOCSTATUS_REQUIRESREVIEW;
                    updateOpp = true;
                }
                else if (contentVersion.Type__c == Constants.CONTENTVERSION_TYPE_WORKEXPERIENCE && opp.WorkExperienceDocs__c != Constants.DOCSTATUS_REQUIRESREVIEW)
                {
                    opp.WorkExperienceDocs__c = Constants.DOCSTATUS_REQUIRESREVIEW;
                    updateOpp = true;
                }
                else if (contentVersion.Type__c == Constants.CONTENTVERSION_TYPE_SPECIALNEEDS && opp.SpecialNeedsDocs__c != Constants.DOCSTATUS_REQUIRESREVIEW)
                {
                    opp.SpecialNeedsDocs__c = Constants.DOCSTATUS_REQUIRESREVIEW;
                    updateOpp = true;
                }

                if (updateOpp)
                    opportunitiesToUpdate.add(opp);
            }
        }

        if (opportunitiesToUpdate.size() > 0)
            Database.update(opportunitiesToUpdate);

        return new ActionResult(ResultStatus.SUCCESS);
    }

    private List<ContentDocumentLink> getDocumentLinks(Id documentId)
    {
        return
        [
            SELECT Id, ContentDocumentId, LinkedEntityId
            FROM ContentDocumentLink
            WHERE ContentDocumentId = :documentId
        ];
    }

    private Set<Id> getOpportunityIds(List<ContentDocumentLink> documentLinks)
    {
        Set<Id> oppIds = new Set<Id>();

        for (ContentDocumentLink documentLink : documentLinks)
            if (String.valueOf(documentLink.LinkedEntityId).startsWith(Constants.ENTITYID_OPPORTUNITY))
                oppIds.add(documentLInk.LinkedEntityId);

        return oppIds;
    }

    private List<Opportunity> getOpportunities(Set<Id> oppIds)
    {
        return
        [
            SELECT Id, ProofOfIDDocs__c, TranscriptDocs__c, WorkExperienceDocs__c, EnglishProficiencyDocs__c, SpecialNeedsDocs__c
            FROM Opportunity
            WHERE Id IN :oppIds
        ];
    }
}