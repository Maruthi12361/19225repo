@isTest
public class CourseEnrolmentServiceTests
{
    private static String cerebroID = 'A009941336';

    @testSetup
    static void setup()
    {
        TestUtilities.setupHEDA();
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact student = TestDataContactMock.createStudent(user, cerebroID);
        Account courseMBA = TestDataAccountMock.createCourseMBA(user);
        Account courseBBA = TestDataAccountMock.createCourseBBA(user);
        Account courseGCM = TestDataAccountMock.createCourseGCM(user);
        TestDataCourseEnrolmentMock.create(user, courseMBA.Id, student.Id, 'Provisionally Expired');
        TestDataCourseEnrolmentMock.create(user, courseBBA.Id, student.Id, 'Expired');
        TestDataCourseEnrolmentMock.create(user, courseGCM.Id, student.Id, 'Approved');
    }

    @isTest
    static void InsertApprovedGCMCourse_MarksCurrentStudentAsTrue()
    {
        // Arrange
        hed__Program_Enrollment__c courseEnrolment =
            [
                SELECT Id,hed__Contact__c,Status__c,StatusResultReason__c,hed__End_Date__c,Programme__c
                FROM hed__Program_Enrollment__c
                WHERE hed__Account__r.Program__c = 'GCM'
            ];

        // Act
        ActionResult result = CourseEnrolmentService.mergeContact(new List<hed__Program_Enrollment__c>{ courseEnrolment }, new Map<Id, hed__Program_Enrollment__c>(), Enums.TriggerContext.AFTER_INSERT);
        System.assertEquals(ResultStatus.SUCCESS, result.Status);

        // Assert
        Contact contact =
            [
                SELECT Id,Alumni__c,CurrentStudent__c,BBAComplete__c,GCMComplete__c,MBAComplete__c,LastCourseCompletionDate__c,Cerebro_Student_ID__c
                FROM Contact
                WHERE Id = : courseEnrolment.hed__Contact__c
            ];

        System.assertEquals(false, contact.Alumni__c);
        System.assertEquals(true, contact.CurrentStudent__c);
        System.assertEquals(false, contact.BBAComplete__c);
        System.assertEquals(false, contact.GCMComplete__c);
        System.assertEquals(false, contact.MBAComplete__c);
        System.assertEquals(null, contact.LastCourseCompletionDate__c);
    }

    @isTest
    static void ChangeGCMCourseToOnGoing_MarksCurrentStudentAsTrue()
    {
        // Arrange
        hed__Program_Enrollment__c courseEnrolment =
            [
                SELECT Id,hed__Contact__c,Status__c,StatusResultReason__c,hed__End_Date__c,Programme__c
                FROM hed__Program_Enrollment__c
                WHERE hed__Account__r.Program__c = 'GCM'
            ];

        hed__Program_Enrollment__c newCourseEnrolment = courseEnrolment.clone(true, false, true, true);
        newCourseEnrolment.Status__c = 'Ongoing';
        List<hed__Program_Enrollment__c> items = new List<hed__Program_Enrollment__c>{ newCourseEnrolment };
        Map<Id, hed__Program_Enrollment__c> oldMap = new Map<Id, hed__Program_Enrollment__c>();
        oldMap.put(courseEnrolment.Id, courseEnrolment);

        // Act
        ActionResult result = CourseEnrolmentService.mergeContact(items, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        System.assertEquals(ResultStatus.SUCCESS, result.Status);

        // Assert
        Contact contact =
            [
                SELECT Id,Alumni__c,CurrentStudent__c,BBAComplete__c,GCMComplete__c,MBAComplete__c,LastCourseCompletionDate__c,Cerebro_Student_ID__c
                FROM Contact
                WHERE Id = : courseEnrolment.hed__Contact__c
            ];

        System.assertEquals(false, contact.Alumni__c);
        System.assertEquals(true, contact.CurrentStudent__c);
        System.assertEquals(false, contact.BBAComplete__c);
        System.assertEquals(false, contact.GCMComplete__c);
        System.assertEquals(false, contact.MBAComplete__c);
        System.assertEquals(null, contact.LastCourseCompletionDate__c);
    }

    @isTest
    static void DeleteApprovedCourse_MarksCurrentStudentAsFalse()
    {
        // Arrange
        hed__Program_Enrollment__c courseEnrolment =
            [
                SELECT Id,hed__Contact__c,Status__c,StatusResultReason__c,hed__End_Date__c,Programme__c
                FROM hed__Program_Enrollment__c
                WHERE hed__Account__r.Program__c = 'GCM'
            ];

        List<hed__Program_Enrollment__c> items = new List<hed__Program_Enrollment__c>{ courseEnrolment };
        Map<Id, hed__Program_Enrollment__c> oldMap = new Map<Id, hed__Program_Enrollment__c>();
        oldMap.put(courseEnrolment.Id, courseEnrolment);

        // Act
        ActionResult result = CourseEnrolmentService.mergeContact(items, oldMap, Enums.TriggerContext.AFTER_DELETE);
        System.assertEquals(ResultStatus.SUCCESS, result.Status);

        // Assert
        Contact contact =
            [
                SELECT Id,Alumni__c,CurrentStudent__c,BBAComplete__c,GCMComplete__c,MBAComplete__c,LastCourseCompletionDate__c,Cerebro_Student_ID__c
                FROM Contact
                WHERE Id = : courseEnrolment.hed__Contact__c
            ];

        System.assertEquals(false, contact.Alumni__c);
        System.assertEquals(false, contact.CurrentStudent__c);
        System.assertEquals(false, contact.BBAComplete__c);
        System.assertEquals(false, contact.GCMComplete__c);
        System.assertEquals(false, contact.MBAComplete__c);
        System.assertEquals(null, contact.LastCourseCompletionDate__c);
    }

    @isTest
    static void ChangeGCMCourseToCompleted_MarksGCMCompleteAsTrue()
    {
        // Arrange
        hed__Program_Enrollment__c courseEnrolment =
            [
                SELECT Id,hed__Contact__c,Status__c,StatusResultReason__c,hed__End_Date__c,Programme__c
                FROM hed__Program_Enrollment__c
                WHERE hed__Account__r.Program__c = 'GCM'
            ];

        hed__Program_Enrollment__c newCourseEnrolment = courseEnrolment.clone(true, false, true, true);
        newCourseEnrolment.Status__c = 'Completed';
        newCourseEnrolment.StatusResultReason__c = 'Awarded';
        newCourseEnrolment.hed__End_Date__c = Date.today();
        List<hed__Program_Enrollment__c> items = new List<hed__Program_Enrollment__c>{ newCourseEnrolment };
        Map<Id, hed__Program_Enrollment__c> oldMap = new Map<Id, hed__Program_Enrollment__c>();
        oldMap.put(courseEnrolment.Id, courseEnrolment);

        // Act
        ActionResult result = CourseEnrolmentService.mergeContact(items, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        System.assertEquals(ResultStatus.SUCCESS, result.Status);

        // Assert
        Contact contact =
            [
                SELECT Id,Alumni__c,CurrentStudent__c,BBAComplete__c,GCMComplete__c,MBAComplete__c,LastCourseCompletionDate__c,Cerebro_Student_ID__c
                FROM Contact
                WHERE Id = : courseEnrolment.hed__Contact__c
            ];

        System.assertEquals(false, contact.Alumni__c);
        System.assertEquals(false, contact.CurrentStudent__c);
        System.assertEquals(false, contact.BBAComplete__c);
        System.assertEquals(true, contact.GCMComplete__c);
        System.assertEquals(false, contact.MBAComplete__c);
        System.assertEquals(Date.today(), contact.LastCourseCompletionDate__c);
    }

    @isTest
    static void ChangeMBAGCMToCompleted_MarksAlumniAsTrue()
    {
        // Arrange
        List<hed__Program_Enrollment__c> courseEnrolments =
            [
                SELECT Id,hed__Contact__c,Status__c,StatusResultReason__c,hed__End_Date__c,Programme__c
                FROM hed__Program_Enrollment__c
            ];
        List<hed__Program_Enrollment__c> items = new List<hed__Program_Enrollment__c>();

        for (hed__Program_Enrollment__c courseEnrolment : courseEnrolments)
        {
            hed__Program_Enrollment__c newCourseEnrolment = courseEnrolment.clone(true, false, true, true);

            if (courseEnrolment.Programme__c == 'MBA')
            {
                newCourseEnrolment.hed__End_Date__c = Date.today();
                newCourseEnrolment.Status__c = 'Completed';
                newCourseEnrolment.StatusResultReason__c = 'Awarded';
            }
            else if (courseEnrolment.Programme__c == 'GCM')
            {
                newCourseEnrolment.hed__End_Date__c = Date.today().addDays(-100);
                newCourseEnrolment.Status__c = 'Completed';
                newCourseEnrolment.StatusResultReason__c = 'Awarded';
            }

            items.add(newCourseEnrolment);
        }

        Map<Id, hed__Program_Enrollment__c> oldMap = new Map<Id, hed__Program_Enrollment__c>(courseEnrolments);

        // Act
        ActionResult result = CourseEnrolmentService.mergeContact(items, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        System.assertEquals(ResultStatus.SUCCESS, result.Status);

        // Assert
        Contact contact =
            [
                SELECT Id,Alumni__c,CurrentStudent__c,BBAComplete__c,GCMComplete__c,MBAComplete__c,LastCourseCompletionDate__c,Cerebro_Student_ID__c
                FROM Contact
                WHERE Id = : courseEnrolments[0].hed__Contact__c
            ];

        System.assertEquals(true, contact.Alumni__c);
        System.assertEquals(false, contact.CurrentStudent__c);
        System.assertEquals(false, contact.BBAComplete__c);
        System.assertEquals(true, contact.GCMComplete__c);
        System.assertEquals(true, contact.MBAComplete__c);
        System.assertEquals(Date.today(), contact.LastCourseCompletionDate__c);
    }

    @isTest
    static void ChangeBBACourseToCompleted_MarksBBACompleteAsTrue()
    {
        // Arrange
        hed__Program_Enrollment__c courseEnrolment =
            [
                SELECT Id,hed__Contact__c,Status__c,StatusResultReason__c,hed__End_Date__c,Programme__c
                FROM hed__Program_Enrollment__c
                WHERE hed__Account__r.Program__c = 'BBA'
            ];

        hed__Program_Enrollment__c newCourseEnrolment = courseEnrolment.clone(true, false, true, true);
        newCourseEnrolment.Status__c = 'Completed';
        newCourseEnrolment.StatusResultReason__c = 'Awarded';
        List<hed__Program_Enrollment__c> items = new List<hed__Program_Enrollment__c>{ newCourseEnrolment };
        Map<Id, hed__Program_Enrollment__c> oldMap = new Map<Id, hed__Program_Enrollment__c>();
        oldMap.put(courseEnrolment.Id, courseEnrolment);

        // Act
        ActionResult result = CourseEnrolmentService.mergeContact(items, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        System.assertEquals(ResultStatus.SUCCESS, result.Status);

        // Assert
        Contact contact =
            [
                SELECT Id,Alumni__c,CurrentStudent__c,BBAComplete__c,GCMComplete__c,MBAComplete__c,LastCourseCompletionDate__c,Cerebro_Student_ID__c
                FROM Contact
                WHERE Id = : courseEnrolment.hed__Contact__c
            ];

        System.assertEquals(true, contact.Alumni__c);
        System.assertEquals(true, contact.CurrentStudent__c);
        System.assertEquals(true, contact.BBAComplete__c);
        System.assertEquals(false, contact.GCMComplete__c);
        System.assertEquals(false, contact.MBAComplete__c);
        System.assertEquals(null, contact.LastCourseCompletionDate__c);
    }
}