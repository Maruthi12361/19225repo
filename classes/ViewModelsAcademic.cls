public class ViewModelsAcademic
{
    public class Data implements Interfaces.IViewModel
    {
        public List<Term> Terms { get; set; }
        public List<Subject> Subjects { get; set; }
        public List<Course> Courses { get; set; }
        public List<SubjectOffering> SubjectOfferings { get; set; }
    }

    public class Term
    {
        public Id Id { get; set; }
        public String Name { get; set; }
        public Date StartDate { get; set; }
        public Date EndDate { get; set; }
        public Date EnrolCutoffDate { get; set; }
        public Id DepartmentId { get; set; }
        public Integer Year { get; set; }
        public Integer TermNumber { get; set; }
    }

    public class Subject
    {
        public Id Id { get; set; }
        public String Code { get; set; }
        public String Name { get; set; }
        public String Description { get; set; }
        public List<Id> PrerequisiteSubjectIds { get; set; }
        public Integer Prerequisites { get; set; }
    }

    public class Course
    {
        public Id Id { get; set; }
        public String Name { get; set; }
        public String ProductName { get; set; }
        public String Program { get; set; }
        public List<CoursePlan> Plans { get; set; }
        public Id DepartmentId { get; set; }
    }

    public class CoursePlan
    {
        public Id Id { get; set; }
        public String Name { get; set; }
        public String Version { get; set; }
        public Boolean IsPrimary { get; set; }
        public List<PlanRequirement> Requirements { get; set; }
    }

    public class PlanRequirement
    {
        public String Name { get; set; }
        public Decimal Sequence { get; set; }
        public Integer NumberOfSubjects { get; set; }
        public Integer NumberOfPassRequrements { get; set; }
        public List<PlanRequirementSubject> Subjects { get; set; }
    }

    public class PlanRequirementSubject
    {
        public Id Id { get; set; }
        public Integer Sequence { get; set; }
        public Integer OptimalSequence { get; set; }
        public Boolean ConcurrentStudy { get; set; }
    }

    public class SubjectOffering
    {
        public Id Id { get; set; }
        public Id TermId { get; set; }
        public Id SubjectId { get; set; }
        public String SubjectName { get; set; }
        public Date StartDate { get; set; }
        public Date EndDate { get; set; }
        public Date CensusDate { get; set; }
        public Date AdminDate { get; set; }
    }
}