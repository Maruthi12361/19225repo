public class ViewModelsCerebro
{
    public class Application
    {
        public Integer ApplicationID { get; set; }
        public string LeadID { get; set; }
        public string ContactID { get; set; }
        public string OpportunityID { get; set; }

        public PrimaryDetails PrimaryDetails { get; set; }
        public Qualification Qualification { get; set; }
        public SpecialNeeds SpecialNeeds { get; set; }
        public Statistics Statistics { get; set; }
        public FeeHelp FeeHelp { get; set; }

        public Boolean IsSalesForceData { get; set; }
        public string CourseName { get; set; }
        public string Title{ get; set; }
        public string ResidenceCountry { get; set; }
        public string PostalCountry { get; set; }
        public string PermanentCountry { get; set; }
        public string CountryOfBirth { get; set; }
        public string Gender { get; set; }
        public string CitizenOfCountry { get; set; }
        public string QualificationLanguage { get; set; }
        public string LanguageSpokenAtHome { get; set; }
        public string AboriginalAndTorresStraitIslander { get; set; }
        public string HigherEducationAttainedParentOne { get; set; }
        public string HigherEducationAttainedParentTwo { get; set; }
        public string HighestQualification { get; set; }
        public string HighSchoolLevel { get; set; }
        public string TeachingCentreCode { get; set; }
        public Integer YearLeftHighSchool { get; set; }
    }

    public class PrimaryDetails
    {
        public string AIBStudentID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string OtherNames { get; set; }
        public string AddressLine1 { get; set; }
        public string SuburbTown { get; set; }
        public string Postcode { get; set; }
        public string State { get; set; }
        public Integer CountryID { get; set; }
        public string PostalAddressLine1 { get; set; }
        public string PostalSuburbTown { get; set; }
        public string PostalPostcode { get; set; }
        public string PostalState { get; set; }
        public string PermanentHomeLocationAddressLine1 { get; set; }
        public string PermanentHomeLocationAddressLine2 { get; set; }
        public string PermanentHomeLocationSuburbTown { get; set; }
        public string PermanentHomeLocationPostcode { get; set; }
        public string PermanentHomeLocationState { get; set; }
        public Integer SameResidentalAddressTypeID { get; set; }
        public Integer SamePostalAddressTypeID { get; set; }
        public Integer SamePermanentHomeAddressTypeID { get; set; }
        public string HomePhone { get; set; }
        public string MobilePhone { get; set; }
        public string Email { get; set; }
        public string OtherEmail { get; set; }
        public string OtherEmail2 { get; set; }
        public string OtherEmail3 { get; set; }
        public Datetime DateOfBirth { get; set; }
        public Integer ArrivalDate { get; set; }
    }

    public class FeeHelp
    {
        public string HighSchoolName { get; set; }
        public string StateTerritory { get; set; }
        public string CHESSN { get; set; }
        public string PreviousSurname { get; set; }
        public string PreviousFirstName { get; set; }
        public string PreviousOtherName { get; set; }
    }

    public class Qualification
    {
        public string QualificationName { get; set; }
        public string Institution { get; set; }
        public DateTime DurationTo { get; set; }
    }

    public class SpecialNeeds
    {
        public Boolean HasDisability { get; set; }
        public Boolean Hearing { get; set; }
        public Boolean Learning { get; set; }
        public Boolean Mobility { get; set; }
        public Boolean Vision { get; set; }
        public Boolean Medical { get; set; }
        public Boolean Other { get; set; }
        public Boolean IntellectualDisability { get; set; }
        public Boolean MentalHealth { get; set; }
        public Boolean BrainInjury { get; set; }
        public Boolean NeurologicalCondition { get; set; }
        public Boolean ReceiveAdvice { get; set; }
    }

    public class Statistics
    {
        public string HighSchoolPostcode { get; set; }
        public string HighSchoolSuburb { get; set; }
        public Integer YearOfCompletionHighestQulificaiton { get; set; }
    }
}