//
// BaseQueueableCommand
//
// Provides a base class to implement commands on, which handles both chunking and
// deferring of the work in a controlled manner using Salesforce Queueables.
//
// Queueables have limitations when being executed in a test window, so to get a
// better understanding of how the process works, follow these steps:
//
//   1. Import, or create 200 Case items.
//   2. Open the Developer Console window.
//   3. Open the Debug > Open Execute Anonymous window.
//   4. Paste in the following code:
// 
//        TestBaseQueueableCommand command = new TestBaseQueueableCommand();
//
//        ActionResult result = command.execute([SELECT Id, Subject, Description FROM Case]);
//
//   5. Execute the code.
//   6. Open the Setup > Jobs > Apex Jobs page.
//
// A series of TestBaseQueueableCommand jobs will visible, as the complete list of Cases are
// split and processed in a deferred manner.
//
public abstract class BaseQueueableCommand extends BaseComponent implements Queueable, Database.AllowsCallouts
{
    private List<SObject> items;
    private Integer batchSize;

    protected UnitOfWork uow
    {
        get
        {
            return UnitOfWork.current;
        }
    }

    public BaseQueueableCommand(Integer batchSize)
    {
        super();

        this.batchSize = batchSize;
    }

    public BaseQueueableCommand(Integer batchSize, Integer retry)
    {
        super(retry);

        this.batchSize = batchSize;
    }

    public BaseQueueableCommand(Integer batchSize, String enabledLabelText)
    {
        super();

        enabledText = enabledLabelText;
        this.batchSize = batchSize;
    }

    protected abstract List<SObject> validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext);
    protected abstract ActionResult command(List<SObject> items, Id jobId);

    public ActionResult execute(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        Boolean isValid = true;
        Boolean uowScope = false;
        ActionResult result = new ActionResult(ResultStatus.SUCCESS);
        List<SObject> validItems;

        // Parameter Validation
        if (items == null)
            throw new InvalidParameterValueException('items', 'Command execution was requested but was provided with no information to act on');

        if (items.size() == 0)
            return result;

        if (System.isFuture())
            return result;

        if (!isEnabled)
        {
            Log.info(this.className, 'execute', 'Skipping command. Disabled through configuration.'); 
            isValid  = false;
        }

        if (isValid)
        {
            // Validation
            try
            {
                Log.info(this.className, 'execute', 'Validating command.');
                validItems = validate(items, oldMap, triggerContext);

                if (validItems.size() == 0)
                {
                    isValid = false;
                    Log.info(this.className, 'execute', 'Validation: No objects meeting conditions to process.');
                }
            }
            catch (Exception ex)
            {
                exc = ex;
                isValid = false; 
                Log.error(this.className, 'execute', 'Error validating command: ', ex);
                result = new ActionResult(ResultStatus.ERROR, 'Error validating command ' + this.className + ': ' + StringUtils.getExceptionMessage(ex));
            }
        }

        if (isValid)
        {
            // Execution
            try
            {
                if (UnitOfWork.current == null)
                {
                    uowScope = true;
                    UnitOfWork.current = new UnitOfWork();
                }

                this.items = validItems;

                Log.info(this.className, 'execute', 'Queuing of {0} items', new List<String> { String.valueOf(this.items.size()) });

                if (Test.isRunningTest())
                    this.execute(null);
                else if (Limits.getQueueableJobs() < Limits.getLimitQueueableJobs())
                {
                    result.resultId = System.enqueueJob(this);
                }
                else
                    Log.info(this.className, 'execute', 'Reaching QueueableJob limit, abandon queuing of {0} items', new List<String> { String.valueOf(this.items.size()) });

                if (uowScope)
                {
                    UnitOfWork.current.commitWork();
                    uowScope = false;
                }
            }
            catch (Exception ex)
            {
                exc = ex;
                isValid = false; 
                Log.error(this.className, 'execute', 'Error executing command: ', ex);
                result = new ActionResult(ResultStatus.ERROR, 'Error executing command ' + this.className + ': ' + StringUtils.getExceptionMessage(ex));
            }
        }

        if (!trigger.isExecuting && this.saveLogs)
            Log.save();

        if (exc != null && Test.isRunningTest() && TestUtilities.throwOnException)
            throw exc;

        return result;
    }

    public void execute(QueueableContext context)
    {
        Id jobId;
        List<SObject> current = new List<SObject>();
        List<SObject> remaining = new List<SObject>();
        ActionResult result;
        Boolean uowScope = false;
        Integer itemCount = items.size();

        Log.info(this.className, 'execute', 'Processing {0} items, chunked into batches of {0}.', new List<String> { String.valueOf(items.size()), String.valueOf(batchSize) });

        // Identify current item to process.
        remaining.addAll(items);
        while (remaining.size() > 0 && current.size() < batchSize)
            current.add(remaining.remove(0));

        Log.info(this.className, 'execute', 'Identified {0} items to process in current transaction.', new List<String> { String.valueOf(current.size()) });

        // Queue remaining
        if (remaining.size() > 0)
        {
            this.items = remaining;
            
            if (Test.isRunningTest())
            {
                Log.info(this.className, 'execute', 'Salesforce testing infrastructure doesn\'t support cascading queueable jobs', new List<String> { String.valueOf(current.size()) });
            }
            else if (Limits.getQueueableJobs() < Limits.getLimitQueueableJobs())
            {
                jobId = System.enqueueJob(this);

                Log.info(this.className, 'execute', 'Requeued {0} remaining items for command. Id: {1}', new List<String> { String.valueOf(remaining.size()), String.valueOf(jobId) });
            }
            else
                Log.info(this.className, 'execute', 'Reaching QueueableJob limit, abandon queuing of {0} items', new List<String> { String.valueOf(remaining.size()) });
        }

        // Execution of single item
        Log.info(this.className, 'execute', 'Starting command execution.');

        for (Integer i = 0 ; i <= retry; i++)
        {
            result = execute(current, jobId);

            if (result.Status == ResultStatus.SUCCESS || (!result.message.contains('UNABLE_TO_LOCK_ROW') && !result.message.contains('Please try again')))
                break;

            Log.info(this.className, 'execute', 'Unable to lock row, remaining retry {0}', new List<String>{ String.valueOf(retry - i) });
        }

        Log.info(this.className, 'execute', 'Finished command execution.');
    }

    public ActionResult execute(List<SObject> items, Id jobId)
    {
        Boolean uowScope = false;
        ActionResult result;

        if (UnitOfWork.current == null)
        {
            uowScope = true;
            UnitOfWork.current = new UnitOfWork();
        }

        try
        {
            result = command(items, jobId);

            if (uowScope)
            {
                UnitOfWork.current.commitWork();
            }
        }
        catch (Exception ex)
        {
            result = new ActionResult(ResultStatus.ERROR, 'Error executing command: ' + StringUtils.getExceptionMessage(ex));
            Log.error(this.className, 'execute', 'Error executing command: ', ex);
            UnitOfWork.current = null;

            if (Test.isRunningTest())
                throw ex;
        }

        if (uowScope)
            uowScope = false;

        return result;
    }
}