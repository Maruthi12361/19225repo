public with sharing class ViewModelsTestData
{
    public class Department
    {
        public Department(String name, String institutionId)
        {
            this.Name = name;
            this.InstitutionId = institutionId;
        }

        public String Name { get; set; }
        public Id InstitutionId { get; set; }
    }

    public class Institution
    {
        public Institution(String name, String businessOrgId, String website)
        {
            this.Name = name;
            this.BusinessOrgId = businessOrgId;
            this.Website = website;
        }

        public String Name { get; set; }
        public Id BusinessOrgId { get; set; }
        public String Website { get; set; }
    }

    public class Term
    {
        public Term (Id departmentId, Date subjectStartDate)
        {
            this.DepartmentId = departmentId;
            this.SubjectStartDate = subjectStartDate;
            this.Name = null;
        }

        public Term (Id departmentId, String name)
        {
            this.DepartmentId = departmentId;
            this.SubjectStartDate = null;
            this.Name = name;
        }

        public Id DepartmentId { get; set; }
        public Date SubjectStartDate { get; set; }
        public String Name { get; set; }
    }

    public class SubjectDefinition
    {
        public SubjectDefinition (String code, String name, String departmentId, String description)
        {
            this.Code = code;
            this.Name = name;
            this.DepartmentId = departmentId;
            this.Description = description;
        }

        public String Code { get; set; }
        public String Name { get; set; }
        public Id DepartmentId { get; set; }
        public String Description { get; set; }
    }

    public class SubjectVersion
    {
        public SubjectVersion (String code, String name, Id departmentId, Id subjectDefinitionId, Boolean isPrimary, Double versionNumber)
        {
            this.Code = code;
            this.Name = name;
            this.DepartmentId = departmentId;
            this.SubjectDefinitionId = subjectDefinitionId;
            this.IsPrimary = isPrimary;
            this.VersionNumber = versionNumber;
        }

        public String Code { get; set; }
        public String Name { get; set; }
        public Id DepartmentId { get; set; }
        public Id SubjectDefinitionId { get; set; }
        public Boolean IsPrimary { get; set; }
        public Double VersionNumber { get; set; }
    }

    public class Course
    {
        public Course (String name, String productName, String abbreviatedName, String program, Id departmentId)
        {
            this.Name = name;
            this.ProductName = productName;
            this.AbbreviatedName = abbreviatedName;
            this.Program = program;
            this.DepartmentId = departmentId;
        }

        public String Name { get; set; }
        public String ProductName { get; set; }
        public String AbbreviatedName { get; set; }
        public String Program { get; set; }
        public Id DepartmentId { get; set; }
    }

    public class CoursePlan
    {
        public CoursePlan (String name, Id courseId, String version, Boolean isPrimary)
        {
            this.Name = name;
            this.CourseId = courseId;
            this.Version = version;
            this.IsPrimary = isPrimary;
        }

        public String Name { get; set; }
        public Id CourseId { get; set; }
        public String Version { get; set; }
        public Boolean IsPrimary { get; set; }
    }

    public class CoursePlanRequirement
    {
        public CoursePlanRequirement(String name, Id coursePlanId, Integer sequence, Integer numberofSubjects, Integer numberofPassRequirements)
        {
            this.Name = name;
            this.CoursePlanId = coursePlanId;
            this.Sequence = sequence;
            this.NumberOfSubjects = numberOfSubjects;
            this.NumberOfPassRequirements = numberOfPassRequirements;
        }

        public String Name { get; set; }
        public Id CoursePlanId { get; set; }
        public Integer Sequence { get; set; }
        public Integer NumberOfSubjects { get; set; }
        public Integer NumberOfPassRequirements { get; set; }
    }

    public class SubjectRequirement
    {
        public SubjectRequirement(String name, Id subjectId)
        {
            this.Name = name;
            this.SubjectId = subjectId;
            this.NumberOfPassRequirements = 1;
        }

        public SubjectRequirement(String name, Id subjectId, Integer numberOfPassRequirements)
        {
            this.Name = name;
            this.SubjectId = subjectId;
            this.NumberOfPassRequirements = numberOfPassRequirements;
        }

        public String Name { get; set; }
        public Id SubjectId { get; set; }
        public Integer NumberOfPassRequirements { get; set; }
    }

    public class DependencyItem
    {
        public DependencyItem (String name, Id requirementId, Id subjectId)
        {
            this.Name = name;
            this.RequirementId = requirementId;
            this.SubjectId = subjectId;
            this.ConcurrentStudy = true;
        }

        public DependencyItem (String name, Id requirementId, Id subjectId, Integer optimalSequence)
        {
            this.Name = name;
            this.RequirementId = requirementId;
            this.SubjectId = subjectId;
            this.OptimalSequence = optimalSequence;
            this.ConcurrentStudy = true;
        }
    
        public DependencyItem (String name, Id requirementId, Id subjectId, Integer optimalSequence, Integer sequence)
        {
            this.Name = name;
            this.RequirementId = requirementId;
            this.SubjectId = subjectId;
            this.OptimalSequence = optimalSequence;
            this.Sequence = sequence;
        }

        public DependencyItem (String name, Id requirementId, Id subjectId, Integer optimalSequence, Integer sequence, Boolean concurrentStudy)
        {
            this.Name = name;
            this.RequirementId = requirementId;
            this.SubjectId = subjectId;
            this.OptimalSequence = optimalSequence;
            this.Sequence = sequence;
            this.ConcurrentStudy = concurrentStudy;
        }

        public String Name { get; set; }
        public Id RequirementId { get; set; }
        public Id SubjectId { get; set; }
        public Integer Sequence { get; set; }
        public Integer OptimalSequence { get; set; }
        public Boolean ConcurrentStudy { get; set; }
    }

    public class SubjectOffering
    {
        public SubjectOffering(hed__Course__c version, hed__Term__c term)
        {
            this(version, term, Constants.SUBJECTOFFERING_TYPE_STANDARD, null);
        }

        public SubjectOffering(hed__Course__c version, hed__Term__c term, String type)
        {
            this(version, term, type, null);
        }

        public SubjectOffering(hed__Course__c version, hed__Term__c term, String type, Date startDate)
        {
            if (startDate == null)
                startDate = term.FirstSubjectStartDate__c;

            this.StartDate = startDate;
            this.Version = version;
            this.Term = term;
            this.Type = type;

            if (type == Constants.SUBJECTOFFERING_TYPE_VIRTUAL && startDate != null)
                throw(new Exceptions.InvalidParameterException('Virtual Subject Offering specified with a Start Date'));

            if (type == Constants.SUBJECTOFFERING_TYPE_BREAK && version.hed__Course_ID__c != Constants.SUBJECT_COURSEID_BREAK)
                throw(new Exceptions.InvalidParameterException('Break Subject Offering specified with invalid Subject Code'));

            if (type == Constants.SUBJECTOFFERING_TYPE_STANDARD && version == null)
                throw(new Exceptions.InvalidParameterException('Standard Subject Offering specified with no Subject Version information'));

            if (type == Constants.SUBJECTOFFERING_TYPE_STANDARD && version.hed__Course_ID__c == '')
                throw(new Exceptions.InvalidParameterException('Standard Subject Offering specified with a Subject Version that has no Course Id'));

            if (type == Constants.SUBJECTOFFERING_TYPE_STANDARD && startDate == null)
                throw(new Exceptions.InvalidParameterException('Standard Subject Offering specified with no Start Date'));
        }

        public Date StartDate { get; set; }
        public hed__Course__c Version { get; set; }
        public hed__Term__c Term { get; set; }
        public String Type { get; set; }
    }

    public class Data implements Interfaces.IViewModel
    {
        public Account BusinessOrg { get; set; }
        public Account EducationInstitution { get; set; }
        public Account DepartmentBusiness { get; set; }
        public Account DepartmentResearch { get; set; }

        public List<hed__Term__c> Terms { get; set; }

        public List<hed__Course__c> Subjects { get; set; }
        public List<hed__Course__c> SubjectVersions { get; set; }
        public List<hed__Course_Offering__c> SubjectOfferings { get; set; }
        public List<Account> Courses { get; set; }
        public List<hed__Program_Plan__c> CoursePlans { get; set; }
        public List<hed__Plan_Requirement__c> CoursePlanReqts { get; set; }
        public List<hed__Plan_Requirement__c> CoursePlanReqtSubjects { get; set; }

        public List<hed__Plan_Requirement__c> SubjectReqts { get; set; }
        public List<hed__Plan_Requirement__c> SubjectReqtSubjects { get; set; }

        public Data()
        {
            Courses = new List<Account>();
            CoursePlans = new List<hed__Program_Plan__c>();
            CoursePlanReqts = new List<hed__Plan_Requirement__c>();
            CoursePlanReqtSubjects = new List<hed__Plan_Requirement__c>();
            Subjects = new List<hed__Course__c>();
            SubjectVersions = new List<hed__Course__c>();
            SubjectReqts = new List<hed__Plan_Requirement__c>();
            SubjectReqtSubjects = new List<hed__Plan_Requirement__c>();
        }

        public hed__Course__c subject(String reference)
        {
            Id recordTypeId = RecordTypeUtils.getRecordTypeId(Constants.RECORDTYPE_SUBJECT_DEFINITION, SObjectType.hed__Course__c);

            for (hed__Course__c subject : this.Subjects)
                if (subject.RecordTypeId == recordTypeId && (subject.hed__Course_Id__c == reference || subject.Id == IdUtils.toId(reference)))
                    return subject;

            return null;
        }

        public hed__Course__c subjectVersion(String reference)
        {
            return subjectVersion(reference, true);
        }

        public hed__Course__c subjectVersion(String reference, Boolean isPrimaryOnly)
        {
            Id recordTypeId = RecordTypeUtils.getRecordTypeId(Constants.RECORDTYPE_SUBJECT_VERSION, SObjectType.hed__Course__c);

            for (hed__Course__c subject : this.SubjectVersions)
                if (subject.RecordTypeId == recordTypeId && (subject.hed__Course_Id__c == reference || subject.Id == IdUtils.toId(reference)) && (isPrimaryOnly == false || subject.IsPrimary__c == true ))
                    return subject;

            return null;
        }

        public Account course(String abbreviatedName)
        {
            for (Account course : this.Courses)
                if (course.AbbreviatedName__c == abbreviatedName)
                    return course;

            return null;
        }

        public hed__Program_Plan__c coursePlan(String coursePlanName)
        {
            for (hed__Program_Plan__c plan : this.CoursePlans)
                if (plan.Name == coursePlanName)
                    return plan;

            return null;
        }

        public hed__Plan_Requirement__c coursePlanReqt(String coursePlanName, String name)
        {
            Id recordTypeId = RecordTypeUtils.getRecordTypeId(Constants.RECORDTYPE_PLANREQUIREMENT_COURSEPLANREQUIREMENT, SObjectType.hed__Plan_Requirement__c);

            hed__Program_Plan__c plan = coursePlan(coursePlanName);

            if (plan == null)
                return null;

            for (hed__Plan_Requirement__c requirement : this.CoursePlanReqts)
                if (requirement.RecordTypeId == recordTypeId && requirement.hed__Program_Plan__c == plan.Id && requirement.Name == name)
                    return requirement;

            return null;
        }

        public hed__Plan_Requirement__c coursePlanReqtSubject(String coursePlanName, String coursePlanReqtName, String subjectCode)
        {
            Id recordTypeId = RecordTypeUtils.getRecordTypeId(Constants.RECORDTYPE_PLANREQUIREMENT_DEPENDENCYITEM, SObjectType.hed__Plan_Requirement__c);

            hed__Plan_Requirement__c planReqt = coursePlanReqt(coursePlanName, coursePlanReqtName);

            if (planReqt == null)
            {
                System.assert(false, 'Cant find plan');
                return null;
            }

            hed__Course__c subject = subject(subjectCode);

            if (subject == null)
            {
                System.assert(false, 'Cant find subject');
                return null;
            }

            for (hed__Plan_Requirement__c requirement : this.CoursePlanReqtSubjects)
                if (requirement.RecordTypeId == recordTypeId && requirement.hed__Plan_Requirement__c == planReqt.Id && requirement.hed__Course__c == subject.Id)
                    return requirement;

            return null;
        }

        public hed__Plan_Requirement__c subjectReqt(String name)
        {
            for (hed__Plan_Requirement__c requirement : this.SubjectReqts)
                if (requirement.RecordTypeId == RecordTypeUtils.getRecordTypeId(Constants.RECORDTYPE_PLANREQUIREMENT_SUBJECTREQUIREMENT, SObjectType.hed__Plan_Requirement__c)
                    && requirement.Name == name)
                    return requirement;

            return null;
        }
        
        public hed__Term__c term(Date startDate)
        {
            for (hed__Term__c term : this.Terms)
                if (term.hed__Start_Date__c == startDate)
                    return term;

            return null;
        }
        
        public hed__Course_Offering__c subjectOffering(Date startDate, String subjectCode)
        {
            hed__Term__c term = this.term(startDate);
            hed__Course__c subject = this.subjectVersion(subjectCode);

            for (hed__Course_Offering__c offering : this.SubjectOfferings)
                if (offering.hed__Term__c == term.Id && offering.hed__Course__c == subject.Id)
                    return offering;

            return null;
        }
    }
}