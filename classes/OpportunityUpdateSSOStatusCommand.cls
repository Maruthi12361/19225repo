public class OpportunityUpdateSSOStatusCommand extends BaseTriggerCommand
{
    protected override List<SObject> validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        List<Opportunity> opportunities = new List<Opportunity>();
        Id acquisitionRecordTypeId = RecordTypeUtils.getRecordTypeId(Constants.RECORDTYPE_OPPORTUNITY_ACQUISITION, SObjectType.Opportunity);

        for (Opportunity opp : (List<Opportunity>) items)
        {
            Opportunity oldValue = (Opportunity)oldMap.get(opp.Id);

            if (opp.RecordTypeId == acquisitionRecordTypeId && opp.Contact__c != null && opp.OfferAcceptedDate__c != oldValue.OfferAcceptedDate__c && opp.OfferAcceptedDate__c != null)
            {
                opportunities.add(opp);
            }
        }

        return opportunities;
    }

    protected override ActionResult command(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        List<Contact> contactsToEnableSSO = new List<Contact>();
        List<Contact> contacts = Contactservice.getMany(SObjectUtils.getIDs(items, 'Contact__c'), 'Id, Cerebro_Student_ID__c, SingleSignOnStatus__c', false);

        for (Opportunity opp: (List<Opportunity>) items)
        {
            Contact contact = getContact(opp.Contact__c, contacts);

            if (contact == null)
            {
                Log.info(this.className, 'command', 'No contact found {0}', new List<String> { opp.Contact__c });
                continue;
            }

            if (String.isBlank(contact.Cerebro_Student_ID__c))
            {
                Log.info(this.className, 'command', 'No cerebro student identifier found for {0} contact', new List<String> { opp.Contact__c });
                continue;
            }

            if (contact.SingleSignOnStatus__c == Constants.CONTACT_SINGLESIGNONSTATUS_NONE)
            {
                contact.SingleSignOnStatus__c = Constants.CONTACT_SINGLESIGNONSTATUS_PROSPECTPROVISIONING;
                contact.SingleSignOnStatusLastUpdated__c = Datetime.now();
                contactsToEnableSSO.add(contact);
                uow.registerDirty(contact);
            }
        }

        return ContactService.enableSSO(contactsToEnableSSO);
    }

    private Contact getContact(Id contactId, List<Contact> contacts)
    {
        for (Contact contact : contacts)
            if (contact.Id == contactId)
                return contact;

        return null;
    }
}