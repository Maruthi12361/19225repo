public class LeadService 
{
    public ActionResult createProspect(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        return new LeadCreateProspectCommand().execute(items, oldMap, triggerContext);
    }
}