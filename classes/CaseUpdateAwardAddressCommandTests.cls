@isTest
public class CaseUpdateAwardAddressCommandTests
{
    @testSetup
    public static void setup()
    {
        TestUtilities.setupHEDA();
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createStudent(user);
        TestDataCaseMock.createAwardCase(user, contact);
        Account tc = TestDataAccountMock.createInstitution(user, 'Oxford College of Business');
        tc.BillingStreet = '100 Thurstan Road';
        tc.BillingCity = 'Colombo 7';
        tc.BillingState = null;
        tc.BillingPostalCode = '46100';
        tc.BillingCountry = 'SRI LANKA';
        update tc;
    }

    @isTest
    static void InsertAwardCase_UpdatesCaseAwardAddress()
    {
        // Arrange
        Contact contact =
            [
                SELECT Id,MailingStreet,MailingCity,MailingState,MailingPostalCode,MailingCountry
                FROM Contact
                LIMIT 1
            ];
        List<Case> cases =
            [
                SELECT Id,RecordTypeId,Teaching_Centre__c,ContactId,AwardAddressStreet__c,AwardAddressSuburb__c,AwardAddressState__c,AwardAddressPostcode__c,AwardAddressCountry__c
                FROM Case
                WHERE RecordType.DeveloperName = : Constants.RECORDTYPE_CASE_AWARD
            ];
        Map<Id, Case> oldMap = new Map<Id, Case>();

        // Act
        Test.startTest();
        ActionResult result = CaseService.updateAwardAddress(cases, oldMap, Enums.TriggerContext.BEFORE_INSERT);
        Test.stopTest();

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.Status);
        System.assertEquals(contact.MailingStreet == null ? '' : contact.MailingStreet, cases[0].AwardAddressStreet__c);
        System.assertEquals(contact.MailingCity == null ? '' : contact.MailingCity, cases[0].AwardAddressSuburb__c);
        System.assertEquals(contact.MailingState == null ? '' : contact.MailingState, cases[0].AwardAddressState__c);
        System.assertEquals(contact.MailingPostalCode == null ? '' : contact.MailingPostalCode, cases[0].AwardAddressPostcode__c);
        System.assertEquals(contact.MailingCountry == null ? '' : contact.MailingCountry, cases[0].AwardAddressCountry__c);
    }

    @isTest
    static void UpdateAwardCaseTeachingCentre_UpdatesCaseAwardAddress()
    {
        // Arrange
        List<Case> cases =
            [
                SELECT Id,RecordTypeId,Teaching_Centre__c,ContactId,AwardAddressStreet__c,AwardAddressSuburb__c,AwardAddressState__c,AwardAddressPostcode__c,AwardAddressCountry__c
                FROM Case
                WHERE RecordType.DeveloperName = : Constants.RECORDTYPE_CASE_AWARD
            ];
        Account tc =
            [
                SELECT Id,Name,BillingStreet,BillingCity,BillingState,BillingPostalCode,BillingCountry
                FROM Account
                WHERE RecordType.DeveloperName = 'Educational_Institution' AND Name = 'Oxford College of Business'
            ];
        Case newCase = cases[0].clone(true, false, true, true);
        newCase.Teaching_Centre__c = tc.Name;
        Map<Id, Case> oldMap = new Map<Id, Case>(cases);

        // Act
        Test.startTest();
        ActionResult result = CaseService.updateAwardAddress(new List<Case>{ newCase }, oldMap, Enums.TriggerContext.BEFORE_UPDATE);
        Test.stopTest();

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.Status);
        System.assertEquals(tc.BillingStreet == null ? '' : tc.BillingStreet, newCase.AwardAddressStreet__c);
        System.assertEquals(tc.BillingCity == null ? '' : tc.BillingCity, newCase.AwardAddressSuburb__c);
        System.assertEquals(tc.BillingState == null ? '' : tc.BillingState, newCase.AwardAddressState__c);
        System.assertEquals(tc.BillingPostalCode == null ? '' : tc.BillingPostalCode, newCase.AwardAddressPostcode__c);
        System.assertEquals(tc.BillingCountry == null ? '' : tc.BillingCountry, newCase.AwardAddressCountry__c);
    }

    @isTest
    static void UpdateAwardCaseContact_UpdatesCaseAwardAddress()
    {
        // Arrange
        List<Case> cases =
            [
                SELECT Id,RecordTypeId,Teaching_Centre__c,ContactId,AwardAddressStreet__c,AwardAddressSuburb__c,AwardAddressState__c,AwardAddressPostcode__c,AwardAddressCountry__c
                FROM Case
                WHERE RecordType.DeveloperName = : Constants.RECORDTYPE_CASE_AWARD
            ];
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createStudent(user);
        contact.MailingStreet = '165 Sir Donald Bradman Drive';
        contact.MailingCity = 'Hilton';
        contact.MailingState = 'SA';
        contact.MailingPostalCode = '5033';
        contact.MailingCountry = 'Australia';
        update contact;
        Case newCase = cases[0].clone(true, false, true, true);
        newCase.ContactId = contact.Id;
        Map<Id, Case> oldMap = new Map<Id, Case>(cases);

        // Act
        Test.startTest();
        ActionResult result = CaseService.updateAwardAddress(new List<Case>{ newCase }, oldMap, Enums.TriggerContext.BEFORE_UPDATE);
        Test.stopTest();

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.Status);
        System.assertEquals(contact.MailingStreet == null ? '' : contact.MailingStreet, newCase.AwardAddressStreet__c);
        System.assertEquals(contact.MailingCity == null ? '' : contact.MailingCity, newCase.AwardAddressSuburb__c);
        System.assertEquals(contact.MailingState == null ? '' : contact.MailingState, newCase.AwardAddressState__c);
        System.assertEquals(contact.MailingPostalCode == null ? '' : contact.MailingPostalCode, newCase.AwardAddressPostcode__c);
        System.assertEquals(contact.MailingCountry == null ? '' : contact.MailingCountry, newCase.AwardAddressCountry__c);
    }
}