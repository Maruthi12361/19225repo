@isTest
public class ContactResetSSOPasswordCommandTests
{
    @testSetup
    static void setup()
    {
        TestUtilities.setupHEDA();
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createProspectCold(user, 'John', 'Test','61408815278', 'john.test@yahoo.com', 'A0019789789');
    }

    @isTest
    static void ProspectWithNoCerebroId_PasswordResetNotExecuted()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        Contact contact = [SELECT Id, Cerebro_Student_ID__c, SingleSignOnStatus__c FROM Contact LIMIT 1];
        contact.Cerebro_Student_ID__c = null;
        update contact;

        // Act
        Test.startTest();
        ActionResult result = ContactService.resetSSOPassword(contact);
        Test.stopTest();

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.status, 'Unexpected result status');
        System.assertEquals(0, TestHttpCalloutMock.requests.size());
    }

    @isTest
    static void ProspectWithNoSSOProspectProvisioningStatus_PasswordResetNotExecuted()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        Contact contact = [SELECT Id, Cerebro_Student_ID__c, SingleSignOnStatus__c FROM Contact LIMIT 1];

        // Act
        Test.startTest();
        ActionResult result = ContactService.resetSSOPassword(contact);
        Test.stopTest();

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.status, 'Unexpected result status');
        System.assertEquals(0, TestHttpCalloutMock.requests.size());
    }

    @isTest
    static void ProspectNotCreatedInAzure_NoUserFoundErrorReturned()
    {
        // Arrange
        String errorMsg = '';
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createProspectCold(user, 'Prospect', 'Azure','61408815120', 'prospect.azure@yahoo.com', 'A0000000000');
        contact.SingleSignOnStatus__c = Constants.CONTACT_SINGLESIGNONSTATUS_PROSPECTENABLED;
        update contact;

        // Act
        test.startTest();
        try
        {
            ActionResult result = ContactService.resetSSOPassword(contact);
        }
        catch (Exception ex)
        {
            errorMsg = ex.getMessage();
        }
        test.stopTest();

        // Assert
        System.assertEquals(true, errorMsg.containsIgnoreCase(ContactResetSSOPasswordCommand.ERROR_MSG_USERNOTFOUND), 'Unexpected result status');
    }

    @isTest
    static void ProspectWithSSOProspectEnabledStatus_PasswordResetSuccessfully()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        Contact contact = [SELECT Id, Cerebro_Student_ID__c, SingleSignOnStatus__c, EnrolmentTrackingURL__c FROM Contact LIMIT 1];
        contact.SingleSignOnStatus__c = Constants.CONTACT_SINGLESIGNONSTATUS_PROSPECTENABLED;
        update contact;

        // Act
        Test.startTest();
        ActionResult result = ContactService.resetSSOPassword(contact);
        Test.stopTest();

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.status, 'Unexpected result status');
        System.assertEquals(4, TestHttpCalloutMock.requests.size());
    }
}