global class CaseLastInteractionBatch extends BaseBatchCommand
{
    String query;

    global CaseLastInteractionBatch(String qry) 
    {
        Log.info(this.className, 'constructor', 'Query being processed in batch execution: ' + qry);
        this.query = qry;
    }

    global override Database.QueryLocator query(Database.BatchableContext BC)
    {
        return Database.getQueryLocator(query);
    }

    global override void command(Database.BatchableContext BC, List<sObject> scope) 
    {
        ActionResult result = CaseService.calculateLastInteraction(scope);

        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);
    }
}