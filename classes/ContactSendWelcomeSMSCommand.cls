public class ContactSendWelcomeSMSCommand extends BaseQueueableCommand
{
    public ContactSendWelcomeSMSCommand()
    {
        super(50, 5);
    }

    public override List<SObject> validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        List<Contact> filteredList = new List<Contact>();

        for (Contact contact: (List<Contact>)items)
        {
            if (contact.RecordTypeId != RecordTypeUtils.getRecordTypeId(Constants.RECORDTYPE_CONTACT_PROSPECT, SObjectType.Contact))
            {
                Log.info(this.className, 'validate', 'Skip process as Contact is not Prospect RecordType.');
                continue;
            }
            else if (String.isBlank(contact.FirstName))
            {
                Log.info(this.className, 'validate', 'Skip process as Contact does not have First Name.');
                continue;
            }
            else if (contact.WelcomeProspectSMSSent__c)
            {
                Log.info(this.className, 'validate', 'Welcome SMS has already been sent.');
                continue;
            }
            else if (contact.Phone == null || !String.valueOf(contact.Phone).startsWith('+61'))
            {
                Log.info(this.className, 'validate', 'Skip process as Contact either has no Phone number or it is not Australia number.');
                continue;
            }
            else if (contact.Program__c == null || contact.Program__c != 'MBA' || contact.Portfolio__c == null || contact.Portfolio__c != 'Domestic')
            {
                Log.info(this.className, 'validate', 'Skip process as Contact Program Portfolio is not DMBA.');
                continue;
            }
            else if (triggerContext == Enums.TriggerContext.AFTER_UPDATE && SObjectUtils.hasPropertyChanged(contact, oldMap.get(contact.Id), 'WelcomeProspectSMSSent__c'))
            {
                continue;
            }
            else
                filteredList.add(contact);
        }

        return filteredList;
    }

    public override ActionResult command(List<SObject> items, Id jobId)
    {
        if (items.size() == 0)
            return new ActionResult(ResultStatus.SUCCESS);

        List<Contact> updatedContacts = new List<Contact>();
        List<ViewModelsSMS.Details> welcomeSMS = new List<ViewModelsSMS.Details>();

        List<Contact> contacts = ContactService.getMany((new Map<Id, SObject>(items)).keySet(), 'Id, Phone, WelcomeProspectSMSSent__c', false);

        for (Contact contact: contacts)
        {
            if (contact.WelcomeProspectSMSSent__c)
                continue;

            EmailTemplate smsTemplate = EmailService.getEmailTemplate(Constants.EMAILTEMPLATE_WELCOME_SMS);

            if (smsTemplate == null)
            {
                Log.error(this.className, 'command', 'Failed to send Welcome SMS to {0} as there was no Welcome SMS Template.', new List<String>{ contact.Name });
                continue;
            }

            Messaging.SingleEmailMessage sms = Messaging.renderStoredEmailTemplate(smsTemplate.Id, contact.Id, contact.Id);
            welcomeSMS.add(new ViewModelsSMS.Details(contact.Id, contact.Phone, sms.getPlainTextBody()));
            updatedContacts.add(contact);
        }

        List<Contact> contactsForUpdate = ContactService.getMany((new Map<Id, SObject>(updatedContacts)).keySet(), 'Id, WelcomeProspectSMSSent__c', true);

        for (Contact contact: contactsForUpdate)
            contact.WelcomeProspectSMSSent__c = true;

        if (contactsForUpdate.size() > 0)
            update contactsForUpdate;

        for (ViewModelsSMS.Details payload: welcomeSMS)
            sendSMS(payload.fromName, payload.ToPhoneNumber, payload.Message);

        return new ActionResult(ResultStatus.SUCCESS);
    }

    @future(callout = true)
    private static void sendSMS(Id contactId, String toPhoneNumber, String message)
    {
        ActionResult result = SMSService.sendToRecipient(Constants.APIPROVIDER_MARKETINGCLOUD, null, toPhoneNumber, message);

        if (result.isError)
        {
            User sysAdmin = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
            String errMessage = 'Failed to send Welcome SMS to ' + toPhoneNumber + ' as ' + result.message;
            EmailService.sendLegacy(sysAdmin.Email, 'Failed to send Welcome SMS', message);
            Log.error('ContactSendWelcomeSMSCommand', 'sendSMS', errMessage);
            Log.save();

            Contact contact = ContactService.get(contactId, 'Id, WelcomeProspectSMSSent__c', true);
            contact.WelcomeProspectSMSSent__c = false;
            update contact;
        }
    }
}