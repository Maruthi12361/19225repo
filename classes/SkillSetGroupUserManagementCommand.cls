public class SkillSetGroupUserManagementCommand extends BaseTriggerCommand
{
    private static Set<String> groupNames = new Set<String>{ Constants.GROUP_COURSE_CONSULTANTS, Constants.GROUP_COURSE_ADVISORS };

    public override List<SObject> validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        Id recordTypeId = RecordTypeUtils.getRecordTypeId(Constants.RECORDTYPE_SKILLSET_STAFF, SObjectType.SkillSet__c);
        List<Group> groupsToReCalc = new List<Group>();
        Set<String> groupsNamesAffected = new Set<String>();

        for (SkillSet__c skillSet : (List<SkillSet__c>) items)
        {
            if (skillSet.RecordTypeId == recordTypeId)
            {
                if (triggerContext == Enums.TriggerContext.AFTER_INSERT || triggerContext == Enums.TriggerContext.AFTER_DELETE)
                {
                    if (String.isNotBlank(skillSet.Type__c) && groupNames.contains(skillSet.Type__c))
                        groupsNamesAffected.add(skillSet.Type__c);
                }
                else if (triggerContext == Enums.TriggerContext.AFTER_UPDATE)
                {
                    if (String.isNotBlank(skillSet.Type__c) && groupNames.contains(skillSet.Type__c))
                        groupsNamesAffected.add(skillSet.Type__c);

                    SkillSet__c oldValue = (SkillSet__c)oldMap.get(skillSet.Id);
                    if ((oldValue.Type__c != null && oldValue.Type__c != skillSet.Type__c) || (oldValue.User__c != null && oldValue.User__c != skillSet.User__c))
                        groupsNamesAffected.add(oldValue.Type__c);
                }
            }
        }

        if (groupsNamesAffected.size() > 0)
            groupsToReCalc = [SELECT Id, Name, DeveloperName FROM Group WHERE DeveloperName in : groupsNamesAffected AND Type = 'Regular'];

        return groupsToReCalc;
    }

    public override ActionResult command(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        Log.info(this.className, 'command', 'Running command on {0} Group/s: {1}.', new List<String>{ String.valueOf(items.size()), String.valueOf(items) });

        for (Group grp : (List<Group>)items)
        {
            Set<Id> userIdsInGroup = new Map<Id, User>([SELECT Id FROM User WHERE Id in (SELECT UserOrGroupId FROM GroupMember WHERE GroupId =: grp.Id)]).keySet();
            Set<Id> userIdsInSkillSet = new Set<Id>();

            for (AggregateResult result : [SELECT User__c FROM SkillSet__c WHERE Type__c =: grp.DeveloperName AND User__r.isActive = true GROUP BY User__c])
                userIdsInSkillSet.add((Id)result.get('User__c'));

            // Work out intersection between the two lists of group and skillset users to determine the list of users to be added/removed to/from each group
            List<Id> userIdsToAdd = new List<Id>();
            for (Id skillSetUserId : userIdsInSkillSet)
                if (!userIdsInGroup.contains(skillSetUserId))
                    userIdsToAdd.add(skillSetUserId);

            List<Id> userIdsToRemove = new List<Id>();
            for (Id groupUserId : userIdsInGroup)
                if (!userIdsInSkillSet.contains(groupUserId))
                    userIdsToRemove.add(groupUserId);

            Log.info(this.className, 'command', 'For Group:{0} obtained {1} userIdsInGroup and {2} userIdsInSkillSet resulting in {3} userIdsToRemove and {4} userIdsToAdd. References - userIdsInGroup:{5}, userIdsInSkillSet{6}, userIdsToRemove:{7}, userIdsToAdd:{8}.',
                new List<String>{ grp.DeveloperName, String.valueOf(userIdsInGroup.size()), String.valueOf(userIdsInSkillSet.size()), String.valueOf(userIdsToRemove.size()), String.valueOf(userIdsToAdd.size()), String.valueOf(userIdsInGroup), String.valueOf(userIdsInSkillSet), String.valueOf(userIdsToRemove), String.valueOf(userIdsToAdd) });

            updateGroupMembers(grp.Id, grp.DeveloperName, userIdsToRemove, false);
            updateGroupMembers(grp.Id, grp.DeveloperName, userIdsToAdd, true);
            updateUsersInGroupValue(grp, userIdsToRemove, false);
            updateUsersInGroupValue(grp, userIdsToAdd, true);
        }

        return new ActionResult(ResultStatus.SUCCESS);
    }

    @future
    private static void updateGroupMembers(Id groupId, String groupDeveloperName, List<Id> userIds, boolean addingToGroup)
    {
        List<GroupMember> groupMembers = new List<GroupMember>();

        if (addingToGroup)
            for (Id userIdToAdd : userIds)
                groupMembers.add(new GroupMember(GroupId = groupId, UserOrGroupId = userIdToAdd));
        else
            groupMembers.addAll([SELECT Id, UserOrGroupId FROM GroupMember WHERE UserOrGroupId in : userIds AND groupId =: groupId]);

        Log.info('SkillSetGroupUserManagementCommand', 'updateGroupMembers', 'About to {0} the following {1} GroupMembers from Group: {2}. References - GroupMembers: {3}.', 
            new List<String>{ (addingToGroup ? 'insert' : 'delete'), String.valueOf(groupMembers.size()), groupDeveloperName, String.valueOf(groupMembers) });

        if (addingToGroup)
            insert groupMembers;
        else
            delete groupMembers;
    }

    private void updateUsersInGroupValue(Group grp, List<Id> userIds, boolean addingToGroup)
    {
        Log.info(this.className, 'updateUsersInGroupValue', 'Updating {0} users.inGroup__c value to {1}. References - UserIds: {2}.', new List<String>{ String.valueOf(userIds.size()), (addingToGroup ? grp.DeveloperName : 'null'), String.valueOf(UserIds) });

        List<User> users = addingToGroup ? [SELECT Id, inGroup__c FROM User WHERE Id in : userIds and inGroup__c != : grp.DeveloperName]
                                         : [SELECT Id, inGroup__c FROM User WHERE Id in : userIds and inGroup__c = : grp.DeveloperName];

        for (User usr : users)
            usr.inGroup__c = addingToGroup ? grp.DeveloperName : null;

        update users;
    }
}