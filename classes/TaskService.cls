public class TaskService 
{
    public static ActionResult create(String ownerId, String whoId, String whatId, Date dueDate, String priority, String recordType, String status, String subject, String subtask, String type)
    {
        Task newTask = new Task
        (
            OwnerId = ownerId,
            WhoId = whoId,
            WhatId = whatId,
            ActivityDate = dueDate,
            Priority = priority,
            RecordTypeId = recordType,
            Status = status,
            TaskSubtype = subtask,
            Type = type,
            Subject = subject
        );
        
        return new TaskCreateCommand().execute(new List<Task> { newTask });
    }

    public static ActionResult updateNVMSubTypeToCall(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        return new TaskNVMSubTypeUpdateCommand().execute(items, oldMap, triggerContext);
    }

    public static ActionResult syncCallCountToContact(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        return new TaskSyncCallCountToContactCommand().execute(items, oldMap, triggerContext);
    }

    public static ActionResult updateCaseOppDocStatus(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        return new TaskUpdateCaseOppDocStatusCommand().execute(items, oldMap, triggerContext);
    }

    public static ActionResult updateContactCall(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        return new TaskUpdateContactCallCommand().execute(items, oldMap, triggerContext);
    }
}