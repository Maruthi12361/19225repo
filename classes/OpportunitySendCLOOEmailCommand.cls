public class OpportunitySendCLOOEmailCommand extends BaseTriggerCommand
{
    static Set<Id> activeRecordSet = new Set<Id>();
    private static final Id acquisitionRecordTypeId = Label.ID_RecordType_Opportunity_Acquisition;

    public override List<SObject> validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        List<Opportunity> filteredList = new List<Opportunity>();

        for (Opportunity newValue: (List<Opportunity>)items)
        {
            if (triggerContext == Enums.TriggerContext.AFTER_UPDATE)
            {
                Opportunity oldValue = (Opportunity) oldMap.get(newValue.Id);

                if (newValue.RecordTypeId == acquisitionRecordTypeId && !newValue.IsClosed && newValue.CLOOSentDate__c == null && 
                    newValue.OwnerId != Constants.USERID_OAF && newValue.OwnerId != oldValue.OwnerId && 
                    GroupMemberUtils.containsUser(Constants.GROUP_COURSE_ADVISORS, newValue.OwnerId))
                {
                    if (activeRecordSet.contains(newValue.Id))
                    {
                        Log.info(this.className, 'validate', 'Skipping ' + newValue.Id + ' because it has been processed already.');
                        continue;
                    }

                    filteredList.add(newValue);
                }
            }
            else if (triggerContext == Enums.TriggerContext.AFTER_INSERT)
            {
                if (newValue.RecordTypeId == acquisitionRecordTypeId && !newValue.IsClosed && newValue.CLOOSentDate__c == null && 
                    newValue.OwnerId != Constants.USERID_OAF && GroupMemberUtils.containsUser(Constants.GROUP_COURSE_ADVISORS, newValue.OwnerId))
                    filteredList.add(newValue);
            }
        }

        return filteredList;
    }

    public override ActionResult command(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        for (Opportunity opportunity: (List<Opportunity>)items)
        {
            activeRecordSet.add(opportunity.Id);
        }

        return OpportunityService.queueCLOOEmail(items, oldMap, triggerContext);
    }
}