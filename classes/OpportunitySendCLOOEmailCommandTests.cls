@isTest
public class OpportunitySendCLOOEmailCommandTests
{
    @testSetup
    static void setup()
    {
        TestUtilities.setupHEDA();
    }

    @isTest
    static void ChangedOwnerToOAFUser_NoCLOO()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createProspectHot(user);
        Opportunity opportunity = TestDataOpportunityMock.create(user, contact, 'Applying', true);
        Map<Id, Opportunity> oldMap = new Map<Id, Opportunity>();
        oldMap.put(opportunity.Id, opportunity);
        Opportunity newOpportunity = opportunity.clone(true, false, true, true);
        newOpportunity.OwnerId = Constants.USERID_OAF;

        // Act
        Test.startTest();
        OpportunityService.sendCLOOEmail(new List<Opportunity>{ newOpportunity }, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        Test.stopTest();

        // Assert
        List<Task> tasks =
        [
            SELECT Id, OwnerId, Subject, Description, TaskSubtype
            FROM Task
            WHERE WhoId = : contact.Id AND WhatId = : opportunity.Id
        ];
        opportunity =
        [
            SELECT Id, OwnerId, CLOOSentDate__c
            FROM Opportunity
            WHERE Id = : opportunity.Id
        ];
        System.assertEquals(0, tasks.size());
        System.assertEquals(null, opportunity.CLOOSentDate__c);
    }

    @isTest
    static void ChangedOwnerToNonOAFUserInCAGroup_SendsCLOO()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createProspectHot(user);
        Opportunity opportunity = TestDataOpportunityMock.create(user, contact, 'Applying', true);
        opportunity.OwnerId = Constants.USERID_OAF;
        Map<Id, Opportunity> oldMap = new Map<Id, Opportunity>();
        oldMap.put(opportunity.Id, opportunity);
        Opportunity newOpportunity = opportunity.clone(true, false, true, true);
        GroupMember gm = [SELECT Id, userOrGroupId FROM GroupMember WHERE Group.DeveloperName =: Constants.GROUP_COURSE_ADVISORS LIMIT 1];
        user = UserService.get(gm.UserOrGroupId);
        newOpportunity.OwnerId = user.Id;

        // Act
        Test.startTest();
        OpportunityService.sendCLOOEmail(new List<Opportunity>{ newOpportunity }, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        OpportunityService.sendCLOOEmail(new List<Opportunity>{ newOpportunity }, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        Test.stopTest();

        // Assert
        List<Task> tasks =
        [
            SELECT Id, OwnerId, Subject, Description, TaskSubtype
            FROM Task
            WHERE WhoId = : contact.Id AND WhatId = : opportunity.Id
        ];
        opportunity =
        [
            SELECT Id, OwnerId, CLOOSentDate__c
            FROM Opportunity
            WHERE Id = : opportunity.Id
        ];
        System.assertEquals(1, tasks.size());
        System.assertEquals(opportunity.OwnerId, tasks[0].OwnerId);
        System.assertEquals('Email', tasks[0].TaskSubtype);
        System.assertNotEquals(null, tasks[0].Subject);
        System.assertNotEquals(null, tasks[0].Description);
        System.assertNotEquals(null, opportunity.CLOOSentDate__c);
    }

    @isTest
    static void NewOpportunityWithNonOAFUserInCAGroupAsOwner_SendCLOO()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createProspectHot(user);
        Opportunity opportunity = TestDataOpportunityMock.create(user, contact, 'Applying', true);
        GroupMember gm = [SELECT Id, userOrGroupId FROM GroupMember WHERE Group.DeveloperName =: Constants.GROUP_COURSE_ADVISORS LIMIT 1];
        user = UserService.get(gm.UserOrGroupId);
        opportunity.OwnerId = user.Id;

        // Act
        Test.startTest();
        OpportunityService.sendCLOOEmail(new List<Opportunity>{ opportunity }, new Map<Id, Opportunity>(), Enums.TriggerContext.AFTER_INSERT);
        Test.stopTest();

        // Assert
        List<Task> tasks =
        [
            SELECT Id, OwnerId, Subject, Description, TaskSubtype
            FROM Task
            WHERE WhoId = : contact.Id AND WhatId = : opportunity.Id
        ];
        opportunity =
        [
            SELECT Id, OwnerId, CLOOSentDate__c
            FROM Opportunity
            WHERE Id = : opportunity.Id
        ];
        System.assertEquals(1, tasks.size());
        System.assertNotEquals(null, opportunity.CLOOSentDate__c);
    }
}