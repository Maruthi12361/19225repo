@isTest
public class TermSendKickOffEmailCommandTests
{
    @testSetup
    static void setup()
    {        
        TestUtilities.setupHEDA();
    	User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Account course = TestDataAccountMock.createCourseMBA(user);
        Account account = TestDataAccountMock.create(user, 'Test AIB Account');
        hed__Term__c term = TestDataTermMock.create(user, account.Id, Date.today());
        hed__Course__c subjectDefinition = TestDataSubjectMock.createDefinitionCGOV(user);
        hed__Course__c subjectVersion = TestDataSubjectMock.createVersionCGOV(user);
        hed__Course_Offering__c subjectOffering = TestDataSubjectOfferingMock.createOffering(user, subjectVersion, term);

        Contact student = TestDataContactMock.createStudent(user, 'A001648585', 'A001648585@study.aib.edu.au', true, false);
        hed__Program_Enrollment__c courseEnrolment = TestDataCourseEnrolmentMock.create(user, course.Id, student.Id, 'Ongoing');
        TestDataSubjectEnrolmentMock.create(UserService.getByAlias(Constants.USER_SYSTEMADMIN), subjectOffering, courseEnrolment, 'Enrolled', 'Standard', null, null);
    }

    @isTest 
    static void TermWithSubjectOfferingAndSubjectEnrolmentWithHeadOfDiscipline_EmailSuccessfullyGenerated()
    {        
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        List<hed__Term__c> terms = [SELECT Id, Name FROM hed__Term__c LIMIT 1];

        // Act
        Test.startTest();
        Integer callCount = Limits.getFutureCalls();
        ActionResult result = new TermSendKickOffEmailCommand().execute(terms, null, null);
        callCount = Limits.getFutureCalls() - callCount;
        Test.stopTest();

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Unexpected result status.');
        System.assertEquals(1, callCount, 'Unexpected number of emails sent');
    }

    @isTest 
    static void TermWithSubjectOfferingAndSubjectEnrolmentNoHeadOfDiscipline_NoEmailsGenerated()
    {        
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        List<hed__Term__c> terms = [SELECT Id, Name FROM hed__Term__c LIMIT 1];
        hed__Course_Offering__c subjectOffering = [SELECT Id, hed__Course__c, hed__Course__r.HeadOfDiscipline__c, ActualStudents__c FROM hed__Course_Offering__c LIMIT 1];
        subjectOffering.hed__Course__r.HeadOfDiscipline__c = null;
        update subjectOffering.hed__Course__r;

        // Act
        Test.startTest();
        Integer callCount = Limits.getFutureCalls();
        ActionResult result = new TermSendKickOffEmailCommand().execute(terms, null, null);
        callCount = Limits.getFutureCalls() - callCount;
        Test.stopTest();

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Unexpected result status.');
        System.assertEquals(0, callCount, 'Expected no emails to be sent');
    }
}