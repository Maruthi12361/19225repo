public class ViewModelsFile
{
    public class Upload implements Interfaces.IViewModel
    {
        public Id ParentId { get; set; }
        public Id OwnerId { get; set; }
        public Id CommunityId { get; set; }
        public String Reference { get; set; }
        public String FileName { get; set; }
        public String Description { get; set; }
        public Blob Content { get; set; }
        public String Type { get; set; }
    }

    public class Details implements Interfaces.IViewModel
    {
        public Id Id { get; set; }
        public Id VersionId { get; set; }
        public Id EntityId { get; set; }
        public String Name { get; set; }
        public String ContentType { get; set; }
        public Integer BodyLength { get; set; }
        public DateTime CreatedDate { get; set; }
        public String Type { get; set; }
    }

    public class DocumentLink implements Interfaces.IViewModel
    {
        public Id ParentId { get; set; }
        public Set<Id> DocumentIds { get; set; }
    }

    public class ContentDetails implements Interfaces.IViewModel
    {
        public Id VersionId { get; set; }
        public String Name { get; set; }
        public String Extension { get; set; }
        public Blob Content { get; set; }
    }
}