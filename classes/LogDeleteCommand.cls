public class LogDeleteCommand extends BaseCommand
{
    public override List<SObject> validate(List<SObject> params)
    {
        return params;
    }

    public override ActionResult command(List<SObject> params)
    {
        try
        {
            Database.DeleteResult[] drList = Database.delete(params);
            Database.emptyRecycleBin(params);

            return new ActionResult(ResultStatus.SUCCESS, new List<String>{'Successfully deleted logs'});
        }
        catch(Exception ex)
        {
            //Log error to sytem debug
            String error = String.Format('{0} - {1}, \n{2}', new List<String>{'LogCommand', ex.getMessage(), ex.getStackTraceString()});
            Log.error(this.className, 'command', error);
            return new ActionResult(ResultStatus.ERROR, new List<String>{error});
        }
    }
}