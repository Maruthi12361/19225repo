public class SubjectTriggerHandler extends BaseTriggerHandler
{
    public override void afterInsert()
    {
        ActionResult result = SubjectService.createVersion(Trigger.new, Trigger.oldMap, Enums.TriggerContext.AFTER_INSERT);
        
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);
        
        result = SubjectService.updatePrimaryVersion(Trigger.new, Trigger.oldMap, Enums.TriggerContext.AFTER_INSERT);
        
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);
    }
    
    public override void afterUpdate()
    {
        ActionResult result = SubjectService.createVersion(Trigger.new, Trigger.oldMap, Enums.TriggerContext.AFTER_UPDATE);
        
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);
        
        result = SubjectService.updatePrimaryVersion(Trigger.new, Trigger.oldMap, Enums.TriggerContext.AFTER_UPDATE);
        
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);
    }
}