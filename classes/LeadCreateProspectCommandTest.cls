@IsTest
public class LeadCreateProspectCommandTest 
{
    @testSetup
    static void setup()
    {
        TestUtilities.setupHEDA();
    }

    @isTest 
    static void NewLeadCreated_ContactProspectSuccessfullyCreated() 
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        Lead lead = TestDataLeadMock.create(user);

        // Act
        Contact contact = [SELECT Id, FirstName, LastName, PersonalEmail__c, HomePhone, MobilePhone, MailingCountry FROM Contact LIMIT 1];

        // Assert
        System.assertEquals(lead.FirstName, contact.FirstName);
        System.assertEquals(lead.LastName, contact.LastName);
        System.assertEquals(lead.Email, contact.PersonalEmail__c);
        System.assertEquals(lead.Phone, contact.HomePhone);
        System.assertEquals(lead.MobilePhone, contact.MobilePhone);
        System.assertEquals(lead.Country, contact.MailingCountry);
    }
}