@RestResource(urlMapping='/prospect/*')
global class ProspectController 
{
    @HttpGet
    global static void get()
    {
        try
        {
            String reference = '';
            RestRequest req = RestContext.request;
            ViewModels.Param param = new ViewModels.Param();
            Map<String, Object> optionMap = new Map<String, Object>();

            if(req.headers.containsKey('Reference'))
                reference = req.headers.get('Reference');

            param.Identifier = reference;
            param.IsAuthenticated = reference == '' || reference.contains('@') ? false : true;

            RestContext.response.addHeader('Content-Type', 'application/json');
            RestContext.response.responseBody = Blob.valueOf(JSON.serialize(ContactService.getProspect(param)));
            RestContext.response.statusCode = 200;
        }
        catch(Exception ex)
        {
            Map<String, String> errorMap = new Map<String, String>
            {
                'Message' => ex.getMessage(),
                'Details' => ex.getStackTraceString() 
            };

            RestContext.response.addHeader('Content-Type', 'application/json');
            RestContext.response.responseBody = Blob.valueOf(JSON.serialize(errorMap));
            RestContext.response.statusCode = 400;
        }
    }

    @HttpPost
    global static void save()
    {
        String reference = '';
        RestRequest req = RestContext.request;
        ViewModels.Param reqParam = new ViewModels.Param();
        Map<String, Object> optionMap = new Map<String, Object>();

        if (req.headers.containsKey('Reference'))
            reference = req.headers.get('Reference');

        reqParam.Identifier = reference;
        reqParam.Payload =  req.requestBody.toString();
        reqParam.IsAuthenticated = reference == '' || reference.contains('@') ? false : true;

        Log.info('ProspectController', 'save', 'Identifier value {0} \n Payload: {1}', new List<String>{ reqParam.Identifier, reqParam.Payload });

        ActionResult result;

        for (Integer i = 0; i < 5; i++)
        {
            result = ContactService.saveProspect(reqParam);

            if (result.Status == ResultStatus.SUCCESS || (!result.message.contains('UNABLE_TO_LOCK_ROW') && !result.message.contains('Please try again')))
                break;

            Log.info('ProspectController', 'save', 'Unable to lock row, remaining retry {0}', new List<String>{ String.valueOf(4 - i) });
        }

        if (result.Status == ResultStatus.SUCCESS)
        {
            ViewModels.Param param = new ViewModels.Param();
            param.Identifier = EncryptionUtil.Encrypt(result.resultId);

            param.IsAuthenticated = result.message.equalsIgnoreCase('Created') ? true : reqParam.IsAuthenticated;

            RestContext.response.addHeader('Content-Type', 'application/json');
            RestContext.response.responseBody = Blob.valueOf(JSON.serialize(ContactService.getProspect(param)));
            RestContext.response.statusCode = 200;
        }
        else 
        {
            Log.error('ProspectControler', 'save', result.message);
            throw new Exceptions.ApplicationException(result.message);
        }
    }
}