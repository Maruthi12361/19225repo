public class ContactCreateReferralCommand extends BaseComponent
{
    private Boolean validate(Object parameter)
    {
        ViewModels.Param param = (ViewModels.Param) parameter;

        if (String.isEmpty(param.Identifier))
        {
            Log.error(this.className, 'validate',  Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Identifier' });
            throw new Exceptions.InvalidParameterException(String.format(Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Identifier' }));
        }
        else if (String.isEmpty(param.Payload))
        {
            Log.error(this.className, 'validate',  Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Referral' });
            throw new Exceptions.InvalidParameterException(String.format(Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Referral' }));
        }
        else
        {
            Map<String, Object> prospectMap = (Map<String, Object>)JSON.deserializeUntyped(param.Payload);

            if (prospectMap.size() == 0)
            {
                Log.error(this.className, 'validate',  Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Referral Body' });
                throw new Exceptions.InvalidParameterException(String.format(Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Referral Body' }));
            }
        }

        return true;
    }

    public ActionResult command(Object parameter)
    {
        Contact contact;
        ActionResult result;

        validate(parameter);

        ViewModels.Param param = (ViewModels.Param) parameter;
        Log.info(this.className, 'command', 'Request made with following parameter: Identifier {0} \n Prospect {1}', new List<String>{ param.Identifier, param.Payload });

        try
        {
            contact = ContactService.get(param.Identifier, 'Id, FirstName, LastName', false);
        }
        catch(QueryException ex)
        {
            String message = ex.getMessage();
            result = new ActionResult(ResultStatus.ERROR, message);
            Log.error(this.className, 'command', message);
            return result;
        }

        if (contact == null)
            return new ActionResult(ResultStatus.ERROR, 'Contact does not exists in db'); // TODO: Fix error message

        Id prospectId;
        Map<String, Object> prospectMap = (Map<String, Object>)JSON.deserializeUntyped(param.Payload);

        result = saveProspect(prospectMap);
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);

        prospectId = result.ResultId;
        result = saveInterestingMoment(prospectId, 'Milestone', String.format('Referred by {0} {1} ({2})', 
            new List<String> { contact.FirstName, contact.LastName, contact.Id }));

        if (result.isError)
            throw new Exceptions.ApplicationException(result.message); 

        result = saveInterestingMoment(contact.Id, 'Form Completed', String.format('Referred {0} {1} ({2})', 
            new List<String> { String.valueOf(prospectMap.get('Cnt_FirstName')), String.valueOf(prospectMap.get('Cnt_LastName')), prospectId }));

        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);

        result = saveReferral(contact.Id, prospectId);

        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);

        result.ResultId = prospectId;
        return result;
    }

    private ActionResult saveProspect(Map<String, Object> prospectMap)
    {
        if (!hasUtmFields(prospectMap))
        {
            // No utm fields in payload, add default referral ones
            prospectMap.put('utm_source__c', 'referral marketing');
            prospectMap.put('utm_medium__c', 'lead-gen');
            prospectMap.put('utm_content__c', 'landing-page');
        }
        prospectMap.put('Cnt_WelcomeProspectEmailSent_c', 'true');

        ViewModels.Param prospectParam = new ViewModels.Param();
        prospectParam.Identifier = String.valueOf(prospectMap.get('Cnt_Email'));
        prospectParam.Payload = JSON.serialize(prospectMap);
        prospectParam.AllowUpdate = false;

        return ContactService.saveProspect(prospectParam);
    }

    private ActionResult saveInterestingMoment(Id prospectId, String type, String source)
    {
        List<Object> payload = new List<Object>();

        Map<String, Object> momentMap = new Map<String, Object>();
        momentMap.put('Source__c', source);
        momentMap.put('EventDate__c', Datetime.now());
        momentMap.put('Type__c', type);

        payload.add((Object) JSON.deserializeUntyped(JSON.serialize(momentMap)));

        ViewModels.Param momentParam = new ViewModels.Param();
        momentParam.Identifier = EncryptionUtil.Encrypt(prospectId);
        momentParam.Payload = JSON.serialize(payload);

        return ContactService.saveInterestingMoment(momentParam);
    }

    private ActionResult saveReferral(Id contactId, Id prospectId)
    {
        Referral__c referral = new Referral__c 
        (
            Referrer_Contact__c = contactId,
            Referee_Contact__c = prospectId 
        );

        return ReferralService.save(referral);
    }

    private Boolean hasUtmFields(Map<String, Object> referralMap )
    {
        Set<String> utmFields = new Set<String>{ 'utm_campaign__c', 'utm_content__c', 'utm_keyword__c', 'utm_medium__c', 'utm_source__c', 'utm_term__c', 'utm_gclid__c', 'utm_referralSource__c' };

        for (String utmField : utmFields)
        {
            if (referralMap.containsKey(utmField))
                return true;
        }

        return false;
    }
}