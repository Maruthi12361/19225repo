public class ContactTrackStatusHistoryCommand extends BaseTriggerCommand
{
    static Map<Id, Status_Tracking_History__c> activeHistoryMap = new Map<Id, Status_Tracking_History__c>();
    static DateTime now = DateTime.now();

    public override List<SObject> validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        List<Contact> filteredList = new List<Contact>();

        if (triggerContext == Enums.TriggerContext.AFTER_INSERT || oldMap == NULL)
        {
            for (Contact newValue: (List<Contact>)items)
            {
                filteredList.add(newValue);
            }
        }
        else if (triggerContext == Enums.TriggerContext.AFTER_UPDATE)
        {
            for (Contact newValue: (List<Contact>)items)
            {
                Contact oldValue = (Contact) oldMap.get(newValue.Id);

                if (newValue.Status__c != oldValue.Status__c)
                {
                    Status_Tracking_History__c activeHistory = activeHistoryMap.get(newValue.Id);

                    if (activeHistory != null && activeHistory.Type__c == 'State Change' && activeHistory.Status_Stage__c == newValue.Status__c)
                    {
                        Log.info(this.className, 'validate', 'Skipping ' + newValue.Id + ' because there exists an active history ' + activeHistory);
                        continue;
                    }

                    filteredList.add(newValue);
                }
            }
        }

        return filteredList;
    }

    public override ActionResult command(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        Status_Tracking_History__c current;
        Status_Tracking_History__c previous;
        List<Status_Tracking_History__c> history;
        Id prospectType = RecordTypeUtils.getRecordTypeId(Constants.RECORDTYPE_CONTACT_PROSPECT, SObjectType.Contact);

        if (triggerContext == Enums.TriggerContext.AFTER_UPDATE)
        {
            history =
            [
                SELECT Id, Contact_ID__c, Type__c, Status_Stage__c, StartDateTime__c, EndDateTime__c, Call_Stage__c, Lead_Timer_Minutes__c, Call_Outcome__c
                FROM Status_Tracking_History__c
                WHERE Contact_ID__c = :(new Map<Id, SObject>(items).keySet()) AND Type__c IN ('State Change')
                    AND CaseId__c = NULL AND Opportunity_ID__c = NULL AND EndDateTime__c = NULL
                ORDER BY CreatedDate DESC
            ];
        }

        for (Contact item: (List<Contact>)items)
        {
            if (triggerContext == Enums.TriggerContext.AFTER_INSERT)
            {
                current = createStatusTrackingHistory(item, Constants.STATUS_COLD, 'State Change', now);
                uow.registerNew(current);
                Log.info(this.className, 'command', 'Creating new Status Tracking History item for new contact ' + current);

                if (item.Status__c != Constants.STATUS_COLD)
                {
                    current.EndDateTime__c = now;
                    current.Next_User__c = UserInfo.getName();
                    current.Next_Status_Stage__c = item.Status__c;
                    current.Direction__c = getDirection(Constants.STATUS_COLD, item.Status__c);

                    current = createStatusTrackingHistory(item, item.Status__c, 'State Change', now);
                    uow.registerNew(current);
                    Log.info(this.className, 'command', 'Creating new Status Tracking History item for new contact ' + current);
                }
            }
            else if (triggerContext == Enums.TriggerContext.AFTER_UPDATE)
            {
                Contact oldValue = (Contact) oldMap.get(item.Id);

                // Find the previous history entry for this contact
                for (Status_Tracking_History__c historyItem: history)
                {
                    if (historyItem.Contact_ID__c == item.Id && historyItem.Type__c == 'State Change')
                    {
                        previous = historyItem;
                        continue;
                    }
                }

                // Update previous item
                if (previous != null)
                {
                    previous.EndDateTime__c = now;
                    previous.Next_Status_Stage__c = item.Status__c;
                    previous.Next_User__c = UserInfo.getName();
                    previous.Direction__c = getDirection(previous.Status_Stage__c, item.Status__c);

                    uow.registerDirty(previous);
                    Log.info(this.className, 'command', 'Updating previous Status Tracking History ' + previous);
                }

                // Create new item
                current = createStatusTrackingHistory(item, now);
                uow.registerNew(current);
                activeHistoryMap.put(current.Contact_ID__c, current);
                Log.info(this.className, 'command', 'Creating new Status Tracking History item ' + current);
            }
        }

        return new ActionResult(ResultStatus.SUCCESS);
    }

    private Integer getStateValue(String state)
    {
        Integer value = 2;

        if (!String.isBlank(state))
        {
            if (state == Constants.STATUS_DISQUALIFIED)
                value = 1;
            else if (state == Constants.STATUS_COLD)
                value = 2;
            else if (state == Constants.STATUS_WARM)
                value = 3;
            else if (state == Constants.STATUS_HOT)
                value = 4;
            else if (state == Constants.STATUS_PROGRESSION)
                value = 5;
        }

        return value;
    }

    private String getDirection(String oldStatus, String newStatus)
    {
        String result = 'None';
        Integer oldStageValue = getStateValue(oldStatus);
        Integer newStageValue = getStateValue(newStatus);

        if (newStageValue > oldStageValue)
            result = 'Forward';
        else if (newStageValue < oldStageValue)
            result = 'Backward';

        return result;
    }

    private Status_Tracking_History__c createStatusTrackingHistory(Contact contact, DateTime now)
    {
        String status = contact.Status__c;
        String statusType = 'State Change';

        return createStatusTrackingHistory(contact, status, statusType, now);
    }

    private Status_Tracking_History__c createStatusTrackingHistory(Contact contact, String status, String statusType, DateTime now)
    {
        Status_Tracking_History__c history = new Status_Tracking_History__c();
        history.Contact_ID__c = contact.Id;
        history.OwnerId = contact.OwnerId;
        history.User__c = UserInfo.getName();
        history.StartDateTime__c = now;
        history.Status_Stage__c = status;
        history.Type__c = statusType;
        history.Department__c = 'Sales';

        return history;
    }
}