public class TermUpdateDatesBatch extends BaseBatchCommand
{
    String query = 'SELECT Id, Name, Status__c, FirstAdministrationDate__c, FirstCensusDate__c, FirstSubjectStartDate__c ' +
                   'FROM hed__Term__c ' +
                   'WHERE hed__Account__c != null AND (Status__c IN (\'Current\', \'Next\', \'Future\') OR FirstAdministrationDate__c = NULL OR FirstCensusDate__c = NULL OR FirstSubjectStartDate__c = NULL) ' +
                   'ORDER BY Name';


    public TermUpdateDatesBatch()
    {
    }

    public override Database.QueryLocator query(Database.BatchableContext context)
    {
        return Database.getQueryLocator(query);
    }

    public override void command(Database.BatchableContext context, List<sObject> scope)
    {
        List<hed__Term__c> updateTerms = new List<hed__Term__c>();
        Map<Id, hed__Term__c> termMap = new Map<Id, hed__Term__c>((List<hed__Term__c>)scope);
        List<AggregateResult> results =
            [
                SELECT hed__Term__c, MIN(AdministrationDate__c), MIN(CensusDate__c), MIN(hed__Start_Date__c)
                FROM hed__Course_Offering__c
                WHERE hed__Term__c IN : scope
                GROUP BY hed__Term__c
            ];

        for (AggregateResult result : results)
        {
            Boolean isDirty = false;
            Id key = (Id)result.get('hed__Term__c');
            Date adminDate = (Date)result.get('expr0');
            Date censusDate = (Date)result.get('expr1');
            Date startDate = (Date)result.get('expr2');
            hed__Term__c term = termMap.get(key);

            if (adminDate != null && adminDate != term.FirstAdministrationDate__c)
            {
                term.FirstAdministrationDate__c = adminDate;
                isDirty = true;
            }

            if (censusDate != null && censusDate != term.FirstCensusDate__c)
            {
                term.FirstCensusDate__c = censusDate;
                isDirty = true;
            }

            if (startDate != null && startDate != term.FirstSubjectStartDate__c)
            {
                term.FirstSubjectStartDate__c = startDate;
                isDirty = true;
            }

            if (isDirty)
                updateTerms.add(term);
        }

        if (updateTerms.size() > 0)
            update updateTerms;
    }

    public override void finalise(Database.BatchableContext BC)
    {
        database.executeBatch(new TermUpdateStatusBatch());
    }
}