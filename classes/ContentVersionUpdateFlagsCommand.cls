public class ContentVersionUpdateFlagsCommand extends BaseQueueableCommand 
{
    public ContentVersionUpdateFlagsCommand()
    {
        super(1);
    }

    protected override List<SObject> validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        List<ContentVersion> result = new List<ContentVersion>();

        for (ContentVersion contentVersion : (List<ContentVersion>)items)
        {
            if (triggerContext != Enums.TriggerContext.AFTER_INSERT)
                continue;

            if (contentVersion.Type__c != Constants.CONTENTVERSION_TYPE_LOO && contentVersion.Type__c != Constants.CONTENTVERSION_TYPE_TIMETABLE
                && contentVersion.Type__c != Constants.CONTENTVERSION_TYPE_INVOICE && contentVersion.Type__c != Constants.CONTENTVERSION_TYPE_WELCOMELETTER)
                continue;

            result.add(contentVersion);
        }

        return result;
    }

    protected override ActionResult command(List<SObject> items, Id jobId)
    {
        for (ContentVersion contentVersion : (List<ContentVersion>)items)
        {
            Set<Id> caseIds = new Set<Id>();
            Set<Id> opportunityIds = new Set<Id>();

            // Had to query db inside loop as ContentDocumentLink can be filtered only by a single id
            List<ContentDocumentLink> contentDocumentLinks = getDocumentLinks(contentVersion.ContentDocumentId);

            Map<Id, ContentDocumentLink> contentDocumentCaseLinkMap = new Map<Id, ContentDocumentLink>();
            Map<Id, ContentDocumentLink> contentDocumentOppLinkMap = new Map<Id, ContentDocumentLink>();

            for (ContentDocumentLink contentDocumentLink: contentDocumentLinks)
            {
                if (String.valueOf(contentDocumentLink.LinkedEntityId).startsWith(Constants.ENTITYID_CASE))
                {
                    caseIds.add(contentDocumentLink.LinkedEntityId);
                    contentDocumentCaseLinkMap.put(contentDocumentLink.ContentDocumentId, contentDocumentLink);
                }
                else if (String.valueOf(contentDocumentLink.LinkedEntityId).startsWith(Constants.ENTITYID_OPPORTUNITY))
                {
                    opportunityIds.add(contentDocumentLink.LinkedEntityId);
                    contentDocumentOppLinkMap.put(contentDocumentLink.ContentDocumentId, contentDocumentLink);
                }
            }

            List<Case> cases = getCases(caseIds);
            List<Opportunity> opportunities = getOpportunities(opportunityIds);

            if (contentVersion.Type__c == Constants.CONTENTVERSION_TYPE_LOO && contentDocumentCaseLinkMap.containsKey(contentVersion.ContentDocumentId))
            {
                ContentDocumentLink contentDocLink = contentDocumentCaseLinkMap.get(contentVersion.ContentDocumentId);
                Case documentCase = getDocumentCase(contentDocLink.LinkedEntityId, cases);

                if (documentCase == null)
                {
                    Log.info(this.className, 'command', 'No document case {0} found in db', new List<String> { documentCase.Id });
                    continue;
                }

                if (documentCase.OfferGeneratedDate__c == null)
                {
                    documentCase.OfferGeneratedDate__c = Datetime.now();
                    Log.info(this.className, 'command', 'Updating case {0} offer generated date', new List<String> { documentCase.Id });
                    uow.registerDirty(documentCase);
                }
            }
            else if (contentVersion.Type__c == Constants.CONTENTVERSION_TYPE_TIMETABLE && contentDocumentOppLinkMap.containsKey(contentVersion.ContentDocumentId))
            {
                ContentDocumentLink contentDocLink = contentDocumentOppLinkMap.get(contentVersion.ContentDocumentId);
                Opportunity opportunity = getDocumentOpportunity(contentDocLink.LinkedEntityId, opportunities);

                if (opportunity == null)
                {
                    Log.info(this.className, 'command', 'No document opportunity {0} found in db', new List<String> { opportunity.Id });
                    continue;
                }

                if (!opportunity.TimetableSent__c)
                {
                    opportunity.TimetableSent__c = true;
                    Log.info(this.className, 'command', 'Updating opportunity {0} Timetable Sent flag', new List<String> { opportunity.Id });
                    uow.registerDirty(opportunity);
                }
            }
            else if (contentVersion.Type__c == Constants.CONTENTVERSION_TYPE_INVOICE && contentDocumentCaseLinkMap.containsKey(contentVersion.ContentDocumentId))
            {
                ContentDocumentLink contentDocLink = contentDocumentCaseLinkMap.get(contentVersion.ContentDocumentId);
                Case documentCase = getDocumentCase(contentDocLink.LinkedEntityId, cases);

                if (documentCase == null)
                {
                    Log.info(this.className, 'command', 'No document case {0} found in db', new List<String> { documentCase.Id });
                    continue;
                }

                if (!documentCase.Invoice_Generated__c)
                {
                    documentCase.Invoice_Generated__c = true;
                    Log.info(this.className, 'command', 'Updating case {0} invoice generated value', new List<String> { documentCase.Id });
                    uow.registerDirty(documentCase);
                }
            }
            else if (contentVersion.Type__c == Constants.CONTENTVERSION_TYPE_WELCOMELETTER && contentDocumentCaseLinkMap.containsKey(contentVersion.ContentDocumentId))
            {
                ContentDocumentLink contentDocLink = contentDocumentCaseLinkMap.get(contentVersion.ContentDocumentId);
                Case documentCase = getDocumentCase(contentDocLink.LinkedEntityId, cases);

                if (documentCase == null)
                {
                    Log.info(this.className, 'command', 'No document case {0} found in db', new List<String> { documentCase.Id });
                    continue;
                }

                if (!documentCase.Welcome_Letter_Sent__c)
                {
                    documentCase.Welcome_Letter_Sent__c = true;
                    Log.info(this.className, 'command', 'Updating case {0} welcome letter sent value', new List<String> { documentCase.Id });
                    uow.registerDirty(documentCase);
                }
            }
        }

        return new ActionResult(ResultStatus.SUCCESS);
    }

    private List<ContentDocumentLink> getDocumentLinks(Id documentId)
    {
        return
        [
            SELECT Id, ContentDocumentId, LinkedEntityId
            FROM ContentDocumentLink
            WHERE ContentDocumentId = :documentId
        ];
    }

    private List<Case> getCases(Set<Id> caseIds)
    {
        return
        [
            SELECT Id, OfferGeneratedDate__c, Invoice_Generated__c, Welcome_Letter_Sent__c
            FROM Case
            WHERE Id IN :caseIds
        ];
    }

    private Case getDocumentCase(Id caseId, List<Case> cases)
    {
        for (Case currentCase : cases)
            if (currentCase.Id == caseId)
                return currentCase;

        return null;
    }

    private List<Opportunity> getOpportunities(Set<Id> opportunityIds)
    {
        return
        [
            SELECT Id, TimetableSent__c
            FROM Opportunity
            WHERE Id IN :opportunityIds
        ];
    }

    private Opportunity getDocumentOpportunity(Id oppId, List<Opportunity> opps)
    {
        for (Opportunity opp : opps)
            if (opp.Id == oppId)
                return opp;

        return null;
    }
}