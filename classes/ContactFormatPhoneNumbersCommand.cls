public class ContactFormatPhoneNumbersCommand extends BaseTriggerCommand
{
    protected override List<SObject> validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        List<Contact> result = new List<Contact>();

        for (Contact item : (List<Contact>)items)
        {
            if (triggerContext == Enums.TriggerContext.BEFORE_INSERT)
            {
                result.add(item);
                continue;
            }

            Contact current = (Contact) oldMap.get(item.Id);

            if (item.Phone != current.Phone 
                || item.MobilePhone != current.MobilePhone 
                || item.OtherPhone != current.OtherPhone
                || item.HomePhone != current.HomePhone
                || item.hed__WorkPhone__c != current.hed__WorkPhone__c
                || String.isEmpty(item.MobileLocale__c)
                || item.MobilePhoneCountry__c != current.MobilePhoneCountry__c)
            {
                result.add(item);
            }
        }

        return result;
    }

    protected override ActionResult command(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        for (Contact item : (List<Contact>)items)
        {
            if (String.isBlank(item.Backup_Phone__c))
                item.Backup_Phone__c = item.Phone;

            if (String.isBlank(item.Backup_Mobile_Phone__c))
                item.Backup_Mobile_Phone__c = item.MobilePhone;

            if (String.isBlank(item.BackupOtherPhone__c))
                item.BackupOtherPhone__c = item.OtherPhone;

            if (String.isBlank(item.BackupHomePhone__c))
                item.BackupHomePhone__c = item.HomePhone;

            if (String.isBlank(item.BackupWorkPhone__c))
                item.BackupWorkPhone__c = item.hed__WorkPhone__c;

            item.Phone = PhoneNumberUtilities.cleanNumber(item.Phone);
            item.MobilePhone = PhoneNumberUtilities.cleanNumber(item.MobilePhone);
            item.OtherPhone = PhoneNumberUtilities.cleanNumber(item.OtherPhone);
            item.HomePhone = PhoneNumberUtilities.cleanNumber(item.HomePhone);
            item.hed__WorkPhone__c = PhoneNumberUtilities.cleanNumber(item.hed__WorkPhone__c);

            if (triggerContext == Enums.TriggerContext.BEFORE_INSERT || String.isEmpty(item.MobileLocale__c) || item.MobilePhoneCountry__c != ((Contact) oldMap.get(item.Id)).MobilePhoneCountry__c)
            {
                String code = PhoneNumberUtilities.getCountryCode(item.MobilePhoneCountry__c);

                if (!String.isEmpty(code))
                {
                    item.MobileLocale__c = code;
                }
            }
        }

        return new ActionResult();
    }
}