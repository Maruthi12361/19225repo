@isTest
public class ContentVersionCreateCommandTests
{
   @testSetup
    public static void setup()
    {
        TestUtilities.setupHEDA();
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createProspectHot(user);
        TestDataOpportunityMock.createApplyingAcquisition(user, contact);
    }

    @isTest
    static void CreateFileWithSave_DocumentCreatedCorrectly()
    {
        // Arrange
        String fileName = 'Test File.txt';
        String type = 'Proof of ID';
        String fileContent = 'Test Test and Test\nTest';
        Blob data = Blob.valueOf(fileContent);
        List<Opportunity> opps = [SELECT Id, OwnerId FROM Opportunity LIMIT 1];
        Id parentId = opps[0].Id;
        Id ownerId = opps[0].OwnerId;

        // Act
        ActionResult result = ContentDocumentService.createFile(fileName, type, data, parentId, ownerId, null);

        // Assert
        System.assertNotEquals(null, result.resultId);
        List<ContentDocumentLink> cdLinks =
        [
            SELECT Id, ContentDocumentId, ContentDocument.LatestPublishedVersion.Title, ContentDocument.LatestPublishedVersion.VersionData,
                ContentDocument.LatestPublishedVersion.Type__c, ContentDocument.OwnerId
            FROM ContentDocumentLink
            WHERE LinkedEntityId = : parentId
        ];

        System.assertEquals(1, cdLinks.size());

        ContentDocument document = cdLinks[0].ContentDocument;
        System.assertEquals(result.resultId, document.LatestPublishedVersion.Id);
        System.assertEquals(fileName, document.LatestPublishedVersion.Title);
        System.assertEquals(type, document.LatestPublishedVersion.Type__c);
        System.assertEquals(fileContent, document.LatestPublishedVersion.VersionData.toString());
        System.assertEquals(ownerId, document.OwnerId);
    }
}