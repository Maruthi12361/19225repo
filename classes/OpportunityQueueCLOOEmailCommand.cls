public class OpportunityQueueCLOOEmailCommand extends BaseQueueableCommand
{
    public OpportunityQueueCLOOEmailCommand()
    {
        super(50, 5);
    }

    public override List<SObject> validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        return items;
    }

    public override ActionResult command(List<SObject> items, Id jobId)
    {
        EmailTemplate emailTemplateGCM = EmailService.getEmailTemplate('Conditional Letter Of Offer GCM');
        EmailTemplate emailTemplateMBA = EmailService.getEmailTemplate('Conditional Letter Of Offer MBA');

        List<Opportunity> opportunities =
        [
            SELECT Id, Contact__c, Contact__r.Name, Contact__r.Email, Contact__r.ApplicationCourse__c, Owner.Email, Owner.Name
            FROM Opportunity
            WHERE Id IN : (new Map<Id, SObject>(items)).keySet() AND Contact__c != NULL AND RecordType.DeveloperName =: Constants.RECORDTYPE_OPPORTUNITY_ACQUISITION
        ];

        List<Task> newTasks = new List<Task>();
        List<Opportunity> updates = new List<Opportunity>();

        Map<Id, Messaging.SingleEmailMessage> messageMap = new Map<Id, Messaging.SingleEmailMessage>();
        for (Opportunity opportunity : opportunities)
        {
            String course = opportunity.Contact__r.ApplicationCourse__c;
            EmailTemplate emailTemplate = course == null ? null : ((course.startsWithIgnoreCase('GCM')) ? emailTemplateGCM : emailTemplateMBA);

            if (emailTemplate == null)
            {
                Log.error(this.className, 'command', 'Failed to send CLOO to {0}<{1}> as there was no matching email template for {2} course.', new List<String> {opportunity.Contact__r.Name, opportunity.Contact__r.Email, course });
                continue;
            }

            Messaging.SingleEmailMessage mail = Messaging.renderStoredEmailTemplate(emailTemplate.Id, opportunity.Contact__c, opportunity.Id);
            messageMap.put(opportunity.Id, mail);
            updates.add(opportunity);
            Task task = createActivity(opportunity, mail.getSubject(), mail.getPlainTextBody());
            newTasks.add(task);
        }

        if (newTasks.size() > 0)
            insert newTasks;

        List<Opportunity> oppsForUpdate = [SELECT Id FROM Opportunity WHERE Id IN : (new Map<Id, SObject>(updates)).keySet() FOR UPDATE];

        for (Opportunity opp: oppsForUpdate)
            opp.CLOOSentDate__c = DateTime.now();

        if (oppsForUpdate.size() > 0)
            update oppsForUpdate;

        for (Opportunity opportunity : opportunities)
        {
            Messaging.SingleEmailMessage mail = messageMap.get(opportunity.Id);

            if (mail == null)
                continue;

            if (trigger.isExecuting)
                Log.info(this.className, 'command', 'this queueable command cannot be run in trigger mode');
            else
                sendEmail(opportunity.Id, mail.getSubject(), mail.getHtmlBody());
        }

        return new ActionResult(ResultStatus.SUCCESS);
    }

    @future(callout = true)
    private static void sendEmail(Id opportunityId, String subject, String body)
    {
        Opportunity opportunity =
        [
            SELECT Id, Contact__r.Name, Contact__r.Email, Owner.Name, Owner.Email, Contact__r.FinalCourseConsultant__c, Contact__r.FinalCourseConsultant__r.Name, Contact__r.FinalCourseConsultant__r.Email
            FROM Opportunity
            WHERE Id = : opportunityId
        ];

        List<ViewModelsMail.Recipient> ccEmail = new List<ViewModelsMail.Recipient>{ new ViewModelsMail.Recipient(opportunity.Owner.Name, opportunity.Owner.Email) };
        if (opportunity.Contact__r.FinalCourseConsultant__c != null)
            ccEmail.add(new ViewModelsMail.Recipient(opportunity.Contact__r.FinalCourseConsultant__r.Name, opportunity.Contact__r.FinalCourseConsultant__r.Email));

        ActionResult result = EmailService.sendToRecipient(Constants.APIPROVIDER_DEFAULT, opportunity.Contact__r.Email, opportunity.Contact__r.Name, subject, opportunity.Owner.Email, opportunity.Owner.Name, null, body, ccEmail);

        if (result.isError)
        {
            User sysAdmin = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
            String message = 'Failed to send CLOO to ' + opportunity.Contact__r.Email + ' as ' + result.message;
            EmailService.sendLegacy(sysAdmin.Email, 'Failed to send CLOO', message);
            Log.error('OpportunityQueueCLOOEmailCommand', 'sendEmail', message);
            Log.save();
        }
    }

    private Task createActivity(Opportunity opportunity, String subject, String comment)
    {
        Task task = new Task(Status = 'Completed', WhatId = opportunity.Id, WhoId = opportunity.Contact__c);
        task.RecordTypeId = RecordTypeUtils.getRecordTypeId(Constants.RECORDTYPE_TASK_CLIENTCONTACT, SObjectType.Task);
        task.ActivityDate = Date.today();
        task.OwnerId = opportunity.OwnerId;
        task.TaskSubtype = 'Email';
        task.Subject = subject;
        task.Description = comment;
        task.Type = null;

        return task;
    }
}