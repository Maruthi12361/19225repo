@isTest
public class ContactUploadAttachmentCommandTest
{
    @testSetup
    public static void setup()
    {
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact contact = TestDataContactMock.createProspectCold(user, '0455548889');
        Opportunity opp = TestDataOpportunityMock.create(user, contact, 'Applying', true);
    }

    @isTest
    static void BlankIdentifier_Error()
    {
        // Arrange
        ViewModelsFile.Upload param = new ViewModelsFile.Upload();
        param.Reference = '';
        param.Content = null;

        // Act
        ActionResult result = ContactService.uploadAttachment(param);

        // Assert
        System.assertEquals('Reference parameter cannot be empty or null', result.Message);
    }

    @isTest
    static void BlankPayload_Error()
    {
        // Arrange
        ViewModelsFile.Upload param = new ViewModelsFile.Upload();
        param.Reference = 'myemail@email.com';
        param.Content = Blob.valueOf('');

        // Act
        ActionResult result = ContactService.uploadAttachment(param);

        // Assert
        System.assertEquals('FileContent parameter cannot be empty or null', result.Message);
    }

    @isTest
    static void NonExistantContact_Error()
    {
        // Arrange
        ViewModelsFile.Upload param = new ViewModelsFile.Upload();

        param.Reference = 'myemail@email.com';
        param.FileName = 'Test.txt';
        param.Type = 'Proof of ID';
        param.Content = Blob.valueOf('Test'); //JSON.serialize(attachmentMap);

        // Act
        ActionResult result = ContactService.uploadAttachment(param);

        // Assert
        System.assertEquals('Opportunity does not exist', result.Message);
    }

    @isTest
    static void MissingFilenameValue_Error()
    {
        // Arrange
        Contact contact = [SELECT Id, PersonalEmail__c FROM Contact Limit 1];

        ViewModelsFile.Upload param = new ViewModelsFile.Upload();
        param.Reference = contact.PersonalEmail__c;
        param.Description = 'Test';
        param.FileName = '';
        param.Content = Blob.valueOf('Test');

        // Act
        ActionResult result = ContactService.uploadAttachment(param);

        // Assert
        System.assertEquals('Filename value cannot be null or empty', result.Message);
    }

    @isTest
    static void MissingDescriptionValue_Error()
    {
        // Arrange
        Contact contact = [SELECT Id, PersonalEmail__c FROM Contact Limit 1];

        ViewModelsFile.Upload param = new ViewModelsFile.Upload();
        param.Reference = contact.PersonalEmail__c;
        param.FileName = 'Test.txt';
        param.Type = '';
        param.Content = Blob.valueOf('Test');

        // Act
        ActionResult result = ContactService.uploadAttachment(param);

        // Assert
        System.assertEquals('Type value cannot be null or empty', result.Message);
    }

    @isTest
    static void InvalidFileType_Error()
    {
        // Arrange
        Contact contact = [SELECT Id, PersonalEmail__c FROM Contact Limit 1];

        ViewModelsFile.Upload param = new ViewModelsFile.Upload();
        param.Reference = contact.PersonalEmail__c;
        param.FileName = 'Test';
        param.Type = 'Proof of ID';
        param.Content = Blob.valueOf('Test File Content');

        // Act
        ActionResult result = ContactService.uploadAttachment(param);

        // Assert
        System.assertEquals('Invalid file type', result.Message);
    }

    @isTest
    static void UnsupportefFileExtension_Error()
    {
        // Arrange
        Contact contact = [SELECT Id, PersonalEmail__c FROM Contact Limit 1];

        ViewModelsFile.Upload param = new ViewModelsFile.Upload();
        param.Reference = contact.PersonalEmail__c;
        param.FileName = 'Test.js';
        param.Type = 'Proof of ID';
        param.Content = Blob.valueOf('Test File content');

        // Act
        ActionResult result = ContactService.uploadAttachment(param);

        // Assert
        System.assertEquals('Unsupported file extension', result.Message);
    }

    @isTest
    static void NewAttachmentEmailIdentifier_SuccessfullyCreated()
    {
        // Arrange
        Contact contact = [SELECT Id, PersonalEmail__c FROM Contact Limit 1];
        Opportunity opp = [SELECT Id FROM Opportunity WHERE Contact__c =:contact.Id];

        ViewModelsFile.Upload param = new ViewModelsFile.Upload();
        param.Reference = contact.PersonalEmail__c;
        param.FileName = 'Test.txt';
        param.Type = 'Proof of ID';
        param.Content = Blob.valueOf('Test File content');

        // Act
        ActionResult result = ContactService.uploadAttachment(param);
        List<ViewModelsFile.Details> files = ContentDocumentService.getFiles(opp.Id);

        // Assert
        System.assertEquals('', result.Message);
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Unexpected status value');
        System.assertEquals(1, files.size());
        System.assertEquals('Test.txt', files[0].Name, 'Unexpected name value');
        System.assertEquals('Proof of ID', files[0].Type, 'Unexpected type value');
    }

    @isTest
    static void NewAttachmentEncryptedIdentifier_SuccessfullyCreated()
    {
        // Arrange
        Contact contact = [SELECT Id FROM Contact Limit 1];
        Opportunity opp = [SELECT Id FROM Opportunity WHERE Contact__c =:contact.Id];

        ViewModelsFile.Upload param = new ViewModelsFile.Upload();
        param.Reference = EncryptionUtil.Encrypt(contact.Id);
        param.FileName = 'Test.docx';
        param.Type = 'Proof of ID';
        param.Content = Blob.valueOf('Encrypted File content');

        // Act
        ActionResult result = ContactService.uploadAttachment(param);
        List<ViewModelsFile.Details> files = ContentDocumentService.getFiles(opp.Id);

        // Assert
        System.assertEquals('', result.Message);
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Unexpected status value');
        System.assertEquals(1, files.size());
        System.assertEquals('Test.docx', files[0].Name, 'Unexpected name value');
        System.assertEquals('Proof of ID', files[0].Type, 'Unexpected type value');
    }
}