public class CaseEnrolmentDeleteChildCommand extends BaseTriggerCommand
{
    public override List<SObject> validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        List<CaseEnrolment__c> result = new List<CaseEnrolment__c>();

        for (CaseEnrolment__c item : (List<CaseEnrolment__c>)items)
        {
            if (triggerContext == Enums.TriggerContext.AFTER_DELETE)
            {
                if (item.SubjectEnrolment__c != null)
                    result.add(item);
            }
        }

        return result;
    }

    public override ActionResult command(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        Set<Id> caseIds = new Set<Id>();
        Set<Id> enrolmentIds = new Set<Id>();

        for (CaseEnrolment__c item : (List<CaseEnrolment__c>)items)
        {
            if (item.Case__c != null)
                caseIds.add(item.Case__c);
        }
        
        Map<Id, Case> caseMap = new Map<Id, Case>([SELECT Id FROM Case WHERE Id IN : caseIds AND RecordType.DeveloperName = : Constants.RECORDTYPE_CASE_EXEMPTIONS]);
        
        for (CaseEnrolment__c item : (List<CaseEnrolment__c>)items)
        {
            if (item.Case__c == null || caseMap.containsKey(item.Case__c))
                enrolmentIds.add(item.SubjectEnrolment__c);
        }

        List<hed__Course_Enrollment__c> enrolments = [SELECT Id FROM hed__Course_Enrollment__c WHERE Id IN : enrolmentIds AND RecordType.DeveloperName = : Constants.RECORDTYPE_COURSEENROLLMENT_PROVISIONAL];

        if (enrolments.size() > 0)
            database.delete(enrolments);

        return new ActionResult();
    }
}