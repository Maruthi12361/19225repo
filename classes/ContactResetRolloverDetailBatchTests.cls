@isTest
public class ContactResetRolloverDetailBatchTests 
{
    private static String cerebroID = 'A001641336';
    
    @testSetup
    static void setup()
    {
        // Arrange
        TestUtilities.setupHEDA();
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        TestDataContactMock.createRolloverStudent(user, cerebroID);
    }
    
    @isTest
    static void ExecuteBatchWithoutParameter_ResetsAllStudents() 
    {
        // Act        
        Test.startTest();
        Database.executeBatch(new ContactResetRolloverDetailBatch());
        Test.stopTest();        
		
        // Assert
        Contact contact = 
            [
                SELECT Id,Current_Course_Status__c,Course_Start_Date__c,Next_Followup_Date__c,FeeHelp_Eligibility__c
                FROM Contact
                WHERE Cerebro_Student_ID__c = : cerebroID
            ];
        
        system.assertEquals(null, contact.Current_Course_Status__c, 'Wrong value for Current_Course_Status__c');
        system.assertEquals(null, contact.FeeHelp_Eligibility__c, 'Wrong value for FeeHelp_Eligibility__c');
        system.assertEquals(null, contact.Course_Start_Date__c, 'Wrong value for Course_Start_Date__c');
        system.assertEquals(null, contact.Next_Followup_Date__c, 'Wrong value for Next_Followup_Date__c');
    }
    
    @isTest
    static void ExecuteBatchWithParameter_ResetsIdentifiedStudents() 
    {
        // Act
        Test.startTest();
        Database.executeBatch(new ContactResetRolloverDetailBatch('\'' + cerebroID + '\''));
        Test.stopTest();        
		
        // Assert
        Contact contact = 
            [
                SELECT Id,Current_Course_Status__c,Course_Start_Date__c,Next_Followup_Date__c,FeeHelp_Eligibility__c
                FROM Contact 
                WHERE Cerebro_Student_ID__c = : cerebroID
            ];
        
        system.assertEquals(null, contact.Current_Course_Status__c, 'Wrong value for Current_Course_Status__c');
        system.assertEquals(null, contact.FeeHelp_Eligibility__c, 'Wrong value for FeeHelp_Eligibility__c');
        system.assertEquals(null, contact.Course_Start_Date__c, 'Wrong value for Course_Start_Date__c');
        system.assertEquals(null, contact.Next_Followup_Date__c, 'Wrong value for Next_Followup_Date__c');
    }
}