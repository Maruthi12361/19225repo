public abstract class BaseQuery
{
    protected abstract List<SObject> query();
}