@isTest
public class ContactSendPassResetQueuedCommandTests
{
    @testSetup
    static void setup()
    {
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createProspectCold(user, 'John', 'Test','61408815278', 'john.test@yahoo.com', 'A0019789789');
    }
    
    @isTest
    static void InvalidTemplate_EmailNotSent()
    {
        // Arrange
        String errorMsg = '';
        ContactSendPassResetQueuedCommand.templateName = 'Non existant template';
        Contact contact = [SELECT Id, FirstName, LastName, Email FROM Contact LIMIT 1];

        // Act
        Test.startTest();
        try 
        {
            ActionResult result = ContactService.sendPasswordResetQueuedEmail(contact);
        } 
        catch (Exception ex)
        {
            errorMsg = ex.getMessage();
        }
        Test.stopTest();

        // Assert
        System.assertEquals(true, errorMsg.startsWith('No email template found'), 'Unexpected error message');
    }

    @isTest
    static void ValidProspect_EmailSuccessfullySent()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        Contact contact = [SELECT Id, FirstName, LastName, Email FROM Contact LIMIT 1];

        // Act
        Test.startTest();
        ActionResult result = ContactService.sendPasswordResetQueuedEmail(contact);
        Test.stopTest();

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.status, 'Unexpected status');
        System.assertEquals(1, TestHttpCalloutMock.requests.size());
    }
}