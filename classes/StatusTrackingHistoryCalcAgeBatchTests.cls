@isTest
public class StatusTrackingHistoryCalcAgeBatchTests
{
    @testSetup
    static void setup()
    {
        TestUtilities.setupHEDA();
    }

    @isTest
    static void TwoWeekdayClosedSTHRecords_BatchUpdatesBusinessHoursAndUpdatesParentCase()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact prospect = TestDataContactMock.createProspectHot(user);
        Case parentCase = TestDataCaseMock.createApplication(user, prospect);
        Integer status1InMinutes = 60;
        Integer status2InMinutes = 240;
        Datetime startTime = Datetime.newInstance(2019, 8, 7, 9, 0, 0); // This is a Wednesday, workday
        Datetime endTime = startTime.addMinutes(status1InMinutes);      // with both records tracked over
        Datetime endTime2 = endTime.addMinutes(status2InMinutes);       // course of the single/same day
        Status_Tracking_History__c first = TestDataStatusTrackingHistoryMock.createStatusTrackingHistory(user, parentCase, true, startTime, endTime);
        Status_Tracking_History__c second = TestDataStatusTrackingHistoryMock.createStatusTrackingHistory(user, parentCase, true, endTime, endTime2);
        List<Status_Tracking_History__c> items = new List<Status_Tracking_History__c>{ first, second };

        // Act
        Test.startTest();
        Database.executeBatch(new StatusTrackingHistoryCalcAgeBatch());
        Test.stopTest();

        // Assert
        List<Status_Tracking_History__c> histories = [SELECT Id, BusinessMinutesInStage__c FROM Status_Tracking_History__c WHERE CaseId__c =: parentCase.Id ORDER BY StartDateTime__c];
        Case updatedCase = [SELECT Id, AgeBusinessMinutes__c FROM Case WHERE Id =: parentCase.Id];

        // Since no STH.businessMinutesInStage has been set yet then all ages on both STH and Case must be calculated during the batch
        System.assertEquals(2, histories.size());
        System.assertEquals(status1InMinutes, histories[0].BusinessMinutesInStage__c);
        System.assertEquals(status2InMinutes, histories[1].BusinessMinutesInStage__c);
        System.assertEquals(status1InMinutes + status2InMinutes, updatedCase.AgeBusinessMinutes__c);
    }

    @isTest
    static void TwoHistoricWeekdayClosedSTHRecords_BatchDoesntUpdateBusinessHoursOrParentCase()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact prospect = TestDataContactMock.createProspectHot(user);
        Case parentCase = TestDataCaseMock.createApplication(user, prospect);
        parentCase.AgeBusinessMinutes__c = 20;
        update parentCase;

        Integer status1InMinutes = 10;
        Integer status2InMinutes = 10;
        Datetime startTime = Datetime.newInstance(2019, 8, 1, 9, 0, 0); // This is a Thursday, workday with both records tracked over the
        Datetime endTime = startTime.addMinutes(status1InMinutes + 33); // course of the same day. Note: this unlikely situation of having
        Datetime endTime2 = endTime.addMinutes(status2InMinutes + 33);  // updated start/endTime is only to test no updates occur during batch
        Status_Tracking_History__c first = TestDataStatusTrackingHistoryMock.createStatusTrackingHistory(user, parentCase, true, startTime, endTime);
        Status_Tracking_History__c second = TestDataStatusTrackingHistoryMock.createStatusTrackingHistory(user, parentCase, true, endTime, endTime2);
        first.BusinessMinutesInStage__c = status1InMinutes;
        second.BusinessMinutesInStage__c = status2InMinutes;
        List<Status_Tracking_History__c> items = new List<Status_Tracking_History__c>{ first, second };
        update items;

        // Act
        Test.startTest();
        Database.executeBatch(new StatusTrackingHistoryCalcAgeBatch());
        Test.stopTest();

        // Assert
        List<Status_Tracking_History__c> histories = [SELECT Id, BusinessMinutesInStage__c FROM Status_Tracking_History__c WHERE CaseId__c =: parentCase.Id ORDER BY StartDateTime__c];
        Case updatedCase = [SELECT Id, AgeBusinessMinutes__c FROM Case WHERE Id =: parentCase.Id];

        // Since STH.businessMinutesInStage has already been set and this is a historical ( < YESTERDAY ) non-Closed record then DO NOT recalculate during batch
        System.assertEquals(2, histories.size());
        System.assertEquals(status1InMinutes, histories[0].BusinessMinutesInStage__c);
        System.assertEquals(status2InMinutes, histories[1].BusinessMinutesInStage__c);
        System.assertEquals(status1InMinutes + status2InMinutes, updatedCase.AgeBusinessMinutes__c);
    }

    @isTest
    static void WeekdayClosedAndOpenSTHRecords_BatchUpdatesBusinessHoursOfOpenRecordAndParentCase()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact prospect = TestDataContactMock.createProspectHot(user);
        Case parentCase = TestDataCaseMock.createApplication(user, prospect);
        Integer originalStatus1Time = 20;
        parentCase.AgeBusinessMinutes__c = originalStatus1Time;
        update parentCase;

        Integer status1InMinutes = 60;
        Integer status2InMinutes = 240;
        Datetime startTime = Datetime.newInstance(2019, 8, 7, 9, 0, 0); // This is a Wednesday, workday
        Datetime endTime = startTime.addMinutes(status1InMinutes);      // with both records tracked over
        Datetime endTime2 = endTime.addMinutes(status2InMinutes);       // course of the single/same day
        Status_Tracking_History__c first = TestDataStatusTrackingHistoryMock.createStatusTrackingHistory(user, parentCase, true, startTime, endTime);
        Status_Tracking_History__c second = TestDataStatusTrackingHistoryMock.createStatusTrackingHistory(user, parentCase, true, endTime, endTime2);
        first.BusinessMinutesInStage__c = originalStatus1Time;
        update first;
        List<Status_Tracking_History__c> items = new List<Status_Tracking_History__c>{ first, second };

        // Act
        Test.startTest();
        Database.executeBatch(new StatusTrackingHistoryCalcAgeBatch());
        Test.stopTest();

        // Assert
        List<Status_Tracking_History__c> histories = [SELECT Id, BusinessMinutesInStage__c FROM Status_Tracking_History__c WHERE CaseId__c =: parentCase.Id ORDER BY StartDateTime__c];
        Case updatedCase = [SELECT Id, AgeBusinessMinutes__c FROM Case WHERE Id =: parentCase.Id];

        System.assertEquals(2, histories.size());
        System.assertEquals(originalStatus1Time, histories[0].BusinessMinutesInStage__c);  //do not update the first as old record > yesterday and BusinessMinutesInStage__c > null
        System.assertEquals(status2InMinutes, histories[1].BusinessMinutesInStage__c);
        System.assertEquals(originalStatus1Time + status2InMinutes, updatedCase.AgeBusinessMinutes__c);
    }

    @isTest
    static void TwoWeekendClosedSTHRecords_BatchUpdatesBusinessHoursAndUpdatesParentCaseWithZeroHours()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact prospect = TestDataContactMock.createProspectHot(user);
        Case parentCase = TestDataCaseMock.createApplication(user, prospect);
        parentCase.AgeBusinessMinutes__c = 20;
        update parentCase;

        Integer status1InMinutes = 60;
        Integer status2InMinutes = 240;
        Datetime startTime = Datetime.newInstance(2019, 8, 4, 9, 0, 0); // This is a Sunday, non-workday
        Datetime endTime = startTime.addMinutes(status1InMinutes);      // with both records tracked over
        Datetime endTime2 = endTime.addMinutes(status2InMinutes);       // course of the single/same day
        Status_Tracking_History__c first = TestDataStatusTrackingHistoryMock.createStatusTrackingHistory(user, parentCase, true, startTime, endTime);
        Status_Tracking_History__c second = TestDataStatusTrackingHistoryMock.createStatusTrackingHistory(user, parentCase, true, endTime, endTime2);
        List<Status_Tracking_History__c> items = new List<Status_Tracking_History__c>{ first, second };

        // Act
        Test.startTest();
        Database.executeBatch(new StatusTrackingHistoryCalcAgeBatch());
        Test.stopTest();

        // Assert
        List<Status_Tracking_History__c> histories = [SELECT Id, BusinessMinutesInStage__c FROM Status_Tracking_History__c WHERE CaseId__c =: parentCase.Id ORDER BY StartDateTime__c];
        Case updatedCase = [SELECT Id, AgeBusinessMinutes__c FROM Case WHERE Id =: parentCase.Id];

        System.assertEquals(2, histories.size());
        System.assertEquals(0, histories[0].BusinessMinutesInStage__c);
        System.assertEquals(0, histories[1].BusinessMinutesInStage__c);
        System.assertEquals(0, updatedCase.AgeBusinessMinutes__c);
    }
}