global class SystemHealthCheckSchedule implements Schedulable
{
    global void execute(SchedulableContext ctx)
    {
        // To extend with new Health Checks simply add a new QueryResult object for each check against the Salesforce Environment,
        // with the required format/structure following the other methods as examples, and adding to the List in the Service call.
        SystemService.runSystemHealthCheck(new List<QueryResult>
        {
            checkInactiveContactOwners(),
            checkInactiveOpportunityOwners(),
            checkInactiveCaseOwners(),
            checkMultipleBreakSubjectOfferingsForTerm(),
            checkMultipleVirtualSubjectOfferingsForTerm()
        });
    }

    private QueryResult checkInactiveContactOwners()
    {
        return new QueryResult
        (
            'Check Inactive Contact Owners',
            'Checks that for each Contact (not including those with a status__c \'Disqualilfied\') the Owner is not inactive as these contacts should be re-assigned (by DoD jobs) to a new, Active Owner to make sure they are being handled by someone. Groups results based on the RecordType and status__c.',
            new List<String>{ 'Total', 'RecordType', 'Status__c' },
            [
                SELECT count(Id) Total, RecordType.DeveloperName RecordType, Status__c
                FROM Contact
                WHERE Status__c != 'Disqualified' AND Owner.isActive = false
                GROUP BY RecordType.DeveloperName, status__c
                ORDER BY count(Id) DESC
            ]
        );
    }

    private QueryResult checkInactiveOpportunityOwners()
    {
        return new QueryResult
        (
            'Check Inactive Opportunity Owners',
            'Checks that for each Opportunity (that is not Closed, ie: in Stages \'Closed Lost\' or \'Closed Won\') the Owner is not inactive as these Opportunities should be re-assigned (by DoD jobs) to a new, Active Owner to make sure they are being handled by someone. Groups results based on the RecordType and StageName.',
            new List<String>{ 'Total', 'RecordType', 'StageName' },
            [
                SELECT count(Id) Total, RecordType.DeveloperName RecordType, StageName
                FROM Opportunity
                WHERE isClosed = false AND Owner.isActive = false
                GROUP BY RecordType.DeveloperName, StageName
                ORDER BY count(Id) DESC
            ]
        );
    }

    private QueryResult checkInactiveCaseOwners()
    {
        return new QueryResult
        (
            'Check Inactive Case Owners',
            'Checks that for each Case (that is not Closed) the Owner is not inactive as these Cases should be re-assigned (by a manual DB job) to a new, Active Owner to make sure they are owned by someone. Groups results based on the RecordType, department__c and Status.',
            new List<String>{ 'Total', 'RecordType', 'Department', 'Status' },
            [
                SELECT count(Id) Total, RecordType.DeveloperName RecordType, department__c Department, Status
                FROM Case
                WHERE isClosed = false AND Owner.isActive = false
                GROUP BY RecordType.DeveloperName, department__c, status
                ORDER BY count(Id) DESC
            ]
        );
    }

    private QueryResult checkMultipleBreakSubjectOfferingsForTerm()
    {
        return new QueryResult
        (
            'Check Multiple Break Subject Offerings Per Term',
            'Checks that for each Term there is at most 1 Break Subject Offering. More than 1 is an invalid state and will need to be merged together into a single record.',
            new List<String>{ 'Total', 'TermName' },
            [
                SELECT count(Id) Total, hed__Term__r.Name TermName
                FROM hed__Course_Offering__c
                WHERE Type__c = 'Break'
                GROUP BY hed__Term__r.Name
                HAVING count(Id) > 1
                ORDER BY count(Id) DESC
            ]
        );
    }

    private QueryResult checkMultipleVirtualSubjectOfferingsForTerm()
    {
        return new QueryResult
        (
            'Check Multiple Virtual Subject Offerings Per Virtual Term and Subject',
            'Checks that for each Term and for each Subject that there is at most 1 Virtual Subject Offering. More than 1 is an invalid state and will need to be merged together into a single record.',
            new List<String>{ 'Total', 'TermName', 'SubjectName', 'SubjectCode' },
            [
                SELECT count(Id) Total, hed__Term__r.Name TermName, hed__Course__r.Name SubjectName, hed__Course__r.hed__Course_ID__c SubjectCode
                FROM hed__Course_Offering__c
                WHERE Type__c = 'Virtual'
                AND SubjectVersionIsPrimary__c = true
                GROUP BY hed__Term__r.Name, hed__Course__r.Name, hed__Course__r.hed__Course_ID__c
                HAVING count(Id) > 1
                ORDER BY count(Id) DESC
            ]
        );
    }
}