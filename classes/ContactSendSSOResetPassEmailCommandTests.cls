@isTest
public class ContactSendSSOResetPassEmailCommandTests
{

    @isTest
    static void InvalidTemplate_EmailNotFoundErrorReturned()
    {
        // Arrange
        String errorMsg = '';
        ContactSendSSOResetPassEmailCommand.templateName = 'Non existant template';

        // Act
        Test.startTest();
        try
        {
            ActionResult result = ContactService.sendSSOResetPasswordEmail(null);
        } 
        catch (Exception ex)
        {
            errorMsg = ex.getMessage();
        }
        Test.stopTest();

        // Assert
        System.assertEquals(true, errorMsg.startsWith('No email template found'), 'Unexpected email not found error message');
    }

    @isTest
    static void NullParameter_NullParameterErrorReturned()
    {
        // Arrange
        String errorMessage = '';
        String expectedErrorMessage = String.format(Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Param object' });

        // Act
        try
        {
            ActionResult result = ContactService.sendSSOResetPasswordEmail(null);
        }
        catch (Exception ex)
        {
            errorMessage = ex.getMessage();
        }

        // Assert
        System.assertEquals(errorMessage, expectedErrorMessage, 'Unexpected null parameter error message');
    }


    @isTest
    static void EmptyContactId_EmptyContactIdErrorReturned()
    {
        // Arrange
        String errorMessage = '';
        ViewModelsSSO.EmailLoginDetails emailDetails = new ViewModelsSSO.EmailLoginDetails();
        String expectedErrorMessage = String.format(Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Contact Id' });

        // Act
        try
        {
            ActionResult result = ContactService.sendSSOResetPasswordEmail(emailDetails);
        }
        catch (Exception ex)
        {
            errorMessage = ex.getMessage();
        }

        // Assert
        System.assertEquals(errorMessage, expectedErrorMessage, 'Unexpected contact id error message');
    }

    @isTest
    static void EmptyContactEmail_EmptyEmailErrorReturned()
    {
        // Arrange
        String errorMessage = '';
        String expectedErrorMessage = String.format(Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Contact email' });
        ViewModelsSSO.EmailLoginDetails emailDetails = new ViewModelsSSO.EmailLoginDetails();
        emailDetails.contactId = '0032O000004byrhQAA';
        emailDetails.singleSignOnId = 'A0001978578@study.edu.au';

        // Act
        try
        {
            ActionResult result = ContactService.sendSSOResetPasswordEmail(emailDetails);
        }
        catch (Exception ex)
        {
            errorMessage = ex.getMessage();
        }

        // Assert
        System.assertEquals(errorMessage, expectedErrorMessage, 'Unexpected email error message');
    }

    @isTest
    static void EmptyContactFirstName_EmptyFirstNameErrorReturned()
    {
        // Arrange
        String errorMessage = '';
        String expectedErrorMessage = String.format(Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Contact first name' });
        ViewModelsSSO.EmailLoginDetails emailDetails = new ViewModelsSSO.EmailLoginDetails();
        emailDetails.contactId = '0032O000004byrhQAA';
        emailDetails.singleSignOnId = 'A0001978578@study.edu.au';
        emailDetails.contactEmail = 'john.smith@email.com';

        // Act
        try
        {
            ActionResult result = ContactService.sendSSOResetPasswordEmail(emailDetails);
        }
        catch (Exception ex)
        {
            errorMessage = ex.getMessage();
        }

        // Assert
        System.assertEquals(errorMessage, expectedErrorMessage, 'Unexpected first name error message');
    }

    @isTest
    static void EmptyContactLastName_EmptyLastNameErrorReturned()
    {
        // Arrange
        String errorMessage = '';
        String expectedErrorMessage = String.format(Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Contact last name' });
        ViewModelsSSO.EmailLoginDetails emailDetails = new ViewModelsSSO.EmailLoginDetails();
        emailDetails.contactId = '0032O000004byrhQAA';
        emailDetails.singleSignOnId = 'A0001978578@study.edu.au';
        emailDetails.contactEmail = 'john.smith@email.com';
        emailDetails.contactFirstName = 'John';

        // Act
        try
        {
            ActionResult result = ContactService.sendSSOResetPasswordEmail(emailDetails);
        }
        catch (Exception ex)
        {
            errorMessage = ex.getMessage();
        }

        // Assert
        System.assertEquals(errorMessage, expectedErrorMessage, 'Unexpected last name error message');
    }

    @isTest
    static void EmptyPassword_EmptyPasswordErrorReturned()
    {
        // Arrange
        String errorMessage = '';
        String expectedErrorMessage = String.format(Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Password' });
        ViewModelsSSO.EmailLoginDetails emailDetails = new ViewModelsSSO.EmailLoginDetails();
        emailDetails.contactId = '0032O000004byrhQAA';
        emailDetails.singleSignOnId = 'A0001978578@study.edu.au';
        emailDetails.contactEmail = 'john.smith@email.com';
        emailDetails.contactFirstName = 'John';
        emailDetails.contactLastName = 'Smith';

        // Act
        try
        {
            ActionResult result = ContactService.sendSSOResetPasswordEmail(emailDetails);
        }
        catch (Exception ex)
        {
            errorMessage = ex.getMessage();
        }

        // Assert
        System.assertEquals(errorMessage, expectedErrorMessage, 'Unexpected no password error message');
    }

    @isTest
    static void ValidParametersDisableCommand_EmailCommandSuccessfullyDisabled()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createProspectCold(user, 'John', 'Test','61408815278', 'john.test@yahoo.com', 'A0019789789');

        ViewModelsSSO.EmailLoginDetails emailDetails =
            new ViewModelsSSO.EmailLoginDetails(contact.Id, contact.FirstName, contact.LastName, contact.email, 'pass@#e3', contact.EnrolmentTrackingURL__c);

        // Act
        Test.startTest();
        TestUtilities.disableComponent('ContactSendSSOResetPassEmailCommand');
        ActionResult result = ContactService.sendSSOResetPasswordEmail(emailDetails);
        Test.stopTest();

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.status, 'Unexpected status');
        System.assertEquals(0, TestHttpCalloutMock.requests.size());
    }

    @isTest
    static void ValidParameters_EmailSuccessfullySent()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createProspectCold(user, 'John', 'Test','61408815278', 'john.test@yahoo.com', 'A0019789789');

        ViewModelsSSO.EmailLoginDetails emailDetails =
            new ViewModelsSSO.EmailLoginDetails(contact.Id, contact.FirstName, contact.LastName, contact.email, 'pass@#e3', contact.EnrolmentTrackingURL__c);

        // Act
        Test.startTest();
        ActionResult result = ContactService.sendSSOResetPasswordEmail(emailDetails);
        Test.stopTest();

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.status, 'Unexpected status');
        System.assertEquals(1, TestHttpCalloutMock.requests.size());
    }
}