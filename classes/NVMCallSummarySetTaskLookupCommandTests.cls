@IsTest
public class NVMCallSummarySetTaskLookupCommandTests
{
    @testSetup
    static void setup()
    {
        // Arrange
        TestUtilities.setupHEDA();
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createProspectHot(user);
    }

    @IsTest
    static void NVMCallSummaryUpdatingTaskIdFieldValue_TaskNVMCallSummaryFieldSuccessfullyUpdated()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = [SELECT Id FROM Contact LIMIT 1];
        Task task = TestDataTaskMock.createNVMCallTask(user, contact.Id, 'Left Voicemail Message', 'Call to Prospect', true);
        NVMStatsSF__NVM_Call_Summary__c callSummary = TestDataNVMCallSummaryMock.create(user);
        Map<Id, SObject> oldMap = new Map<Id, SObject>();
        oldMap.put(callSummary.Id, callSummary);

        NVMStatsSF__NVM_Call_Summary__c newCallSummary = callSummary.clone(true, true);
        newCallSummary.NVMStatsSF__TaskID__c = task.Id;

        // Act
        ActionResult result = NVMCallSummaryService.setTaskLookup(new List<SObject>{ newCallSummary }, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        Task updatedTask = [SELECT Id, NVMCallSummary__c FROM Task WHERE Id = :task.Id];

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.status, 'Unexpected result status');
        System.assertEquals(callSummary.Id, updatedTask.NVMCallSummary__c, 'Unexpected NVM Call Summary look up field value');
    }

    @IsTest
    static void TaskNVMCallSummaryFieldHasValueUpdateCallSummaryTaskId_TaskNVMCallSummaryFieldNotUpdated()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = [SELECT Id FROM Contact LIMIT 1];
        NVMStatsSF__NVM_Call_Summary__c callSummary = TestDataNVMCallSummaryMock.create(user);
        NVMStatsSF__NVM_Call_Summary__c newTaskCallSummary = TestDataNVMCallSummaryMock.create(user);
        Task task = TestDataTaskMock.createNVMCallTask(user, contact.Id, 'Left Voicemail Message', 'Call to Prospect', true, callSummary.Id);

        Map<Id, SObject> oldMap = new Map<Id, SObject>();
        oldMap.put(callSummary.Id, newTaskCallSummary);

        NVMStatsSF__NVM_Call_Summary__c newCallSummary = newTaskCallSummary.clone(true, true);
        newCallSummary.NVMStatsSF__TaskID__c = task.Id;

        // Act
        ActionResult result = NVMCallSummaryService.setTaskLookup(new List<SObject>{ newCallSummary }, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        Task updatedTask = [SELECT Id, NVMCallSummary__c FROM Task WHERE Id = :task.Id];

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.status, 'Unexpected result status');
        System.assertNotEquals(newTaskCallSummary.Id, updatedTask.NVMCallSummary__c, 'Unexpected NVM Call Summary look up field value');
    }
}