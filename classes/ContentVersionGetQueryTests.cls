@isTest
public class ContentVersionGetQueryTests
{
    @testSetup
    public static void setup()
    {
        TestUtilities.setupHEDA();
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact contact = TestDataContactMock.createProspectCold(user, '04045566889');
        Opportunity opp = TestDataOpportunityMock.createApplyingAcquisition(user, contact);
    }

    @isTest
    static void GetQuery_ReturnsFileDetails()
    {
        // Arrange
        String[] fileNames = new String[] {'Test File.txt', 'Test PDF.pdf'};
        String[] types = new String[] {'Proof of ID', 'Letter of Offer'};
        String[] fileContents = new String[] {'Test Test and Test\nTest', '%PDF-1.5\n%亏'};
        List<Opportunity> opps = [SELECT Id, OwnerId FROM Opportunity LIMIT 1];
        Id parentId = opps[0].Id;
        Id ownerId = opps[0].OwnerId;
        ActionResult result1 = ContentDocumentService.createFile(fileNames[0], types[0], Blob.valueOf(fileContents[0]), parentId, ownerId, null);
        ActionResult result2 = ContentDocumentService.createFile(fileNames[1], types[1], Blob.valueOf(fileContents[1]), parentId, ownerId, null);

        // Act
        List<ViewModelsFile.Details> details = ContentDocumentService.getFiles(parentId);
        Integer firstIndex = (result1.resultId == details[0].VersionId) ? 0 : 1;
        Integer secondIndex = (result2.resultId == details[1].VersionId) ? 1 : 0;

        // Assert
        System.assertEquals(true, details.size() > 0);
        System.assertEquals(fileNames[0], details[firstIndex].Name);
        System.assertEquals(types[0], details[firstIndex].Type);
        System.assertEquals(Blob.valueOf(fileContents[0]).size(), details[firstIndex].BodyLength);
        System.assertEquals('TEXT', details[firstIndex].ContentType);
        System.assertEquals(fileNames[1], details[secondIndex].Name);
        System.assertEquals(types[1], details[secondIndex].Type);
        System.assertEquals(Blob.valueOf(fileContents[1]).size(), details[secondIndex].BodyLength);
        System.assertEquals('PDF', details[secondIndex].ContentType);
    }
}