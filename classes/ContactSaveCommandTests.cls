@isTest
public class ContactSaveCommandTests
{
    @isTest
    static void NullParameter_NullParameterError()
    {
        // Arrange
        String errorMessage = '';
        ViewModels.Param param;

        // Act
        try
        {
           ActionResult result = new ContactSaveCommand().command(param);
        }
        catch (Exception ex)
        {
           errorMessage = ex.getMessage();
        }

        // Assert
        System.assertEquals(String.format(Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Param object'}), errorMessage, 'Unexpected error message');
    }

    @isTest
    static void BlankIdentifier_NoContactFoundError()
    {
        // Arrange
        String errorMessage = '';
        ViewModels.Param param = new ViewModels.Param();
        param.Identifier = '';
        param.Payload = null;

        // Act
        try
        {
           ActionResult result = new ContactSaveCommand().command(param);
        }
        catch (Exception ex)
        {
           errorMessage = ex.getMessage();
        }

        // Assert
        System.assertEquals(String.format(ContactSaveCommand.errorNoContactFound, new List<String>{ param.Identifier }), errorMessage, 'Unexpected error message');
    }

    @isTest
    static void NullPayload_NullPayloadError()
    {
        // Arrange
        String errorMessage = '';
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        TestDataContactMock.createStudent(user);
        Contact createdContact = [SELECT Id, EncryptedId__c FROM Contact LIMIT 1];
        ViewModels.Param param = new ViewModels.Param();
        param.Identifier = createdContact.EncryptedId__c;
        param.Payload = null;

        // Act
        try
        {
           ActionResult result = new ContactSaveCommand().command(param);
        }
        catch (Exception ex)
        {
           errorMessage = ex.getMessage();
        }

        // Assert
        System.assertEquals(String.format(Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Payload' }), errorMessage, 'Unexpected error message');
    }

    @isTest
    static void InvalidPayload_InvalidPayloadError()
    {
        // Arrange
        String errorMessage = '';
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        TestDataContactMock.createStudent(user);
        Contact createdContact = [SELECT Id, EncryptedId__c FROM Contact LIMIT 1];
        ViewModels.Param param = new ViewModels.Param();
        param.Identifier = createdContact.EncryptedId__c;
        param.Payload = '{some: "value}';

        // Act
        try
        {
           ActionResult result = ContactService.unsubscribe(param);
        }
        catch (Exception ex)
        {
           errorMessage = ex.getMessage();
        }

        // Assert
        System.assertEquals(true, errorMessage.contains('Unexpected character'), 'Unexpected error message');
    }

    @isTest
    static void FourFieldsInPayload_UnexpectedNumberOfFieldsError()
    {
        // Arrange
        String errorMessage = '';
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        TestDataContactMock.createStudent(user);
        Contact createdContact = [SELECT Id, EncryptedId__c FROM Contact LIMIT 1];
        Map<String, Object> prospectMap = new Map<String, Object>
        {
            'Cnt_Salutation' => 'Mr',
            'Cnt_FirstName' => 'Prospect',
            'Cnt_hed_Gender_c' => 'Male',
            'Cnt_DoNotCall' => true
        };

        ViewModels.Param param = new ViewModels.Param();
        param.Identifier = createdContact.EncryptedId__c;
        param.Payload = JSON.serialize(prospectMap);

        // Act
        try
        {
           ActionResult result = ContactService.unsubscribe(param);
        }
        catch (Exception ex)
        {
           errorMessage = ex.getMessage();
        }

        // Assert
        System.assertEquals(ContactSaveCommand.errorUnexpectedNumberOfFields, errorMessage, 'Unexpected error message');
    }

    @isTest
    static void UnsupportedFieldinPayload_UnexpectedFieldError()
    {
        // Arrange
        String errorMessage = '';
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        TestDataContactMock.createStudent(user);
        Contact createdContact = [SELECT Id, EncryptedId__c FROM Contact LIMIT 1];
        Map<String, Object> prospectMap = new Map<String, Object>
        {
            'Cnt_FirstName' => 'Prospect',
            'Cnt_DoNotCall' => true
        };

        ViewModels.Param param = new ViewModels.Param();
        param.Identifier = createdContact.EncryptedId__c;
        param.Payload = JSON.serialize(prospectMap);

        // Act
        try
        {
           ActionResult result = ContactService.unsubscribe(param);
        }
        catch (Exception ex)
        {
           errorMessage = ex.getMessage();
        }

        // Assert
        System.assertEquals(String.format(ContactSaveCommand.errorUnexpectedField,
            new List<String>{ ContactSaveCommand.unsubscribeEmailField, ContactSaveCommand.unsubscribePhoneField }), errorMessage, 'Unexpected error message');
    }

    @isTest
    static void UnsubscribeFieldsInPayload_UnsubscribeFieldsUpdatedSuccessfully()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        TestDataContactMock.createStudent(user);
        Contact createdContact = [SELECT Id, EncryptedId__c FROM Contact LIMIT 1];
        Map<String, Object> prospectMap = new Map<String, Object>
        {
            'Cnt_HasOptedOutOfEmail' => true,
            'Cnt_DoNotCall' => true
        };

        ViewModels.Param param = new ViewModels.Param();
        param.Identifier = createdContact.EncryptedId__c;
        param.Payload = JSON.serialize(prospectMap);

        // Act
        Test.startTest();
        ActionResult result = ContactService.unsubscribe(param);
        Test.stopTest();
        Contact updatedContact = [SELECT Id, HasOptedOutOfEmail, DoNotCall FROM Contact WHERE Id =: createdContact.Id];

        // Assert
        System.assertEquals(true, updatedContact.HasOptedOutOfEmail, 'Unexpected Has Opted Out Of Email value');
        System.assertEquals(true, updatedContact.DoNotCall, 'Unexpected Do Not Call value');
    }

    @isTest
    static void HasOptedOutOfEmailFieldInPayload_HasOptedOutOfEmailUpdatedSuccessfully()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        TestDataContactMock.createStudent(user);
        Contact createdContact = [SELECT Id, EncryptedId__c FROM Contact LIMIT 1];
        Map<String, Object> prospectMap = new Map<String, Object>
        {
            'Cnt_HasOptedOutOfEmail' => true
        };

        ViewModels.Param param = new ViewModels.Param();
        param.Identifier = createdContact.EncryptedId__c;
        param.Payload = JSON.serialize(prospectMap);

        // Act
        Test.startTest();
        ActionResult result = ContactService.unsubscribe(param);
        Test.stopTest();
        Contact updatedContact = [SELECT Id, HasOptedOutOfEmail FROM Contact WHERE Id =: createdContact.Id];

        // Assert
        System.assertEquals(true, updatedContact.HasOptedOutOfEmail, 'Unexpected Has Opted Out Of Email value');
    }

    @isTest
    static void DoNotCallFieldInPayload_DoNotCallFieldUpdatedSuccessfully()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        TestDataContactMock.createStudent(user);
        Contact createdContact = [SELECT Id, EncryptedId__c FROM Contact LIMIT 1];
        Map<String, Object> prospectMap = new Map<String, Object>
        {
            'Cnt_DoNotCall' => true
        };

        ViewModels.Param param = new ViewModels.Param();
        param.Identifier = createdContact.EncryptedId__c;
        param.Payload = JSON.serialize(prospectMap);

        // Act
        Test.startTest();
        ActionResult result = ContactService.unsubscribe(param);
        Test.stopTest();
        Contact updatedContact = [SELECT Id, DoNotCall FROM Contact WHERE Id =: createdContact.Id];

        // Assert
        System.assertEquals(true, updatedContact.DoNotCall, 'Unexpected Do Not Call value');
    }
}