public class ContactUpdateRolloverDetailsCommand extends BaseCommand
{
    public override List<SObject> validate(List<SObject> items)
    {
        if (items instanceof List<hed__Program_Enrollment__c>)
        {
            return items;
        }
        else if (items instanceof List<Contact>)
        {
            List<hed__Program_Enrollment__c>  courseEnrolments =
                [
                    SELECT Id,hed__Contact__c,hed__Start_Date__c,Status__c,FeeHelpEligibilityType__c,NextFollowUpDate__c,CurrentStageStatus__c
                    FROM hed__Program_Enrollment__c
                    WHERE Status__c = 'Ongoing' AND hed__Account__r.Program__c IN ('MBA', 'GCM', 'GDM')
                    AND hed__Contact__c IN : items
                    ORDER BY CerebroID__c DESC
                ];

            if (courseEnrolments.size() == 0 && items.size() == 1)
                throw new Exceptions.ApplicationException('There is no valid Course Enrolments for this contact.');

            return courseEnrolments;
        }

        return new List<SObject>();
    }

    public override ActionResult command(List<SObject> items)
    {
        Map<Id, hed__Program_Enrollment__c> courseEnrolmentMap = new Map<Id, hed__Program_Enrollment__c>();

        for (hed__Program_Enrollment__c courseEnrolment : (List<hed__Program_Enrollment__c>)items)
        {
            if (!courseEnrolmentMap.containsKey(courseEnrolment.hed__Contact__c))
                courseEnrolmentMap.put(courseEnrolment.hed__Contact__c, courseEnrolment);
        }

        List<Contact> contacts =
            [
                SELECT Id,Current_Course_Status__c,Rollover_Status__c,Next_Followup_Date__c,FeeHelp_Eligibility__c
                FROM Contact
                WHERE Id IN : courseEnrolmentMap.keySet()
            ];
        List<Contact> updatedContacts = new List<Contact>();

        if (contacts.size() > 0)
        {
            for (Contact contact : contacts)
            {
                hed__Program_Enrollment__c courseEnrolment = courseEnrolmentMap.get(contact.Id);

                if (courseEnrolment.CurrentStageStatus__c == RolloverConstantsDefinition.STAGE_ATRISK || courseEnrolment.CurrentStageStatus__c == RolloverConstantsDefinition.STAGE_ENROLLED)
                {
                    contact.Current_Course_Status__c = courseEnrolment.Status__c;
                    contact.FeeHelp_Eligibility__c = courseEnrolment.FeeHelpEligibilityType__c;
                    contact.Course_Start_Date__c = courseEnrolment.hed__Start_Date__c;
                    contact.StudentExpiryDate__c = contact.Course_Start_Date__c != null ? contact.Course_Start_Date__c.addYears(3) : null;
                    contact.Next_Followup_Date__c = courseEnrolment.NextFollowUpDate__c;

                    if (contact.Next_Followup_Date__c == null)
                    {
                        if (contact.Rollover_Status__c != RolloverConstantsDefinition.RO_NOENROLMENTS)
                            contact.Rollover_Status__c = RolloverConstantsDefinition.RO_ALLSTAGE;
                    }

                    updatedContacts.add(contact);
                }
            }

            if (updatedContacts.size() > 0)
                uow.registerDirty(updatedContacts);
        }

        return new ActionResult(ResultStatus.SUCCESS);
    }
}