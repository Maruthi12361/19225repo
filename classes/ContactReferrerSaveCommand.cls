public class ContactReferrerSaveCommand extends BaseViewModelCommand
{
    private List<ContactReferrer__c> existingContactReferrers;

    public override List<Interfaces.IViewModel> validate(List<Interfaces.IViewModel> items)
    {
        List<ViewModelsAcquisition.ContactReferrer> referrers = typeCast(items);

        for (ViewModelsAcquisition.ContactReferrer referrer : referrers)
        {
            if (referrer.ContactID == null)
            {
                Log.error(this.className, 'validate', 'Referrer contact ID value cannot be null');
                throw new Exceptions.ApplicationException('Referrer contact ID value cannot be null');
            }
        }

        return items;
    }

    public override ActionResult command(List<Interfaces.IViewModel> items)
    {
        List<ContactReferrer__c> newReferrers = new List<ContactReferrer__c>();
        List<ViewModelsAcquisition.ContactReferrer> referrers = typeCast(items);
        existingContactReferrers = getContactReferrers(getContactIds(referrers));
        
        for (ViewModelsAcquisition.ContactReferrer referrer : referrers)
        {
            if (getContactReferrer(referrer) != null)
            {
                Log.info(this.className, 'command', 'Same contact referrer already created today');
                continue;
            }
                
            ContactReferrer__c newReferrer = new ContactReferrer__c();
            newReferrer.ContactID__c = referrer.ContactID;
            
            if (!String.isEmpty(referrer.LeadSource))
            {    
                newReferrer.LeadSource__c = referrer.LeadSource;   
            }
            else 
            {
                newReferrer.utm_campaign__c = referrer.utm_campaign;
                newReferrer.utm_content__c = referrer.utm_content;
                newReferrer.utm_gclid__c = referrer.utm_gclid;
                newReferrer.utm_keyword__c = referrer.utm_keyword;
                newReferrer.utm_medium__c = referrer.utm_medium;
                newReferrer.utm_source__c = referrer.utm_source;
                newReferrer.utm_term__c = referrer.utm_term;

                if (!String.isEmpty(referrer.utm_referralSource))
                {
                    Integer length = SObjectUtils.getMaxStringLength(newReferrer, 'utm_referralSource__c');
                    newReferrer.utm_referralSource__c = StringUtils.copyLeft(referrer.utm_referralSource, length);
                }
            }

            newReferrers.add(newReferrer);
            Log.info(this.className, 'command', 'Creating contact referrer record for ' + newReferrer.ContactID__c);
        }

        if (newReferrers.size() > 0)
            Database.insert(newReferrers);

        return new ActionResult(ResultStatus.Success);
    }

    private List<ViewModelsAcquisition.ContactReferrer> typeCast(List<Interfaces.IViewModel> items)
    {
        List<ViewModelsAcquisition.ContactReferrer> referrers = new List<ViewModelsAcquisition.ContactReferrer>();
        
        for (Interfaces.IViewModel item : items)
            referrers.add((ViewModelsAcquisition.ContactReferrer) item);

        return referrers;
    }

    private Set<Id> getContactIds(List<ViewModelsAcquisition.ContactReferrer> referrers)
    {
        Set<ID> contactIds = new Set<ID>();

        for (ViewModelsAcquisition.ContactReferrer referrer : referrers)
            contactIds.add(referrer.ContactID);

        return contactIds;
    }

    private List<ContactReferrer__c> getContactReferrers(Set<ID> ids)
    {
        String query = 
            'SELECT ' + 
                'Id, IsDeleted, ContactID__c, CreatedDate, utm_campaign__c, utm_keyword__c, utm_source__c ' + 
            'FROM ContactReferrer__c ' + 
            'WHERE IsDeleted = false AND ContactID__c IN :ids AND CreatedDate = TODAY';

        return Database.query(query);
    }

    private ContactReferrer__c getContactReferrer(ViewModelsAcquisition.ContactReferrer referrer)
    {
        for (ContactReferrer__c existingReferrer : existingContactReferrers)
        {
            if (existingReferrer.ContactID__c == referrer.ContactID && existingReferrer.utm_campaign__c == referrer.utm_campaign &&
                existingReferrer.utm_keyword__c == referrer.utm_keyword && existingReferrer.utm_source__c == referrer.utm_source)
            {
                return existingReferrer;
            }
        }

        return null;
    }
}