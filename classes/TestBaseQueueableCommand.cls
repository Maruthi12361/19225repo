public class TestBaseQueueableCommand extends BaseQueueableCommand
{
    public TestBaseQueueableCommand()
    {
        super(25);
    }
    
    public override List<SObject> validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        return items;
    }
    
    public override ActionResult command(List<SObject> items, Id jobId)
    {
        for (Case item : (List<Case>)items)
        {
            Log.info('TestBaseQueueableCommand', 'command', 'Updating: ' + JSON.serialize(item));
            
            item.Subject = 'Updated ' + String.valueOf(system.now());
            uow.registerDirty(item);
        }

        return new ActionResult();
    }
}