public class CourseEnrolmentTriggerHandler extends BaseTriggerHandler
{
    public override void afterInsert()
    {
        ActionResult result = CourseEnrolmentService.mergeContact(Trigger.new, Trigger.oldMap, Enums.TriggerContext.AFTER_INSERT);
        
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);
    }
    
    public override void afterUpdate()
    {
        ActionResult result = CourseEnrolmentService.mergeContact(Trigger.new, Trigger.oldMap, Enums.TriggerContext.AFTER_UPDATE);
        
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);
    }
    
    public override void afterDelete()
    {
        ActionResult result = CourseEnrolmentService.mergeContact(Trigger.old, Trigger.oldMap, Enums.TriggerContext.AFTER_DELETE);
        
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);
    }
}