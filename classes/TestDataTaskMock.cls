@isTest
public class TestDataTaskMock
{
    public static Task createRolloverCallTask(User user, Id whoId, Id whatId, String callOutcome, String description, Boolean isPersistent)
    {
        Task task = new Task(WhoId = whoId, WhatId = whatId, Status = 'Completed');
        task.Type = 'Call';
        task.TaskSubtype = 'Task';
        task.Call_Outcome__c = callOutcome;
        task.Description = description;

        if (isPersistent)
        {
            System.runAs(user)
            {
                insert task;
            }
            task =
                [
                    SELECT Id, WhoId, WhatId, Type, TaskSubtype, Call_Outcome__c, Subject, LastModifiedDate
                    FROM Task
                    WHERE Id = : task.Id
                ];
        }
        else
        {
            task.LastModifiedDate = Datetime.now();
        }

        return task;
    }

    public static Task createNVMCallTask(User user, Id whoId, String callOutcome, String description, Boolean isPersistent)
    {
        return createNVMCallTask(user, whoId, callOutcome, description, isPersistent, null);
    }

    public static Task createNVMCallTask(User user, Id whoId, String callOutcome, String description, Boolean isPersistent, Id nvmCallSummaryId)
    {
        Task task = new Task(WhoId = whoId, Status = 'Completed', Type = 'Call', TaskSubtype = 'Call', CallType = 'Outbound', Subject = 'Outbound call to +61400123456');
        task.Call_Outcome__c = callOutcome;
        task.Description = description;
        task.CreatedById = Constants.USERID_NVM_ADMINISTRATOR;
        task.RecordTypeId = RecordTypeUtils.getRecordTypeId(Constants.RECORDTYPE_TASK_CLIENTCONTACT, SObjectType.Task);
        task.NVMCallSummary__c = nvmCallSummaryId;

        if (isPersistent)
        {
            System.runAs(user)
            {
                insert task;
            }
            task =
                [
                    SELECT Id, Call_Outcome__c, NVMCallSummary__c, TaskSubtype, Type, WhoId
                    FROM Task
                    WHERE Id = : task.Id
                ];
        }
        else
        {
            task.LastModifiedDate = Datetime.now();
        }

        return task;
    }

    public static Task createRolloverEmailTask(User user, Id whoId, Id whatId, String subject, Boolean isPersistent)
    {
        Task task = new Task(WhoId = whoId, WhatId = whatId, Status = 'Completed');
        task.TaskSubtype = 'Email';
        task.Subject = subject;
        task.RecordTypeId = RecordTypeUtils.getRecordTypeId(Constants.RECORDTYPE_TASK_FOLLOWUP, SObjectType.Task);

        if (isPersistent)
        {
            System.runAs(user)
            {
                insert task;
            }
            task =
                [
                    SELECT Id, WhoId, WhatId, Type, TaskSubtype, Call_Outcome__c, Subject, LastModifiedDate
                    FROM Task
                    WHERE Id = : task.Id
                ];
        }
        else
        {
            task.LastModifiedDate = Datetime.now();
        }

        return task;
    }

    public static Task createMIRTask(User user, Id whoId, Id whatId, String subject, String reason)
    {
        Task task = new Task
        (
            WhoId = whoId, 
            WhatId = whatId, 
            Status = 'Open',
            Subject = subject,
            MIR_Reason__c = reason,
            RecordTypeId = RecordTypeUtils.getRecordTypeId(Constants.RECORDTYPE_TASK_MIR, SObjectType.Task)
        );

        if (user != null)
        {
            System.RunAs(user)
            { 
                insert task;
            }
        }
        else
            insert task;

        return task;
    }
    
    public static Task createFollowUpTask(User user, Id whoId, Id whatId, String subject, Boolean notification)
    {
        Id followUpTaskRecordTypeId = RecordTypeUtils.getRecordTypeId(Constants.RECORDTYPE_TASK_FOLLOWUP, SObjectType.Task);
        
        Task task = new Task
        (
            OwnerId = user.Id,
            WhatId = whatId,
            WhoId = whoId,
            Type = 'Call',
            RecordTypeId = followUpTaskRecordTypeId,
            Priority = 'High',
            Subject = subject,
            Status = 'Open',
            ActivityDate = System.today().addDays(1),
            Description = 'How can we help?',
            Notification__c = notification
        );
        
        if (user != null)
        {
            System.runAs(user)
            {
                insert task;
            }
        }
        else
            insert task;
       
        return task;
    }
}