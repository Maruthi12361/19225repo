public class TopicPortalController 
{
    @AuraEnabled
    public static List<Object> getTopics()
    {
        try
        {
            return [SELECT Topic.Id, Topic.Name name, Count(TopicId) cnt
                    FROM TopicAssignment 
                    WHERE EntityType='Knowledge' AND EntityId In (SELECT Id FROM Knowledge__kav where PublishStatus = 'Online')
                    GROUP BY Topic.Id, Topic.Name];
        }
        catch (Exception ex)
        {
            Log.error('TopicController', 'getTopics', 'Failed to retrive Topics', ex);
            throw ex;
        }
    }
}