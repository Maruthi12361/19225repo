public class SObjectSendConfirmationEmailCommand extends BaseTriggerCommand
{
    private Set<String> internalMIRReasons = new Set<String> { 'M-5 Identity Error (Proof of Name/Citizenship/VEVO)',
        'M-7 NOOSR/NARIC/RTO Report Error (Not provided/Incorrect/Incomplete)' , 'M-10 Other (please provide in Comments field)'};
    public static Map<String, ViewModelsMail.Confirmation> confirmationEmailsMap = new Map<String, ViewModelsMail.Confirmation>();

    public override List<SObject> validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        List<SObject> result = new List<SObject>();

        if (items == null || items.size() == 0)
            return result;

        if (items[0] instanceOf Opportunity)
        {
            List<Opportunity> acquisitionOpportunities = new List<Opportunity>();
            Id acquisitionRecordTypeId = RecordTypeUtils.getRecordTypeId(Constants.RECORDTYPE_OPPORTUNITY_ACQUISITION, SObjectType.Opportunity);

            for (Opportunity opp : (List<Opportunity>) items)
            {
                Opportunity oldValue = (Opportunity)oldMap.get(opp.Id);

                if (opp.RecordTypeId == acquisitionRecordTypeId &&
                   ((opp.OAF_Submitted__c && SObjectUtils.hasPropertyChanged(opp, oldValue, 'OAF_Submitted__c')) ||
                   (opp.OfferGeneratedDate__c != null && opp.OfferAcceptedDate__c != null && SObjectUtils.hasPropertyChanged(opp, oldValue, 'OfferAcceptedDate__c')) ||
                   (opp.TimetableSent__c == true && SObjectUtils.hasPropertyChanged(opp, oldValue, 'TimetableSent__c')) ||
                   (opp.TimetableAccepted__c && SObjectUtils.hasPropertyChanged(opp, oldValue, 'TimetableAccepted__c')) ||
                   (opp.Remittance_Received__c && SObjectUtils.hasPropertyChanged(opp, oldValue, 'Remittance_Received__c')) ||
                   (opp.FEE_HELP_Forms_Submitted__c && SObjectUtils.hasPropertyChanged(opp, oldValue, 'FEE_HELP_Forms_Submitted__c'))))
                {
                    acquisitionOpportunities.add(opp);
                }
            }

            if (acquisitionOpportunities.size() > 0)
            {
                Map<Id, Case> oppCasesMap = getOpportunityCases(SObjectUtils.getIDs(acquisitionOpportunities, 'Application_Case__c'));

                for (Opportunity opp : acquisitionOpportunities)
                {
                    String confirmationEmailName;
                    ViewModelsMail.Confirmation confirmationEmail;
                    Case oppCase = oppCasesMap.get(opp.Application_Case__c);
                    Opportunity oldValue = (Opportunity)oldMap.get(opp.Id);

                    if (opp.OAF_Submitted__c && SObjectUtils.hasPropertyChanged(opp, oldValue, 'OAF_Submitted__c'))
                    {
                        confirmationEmailName = Constants.CONFIRMATIONEMAIL_APPLICATIONSUBMISSION;
                        confirmationEmail = new ViewModelsMail.Confirmation(opp.Id, 'Application Submission Confirmation', false);
                    }
                    else if (opp.OfferGeneratedDate__c != null && opp.OfferAcceptedDate__c != null && SObjectUtils.hasPropertyChanged(opp, oldValue, 'OfferAcceptedDate__c'))
                    {
                        confirmationEmailName = Constants.CONFIRMATIONEMAIL_LOOACCEPTANCECONFIRMATION;
                        confirmationEmail = new ViewModelsMail.Confirmation(opp.Id, 'Confirmation Of Offer Next Steps', false);
                    }
                    else if (opp.TimetableSent__c == true && SObjectUtils.hasPropertyChanged(opp, oldValue, 'TimetableSent__c'))
                    {
                        confirmationEmailName = Constants.CONFIRMATIONEMAIL_TIMETABLEACCEPTANCEREQUEST;
                        confirmationEmail = new ViewModelsMail.Confirmation(opp.Id, 'Timetable Acceptance Request', false);
                    }
                    else if (opp.TimetableAccepted__c && SObjectUtils.hasPropertyChanged(opp, oldValue, 'TimetableAccepted__c'))
                    {
                        confirmationEmailName = Constants.CONFIRMATIONEMAIL_TIMETABLEACCEPTANCECONFIRMATION;
                        confirmationEmail = new ViewModelsMail.Confirmation(opp.Id, 'Timetable Acceptance Confirmation', true);
                    }
                    else if (opp.Remittance_Received__c && SObjectUtils.hasPropertyChanged(opp, oldValue, 'Remittance_Received__c') &&
                        oppCase != null && oppCase.Invoice_Required__c && oppCase.Invoice_Generated__c)
                    {
                        confirmationEmailName = Constants.CONFIRMATIONEMAIL_INVOICEPAYMENTCONFIRMATION;
                        confirmationEmail = new ViewModelsMail.Confirmation(opp.Id, 'Invoice Payment Confirmation', true);
                    }
                    else if (oppCase != null && oppCase.FeeHelpDetailsRequired__c && opp.FEE_HELP_Forms_Submitted__c &&
                        SObjectUtils.hasPropertyChanged(opp, oldValue, 'FEE_HELP_Forms_Submitted__c'))
                    {
                        confirmationEmailName = Constants.CONFIRMATIONEMAIL_FEEHELPDETAILSCONFIRMATION;
                        confirmationEmail = new ViewModelsMail.Confirmation(opp.Id, 'FEE HELP Details Confirmation', true);
                    }

                    if (confirmationEmail != null && !confirmationEmailsMap.containsKey(confirmationEmailName))
                    {
                        result.add(opp);
                        confirmationEmailsMap.put(confirmationEmailName, confirmationEmail);
                    }
                }
            }
        }
        else if (items[0] instanceOf Case)
        {
            Id applicationRecordTypeId = RecordTypeUtils.getRecordTypeId(Constants.RECORDTYPE_CASE_APPLICATION, SObjectType.Case);

            for (Case currentCase : (List<Case>) items)
            {
                if (currentCase.RecordTypeId != applicationRecordTypeId)
                    continue;

                String confirmationEmailName;
                ViewModelsMail.Confirmation confirmationEmail;
                Case oldValue = (Case)oldMap.get(currentCase.Id);

                if (currentCase.Vetting_Complete__c &&
                    SObjectUtils.hasPropertyChanged(currentCase, oldValue, 'Vetting_Complete__c'))
                {
                    confirmationEmailName = Constants.CONFIRMATIONEMAIL_LOOACCEPTANCEREQUEST;
                    confirmationEmail = new ViewModelsMail.Confirmation(currentCase.Id, 'Letter of Offer Acceptance Request', false);
                }
                else if (currentCase.Invoice_Required__c && currentCase.Invoice_Generated__c &&
                         SObjectUtils.hasPropertyChanged(currentCase, oldValue, 'Invoice_Generated__c'))
                {
                    confirmationEmailName = Constants.CONFIRMATIONEMAIL_INVOICEPAYMENTREQUEST;
                    confirmationEmail = new ViewModelsMail.Confirmation(currentCase.Id, 'Invoice Payment Request', false);
                }
                else if (currentCase.Welcome_Letter_Sent__c && SObjectUtils.hasPropertyChanged(currentCase, oldValue, 'Welcome_Letter_Sent__c'))
                {
                    confirmationEmailName = Constants.CONFIRMATIONEMAIL_WELCOMETOAIB;
                    confirmationEmail = new ViewModelsMail.Confirmation(currentCase.Id, 'Welcome to AIB', false);
                }

                if (confirmationEmail != null && !confirmationEmailsMap.containsKey(confirmationEmailName))
                {
                    result.add(currentCase);
                    confirmationEmailsMap.put(confirmationEmailName, confirmationEmail);
                }
            }
        }
        else if (items[0] instanceOf Task)
        {
            Id mirRecordTypeId = RecordTypeUtils.getRecordTypeId(Constants.RECORDTYPE_TASK_MIR, SObjectType.Task);

            if (triggerContext == Enums.TriggerContext.AFTER_INSERT)
            {
                for (Task task : (List<Task>) items)
                {
                    String confirmationEmailName;
                    ViewModelsMail.Confirmation confirmationEmail;

                    if (task.RecordTypeId == mirRecordTypeId && !internalMIRReasons.contains(task.MIR_Reason__c))
                    {
                        confirmationEmailName = Constants.CONFIRMATIONEMAIL_APPLICATIONMIRREQUEST;
                        confirmationEmail = new ViewModelsMail.Confirmation(task.Id, 'Application MIR Request', false);
                    }

                    if (confirmationEmail != null && !confirmationEmailsMap.containsKey(confirmationEmailName))
                    {
                        result.add(task);
                        confirmationEmailsMap.put(confirmationEmailName, confirmationEmail);
                    }
                }
            }
        }
        else if (items[0] instanceOf Contact)
        {
            List<Contact> prospects = new List<Contact>();
            Id prospectRecordTypeId = RecordTypeUtils.getRecordTypeId(Constants.RECORDTYPE_CONTACT_PROSPECT, SObjectType.Contact);

            for (Contact contact : (List<Contact>) items)
            {
                if (contact.RecordTypeId == prospectRecordTypeId &&
                    contact.SingleSignOnStatusViewed__c &&
                    SObjectUtils.hasPropertyChanged(contact, oldMap.get(contact.Id), 'SingleSignOnStatusViewed__c'))
                {
                    prospects.add(contact);
                }
            }

            if (prospects.size() > 0)
            {
                Map<Id, Opportunity> contactOppMap = getContactOpportunities(SObjectUtils.getIDs(prospects, 'Id'));

                for (Contact contact : (List<Contact>) prospects)
                {
                    String confirmationEmailName;
                    ViewModelsMail.Confirmation confirmationEmail;

                    Opportunity applicationOpp = contactOppMap.get(contact.Id);

                    if (applicationOpp == null)
                    {
                        Log.info(this.className, 'validate', 'No application opportunity found {0} contact. Skipping send confirmation email', new List<String> { contact.Id });
                        continue;
                    }

                    if (applicationOpp.Payment_Method__c == 'Mixed' || applicationOpp.Payment_Method__c == 'FEE-HELP')
                    {
                        confirmationEmailName = Constants.CONFIRMATIONEMAIL_FEEHELPDETALSREQUEST;
                        confirmationEmail = new ViewModelsMail.Confirmation(contact.Id, 'FEE HELP Details Request', false);
                    }

                    if (confirmationEmail != null && !confirmationEmailsMap.containsKey(confirmationEmailName))
                    {
                        result.add(contact);
                        confirmationEmailsMap.put(confirmationEmailName, confirmationEmail);
                    }
                }
            }
        }

        return result;
    }

    public override ActionResult command(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        for (String confirmationEmailName : confirmationEmailsMap.keySet())
        {
            Log.info('SObjectSendConfirmationEmailCommand', 'command', 'Processing {0} confirmation email', new List<String> { confirmationEmailName });
            ViewModelsMail.Confirmation mailConfirmation = confirmationEmailsMap.get(confirmationEmailName);

            if (!mailConfirmation.isEmailSent)
            {
                sendEmail(mailConfirmation.objectId, confirmationEmailName, mailConfirmation.emailTemplate, mailConfirmation.sendToOwnerOnly);

                mailConfirmation.isEmailSent = true;
                confirmationEmailsMap.put(confirmationEmailName, mailConfirmation);
            }
        }

        return new ActionResult(ResultStatus.SUCCESS);
    }

    @future(callout=true)
    private static void sendEmail(Id objectId, String confirmationEmail, String emailTemplateName, Boolean sendToOwnerOnly)
    {
        Log.info('SObjectSendConfirmationEmailCommand', 'sendEmail', 'Sending {0} email', new List<String> { confirmationEmail });
        Contact contact;

        String stringObjectId = String.valueOf(objectId);
        EmailTemplate emailTemplate = EmailService.getEmailTemplate(emailTemplateName);

        if (emailTemplate == null)
        {
            String message = String.format('No email template found for {0}.', new List<String> { emailTemplateName });
            Log.error('SObjectSendConfirmationEmailCommand', 'sendEmail', message);
            throw new Exceptions.ApplicationException(message);
        }

        if (stringObjectId.startsWith(Constants.ENTITYID_OPPORTUNITY))
        {
            Opportunity opp = getOpportunity(objectId);

            if (opp != null)
                contact = opp.Contact__r;
        }
        else if (stringObjectId.startsWith(Constants.ENTITYID_CASE))
        {
            Case currentCase = getCase(objectId);

            if (currentCase != null)
                contact = currentCase.Contact;
        }
        else if (stringObjectId.startsWith(Constants.ENTITYID_TASK))
        {
            contact = getTaskContact(objectId);
        }
        else if (stringObjectId.startsWith(Constants.ENTITYID_CONTACT))
        {
            contact = getContact(objectId);
        }

        if (contact != null)
        {
            String toName;
            String toEmail;

            List<ViewModelsMail.Recipient> ccEmails;
            String htmlBody =  renderBody(emailTemplate.HtmlValue, contact);
            String textBody =  renderBody(emailTemplate.Body, contact);
            emailTemplate.Subject = emailTemplate.Subject.replace('{Contact.FirstName}', contact.FirstName);

            if (confirmationEmail == Constants.CONFIRMATIONEMAIL_INVOICEPAYMENTREQUEST ||
                confirmationEmail == Constants.CONFIRMATIONEMAIL_TIMETABLEACCEPTANCEREQUEST)
            {
                ViewModelsFile.Details fileDetails = getFile(ContentDocumentService.getFiles(objectId), confirmationEmail);

                if (fileDetails != null)
                {
                    htmlBody = htmlBody.replace('{Contact.FileDownloadUrl}', String.format('{0}files/get?id={1}&reference={2}',
                        new List<String>{ Label.OAFBaseURL, fileDetails.VersionId, contact.EncryptedId__c}));
                    textBody = textBody.replace('{Contact.FileDownloadUrl}', String.format('{0}files/get?id={1}&reference={2}',
                        new List<String>{ Label.OAFBaseURL, fileDetails.VersionId, contact.EncryptedId__c}));
                }
            }

            if (!sendToOwnerOnly)
            {
                toName = contact.Name;
                toEmail = contact.Email;
                ccEmails = new List<ViewModelsMail.Recipient>{ new ViewModelsMail.Recipient(contact.Owner.Name, contact.Owner.Email) };
            }
            else
            {
                toName = contact.Owner.Name;
                toEmail = contact.Owner.Email;
            }

            ActionResult result = EmailService.sendToRecipient(Constants.APIPROVIDER_DEFAULT, toEmail, toName, emailTemplate.Subject,
                contact.Owner.Email, contact.Owner.Name, textBody, htmlBody, ccEmails);
        }
        else
        {
            Log.info('SObjectSendConfirmationEmailCommand', 'sendEmail', 'No contact found based on {0} object id', new List<String> { stringObjectId });
        }
    }

    private static Opportunity getOpportunity(Id oppId)
    {
        List<Opportunity> opps =
        [
            SELECT Id, Contact__c, Contact__r.EnrolmentTrackingURL__c, Contact__r.Email, Contact__r.EncryptedId__c, Contact__r.FirstName, Contact__r.Name,
                Contact__r.Owner.Email, Contact__r.Owner.FirstName, Contact__r.Owner.LastName, Contact__r.Owner.LinkedInURL__c, Contact__r.Owner.Name,
                Contact__r.Owner.Phone, Contact__r.Owner.Title, Contact__r.Program__c
            FROM Opportunity
            WHERE Id = :oppId
        ];

        if (opps.size() != 0)
            return opps[0];

        return null;
    }

    private static Case getCase(Id caseId)
    {
        List<Case> cases =
        [
            SELECT Id, Contact.EnrolmentTrackingURL__c, Contact.Email, Contact.EncryptedId__c, Contact.FirstName, Contact.Name, Contact.Owner.Email,
                Contact.Owner.FirstName, Contact.Owner.LastName, Contact.Owner.LinkedInURL__c, Contact.Owner.Name, Contact.Owner.Phone, Contact.Owner.Title,
                Contact.Program__c
            FROM Case
            WHERE Id = :caseId
        ];

        if (cases.size() != 0)
            return cases[0];

        return null;
    }

    private static Contact getTaskContact(Id taskId)
    {
        List<Task> tasks = [SELECT Id, WhoId FROM Task WHERE Id = :taskId];

        if (tasks.size() == 0)
            return null;

        if (tasks[0].WhoId != null && String.valueOf(tasks[0].WhoId).startsWith(Constants.ENTITYID_CONTACT))
        {
            List<Contact> contacts =
            [
                SELECT Id, EnrolmentTrackingURL__c, Email, EncryptedId__c, FirstName, Name, Owner.Email, Owner.FirstName, Owner.LastName, Owner.LinkedInURL__c,
                    Owner.Name,Owner.Phone,Owner.Title,Program__c
                FROM Contact
                WHERE Id = :tasks[0].WhoId
            ];

            if (contacts.size() != 0)
                return contacts[0];
        }

        return null;
    }

    private static Contact getContact(Id contactId)
    {
        return
        [
            SELECT Id, EnrolmentTrackingURL__c, Email, EncryptedId__c, FirstName, Name, Owner.Email, Owner.FirstName, Owner.LastName, Owner.LinkedInURL__c,
                Owner.Name,Owner.Phone,Owner.Title,Program__c
            FROM Contact
            WHERE Id = :contactId
        ];
    }

    private Map<Id, Case> getOpportunityCases(Set<Id> applicationCaseIds)
    {
        return new Map<Id, Case>
        ([
            SELECT Id, FEE_HELP_Forms_Submitted_Opportunity__c, FeeHelpDetailsRequired__c, Invoice_Generated__c, Invoice_Required__c, Opportunity__c, Vetting_Complete__c, Timetable_Processed__c, Welcome_Letter_Sent__c
            FROM Case
            WHERE Id IN :applicationCaseIds
        ]);
    }

    private Map<Id, Opportunity> getCaseOpportunities(Set<Id> acquisitionOppIds)
    {
        return new Map<Id, Opportunity>
        ([
            SELECT Id, Payment_Method__c, TimetableAccepted__c
            FROM Opportunity
            WHERE Id IN :acquisitionOppIds
        ]);
    }

    private Map<Id, Opportunity> getContactOpportunities(Set<Id> contactIds)
    {
        Map<Id, Opportunity> oppMap = new Map<Id, Opportunity>();
        List<Opportunity> opps =
        [
            SELECT Id, Contact__c, Payment_Method__c
            FROM Opportunity
            WHERE Contact__c IN :contactIds AND RecordType.DeveloperName = :Constants.RECORDTYPE_OPPORTUNITY_ACQUISITION
        ];

        for (Opportunity opp : opps)
            oppMap.put(opp.Contact__c, opp);

        return oppMap;
    }

    private static ViewModelsFile.Details getFile(List<ViewModelsFile.Details> files, String confirmationEmail)
    {
        String type = '';

        if (confirmationEmail == Constants.CONFIRMATIONEMAIL_INVOICEPAYMENTREQUEST)
            type = 'Invoice';
        else if (confirmationEmail == Constants.CONFIRMATIONEMAIL_TIMETABLEACCEPTANCEREQUEST)
            type = 'Timetable';

        if (String.isEmpty(type))
            throw new Exceptions.ApplicationException('Unsupported confirmation email in getFile');

        if (files == null || files.size() == 0)
            return null;

        for (ViewModelsFile.Details file : files)
        {
            if (file.Type == type)
                return file;
        }

        return null;
    }

    private static String renderBody(String body, Contact contact)
    {
        String instanceUrl = System.URL.getSalesforceBaseURL().toExternalForm();

        body = body.replace('{Owner.FirstName}', contact.Owner.FirstName);
        body = body.replace('{Owner.LastName}', contact.Owner.LastName);
        body = body.replace('{Owner.Phone}', contact.Owner.Phone != null ? contact.Owner.Phone : '');
        body = body.replace('{Owner.Email}', contact.Owner.Email);
        body = body.replace('{Owner.Title}', contact.Owner.Title != null ? contact.Owner.Title : '');
        body = body.replace('{Owner.LinkedInURL__c}', contact.Owner.LinkedInURL__c != null ? contact.Owner.LinkedInURL__c : '');
        body = body.replace('{Owner.UrlName}',  EncodingUtil.urlEncode(contact.Owner.FirstName + '' + contact.Owner.LastName, 'UTF-8').replace('+', '%20'));

        body = body.replace('{Contact.Name}', contact.Name);
        body = body.replace('{Contact.FirstName}', contact.FirstName);
        body = body.replace('{Contact.Program__c}', contact.Program__c != null ? contact.Program__c : '');
        body = body.replace('{Contact.EnrolmentTrackingURL__c}', contact.EnrolmentTrackingURL__c);

        body = body.replace('{Contact.DetailsURL}', String.format('{0}/lightning/r/Contact/{1}/view',
            new List<String>{ instanceUrl, contact.Id }));

        return body;
    }
}