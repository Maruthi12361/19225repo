public class ContactController
{
    @AuraEnabled
    public static String resetPassword(Id contactId)
    {
        
        Contact contact = ContactService.get(contactId, 'Id, FirstName, LastName, Email, Cerebro_Student_ID__c, SingleSignOnStatus__c', false);
        ActionResult result = ContactService.resetSSOPassword(contact);
        
        if (result.isError)
            return result.message;

        ContactService.sendPasswordResetQueuedEmail(contact);

        return 'Password reset successfully queued';
    }
}