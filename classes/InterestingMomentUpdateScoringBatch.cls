public class InterestingMomentUpdateScoringBatch extends BaseBatchCommand
{
    List<SalesScoreConfig__c> salesScoreConfigs;

    public override Database.QueryLocator query(Database.BatchableContext BC) 
    {
        Datetime lastModifiedDate = getLastestSalesScoreConfigLastModifedDate();

        return Database.getQueryLocator(getInterestingMomentQuery(lastModifiedDate));
    }

    public override void command(Database.BatchableContext BC, List<sObject> scope) 
    {
        List<InterestingMoment__c> toUpdateList = new List<InterestingMoment__c>();
        salesScoreConfigs = getSalesScoreConfigs();

        for (InterestingMoment__c moment : (List<InterestingMoment__c>) scope)
        {
            Double scoreValue = getBehaviourScoreValue(moment.Type__c, moment.Source__c);
            if (moment.BehaviourScore__c != scoreValue)
            {
                moment.BehaviourScore__c = scoreValue;
                toUpdateList.add(moment);
            }
        }

        if (toUpdateList.size() > 0)
            update toUpdateList;
    }

    private Datetime getLastestSalesScoreConfigLastModifedDate()
    {
        List<SalesScoreConfig__c> configs = 
        [
            SELECT Id, LastModifiedDate, Active__c 
            FROM SalesScoreConfig__c 
            WHERE Active__c = True 
            ORDER BY LastModifiedDate 
            LIMIT 2
        ];

        if (configs.size() == 0)
            return null;

        return configs[0].LastModifiedDate;
    }

    private String getInterestingMomentQuery(Datetime lastModifiedDate)
    {
        String whereStatement = '';

        if (lastModifiedDate == null)
            whereStatement = 'WHERE LastModifiedDate = NEXT_WEEK';
        else 
            whereStatement = 'WHERE LastModifiedDate <= :lastModifiedDate';

        return 
            'SELECT ID, LastModifiedDate, Type__c, Source__c, BehaviourScore__c ' + 
            'FROM InterestingMoment__c ' + whereStatement;
    }

    private List<SalesScoreConfig__c> getSalesScoreConfigs()
    {
        return 
            [
                SELECT Id, Active__c, Operator__c, ScoreType__c, ScoreValue__c, Source__c, Value__c
                FROM SalesScoreConfig__c
                WHERE Active__c = true
            ];
    }

    private List<SalesScoreConfig__c> getInterestingMomentSalesScoreConfigs(String scoreType)
    {
        List<SalesScoreConfig__c> result = new List<SalesScoreConfig__c>();

        for (SalesScoreConfig__c config : salesScoreConfigs)
        {
            if (config.ScoreType__c == scoreType)
                result.add(config);
        }

        return result;
    }

    private Double getBehaviourScoreValue(String scoreType, String source)
    {
        List<SalesScoreConfig__c> interestingMomentConfigs = getInterestingMomentSalesScoreConfigs(scoreType);

        for (SalesScoreConfig__c config : interestingMomentConfigs)
        {
            if (StringUtils.isLogicalStatementTrue(config.Operator__c, source, config.Source__c))
                return config.ScoreValue__c;
        }

        return 0;
    }
}