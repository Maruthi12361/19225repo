public class TaskNVMSubTypeUpdateCommand extends BaseTriggerCommand
{
    public override List<SObject> validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        List<Task> taskList = new List<Task>();

        for (Task nvmTask: (List<Task>)items)
        {
            if (triggerContext == Enums.TriggerContext.BEFORE_INSERT)
            {
                if (nvmTask.Type == 'Call' && (nvmTask.TaskSubtype == 'Task' || nvmTask.TaskSubtype == null) && nvmTask.RecordTypeId == System.Label.ID_RecordType_Task_ClientContact &&
                   (nvmTask.CallType == 'Outbound' || nvmTask.CallType == 'Inbound') && nvmTask.Status == 'Completed')
                {
                    taskList.add(nvmTask);
                }
            }
        }

        return taskList;
    }

    public override ActionResult command(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        Log.info(this.className, 'command', 'Updating TaskSubtype on {0} task(s) to Call for NVM.', new List<String>{ String.valueOf(items.size()) });

        for (Task nvmTask: (List<Task>)items)
            nvmTask.TaskSubtype = 'Call';

        return new ActionResult(ResultStatus.SUCCESS);
    }
}