public class ContactGetOfferingFacultiesQuery 
{
    public List<ViewModelsWorkforce.FacultyStaffPlanOffering> query(Object param)
    {
        return getFaculties((ViewModels.Param) param);
    }

    private List<ViewModelsWorkforce.FacultyStaffPlanOffering> getFaculties(ViewModels.Param param)
    {
        Log.info('ContactGetOfferingFacultiesQuery', 'getFaculties', 'Getting contact faculties for {0} subject offering', new List<String> { param.Identifier });

        List<ViewModelsWorkforce.FacultyStaffPlanOffering> result = new List<ViewModelsWorkforce.FacultyStaffPlanOffering>();
        hed__Course_Offering__c subjectOffering = getSubjectOffering(param.Identifier);
        List<SkillSet__c> skillSets = getOfferingSkillSets(subjectOffering.hed__Course__r.SubjectDefinition__c);
        List<hed__Affiliation__c> affiliations = getAppointmentAffiliations(SObjectUtils.getIds(skillSets, 'Contact__c'));

        for (SkillSet__c skillSet : skillSets)
        {
            List<hed__Affiliation__c> appointments = getFacultyAppointmentAffiliations(skillSet.Contact__c, affiliations);

            for (hed__Affiliation__c appointment : appointments)
            {
                ViewModelsWorkforce.FacultyStaffPlanOffering faculty = new ViewModelsWorkforce.FacultyStaffPlanOffering();
                faculty.Id = skillSet.Contact__r.Id;
                faculty.Name = skillSet.Contact__r.Name;
                faculty.Role = appointment.hed__Role__c;
                faculty.PrerequisitesCompleted = appointment.PrerequisitesCompleted__c;

                result.add(faculty);
            }
        }

        return result;
    }

    private List<SkillSet__c> getOfferingSkillSets(Id subjectDefinition)
    {
        return 
        [
            SELECT Id, Name, SubjectDefinition__c, Type__c, Contact__c, Contact__r.Id, Contact__r.FirstName, Contact__r.LastName, Contact__r.Name
            FROM SkillSet__c
            WHERE SubjectDefinition__c = :subjectDefinition AND RecordType.DeveloperName = 'Faculty'
        ];
    }

    private hed__Course_Offering__c getSubjectOffering(Id id)
    {
        return
        [
            SELECT Id, hed__Course__c, hed__Course__r.SubjectDefinition__c
            FROM hed__Course_Offering__c
            WHERE Id = :id
        ];
    }

    private List<hed__Affiliation__c> getAppointmentAffiliations(Set<Id> contactIds)
    {
        return
        [
            SELECT Id, hed__Contact__c, hed__Account__c, hed__Account__r.Name, hed__Role__c, PrerequisitesCompleted__c, hed__Status__c
            FROM hed__Affiliation__c
            WHERE 
                RecordType.DeveloperName = 'Appointment' AND hed__Contact__c IN :contactIds AND 
                hed__Account__r.Name = :Constants.ACCOUNT_INSTITUTION_AIB AND hed__Status__c IN :Constants.AFFILIATION_APPOINTMENTSTATUSES
        ];
    }

    private List<hed__Affiliation__c> getFacultyAppointmentAffiliations(Id facultyId, List<hed__Affiliation__c> affiliations)
    {
        List<hed__Affiliation__c> facultyAppointments = new List<hed__Affiliation__c>();

        for (hed__Affiliation__c affiliation : affiliations)
            if (affiliation.hed__Contact__c == facultyId)
                facultyAppointments.add(affiliation);

        return facultyAppointments;
    }
}