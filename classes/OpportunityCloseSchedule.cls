global class OpportunityCloseSchedule implements Schedulable 
{
    global void execute(SchedulableContext sc) 
    {
        OpportunityCloseBatch batch = new OpportunityCloseBatch();
        database.executeBatch(batch, 50);
    }
}