@isTest
public class ContactReferrerSetLeadSourceBatchTest 
{
    @testSetup
    static void setup()
    {
        TestUtilities.setupHEDA();
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        TestDataLeadSourceConfigMock.create(user);
    }

    @isTest 
    static void ContactRefererWithNoLeadSource_LeadSourceSetToFacebookSuccessfully() 
    {
        // Arrange 
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact contact = TestDataContactMock.createProspect(user, 'alex.test@email.com', 'Unknown');
        ContactReferrer__c referrer = TestDataContactReferrerMock.create(user, contact.id);
        referrer.LeadSource__c = null;
        update referrer;

        // Act
        test.startTest();
        Database.executeBatch(new ContactReferrerSetLeadSourceBatch());
        test.stopTest();

        ContactReferrer__c updatedReferrer = [SELECT Id, LeadSource__c FROM ContactReferrer__c WHERE Id = :referrer.Id];

        // Assert
        System.assertEquals('Facebook (Paid)', updatedReferrer.LeadSource__c, 'Unexpected Lead source value');
    }

    @isTest 
    static void ContactRefererWithLeadSource_LeadSourceNotOverridden() 
    {
        // Arrange 
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact contact = TestDataContactMock.createProspect(user, 'alex.test@email.com', 'Unknown');
        ContactReferrer__c referrer = TestDataContactReferrerMock.create(user, contact.id);
        referrer.LeadSource__c = 'Test';
        update referrer;

        // Act
        test.startTest();
        Database.executeBatch(new ContactReferrerSetLeadSourceBatch());
        test.stopTest();

        ContactReferrer__c updatedReferrer = [SELECT Id, LeadSource__c FROM ContactReferrer__c WHERE Id = :referrer.Id];

        // Assert
        System.assertEquals('Test', updatedReferrer.LeadSource__c, 'Unexpected Lead source value');
    }
}