public class UserUpdateLightningPreferenceSchedule implements Schedulable
{
    public void execute(SchedulableContext sc)
    {
        Database.executebatch(new UserUpdateLightningPreferenceBatch());
    }
}