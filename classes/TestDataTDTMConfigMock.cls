@isTest
public class TestDataTDTMConfigMock
{
    static List<String> activeClassNames = new List<String>{'ACCT_IndividualAccounts_TDTM', 'CON_Preferred_TDTM'};
    static List<String> disabledClassNames = new List<String>{'CENR_AcademicProgram_TDTM', 'PREN_Affiliation_TDTM'};

    public static void create(User user)
    {
        List<hed__Trigger_Handler__c> handlers = getDefault();

        for (hed__Trigger_Handler__c handler : handlers)
        {
            if (activeClassNames.contains(handler.hed__Class__c))
                handler.hed__Active__c = true;
        }

        System.RunAs(user)
        {
            if (handlers.size() > 0)
                insert handlers;
        }
    }

    public static List<hed__Trigger_Handler__c> getDefault()
    {
        List<hed.TDTM_Global_API.TdtmToken> tokens = hed.TDTM_Global_API.getDefaultTdtmConfig();
        List<hed__Trigger_Handler__c> handlers = new List<hed__Trigger_Handler__c>();

        for (hed.TDTM_Global_API.TdtmToken token : tokens)
        {
            hed__Trigger_Handler__c handler = new hed__Trigger_Handler__c();

            handler.hed__Class__c = token.className;
            handler.hed__Object__c = token.targetObject;
            handler.hed__Trigger_Action__c = token.actions;
            handler.hed__Load_Order__c = token.loadOrderDec;
            handler.hed__Active__c = token.active;
            handler.hed__Asynchronous__c = token.async;
            handler.hed__Filter_Field__c = token.filterField;
            handler.hed__Filter_Value__c = token.filterValue;
            handler.hed__User_Managed__c = token.userManaged;
            handler.hed__Owned_by_Namespace__c = token.ownedByNamespace;
            handler.hed__Usernames_to_Exclude__c = token.usernamesToExclude;

            if (token.targetObject == 'Contact' || token.targetObject == 'Account' || disabledClassNames.contains(token.className))
            {
                handler.hed__Active__c = false;
            }

            handlers.add(handler);
        }

        return handlers;
    }
}