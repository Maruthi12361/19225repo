@isTest
public class ProspectSaveMilestoneCommandTests
{
    @testSetup
    static void setup()
    {
        TestUtilities.setupHEDA();
    }

    @isTest
    static void LeadScoreOver300Single_CreateMilestone()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createProspectCold(user, '0455566787');
        TestDataInterestingMomentMock.create(user, contact, 'Email Link Clicked', 'Test URL', 275);
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        contact.recalculateFormulas();
        Contact newContact = contact.clone(true, false, true, true);
        TestDataInterestingMomentMock.create(user, contact, 'Form Completed', 'Test Form', 25);
        newContact.BehaviourDecaySum__c = 0;
        newContact.recalculateFormulas();
        Map<Id, Contact> oldMap = new Map<Id, Contact>();
        oldMap.put(contact.Id, contact);
        
        // Act
        Test.startTest();
        ActionResult result = ContactService.saveMilestone(new List<Contact>{ newContact }, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        Test.stopTest();
        InterestingMoment__c moment =
            [
                SELECT Id, ContactID__c
                FROM InterestingMoment__c
                WHERE Type__c = 'Milestone' AND Source__c = 'Lead Score Over 300'
            ];
        
        // Assert
        System.assertEquals(true, result.isSuccess);
        System.assertEquals(contact.Id, moment.ContactID__c);
    }
    
    @isTest
    static void LeadScoreOver300Multiple_CreateMilestoneOnlyOnce()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createProspectCold(user, '0455566787');
        TestDataInterestingMomentMock.create(user, contact, 'Email Link Clicked', 'Test URL1', 275);
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        contact.recalculateFormulas();
        Contact newContact = contact.clone(true, false, true, true);
        TestDataInterestingMomentMock.create(user, contact, 'Form Completed', 'Test Form', 30);
        newContact.BehaviourDecaySum__c = 0;
        newContact.recalculateFormulas();
        Map<Id, Contact> oldMap = new Map<Id, Contact>();
        oldMap.put(contact.Id, contact);
        
        // Act
        Test.startTest();
        ActionResult result = ContactService.saveMilestone(new List<Contact>{ newContact }, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        result = ContactService.saveMilestone(new List<Contact>{ newContact }, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        Test.stopTest();
        List<InterestingMoment__c> moments =
            [
                SELECT Id, ContactID__c
                FROM InterestingMoment__c
                WHERE Type__c = 'Milestone' AND Source__c = 'Lead Score Over 300'
            ];
        
        // Assert
        System.assertEquals(true, result.isSuccess);
        System.assertEquals(1, moments.size());
        System.assertEquals(contact.Id, moments[0].ContactID__c);
    }
    
    @isTest
    static void HasOptedOutOfEmail_CreateMileStone()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createProspectCold(user, '0455566787');        
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());        
        Contact newContact = contact.clone(true, false, true, true);        
        newContact.HasOptedOutOfEmail = true;
        Map<Id, Contact> oldMap = new Map<Id, Contact>();
        oldMap.put(contact.Id, contact);
        
        // Act
        Test.startTest();
        ActionResult result = ContactService.saveMilestone(new List<Contact>{ newContact }, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        Test.stopTest();
        InterestingMoment__c moment =
            [
                SELECT Id, ContactID__c
                FROM InterestingMoment__c
                WHERE Type__c = 'Milestone' AND Source__c = 'Unsubscribed'
            ];
        
        // Assert
        System.assertEquals(true, result.isSuccess);
        System.assertEquals(contact.Id, moment.ContactID__c);
    }
    
    @isTest
    static void DoNotCall_CreateMileStone()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createProspectCold(user, '0455566787');        
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());        
        Contact newContact = contact.clone(true, false, true, true);        
        newContact.DoNotCall = true;
        Map<Id, Contact> oldMap = new Map<Id, Contact>();
        oldMap.put(contact.Id, contact);
        
        // Act
        Test.startTest();
        ActionResult result = ContactService.saveMilestone(new List<Contact>{ newContact }, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        Test.stopTest();
        InterestingMoment__c moment =
            [
                SELECT Id, ContactID__c
                FROM InterestingMoment__c
                WHERE Type__c = 'Milestone' AND Source__c = 'Do Not Call'
            ];
        
        // Assert
        System.assertEquals(true, result.isSuccess);
        System.assertEquals(contact.Id, moment.ContactID__c);
    }
    
    @isTest
    static void BothDNCANDEmailOptOut_CreateTwoMileStones()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createProspectCold(user, '0455566787');        
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());        
        Contact newContact = contact.clone(true, false, true, true);        
        newContact.DoNotCall = true;
        newContact.HasOptedOutOfEmail = true;
        Map<Id, Contact> oldMap = new Map<Id, Contact>();
        oldMap.put(contact.Id, contact);
        
        // Act
        Test.startTest();
        ActionResult result = ContactService.saveMilestone(new List<Contact>{ newContact }, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        Test.stopTest();
        List<InterestingMoment__c> moments =
            [
                SELECT Id, ContactID__c, Source__c
                FROM InterestingMoment__c
                WHERE Type__c = 'Milestone'
            ];
        
        // Assert
        System.assertEquals(true, result.isSuccess);
        System.assertEquals(2, moments.size());
        System.assertEquals(contact.Id, moments[0].ContactID__c);
        System.assertEquals('Unsubscribed', moments[0].Source__c);
        System.assertEquals(contact.Id, moments[1].ContactID__c);
        System.assertEquals('Do Not Call', moments[1].Source__c);        
    }
}