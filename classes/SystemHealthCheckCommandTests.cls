@isTest
public class SystemHealthCheckCommandTests
{
    @testSetup
    static void setup()
    {
        TestUtilities.setupHEDA();
    }

    @isTest
    static void SingleSuccesfullCheck_DoesntSendEmails()
    {
        // Arrange
        QueryResult qr = new QueryResult
        (
           'Title of Query',
           'Description of Query',
            new List<String>{ 'Total', 'RecordType', 'Status__c' },
            new List<AggregateResult>()
        );

        // Act
        test.startTest();
        ActionResult result = SystemService.runSystemHealthCheck(new List<QueryResult>{ qr });
        test.stopTest();

        // Assert
        ViewModelsSystem.SystemHealthCheck check = (ViewModelsSystem.SystemHealthCheck)result.resultInfo;
        System.assertEquals(0, check.FailedChecks);
        System.assertEquals(1, check.TotalChecks);
    }

    @isTest
    static void SingleFailedCheck_SendsEmailsToEachUserInGroup()
    {
        // Arrange
        QueryResult qr = new QueryResult
        (
           'Title of Query',
           'Description of Query',
            new List<String>{ 'Total', 'RecordType', 'Status__c' },
            new List<AggregateResult>()
        );
        qr.results = new List<List<String>>
        {
            new List<String>{ '487', 'Student', 'Cold' },
            new List<String>{ '167', 'Prospect', 'Hot' },
            new List<String>{ '14', 'Prospect', 'Warm' }
        };
        qr.totalResults = (487 + 167 + 14);

        // Act
        test.startTest();
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        ActionResult result = SystemService.runSystemHealthCheck(new List<QueryResult>{ qr });
        test.stopTest();

        // Assert
        ViewModelsSystem.SystemHealthCheck check = (ViewModelsSystem.SystemHealthCheck)result.resultInfo;
        List<User> users = [SELECT Id, email, Name FROM User WHERE isActive = true AND Id IN (SELECT userOrGroupId FROM groupMember WHERE group.DeveloperName =: Constants.GROUP_HEALTH_CHECK_TEAM)];
        System.assertEquals(ResultStatus.SUCCESS, result.status);
        System.assertEquals(1, check.FailedChecks);
        System.assertEquals(1, check.TotalChecks);
        System.assertEquals(users.size(), check.UserEmailsToSend);
    }

    @isTest
    static void MultipleBreakOfferingsInATerm_SendEmail()
    {
        // Arrange - 2 Break Offerings in single term duplicating Invalid DB State
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createProspectHot(user);
        Account educationalAccount = TestDataAccountMock.create(user, Constants.ACCOUNT_INSTITUTION_AIB);
        Account department = TestDataAccountMock.createDepartmentBusiness(user);
        hed__Term__c term = TestDataTermMock.create(user, department.Id, Date.today());
        hed__Course__c breakSubjectDefinition = TestDataSubjectMock.createDefinition(user, Constants.SUBJECT_COURSEID_BREAK, 'Break', department.Id);
        hed__Course__c breakSubjectVersion = TestDataSubjectMock.createVersion(user, breakSubjectDefinition.hed__Course_ID__c, breakSubjectDefinition.Name, department.Id, breakSubjectDefinition.Id, true, 1);
        ViewModelsTestData.SubjectOffering breakOffering = new ViewModelsTestData.SubjectOffering(breakSubjectVersion, term, Constants.SUBJECTOFFERING_TYPE_BREAK, Date.today());
        ViewModelsTestData.SubjectOffering breakOffering2 = new ViewModelsTestData.SubjectOffering(breakSubjectVersion, term, Constants.SUBJECTOFFERING_TYPE_BREAK, Date.today().addDays(1));
        List<hed__Course_Offering__c> breakOfferings = TestDataSubjectOfferingMock.createOfferings(user, new List<ViewModelsTestData.SubjectOffering>{ breakOffering, breakOffering2 });
        List<QueryResult> results = new List<QueryResult>
        {
            new QueryResult
            (
                'Check Multiple Break Subject Offerings Per Term',
                'Checks that for each Term there is at most 1 Break Subject Offering. More than 1 is an invalid state and will need to be merged together into a single record.',
                new List<String>{ 'Total', 'TermName' },
                [
                    SELECT count(Id) Total, hed__Term__r.Name TermName
                    FROM hed__Course_Offering__c
                    WHERE Active__c = true And Type__c = 'Break'
                    GROUP BY hed__Term__r.Name
                    HAVING count(Id) > 1
                    ORDER BY count(Id) DESC
                ]
            )
        };

        // Act
        test.startTest();
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        ActionResult result = SystemService.runSystemHealthCheck(results);
        test.stopTest();

        // Assert
        ViewModelsSystem.SystemHealthCheck check = (ViewModelsSystem.SystemHealthCheck)result.resultInfo;
        System.assertEquals(1, check.FailedChecks);
        System.assertEquals(1, check.TotalChecks);
    }
}