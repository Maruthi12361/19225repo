public class ContactSendPassResetQueuedCommand extends BaseQueueableCommand
{
    @testVisible
    private static string templateName = 'Single Sign On - Password Reset Queued';

    public ContactSendPassResetQueuedCommand()
    {
        super(1);
    }

    public override List<SObject> validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        EmailTemplate emailTemplate = EmailService.getEmailTemplate(templateName);

        if (emailTemplate == null)
        {
            String message = String.format('No email template found for {0}.', new List<String> { templateName });
            Log.error(this.className, 'validate', message);
            throw new Exceptions.ApplicationException(message);
        }

        return items;
    }

    public override ActionResult command(List<SObject> items, Id jobId)
    {
        EmailTemplate emailTemplate = EmailService.getEmailTemplate(templateName);
        User currentUser = UserService.get(UserInfo.getUserId());

        for (Contact contact : (List<Contact>) items)
        {
            String body = emailTemplate.HtmlValue;

            body = body.replace('{user.FirstName}', currentUser.FirstName);
            body = body.replace('{user.Title}', String.isNotEmpty(currentUser.Title) ? currentUser.Title : '');
            body = body.replace('{contact.FirstName}', contact.FirstName);
            body = body.replace('{contact.LastName}', contact.LastName);
            body = body.replace('{contact.Email}', contact.Email);

            EmailService.sendToRecipient(Constants.APIPROVIDER_DEFAULT, currentUser.Email, currentUser.Name, emailTemplate.Subject, Constants.STUDENTCENTRAL_EMAIL, Constants.STUDENTCENTRAL_NAME, null, body, null);
        }

        return new ActionResult(ResultStatus.SUCCESS);
    }
}