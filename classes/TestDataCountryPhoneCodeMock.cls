@isTest
public class TestDataCountryPhoneCodeMock 
{
    public static Country_Phone_Code__c create(User user, String countryName, String countryPhoneCode)
    {
        return create(user, countryName, countryPhoneCode, '');
    }

    public static Country_Phone_Code__c create(User user, String countryName, String countryPhoneCode, String countryCode)
    {
        Country_Phone_Code__c item = new Country_Phone_Code__c
        (
            Name = countryName,
            Phone_Code__c = countryPhoneCode,
            CountryCode__c = countryCode
        );
        
        System.RunAs(user)
        {
            insert item;
        }
        
        return item;
    }
}