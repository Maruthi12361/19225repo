public class ContactCalcPassRateBatch extends BaseBatchCommand
{
    String query = 'SELECT Id,hed__Contact__c ' +
                   'FROM hed__Program_Enrollment__c ' +
                   'WHERE Status__c = \'Ongoing\' AND hed__Contact__c != null ' +
                   'ORDER BY hed__Contact__r.Cerebro_Student_ID__c';

    public ContactCalcPassRateBatch()
    {
    }

    public override Database.QueryLocator query(Database.BatchableContext context)
    {
        return Database.getQueryLocator(query);
    }

    public override void command(Database.BatchableContext context, List<sObject> scope)
    {
        List<Contact> updateContacts = new List<Contact>();
        Set<Id> contactIds = SObjectUtils.getIds(scope, 'hed__Contact__c');

        List<Contact> students =
            [
                SELECT Id, Cerebro_Student_ID__c, FailedSubjects__c, PassRate__c,
                    (SELECT Id, Grade__c, hed__Course_Offering__c FROM hed__Student_Course_Enrollments__r WHERE Grade__c != null)
                FROM Contact
                WHERE Id IN : contactIds
                ORDER BY Cerebro_Student_ID__c
            ];

        for (Contact student : students)
        {
            Integer failedCount = 0;
            Integer passRate;
            Integer passedCount = 0;
            Integer participatedCount = 0;
            List<String> failedGrades = new List<String> {'Fail', 'Withdraw Fail', 'Did Not Sit or Submit', 'Competency Not Achieved'};
            List<String> passedGrades = new List<String> {'High Distinction', 'Distinction', 'Credit', 'Pass 1', 'Pass 2', 'Competency Achieved', 'Non-Graded Pass'};
            List<String> excludedGrades = new List<String> {'Medical / Compassionate', 'Withdraw Not Fail', 'Exemption'};
            Set<Id> keySet = new Set<Id>();

            for (hed__Course_Enrollment__c enrolment : student.hed__Student_Course_Enrollments__r)
            {
                // We should count the SubjectEnrolments only once according to their Subject Offerings to skip the duplicated Enrolments.
                // There may be duplicated Enrolments migrated from Cerebro caused by forked transfers.
                String key = enrolment.hed__Course_Offering__c;

                if (keySet.contains(key))
                    continue;

                if (StringUtils.containsInList(enrolment.Grade__c, failedGrades, 0))
                    failedCount++;

                if (StringUtils.containsInList(enrolment.Grade__c, passedGrades, 0))
                    passedCount++;

                if (!StringUtils.containsInList(enrolment.Grade__c, excludedGrades, 0))
                    participatedCount++;

                keySet.add(key);
            }

            if (participatedCount > 0)
                passRate = passedCount * 100 / participatedCount;

            if (failedCount != student.FailedSubjects__c || passRate != student.PassRate__c)
            {
                student.FailedSubjects__c = failedCount;
                student.PassRate__c = passRate;
                updateContacts.add(student);
            }
        }

        if (updateContacts.size() > 0)
            update updateContacts;
    }
}