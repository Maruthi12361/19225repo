/**
 * Test class for DecsOnD_CustomAssignmentHelper
 *
 * Copyright 2017, Decisions on Demand, Inc.
 * All Rights Reserved.
 *
 * For support, contact support@decisionsondemand.com
*/
@isTest(SeeAllData=false)
public class DecsOnD_TestCustomAssignmentHelper
{
    @testSetup
    static void setup()
    {
        TestUtilities.setupHEDA();
    }

    @IsTest
    static void testBasics()
    {
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        Test.startTest();
        DecsOnD.PolicyActionRecord actionRec = initTestDataAndCreateActionRec(Lead.sObjectType, null);
        DecsonD.PolicyInvocationContext context = actionRec.getContext().policyContext;
        DecsOnD.AssignmentHelper helper = DecsOnD.AssignOwnerAction.getCachedHelper(context);
        Test.stopTest();
        System.assert(helper instanceof DecsOnD_CustomAssignmentHelper);
    }

    @IsTest
    static void testWeightedMode()
    {
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        Test.startTest();
        DecsOnD.PolicyActionRecord actionRec = initTestDataAndCreateActionRec(Lead.sObjectType, new String[]{ DecsOnD_AssignmentGlobals.WEIGHTED_OPTION });
        Map<Id, User> testUserMap = new Map<Id, User>(testUsers);
        DecsonD.PolicyInvocationContext context = actionRec.getContext().policyContext;
        DecsOnD_CustomAssignmentHelper helper = (DecsOnD_CustomAssignmentHelper)DecsOnD.AssignOwnerAction.getCachedHelper(context);

        User owner = (User)helper.getAssignedOwner(actionRec);
        Test.stopTest();
        System.assertNotEquals(null, owner);
        System.assert(testUserMap.containsKey(owner.Id));
        // Verify there are no issues when actually applying the action
        actionRec.action.prepare(actionRec);
        DecsOnD.PolicyActionHandler.applyAction(actionRec);
        if (actionRec.batch != null)
            actionRec.batch.apply();

        Lead l = (Lead)actionRec.getContext().checkMainRecord();
        l = [SELECT Id, Name, OwnerId FROM Lead WHERE Id = :l.Id];
        System.assert(testUserMap.containsKey(l.OwnerId));
    }

    @IsTest
    static void testSkillsBasedMode()
    {
        // Test with combination of weighted and skills-based mode
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        Test.startTest();
        DecsOnD.PolicyActionRecord actionRec = initTestDataAndCreateActionRec(Contact.sObjectType, new String[]{ DecsOnD_AssignmentGlobals.SKILLS_BASED_OPTION, DecsOnD_AssignmentGlobals.WEIGHTED_OPTION });
        Map<Id, User> testUserMap = new Map<Id, User>(testUsers);

        DecsOnD.PolicyInvocationContext context = actionRec.getContext().policyContext;
        DecsOnD_CustomAssignmentHelper helper = (DecsOnD_CustomAssignmentHelper)DecsOnD.AssignOwnerAction.getCachedHelper(context);
        User owner = (User)helper.getAssignedOwner(actionRec);
        System.assertNotEquals(null, owner);
        System.assertEquals(testUsers[1].Id, owner.Id);
        // Verify there are no issues when actually applying the action
        actionRec.action.prepare(actionRec);
        DecsOnD.PolicyActionHandler.applyAction(actionRec);
        if (actionRec.batch!=null)
            actionRec.batch.apply();

        Contact c = (Contact)actionRec.getContext().checkMainRecord();
        c = [SELECT Id, Name, OwnerId FROM Contact WHERE Id = :c.Id];
        System.assertEquals(owner.Id, c.OwnerId);

        // Check skills-based mode applied to object that doesn't support it. Should work without issues
        Lead l = DecsOnD.TestUtils.createTestLead(owner, true);
        Test.stopTest();
        
        // Create invocation context
        DecsOnD__Policy__c policy = DecsOnD.TestUtils.createTestPolicy('Lead', 'Assignment', true);
        context = new DecsOnD.PolicyInvocationContext(policy);
        actionRec = DecsOnD.TestUtils.createTestActionRecord(context, l, DecsOnD.AssignOwnerAction.NAME, actionRec.actionParameters);
        actionRec.action.prepare(actionRec);
        DecsOnD.PolicyActionHandler.applyAction(actionRec);
        if (actionRec.batch != null)
            actionRec.batch.apply();
    }

    @isTest
    private static void testCustomizationManager()
    {
        DecsOnD_CustomAssignmentHelper helper = (DecsOnD_CustomAssignmentHelper)DecsOnD.Config.getCustomApexInstance(DecsOnD.AssignmentHelper.class, new Map<String, Object>());
        System.assertNotEquals(null, helper);
    }

    private static Map<String, String[]> usr1Skills = new Map<String, String[]>
    {
        'Entry Pathway' => new String[]{ 'Masters' },
        'Gender' => new String[]{ 'Male', 'Indeterminate' },
        'Portfolio' => new String[]{ 'International (Canada)' }
    };
    private static Map<String, String[]> usr2Skills = new Map<String, String[]>
    {
        'Entry Pathway' => new String[]{ 'Masters', 'Work Experience' },
        'Gender' => new String[]{ 'Female' },
        'Portfolio' => new String[]{ 'Domestic', 'International (Other)' }
    };
    private static Map<String, String> testContactInferredSkills = new Map<String, String>
    {
        'Entry Pathway' => 'Masters', 
        'Gender' => 'Female',
        'Portfolio' => 'Domestic'
    };
    // Values that make up the inferred skills
    private static Map<sObjectField, String> testContactUnderlyingFieldValues = new Map<sObjectField, String>
    {
        Contact.Salutation => 'Ms',
        Contact.HighestTertiaryLevelDegree__c => 'Master\'s degree, post graduate degree or higher',
        Contact.Portfolio__c => 'Domestic'
    };

    private static DecsOnD.PolicyActionRecord initTestDataAndCreateActionRec(sObjectType objType, String[] options)
    {
        DecsOnD.TestUtils.initializeTestSetup();
        // Init data
        initTestUsersAndGroups();
        User usr = testUsers[0];
        Group grp = testGroups[0];
        Map<Id, User> testUserMap = new Map<Id, User>(testUsers);

        sObject mainObj = null;
        if (objType == Lead.sObjectType)
        {
            mainObj = DecsOnD.TestUtils.createTestLead(usr, true);
        }
        else if (objType == Contact.sObjectType)
        {
            Account a = DecsOnD.TestUtils.createTestAccount(usr, true);
            mainObj = DecsOnD.TestUtils.createTestContact(a, false);
            for (sObjectField field: testContactUnderlyingFieldValues.keySet())
                mainObj.put(field, testContactUnderlyingFieldValues.get(field));

            insert mainObj;

            mainObj = [SELECT Id, Name, OwnerId, InferredEntryPathway__c, InferredGender__c, InferredPortfolio__c FROM Contact WHERE Id = :mainObj.Id];

            for (String skill: DecsOnD_AssignmentGlobals.CONTACT_INFERRED_SKILLS_FIELDS.keySet())
            {
                sObjectField field = DecsOnD_AssignmentGlobals.CONTACT_INFERRED_SKILLS_FIELDS.get(skill);                
                System.assertEquals(testContactInferredSkills.get(skill), mainObj.get(field), 'Checking inferred skill: ' + skill);
            }
        }

        // Create invocation context
        DecsOnD__Policy__c policy = DecsOnD.TestUtils.createTestPolicy(objType.getDescribe().getName(), 'Assignment', true);
        DecsOnD.PolicyInvocationContext context = new DecsOnD.PolicyInvocationContext(policy);

        Map<String, Object> params = new Map<String, Object>();
        params.put(DecsOnD.AssignmentConstants.GROUP_PARAMETER, grp.DeveloperName);
        if (!DecsOnD.Utilities.isEmptyList(options))
            params.put(DecsOnD_AssignmentGlobals.ASSIGNMENT_OPTIONS_PARAM, String.join(options, ';'));

        return DecsOnD.TestUtils.createTestActionRecord(context, mainObj, DecsOnD.AssignOwnerAction.NAME, params);
    }

    private static User[] testUsers = null;
    private static Group[] testGroups = null;

    private static void initTestUsersAndGroups()
    {
        // Init data
        User usr = DecsOnD.TestUtils.createTestUser(true, false);
        usr.put(DecsOnD_AssignmentGlobals.ASSIGNMENT_TIER_FIELD, 'Increased');
        insert usr;
        usr =
        [
            SELECT Id, Name, Email, isActive, Username, DecsOnD_Assignment_Tier__c, DecsOnD_Assignment_Weight__c
            FROM User
            WHERE Id = :usr.Id
        ];
        System.assert(DecsOnD_CustomAssignmentHelper.getAssignmentWeight(usr) > 1.0);
        Skillset__c[] skillsets = createSkillsets(usr, usr1Skills, false);

        User usr2 = DecsOnD.TestUtils.createTestUser(true, true);
        usr2 =
        [
            SELECT Id, Name, Email, isActive, Username, DecsOnD_Assignment_Tier__c, DecsOnD_Assignment_Weight__c
            FROM User
            WHERE Id = :usr2.Id
        ];
        System.assertEquals(1.0, DecsOnD_CustomAssignmentHelper.getAssignmentWeight(usr2));
        skillsets.addAll(createSkillsets(usr2, usr2Skills, false));
        insert skillsets;

        testUsers = new User[]{ usr, usr2 };
        Set<Id> userIds = new Set<Id>{ usr.id, usr2.id };

        Group grp = [SELECT Id, Name, DeveloperName FROM Group WHERE DeveloperName = 'CourseAdvisor'];
        DecsOnD.TestUtils.addGroupMembers(grp, testUsers, true);
        DecsOnD.GroupLoader.addGroupMembersToCache(grp.id, testUsers);
        testGroups = new Group[]{ grp };
    }

    private static List<Skillset__c> createSkillsets(User usr, Map<String, String[]> skills, boolean insertIntoDb)
    {
        List<Skillset__c> skillsets = new List<Skillset__c>();
        for (String skill : skills.keySet())
            for (String value : skills.get(skill))
                skillsets.add(new Skillset__c(Type__c = DecsOnD_AssignmentGlobals.COURSE_ADVISOR_SKILLSET_TYPE, SubType__c = skill + ': ' + value, User__c = usr.Id));

        if (insertIntoDb)
            insert skillsets;

        return skillsets;
    }
}