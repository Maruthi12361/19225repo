public class TaskCreateCommand extends BaseCommand
{
    public override List<SObject> validate(List<SObject> items)
    {
        return items;
    }
    
    public override ActionResult command(List<SObject> items)
    {
        try 
        {
            Log.info(this.className, 'command', 'Inserting {0} task(s)', new List<String>{ String.valueOf(items.size()) });
    
            uow.registerNew(items);

            return new ActionResult(ResultStatus.SUCCESS);
        } 
        catch (Exception ex) 
        {
            Log.error(this.className, 'command', 'Failed to insert tasks. ' + ex.getMessage());
            return new ActionResult(ResultStatus.ERROR, ex.getMessage());
        }
    }
}