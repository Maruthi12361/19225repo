public class ContactEnableSSOCommand extends BaseQueueableCommand
{
    private static String fields = 'Cerebro_Student_ID__c, Email, FirstName, Id, LastName, MailingCountry, MiddleName, Name, OAF_Application_Id__c, ' +
        'SingleSignOnStatus__c, SingleSignOnStatusLastUpdated__c, SingleSignOnSyncMessage__c, SSO_ID__c, EnrolmentTrackingURL__c';

    public ContactEnableSSOCommand()
    {
        super(1);
    }

    protected override List<SObject> validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        List<Contact> contacts = new List<Contact>();

        for (Contact contact : (List<Contact>) items)
        {
            if (String.isEmpty(contact.Cerebro_Student_ID__c))
            {
                Log.info(this.className, 'validate', 'Cerebro student Id is null for {0} contact', new List<String> { contact.Id });
                continue;
            }

            if (contact.SingleSignOnStatus__c != Constants.CONTACT_SINGLESIGNONSTATUS_NONE && 
                contact.SingleSignOnStatus__c != Constants.CONTACT_SINGLESIGNONSTATUS_PROSPECTPROVISIONING &&
                contact.SingleSignOnStatus__c != Constants.CONTACT_SINGLESIGNONSTATUS_STUDENTPROVISIONING)
            {
                Log.info(this.className, 'validate', 'Contact {0} is not in right SSO status. Current status: {1}', new List<String> { contact.Id, contact.SingleSignOnStatus__c });
                continue;
            }

            contacts.add(contact);
        }

        return contacts;
    }

    public override ActionResult command(List<SObject> items, Id jobId)
    {
        List<Contact> contacts = ContactService.getMany(SObjectUtils.getIDs(items, 'ID'), fields, false);
        Log.info(this.className, 'command', 'Enabling SSO for {0} contacts', new List<String> { String.valueOf(contacts.size()) });

        for (Contact contact : contacts)
        {
            try
            {
                enableProspectSSO(contact.clone(true, true));
            }
            catch (Exception ex)
            {
                Log.error(this.className, 'command', ex.getMessage());

                if (contact.SingleSignOnStatus__c == Constants.CONTACT_SINGLESIGNONSTATUS_PROSPECTPROVISIONING)
                    contact.SingleSignOnStatus__c = Constants.CONTACT_SINGLESIGNONSTATUS_NONE;
                else if (contact.SingleSignOnStatus__c == Constants.CONTACT_SINGLESIGNONSTATUS_STUDENTPROVISIONING)
                    contact.SingleSignOnStatus__c = Constants.CONTACT_SINGLESIGNONSTATUS_PROSPECTENABLED;

                contact.SingleSignOnStatusLastUpdated__c = Datetime.now();
                contact.SingleSignOnSyncMessage__c = ex.getMessage();

                uow.registerDirty(contact);
            }
        }

        return new ActionResult(ResultStatus.SUCCESS);
    }

    private void enableProspectSSO(Contact contact)
    {
        Log.info(this.className, 'enableProspectSSO', 'Enabling sso for {0}', new List<String> { contact.ID });
        SingleSignOnConfiguration__mdt ssoConfiguration = SingleSignOnUtilities.getConfiguration();

        Boolean toUpdateRecord = false;
        String signonId = contact.Cerebro_Student_ID__c + ssoConfiguration.StudentDomain__c;

        if (String.isNotEmpty(contact.SSO_ID__c))
            if (contact.SSO_ID__c != signonId)
                throw new Exceptions.ApplicationException(String.format('Existing SSO ID ({0}) does not match expected SSO ID ({1})', new List<String> { contact.SSO_ID__c, signonId }));

        String userObjectId = SingleSignOnUtilities.getUserObjectId(signonId);

        if (String.isNotBlank(userObjectId))
        {
            if (String.isEmpty(contact.SSO_ID__c))
            {
                contact.SSO_ID__c = signonId;
                toUpdateRecord = true;
            }
            Log.info(this.className, 'enableProspectSSO', 'Skipping creation of user {0} in Azure AD. User already exists - {1}',
                new List<String> { contact.Id, userObjectId });
        }
        else
        {
            String password =  SingleSignOnUtilities.generatePassword(6, 'aU');
            userObjectId = SingleSignOnUtilities.createUser(contact, signonId, password);
            if (String.isNotEmpty(userObjectId))
            {
                String errorMsg = '';
                Log.info(this.className, 'enableProspectSSO', 'User created successfully. Sending email');

                try
                {
                    ActionResult emailResult = ContactService.sendSSOEmail(new ViewModelsSSO.EmailLoginDetails(contact.Id, contact.FirstName, contact.LastName, contact.Email, signonId, password, contact.EnrolmentTrackingURL__c));

                    if (emailResult.isError)
                    {
                        Log.error(this.className, 'enableProspectSSO', 'Failed to send email. {0}', new List<String> { emailResult.message });
                        errorMsg = emailResult.message;
                    }
                }
                catch (Exception ex)
                {
                    Log.error(this.className, 'enableProspectSSO', 'Failed to send email. {0}', new List<String> { ex.getMessage() });
                    errorMsg = ex.getMessage();
                }

                if (String.isNotEmpty(errorMsg))
                {
                    try
                    {
                        SingleSignOnUtilities.deleteUser(signonId);
                    }
                    catch (Exception ex)
                    {
                        Log.error(this.className, 'enableProspectSSO', 'Failed to delete user. {0}', new List<String> { ex.getMessage() });
                        errorMsg += ' ' + ex.getMessage();
                    }

                    throw new Exceptions.ApplicationException(errorMsg);
                }

                contact.SSO_ID__c = signonId;
                toUpdateRecord = true;
            }
            else
            {
                Log.error(this.className, 'enableProspectSSO', 'Failed to create user - ContactID: {0} SignonID: {1}', new List<String> { contact.Id, signonId });
                throw new Exceptions.ApplicationException('Failed to create user');
            }
        }

        if (!SingleSignOnUtilities.existsInUserGroup(userObjectId, Constants.SINGLESIGNON_ACCESS_PROVISIONALGROUP))
        {
            if (!SingleSignOnUtilities.addUserToGroup(userObjectId, Constants.SINGLESIGNON_ACCESS_PROVISIONALGROUP))
            {
                Log.error(this.className, 'enableProspectSSO', 'Failed to add user to {0} user group',
                    new List<String> { Constants.SINGLESIGNON_ACCESS_PROVISIONALGROUP });
                throw new Exceptions.ApplicationException(String.format('Failed to add user to {0} group', new List<String> { Constants.SINGLESIGNON_ACCESS_PROVISIONALGROUP }));
            }
        }

        if (contact.SingleSignOnStatus__c == Constants.CONTACT_SINGLESIGNONSTATUS_NONE ||
            contact.SingleSignOnStatus__c == Constants.CONTACT_SINGLESIGNONSTATUS_PROSPECTPROVISIONING)
        {
            contact.SingleSignOnStatus__c = Constants.CONTACT_SINGLESIGNONSTATUS_PROSPECTENABLED;
            contact.SingleSignOnStatusLastUpdated__c = Datetime.now();
            toUpdateRecord = true;

            Log.info(this.className, 'enableProspectSSO', 'Successfully provisoned prospect in Azure AD');
        }
        else if (contact.SingleSignOnStatus__c == Constants.CONTACT_SINGLESIGNONSTATUS_STUDENTPROVISIONING)
        {
            if (!SingleSignOnUtilities.existsInUserGroup(userObjectId, Constants.SINGLESIGNON_ACCESS_STUDENTGROUP))
            {
                if (!SingleSignOnUtilities.addUserToGroup(userObjectId, Constants.SINGLESIGNON_ACCESS_STUDENTGROUP))
                {
                    Log.error(this.className, 'enableProspectSSO', 'Failed to add user to {0} user group',
                        new List<String> { Constants.SINGLESIGNON_ACCESS_STUDENTGROUP});
                    throw new Exceptions.ApplicationException(String.format('Failed to add user to {0} group', new List<String> { Constants.SINGLESIGNON_ACCESS_STUDENTGROUP }));
                }
            }

            contact.SingleSignOnStatus__c = Constants.CONTACT_SINGLESIGNONSTATUS_STUDENTENABLED;
            contact.SingleSignOnStatusLastUpdated__c = Datetime.now();
            toUpdateRecord = true;

            Log.info(this.className, 'enableProspectSSO', 'Successfully provisoned student in Azure AD');
        }

        if (String.isNotEmpty(contact.SingleSignOnSyncMessage__c))
            contact.SingleSignOnSyncMessage__c = '';

        if (toUpdateRecord)
            Database.update(contact);
    }
}