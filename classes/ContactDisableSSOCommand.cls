public class ContactDisableSSOCommand extends BaseQueueableCommand
{
    private static String fields = 'Cerebro_Student_ID__c, Id, SingleSignOnStatus__c, SingleSignOnStatusLastUpdated__c, SSO_ID__c';

    public ContactDisableSSOCommand()
    {
        super(1);
    }

    public override List<SObject> validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        List<Contact> contacts = new List<Contact>();

        for (Contact contact : (List<Contact>) items)
        {
            if (String.isNotBlank(contact.Cerebro_Student_ID__c))
                contacts.add(contact);
        }

        return contacts;
    }

    public override ActionResult command(List<SObject> items, Id jobId)
    {
       for (Contact contact : (List<Contact>) items)
           disableSSO(contact.Id);

        return new ActionResult(ResultStatus.SUCCESS);
    }

    private void disableSSO(Id contactId)
    {
        Boolean toUpdateRecord = false;

        Log.info(this.className, 'disableSSO', 'Disabling sso for {0}', new List<String> { contactId });
        Contact contact = ContactService.get(contactId, fields, false);
        String signonId = contact.Cerebro_Student_ID__c + SingleSignOnUtilities.getConfiguration().StudentDomain__c;
        String azureObjectId = SingleSignOnUtilities.getUserObjectId(signonId);

        if (String.isBlank(azureObjectId))
        {
            Log.info(this.className, 'disableSSO', 'Skipping disabling of user {0} in Azure AD. User does not exists.',
                new List<String> { contactId });

            if (contact.SingleSignOnStatus__c != Constants.CONTACT_SINGLESIGNONSTATUS_NONE)
            {
                contact.SingleSignOnStatus__c = Constants.CONTACT_SINGLESIGNONSTATUS_NONE;
                contact.SingleSignOnStatusLastUpdated__c = Datetime.now();
                toUpdateRecord = true;
            }
        }
        else
        {
            if (SingleSignOnUtilities.deleteUser(signonId))
            {
                contact.SingleSignOnStatus__c = Constants.CONTACT_SINGLESIGNONSTATUS_DISABLED;
                contact.SingleSignOnStatusLastUpdated__c = Datetime.now();
                toUpdateRecord = true;

                Log.info(this.className, 'disableSSO', 'Successfully disabled contact in Azure AD'); 
            }
            else
            {
                Log.error(this.className, 'disableSSO', 'Failed to disable contact - ContactID: {0} SignonID: {1}', new List<String> { contact.Id, signonId });
                throw new Exceptions.ApplicationException('Failed to disable contact');
            }
        }

        if (toUpdateRecord)
            uow.registerDirty(contact);
    }
}