public class ViewModelsSMS
{
    public class Details implements Interfaces.IViewModel
    {
        public String FromName { get; set; }
        public String ToPhoneNumber { get; set; }
        public String Message { get; set; }

        public Details(String fromName, String toPhoneNumber, String message)
        {
            this.FromName = fromName;
            this.ToPhoneNumber = toPhoneNumber;
            this.Message = message;
        }
    }

    public class SMSMagicPayload implements Interfaces.IViewModel
    {
        public String sender_id { get; set; }
        public String mobile_number { get; set; }
        public String sms_text { get; set; }

        public SMSMagicPayload(String senderId, String mobileNumber, String smsMessage)
        {
            this.sender_id = senderId;
            this.mobile_number = mobileNumber;
            this.sms_text = smsMessage;
        }
    }
    
    public class MarketingCloudPayload implements Interfaces.IViewModel
    {
        public String ContactKey { get; set; }
        public String EventDefinitionKey { get; set; }
        public MarketingCloudBody Data { get; set; }

        public MarketingCloudPayload(){}

        public MarketingCloudPayload(String contactKey, String eventDefinitionKey, MarketingCloudBody data)
        {
            this.ContactKey = contactKey;
            this.EventDefinitionKey = eventDefinitionKey;
            this.Data = data;
        }
    }
    
    public class MarketingCloudBody implements Interfaces.IViewModel
    {
        public String EventId { get; set; }
        public String MobileNumber { get; set; }
        public String Locale { get; set; }
        public String ContactId { get; set; }
        public String SMSMessage { get; set; }

        public MarketingCloudBody(){}

        public MarketingCloudBody(String eventId, String locale, 
            String contactId, String mobileNumber, String smsMessage)
        {
            this.EventId = eventId;
            this.Locale = locale;
            this.ContactId = contactId;
            this.MobileNumber = mobileNumber;
            this.SMSMessage = smsMessage;
        }
    }
}