public class ReferralSaveCommand extends BaseCommand
{
    public override List<SObject> validate(List<SObject> items)
    {
        for (Referral__c referral : (List<Referral__c>) items)
        {
            if (referral.Referrer_Contact__c == null || referral.Referee_Contact__c == null)
            {
                String message = 'Referrer Contact and Referree Contact field values cannot be null';
                Log.error(this.className, 'validate', message);
                throw new Exceptions.InvalidParameterException(message);
            }
        }

        return items;
    }
    
    public override ActionResult command(List<SObject> items)
    {
        Database.insert(items);

        return new ActionResult(ResultStatus.SUCCESS);
    }
}