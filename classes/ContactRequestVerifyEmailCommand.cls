public class ContactRequestVerifyEmailCommand extends BaseComponent
{
    private static final String emailProspectVerificationTemplate = 'Prospect Verification';
    private static Set<String> validVerifyTypes = new Set<String> { 'email', 'prospect' };

    private Boolean validate(Object parameter)
    {
        ViewModels.Param param = (ViewModels.Param) parameter;
        Log.info(this.className, 'validate', 'Identifier {0} \n Payload {1}', new List<String>{ param.Identifier, param.Payload });

        if (String.isEmpty(param.Identifier))
        {
            Log.error(this.className, 'validate',  Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Identifier' });
            throw new Exceptions.InvalidParameterException(String.format(Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Identifier' }));
        }
        else if (ContactService.get(param.Identifier, 'Id', false) == null)
        {
            Log.error(this.className, 'validate', 'Contact does not exists in database: ' + param.Identifier);
            throw new Exceptions.InvalidParameterException('Contact does not exist');
        }
        else if (String.isEmpty(param.Payload))
        {
            Log.error(this.className, 'validate', 'Parameter payload cannot be null: ' + param.Identifier);
            throw new Exceptions.InvalidParameterException('Missing parameter payload');
        }
        else
        {
            Map<String, Object> paramMap = (Map<String, Object>)JSON.deserializeUntyped(param.Payload);
            if (!paramMap.containsKey('type'))
            {
                Log.error(this.className, 'validate', 'Missing type parameter in payload ' + param.Identifier);
                throw new Exceptions.InvalidParameterException('Missing parameter payload');
            }
            else if (String.isEmpty(String.valueOf(paramMap.get('type'))))
            {
                Log.error(this.className, 'validate', 'Missing type parameter value in payload ' + param.Identifier);
                throw new Exceptions.InvalidParameterException('Missing parameter type value');
            }
            else if (!validVerifyTypes.contains(String.valueOf(paramMap.get('type'))))
            {
                Log.error(this.className, 'validate', 'Invalid parameter type value ' + String.valueOf(paramMap.get('type')) + ' ID: ' + param.Identifier);
                throw new Exceptions.InvalidParameterException('Invalid parameter type value');
            }

            if (paramMap.containsKey('endpoint'))
            {
                if (String.isEmpty(String.valueOf(paramMap.get('endpoint'))))
                {
                    Log.error(this.className, 'validate', 'Endpoint parameter value cannot be null or emtpy');
                    throw new Exceptions.InvalidParameterException('Endpoint parameter value cannot be null or emtpy');
                }
            }
        }

        return true;
    }

    public ActionResult command(Object parameter)
    {
        validate(parameter);

        ActionResult result = new ActionResult(ResultStatus.SUCCESS);
        ViewModels.Param param = (ViewModels.Param) parameter;
        Map<String, Object> paramMap = (Map<String, Object>)JSON.deserializeUntyped(param.Payload);

        String type = String.valueOf(paramMap.get('type'));
        Contact contact = ContactService.get(param.Identifier, 'Id, Name, Email, OwnerId, Owner.Email, Owner.Name', false);

        if (type.equalsIgnoreCase('email'))
            result = ContactService.sendEmailVerification(new List<Contact> { contact });
        else if (type.equalsIgnoreCase('prospect'))
            sendEmail(contact.Id, emailProspectVerificationTemplate, param.Payload);

        return result;
    }

    @Future(callout=true)
    public static void sendEmail(Id contactId, String templateName, String params)
    {
        Contact contact = ContactService.get(contactId, 'Id, Name, Email, EncryptedId__c, Owner.Email, Owner.Name', false);
        EmailTemplate emailTemplate = EmailService.getEmailTemplate(templateName);
        Map<String, Object> paramMap = (Map<String, Object>)JSON.deserializeUntyped(params);

        Messaging.SingleEmailMessage mail = Messaging.renderStoredEmailTemplate(emailTemplate.Id, contact.Id, contact.Id);
        String body =  mail.getHtmlBody();
        String textBody = mail.getPlainTextBody();

        if (paramMap.containsKey('endpoint'))
        {
            String endpoint = String.valueOf(paramMap.get('endpoint'));
            String ip = '';

            if (paramMap.containsKey('ip'))
                ip = '&ip=' + String.valueOf(paramMap.get('ip'));
            
            String url = Label.OAFBaseURL + 'email-link/?id=' + contact.EncryptedId__c + '&target=' + EncodingUtil.urlEncode(Label.OAFBaseURL + endpoint + '/?id=' + contact.EncryptedId__c + ip, 'UTF-8');
            body = body.replace('{{url}}', url);
            body = body.replace('{{page}}', Constants.EMAIL_ENDPOINTPAGEMAP.get(endpoint));

            textBody = textBody.replace('{{url}}', url);
            textBody = textBody.replace('{{page}}', Constants.EMAIL_ENDPOINTPAGEMAP.get(endpoint));
        }

        ActionResult result = EmailService.sendToRecipient(Constants.APIPROVIDER_DEFAULT, contact.Email, contact.Name, mail.getSubject(), contact.Owner.Email, contact.Owner.Name, textBody, body, null);

        if (result.isError)
        {
            String message = String.format('Failed to send email verification to {0}', new List<String> { contact.Name });
            Log.error('ContactSendProspectVerificationCommand', 'command', message + ' ' + result);
            throw new Exceptions.ApplicationException(message);
        }
    }
}