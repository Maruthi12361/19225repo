public class TermUpdateStatusBatch extends BaseBatchCommand
{
    String query = 'SELECT Id, hed__Start_Date__c, Status__c, hed__Account__c ' +
                   'FROM hed__Term__c ' +
                   'WHERE Status__c NOT IN (\'Invalid\', \'Past\') AND hed__Account__c != null AND hed__Start_Date__c != null ' +
                   'ORDER BY hed__Account__c, hed__Start_Date__c';


    public TermUpdateStatusBatch()
    {
    }

    public override Database.QueryLocator query(Database.BatchableContext context)
    {
        return Database.getQueryLocator(query);
    }

    public override void command(Database.BatchableContext context, List<sObject> scope)
    {
        ActionResult result = TermService.updateStatus(scope);

        if (result.isError)
            Log.error(this.classname, 'command', result.message);
    }

    public override void finalise(Database.BatchableContext BC)
    {
        database.executeBatch(new CampaignUpdateStatusBatch());
    }
}