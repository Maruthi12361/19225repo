public class ContactSetCountryFieldsValueCommand extends BaseTriggerCommand
{
    private final Set<String> countryFields = new Set<String>{ 'InferredCountry__c', 'MobilePhoneCountry__c', 'PhoneCountry__c', 'RefereeOneMobileCountry__c', 'RefereeTwoMobileCountry__c', 'MailingCountry', 'OtherCountry' };
    protected override List<SObject> validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        List<Contact> result = new List<Contact>();

        for (Contact item : (List<Contact>)items)
        {
            for (String countryField : countryFields)
            {
                String countryFieldValue = String.valueOf(item.get(countryField));
                if (!String.isEmpty(countryFieldValue) && countryFieldValue.length() == 2)
                {
                    result.add(item);
                    break;
                }
            }
        }

        return result;
    }

    protected override ActionResult command(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        for (Contact contact : (List<Contact>)items)
        {
            for (String countryField : countryFields)
            {
                String countryFieldValue = String.valueOf(contact.get(countryField));

                if (!String.isEmpty(countryFieldValue) && countryFieldValue.length() == 2)
                {
                    String countryName = PhoneNumberUtilities.getCountryName(countryFieldValue);

                    if (!String.isEmpty(countryName))
                        contact.put(countryField, countryName);
                }
            }
        }

        return new ActionResult(ResultStatus.SUCCESS);
    }
}