public class ContactExpressConsentBatch extends BaseBatchCommand
{
    public static final String DEFAULT_QUERY = 'SELECT Id, Phone, MobilePhone, OtherPhone, HomePhone, hed__WorkPhone__c, Express_Consent_Expires__c, Express_Consent_Exists__c, DoNotCall,  MailingCountry, OK_To_Call__c, ' +
                                               '    dncau__Phone_DNC_Status__c, dncau__HomePhone_DNC_Status__c, dncau__MobilePhone_DNC_Status__c, WorkPhone_DNC_Status__c, dncau__OtherPhone_DNC_Status__c, ' +
                                               '    dncau__Phone_DNC_Checked__c, dncau__HomePhone_DNC_Checked__c, dncau__MobilePhone_DNC_Checked__c, WorkPhone_DNC_Checked__c, dncau__OtherPhone_DNC_Checked__c ' +
                                               'FROM Contact ';

    private static final string PHONE = 'Phone';
    private static final string HOME = 'Home Phone';
    private static final string MOBILE = 'Mobile Phone';
    private static final string WORK = 'Work Phone';
    private static final string OTHER = 'Other Phone';

    String query;

    public ContactExpressConsentBatch(String qry)
    {
        this.query = qry;
    }

    public override Database.QueryLocator query(Database.BatchableContext BC)
    {
        return Database.getQueryLocator(query);
    }

    public override void command(Database.BatchableContext BC, List<sObject> scope)
    {
        List<Contact> contacts = new List<Contact>();
        for(sObject obj: scope)
        {
            Contact c = (Contact)obj;
            if(updateOKToCall(c))
            {
                contacts.add(c);
            }
        }

        Log.info(this.className, 'getUserObjectId', 'Number of Contacts to be updated: ' + contacts.size());
        update contacts;
    }

    private Boolean updateOKToCall(Contact con)
    {
        Boolean currentExists = con.Express_Consent_Exists__c;
        String currentString;
        Boolean updated = false;

        // Correct Express Consent Flag
        con.Express_Consent_Exists__c = con.Express_Consent_Expires__c > System.today();
        if (currentExists != con.Express_Consent_Exists__c)
            updated = true;

        if(con.DoNotCall)
        {
            con.OK_To_Call__c = Constants.DNC_NOT_OK_ANY_NUMBER;
            updated = true;
        }
        else
        {
            Boolean okToCallPhone = isOKToCallDNC(con.dncau__Phone_DNC_Status__c, con.dncau__Phone_DNC_Checked__c);
            Boolean okToCallHome = isOKToCallDNC(con.dncau__HomePhone_DNC_Status__c, con.dncau__HomePhone_DNC_Checked__c);
            Boolean okToCallMobile = isOKToCallDNC(con.dncau__MobilePhone_DNC_Status__c, con.dncau__MobilePhone_DNC_Checked__c);
            Boolean okToCallWork = isOKToCallDNC(con.WorkPhone_DNC_Status__c, con.WorkPhone_DNC_Checked__c);
            Boolean okToCallOther = isOKToCallDNC(con.dncau__OtherPhone_DNC_Status__c, con.dncau__OtherPhone_DNC_Checked__c);

            // Correct OK To Call text
            currentString = con.OK_To_Call__c;
            if (con.Express_Consent_Exists__c || (okToCallPhone && okToCallHome && okToCallMobile && okToCallWork && okToCallOther))
                con.OK_To_Call__c = Constants.DNC_OK_ANY_NUMBER;
            else if (!okToCallPhone && !okToCallHome && !okToCallMobile && !okToCallWork && !okToCallOther)
                con.OK_To_Call__c = Constants.DNC_NOT_OK_ANY_NUMBER;
            else
            {
                string okToCall = 'OK - ';

                if (okToCallPhone)
                    okToCall += PHONE + ', ';
                if (okToCallHome)
                    okToCall += HOME + ', ';
                if (okToCallMobile)
                    okToCall += MOBILE + ', ';
                if (okToCallWork)
                    okToCall += WORK + ', ';
                if (okToCallOther)
                    okToCall += OTHER + ', ';

                con.OK_To_Call__c = okToCall.substring(0, okToCall.length() - 2);
            }

            if (currentString != con.OK_To_Call__c)
                updated = true;
        }

        return updated;
    }

    private Boolean isOKToCallDNC(string status, Datetime checkedDate)
    {
        Date TODAY = System.today();

        if (status == Constants.DNC_CAN_CALL_STATUS && checkedDate > TODAY.addDays(-30))
            return true;
        else
            return false;
    }
}