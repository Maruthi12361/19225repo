@isTest
public class TestDataCaseMock
{
    public static Case createApplication(User user) 
    {
        return createApplication(user, TestDataContactMock.createStudent(user));
    }

    public static Case createApplication(User user, Contact contact)
    {
        return createApplication(user, contact, null);
    }

    public static Case createApplication(User user, Contact contact, Opportunity opp)
    {
        Case applicationCase = new Case
        (
            RecordTypeId = RecordTypeUtils.getRecordTypeId(Constants.RECORDTYPE_CASE_APPLICATION, SObjectType.Case),
            ContactId = contact.Id,
            Subject = 'Application',
            Status = Constants.CASE_STATUS_NEW,
            Department__c = Constants.CASE_DEPT_ACQUISITIONS,
            Invoice_Processed__c = false,
            FEE_HELP_Processed__c = false,
            Welcome_Letter_Sent__c = false
        );

        if (opp != null)
        {
            applicationCase.Opportunity__c = opp.Id;
        }

        return insertCaseAsUser(applicationCase, user);
    }

    public static Case createAwardCase(User user, Contact contact)
    {
        Case awardCase = new Case
        (
            RecordTypeId = RecordTypeUtils.getRecordTypeId(Constants.RECORDTYPE_CASE_AWARD, SObjectType.Case),
            ContactId = contact.Id,
            WorkType__c = 'Full Course Completion',
            Status = Constants.CASE_STATUS_IN_PROGRESS,
            Department__c = Constants.CASE_DEPT_STUDENT_CENTRAL,
            Exam_Date__c = System.today().addDays(20),
            Exit_Date__c = System.today(),
            Teaching_Centre__c = Constants.AIB_DL,
            Qualification_Award__c = 'Diploma of Management'
        );

        return insertCaseAsUser(awardCase, user);
    }

    public static Case createRolloverCase(User user, Contact contact)
    {
        Case rolloverCase = new Case
        (
            RecordTypeId = RecordTypeUtils.getRecordTypeId(Constants.RECORDTYPE_CASE_ROLLOVER, SObjectType.Case),
            ContactId = contact.Id,
            Status = Constants.CASE_STATUS_NEW,
            Department__c = Constants.CASE_DEPT_PROGRESSIONS,
            Revenue_Amount__c = 8888,
            Payment_Method__c = 'Upfront/FIC/MC',
            Timetable_Confirmed__c = true
        );

        return insertCaseAsUser(rolloverCase, user);
    }

    public static Case createStudentSupportCase(User user, Contact contact)
    {
        return createStudentSupportCase(user, contact, true);
    }

    public static Case createStudentSupportCase(User user, Contact contact, Boolean saveRecord)
    {
        return createStudentSupportCase(user, contact, 'General', null, saveRecord);
    }

    public static Case createStudentSupportCase(User user, Contact contact, String workType, String workSubType, Boolean saveRecord)
    {
        Case studentSupportCase = new Case
        (
            RecordTypeId = RecordTypeUtils.getRecordTypeId(Constants.RECORDTYPE_CASE_STUDENT_SUPPORT, SObjectType.Case),
            ContactId = contact.Id,
            Subject = 'Student Support Case',
            Status = Constants.CASE_STATUS_NEW,
            Department__c = Constants.CASE_DEPT_STUDENT_CENTRAL,
            WorkType__c = workType,
            WorkSubType__c = workSubType
        );

        return saveRecord ? insertCaseAsUser(studentSupportCase, user) : studentSupportCase;
    }

    public static Case createExemptionCase(User user, Contact contact)
    {
        Case exemptionCase = new Case
        (
            RecordTypeId = RecordTypeUtils.getRecordTypeId(Constants.RECORDTYPE_CASE_EXEMPTIONS, SObjectType.Case),
            ContactId = contact.Id,
            Subject = 'Test Exemption Case',
            Status = Constants.CASE_STATUS_NEW,
            Department__c = Constants.CASE_DEPT_STUDENT_CENTRAL,
            WorkType__c = 'Credit Transfer'
        );

        return insertCaseAsUser(exemptionCase, user);
    }

    private static Case insertCaseAsUser(Case caseToStore, User user)
    {
        System.runAs(user)
        {
            insert caseToStore;
        }

        return caseToStore;
    }
}