public class PlanRequirementTriggerHandler extends BaseTriggerHandler
{
    public override void afterInsert()
    {
        ActionResult result = PlanRequirementService.updateParent(Trigger.new, Trigger.oldMap, Enums.TriggerContext.AFTER_INSERT);
        
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);
    }
    
    public override void afterUpdate()
    {
        ActionResult result = PlanRequirementService.updateParent(Trigger.new, Trigger.oldMap, Enums.TriggerContext.AFTER_UPDATE);
        
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);
    }
    
    public override void afterDelete()
    {
        ActionResult result = PlanRequirementService.updateParent(Trigger.old, Trigger.oldMap, Enums.TriggerContext.AFTER_DELETE);
        
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);
    }
}