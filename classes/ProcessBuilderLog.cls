public class ProcessBuilderLog
{   
    @InvocableMethod(label='Print To Log' description='Print out to log to notify of an action being executed')
    public static void PrintToLog(List<ProcessBuilderParameter> params)
    {
        for (ProcessBuilderParameter p: params)
        {
        	Log.info('ProcessBuilderLog', 'PrintToLog', 'Process Builder Debug: Action: ' + p.actionName + '; Process: ' + p.processName);    
        }    	
    }
    
    public class ProcessBuilderParameter 
	{
    	@InvocableVariable(required = true)
		public String actionName;
    
    	@InvocableVariable(required = true)
		public String processName;
	}
}