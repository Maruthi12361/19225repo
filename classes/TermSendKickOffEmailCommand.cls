public class TermSendKickOffEmailCommand extends BaseQueueableCommand
{
    private static final String templateName = 'Workforce Management - Term Kickoff';

    public TermSendKickOffEmailCommand()
    {
        super(1);
    }

    public override List<SObject> validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        EmailTemplate emailTemplate = EmailService.getEmailTemplate(templateName);

        if (emailTemplate == null)
        {
            String message = String.format('No email template found for {0}', new List<String> { templateName });
            Log.error(this.className, 'validate', message);
            throw new Exceptions.ApplicationException(message);
        }

        return items;
    }

    public override ActionResult command(List<SObject> items, Id jobId)
    {
        EmailTemplate emailTemplate = EmailService.getEmailTemplate(templateName);
        List<hed__Course_Offering__c> termsSubjectOffering = getTermsSubjectOfferings(SObjectUtils.getIds(items, 'Id'));
        List<hed__Course__c> subjectOfferingSubjects = getSubjects(SObjectUtils.getIDs(termsSubjectOffering, 'hed__Course__c'));
        List<hed__Course_Enrollment__c> previousSubjectEnrolments = getFacultyPreviousSubjectOfferingSubjectEnrolments(termsSubjectOffering, subjectOfferingSubjects);
        List<SkillSet__c> skillSets = getSkillSets(SObjectUtils.getIds(subjectOfferingSubjects, 'SubjectDefinition__c'));
        List<hed__Affiliation__c> olfAppointments =  getStaffAppointmentAffiliations(SObjectUtils.getIDs(skillSets, 'Contact__c'));

        for (hed__Term__c term : (List<hed__Term__c>)items)
        {
            List<hed__Course_Offering__c> subjectOfferings = getSubjectOfferings(term.Id, termsSubjectOffering);

            for (hed__Course_Offering__c offering : subjectOfferings)
            {
                String body;
                String subject;
                Messaging.SingleEmailMessage mail;

                if (offering.hed__Course__r == null)
                {
                    Log.info(this.className, 'command', 'Kick off email not sent {0} ({1}) subject offering as subject is either not set or does not exist',
                            new List<String> { offering.Name, offering.Id});
                    continue;
                }
                else if (offering.hed__Course__r.HeadOfDiscipline__r == null)
                {
                    Log.info(this.className, 'command', 'Kick off email not sent {0} ({1}) subject offering as head of discipline is either not set or does not exist',
                            new List<String> { offering.Name, offering.Id});
                    continue;
                }
                else if (offering.hed__Course__r.HeadOfDiscipline__r.Email == null)
                {
                    Log.info(this.className, 'command', 'Kick off email not sent {0} ({1}) subject offering as head of discipline email is not set',
                            new List<String> { offering.Name, offering.Id});
                    continue;
                }

                mail = Messaging.renderStoredEmailTemplate(emailTemplate.Id, offering.hed__Course__r.HeadOfDiscipline__c, null);
                body = mail.getHtmlBody();
                subject = mail.getSubject();

                body = body.replace('{headOfDiscipline.FirstName}', offering.hed__Course__r.HeadOfDiscipline__r.FirstName);
                body = body.replace('{term.TermName}', term.Name);
                body = body.replace('{subjectVersion.SubjectName}', offering.hed__Course__r.Name);

                subject = subject.replace('{term.TermName}', term.Name);
                subject = subject.replace('{subjectVersion.SubjectName}', offering.hed__Course__r.Name);

                List<Contact> skilledFacilitators = getSubjectSkilledFacilitators(getSubjectOfferingSubjectVersion(offering, subjectOfferingSubjects), skillSets, olfAppointments);

                if (skilledFacilitators.size() == 0)
                    body = body.replace('{table.subjectOffering}', 'No skilled facilitators found for this offering');
                else
                    body = body.replace('{table.subjectOffering}', getTable(skilledFacilitators));

                hed__Course_Offering__c previousSubjectOffering = getPreviousSubjectOffering(offering, subjectOfferingSubjects);

                if (previousSubjectOffering == null)
                    body = body.replace('{table.previoussubjectOffering}', 'No data available for previous offering');
                else
                {
                    List<Contact> facilitators = getSubjectEnrolmentFacilitators(previousSubjectOffering.Id, previousSubjectEnrolments);

                    if (facilitators.size() == 0)
                        body = body.replace('{table.previoussubjectOffering}', 'No data available for previous offering');
                    else
                        body = body.replace('{table.previoussubjectOffering}', getTable(facilitators));
                }

                sendEmail(offering.hed__Course__r.HeadOfDiscipline__r.Email, offering.hed__Course__r.HeadOfDiscipline__r.Name, subject,
                    body, Label.TermKickOff_ReplyEmail, Label.TermKickOff_ReplyName, Label.TermKickOff_ReplyEmail, Label.TermKickOff_ReplyName);
            }
        }

        return new ActionResult(ResultStatus.SUCCESS);
    }

    @Future(callout=true)
    public static void sendEmail(String email, String name, String subject, String body, String replyEmail, String replyName, String ccEmail, String ccName)
    {
        List<ViewModelsMail.Recipient> ccEmails = new List<ViewModelsMail.Recipient> { new ViewModelsMail.Recipient(ccName, ccEmail) };
        ActionResult result = EmailService.sendToRecipient(Constants.APIPROVIDER_DEFAULT, email, name, subject, replyEmail, replyName, null, body, ccEmails);

        if (result.isError)
            Log.error('TermSendKickOffEmailCommand', 'command', 'Failed to send Term kick off email to {0}<{1}> due to: {2}.', new List<String>{ name, email, String.valueOf(result.messages) });
    }

    private static List<hed__Course_Offering__c> getTermsSubjectOfferings(Set<Id> termIds)
    {
        List<hed__Course_Offering__c> offerrings =
        [
            SELECT Id, Name, ActualStudents__c, hed__Term__c, hed__Start_Date__c, hed__End_Date__c, hed__Course__c, hed__Course__r.HeadOfDiscipline__c, 
                hed__Course__r.Name, hed__Course__r.hed__Course_ID__c, hed__Course__r.HeadOfDiscipline__r.Email, 
                hed__Course__r.HeadOfDiscipline__r.FirstName, hed__Course__r.HeadOfDiscipline__r.Name
            FROM hed__Course_Offering__c
            WHERE hed__Term__c = :termIds AND ActualStudents__c > 0
        ];

        return offerrings;
    }

    private static List<hed__Course_Offering__c> getSubjectOfferings(Id termId, List<hed__Course_Offering__c> offerings)
    {
        List<hed__Course_Offering__c> subjectOfferings = new List<hed__Course_Offering__c>();

        for (hed__Course_Offering__c offering : offerings)
        {
            if (offering.hed__Term__c == termId)
                subjectOfferings.add(offering);
        }

        return subjectOfferings;
    }

    private static hed__Course__c getSubjectOfferingSubjectVersion(hed__Course_Offering__c offering, List<hed__Course__c> subjectVersions)
    {
        for (hed__Course__c subjectVersion : subjectVersions)
        {
            if (subjectVersion.Id == offering.hed__Course__c)
                return subjectVersion;
        }

        return null;
    }

    private static hed__Course_Offering__c getPreviousSubjectOffering(hed__Course_Offering__c currentOffering, List<hed__Course__c> courses)
    {
        hed__Course_Offering__c previousOffering;

        for (hed__Course__c course : courses)
        {
            if (course.Id == currentOffering.hed__Course__c)
            {
                for (hed__Course_Offering__c offering : course.hed__Course_Offerings__r)
                {
                    if (currentOffering.Id != offering.Id && offering.hed__Start_Date__c < currentOffering.hed__End_Date__c)
                    {
                        previousOffering = offering;
                        break;
                    }
                }
                break;
            }
        }

        return previousOffering;
    }

    private static List<hed__Course_Enrollment__c> getFacultyPreviousSubjectOfferingSubjectEnrolments(List<hed__Course_Offering__c> termsSubjectOfferings, List<hed__Course__c> subjectOfferingSubjects)
    {
        Set<Id> previousOfferingIds = new Set<Id>();
        List<hed__Course_Enrollment__c> subjectEnrolments = new List<hed__Course_Enrollment__c>();

        for (hed__Course_Offering__c offering : termsSubjectOfferings)
        {
            hed__Course_Offering__c previousOffering = getPreviousSubjectOffering(offering, subjectOfferingSubjects);
            if (previousOffering != null)
                previousOfferingIds.add(previousOffering.Id);
        }

        if (previousOfferingIds.size() == 0)
            return subjectEnrolments;

        subjectEnrolments =
        [
            SELECT Id, hed__Course_Offering__c, hed__Contact__c, hed__Contact__r.Id, hed__Contact__r.Name, hed__Contact__r.FirstName, hed__Contact__r.LastName, hed__Contact__r.Email
            FROM hed__Course_Enrollment__c
            WHERE hed__Course_Offering__c IN :previousOfferingIds AND  RecordType.DeveloperName = 'Faculty'
        ];

        return subjectEnrolments;
    }

    private static List<Contact> getSubjectEnrolmentFacilitators(ID courseOfferingId, List<hed__Course_Enrollment__c> subjectEnrolments)
    {
        List<Contact> facilitators = new List<Contact>();

        for (hed__Course_Enrollment__c subjectEnrolment : subjectEnrolments)
        {
            if (subjectEnrolment.hed__Course_Offering__c == courseOfferingId && subjectEnrolment.hed__Contact__r != null)
                facilitators.add(subjectEnrolment.hed__Contact__r);
        }

        return facilitators;
    }

    private static List<hed__Course__c> getSubjects(Set<Id> ids)
    {
        List<hed__Course__c> subjects =
        [
            SELECT Id, SubjectDefinition__c, (SELECT Id, hed__Start_Date__c, hed__End_Date__c, Active__c FROM hed__Course_Offerings__r ORDER BY hed__Start_Date__c DESC)
            FROM hed__Course__c
            WHERE Id IN :ids
        ];

        return subjects;
    }

    private static List<SkillSet__c> getSkillSets(Set<Id> subjectDefinitionIds)
    {
        return
        [
            SELECT Id, Contact__c, SubjectDefinition__c, Contact__r.Id, Contact__r.Email, Contact__r.Name, Contact__r.FirstName, Contact__r.Lastname
            FROM SkillSet__c
            WHERE SubjectDefinition__c IN :subjectDefinitionIds
        ];
    }

    private static List<Contact> getSubjectSkilledFacilitators(hed__Course__c subjectVersion, List<SkillSet__c> skillSets, List<hed__Affiliation__c> olfAppointments)
    {
        List<Contact> skilledFacilitators = new List<contact>();
        for (SkillSet__c skillSet : skillSets)
        {
            hed__Affiliation__c olfAppointment = getStaffAppointment(skillSet.Contact__c, olfAppointments);

            if (olfAppointment != null && skillSet.SubjectDefinition__c == subjectVersion.SubjectDefinition__c)
                skilledFacilitators.add(skillSet.Contact__r);
        }

        return skilledFacilitators;
    }

    private static List<hed__Affiliation__c> getStaffAppointmentAffiliations(Set<Id> contactIds)
    {
        return
        [
            SELECT Id, hed__Contact__c, hed__Account__c, hed__Account__r.Name, hed__Role__c, PrerequisitesCompleted__c, hed__Status__c
            FROM hed__Affiliation__c
            WHERE RecordType.DeveloperName = :Constants.RECORDTYPE_AFFILIATION_APPOINTMENT AND hed__Contact__c IN :contactIds AND
                hed__Account__r.Name = :Constants.ACCOUNT_INSTITUTION_AIB AND hed__Status__c IN :Constants.AFFILIATION_APPOINTMENTSTATUSES AND
                hed__Role__c IN :Constants.AFFILIATION_ROLE_OLF
        ];
    }

    private static hed__Affiliation__c getStaffAppointment(Id contactId, List<hed__Affiliation__c> appointmentAffiliations)
    {
        List<hed__Affiliation__c> appointments = new List<hed__Affiliation__c>();

        for (hed__Affiliation__c appointment : appointmentAffiliations)
        {
            if (appointment.hed__Contact__c == contactId)
                appointments.add(appointment);
        }

        if (appointments.size() > 0)
            return appointments[0];

        return null;
    }

    private static String getTable(List<Contact> contacts)
    {
        String table =
            '<table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" class="listing" style="color: #313136;font-size: 14px;line-height: 22px;font-family: Helvetica, Arial, sans-serif;border-collapse: collapse;">' +
                '<tbody>' +
                    '<tr>' +
                        '<td style="padding: 10px;border-top: 1px solid #ddd;border-bottom: 1px solid #ddd;"><b>First Name</b></td>' +
                        '<td style="padding: 10px;border-top: 1px solid #ddd;border-bottom: 1px solid #ddd;"><b>Last Name</b></td>' +
                        '<td style="padding: 10px;border-top: 1px solid #ddd;border-bottom: 1px solid #ddd;"><b>Email Address</b></td>' +
                    '</tr>';

        for (Contact contact : contacts)
        {
            String firstName = contact.FirstName != null ? contact.FirstName : '';
            String lastName = contact.LastName != null ? contact.LastName : '';
            String email = contact.Email != null ? contact.Email : 'No email';

            table +=
                    '<tr>' +
                        '<td style="padding: 10px;border-top: 1px solid #ddd;border-bottom: 1px solid #ddd;">' + firstName + '</td>' +
                        '<td style="padding: 10px;border-top: 1px solid #ddd;border-bottom: 1px solid #ddd;">' + lastName + '</td>' +
                        '<td style="padding: 10px;border-top: 1px solid #ddd;border-bottom: 1px solid #ddd;">' +
                            '<a style="color: #d33039;" href="mailto:' + email + '">' + email + '</a>' +
                        '</td>' +
                    '</tr>';
        }

        table +=
                '</tbody>' +
            '</table></br>';

        return table;
    }
}