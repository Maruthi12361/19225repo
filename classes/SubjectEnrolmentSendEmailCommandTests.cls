@isTest
public class SubjectEnrolmentSendEmailCommandTests
{
    @testSetup
    static void setup()
    {        
        TestUtilities.setupHEDA();
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact facultyContact = TestDataContactMock.createFacultyContact(UserService.getByAlias(Constants.USER_SYSTEMADMIN));

        Account department = TestDataAccountMock.createDepartmentBusiness(user);
        hed__Term__c term = TestDataTermMock.create(user, department.Id, Date.today());
        hed__Course__c subjectDefinition = TestDataSubjectMock.createDefinitionCGOV(user);
        hed__Course__c subjectVersion = TestDataSubjectMock.createVersionCGOV(user);
        hed__Course_Offering__c subjectOffering = TestDataSubjectOfferingMock.createOffering(user, subjectVersion, term);

        TestDataSubjectEnrolmentMock.createFacultyEnrolment(user, subjectOffering, facultyContact, 'Requested', 'Subject Coordinator');
    }

    @isTest
    static void SubjectEnrolmentSendEmail_EmailSuccessfullySent()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        List<hed__Course_Enrollment__c> enrolments = [SELECT Id, hed__Contact__c FROM hed__Course_Enrollment__c];

        // Act
        Test.startTest();
        ActionResult result = new SubjectEnrolmentSendEmailCommand().execute(enrolments, null, null);
        Test.stopTest();

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Unexpected result status.');
    }
}