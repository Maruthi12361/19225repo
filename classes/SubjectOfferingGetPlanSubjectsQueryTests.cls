@isTest
public class SubjectOfferingGetPlanSubjectsQueryTests
{
    @testSetup
    static void setup()
    {
        TestUtilities.setupHEDA();
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Account account = TestDataAccountMock.create(user, 'Test AIB Account');
        hed__Term__c term = TestDataTermMock.create(user, account.Id, Date.today());
        hed__Course__c subjectDefinition = TestDataSubjectMock.createDefinitionCGOV(user);
        hed__Course__c subjectVersion = TestDataSubjectMock.createVersionCGOV(user);
        hed__Course_Offering__c subjectOffering = TestDataSubjectOfferingMock.createOffering(user, subjectVersion, term);

        Account courseMBA = TestDataAccountMock.createCourseMBA(user);
        Contact student = TestDataContactMock.createStudent(user);
        TestDataCourseEnrolmentMock.create(user, courseMBA.Id, student.Id, 'Ongoing');
    }

    @isTest
    static void TermHasSubjectOfferingWithNoEnrolledStudents_NoSubjectOfferingsReturned()
    {
        // Arrange
        hed__Term__c term = [SELECT Id FROM hed__Term__c LIMIT 1];
        ViewModels.Param param = new ViewModels.Param();
        param.Identifier = term.Id;

        // Act
        test.startTest();
        List<ViewModelsWorkforce.SubjectOfferingListItem> subjectOfferings = new SubjectOfferingGetPlanSubjectsQuery().query(param);
        test.stopTest();

        // Assert
        System.assertEquals(0, subjectOfferings.size(), 'There should be no subject offerings returned');
    }

    @isTest
    static void TermHasSubjectOfferingWithSubjectEnrolment_SuccessfullyReturnedListOfRecords()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        hed__Term__c term = [SELECT Id FROM hed__Term__c LIMIT 1];
        hed__Course_Offering__c subjectOffering = [SELECT Id FROM hed__Course_Offering__c LIMIT 1];
        hed__Program_Enrollment__c courseEnrolment = [SELECT Id, hed__Contact__c, hed__Account__c FROM hed__Program_Enrollment__c LIMIT 1];
        TestDataSubjectEnrolmentMock.create(user, subjectOffering, courseEnrolment, 'Enrolled', 'Standard', null, null);

        ViewModels.Param param = new ViewModels.Param();
        param.Identifier = term.Id;

        // Act
        test.startTest();
        List<ViewModelsWorkforce.SubjectOfferingListItem> subjectOfferings = new SubjectOfferingGetPlanSubjectsQuery().query(param);
        test.stopTest();

        // Assert
        System.assert(subjectOfferings.size() > 0, 'Subject offering size should not be 0');
    }
}