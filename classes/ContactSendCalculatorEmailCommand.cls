public class ContactSendCalculatorEmailCommand extends BaseComponent
{
    private Boolean validate(Object parameter)
    {
        ViewModels.Param param = (ViewModels.Param) parameter;

        if (String.isEmpty(param.Identifier))
        {
            Log.error(this.className, 'validate',  Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Identifier' });
            throw new Exceptions.InvalidParameterException(String.format(Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Identifier' }));
        }
        else if (String.isEmpty(param.Payload))
        {
            Log.error(this.className, 'validate',  Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Calculator' });
            throw new Exceptions.InvalidParameterException(String.format(Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Calculator' }));
        }
        else
        {
            Map<String, Object> payloadMap = (Map<String, Object>)JSON.deserializeUntyped(param.Payload);

            if (payloadMap.size() == 0)
            {
                Log.error(this.className, 'validate',  Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Calculator Body' });
                throw new Exceptions.InvalidParameterException(String.format(Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Calculator Body' }));
            }
        }

        return true;
    }

    public ActionResult command(Object parameter)
    {
        validate(parameter);

        ViewModels.Param param = (ViewModels.Param) parameter;
        Log.info(this.className, 'command', 'Request made with following parameter: Identifier {0} \n Prospect {1}', new List<String>{ param.Identifier, param.Payload });

        Map<String, Object> payloadMap = (Map<String, Object>)JSON.deserializeUntyped(param.Payload);

        String templateName = getEmailTemplateName(String.valueOf(payloadMap.get('EmailName')));

        sendEmail(param.Identifier, templateName);
        generateCacheImage(param.Identifier, String.valueOf(payloadMap.get('EmailName')));

        return new ActionResult(ResultStatus.SUCCESS);
    }

    @future(callout = true)
    private static void sendEmail(Id contactId, String templateName)
    {
        Id whatId = contactId;
        String fromName = Label.CalculatorEmail_FromName;
        String fromAddress = Label.CalculatorEmail_FromAddress;
        EmailTemplate template = EmailService.getEmailTemplate(templateName);
        Contact contact =
        [
            SELECT Id, Name, Email, OwnerId
            FROM Contact
            WHERE Id = :contactId
        ];

        Messaging.SingleEmailMessage mail = Messaging.renderStoredEmailTemplate(template.Id, contact.OwnerId, contact.Id);

        ActionResult result = EmailService.sendToRecipient(Constants.APIPROVIDER_DEFAULT, contact.Email, contact.Name, mail.getSubject(), fromAddress, fromName, null, mail.getHtmlBody(), null);

        if (result.isError)
            Log.error('ContactSendCalculatorEmailCommand', 'sendEmail', 'Failed to send email {0} to {1}<{2}> due to: {3}.', new List<String> { templateName, contact.Name, contact.Email, String.valueOf(result.messages) });
        else
            Log.info('ContactSendCalculatorEmailCommand', 'sendEmail', 'Successfully sent email {0} to {1}<{2}>.', new List<String>{ templateName, contact.Name, contact.Email });
    }

    @future(callout = true)
    private static void generateCacheImage(Id contactId, String calculatorName)
    {
        Contact contact =
        [
            SELECT Id, CalculatorAgeRange__c, CalculatorPayPeriod__c, CalculatorRemainingWorkYears__c, CalculatorSalaryPerAnnum__c, 
                CalculatorStudyCommuting__c, CalculatorStudyEvening__c, CalculatorStudyMorning__c, CalculatorStudyWeekend__c, CalculatorStudyWork__c
            FROM Contact
            WHERE Id = :contactId
        ];

        HttpRequest req = new HttpRequest();
        String endpoint = getImageEndpoint(calculatorName, contact);

        if (endpoint == null)
            return;

        if (Test.isRunningTest())
        {
            req.setEndpoint('https://online.aibtest.edu.au/' + endpoint);
            req.setHeader('Accept', 'image/png');
            req.setHeader('Content-Type', 'image/png');
        }
        else
        {
            req.setEndpoint(Label.OAFBaseURL + endpoint);
        }

        req.setMethod('GET');

        Log.info('ContactSendCalculatorEmailCommand', 'generateCacheImage', 'Making image cache request: ' + Label.OAFBaseURL + endpoint);
        Http http = new Http();
        HTTPResponse response = http.send(req);

        if (response.getStatusCode() != 200)
            Log.error('ContactSendCalculatorEmailCommand', 'generateCacheImage', 'Error caching calculator image. Error Code: {0}, Error Body: {1}', new String [] { String.valueOf(response.getStatusCode()), response.getBody() });
    }

    private String getEmailTemplateName(String emailName)
    {
        switch on emailName 
        {
           when 'Calculator_FeeHelp'
           {
               return 'Fee Help Calculator';
           }
           when 'Calculator_TimeToStudy'
           {
               return 'Time To Study Calculator';
           }
           when 'Calculator_ROI'
           {
               return 'ROI Calculator';
           }
           when else
           {
               return '';
           }
        }
    }

    private static String getImageEndpoint(String calculatorName, Contact contact)
    {
        String endpoint = null;
        Map<String, String> tagFieldMap = new Map<String,String>
        {
            '{Cnt_CalculatorStudyMorning__c}' => 'CalculatorStudyMorning__c',
            '{Cnt_CalculatorStudyEvening__c}' => 'CalculatorStudyEvening__c',
            '{Cnt_CalculatorStudyCommuting__c}' => 'CalculatorStudyCommuting__c',
            '{Cnt_CalculatorStudyWork__c}' => 'CalculatorStudyWork__c',
            '{Cnt_CalculatorStudyWeekend__c}' => 'CalculatorStudyWeekend__c',
            '{Cnt_CalculatorSalaryPerAnnum__c}' => 'CalculatorSalaryPerAnnum__c',
            '{Cnt_CalculatorAgeRange__c}' => 'CalculatorAgeRange__c',
            '{Cnt_CalculatorRemainingWorkYears__c}' => 'CalculatorRemainingWorkYears__c',
            '{Cnt_CalculatorPayPeriod__c}' => 'CalculatorPayPeriod__c'
        };

        switch on calculatorName 
        {
           when 'Calculator_FeeHelp'
           {
               endpoint = Label.FeeHelpCalculator_ImageEndpoint;
           }
           when 'Calculator_TimeToStudy'
           {
               endpoint = Label.TimeToStudyCalculator_ImageEndpoint;
           }
           when 'Calculator_ROI'
           {
               endpoint = Label.ROICalculator_ImageEndpoint;
           }
           when else
           {
               endpoint = null;
           }
        }

        for (String tag : tagFieldMap.keySet())
        {
            if (endpoint.contains(tag))
            {
                Object fieldValue = contact.get(tagFieldMap.get(tag));
                if (fieldValue == null)
                {
                    Log.info('ContactSendCalculatorEmailCommand', 'getImageEndpoint', 'Field value for {0} tag is null', new List<String> {tag});
                    endpoint = null;
                    break;
                }

                if (tag == '{Cnt_CalculatorSalaryPerAnnum__c}')
                {
                    List<String> args = new String[]{'0','number','currency'};
                    Decimal salary = Decimal.valueOf(String.valueOf(fieldValue));
                    endpoint = endpoint.replace(tag, '$' + String.format(salary.format(), args));
                }
                else
                {
                    endpoint = endpoint.replace(tag, String.valueOf(fieldValue));
                }
            }
        }

        return endpoint;
    }
}