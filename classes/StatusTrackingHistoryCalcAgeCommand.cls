public class StatusTrackingHistoryCalcAgeCommand extends BaseCommand
{
    public override List<SObject> validate(List<SObject> items)
    {
        List<Status_Tracking_History__c> history = new List<Status_Tracking_History__c>();
        long yesterday = System.now().addDays(-1).getTime();

        for (Status_tracking_History__c sth : (List<Status_Tracking_History__c>) items)
            if (sth.EndDateTime__c == null || sth.BusinessMinutesInStage__c == null || sth.EndDateTime__c.getTime() > yesterday)
                history.add(sth);

        return history;
    }

    public override ActionResult command(List<SObject> items)
    {
        Log.info(this.className, 'command', 'Updating {0} STH Records', new List<String>{ String.valueOf(items.size()) });
        Map<Id, Status_Tracking_History__c> caseSTHRecords = new Map<Id, Status_Tracking_History__c>();
        Set<Id> caseIds = new Set<Id>();

        for (Status_tracking_History__c sth : (List<Status_Tracking_History__c>) items)
        {
            Datetime endDateTimeToUse = sth.EndDateTime__c == null ? System.now() : sth.EndDateTime__c;
            Decimal minutesInStage = DateTimeUtils.getTimeAs(DateTimeUtils.getBusinessElapsedTime(sth.StartDateTime__c, endDateTimeToUse), DateTimeUtils.Duration.MINUTES);

            if (sth.BusinessMinutesInStage__c != minutesInStage)
            {
                sth.BusinessMinutesInStage__c = minutesInStage;
                uow.registerDirty(sth);

                if (sth.CaseId__c != null)
                {
                    caseSTHRecords.put(sth.Id, sth);
                    caseIDs.add(sth.CaseId__c);
                }
            }
        }

        List<Case> parentCases = [SELECT Id, AgeBusinessMinutes__c FROM Case WHERE Id in : caseIds ];

        for (Case parentCase : parentCases)
        {
            Decimal minutes = 0;
            List<Status_Tracking_History__c> records = [SELECT Id, BusinessMinutesInStage__c FROM Status_Tracking_History__c WHERE CaseId__c =: parentCase.Id];
            for (Status_Tracking_History__c sth : records)
            {
                Status_Tracking_History__c updatedSTH = caseSTHRecords.get(sth.Id);
                if (updatedSTH != null)
                    minutes = minutes + updatedSTH.BusinessMinutesInStage__c;
                else
                    minutes = minutes + (sth.BusinessMinutesInStage__c == null ? 0 : sth.BusinessMinutesInStage__c);
            }

            if (parentCase.AgeBusinessMinutes__c != minutes)
            {
                Log.info(this.className, 'command', 'Updating parentCase {0} with new AgeBusinessMinutes value {1}', new List<String>{ String.valueOf(parentCase.Id), minutes.toPlainString() });

                parentCase.AgeBusinessMinutes__c = minutes;
                uow.registerDirty(parentCase);
            }
        }

        return new ActionResult(ResultStatus.SUCCESS);
    }
}