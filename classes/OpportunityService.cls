public class OpportunityService 
{
    public static Opportunity get(Id opportunityId)
    {
        List<Opportunity> items = new OpportunityGetQuery().query(new Set<Id> { opportunityId });
        
        return items.size() == 1 ? items[0] : null;
    }
    
    public static List<Opportunity> get(Set<Id> opportunityIds)
    {
        return new OpportunityGetQuery().query(opportunityIds);
    }

    public static ActionResult close(List<Opportunity> opp)
    {
        return new OpportunityCloseCommand().execute(opp);
    }
    
    public static ActionResult trackStatusHistory(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        return new OpportunityTrackStatusHistoryCommand().execute(items, oldMap, triggerContext);
    }
    
    public static ActionResult syncRolloverStatusToCampaign(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        return new OpportunitySyncStatusToCampaignCommand().execute(items, oldMap, triggerContext);
    }

    public static ActionResult validateInsert(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        return new OpportunityValidateInsertCommand().execute(items, oldMap, triggerContext); 
    }
    
    public static ActionResult sendCLOOEmail(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        return new OpportunitySendCLOOEmailCommand().execute(items, oldMap, triggerContext);
    }

    public static ActionResult assignCampaign(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        return new OpportunityAssignCampaignCommand().execute(items, oldMap, triggerContext);
    }
    
    public static ActionResult queueCLOOEmail(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        return new OpportunityQueueCLOOEmailCommand().execute(items, oldMap, triggerContext);
    }
    
    public static ActionResult checkReopen(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        return new OpportunityCheckReopenCommand().execute(items, oldMap, triggerContext);
    }
    
    public static ActionResult updateSSOStatus(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        return new OpportunityUpdateSSOStatusCommand().execute(items, oldMap, triggerContext);
    }
}