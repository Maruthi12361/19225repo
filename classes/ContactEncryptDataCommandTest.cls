@isTest
public class ContactEncryptDataCommandTest 
{
    @testSetup
    static void setup()
    {
        TestUtilities.setupHEDA();
    }

    @isTest
    static void CreateContact_ContactIDEncrypted()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact contact = TestDataContactMock.createStudent(user);
        
        // Act
        Contact updatedContact = [SELECT Id, EncryptedId__c FROM Contact WHERE Id = :contact.Id];

        // Assert
        System.assert(!String.isEmpty(updatedContact.EncryptedId__c), 'Encrypted id should not be empty');
    } 
}