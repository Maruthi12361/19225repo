public class ContactSetCourseConsultantCommand extends BaseTriggerCommand
{
    public override List<SObject> validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        List<Contact> contacts = new List<Contact>();

        if (triggerContext == Enums.TriggerContext.BEFORE_UPDATE)
        {
            for (Contact c : (List<Contact>)items)
            {
                Contact oldContact = (Contact)oldMap.get(c.id);

                if (c.RecordTypeId == System.Label.ID_RecordType_Contact_Prospect && c.OwnerId != null && c.OwnerId != oldContact.OwnerId && 
                    GroupMemberUtils.containsUser(Constants.GROUP_COURSE_CONSULTANTS, oldContact.OwnerId) && 
                    GroupMemberUtils.containsUser(Constants.GROUP_COURSE_ADVISORS, c.OwnerId))
                    contacts.add(c);
            }
        }

        return contacts;
    }

    public override ActionResult command(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        Log.info(this.className, 'command', 'Setting FinalCourseConsultant__c on {0} Contacts', new List<String>{ String.valueOf(items.size()) });

        for (Contact c: (List<Contact>)items)
        {
            Contact oldContact = (Contact)oldMap.get(c.id);
            c.FinalCourseConsultant__c = oldContact.OwnerId;
        }

        return new ActionResult(ResultStatus.SUCCESS); 
    }
}