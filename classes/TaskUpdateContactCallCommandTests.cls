@IsTest
public class TaskUpdateContactCallCommandTests
{
    @testSetup
    static void setup()
    {
        // Arrange
        TestUtilities.setupHEDA();
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createProspectHot(user);
    }

    @IsTest
    static void NonNVMCallTask_ContactCallOutcomeFieldsNotUpdated()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = [SELECT Id FROM Contact LIMIT 1];
        Task task = TestDataTaskMock.createNVMCallTask(user, contact.Id, 'Left Voicemail Message', 'Call to Prospect', true);
        
        Map<Id, SObject> oldMap = new Map<Id, SObject>();
        oldMap.put(task.Id, task);

        Task newTaskCall = task.clone(true, true);

        // Act
        ActionResult result = TaskService.updateContactCall(new List<SObject>{ newTaskCall }, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        Contact updatedContact = [SELECT Id, LastCallOutcome__c, LastCallDateTime__c, NVMConnect__NextContactTime__c FROM Contact WHERE Id = :contact.Id];

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.status, 'Unexpected result status');
        System.assertEquals(null, updatedContact.LastCallOutcome__c, 'Unexpected LastCallOutcome field value');
        System.assertEquals(null, updatedContact.LastCallDateTime__c, 'Unexpected LastCallDateTime field value');
        System.assertEquals(null, updatedContact.NVMConnect__NextContactTime__c, 'Unexpected NextContactTime field value');
    }

    @IsTest
    static void NVMCallTask_ContactCallOutcomeFieldsUpdatedSuccessfully()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = [SELECT Id FROM Contact LIMIT 1];
        NVMStatsSF__NVM_Call_Summary__c callSummary = TestDataNVMCallSummaryMock.create(user);
        Task task = TestDataTaskMock.createNVMCallTask(user, contact.Id, 'Left Voicemail Message', 'Call to Prospect', true, callSummary.Id);
        
        Map<Id, SObject> oldMap = new Map<Id, SObject>();
        oldMap.put(task.Id, task);

        Task newTaskCall = task.clone(true, true);

        // Act
        ActionResult result = TaskService.updateContactCall(new List<SObject>{ newTaskCall }, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        Contact updatedContact = [SELECT Id, LastCallOutcome__c, LastCallDateTime__c, NVMConnect__NextContactTime__c FROM Contact WHERE Id = :contact.Id];

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.status, 'Unexpected result status');
        System.assertEquals(task.Call_Outcome__c, updatedContact.LastCallOutcome__c, 'Unexpected LastCallOutcome field value');
        System.assertNotEquals(null, updatedContact.LastCallDateTime__c, 'Unexpected LastCallDateTime field value');
        System.assertEquals(updatedContact.LastCallDateTime__c.addHours(9), updatedContact.NVMConnect__NextContactTime__c, 'Unexpected NextContactTime field value');
    }

    @IsTest
    static void NVMCallTaskContactNotFound_ContactCallOutcomeFieldsNotUpdated()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = [SELECT Id FROM Contact LIMIT 1];
        NVMStatsSF__NVM_Call_Summary__c callSummary = TestDataNVMCallSummaryMock.create(user);
        Task task = TestDataTaskMock.createNVMCallTask(user, contact.Id, 'Left Voicemail Message', 'Call to Prospect', true, callSummary.Id);
        
        Map<Id, SObject> oldMap = new Map<Id, SObject>();
        oldMap.put(task.Id, task);

        Task newTaskCall = task.clone(true, true);
        newTaskCall.whoId = Id.valueOf('0035O0000021Llh');

        // Act
        ActionResult result = TaskService.updateContactCall(new List<SObject>{ newTaskCall }, oldMap, Enums.TriggerContext.AFTER_UPDATE);

        // Assert
        System.assertEquals(ResultStatus.ERROR, result.status, 'Unexpected result status');
    }

    @IsTest
    static void NVMCallTaskNotLatest_ContactCallOutcomeFieldsNotUpdated()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = [SELECT Id FROM Contact LIMIT 1];
        NVMStatsSF__NVM_Call_Summary__c latestTaskcallSummary = TestDataNVMCallSummaryMock.create(user);
        Task latestTask = TestDataTaskMock.createNVMCallTask(user, contact.Id, 'Left Voicemail Message', 'Call to Prospect', true, latestTaskcallSummary.Id);
        NVMStatsSF__NVM_Call_Summary__c callSummary = TestDataNVMCallSummaryMock.create(user);
        Task task = TestDataTaskMock.createNVMCallTask(user, contact.Id, 'Contact Successful', 'Call to Prospect', true, callSummary.Id);
        Test.setCreatedDate(task.Id, Datetime.now().addDays(-10));

        Map<Id, SObject> oldMap = new Map<Id, SObject>();
        oldMap.put(task.Id, task);
        Task newTaskCall = task.clone(true, true);

        // Act
        ActionResult result = TaskService.updateContactCall(new List<SObject>{ newTaskCall }, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        Contact updatedContact = [SELECT Id, LastCallOutcome__c, LastCallDateTime__c, NVMConnect__NextContactTime__c FROM Contact WHERE Id = :contact.Id];

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.status, 'Unexpected result status');
        System.assertEquals(null, updatedContact.LastCallOutcome__c, 'Unexpected LastCallOutcome field value');
        System.assertEquals(null, updatedContact.LastCallDateTime__c, 'Unexpected LastCallDateTime field value');
        System.assertEquals(null, updatedContact.NVMConnect__NextContactTime__c, 'Unexpected NextContactTime field value');
    }

    @IsTest
    static void NVMCallTaskCalloutSameAsLastCallOutcome_ContactCallOutcomeFieldsNotUpdated()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = [SELECT Id FROM Contact LIMIT 1];
        NVMStatsSF__NVM_Call_Summary__c callSummary = TestDataNVMCallSummaryMock.create(user);
        Task task = TestDataTaskMock.createNVMCallTask(user, contact.Id, 'Contact Successful', 'Call to Prospect', true, callSummary.Id);
        Test.setCreatedDate(task.Id, Datetime.now().addDays(-10));

        contact.LastCallOutcome__c = 'Contact Successful';
        contact.LastCallDateTime__c = Datetime.now().addDays(-10);
        contact.NVMConnect__NextContactTime__c = contact.LastCallDateTime__c.addHours(9);
        update contact;

        Map<Id, SObject> oldMap = new Map<Id, SObject>();
        oldMap.put(task.Id, task);
        Task newTaskCall = task.clone(true, true);

        // Act
        ActionResult result = TaskService.updateContactCall(new List<SObject>{ newTaskCall }, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        Contact updatedContact = [SELECT Id, LastCallOutcome__c, LastCallDateTime__c, NVMConnect__NextContactTime__c FROM Contact WHERE Id = :contact.Id];

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.status, 'Unexpected result status');
        System.assertEquals(contact.LastCallOutcome__c, updatedContact.LastCallOutcome__c, 'Unexpected LastCallOutcome field value');
        System.assertEquals(contact.LastCallDateTime__c, updatedContact.LastCallDateTime__c, 'Unexpected LastCallDateTime field value');
        System.assertEquals(contact.NVMConnect__NextContactTime__c, updatedContact.NVMConnect__NextContactTime__c, 'Unexpected NextContactTime field value');
    }
}