@RestResource(urlMapping='/password-reset/*')
global class ContactPasswordResetController
{
    @HttpPost
    global static void reset()
    {
        String reference = '';
        RestRequest req = RestContext.request;
        ViewModels.Param reqParam = new ViewModels.Param();
        Map<String, Object> optionMap = new Map<String, Object>();

        if (req.headers.containsKey('Reference'))
            reference = req.headers.get('Reference');

        validate(reference);
        reqParam.Identifier = reference;

        Log.info('ContactPasswordResetController', 'reset', 'Identifier value {0} \n Payload: {1}', new List<String>{ reqParam.Identifier, reqParam.Payload });

        ActionResult result = ContactService.resetSSOPassword(reqParam);

        if (result.Status == ResultStatus.SUCCESS)
        {
            RestContext.response.addHeader('Content-Type', 'application/json');
            RestContext.response.statusCode = 200;
        }
        else
        {
            Log.error('ContactPasswordResetController', 'reset', result.message);
            throw new Exceptions.ApplicationException(result.message);
        }
    }

    private static void validate(String reference)
    {
        if (String.isEmpty(reference))
        {
            Log.error('ContactPasswordResetController', 'validate', Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Reference' });
            throw new Exceptions.InvalidParameterException(String.format(Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Reference' }));
        }
        else if (reference.contains('@'))
        {
            Log.error('ContactPasswordResetController', 'validate', 'Unexpected reference value format');
            throw new Exceptions.InvalidParameterException('Unexpected reference value format');
        }
    }
}