public class ProfileUtilities 
{
    public static String getProfileName(Id userId)
    {
        return [SELECT Profile.Name FROM User WHERE Id = :userId].Profile.Name;
    }    
    
    public static String getCurrentUserProfileName()
    {
        return [SELECT Name FROM Profile WHERE Id = :UserInfo.getProfileId()].Name;
    }
}