@IsTest
public class ContactValidateDeletionCommandTest
{
    @testSetup
    public static void setup()
    {
        TestUtilities.setupHEDA();
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        TestDataContactMock.createStudent(user);
        TestDataContactMock.createStudent(user);
    }

    @isTest
    static void SystemAdminMergesTwoContactStudents_SuccessfullMerge()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        List<Contact> contacts = [SELECT Id FROM Contact Limit 2];
        Contact masterContact = contacts[0];
        Contact duplicateContact = contacts[1];

        // Act
        Database.merge(masterContact, duplicateContact);
        List<Contact> mergedContacts = [SELECT Id, IsDeleted FROM Contact WHERE Id = :duplicateContact.Id];

        // Assert
        System.assertEquals(0, mergedContacts.size(), 'Merged contact should be deleted');
    }

    @isTest
    static void NonAdminMergesTwoContactStudents_ExceptionThrown()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        List<Contact> contacts = [SELECT Id FROM Contact Limit 2];
        Contact masterContact = contacts[0];
        Contact duplicateContact = contacts[1];
        User mergeUser = UserService.getByAlias(Constants.USER_NON_ADMIN);

        // Act
        try
        {
            System.RunAs(mergeUser)
            {
                Database.merge(masterContact, duplicateContact);
            }
        }
        catch(Exception e)
        {
            // Assert
            System.assert(e.getMessage().contains('User not allowed to performed a record merge as this would delete Student contact type'));
        }
    }
}