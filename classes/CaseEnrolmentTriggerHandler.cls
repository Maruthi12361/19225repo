public class CaseEnrolmentTriggerHandler extends BaseTriggerHandler
{
    public override void afterInsert()
    {
        ActionResult result = CaseEnrolmentService.updateParent(Trigger.new, Trigger.oldMap, Enums.TriggerContext.AFTER_INSERT);
        
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);
    }
    
    public override void afterUpdate()
    {
        ActionResult result = CaseEnrolmentService.updateParent(Trigger.new, Trigger.oldMap, Enums.TriggerContext.AFTER_UPDATE);
        
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);
    }
    
    public override void afterDelete()
    {
        ActionResult result = CaseEnrolmentService.validateDeletion(Trigger.old, Trigger.oldMap, Enums.TriggerContext.AFTER_DELETE);
        
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);
        
        result = CaseEnrolmentService.deleteChild(Trigger.old, Trigger.oldMap, Enums.TriggerContext.AFTER_DELETE);
        
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);
        
        result = CaseEnrolmentService.updateParent(Trigger.old, Trigger.oldMap, Enums.TriggerContext.AFTER_DELETE);

        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);
    }
}