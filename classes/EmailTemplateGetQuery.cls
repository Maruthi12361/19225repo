public class EmailTemplateGetQuery
{
    private Boolean validate(String templateName)
    {
        if (String.isEmpty(templateName))
        {
            Log.error('EmailTemplateGetQuery', 'validate', Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Template Name' });
            throw new Exceptions.InvalidParameterException(String.format(Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Template Name' }));
        }

        return true;
    }

    public EmailTemplate query(String templateName)
    {
        validate(templateName);

        List<EmailTemplate> templates =
        [
            SELECT Id, IsActive, Name, Body, HtmlValue, Subject
            FROM EmailTemplate
            WHERE IsActive = true AND Name = :templateName
        ];

        if (templates.size() == 0)
            return null;

        if (templates.size() > 1)
        {
            String message = String.format('More then one email template found for {0}', new List<String> { templateName });
            Log.error('EmailTemplateGetQuery', 'query', message);
            throw new Exceptions.ApplicationException(message);
        }

        return templates[0];
    }
}