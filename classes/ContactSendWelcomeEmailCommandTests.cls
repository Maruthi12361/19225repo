@isTest
public class ContactSendWelcomeEmailCommandTests
{
    @testSetup
    static void setup()
    {
        TestUtilities.setupHEDA();
    }

    @isTest
    static void CreateContact_WelcomeEmailSent()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);

        // Act
        Test.startTest();
        Contact contact = TestDataContactMock.createProspect(user, 'Domestic', 'Prospect-MBA', '', '+61455789369', 'domestic.mba@gmail.com', 'Cold', 'A0000000000','','', false, true);
        Test.stopTest();

        // Assert
        Contact result = [SELECT Id, WelcomeProspectEmailSent__c FROM Contact WHERE Email = 'domestic.mba@gmail.com'];
        System.assertEquals(true, result.WelcomeProspectEmailSent__c);
    }

    @isTest
    static void CreateContactWhenNetworkServiceDown_WelcomeEmailSentFailed()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        TestUtilities.throwOnException = false;

        // Act
        Test.startTest();
        Contact contact = TestDataContactMock.createProspect(user, 'soft-4237', 'test', '', '+61455789369', 'x0000000@gmail.com', 'Cold', 'X0000000000','','', false, true);
        Test.stopTest();

        // Assert
        Contact result = [SELECT Id, WelcomeProspectEmailSent__c FROM Contact WHERE Email = 'x0000000@gmail.com'];
        System.assertEquals(false, result.WelcomeProspectEmailSent__c);
    }

    @isTest
    static void CreateContactWithoutFirstName_NoWelcomeEmailSent()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);

        // Act
        Test.startTest();
        Contact contact = TestDataContactMock.createProspect(user, '', 'No-FirstName', '', '+61455789369', 'no.first.name@gmail.com', 'Cold', 'A00' + System.now().format('HHmmssSS'),'','', false, true);
        Test.stopTest();

        // Assert
        Contact result = [SELECT Id, WelcomeProspectEmailSent__c FROM Contact WHERE Email = 'no.first.name@gmail.com'];
        System.assertEquals(false, result.WelcomeProspectEmailSent__c);
    }
}