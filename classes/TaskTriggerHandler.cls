public with sharing class TaskTriggerHandler extends BaseTriggerHandler 
{
    public override void beforeInsert()
    {
        ActionResult result;

        result = TaskService.updateNVMSubTypeToCall(Trigger.new, Trigger.oldMap, Enums.TriggerContext.BEFORE_INSERT);
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);
    }

    public override void afterInsert()
    {
        ActionResult result;

        result = TaskService.syncCallCountToContact(Trigger.new, Trigger.oldMap, Enums.TriggerContext.AFTER_INSERT);
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);

        result = TaskService.updateCaseOppDocStatus(Trigger.new, Trigger.oldMap, Enums.TriggerContext.AFTER_INSERT);
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);

        result = SObjectService.sendConfirmationEmail(Trigger.new, Trigger.oldMap, Enums.TriggerContext.AFTER_INSERT);
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);
        
        result = ActivityService.sendNotification(Trigger.new, Trigger.oldMap, Enums.TriggerContext.AFTER_INSERT);
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);

        result = TaskService.updateContactCall(Trigger.new, Trigger.oldMap, Enums.TriggerContext.AFTER_INSERT);
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);
    }

    public override void beforeUpdate()
    {

    }

    public override void afterUpdate()
    {
        ActionResult result;

        result = TaskService.syncCallCountToContact(Trigger.new, Trigger.oldMap, Enums.TriggerContext.AFTER_UPDATE);
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);
        
        result = ActivityService.sendNotification(Trigger.new, Trigger.oldMap, Enums.TriggerContext.AFTER_UPDATE);
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);

        result = TaskService.updateContactCall(Trigger.new, Trigger.oldMap, Enums.TriggerContext.AFTER_UPDATE);
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);
    }
}