public class StatusTrackingHistoryCalcAgeBatch extends BaseBatchCommand
{
    public override Database.QueryLocator query(Database.BatchableContext BC)
    {
        return Database.getQueryLocator(getQuery());
    }

    public override void command(Database.BatchableContext BC, List<sObject> scope) 
    {
        ActionResult result = StatusTrackingHistoryService.calculateStageAgeInBusinessMinutes(scope);

        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);
    }

    private String getQuery()
    {
        return 'SELECT Id, BusinessMinutesInStage__c, CaseId__c, EndDateTime__c, StartDateTime__c ' +
               'FROM Status_Tracking_History__c ' +
               'WHERE CaseId__c <> null AND (EndDateTime__c = null OR BusinessMinutesInStage__c = null OR EndDateTime__c >= YESTERDAY)';
    }
}