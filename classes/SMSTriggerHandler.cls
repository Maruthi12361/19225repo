public class SMSTriggerHandler extends BaseTriggerHandler
{
    public override void afterInsert()
    {
        ActionResult result = ActivityService.create(Trigger.new, Trigger.oldMap, Enums.TriggerContext.AFTER_INSERT);
        
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);
    }
    
    public override void afterUpdate()
    {
        ActionResult result = ActivityService.create(Trigger.new, Trigger.oldMap, Enums.TriggerContext.AFTER_UPDATE);
        
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);
    }
}