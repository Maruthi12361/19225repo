@isTest
public class DataActionBatchTests
{
    @testSetup
    static void setup()
    {
        TestUtilities.setupHEDA();
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createProspect(user, 'Anna', 'Smith', '', '0455566755', 'anna.smith@gmail.com', Constants.STATUS_COLD, 'A001588497', 'Australia', 'Website (Organic)');
    }

    @isTest
    static void ObjectNameParameterEmpty_ObjectNameParameterError()
    {
        // Arrange
        String errorMessage = '';

        // Act
        try
        {
            Database.executeBatch(new DataActionBatch('', null));
        }
        catch (Exception ex)
        {
           errorMessage = ex.getMessage();
        }

        // Assert
        System.assertEquals(String.format(Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Object Name' }), errorMessage, 'Unexpected error message');
    }

    @isTest
    static void ActionParameterEmpty_ActionParameterError()
    {
        // Arrange
        String errorMessage = '';

        // Act
        try
        {
            Database.executeBatch(new DataActionBatch('Contact', null));
        }
        catch (Exception ex)
        {
           errorMessage = ex.getMessage();
        }

        // Assert
        System.assert(errorMessage.contains('Action parameter is null or empty list'), 'Unexpected error message');
    }

    @isTest
    static void ContactMoveFirstNameToLastName_ContactLastNameUpdated()
    {
        // Arrange
        List<ViewModels.DataAction> actions = new List<ViewModels.DataAction>
        {
            new ViewModels.DataAction(Enums.DataUpdateAction.MOVE, 'FirstName', 'LastName')
        };

        // Act
        test.startTest();
        Database.executeBatch(new DataActionBatch('Contact', actions));
        test.stopTest();
        Contact updatedContact = [SELECT Id, FirstName, LastName FROM Contact LIMIT 1];

        // Assert
        System.assertEquals(updatedContact.FirstName, updatedContact.LastName, 'Unexpected last name value');
    }

    @isTest
    static void ContactUpdatePortfolio_InternationalCanada()
    {
        // Arrange
        List<ViewModels.DataAction> actions = new List<ViewModels.DataAction>
        {
            new ViewModels.DataAction(Enums.DataUpdateAction.WRITE, 'Portfolio__c','International (Canada)')
        };
        Contact contact = [SELECT Id, ProgramPortfolioBackup__c, MailingCountry FROM Contact WHERE Email ='anna.smith@gmail.com' LIMIT 1];
        contact.ProgramPortfolioBackup__c = 'IMBA';
        contact.MailingCountry = 'Canada';
        update contact;
        String whereClause = '(ProgramPortfolioBackup__c like \'IMBA\' or ProgramPortfolioBackup__c like \'IGCM\') AND MailingCountry = \'Canada\' ';

        // Act
        test.startTest();
        Database.executeBatch(new DataActionBatch('Contact', actions, whereClause));
        test.stopTest();
        Contact updatedContact = [SELECT Id, Portfolio__c FROM Contact LIMIT 1 ];

        // Assert
        System.assertEquals(updatedContact.Portfolio__c, 'International (Canada)', 'Unexpected Portfolio value');
    }

    @isTest
    static void UpdatingVariousFieldTypes_FieldValuesSuccessfullyUpdated()
    {
        // Arrange
        String primaryMotivation = 'Change career';
        String department = 'Software';
        Contact contact = [SELECT Id FROM Contact LIMIT 1];
        String whereClause = String.format('ID=\'\'{0}\'\'', new List<String> { contact.Id });
        List<ViewModels.DataAction> actions = new List<ViewModels.DataAction>
        {
            new ViewModels.DataAction(Enums.DataUpdateAction.WRITE, 'PrimaryMotivations__c', primaryMotivation),
            new ViewModels.DataAction(Enums.DataUpdateAction.WRITE, 'SingleSignOnStatusLastUpdated__c', '2019-09-25T00:02:00Z'),
            new ViewModels.DataAction(Enums.DataUpdateAction.WRITE, 'Required_Subjects__c', '2'),
            new ViewModels.DataAction(Enums.DataUpdateAction.WRITE, 'Impairments__c', 'Hearing,Learning'),
            new ViewModels.DataAction(Enums.DataUpdateAction.WRITE, 'Current_Position__c', ''),
            new ViewModels.DataAction(Enums.DataUpdateAction.WRITE, 'Department', department),
            new ViewModels.DataAction(Enums.DataUpdateAction.WRITE, 'Express_Consent_Given__c', '2019-09-25'),
            new ViewModels.DataAction(Enums.DataUpdateAction.WRITE, 'ANZPermanentResidency__c', 'TRUE')
        };

        // Act
        test.startTest();
        Database.executeBatch(new DataActionBatch('Contact', actions, whereClause));
        test.stopTest();

        Contact updatedContact =
        [
            SELECT Id, PrimaryMotivations__c, SingleSignOnStatusLastUpdated__c, Required_Subjects__c, Impairments__c, Current_Position__c, Department,
                Express_Consent_Given__c, ANZPermanentResidency__c
            FROM Contact
            WHERE ID = :contact.Id
        ];

        // Assert
        System.assertEquals(primaryMotivation, updatedContact.PrimaryMotivations__c, 'Unexpected Primary Motivations value');
        System.assertEquals(Datetime.valueOfGMT('2019-09-25 00:02:00'), updatedContact.SingleSignOnStatusLastUpdated__c, 'Unexpected First Call Timer Last Modifed value');
        System.assertEquals(2, updatedContact.Required_Subjects__c, 'Unexpected required subjects value');
        System.assertEquals('Hearing;Learning', updatedContact.Impairments__c, 'Unexpected impairments value');
        System.assertEquals(null, updatedContact.Current_Position__c, 'Unexpected current position value');
        System.assertEquals(department, updatedContact.Department, 'Unexpected department value');
        System.assertEquals(Date.valueOf('2019-09-25'), updatedContact.Express_Consent_Given__c, 'Unexpected Express Consent Given date value');
        System.assertEquals(true, updatedContact.ANZPermanentResidency__c, 'Unexpected ANZ permanent residency value');
    }

    @isTest
    static void TransformVariousDateFieldTypes_FieldValuesSuccessfullyTransformed()
    {
        // Arrange
        Contact contact = [SELECT Id, ActualCreateDate__c, HasOptedOutOfEmailDate__c, LastCLOODate__c, PreferredStartDateTime__c  FROM Contact LIMIT 1];
        String whereClause = String.format('ID=\'\'{0}\'\'', new List<String> { contact.Id });

        List<ViewModels.DataAction> actions = new List<ViewModels.DataAction>
        {
            new ViewModels.DataAction(Enums.DataUpdateAction.TRANSFORM, 'ActualCreateDate__c', 'ActualCreateDate__c', '365', 'ADDDAYS'),
            new ViewModels.DataAction(Enums.DataUpdateAction.TRANSFORM, 'LastCLOODate__c', 'LastCourseCompletionDate__c', '120', 'ADDDAYS'),
            new ViewModels.DataAction(Enums.DataUpdateAction.TRANSFORM, 'HasOptedOutOfEmailDate__c', 'LastCallDateTime__c', '5', 'ADDHOURS'),
            new ViewModels.DataAction(Enums.DataUpdateAction.TRANSFORM, 'Preferred_Start_Date__c', 'PreferredStartDateTime__c', '5', 'ADDHOURS')
        };

        contact.HasOptedOutOfEmailDate__c = Datetime.now();
        contact.LastCLOODate__c = Date.today();
        update contact;

        // Act
        test.startTest();
        Database.executeBatch(new DataActionBatch('Contact', actions, whereClause));
        test.stopTest();

        Contact updatedContact =
        [
            SELECT Id, ActualCreateDate__c, CreatedDate, HasOptedOutOfEmailDate__c, LastCallDateTime__c, LastCLOODate__c, LastCourseCompletionDate__c,
                PreferredStartDateTime__c
            FROM Contact
            WHERE ID = :contact.Id
        ];

        // Assert
        System.assertEquals(contact.ActualCreateDate__c.addDays(365), updatedContact.ActualCreateDate__c, 'Unexpected actual created date value value');
        System.assertEquals(updatedContact.LastCLOODate__c.addDays(120), updatedContact.LastCourseCompletionDate__c, 'Unexpected Last Course Completion Date value');
        System.assertEquals(updatedContact.HasOptedOutOfEmailDate__c.addHours(5), updatedContact.LastCallDateTime__c, 'Unexpected Call Date Time value');
        System.assertEquals(contact.PreferredStartDateTime__c, updatedContact.PreferredStartDateTime__c, 'Unexpected PreferredStartDateTime value. It should not be changed');
    }
}