public class ContactReferrerSetLeadSourceBatch extends BaseBatchCommand
{
    public override Database.QueryLocator query(Database.BatchableContext context)
    {
        return Database.getQueryLocator(getQuery()); 
    }

    public override void command(Database.BatchableContext context, List<sObject> scope)
    {
        ContactReferrerService.setLeadSourceValue(scope, null, Enums.TriggerContext.AFTER_INSERT);
    }

    public override void finalise(Database.BatchableContext context)
    {
        Database.executeBatch(new ContactSetLeadSourceBatch(), 50);
    }

    private string getQuery()
    {
        return 
            'SELECT ' + 
                'Id, LeadSource__c ' + 
            'FROM ContactReferrer__c ' + 
            'WHERE LeadSource__c = null';
    }
}