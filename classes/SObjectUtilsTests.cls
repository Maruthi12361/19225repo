@isTest
private class SObjectUtilsTests
{
    @testSetup
    static void setup()
    {
        TestUtilities.setupHEDA();
    }

    @isTest
    private static void DifferentObjectPropertyValues_CorrectlyDetectedAsChanged()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Case oldCase = TestDataCaseMock.createApplication(user);
        Case newCase = oldCase.clone(true, false, true, true);

        // Act
        newCase.Subject = 'Field Changed';
        Boolean isDifferent = SObjectUtils.hasPropertyChanged(oldCase, newCase, 'Subject');

        // Assert
        System.assertEquals(true, isDifferent, 'Field change should correctly be detected when comparing objects');
    }

    @isTest
    private static void NoDifferentFieldValue_CorrectlyDetectedAsSame()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Case oldCase = TestDataCaseMock.createApplication(user);
        Case newCase = oldCase.clone(true, false, true, true);

        // Act
        Boolean isDifferent = SObjectUtils.hasPropertyChanged(oldCase, newCase, 'Subject');

        // Assert
        System.assertEquals(false, isDifferent, 'No field change should correctly be detected when comparing objects');
    }

    @isTest
    private static void ObjectPropertyValueChangingToNull_CorrectlyDetectedAsChanged()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Case oldCase = TestDataCaseMock.createApplication(user);
        Case newCase = oldCase.clone(true, false, true, true);

        // Act
        newCase.Subject = null;
        Boolean isDifferent = SObjectUtils.hasPropertyChanged(oldCase, newCase, 'Subject');

        // Assert
        System.assertEquals(true, isDifferent, 'Field change should correctly be detected when comparing objects');
    }

    @isTest
    private static void ObjectPropertyValueChangingFromNull_CorrectlyDetectedAsChanged()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Case oldCase = TestDataCaseMock.createApplication(user);
        Case newCase = oldCase.clone(true, false, true, true);

        // Act
        oldCase.Subject = null;
        Boolean isDifferent = SObjectUtils.hasPropertyChanged(oldCase, newCase, 'Subject');

        // Assert
        System.assertEquals(true, isDifferent, 'Field change should correctly be detected when comparing objects');
    }

    @isTest
    private static void ObjectBeingCreated_CorrectlyDetectedAsChanged()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Case oldCase;
        Case newCase = TestDataCaseMock.createApplication(user);

        // Act
        Boolean isDifferent = SObjectUtils.hasPropertyChanged(oldCase, newCase, 'Subject');

        // Assert
        System.assertEquals(true, isDifferent, 'Field change should correctly be detected when comparing objects');
    }

    @isTest
    private static void ObjectBeingNulled_CorrectlyDetectedAsAsChanged()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Case oldCase = TestDataCaseMock.createApplication(user);
        Case newCase;

        // Act
        Boolean isDifferent = SObjectUtils.hasPropertyChanged(oldCase, newCase, 'Subject');

        // Assert
        System.assertEquals(true, isDifferent, 'Field change should correctly be detected when comparing objects');
    }

    @isTest
    private static void AnyOneDifferentFieldValue_CorrectlyDetectedAsAsChanged()
    {
       // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Case oldCase = TestDataCaseMock.createApplication(user);
        Case newCase = oldCase.clone(true, false, true, true);

        // Act
        newCase.Subject = 'Field Changed';
        oldCase.Subject = 'Field changed';
        Boolean isDifferent = SObjectUtils.hasAnyPropertyChanged(oldCase, newCase, new List<String> {'Subject', 'IsEscalated'});

        // Assert
        System.assertEquals(true, isDifferent, 'Field change should correctly be detected when comparing objects in case sensitive way');
    }

    @isTest
    private static void NoDifferentFieldValues_CorrectlyDetectedAsAsSame()
    {
       // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Case oldCase = TestDataCaseMock.createApplication(user);
        Case newCase = oldCase.clone(true, false, true, true);

        // Act
        Boolean isDifferent = SObjectUtils.hasAnyPropertyChanged(oldCase, newCase, new List<String> {'Subject', 'IsEscalated'});

        // Assert
        System.assertEquals(false, isDifferent, 'No field change should correctly be detected as not changed when comparing objects');
    }

    @isTest
    private static void AnyOneDifferentFieldValueIgnoreCase_CorrectlyIgnoreCaseChange()
    {
       // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Case oldCase = TestDataCaseMock.createApplication(user);
        Case newCase = oldCase.clone(true, false, true, true);

        // Act
        newCase.Subject = 'Field Changed';
        oldCase.Subject = 'Field changed';
        Boolean isDifferent = SObjectUtils.hasAnyPropertyChangedIgnoreCase(oldCase, newCase, new List<String> {'Subject', 'IsEscalated'});

        // Assert
        System.assertEquals(false, isDifferent, 'String field change only on case should be ignored when comparing objects in case insensitive way');
    }

    @isTest
    private static void AnyNonStringDifferentFieldValueIgnoreCase_CorrectlyDetectedAsAsChanged()
    {
       // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Case oldCase = TestDataCaseMock.createApplication(user);
        Case newCase = oldCase.clone(true, false, true, true);

        // Act
        newCase.Subject = 'Field Changed';
        newCase.IsEscalated = false;
        oldCase.Subject = 'Field changed';
        oldCase.IsEscalated = true;
        Boolean isDifferent = SObjectUtils.hasAnyPropertyChangedIgnoreCase(oldCase, newCase, new List<String> {'Subject', 'IsEscalated'});

        // Assert
        System.assertEquals(true, isDifferent, 'String field change only on case should be ignored when comparing objects in case insensitive way');
    }

    @isTest
    private static void AnyOneDifferentFieldValueIgnoreLineBreak_CorrectlyIgnoreObjectChange()
    {
       // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createProspectWarm(user);
        Contact newContact = contact.clone(true, false, true, true);

        // Act
        contact.OtherStreet = '27 Currie\nStreet';
        contact.MailingStreet = '37 Waymouth\tStreet';
        newContact.OtherStreet = '27 Currie\r\nStreet';
        newContact.MailingStreet = '37 Waymouth\n\nStreet';
        Boolean isDifferent = SObjectUtils.hasAnyPropertyChangedIgnoreLineBreak(contact, newContact, new List<String> {'OtherStreet', 'MailingStreet'});

        // Assert
        System.assertEquals(false, isDifferent); 
    }

    @isTest
    private static void ThousandFieldValuesIgnoreLineBreak_CPUTimeNotExceedsOneSecond()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createProspectWarm(user);
        Contact newContact = contact.clone(true, false, true, true);
 
        // Act
        contact.OtherStreet = '27 Currie\nStreet';
        newContact.OtherStreet = '27 Currie\r\nStreet';
        Boolean isDifferent = true;
        Integer cpuTime = Limits.getCpuTime();

        for (Integer i = 0; i < 1000; i++)
            isDifferent = SObjectUtils.hasAnyPropertyChangedIgnoreLineBreak(contact, newContact, new List<String> {'OtherStreet'});

        cpuTime = Limits.getCpuTime() - cpuTime;

        // Assert
        System.assertEquals(false, isDifferent);
        System.assertEquals(true, cpuTime < 1000);
    }

    @isTest
    private static void ObtainFakeIdMultipleTimes_ReceiveBackMultipleUniqueIds()
    {
       // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        String prefix = Constants.ENTITYID_CASE;

        // Act
        String fakeId1 = SObjectUtils.getFakeId(prefix);
        String fakeId2 = SObjectUtils.getFakeId(prefix);
        String fakeId3 = SObjectUtils.getFakeId(prefix);

        // Assert
        System.assert(fakeId1.startsWith(prefix));
        System.assert(fakeId2.startsWith(prefix));
        System.assert(fakeId3.startsWith(prefix));
        System.assertNotEquals(fakeId1, fakeId2);
        System.assertNotEquals(fakeId1, fakeId3);
        System.assertNotEquals(fakeId2, fakeId3);
        System.assertNotEquals(null, Id.valueOf(fakeId1));
        System.assertNotEquals(null, Id.valueOf(fakeId2));
        System.assertNotEquals(null, Id.valueOf(fakeId3));
        System.assertEquals(Case.sObjectType, Id.valueOf(fakeId1).getSobjectType());
        System.assertEquals(Case.sObjectType, Id.valueOf(fakeId2).getSobjectType());
        System.assertEquals(Case.sObjectType, Id.valueOf(fakeId3).getSobjectType());
    }
}