@isTest
private class BaseCommandTests
{
    @testSetup
    static void setup()
    {
        TestUtilities.setupHEDA();
    }

    @isTest
    static void ExecuteCommandWithNullParameters_RaisesExeceptionToCaller()
    {
        // Arrange
        List<SObject> nullObj;
        TestCommand command = new TestCommand();

        // Act
        try
        {
            command.execute(nullObj);
        }
        catch(Exception e)
        {
            //Assert
            System.assert(e instanceof InvalidParameterValueException, 'Executing a command with a null parameter should raise an exception for the developer.');
        }
    }

    @isTest
    static void ExcecuteCommandWithEmptySetOfParameters_ReturnsSuccessResultToCaller()
    {
        // Arrange
        List<SObject> items = new List<SObject>();
        TestCommand command = new TestCommand();

        // Act
        ActionResult result = command.execute(items);

        // Arrange
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Executing a command without any items to action against should return a successful result to the caller.');
    }

    @isTest
    static void ExecuteCommandAfterSuccessfulValidationAndExecution_ReturnsSuccessToCaller()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Case testCase = TestDataCaseMock.createApplication(user);
        List<Case> items = new List<SObject> { testCase };
        TestCommand command = new TestCommand();

        // Act
        ActionResult result = command.execute(items);

        //Assert
        system.assertEquals(ResultStatus.SUCCESS, result.Status, 'Executing a command with valid items, validating and executing successfully, should return a successful result to the caller.');
    }

    @isTest
    static void ExecuteCommandWithSingleObjectAfterSuccessfulValidationAndExecution_ReturnsSuccessToCaller()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Case item = TestDataCaseMock.createApplication(user);
        TestCommand command = new TestCommand();

        // Act
        ActionResult result = command.execute(item);

        //Assert
        system.assertEquals(ResultStatus.SUCCESS, result.Status, 'Executing a command with valid items, validating and executing successfully, should return a successful result to the caller.');
    }

    class TestValidationExeceptionCommand extends BaseCommand
    {
        public override List<SObject> validate(List<SObject> params)
        {
            throw new SObjectException('Unexpected exception during validation action');
        }

        public override ActionResult command(List<SObject> params)
        {
            return new ActionResult();
        }
    }

    class TestCommandExceptionCommand extends BaseCommand
    {
        public override List<SObject> validate(List<SObject> params)
        {
            return params;
        }

        public override ActionResult command(List<SObject> params)
        {
            throw new SObjectException('Unexpected exception during command action');
        }
    }

    class TestCommand extends BaseCommand
    {
        public override List<SObject> validate(List<SObject> params)
        {
            return params;
        }

        public override ActionResult command(List<SObject> params)
        {
            return new ActionResult();
        }
    }
}