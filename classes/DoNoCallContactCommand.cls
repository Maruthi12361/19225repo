public class DoNoCallContactCommand extends BaseTriggerCommand
{
    public override List<SObject> validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        List<Contact> filteredList;
        if (triggerContext == Enums.TriggerContext.BEFORE_UPDATE)
        {
            filteredList = new List<Contact>();
            for(Contact newValue: (List<Contact>)items )
            {
                Boolean isChanged = false;
                Contact oldValue = (Contact)oldMap.get(newValue.id);
                if (SObjectUtils.hasAnyPropertyChanged(oldValue, newValue, new List<String> {'Phone', 'MobilePhone', 'OtherPhone', 'HomePhone', 'hed__WorkPhone__c'}))
                    isChanged = true;

                if (isChanged)
                    filteredList.add(newValue);
            }
        }

        return filteredList;
    }

    public override ActionResult command(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        if (triggerContext == Enums.TriggerContext.BEFORE_UPDATE)
        {
            List<Contact> contacts = (List<Contact>)items;
            for(Contact newContact: contacts)
            {
                Boolean isDirty = false;
                Contact oldContact = (Contact)oldMap.get(newContact.id);
                
                if (!StringUtils.equalIgnoreCase(newContact.Phone, oldContact.Phone))
                {
                    newContact.dncau__Phone_DNC_Checked__c = null;
                    newContact.dncau__Phone_DNC_Status__c = null;
                    isDirty = true;
                }

                if (!StringUtils.equalIgnoreCase(newContact.MobilePhone, oldContact.MobilePhone))
                {
                    newContact.dncau__MobilePhone_DNC_Checked__c = null;
                    newContact.dncau__MobilePhone_DNC_Status__c = null;
                    isDirty = true;
                }

                if (!StringUtils.equalIgnoreCase(newContact.OtherPhone, oldContact.OtherPhone))
                {
                    newContact.dncau__OtherPhone_DNC_Checked__c = null;
                    newContact.dncau__OtherPhone_DNC_Status__c = null;
                    isDirty = true;
                }

                if (!StringUtils.equalIgnoreCase(newContact.HomePhone, oldContact.HomePhone))
                {
                    newContact.dncau__HomePhone_DNC_Checked__c = null;
                    newContact.dncau__HomePhone_DNC_Status__c = null;
                    isDirty = true;
                }

                if (!StringUtils.equalIgnoreCase(newContact.hed__WorkPhone__c, oldContact.hed__WorkPhone__c))
                {
                    newContact.WorkPhone_DNC_Status__c = null;
                    newContact.WorkPhone_DNC_Checked__c = null;
                    isDirty = true;
                }
            }
        }
        return new ActionResult(ResultStatus.SUCCESS); 
    }
}