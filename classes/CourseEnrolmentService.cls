public class CourseEnrolmentService
{
    public static ActionResult mergeContact(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        return new CourseEnrolmentMergeContactCommand().execute(items, oldMap, triggerContext);
    }
}