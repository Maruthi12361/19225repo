@isTest
public class ContentVersionGetFileQueryTests
{
    @isTest
    static void ParamNull_ParamObjectNullError()
    {
        // Arrange
        String errorMessage = '';

        // Act
        try
        {
            ContentDocumentService.getFile(null);
        }
        catch (Exception ex)
        {
            errorMessage = ex.getMessage();
        }

        // Assert
        System.assertEquals(true, errorMessage.contains(String.format(Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Param object' })), 'Unexpected error message');
    }

    @isTest
    static void NoIdentifierProvided_EmptyIdentifierErrorRetunred()
    {
        // Arrange
        String errorMessage = '';

        ViewModels.Param param = new ViewModels.Param();
        param.Identifier = '';

        // Act
        try
        {
            ContentDocumentService.getFile(param);
        }
        catch (Exception ex)
        {
            errorMessage = ex.getMessage();
        }

        // Assert
        System.assertEquals(true, errorMessage.contains(String.format(Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Identifier' })), 'Unexpected error message');
    }

    @isTest
    static void NoPayloadValue_EmptyPayloadErrorRetunred()
    {
        // Arrange
        String errorMessage = '';

        ViewModels.Param param = new ViewModels.Param();
        param.Identifier = '0685O0000001mI8QAI';

        // Act
        try
        {
            ContentDocumentService.getFile(param);
        }
        catch (Exception ex)
        {
            errorMessage = ex.getMessage();
        }

        // Assert
        System.assertEquals(true, errorMessage.contains(String.format(Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Payload' })), 'Unexpected error message');
    }

    @isTest
    static void NoContactEncryptedIdInPayload_EmptyContactEncryptedIdErrorRetunred()
    {
        // Arrange
        String errorMessage = '';

        ViewModels.Param param = new ViewModels.Param();
        param.Identifier = '0685O0000001mI8QAI';
        Map<String, Object> payloadMap = new Map<String, Object>
        {
            'Cnt_Salutation' => 'Mr'
        };
         param.Payload = JSON.serialize(payloadMap);

        // Act
        try
        {
            ContentDocumentService.getFile(param);
        }
        catch (Exception ex)
        {
            errorMessage = ex.getMessage();
        }

        // Assert
        System.assertEquals(true, errorMessage.contains(String.format(Constants.ERRORMSG_NULLPARAM, new List<String>{ 'ContactIdentifier' })), 'Unexpected error message');
    }

    @isTest
    static void EmailContactIdentifier_UnauthorizedErrorRetunred()
    {
        // Arrange
        String errorMessage = '';

        ViewModels.Param param = new ViewModels.Param();
        param.Identifier = '0685O0000001mI8QAI';
        Map<String, Object> payloadMap = new Map<String, Object>
        {
            'ContactIdentifier' => 'some.email@email.com'
        };
         param.Payload = JSON.serialize(payloadMap);

        // Act
        try
        {
            ContentDocumentService.getFile(param);
        }
        catch (Exception ex)
        {
            System.debug(ex.getMessage());
            errorMessage = ex.getMessage();
        }

        // Assert
        System.assertEquals(true, errorMessage.contains(ContentVersionGetFileQuery.ERROR_UNAUTHORIZED), 'Unexpected error message');
    }

    @isTest
    static void NoContentVersion_NoContentVersionErrorRetunred()
    {
        // Arrange
        String errorMessage = '';
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact =TestDataContactMock.createProspectCold(user, '0455548889');

        ViewModels.Param param = new ViewModels.Param();
        param.Identifier = '0685O0000001mI8QAI';
        Map<String, Object> payloadMap = new Map<String, Object>
        {
            'ContactIdentifier' => EncryptionUtil.Encrypt(contact.Id)
        };
        param.Payload = JSON.serialize(payloadMap);

        // Act
        try
        {
            ContentDocumentService.getFile(param);
        }
        catch (Exception ex)
        {
            System.debug(ex.getMessage());
            errorMessage = ex.getMessage();
        }

        // Assert
        System.assertEquals(true, errorMessage.contains(ContentVersionGetFileQuery.ERROR_NOFILE), 'Unexpected error message');
    }

    @isTest
    static void GetProofOfIdContentVersion_UnauthorizedErrorRetunred()
    {
        // Arrange
        String errorMessage = '';
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createProspectCold(user, '0455548889');
        TestDataOpportunityMock.createApplyingAcquisition(user, contact);

        ViewModelsFile.Upload uploadParam = new ViewModelsFile.Upload();
        uploadParam.Reference = EncryptionUtil.Encrypt(contact.Id);
        uploadParam.FileName = 'Test.docx';
        uploadParam.Type = 'Proof of ID';
        uploadParam.Content = Blob.valueOf('Encrypted File content');

        ContactService.uploadAttachment(uploadParam);
        List<ContentVersion> contentVersions = [SELECT Id FROM ContentVersion];

        ViewModels.Param param = new ViewModels.Param();
        param.Identifier = contentVersions[0].Id;
        Map<String, Object> payloadMap = new Map<String, Object>
        {
            'ContactIdentifier' => EncryptionUtil.Encrypt(contact.Id)
        };
        param.Payload = JSON.serialize(payloadMap);

        // Act
        try
        {
            ContentDocumentService.getFile(param);
        }
        catch (Exception ex)
        {
            System.debug(ex.getMessage());
            errorMessage = ex.getMessage();
        }

        // Assert
        System.assertEquals(true, errorMessage.contains(ContentVersionGetFileQuery.ERROR_UNAUTHORIZED), 'Unexpected error message');
    }

    @isTest
    static void GetWorkExperienceContentVersion_UnauthorizedErrorRetunred()
    {
        // Arrange
        String errorMessage = '';
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createProspectCold(user, '0455548889');
        TestDataOpportunityMock.createApplyingAcquisition(user, contact);

        ViewModelsFile.Upload uploadParam = new ViewModelsFile.Upload();
        uploadParam.Reference = EncryptionUtil.Encrypt(contact.Id);
        uploadParam.FileName = 'Test.docx';
        uploadParam.Type = 'Work Experience';
        uploadParam.Content = Blob.valueOf('Encrypted File content');

        ContactService.uploadAttachment(uploadParam);
        List<ContentVersion> contentVersions = [SELECT Id FROM ContentVersion];

        ViewModels.Param param = new ViewModels.Param();
        param.Identifier = contentVersions[0].Id;
        Map<String, Object> payloadMap = new Map<String, Object>
        {
            'ContactIdentifier' => EncryptionUtil.Encrypt(contact.Id)
        };
        param.Payload = JSON.serialize(payloadMap);

        // Act
        try
        {
            ContentDocumentService.getFile(param);
        }
        catch (Exception ex)
        {
            System.debug(ex.getMessage());
            errorMessage = ex.getMessage();
        }

        // Assert
        System.assertEquals(true, errorMessage.contains(ContentVersionGetFileQuery.ERROR_UNAUTHORIZED), 'Unexpected error message');
    }

    @isTest
    static void GetTrascriptContentVersion_SuccessfullyReturned()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createProspectCold(user, '0455548889');
        TestDataOpportunityMock.createApplyingAcquisition(user, contact);

        ViewModelsFile.Upload uploadParam = new ViewModelsFile.Upload();
        uploadParam.Reference = EncryptionUtil.Encrypt(contact.Id);
        uploadParam.FileName = 'Transcript.docx';
        uploadParam.Type = 'Trascript';
        uploadParam.Content = Blob.valueOf('Encrypted File content');

        ContactService.uploadAttachment(uploadParam);
        List<ContentVersion> contentVersions = [SELECT Id FROM ContentVersion];

        ViewModels.Param param = new ViewModels.Param();
        param.Identifier = contentVersions[0].Id;
        Map<String, Object> payloadMap = new Map<String, Object>
        {
            'ContactIdentifier' => EncryptionUtil.Encrypt(contact.Id)
        };
        param.Payload = JSON.serialize(payloadMap);

        // Act
         ViewModelsFile.ContentDetails fileDetails = ContentDocumentService.getFile(param);

        // Assert
        System.assertNotEquals(null, fileDetails, 'Unexpected file details value');
        System.assertEquals('Transcript.docx', fileDetails.Name, 'Unexpected file name');
        System.assertNotEquals(null, fileDetails.Content, 'Unexpected cotnent value');
    }

    @isTest
    static void GetTrascriptContentVersionFromOtherContact_UnauthorizedErrorRetunred()
    {
        // Arrange
        String errorMessage = '';
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createProspectCold(user, '0455548889');
        TestDataOpportunityMock.createApplyingAcquisition(user, contact);
        Contact otherContact = TestDataContactMock.createProspectCold(user, 'Other', 'Prospect', '0456598789', 'other.prospect@email.com', 'A00189878646');

        ViewModelsFile.Upload uploadParam = new ViewModelsFile.Upload();
        uploadParam.Reference = EncryptionUtil.Encrypt(contact.Id);
        uploadParam.FileName = 'Transcript.docx';
        uploadParam.Type = 'Trascript';
        uploadParam.Content = Blob.valueOf('Encrypted File content');

        ContactService.uploadAttachment(uploadParam);
        List<ContentVersion> contentVersions = [SELECT Id FROM ContentVersion];

        ViewModels.Param param = new ViewModels.Param();
        param.Identifier = contentVersions[0].Id;
        Map<String, Object> payloadMap = new Map<String, Object>
        {
            'ContactIdentifier' => EncryptionUtil.Encrypt(otherContact.Id)
        };
        param.Payload = JSON.serialize(payloadMap);

        // Act
        try
        {
            ViewModelsFile.ContentDetails fileDetails = ContentDocumentService.getFile(param);
        }
        catch (Exception ex)
        {
            errorMessage = ex.getMessage();
        }

        // Assert
        System.assertEquals(true, errorMessage.contains(ContentVersionGetFileQuery.ERROR_UNAUTHORIZED), 'Unexpected error message');
    }
}