public class CaseEnrolmentGetSubjectsQuery
{
    // For portal users, return all the normal Subjects and provisional Enrolling Subjects related to contacts.
    public List<ViewModelsCaseEnrolment.EnrolmentDetails> queryByPortalUser(Id userId)
    {
        if (userId == null)
            return null;

        User record = [SELECT Id, Name, ContactId FROM User WHERE Id = : userId];
        List<hed__Course_Enrollment__c> enrolments =
            [
                SELECT Id, Name, SubjectCode__c, SubjectName__c, Type__c, hed__Course_Offering__r.Name, hed__Status__c, Grade__c, SubjectOfferingAdminDate__c,
                    SubjectOfferingStartDate__c, SubjectOfferingTerm__c, hed__Contact__r.Name, hed__Program_Enrollment__r.hed__Program_Plan__r.Name
                FROM hed__Course_Enrollment__c
                WHERE hed__Contact__c = : record.ContactId AND
                    (RecordType.DeveloperName = : Constants.RECORDTYPE_COURSEENROLLMENT_STUDENT OR
                    (RecordType.DeveloperName = : Constants.RECORDTYPE_COURSEENROLLMENT_PROVISIONAL AND hed__Status__c = : Constants.SUBJECTENROLMENT_STATUS_ENROLLING))
                ORDER BY SubjectOfferingAdminDate__c DESC
            ];
        List<ViewModelsCaseEnrolment.EnrolmentDetails> result = new List<ViewModelsCaseEnrolment.EnrolmentDetails>();

        for (hed__Course_Enrollment__c enrolment : enrolments)
        {
            result.add(ViewModelsCaseEnrolment.convertEnrolment(enrolment, true));
        }

        return result;
    }

    public ViewModelsCaseEnrolment.Payload queryByCase(Id recordId)
    {
        ViewModelsCaseEnrolment.Payload payload;

        if (recordId == null)
            return null;

        Case record = [SELECT Id, IsClosed, RecordType.DeveloperName, ContactId, WorkType__c, WorkSubType__c FROM Case WHERE Id = : recordId];

        if (record.IsClosed || record.RecordType.DeveloperName == Constants.RECORDTYPE_CASE_APPLICATION || record.RecordType.DeveloperName == Constants.RECORDTYPE_CASE_ROLLOVER)
        {
            payload = new ViewModelsCaseEnrolment.Payload(record.RecordType.DeveloperName, null, null, 'This interface is disabled for closed or Application and Rollover types of cases.');
        }
        else if (record.RecordType.DeveloperName == Constants.RECORDTYPE_CASE_EXEMPTIONS)
        {
            // Query the active Course Enrolments, which will be used to determine the Course Plan
            List<hed__Program_Enrollment__c> courseEnrolments =
                [
                    SELECT Id, hed__Program_Plan__c
                    FROM hed__Program_Enrollment__c
                    WHERE hed__Contact__c = : record.ContactId AND Status__c IN ('Approved', 'Ongoing')
                    ORDER BY Status__c DESC
                    LIMIT 1
                ];

            // Query any existing CaseEnrolments linked with this case
            List<CaseEnrolment__c> caseEnrolments = [SELECT SubjectEnrolment__c FROM CaseEnrolment__c WHERE Case__c = : recordId];
            Set<Id> existingEnrolmentIds = new Set<Id>();

            for (CaseEnrolment__c item : caseEnrolments)
            {
                existingEnrolmentIds.add(item.SubjectEnrolment__c);
            }

            // Query any existing Subject Enrolments for the student, whose Subjects will be excluded from Exemption. We should ignore the provisional and withdrawn Subject Enrolments.
            // It is possible that a student may withdraw from a Subject when he finds out he is qualified for Exemption, or applies Exemption for Subjects which were provisionally
            // interested/enrolled before.
            List<hed__Course_Enrollment__c> existEnrolments =
                [
                    SELECT SubjectCode__c
                    FROM hed__Course_Enrollment__c
                    WHERE Id IN : existingEnrolmentIds OR (hed__Contact__c = : record.ContactId  AND
                        RecordType.DeveloperName = : Constants.RECORDTYPE_COURSEENROLLMENT_STUDENT AND
                        hed__Status__c != : Constants.SUBJECTENROLMENT_STATUS_WITHDRAWN)
                ];
            Set<String> excludedSubjects = new Set<String>();

            for (hed__Course_Enrollment__c item : existEnrolments)
            {
                excludedSubjects.add(item.SubjectCode__c);
            }

            // If the Contacts have active Course Enrolments, retrieve the Subject List from the related Course Plans.
            // If there is no active Course Enrolments, then return the Subject Definitions which have future Subject Offerings.
            if (courseEnrolments.size() > 0)
            {
                List<hed__Plan_Requirement__c> planRequirements =
                    [
                        SELECT Id, hed__Plan_Requirement__r.hed__Program_Plan__r.Name, hed__Plan_Requirement__r.Name, hed__Course__r.hed__Course_ID__c , hed__Course__r.Name, hed__Course__r.hed__Credit_Hours__c, hed__Course__r.AQFLevel__c
                        FROM hed__Plan_Requirement__c
                        WHERE hed__Plan_Requirement__r.hed__Program_Plan__c = : courseEnrolments[0].hed__Program_Plan__c AND SubjectCode__c NOT IN : excludedSubjects AND
                        (EffectiveToDate__c = null OR EffectiveToDate__c > TODAY) AND (EffectiveFromDate__c = null OR EffectiveFromDate__c <= TODAY)
                        ORDER BY hed__Plan_Requirement__r.Name, Name
                    ];
                List<ViewModelsCaseEnrolment.PlanRequirementDetails> result = new List<ViewModelsCaseEnrolment.PlanRequirementDetails>();

                for (hed__Plan_Requirement__c requirement : planRequirements)
                {
                    result.add(ViewModelsCaseEnrolment.convertPlanRequirement(requirement, false));
                }

                payload = new ViewModelsCaseEnrolment.Payload(record.RecordType.DeveloperName, 'hed__Plan_Requirement__c', result, null);
            }
            else
            {
                List<AggregateResult> subjects =
                    [
                        SELECT hed__Course__r.SubjectDefinition__c SubjectId
                        FROM hed__Course_Offering__c
                        WHERE hed__End_Date__c >= TODAY
                        GROUP BY hed__Course__r.SubjectDefinition__c
                    ];
                Set<Id> subjectIds = new Set<Id>();

                for(AggregateResult result : subjects)
                {
                    subjectIds.add((Id)result.get('SubjectId'));
                }

                List<hed__Course__c> subjectDetails =
                    [
                        SELECT hed__Course_ID__c, Name, AQFLevel__c, hed__Credit_Hours__c
                        FROM hed__Course__c
                        WHERE Id IN : subjectIds AND hed__Course_ID__c NOT IN : excludedSubjects
                        ORDER BY Name
                    ];

                List<ViewModelsCaseEnrolment.SubjectDetails> result = new List<ViewModelsCaseEnrolment.SubjectDetails>();

                for (hed__Course__c subject : subjectDetails)
                {
                    result.add(ViewModelsCaseEnrolment.convertSubject(subject, false));
                }

                payload = new ViewModelsCaseEnrolment.Payload(record.RecordType.DeveloperName, 'hed__Course__c', result, null);
            }
        }
        else
        {
            // For any other case types, return all the normal Subjects and provisional Enrolling Subjects related to contacts, excluding those already linked with this case.
            List<hed__Course_Enrollment__c> enrolments =
                [
                    SELECT Id, Name, SubjectCode__c, SubjectName__c, Type__c, hed__Course_Offering__r.Name, hed__Status__c, Grade__c, SubjectOfferingAdminDate__c, hed__Contact__r.Name, hed__Program_Enrollment__r.hed__Program_Plan__r.Name
                    FROM hed__Course_Enrollment__c
                    WHERE hed__Contact__c = : record.ContactId AND Id NOT IN (SELECT SubjectEnrolment__c FROM CaseEnrolment__c WHERE Case__c = : recordId)
                        AND (RecordType.DeveloperName = : Constants.RECORDTYPE_COURSEENROLLMENT_STUDENT OR
                        (RecordType.DeveloperName = : Constants.RECORDTYPE_COURSEENROLLMENT_PROVISIONAL AND hed__Status__c = : Constants.SUBJECTENROLMENT_STATUS_ENROLLING))
                    ORDER BY SubjectOfferingAdminDate__c DESC
                ];
            List<ViewModelsCaseEnrolment.EnrolmentDetails> result = new List<ViewModelsCaseEnrolment.EnrolmentDetails>();
            List<String> passGrades = new List<String> {'High Distinction', 'Distinction', 'Credit', 'Pass 1', 'Pass 2'};
            List<String> inProgressStatuses = new List<String> {Constants.SUBJECTENROLMENT_STATUS_ENROLLED, Constants.SUBJECTENROLMENT_STATUS_STUDYING};
            List<String> appealStatuses = new List<String> {Constants.SUBJECTENROLMENT_STATUS_COMPLETED, Constants.SUBJECTENROLMENT_STATUS_WITHDRAWN};

            for (hed__Course_Enrollment__c enrolment : enrolments)
            {
                if (record.RecordType.DeveloperName == Constants.RECORDTYPE_CASE_AWARD && !StringUtils.containsInList(enrolment.Grade__c, passGrades, 0))
                    continue;
                
                if (record.RecordType.DeveloperName == Constants.RECORDTYPE_CASE_STUDENT_SUPPORT)
                {
                    if (record.WorkType__c == 'Withdrawal' && !StringUtils.containsInList(enrolment.hed__Status__c, inProgressStatuses, 0))
                        continue;

                    if (record.WorkType__c == 'Formal Grievance/Appeal' && !StringUtils.containsInList(enrolment.hed__Status__c, appealStatuses, 0))
                        continue;                     
                }

                result.add(ViewModelsCaseEnrolment.convertEnrolment(enrolment, false));
            }

            payload = new ViewModelsCaseEnrolment.Payload(record.RecordType.DeveloperName, 'hed__Course_Enrollment__c', result, null);
        }

        return payload;
    }
}