@isTest
public class CaseTrackStatusHistoryCommandTests
{
    private static String cerebroID = 'A001641336';

    @testSetup
    static void setup()
    {
        TestUtilities.setupHEDA();
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact student = TestDataContactMock.createRolloverStudent(user, cerebroID);
        TestDataCampaignMock.createSeries(user, Date.today(), 2, true);
        TestDataCampaignMock.createRolloverCampaignMemberStatus(user);
        student.Manual_Followup_Date__c = Date.today().addDays(1);
        update student;
        TestDataCaseMock.createRolloverCase(user, student);
    }

    @isTest
    static void TrackStatusOnNewCase_CreatesNewStatusTrackingHistoryRecord()
    {
        // Arrange
        Case rolloverCase =
        [
            SELECT Id, Status, Department__c, StatusDepartment__c, OwnerId, ContactId
            FROM Case
            WHERE Contact.Cerebro_Student_ID__c = : cerebroID
        ];

        // Act
        Test.startTest();
        ActionResult result = CaseService.trackStatusHistory(new List<Case> { rolloverCase }, NULL, Enums.TriggerContext.AFTER_INSERT);
        Test.stopTest();

        List<Status_Tracking_History__c> histories =
        [
            SELECT Id, Contact_ID__c, Status_Stage__c, StartDateTime__c, EndDateTime__c, Type__c, Department__c, Direction__c, Next_Status_Stage__c, User__c, Next_User__c
            FROM Status_Tracking_History__c
            WHERE CaseId__c = : rolloverCase.Id
        ];

        // Assert
        System.assertEquals(1, histories.size());
        System.assertEquals(rolloverCase.StatusDepartment__c, histories[0].Status_Stage__c);
        System.assert(histories[0].Status_Stage__c.contains(Constants.CASE_STATUS_NEW));
        System.assertEquals('State Change', histories[0].Type__c);
        System.assertEquals(rolloverCase.ContactId, histories[0].Contact_ID__c);
        System.assertNotEquals(null, histories[0].StartDateTime__c);
        System.assertEquals(null, histories[0].EndDateTime__c);
        System.assertEquals(getOwnerNameFromId(rolloverCase.OwnerId), histories[0].User__c);
        System.assertEquals(Constants.CASE_DEPT_PROGRESSIONS, histories[0].Department__c);
    }

    @isTest
    static void TrackStatusOnClosedCase_CreatesNewClosedStatusTrackingHistoryRecord()
    {
        // Arrange
        Case rolloverCase =
        [
            SELECT Id, Status, Department__c, StatusDepartment__c, OwnerId, ContactId
            FROM Case
            WHERE Contact.Cerebro_Student_ID__c = : cerebroID
        ];
        rolloverCase.Status = Constants.CASE_STATUS_CLOSED;
        rolloverCase.Department__c = Constants.CASE_DEPT_FINANCE;
        rolloverCase.recalculateFormulas();

        // Act
        Test.startTest();
        ActionResult result = CaseService.trackStatusHistory(new List<Case> { rolloverCase }, NULL, Enums.TriggerContext.AFTER_INSERT);
        Test.stopTest();

        List<Status_Tracking_History__c> histories =
        [
            SELECT Id, Contact_ID__c, Status_Stage__c, StartDateTime__c, EndDateTime__c, Type__c, Department__c, Direction__c, Next_Status_Stage__c, User__c, Next_User__c
            FROM Status_Tracking_History__c
            WHERE CaseId__c = : rolloverCase.Id
        ];

        // Assert
        System.assertEquals(1, histories.size());
        System.assertEquals(rolloverCase.StatusDepartment__c, histories[0].Status_Stage__c);
        System.assert(histories[0].Status_Stage__c.contains(Constants.CASE_STATUS_CLOSED));
        System.assertEquals('State Change', histories[0].Type__c);
        System.assertEquals(rolloverCase.ContactId, histories[0].Contact_ID__c);
        System.assertNotEquals(null, histories[0].StartDateTime__c);
        System.assertNotEquals(null, histories[0].EndDateTime__c);
        System.assertEquals(histories[0].StartDateTime__c, histories[0].EndDateTime__c);
        System.assertEquals(getOwnerNameFromId(rolloverCase.OwnerId), histories[0].User__c);
        System.assertEquals(Constants.CASE_DEPT_FINANCE, histories[0].Department__c);
    }

    @isTest
    static void TrackStatusOnExistingCase_CreatesNewAndUpdatesExistingStatusTrackingHistoryRecord()
    {
        // Arrange
        Case rolloverCase =
        [
            SELECT Id, Status, Department__c, StatusDepartment__c, OwnerId, ContactId
            FROM Case
            WHERE Contact.Cerebro_Student_ID__c = : cerebroID
        ];
        ActionResult result = CaseService.trackStatusHistory(new List<Case> { rolloverCase }, NULL, Enums.TriggerContext.AFTER_INSERT);
        Map<Id, Case> oldMap = new Map<Id, Case>();
        oldMap.put(rolloverCase.Id, rolloverCase);
        Case newCase = rolloverCase.clone(true, false, true, true);
        newCase.Status = Constants.CASE_STATUS_WAITING;
        newCase.Department__c = Constants.CASE_DEPT_FINANCE;
        newCase.recalculateFormulas();

        // Act
        Test.startTest();
        result = CaseService.trackStatusHistory(new List<Case> { newCase }, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        oldMap.put(newCase.Id, newCase);
        rolloverCase = newCase.clone(true, false, true, true);
        rolloverCase.OwnerId = Constants.QUEUEID_FINANCE_TEAM;
        rolloverCase.recalculateFormulas();
        result = CaseService.trackStatusHistory(new List<Case> { rolloverCase }, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        Test.stopTest();

        List<Status_Tracking_History__c> histories =
        [
            SELECT Id, Contact_ID__c, Status_Stage__c, StartDateTime__c, EndDateTime__c, Type__c, Direction__c, Next_Status_Stage__c, User__c, Next_User__c, Department__c
            FROM Status_Tracking_History__c
            WHERE CaseId__c = : rolloverCase.Id
        ];

        // Assert
        System.assertEquals(2, histories.size());
        System.assertEquals(rolloverCase.StatusDepartment__c, histories[0].Next_Status_Stage__c);
        System.assert(histories[0].Status_Stage__c.contains(Constants.CASE_STATUS_NEW));
        System.assertEquals('State Change', histories[0].Type__c);
        System.assertEquals('Forward', histories[0].Direction__c);
        System.assertEquals(getOwnerNameFromId(newCase.OwnerId), histories[0].User__c);
        System.assertEquals(getOwnerNameFromId(rolloverCase.OwnerId), histories[0].Next_User__c);
        System.assertNotEquals(null, histories[0].StartDateTime__c);
        System.assertNotEquals(null, histories[0].EndDateTime__c);
        System.assertEquals(Constants.CASE_DEPT_PROGRESSIONS, histories[0].Department__c);

        System.assertEquals(rolloverCase.StatusDepartment__c, histories[1].Status_Stage__c);
        System.assertEquals('State Change', histories[1].Type__c);
        System.assertEquals(getOwnerNameFromId(rolloverCase.OwnerId), histories[1].User__c);
        System.assertEquals(rolloverCase.ContactId, histories[1].Contact_ID__c);
        System.assertNotEquals(null, histories[1].StartDateTime__c);
        System.assertEquals(null, histories[1].EndDateTime__c);
        System.assertEquals(Constants.CASE_DEPT_FINANCE, histories[1].Department__c);
    }

    @isTest
    static void TrackStatusOnClosedCaseReopened_CreatesNewClosedAndThenUpdatesExistingStatusTrackingHistoryRecord()
    {
        // Arrange
        Case closedCase =
        [
            SELECT Id, Status, Department__c, StatusDepartment__c, OwnerId, ContactId
            FROM Case
            WHERE Contact.Cerebro_Student_ID__c = : cerebroID
        ];
        closedCase.Status = Constants.CASE_STATUS_CLOSED;
        closedCase.recalculateFormulas();
        ActionResult result = CaseService.trackStatusHistory(new List<Case> { closedCase }, NULL, Enums.TriggerContext.AFTER_INSERT);

        // Act
        Test.startTest();
        List<Status_Tracking_History__c> closedHistory =
        [
            SELECT Id, Contact_ID__c, Status_Stage__c, StartDateTime__c, EndDateTime__c, Type__c, Direction__c, Next_Status_Stage__c, User__c, Next_User__c, Department__c
            FROM Status_Tracking_History__c
            WHERE CaseId__c = : closedCase.Id
        ];

        Map<Id, Case> oldMap = new Map<Id, Case>();
        oldMap.put(closedCase.Id, closedCase);
        Case rolloverCase = closedCase.clone(true, false, true, true);
        rolloverCase.OwnerId = Constants.QUEUEID_FINANCE_TEAM;
        rolloverCase.Status = Constants.CASE_STATUS_WAITING;
        rolloverCase.Department__c = Constants.CASE_DEPT_FINANCE;
        rolloverCase.recalculateFormulas();

        result = CaseService.trackStatusHistory(new List<Case> { rolloverCase }, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        Test.stopTest();

        List<Status_Tracking_History__c> histories =
        [
            SELECT Id, Contact_ID__c, Status_Stage__c, StartDateTime__c, EndDateTime__c, Type__c, Direction__c, Next_Status_Stage__c, User__c, Next_User__c, Department__c
            FROM Status_Tracking_History__c
            WHERE CaseId__c = : rolloverCase.Id
        ];

        // Assert
        System.assertEquals(1, closedHistory.size());
        System.assert(closedHistory[0].Status_Stage__c.contains(Constants.CASE_STATUS_CLOSED));
        System.assertEquals(null, closedHistory[0].Next_Status_Stage__c);
        System.assertNotEquals(null, closedHistory[0].StartDateTime__c);
        System.assertNotEquals(null, closedHistory[0].EndDateTime__c);
        System.assertEquals(closedHistory[0].StartDateTime__c, closedHistory[0].EndDateTime__c);

        System.assertEquals(2, histories.size());
        System.assertEquals(rolloverCase.StatusDepartment__c, histories[0].Next_Status_Stage__c);
        System.assert(histories[0].Status_Stage__c.contains(Constants.CASE_STATUS_CLOSED));
        System.assertEquals('State Change', histories[0].Type__c);
        System.assertEquals('Backward', histories[0].Direction__c);
        System.assertEquals(getOwnerNameFromId(closedCase.OwnerId), histories[0].User__c);
        System.assertEquals(getOwnerNameFromId(rolloverCase.OwnerId), histories[0].Next_User__c);
        System.assertNotEquals(null, histories[0].StartDateTime__c);
        System.assertNotEquals(null, histories[0].EndDateTime__c);
        System.assertEquals(Constants.CASE_DEPT_PROGRESSIONS, histories[0].Department__c);

        System.assertEquals(rolloverCase.StatusDepartment__c, histories[1].Status_Stage__c);
        System.assertEquals('State Change', histories[1].Type__c);
        System.assertEquals(getOwnerNameFromId(rolloverCase.OwnerId), histories[1].User__c);
        System.assertEquals(rolloverCase.ContactId, histories[1].Contact_ID__c);
        System.assertNotEquals(null, histories[1].StartDateTime__c);
        System.assertEquals(null, histories[1].EndDateTime__c);
        System.assertEquals(Constants.CASE_DEPT_FINANCE, histories[1].Department__c);
    }

    @isTest
    static void TrackOwnerOnExistingCase_CreatesNewAndUpdatesExistingStatusTrackingHistoryRecord()
    {
        // Arrange
        Case rolloverCase =
        [
            SELECT Id, Status, Department__c, StatusDepartment__c, OwnerId, ContactId
            FROM Case
            WHERE Contact.Cerebro_Student_ID__c = : cerebroID
        ];

        ActionResult result = CaseService.trackStatusHistory(new List<Case> { rolloverCase }, NULL, Enums.TriggerContext.AFTER_INSERT);
        Map<Id, Case> oldMap = new Map<Id, Case>();
        oldMap.put(rolloverCase.Id, rolloverCase);
        Case newCase = rolloverCase.clone(true, false, true, true);
        String originalUser = getOwnerNameFromId(newCase.OwnerId);
        newCase.OwnerId = Constants.QUEUEID_FINANCE_TEAM;
        newCase.recalculateFormulas();

        // Act
        Test.startTest();
        result = CaseService.trackStatusHistory(new List<Case> { newCase }, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        oldMap.put(newCase.Id, newCase);
        rolloverCase = newCase.clone(true, false, true, true);
        rolloverCase.Status = Constants.CASE_STATUS_WAITING;
        rolloverCase.Department__c = Constants.CASE_DEPT_FINANCE;
        rolloverCase.recalculateFormulas();
        result = CaseService.trackStatusHistory(new List<Case> { rolloverCase }, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        Test.stopTest();

        List<Status_Tracking_History__c> histories =
        [
            SELECT Id, Contact_ID__c, Status_Stage__c, StartDateTime__c, EndDateTime__c, Type__c, Direction__c, Next_Status_Stage__c, User__c, Next_User__c, Department__c
            FROM Status_Tracking_History__c
            WHERE CaseId__c = : rolloverCase.Id
        ];

        // Assert
        String ownerName = getOwnerNameFromId(newCase.OwnerId);
        System.assertEquals(2, histories.size());
        System.assertEquals(newCase.StatusDepartment__c, histories[0].Status_Stage__c);
        System.assertEquals(rolloverCase.StatusDepartment__c, histories[0].Next_Status_Stage__c);
        System.assertEquals('State Change', histories[0].Type__c);
        System.assertEquals('Forward', histories[0].Direction__c);
        System.assertEquals(originalUser, histories[0].User__c);
        System.assertEquals(ownerName, histories[0].Next_User__c);
        System.assertNotEquals(null, histories[0].StartDateTime__c);
        System.assertNotEquals(null, histories[0].EndDateTime__c);
        System.assertEquals(Constants.CASE_DEPT_PROGRESSIONS, histories[0].Department__c);

        System.assertEquals(rolloverCase.StatusDepartment__c, histories[1].Status_Stage__c);
        System.assertEquals(null, histories[1].Next_Status_Stage__c);
        System.assertEquals('State Change', histories[1].Type__c);
        System.assertEquals(rolloverCase.ContactId, histories[1].Contact_ID__c);
        System.assertEquals(null, histories[1].EndDateTime__c);
        System.assertNotEquals(null, histories[1].StartDateTime__c);
        System.assertEquals(null, histories[1].EndDateTime__c);
        System.assertEquals(ownerName, histories[1].User__c);
        System.assertEquals(Constants.CASE_DEPT_FINANCE, histories[1].Department__c);
    }

    @isTest
    static void TrackStatusOnNewCaseAndChangeToWithContact_CreatesOneClosedAndOneOpenStatusTrackingHistoryRecord()
    {
        // Arrange
        Case rolloverCase =
        [
            SELECT Id, Status, Department__c, StatusDepartment__c, OwnerId, ContactId
            FROM Case
            WHERE Contact.Cerebro_Student_ID__c = : cerebroID
        ];

        ActionResult result = CaseService.trackStatusHistory(new List<Case> { rolloverCase }, NULL, Enums.TriggerContext.AFTER_INSERT);
        Map<Id, Case> oldMap = new Map<Id, Case>();
        oldMap.put(rolloverCase.Id, rolloverCase);
        Case newCase = rolloverCase.clone(true, false, true, true);
        newCase.Status = Constants.CASE_STATUS_WITH_CONTACT;
        newCase.recalculateFormulas();

        // Act
        Test.startTest();
        result = CaseService.trackStatusHistory(new List<Case> { newCase }, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        Test.stopTest();

        List<Status_Tracking_History__c> histories =
        [
            SELECT Id, Contact_ID__c, Status_Stage__c, StartDateTime__c, EndDateTime__c, Type__c, Next_Status_Stage__c, User__c, Next_User__c, Direction__c
            FROM Status_Tracking_History__c
            WHERE CaseId__c = : rolloverCase.Id
        ];

        // Assert
        String ownerName = getOwnerNameFromId(rolloverCase.OwnerId);
        System.assertEquals(2, histories.size());
        System.assertEquals(rolloverCase.StatusDepartment__c, histories[0].Status_Stage__c);
        System.assertEquals(newCase.StatusDepartment__c, histories[0].Next_Status_Stage__c);
        System.assertEquals('State Change', histories[0].Type__c);
        System.assertEquals(rolloverCase.ContactId, histories[0].Contact_ID__c);
        System.assertNotEquals(null, histories[0].StartDateTime__c);
        System.assertNotEquals(null, histories[0].EndDateTime__c);
        System.assertEquals(ownerName, histories[0].User__c);
        System.assertEquals(ownerName, histories[0].Next_User__c);
        System.assertEquals('Forward', histories[0].Direction__c);
    }

    private static String getOwnerNameFromId(Id ownerId)
    {
        return String.valueOf(ownerId).startsWith(Constants.ENTITYID_USER) ? UserService.get(ownerId).Name : GroupService.get(ownerId).Name;
    }
}