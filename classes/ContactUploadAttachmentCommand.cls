public class ContactUploadAttachmentCommand extends BaseViewModelCommand
{
    public override List<Interfaces.IViewModel> validate(List<Interfaces.IViewModel> params)
    {
        return params;
    }

    public override ActionResult command(List<Interfaces.IViewModel> params)
    {
        for (ViewModelsFile.Upload param: (List<ViewModelsFile.Upload>) params)
        {

            if (String.isEmpty(param.Reference))
            {
                Log.error(this.className, 'validate', 'Reference parameter is null');
                return new ActionResult(ResultStatus.ERROR, 'Reference parameter cannot be empty or null');
            }

            if (param.Content.size() <= 0)
            {
                Log.error(this.className, 'validate', 'FileContent parameter is null');
                return new ActionResult(ResultStatus.ERROR, 'FileContent parameter cannot be empty or null');
            }

            if (getOpportunity(param.Reference) == null)
            {
                Log.error(this.className, 'validate', 'Opportunity does not exists in database: ' + param.Reference);
                return new ActionResult(ResultStatus.ERROR, 'Opportunity does not exist');
            }

            if (String.isEmpty(param.FileName))
            {
                Log.error(this.className, 'validate', 'Filename value cannot be null or empty');
                return new ActionResult(ResultStatus.ERROR, 'Filename value cannot be null or empty');
            }

            if (String.isEmpty(param.Type))
            {
                Log.error(this.className, 'validate', 'Type value cannot be null or empty');
                return new ActionResult(ResultStatus.ERROR, 'Type value cannot be null or empty');
            }

            if (String.valueOf(param.FileName).lastIndexOf('.') == -1)
            {
                Log.error(this.className, 'validate', 'Invalid file type');
                return new ActionResult(ResultStatus.ERROR, 'Invalid file type');
            }

            String extension = param.Filename.substring(param.Filename.lastIndexOf('.') + 1).toLowerCase();

            if (!Constants.ALLOWED_UPLOAD_FILEEXTENSIONS.contains(extension))
            {
                Log.error(this.className, 'validate', 'Unsupported file extension');
                return new ActionResult(ResultStatus.ERROR, 'Unsupported file extension');
            }

            try
            {
                ActionResult result = new ActionResult();
                Opportunity opp = getOpportunity(param.Reference);

                param.ParentId = opp.Id;
                ActionResult attResult = ContentDocumentService.createFile((ViewModelsFile.Upload) param);

                if (attResult.isSuccess)
                {
                    result.Status = ResultStatus.SUCCESS;
                    result.ResultId = opp.Id; 

                    return result; 
                }

                result.Status = ResultStatus.ERROR;
                result.Messages = new List<String> { attResult.Message };

                return result; 
            }
            catch (Exception ex)
            {
                Log.error(this.className, 'command', ex.getMessage());
                return new ActionResult(ResultStatus.ERROR, ex.getMessage());
            }
        }

        return new ActionResult();
    }

    private Opportunity getOpportunity(String identifier)
    {
        try 
        {
            String query = 'Id,(' +
                'SELECT Id, OwnerId, StageName ' +
                'FROM Opportunities__r ' +
                'WHERE RecordType.DeveloperName = \'' + Constants.RECORDTYPE_OPPORTUNITY_ACQUISITION + '\' AND IsClosed = false )';
            Contact contact = ContactService.get(identifier, query, false);

            if (contact != null)
            {
                List<Opportunity> opps = contact.Opportunities__r;

                if (opps.size() > 0)
                    return opps[0];
                else
                    return null;
            }
            else
                return null;
        }
        catch (Exception ex)
        {
            Log.error(this.className, 'getOpportunity', ex.getMessage());
            throw new Exceptions.ApplicationException(ex.getMessage());
        }
    }
}