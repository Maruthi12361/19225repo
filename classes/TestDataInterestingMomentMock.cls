@isTest
public class TestDataInterestingMomentMock 
{
	public static InterestingMoment__c create(User user, Contact contact) 
    {
        return create(user, contact, 'Web Page Access', 'Google', 5);
    }
    
    public static InterestingMoment__c create(User user, Contact contact, Datetime eventDate) 
    {
        return create(user, contact, eventDate, 'Web Page Access', 50, 'Google');
    }

    public static InterestingMoment__c create(User user, Contact contact, String type, String source, Double score) 
    {
        return create(user, contact, Datetime.now(), type, score, source);
    }

    public static InterestingMoment__c create(User user, Contact contact, Datetime eventDate, String type, Double behaviourScore, String source) 
    {
        InterestingMoment__c moment = new InterestingMoment__c
        (
            ContactID__c = contact.Id,
            EventDate__c = eventDate != null ? eventDate : Datetime.now(),
            Source__c = String.isEmpty(source) ? 'aib.edu.au' : source ,
            Type__c = String.isEmpty(type) ? 'Web Page Access' : type,
            BehaviourScore__c = behaviourScore != null ? behaviourScore : 5
        );

        System.RunAs(user)
        {
            insert moment;
        }
            
        return moment;
    }
}