@isTest
public class AffiliationSetQualificationCommandTests
{

    @testSetup
    public static void setup()
    {
        TestUtilities.setupHEDA();
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact facultyContact = TestDataContactMock.createFacultyContact(user);
    }

    @isTest
    public static void CreatedMultpleFacultyQualificationsSetToHighest_OnlyOneQualificationSetToHighest()
    {
        // Arrange
        Contact contact = [SELECT Id FROM Contact LIMIT 1];
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Account educationalAccount = TestDataAccountMock.create(user, Constants.ACCOUNT_INSTITUTION_AIB);

        // Act
        test.startTest();
        TestDataAffiliationMock.createQualification(user, educationalAccount, contact, 'Qualification', 'Current', true);
        TestDataAffiliationMock.createQualification(user, educationalAccount, contact, 'Qualification', 'Current', true);
        hed__Affiliation__c latestQualification = TestDataAffiliationMock.createQualification(user, educationalAccount, contact, 'Qualification', 'Current', true);
        test.stopTest();

        List<hed__Affiliation__c> qualifications =
        [      
            SELECT Id, hed__Contact__c, IsHighestQualification__c
            FROM hed__Affiliation__c
            WHERE hed__Contact__c = :contact.Id AND RecordType.DeveloperName = :Constants.RECORDTYPE_AFFILIATION_QUALIFICATION
        ];

        // Assert
        System.assertEquals(3, qualifications.size(), 'Unexpected number of qualifications');

        Integer highestQualificationCount = 0;
        Id higestQualificationId;
        for (hed__Affiliation__c qualification : qualifications)
        {
            if (qualification.IsHighestQualification__c == true)
            {
                highestQualificationCount += 1;
                higestQualificationId = qualification.Id;
            }
        }

        System.assertEquals(1, highestQualificationCount, 'Expected a single qualification with IsHighestQualification set to true');
        System.assertEquals(latestQualification.Id, higestQualificationId, 'Expected last created qualification with IsHighestQualification set to true to remain true');
    }

    @isTest
    public static void CreatedMultpleFacultyQualificationsWithVariousTypesSetToHighest_OnlyOneQualificationSetToHighestForEachType()
    {
        // Arrange
        Contact contact = [SELECT Id FROM Contact LIMIT 1];
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Account educationalAccount = TestDataAccountMock.create(user, Constants.ACCOUNT_INSTITUTION_AIB);

        // Act
        test.startTest();
        TestDataAffiliationMock.createQualification(user, educationalAccount, contact, 'Qualification', 'Current', true);
        hed__Affiliation__c latestQualification = TestDataAffiliationMock.createQualification(user, educationalAccount, contact, 'Qualification', 'Current', true);
        TestDataAffiliationMock.createQualification(user, educationalAccount, contact, 'Accreditation', 'Current', true);
        hed__Affiliation__c latestAccreditation = TestDataAffiliationMock.createQualification(user, educationalAccount, contact, 'Accreditation', 'Current', true);
        TestDataAffiliationMock.createQualification(user, educationalAccount, contact, 'Membership', 'Current', true);
        hed__Affiliation__c latestMembership = TestDataAffiliationMock.createQualification(user, educationalAccount, contact, 'Membership', 'Current', true);
        test.stopTest();

        List<hed__Affiliation__c> qualifications =
        [      
            SELECT Id, hed__Contact__c, IsHighestQualification__c, Type__c
            FROM hed__Affiliation__c
            WHERE hed__Contact__c = :contact.Id AND RecordType.DeveloperName = :Constants.RECORDTYPE_AFFILIATION_QUALIFICATION
        ];

        // Assert
        System.assertEquals(6, qualifications.size(), 'Unexpected number of qualifications');

        Id higestQualificationId;
        Id higestAccreditationId;
        Id higestMembershipId;
        Integer highestQualificationCount = 0;
        Integer highestAccreditationCount = 0;
        Integer highestMembershipCount = 0;
        
        for (hed__Affiliation__c qualification : qualifications)
        {
            if (qualification.IsHighestQualification__c == true && qualification.Type__c == 'Qualification')
            {
                highestQualificationCount += 1;
                higestQualificationId = qualification.Id;
            }
            else if (qualification.IsHighestQualification__c == true && qualification.Type__c == 'Accreditation')
            {
                highestAccreditationCount += 1;
                higestAccreditationId = qualification.Id;
            }
            else if (qualification.IsHighestQualification__c == true && qualification.Type__c == 'Membership')
            {
                highestMembershipCount += 1;
                higestMembershipId = qualification.Id;
            }
        }

        System.assertEquals(1, highestQualificationCount, 'Expected a single qualification with IsHighestQualification set to true');
        System.assertEquals(latestQualification.Id, higestQualificationId, 'Expected last created qualification with IsHighestQualification set to true to remain true');
        
        System.assertEquals(1, highestAccreditationCount, 'Expected a single accreditation with IsHighestQualification set to true');
        System.assertEquals(latestAccreditation.Id, higestAccreditationId, 'Expected last created qualification with IsHighestQualification set to true to remain true');
        
        System.assertEquals(1, highestMembershipCount, 'Expected a single membership with IsHighestQualification set to true');
        System.assertEquals(latestMembership.Id, higestMembershipId, 'Expected last created qualification with IsHighestQualification set to true to remain true');
    }
}