public with sharing class ContactCampaignMemberDeletionCommand extends BaseIdCommand
{
	
	public override Set<Id> validate(Set<Id> params)
	{
		return params;
	}

	public override ActionResult command(Set<Id> params)
    {
        try
        {
            List<Contact> contacts = getContacts(params);

            for(Contact c : contacts)
            {
            	c.IsWarmCampaignMember__c = false;
            }
			uow.registerDirty(contacts);
            return new ActionResult(ResultStatus.SUCCESS, new List<String>{'Success'}); 

        }
        catch(Exception ex)
        {
            String error = String.Format('{0} - {1}, \n{2}', new List<String>{'LogCommand', ex.getMessage(), ex.getStackTraceString()});
            Log.error(this.className, 'command', error);
            return new ActionResult(ResultStatus.ERROR, new List<String>{error}); 
        }
    }


    private List<Contact> getContacts(Set<Id> ids)
    {
    	String sql = 'SELECT Id,IsWarmCampaignMember__c FROM Contact WHERE Id in :ids';
    	return (List<Contact>)Database.query(sql);
    }
}