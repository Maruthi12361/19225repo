public class OpportunityAssignCampaignCommand extends BaseQueueableCommand
{
    public OpportunityAssignCampaignCommand()
    {
        super(1, 5);
    }

    public override List<SObject> validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        List<Opportunity> result = new List<Opportunity>();
        List<Opportunity> opps = getOpportunitiesWithContact(SObjectUtils.getIDs(items, 'ID'));

        for (Opportunity opp : opps)
        {
            if (opp.RecordTypeId != System.Label.ID_RecordType_Opportunity_Acquisition)
            {
                Log.info(this.className, 'validate', 'Opportunity is not Acquisition. No Campaign assigned to Opportunity');
                continue;
            }

            if (opp.Contact__r.Preferred_Start_Date__c == null)
            {
                Log.info(this.className, 'validate', 'Opportunity {0} has a Contact {1} with Preferred start date equal to null. No Campaign assigned to Opportunity', new List<String>{opp.Contact__c, opp.Id});
                continue;
            }

            result.add(opp);
        }

        return result;
    }

    public override ActionResult command(List<SObject> items, Id jobId)
    {
        List<Opportunity> opps = getOpportunitiesWithContact(SObjectUtils.getIDs(items, 'ID'));
        List<Opportunity> oppsToUpdate = new List<Opportunity>();

        for (Opportunity opp : opps)
        {
            String year = opp.Contact__r.Preferred_Start_Date__c.right(4);
            String month = opp.Contact__r.Preferred_Start_Date__c.mid(3, 2);
            String day = opp.Contact__r.Preferred_Start_Date__c.left(2);
            Date preferredStartDate = Date.valueOf(year + '-' + month + '-' + day);

            Campaign campaign = getCampaignByDate(preferredStartDate, opp.Contact__r.Portfolio__c, opp.Contact__r.Program__c);

            if (campaign == null)
            {
                Log.info(this.className, 'command', 'No campaign found for {0} Preferred Start Date and {1} program portfolio. Campaign not set to Opportunity {2}',
                    new List<String>{ String.valueOf(preferredStartDate), opp.Contact__r.Program__c, opp.Contact__r.Portfolio__c, opp.Id});
                continue;
            }

            opp.CampaignId = campaign.ID;
            opp.Name = String.format('{0} {1} - {2}', new List<String>{ String.isEmpty(opp.Contact__r.FirstName) ? '' : opp.Contact__r.FirstName, opp.Contact__r.LastName, campaign.Name });
            opp.CloseDate = campaign.TermCensusDate__c;

            oppsToUpdate.add(opp);

            Log.info(this.className, 'command', 'Assigning {0} campaign to {1} opportunity', new List<String>{ campaign.ID, opp.ID });
        }

        uow.registerDirty(oppsToUpdate);

        return new ActionResult(ResultStatus.SUCCESS);
    }

    private List<Opportunity> getOpportunitiesWithContact(Set<ID> ids)
    {
        String query =
            'SELECT Id, Name, CloseDate, CampaignId, RecordTypeId, Contact__r.Program__c, Contact__r.Portfolio__c, Contact__r.FirstName, Contact__r.LastName, Contact__r.Preferred_Start_Date__c '+
            'FROM Opportunity ' +
            'WHERE ID IN :ids';

        return Database.query(query);
    }

    private Campaign getCampaignByDate(Date preferredStartDate, String portfolio, String program)
    {
        List<Campaign> allCampaignList;
        Campaign assignedCampaign;

        if (!String.isBlank(portfolio))
        {
            if (portfolio.startsWith('TC'))
                portfolio = 'TC';
            else if (portfolio.startsWith('International'))
                portfolio = 'International';
            else
                portfolio = 'Domestic';
        }

        if (!String.isBlank(program))
        {
            if (program != 'BBA' && program != 'MBA' && program != 'GCM')
                program = 'MBA';
        }

        if (allCampaignList == null || allCampaignList.size() == 0)
        {
            allCampaignList = 
                [
                    SELECT Id, Name, TermStartDate__c, TermCensusDate__c, Portfolio__c, Programme__c
                    FROM Campaign
                    WHERE Type IN ('Acquisition') AND Status IN : Constants.CAMPAIGN_ACTIVE_STATUS AND IsActive = true
                    ORDER BY TermStartDate__c
                ];
        }

        for (Campaign campaign : allCampaignList)
        {
            if (campaign.TermStartDate__c == preferredStartDate && campaign.Programme__c == program && campaign.Portfolio__c == portfolio)
            {
                assignedCampaign = campaign;
                Log.info(this.className, 'getCampaignByDate', 'Match preferredStartDate: ' + preferredStartDate + ' to New Campaign: ' + assignedCampaign);
                break;
            }
        }

        return assignedCampaign;
    }
}