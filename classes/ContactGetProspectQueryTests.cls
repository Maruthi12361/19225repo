@IsTest
public class ContactGetProspectQueryTests
{
    @testSetup
    public static void setup()
    {
        TestUtilities.setupHEDA();
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact contact = TestDataContactMock.createProspectCold(user, '0455548889');
    }

    @isTest
    static void NoIdentifierProvidedOptionsFalse_BlankProspectObjectReturned()
    {
        // Arrange
        ViewModels.Param param = new ViewModels.Param();
        param.Identifier = '';
        param.IsAuthenticated = true;

        // Act
        test.startTest();
        ViewModelsAcquisition.Prospect prospect = new ContactGetProspectQuery().query(param);
        test.stopTest();

        // Assert
        System.assertNotEquals(null, prospect, 'Propsect should not be null');
        System.assertEquals(null, prospect.Details.Cnt_LastName, 'Unexpected last name');
    }

    @isTest
    static void EmailIdentifierProvided_BlankProspectObjectReturned()
    {
        // Arrange
        Contact contact = [SELECT Id, LastName, Email FROM Contact Limit 1];
        ViewModels.Param param = new ViewModels.Param();
        param.Identifier = contact.Email;
        param.IsAuthenticated = true;

        // Act
        test.startTest();
        ViewModelsAcquisition.Prospect prospect = new ContactGetProspectQuery().query(param);
        test.stopTest();

        // Assert
        System.assertNotEquals(null, prospect, 'Propsect should not be null');
        System.assertEquals(null, prospect.Details.Id, 'Unexpected id value');
        System.assertEquals(null, prospect.Details.Cnt_Email, 'Unexpected email value');
    }

    @isTest
    static void EncryptedIdentifierProvided_ProspectObjectReturned()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        Contact contact = [SELECT Id, LastName, Email FROM Contact Limit 1];
        ViewModels.Param param = new ViewModels.Param();
        param.Identifier = EncryptionUtil.Encrypt(contact.Id);
        param.IsAuthenticated = true;

        // Act
        test.startTest();
        ViewModelsAcquisition.Prospect prospect = new ContactGetProspectQuery().query(param);
        test.stopTest();

        // Assert
        System.assertNotEquals(null, prospect, 'Propsect should not be null');
        System.assertEquals(contact.LastName, prospect.Details.Cnt_LastName, 'Unexpected last name');
    }

    @isTest
    static void EncryptedOpportunityIdentifierProvided_ProspectObjectReturned()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact contact = [SELECT Id, LastName, Email FROM Contact Limit 1];
        TestDataAttachmentMock.create(user, contact.Id);
        Opportunity opp = TestDataOpportunityMock.create(user, contact, '', true);

        ViewModels.Param param = new ViewModels.Param();
        param.Identifier = EncryptionUtil.Encrypt(opp.Id);
        param.IsAuthenticated = true;

        // Act
        test.startTest();
        ViewModelsAcquisition.Prospect prospect = new ContactGetProspectQuery().query(param);
        test.stopTest();

        // Assert
        System.assertNotEquals(null, prospect, 'Propsect should not be null');
        System.assertEquals(contact.LastName, prospect.Details.Cnt_LastName, 'Unexpected last name');
    }

    @isTest
    static void HotProspectWithOppAndCaseFiles_ProspectCaseAndOpportunityFilesReturned()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createProspectHot(user);
        Opportunity opp = TestDataOpportunityMock.createApplyingAcquisition(user, contact);
        Case appCase = TestDataCaseMock.createApplication(user, contact, opp);

        System.runAs(user)
        {
            contact.EncryptedId__c = IdUtils.generateGUID();
            update contact;
            opp.Application_Case__c = appCase.Id;
            update opp;
        }

        TestDataContentVersionMock.create(user, 'WorkExperience.txt', 'WorkExperience', opp.Id, Constants.CONTENTVERSION_TYPE_WORKEXPERIENCE);
        TestDataContentVersionMock.create(user, 'LetterOfOffer.txt', 'LetterOfOffer', opp.Application_Case__c, Constants.CONTENTVERSION_TYPE_LOO);

        ViewModels.Param param = new ViewModels.Param();
        param.Identifier = contact.EncryptedId__c;
        param.IsAuthenticated = true;

        // Act
        test.startTest();
        ViewModelsAcquisition.Prospect prospect = new ContactGetProspectQuery().query(param);
        test.stopTest();

        // Assert
        System.assertNotEquals(null, prospect, 'Propsect should not be null');
        System.assertEquals(2, prospect.Files.size(), 'Unexpected number of files');
    }

    @isTest
    static void HotProspectWithOppAndAppCase_ProspectCaseAndOpportunityFieldValuesSuccessfullyReturned()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact contact = TestDataContactMock.createProspectHot(user);
        Contact createdContact = [SELECT Id, EncryptedId__c, (SELECT Id, Application_Case__c, ApplicationReviewed__c, StageName, TimetableAccessed__c, TranscriptDocs__c FROM Opportunities__r) FROM Contact WHERE Id =:contact.Id];
        Opportunity opportunity = createdContact.Opportunities__r[0];
        Case applicationCase = [SELECT Id, Invoice_Generated__c, Invoice_Required__c, FEE_HELP_Processed__c, FeeHelpDetailsRequired__c, Status FROM Case WHERE Id = :opportunity.Application_Case__c];

        ViewModels.Param param = new ViewModels.Param();
        param.Identifier = createdContact.EncryptedId__c;
        param.IsAuthenticated = true;

        // Act
        test.startTest();
        ViewModelsAcquisition.Prospect prospect = new ContactGetProspectQuery().query(param);
        test.stopTest();

        // Assert
        System.assertNotEquals(null, prospect, 'Propsect should not be null');
        System.assertEquals(opportunity.ApplicationReviewed__c, prospect.details.Opp_ApplicationReviewed_c, 'Unexpected opportunity application reviewed field value');
        System.assertEquals(opportunity.TimetableAccessed__c, prospect.details.Opp_TimetableAccessed_c, 'Unexpected opportunity timetable accessed field value');
        System.assertEquals(opportunity.TranscriptDocs__c, prospect.details.Opp_TranscriptDocs_c, 'Unexpected opportunity timetable accessed field value');
        System.assertEquals(opportunity.StageName == 'Closed Lost', prospect.details.Opp_IsLost, 'Unexpected opportunity is lost field value');

        System.assertEquals(applicationCase.Invoice_Generated__c, prospect.details.Case_Invoice_Generated_c, 'Unexpected case invoice generated field value');
        System.assertEquals(applicationCase.Invoice_Required__c, prospect.details.Case_Invoice_Required_c, 'Unexpected case invoice required field value');
        System.assertEquals(applicationCase.FEE_HELP_Processed__c, prospect.details.Case_FEE_HELP_Processed_c, 'Unexpected case Fee Help processed field value');
        System.assertEquals(applicationCase.FeeHelpDetailsRequired__c, prospect.details.Case_FeeHelpDetailsRequired_c, 'Unexpected case Fee Help details required field value');
        System.assertEquals(applicationCase.Status, prospect.details.Case_Status, 'Unexpected case status field value');
    }

    @isTest
    static void ProspectWithOwnerNonAdvisor_ProspectOwnerDetailsNotReturned()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact contact = TestDataContactMock.createProspectHot(user);
        Contact createdContact = [SELECT Id, EncryptedId__c, (SELECT Id, Application_Case__c, ApplicationReviewed__c, StageName, TimetableAccessed__c, TranscriptDocs__c FROM Opportunities__r) FROM Contact WHERE Id =:contact.Id];

        ViewModels.Param param = new ViewModels.Param();
        param.Identifier = createdContact.EncryptedId__c;
        param.IsAuthenticated = true;

        // Act
        test.startTest();
        ViewModelsAcquisition.Prospect prospect = new ContactGetProspectQuery().query(param);
        test.stopTest();

        // Assert
        System.assertNotEquals(null, prospect, 'Propsect should not be null');
        System.assertEquals(null, prospect.details.Owner_FirstName, 'Unexpected owner first name field value');
        System.assertEquals(null, prospect.details.Owner_LastName, 'Unexpected owner last name field value');
        System.assertEquals(null, prospect.details.Owner_Email, 'Unexpected owner email field value');
        System.assertEquals(null, prospect.details.Owner_Phone, 'Unexpected owner phone field value');
        System.assertEquals(null, prospect.details.Owner_LinkedInURL_c, 'Unexpected owner linked in url field value');
    }

    @isTest
    static void ProspectWithOwnerAsAdvisor_ProspectOwnerDetailsReturnedSuccessfully()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact contact = TestDataContactMock.createProspectHot(user);
        Contact createdContact = [SELECT Id, EncryptedId__c, (SELECT Id, Application_Case__c, ApplicationReviewed__c, StageName, TimetableAccessed__c, TranscriptDocs__c FROM Opportunities__r) FROM Contact WHERE Id =:contact.Id];

        ViewModels.Param param = new ViewModels.Param();
        param.Identifier = createdContact.EncryptedId__c;
        param.IsAuthenticated = true;

        user.InGroup__c = 'CourseAdvisor';
        update user;

        // Act
        test.startTest();
        ViewModelsAcquisition.Prospect prospect = new ContactGetProspectQuery().query(param);
        test.stopTest();

        // Assert
        System.assertNotEquals(null, prospect, 'Propsect should not be null');
        System.assertEquals(user.FirstName, prospect.details.Owner_FirstName, 'Unexpected owner first name field value');
        System.assertEquals(user.LastName, prospect.details.Owner_LastName, 'Unexpected owner last name field value');
        System.assertEquals(user.Email, prospect.details.Owner_Email, 'Unexpected owner email field value');
        System.assertEquals(user.Phone, prospect.details.Owner_Phone, 'Unexpected owner phone field value');
        System.assertEquals(user.LinkedInURL__c, prospect.details.Owner_LinkedInURL_c, 'Unexpected owner linked in url field value');
    }
}