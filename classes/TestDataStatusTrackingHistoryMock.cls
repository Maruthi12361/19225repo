@isTest
public class TestDataStatusTrackingHistoryMock
{
    public static Status_Tracking_History__c createStatusTrackingHistory(User user, Case parentCase, Boolean saveRecord)
    {
        return createStatusTrackingHistory(user, parentCase, saveRecord, System.now(), System.now());
    }

    public static Status_Tracking_History__c createStatusTrackingHistory(User user, Case parentCase, Boolean saveRecord, Datetime startTime, Datetime endTime)
    {
        Status_Tracking_History__c sth = new Status_Tracking_History__c
        (
            BusinessMinutesInStage__c = null,
            CaseId__c = parentCase.Id,
            Contact_ID__c = parentCase.ContactId,
            Department__c = parentCase.Department__c,
            EndDateTime__c = endTime,
            StartDateTime__c = startTime,
            Status_Stage__c = parentCase.StatusDepartment__c,
            Type__c = 'State Change',
            User__c = user.FirstName + ' ' + user.LastName
        );

        return saveRecord ? insertSTHAsUser(sth, user) : sth;
    }

    private static Status_Tracking_History__c insertSTHAsUser(Status_Tracking_History__c sthToStore, User user)
    {
        System.runAs(user)
        {
            insert sthToStore;
        }

        return sthToStore;
    }
}