public class StatusTrackingHistoryCalcAgeSchedule implements Schedulable
{
    public void execute(SchedulableContext sc)
    {
        // Batch size reduced from default 200 to 190 to avoid governor limits as its possible in the command that with 200 each
        // STH record that is passed in could have a different case parent resulting in one SQL query for each record gathering
        // all STH for that case plus one initial query gathering all parent cases, ie: 200+1 SQL queries in a single transaction
        Database.executebatch(new StatusTrackingHistoryCalcAgeBatch(), 190);
    }
}