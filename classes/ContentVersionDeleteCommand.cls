public class ContentVersionDeleteCommand extends BaseIdCommand
{
    public override Set<ID> validate(Set<ID> items)
    {
       return items;
    }
    
    public override ActionResult command(Set<ID> items)
    {
        Log.info(this.className, 'command', 'Deleting {0} file attachments', new List<String>{ JSON.serialize(items) });
        List<ID> attachmentIds = new List<ID>();
        attachmentIds.addAll(items);

        Database.delete(attachmentIds, true);

        return new ActionResult(ResultStatus.SUCCESS);
    }
}