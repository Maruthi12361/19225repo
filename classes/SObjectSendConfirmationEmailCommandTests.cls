@isTest
public class SObjectSendConfirmationEmailCommandTests
{
    @testSetup
    static void setup()
    {
        TestUtilities.setupHEDA();
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createProspectHot(user);
        Opportunity opp = TestDataOpportunityMock.createApplyingAcquisition(user, contact);
        Case appCase = TestDataCaseMock.createApplication(user, contact, opp);

        System.runAs(user)
        {
            opp.Application_Case__c = appCase.Id;
            update opp;
        }
    }

    @isTest
    static void OpportunityOAFSubmittedSetToTrue_ApplicationConfirmationEmailSuccessfullySent()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        Map<Id, Opportunity> oldMap = new Map<Id, Opportunity>();
        Opportunity opp =
        [
            SELECT Id, Application_Case__c, FEE_HELP_Forms_Submitted__c, Lost_Reason__c, OAF_Submitted__c, OfferAcceptedDate__c, OfferGeneratedDate__c, Payment_Method__c,
                RecordTypeId, Remittance_Received__c, StageName, TimetableAccepted__c, TimetableSent__c
            FROM Opportunity LIMIT 1
        ];
        oldMap.put(opp.Id, opp);

        Opportunity clonedOpp = opp.clone(true, true);
        clonedOpp.OAF_Submitted__c = true;

        // Act
        Test.startTest();
        ActionResult result = SObjectService.sendConfirmationEmail(new List<Opportunity>{ clonedOpp }, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        Test.stopTest();

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.status, 'Unexpected status');
        System.assertEquals(1, TestHttpCalloutMock.requests.size());
    }

    @isTest
    static void OpportunityOAFSubmittedSetToFalse_NoApplicationConfirmationEmailSent()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        Map<Id, Opportunity> oldMap = new Map<Id, Opportunity>();
        Opportunity opp =
        [
            SELECT Id, Application_Case__c, FEE_HELP_Forms_Submitted__c, Lost_Reason__c, OAF_Submitted__c, OfferAcceptedDate__c, OfferGeneratedDate__c, Payment_Method__c,
                RecordTypeId, Remittance_Received__c, StageName, TimetableAccepted__c, TimetableSent__c
            FROM Opportunity LIMIT 1
        ];
        opp.OAF_Submitted__c = true;
        oldMap.put(opp.Id, opp);

        Opportunity clonedOpp = opp.clone(true, true);
        clonedOpp.OAF_Submitted__c = false;

        // Act
        Test.startTest();
        ActionResult result = SObjectService.sendConfirmationEmail(new List<Opportunity>{ clonedOpp }, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        Test.stopTest();

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.status, 'Unexpected status');
        System.assertEquals(0, TestHttpCalloutMock.requests.size());
    }

    @isTest
    static void OpportunityOfferAcceptedDateValueSetNoOfferGeneratedValue_LOOAcceptanceConfirmationEmailNotSent()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        Map<Id, Opportunity> oldMap = new Map<Id, Opportunity>();
        Opportunity opp =
        [
            SELECT Id, Application_Case__c, FEE_HELP_Forms_Submitted__c, Lost_Reason__c, OAF_Submitted__c, OfferAcceptedDate__c, OfferGeneratedDate__c, Payment_Method__c,
                RecordTypeId, Remittance_Received__c, StageName, TimetableAccepted__c, TimetableSent__c
            FROM Opportunity LIMIT 1
        ];
        oldMap.put(opp.Id, opp);

        Opportunity clonedOpp = opp.clone(true, true);
        clonedOpp.OfferAcceptedDate__c = Datetime.now();

        // Act
        Test.startTest();
        ActionResult result = SObjectService.sendConfirmationEmail(new List<Opportunity>{ clonedOpp }, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        Test.stopTest();

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.status, 'Unexpected status');
        System.assertEquals(0, TestHttpCalloutMock.requests.size());
    }

    @isTest
    static void OpportunityOfferAcceptedDateValueSetWithOfferGeneratedValue_LOOAcceptanceConfirmationEmailSuccessfullySent()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Map<Id, Opportunity> oldMap = new Map<Id, Opportunity>();
        Case appCase = [SELECT Id, OfferGeneratedDate__c FROM Case LIMIT 1];
        ContentVersion contentVersion = TestDataContentVersionMock.create(user, 'John Smith Letter of Offer.txt', 'John Smith Letter of Offer', appCase.Id);
        System.runAs(user)
        {
            appCase.OfferGeneratedDate__c = Datetime.now();
            update appCase;
        }

        Opportunity opp =
        [
            SELECT Id, Application_Case__c, FEE_HELP_Forms_Submitted__c, Lost_Reason__c, OAF_Submitted__c, OfferAcceptedDate__c, OfferGeneratedDate__c, Payment_Method__c,
                RecordTypeId, Remittance_Received__c, StageName, TimetableAccepted__c, TimetableSent__c
            FROM Opportunity LIMIT 1
        ];
        oldMap.put(opp.Id, opp);

        Opportunity clonedOpp = opp.clone(true, true);
        clonedOpp.OfferAcceptedDate__c = Datetime.now();

        // Act
        Test.startTest();
        ActionResult result = SObjectService.sendConfirmationEmail(new List<Opportunity>{ clonedOpp }, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        Test.stopTest();

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.status, 'Unexpected status');
        System.assertEquals(1, TestHttpCalloutMock.requests.size());
    }

    @isTest
    static void OpportunityStageSetToCloseLostWithNotApplicableLostStatus_ApplicationRejectionEmailNotSent()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        Map<Id, Opportunity> oldMap = new Map<Id, Opportunity>();
        Opportunity opp =
        [
            SELECT Id, Application_Case__c, FEE_HELP_Forms_Submitted__c, Lost_Reason__c, OAF_Submitted__c, OfferAcceptedDate__c, OfferGeneratedDate__c, Payment_Method__c,
                RecordTypeId, Remittance_Received__c, StageName, TimetableAccepted__c, TimetableSent__c
            FROM Opportunity LIMIT 1
        ];
        oldMap.put(opp.Id, opp);

        Opportunity clonedOpp = opp.clone(true, true);
        clonedOpp.StageName = 'Closed Lost';
        clonedOpp.Lost_Reason__c = Constants.OPPORTUNITY_CLOSEDLOST_DUPLICATE;

        // Act
        Test.startTest();
        ActionResult result = SObjectService.sendConfirmationEmail(new List<Opportunity>{ clonedOpp }, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        Test.stopTest();

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.status, 'Unexpected status');
        System.assertEquals(0, TestHttpCalloutMock.requests.size());
    }

    @isTest
    static void OpportunityTimetableSentSetToTrue_TimetableAcceptanceRequestEmailSuccessfullySent()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        Map<Id, Opportunity> oldMap = new Map<Id, Opportunity>();

        Opportunity opp =
        [
            SELECT Id, Application_Case__c, FEE_HELP_Forms_Submitted__c, Lost_Reason__c, OAF_Submitted__c, OfferAcceptedDate__c, OfferGeneratedDate__c, Payment_Method__c,
                RecordTypeId, Remittance_Received__c, StageName, TimetableAccepted__c, TimetableSent__c
            FROM Opportunity LIMIT 1
        ];
        oldMap.put(opp.Id, opp);

        Opportunity clonedOpp = opp.clone(true, true);
        clonedOpp.TimetableSent__c = true;

        // Act
        Test.startTest();
        ActionResult result = SObjectService.sendConfirmationEmail(new List<Opportunity>{ clonedOpp }, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        Test.stopTest();

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.status, 'Unexpected status');
        System.assertEquals(1, TestHttpCalloutMock.requests.size());
    }

    @isTest
    static void OpportunityTimetableAcceptedSetToTrue_TimetableAcceptanceConfirmationEmailSuccessfullySent()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        Map<Id, Opportunity> oldMap = new Map<Id, Opportunity>();

        Opportunity opp =
        [
            SELECT Id, Application_Case__c, FEE_HELP_Forms_Submitted__c, Lost_Reason__c, OAF_Submitted__c, OfferAcceptedDate__c, OfferGeneratedDate__c, Payment_Method__c,
                RecordTypeId, Remittance_Received__c, StageName, TimetableAccepted__c, TimetableSent__c
            FROM Opportunity LIMIT 1
        ];
        oldMap.put(opp.Id, opp);

        Opportunity clonedOpp = opp.clone(true, true);
        clonedOpp.TimetableAccepted__c = true;

        // Act
        Test.startTest();
        ActionResult result = SObjectService.sendConfirmationEmail(new List<Opportunity>{ clonedOpp }, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        Test.stopTest();

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.status, 'Unexpected status');
        System.assertEquals(1, TestHttpCalloutMock.requests.size());
    }

    @isTest
    static void OpportunityRemitanceSetToTrueWithAppInvoiceGeneratedAndRequiredSetToFlase_InvoicePaymentConfirmationEmailNotSent()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        Map<Id, Opportunity> oldMap = new Map<Id, Opportunity>();

        Opportunity opp =
        [
            SELECT Id, Application_Case__c, FEE_HELP_Forms_Submitted__c, Lost_Reason__c, OAF_Submitted__c, OfferAcceptedDate__c, OfferGeneratedDate__c, Payment_Method__c,
                RecordTypeId, Remittance_Received__c, StageName, TimetableAccepted__c, TimetableSent__c
            FROM Opportunity LIMIT 1
        ];
        oldMap.put(opp.Id, opp);

        Opportunity clonedOpp = opp.clone(true, true);
        clonedOpp.Remittance_Received__c = true;

        // Act
        Test.startTest();
        ActionResult result = SObjectService.sendConfirmationEmail(new List<Opportunity>{ clonedOpp }, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        Test.stopTest();

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.status, 'Unexpected status');
        System.assertEquals(0, TestHttpCalloutMock.requests.size());
    }

    @isTest
    static void OpportunityRemitanceSetToTrueWithAppInvoiceGeneratedAndRequiredSetToTrue_InvoicePaymentConfirmationEmailSuccessfullySent()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Map<Id, Opportunity> oldMap = new Map<Id, Opportunity>();

        Case appCase = [SELECT Id, Invoice_Required__c, Invoice_Generated__c FROM Case LIMIT 1];
        ContentVersion contentVersion = TestDataContentVersionMock.create(user, 'John INV Smith.txt', 'John INV Smith', appCase.Id);
        System.runAs(user)
        {
            appCase.Invoice_Generated__c = true;
            update appCase;
        }

        Opportunity opp =
        [
            SELECT Id, Application_Case__c, FEE_HELP_Forms_Submitted__c, Lost_Reason__c, OAF_Submitted__c, OfferAcceptedDate__c, OfferGeneratedDate__c, Payment_Method__c,
                RecordTypeId, Remittance_Received__c, StageName, TimetableAccepted__c, TimetableSent__c
            FROM Opportunity LIMIT 1
        ];
        opp.Payment_Method__c = 'Mixed';
        System.runAs(UserService.getByAlias(Constants.USER_TESTSETUP))
        {
            update opp;
        }
        oldMap.put(opp.Id, opp);

        Opportunity clonedOpp = opp.clone(true, true);
        clonedOpp.Remittance_Received__c = true;

        // Act
        Test.startTest();
        ActionResult result = SObjectService.sendConfirmationEmail(new List<Opportunity>{ clonedOpp }, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        Test.stopTest();

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.status, 'Unexpected status');
        System.assertEquals(1, TestHttpCalloutMock.requests.size());
    }

    @isTest
    static void OpportunityFEEHELPFormsSubmittedSetToTrueAppCaseFeeHelpDetailsNotRequired_FEEHELPDetailsConfirmationEmailNotSent()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        Map<Id, Opportunity> oldMap = new Map<Id, Opportunity>();

        Opportunity opp =
        [
            SELECT Id, Application_Case__c, FEE_HELP_Forms_Submitted__c, Lost_Reason__c, OAF_Submitted__c, OfferAcceptedDate__c, OfferGeneratedDate__c, Payment_Method__c,
                RecordTypeId, Remittance_Received__c, StageName, TimetableAccepted__c, TimetableSent__c
            FROM Opportunity LIMIT 1
        ];
        oldMap.put(opp.Id, opp);

        Opportunity clonedOpp = opp.clone(true, true);
        clonedOpp.FEE_HELP_Forms_Submitted__c = true;

        // Act
        Test.startTest();
        ActionResult result = SObjectService.sendConfirmationEmail(new List<Opportunity>{ clonedOpp }, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        Test.stopTest();

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.status, 'Unexpected status');
        System.assertEquals(0, TestHttpCalloutMock.requests.size());
    }

    @isTest
    static void OpportunityFEEHELPFormsSubmittedSetToTrueAppCaseFeeHelpDetailsRequired_FEEHELPDetailsConfirmationEmailSuccessfullySent()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        Map<Id, Opportunity> oldMap = new Map<Id, Opportunity>();

        Opportunity opp =
        [
            SELECT Id, Application_Case__c, FEE_HELP_Forms_Submitted__c, Lost_Reason__c, OAF_Submitted__c, OfferAcceptedDate__c, OfferGeneratedDate__c, Payment_Method__c,
                RecordTypeId, Remittance_Received__c, StageName, TimetableAccepted__c, TimetableSent__c
            FROM Opportunity LIMIT 1
        ];

        opp.Payment_Method__c = 'Mixed';
        System.runAs(UserService.getByAlias(Constants.USER_TESTSETUP))
        {
            update opp;
        }

        oldMap.put(opp.Id, opp);

        Opportunity clonedOpp = opp.clone(true, true);
        clonedOpp.FEE_HELP_Forms_Submitted__c = true;

        // Act
        Test.startTest();
        ActionResult result = SObjectService.sendConfirmationEmail(new List<Opportunity>{ clonedOpp }, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        Test.stopTest();

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.status, 'Unexpected status');
        System.assertEquals(1, TestHttpCalloutMock.requests.size());
    }

    @isTest
    static void ProgressionOpportunityOAFSubmittedSetToTrue_NoApplicationConfirmationEmailSent()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        Map<Id, Opportunity> oldMap = new Map<Id, Opportunity>();
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Campaign campaign = TestDataCampaignMock.create(user, Date.today().addDays(56), 'Domestic', 'MBA', true);
        TestDataCampaignMock.createRolloverCampaignMemberStatus(user);
        
        Contact contact = TestDataContactMock.createRolloverStudent(user, 'A00195466586');
        contact.Next_Followup_Date__c = Date.today().addDays(1);
        update contact;

        Opportunity opp = TestDataOpportunityMock.createProgression(user, contact, campaign);

        opp.OAF_Submitted__c = true;
        oldMap.put(opp.Id, opp);

        Opportunity clonedOpp = opp.clone(true, true);
        clonedOpp.OAF_Submitted__c = false;

        // Act
        Test.startTest();
        ActionResult result = SObjectService.sendConfirmationEmail(new List<Opportunity>{ clonedOpp }, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        Test.stopTest();

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.status, 'Unexpected status');
        System.assertEquals(0, TestHttpCalloutMock.requests.size());
    }

    @isTest
    static void CaseOfferVettingCompleteSet_LOOAcceptanceRequestEmailSuccessfullySent()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        Map<Id, Case> oldMap = new Map<Id, Case>();

        Case appCase =
        [
            SELECT Id, CHESSN_Balance_Notes__c, FEE_HELP_Processed__c, Invoice_Generated__c, Invoice_Processed__c, Invoice_Required__c, OfferGeneratedDate__c,
                Opportunity__c, Payment_Complete__c, Payment_Method__c, RecordTypeId, Vetting_Complete__c
            FROM Case LIMIT 1
        ];

        oldMap.put(appCase.Id, appCase);

        Case clonedAppCase = appCase.clone(true, true);
        clonedAppCase.Vetting_Complete__c = true;

        // Act
        Test.startTest();
        ActionResult result = SObjectService.sendConfirmationEmail(new List<Case>{ clonedAppCase }, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        Test.stopTest();

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.status, 'Unexpected status');
        System.assertEquals(1, TestHttpCalloutMock.requests.size());
    }

    @isTest
    static void CaseInvoiceGeneratedSetToTrue_InvoicePaymentRequestEmailSuccessfullySent()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        Map<Id, Case> oldMap = new Map<Id, Case>();

        Opportunity opp = [SELECT Id, Payment_Method__c FROM Opportunity LIMIT 1];
        opp.Payment_Method__c = 'Mixed';
    
        System.runAs(UserService.getByAlias(Constants.USER_TESTSETUP))
        {
            update opp;
        }

        Case appCase =
        [
            SELECT Id, CHESSN_Balance_Notes__c, FEE_HELP_Processed__c, Invoice_Generated__c, Invoice_Processed__c, Invoice_Required__c, OfferGeneratedDate__c,
                Opportunity__c, Payment_Method__c, RecordTypeId, Vetting_Complete__c
            FROM Case LIMIT 1
        ];

        oldMap.put(appCase.Id, appCase);

        Case clonedAppCase = appCase.clone(true, true);
        clonedAppCase.Invoice_Generated__c = true;

        // Act
        Test.startTest();
        ActionResult result = SObjectService.sendConfirmationEmail(new List<Case>{ clonedAppCase }, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        Test.stopTest();

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.status, 'Unexpected status');
        System.assertEquals(1, TestHttpCalloutMock.requests.size());
    }

    @isTest
    static void CaseWelcomeLetterSentSetToTrue_WelcomeToAIBEmailSuccessfullySent()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createProspectHot(user, 'A0012452123');
        Opportunity opp = TestDataOpportunityMock.createApplyingAcquisition(user, contact);
        Case appCase = TestDataCaseMock.createApplication(user, contact, opp);

        System.runAs(user)
        {
            opp.Application_Case__c = appCase.Id;
            update opp;
        }

        Map<Id, Case> oldMap = new Map<Id, Case>();

        Case updatedCase =
        [
            SELECT Id, Welcome_Letter_Sent__c
            FROM Case 
            WHERE Opportunity__c = :opp.Id LIMIT 1
        ];

        oldMap.put(updatedCase.Id, updatedCase);
        updatedCase.Welcome_Letter_Sent__c = true;
        update updatedCase;

        Case updatedAppCase =
        [
            SELECT Id, CHESSN_Balance_Notes__c, FEE_HELP_Processed__c, Invoice_Generated__c, Invoice_Processed__c, Invoice_Required__c, OfferGeneratedDate__c,
                Opportunity__c, Payment_Complete__c, Payment_Method__c, RecordTypeId, Vetting_Complete__c, Timetable_Processed__c, Welcome_Letter_Sent__c
            FROM Case WHERE Id= :updatedCase.Id LIMIT 1
        ];

        // Act
        Test.startTest();
        ActionResult result = SObjectService.sendConfirmationEmail(new List<Case>{ updatedAppCase }, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        Test.stopTest();

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.status, 'Unexpected status');
        System.assertEquals(1, TestHttpCalloutMock.requests.size());
    }

    @isTest
    static void CaseAwardVettingCompleteValueSet_LOOAcceptanceRequestEmailNotSent()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        Map<Id, Case> oldMap = new Map<Id, Case>();
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = [SELECT Id FROM Contact LIMIT 1];
        Case awardCase = TestDataCaseMock.createAwardCase(user, contact);

        oldMap.put(awardCase.Id, awardCase);

        Case clonedAwardCase = awardCase.clone(true, true);
        clonedAwardCase.Vetting_Complete__c = true;

        // Act
        Test.startTest();
        ActionResult result = SObjectService.sendConfirmationEmail(new List<Case>{ clonedAwardCase }, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        Test.stopTest();

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.status, 'Unexpected status');
        System.assertEquals(0, TestHttpCalloutMock.requests.size());
    }

    @isTest
    static void InternarMIRTaskCreated_ApplicationMIRRequestEmailNotSent()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());

        Case appCase = [SELECT Id FROM Case LIMIT 1];
        Contact contact = [SELECT Id FROM Contact LIMIT 1];
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);

        Task internalMIRTask = TestDataTaskMock.createMIRTask(user, contact.Id, appCase.Id, 'MIR from Test', 'M-10 Other (please provide in Comments field)');

        // Act
        Test.startTest();
        ActionResult result = SObjectService.sendConfirmationEmail(new List<Task>{ internalMIRTask }, null, Enums.TriggerContext.AFTER_INSERT);
        Test.stopTest();

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.status, 'Unexpected status');
        System.assertEquals(0, TestHttpCalloutMock.requests.size());
    }

    @isTest
    static void ExternalMIRTaskCreated_ApplicationMIRRequestEmailSuccessfullySent()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());

        Case appCase = [SELECT Id FROM Case LIMIT 1];
        Contact contact = [SELECT Id FROM Contact LIMIT 1];
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);

        Task internalMIRTask = TestDataTaskMock.createMIRTask(user, contact.Id, appCase.Id, 'MIR from Test', 'M-3 Missing Application Document (Required)');

        // Act
        Test.startTest();
        ActionResult result = SObjectService.sendConfirmationEmail(new List<Task>{ internalMIRTask }, null, Enums.TriggerContext.AFTER_INSERT);
        Test.stopTest();

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.status, 'Unexpected status');
        System.assertEquals(1, TestHttpCalloutMock.requests.size());
    }

    @isTest
    static void ContactSingleSignOnStatusSetToTrueAndFeeHelpDetailsRequired_FEEHELPDetailsRequestEmailSuccessfullySent()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        Map<Id, Contact> oldMap = new Map<Id, Contact>();

        Contact contact = [SELECT Id, RecordTypeId, SingleSignOnStatusViewed__c FROM Contact LIMIT 1];
        oldMap.put(contact.Id, contact);
        Opportunity opp = [SELECT Id, Payment_Method__c FROM Opportunity LIMIT 1];

        System.runAs(UserService.getByAlias(Constants.USER_TESTSETUP))
        {
            opp.Payment_Method__c = 'FEE-HELP';
            update opp;
        }

        Contact clonedContact = contact.clone(true, true);
        clonedContact.SingleSignOnStatusViewed__c = true;

        // Act
        Test.startTest();
        ActionResult result = SObjectService.sendConfirmationEmail(new List<Contact>{ clonedContact }, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        Test.stopTest();

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.status, 'Unexpected status');
        System.assertEquals(1, TestHttpCalloutMock.requests.size());
    }

    @isTest
    static void ContactSingleSignOnStatusSetToTrueAndFeeHelpDetailsNotRequired_FEEHELPDetailsRequestEmailNotSent()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        Map<Id, Contact> oldMap = new Map<Id, Contact>();

        Contact contact = [SELECT Id, RecordTypeId, SingleSignOnStatusViewed__c FROM Contact LIMIT 1];
        oldMap.put(contact.Id, contact);

        Contact clonedContact = contact.clone(true, true);
        clonedContact.SingleSignOnStatusViewed__c = true;

        // Act
        Test.startTest();
        ActionResult result = SObjectService.sendConfirmationEmail(new List<Contact>{ clonedContact }, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        Test.stopTest();

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.status, 'Unexpected status');
        System.assertEquals(0, TestHttpCalloutMock.requests.size());
    }
}