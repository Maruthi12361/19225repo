@isTest
public class UserUpdateLightningPreferenceBatchTests
{
    @testSetup
    static void setup()
    {
        TestUtilities.setupHEDA();
    }

    @isTest
    static void RunBatchOnNonAdmin_SetsUserLightningPreferenceToTrue()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_NON_ADMIN);
        user.UserPreferencesLightningExperiencePreferred = false;
        update user;

        // Act
        Test.startTest();
        Database.executeBatch(new UserUpdateLightningPreferenceBatch());
        Test.stopTest();

        // Assert
        user = UserService.getByAlias(Constants.USER_NON_ADMIN);
        System.assertEquals(true, user.UserPreferencesLightningExperiencePreferred);
    }

    @isTest
    static void RunBatchOnAdmin_DoesNotSetUserLightningPreferenceToTrue()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        user.UserPreferencesLightningExperiencePreferred = false;
        update user;

        // Act
        Test.startTest();
        Database.executeBatch(new UserUpdateLightningPreferenceBatch());
        Test.stopTest();

        // Assert
        user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        System.assertEquals(false, user.UserPreferencesLightningExperiencePreferred);
    }
}