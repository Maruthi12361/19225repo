@isTest
public class TermUpdateDatesBatchTests
{
    @testSetup 
    static void setup() 
    {
        // Arrange
        TestUtilities.setupHEDA();
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Account account = TestDataAccountMock.createDepartmentBusiness(user);
        hed__Term__c term = TestDataTermMock.create(user, account.Id, Date.today().addDays(21));
        TestDataSubjectOfferingMock.createOffering(user, 'TEST001', 'Test Subject One', term, term.FirstSubjectStartDate__c);
        TestDataSubjectOfferingMock.createOffering(user, 'TEST002', 'Test Subject Two', term, Date.today().addDays(14));
        term = TestDataTermMock.create(user, account.Id, Date.today().addDays(77));
        TestDataSubjectOfferingMock.createOffering(user, 'TEST003', 'Test Subject One', term, Date.today().addDays(70));
        TestDataSubjectOfferingMock.createOffering(user, 'TEST004', 'Test Subject Two', term, term.FirstSubjectStartDate__c);
        TestDataTermMock.create(user, account.Id, Date.today().addDays(133));
        database.executebatch(new TermUpdateStatusBatch());
    }
    
    @isTest 
    static void SubjectOfferingsWithEarlierDates_PopulatestheEarliestDatesOnParentTerm()
    {
        // Act        
        Test.startTest();
        Database.executeBatch(new TermUpdateDatesBatch());
        Test.stopTest(); 
        
        // Assert
        List<hed__Term__c> terms = 
            [
                SELECT Id,FirstAdministrationDate__c,FirstCensusDate__c,FirstSubjectStartDate__c
                FROM hed__Term__c
                ORDER BY FirstSubjectStartDate__c
            ];
        System.assertEquals(Date.today().addDays(-7), terms[0].FirstAdministrationDate__c);
        System.assertEquals(Date.today().addDays(14), terms[0].FirstSubjectStartDate__c);
        System.assertEquals(Date.today().addDays(25), terms[0].FirstCensusDate__c);
        System.assertEquals(Date.today().addDays(49), terms[1].FirstAdministrationDate__c);
        System.assertEquals(Date.today().addDays(70), terms[1].FirstSubjectStartDate__c);
        System.assertEquals(Date.today().addDays(81), terms[1].FirstCensusDate__c);
        System.assertEquals(Date.today().addDays(112), terms[2].FirstAdministrationDate__c);
        System.assertEquals(Date.today().addDays(133), terms[2].FirstSubjectStartDate__c);
        System.assertEquals(Date.today().addDays(144), terms[2].FirstCensusDate__c);
    }
}