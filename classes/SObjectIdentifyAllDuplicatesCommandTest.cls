@IsTest
public class SObjectIdentifyAllDuplicatesCommandTest
{
    @testSetup 
    public static void setup()
    {
        TestUtilities.setupHEDA();
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        TestDataContactMock.createProspect(user, 'John', 'Smith', 'Robert', '04568897889', 'johnSmith@email.com');
    }

    @isTest
    static void TwoUniqueContacts_NoDuplicateRecordSetsCreated()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact contact = TestDataContactMock.createProspect(user, 'Mel', 'Rose', 'Katherine', '0413845787', 'mel.rose@email.com');
        List<Contact> contacts = [SELECT Id FROM Contact];

        // Act
        test.startTest();
        SObjectService.identifyAllDuplicates(contacts);
        List<DuplicateRecordItem> duplicateItems = [SELECT Id, RecordId FROM DuplicateRecordItem WHERE RecordId = :contact.Id];
        test.stopTest();

        // Assert
        System.assertEquals(0, duplicateItems.size(), 'Contact shouldn\'t have any duplicate record items');
    }

    @isTest
    static void TwoContactsWithSamePrimaryEmail_DuplicateRecordSetsCreated()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact contact = TestDataContactMock.createProspect(user, 'John', 'Smith', 'Robert', '04568897889', 'johnSmith@email.com');
        List<Contact> contacts = [SELECT Id FROM Contact];

        // Act
        test.startTest();
        SObjectService.identifyAllDuplicates(contacts);
        List<DuplicateRecordItem> duplicateItems = [SELECT Id, RecordId FROM DuplicateRecordItem WHERE RecordId = :contact.Id];
        test.stopTest();

        // Assert
        Integer expectedResult = 1;
        List<DuplicateRule> contactRules = [SELECT Id, IsActive, DeveloperName FROM DuplicateRule WHERE IsDeleted = false AND IsActive = true AND (DeveloperName = 'Duplicate_Contacts' OR DeveloperName = 'Contact_Mobile_Phone')];

        if (contactRules.size() == 0)
            expectedResult = 0;

        System.assertEquals(expectedResult, duplicateItems.size(), 'Contact should only have one duplicate record item');
    }
}