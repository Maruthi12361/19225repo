public class ContactCleanupRolloverDetailBatch extends BaseBatchCommand
{
    public String cerebroIDs;
    public String query = 
        'SELECT Id, Rollover_Status__c, Cerebro_Student_ID__c, Current_Course_Status__c, Next_Followup_Date__c, ' +
            'Rollover_Lost_Count__c, Rollover_Count__c, Current_Rollover_Campaign__c, ' +
            '(' +
                'SELECT Id,status,CampaignId,Campaign.Type,Campaign.Status,Campaign.StartDate '+
                'FROM CampaignMembers ' +
                'WHERE Campaign.Type = \'Rollover\' ' +
                'ORDER BY CreatedDate DESC'+
            ') ' + 
        'FROM Contact ' +
        'WHERE (Current_Course_Status__c = null OR Next_Followup_Date__c = null) AND Current_Rollover_Campaign__c <> null';
    
    
    public ContactCleanupRolloverDetailBatch()
    {
        
    }
    
    public ContactCleanupRolloverDetailBatch(String stringParam) 
    {
        if (stringParam != null && String.isNotBlank(stringParam))
        {
            query += ' and Cerebro_Student_ID__c in (' + stringParam + ')';
            cerebroIDs = stringParam;
        }
    }
    
    
    public override Database.QueryLocator query(Database.BatchableContext BC)
    {
        return Database.getQueryLocator(query);
    }
    
    
    public override void command(Database.BatchableContext BC, List<sObject> scope)
    {
        List<Contact> contacts = (List<Contact>) scope;
        List<CampaignMember> campaignMembers = new List<CampaignMember>();
        List<CampaignMember> deletedCampaignMembers = new List<CampaignMember>();
        
        for (Contact contact : contacts) 
        {
            CampaignMember currentCampaignMember;
            
            for (CampaignMember campaignMember : contact.CampaignMembers)
            {                
                if (contact.Current_Rollover_Campaign__c == campaignMember.CampaignId)
                {
                    currentCampaignMember = campaignMember;
                    break;
                }
            }
            
            if (currentCampaignMember != null)
            {
                if (currentCampaignMember.Status == RolloverConstantsDefinition.ST_OPEN && (currentCampaignMember.Campaign.StartDate > Date.today() || contact.Current_Course_Status__c == 'Ongoing'))
                {
                    Log.info('ContactCleanupRolloverDetailBatch', 'execute', 'Delete the current CampaignMember if it has not started or the student is All Stages Scheduled: ' + currentCampaignMember.CampaignId);
                    deletedCampaignMembers.add(currentCampaignMember);
                    
                    if (contact.Rollover_Count__c != null && contact.Rollover_Count__c > 0)
                    {
                        contact.Rollover_Count__c--;
                    }
                }                
                else if (currentCampaignMember.Status == RolloverConstantsDefinition.ST_OPEN || 
                        currentCampaignMember.Status == RolloverConstantsDefinition.ST_MASSEMAIL || 
                        currentCampaignMember.Status == RolloverConstantsDefinition.ST_CONTACTING)
                {
                    Log.info('ContactCleanupRolloverDetailBatch', 'execute', 'Set the current CampaignMember to Closed Lost if it is active but no case created: ' + currentCampaignMember.CampaignId);
                    currentCampaignMember.Status = RolloverConstantsDefinition.ST_CLOSED_LOST;
                    
                    if (contact.Rollover_Lost_Count__c == null)
                        contact.Rollover_Lost_Count__c = 1;
                    else
                    	contact.Rollover_Lost_Count__c++;
                    
                    campaignMembers.add(currentCampaignMember);
                }
            }

            if (contact.Current_Course_Status__c != RolloverConstantsDefinition.CS_ONGOING)
                contact.Rollover_Status__c = null;

            contact.Current_Rollover_Campaign__c = null;
            contact.Rollover_Call_Count__c = 0;
            contact.Rollover_Email_Count__c = 0;
            contact.Rollover_Contact_History__c = null;
            contact.Rollover_Deferred_Reason__c = null;
            contact.Rollover_Deferred_Reason_Other__c = null;
        }
        
        if (campaignMembers.size() > 0) 
            update campaignMembers;
        
        if (deletedCampaignMembers.size() > 0) 
            delete deletedCampaignMembers; 
        
        if (contacts.size() > 0) 
            update contacts;
    }
    
    public override void finalise(Database.BatchableContext BC)
    {
        ContactResetRolloverSegmentationBatch batch;
        
        if(cerebroIDs == null)
            batch = new ContactResetRolloverSegmentationBatch();
        else
            batch = new ContactResetRolloverSegmentationBatch(cerebroIDs);
        
        database.executebatch(batch);
    }
}