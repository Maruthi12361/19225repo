public class EncryptionUtil{

	static blob iv = blob.valueOf('@1|3P@55wrDwK3y#');
	public static string Encrypt(string key,string toen){
		Blob cryptoKey = Blob.valueOf(key);
		Blob encryptedData = Crypto.encrypt('AES256', cryptoKey, iv,Blob.valueOf(toen));
		return EncodingUtil.urlEncode(EncodingUtil.base64Encode(encryptedData),'UTF-8');
	}

	public static string Encrypt(string toen){
		return Encrypt(System.Label.Encryption_Key,toen);
	}

	public static string Decrypt(string tode){
		return Decrypt(System.Label.Encryption_Key,tode);
	}


	public static string Decrypt(string key,string tode){
		Blob cryptoKey = Blob.valueOf(key);

		tode = EncodingUtil.urlDecode(tode,'UTF-8');
		blob dat= EncodingUtil.base64decode(tode);
		Blob decryptedData = Crypto.decrypt('AES256', cryptoKey,iv, dat);
		return decryptedData.toString();
	}
}