public class ContactUpdateRolloverDetailBatch extends BaseBatchCommand
{
    public String query =
        'SELECT Id,hed__Contact__c,hed__Start_Date__c,Status__c,FeeHelpEligibilityType__c,NextFollowUpDate__c,CurrentStageStatus__c ' +
        'FROM hed__Program_Enrollment__c ' +
        'WHERE Status__c = \'Ongoing\' AND hed__Account__r.Program__c IN (\'MBA\', \'GCM\', \'GDM\') ';

    public String cerebroIDs;

    public ContactUpdateRolloverDetailBatch()
    {
    }

    public ContactUpdateRolloverDetailBatch(String stringParam)
    {
        if (stringParam != null && String.isNotBlank(stringParam))
        {
            cerebroIDs = stringParam;
        }
    }

    public override Database.QueryLocator query(Database.BatchableContext BC)
    {
        if (cerebroIDs != null)
        {
            query += ' AND hed__Contact__r.Cerebro_Student_ID__c IN (' + cerebroIDs + ')';
        }
        query += ' ORDER BY hed__Contact__r.Cerebro_Student_ID__c DESC, CerebroID__c DESC';

        return Database.getQueryLocator(query);
    }

    public override void command(Database.BatchableContext BC, List<sObject> scope)
    {
        ActionResult result = ContactService.updateRolloverDetails(scope);

        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);
    }

    public override void finalise(Database.BatchableContext BC)
    {
        ContactCleanupRolloverDetailBatch batch;

        if (cerebroIDs == null)
            batch = new ContactCleanupRolloverDetailBatch();
        else
            batch = new ContactCleanupRolloverDetailBatch(cerebroIDs);

        database.executebatch(batch);
    }
}