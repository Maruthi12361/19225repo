public class TestDataContactMock
{
    private static Contact faculty;

    public static Contact createStudent(User user)
    {
        return createStudent(user, 'A00' + System.now().format('HHmmssSS'), true);
    }

    public static Contact createStudent(User user, String StudentID)
    {
        return createStudent(user, studentId, true);
    }

    public static Contact createStudent(User user, String StudentID, String email)
    {
        return createStudent(user, studentId, email, true, true);
    }

    public static Contact createStudent(User user, String studentID, Boolean saveRecord)
    {
        return createStudent(user, studentId, true, true);
    }

    public static Contact createStudent(User user, String studentID, Boolean saveRecord, Boolean futureFollowUp)
    {
        return createStudent(user, studentID, 'joanne.david@gmail.com', saveRecord, futureFollowUp);
    }

    public static Contact createStudent(User user, String studentID, String email, Boolean saveRecord, Boolean futureFollowUp)
    {
        Contact contact = new Contact();
        contact.RecordTypeId = RecordTypeUtils.getRecordTypeId(Constants.RECORDTYPE_CONTACT_STUDENT, SObjectType.Contact);

        if (user != null)
            contact.OwnerId = user.Id;

        // Pertinent details
        contact.FirstName = 'Forrest';
        contact.LastName = 'Gump';
        contact.Student_type__c = 'OLD';
        contact.hed__Gender__c = 'Male';
        contact.Language_Spoken_at_Home__c = 'English';
        contact.hed__Dual_Citizenship__c = 'Australia';
        contact.hed__Country_of_Origin__c = 'Australia';
        contact.WelcomeProspectEmailSent__c = true;
        contact.WelcomeProspectSMSSent__c = true;

        // Enrolment details
        contact.Program__c = 'MBA';
        contact.Portfolio__c = 'Domestic';
        contact.Cerebro_Student_ID__c = StudentID;
        contact.Status__c = Constants.STATUS_PROGRESSION;
        contact.Current_Course_Status__c = 'Ongoing';
        contact.Passed_Subjects__c = 11;
        contact.Incomplete_Subjects__c = 1;
        contact.Required_Subjects__c = 12;
        contact.MC_Subjects__c = 4;
        contact.MBA_Course_Name__c = 'MBA';
        contact.AtRisk__c = 'Yes';
        contact.Course_Start_Date__c = Date.valueOf('2016-01-01');

        // Physical location details
        contact.MailingStreet = '37 Currie Street';
        contact.MailingCity = 'Adelaide';
        contact.MailingState = 'SA';
        contact.MailingCountry='Australia';
        contact.MailingPostalcode = '5000';
        contact.Permanent_Home_Country__c = 'Canada';

        // Contact contact
        contact.HomePhone = '0408815279';
        contact.hed__PreferredPhone__c = 'Home';
        contact.MobilePhone = '61408815279';
        contact.OtherPhone = '041345';
        contact.PersonalEmail__c = email;
        contact.hed__Preferred_Email__c = 'Personal Email';
        contact.BirthDate = Date.newInstance(1975, 12, 5);

        // FEE HELP details
        contact.FeeHelp_Eligibility__c = 'FHE';
        contact.Last_Used_Funding_Source__c = 'Student';
        contact.FeeSet__c = 'Test';
        contact.FeeHelp_Approved__c = true;

        // Rollover details
        contact.Rollover_Lost_Count__c = null;
        contact.Rollover_Deferred_Reason_Other__c = 'Enrolled';

        if (futureFollowUp)
        {
            contact.Next_Followup_Date__c = System.now().date().addYears(1);
            contact.Manual_Followup_Date__c = null;
        }
        else
        {
            contact.Next_Followup_Date__c = Date.valueOf('2017-08-04');
            contact.Manual_Followup_Date__c = Date.valueOf('2017-9-22');
        }

        if (user != null)
        {
            System.RunAs(user)
            {
                insert contact;
            }
        }
        else
        {
            insert contact;
        }

        Contact con = [SELECT Id, Phone, Email FROM Contact WHERE Email=: contact.PersonalEmail__c][0];
        contact.Phone = con.Phone;
        contact.Email = con.Email;

        return contact;
    }
        
    public static Contact createProspect(User user, String firstName, String lastName, String middleName, String mobilePhone, String email, String status, String studentAibId, String mailingCountry, String leadSource, Boolean welcomeProspectEmailSent, Boolean welcomeSMSSent)
    {
        Contact contact = new Contact
        (
            FirstName = firstName,
            LastName = lastName,
            MiddleName = middleName,
            MobilePhone = mobilePhone,
            HomePhone = '+61293534846',
            hed__PreferredPhone__c = 'Home',
            dncau__Phone_DNC_Status__c = Constants.DNC_CAN_CALL_STATUS,
            dncau__Phone_DNC_Checked__c = DateTime.newInstance(2018, 02, 15, 12, 0, 0),
            dncau__MobilePhone_DNC_Status__c = Constants.DNC_DO_NOT_CALL_STATUS,
            dncau__MobilePhone_DNC_Checked__c = DateTime.newInstance(2018, 02, 15, 12, 0, 0),
            OtherPhone = '+61417015977',
            dncau__OtherPhone_DNC_Status__c = Constants.DNC_CAN_CALL_STATUS,
            dncau__OtherPhone_DNC_Checked__c = DateTime.newInstance(2018, 02, 15, 12, 0, 0),
            Student_type__c = 'OLD',
            Status__c = status,
            PersonalEmail__c = email,
            hed__Preferred_Email__c = 'Personal Email',
            RecordTypeId = RecordTypeUtils.getRecordTypeId(Constants.RECORDTYPE_CONTACT_PROSPECT, SObjectType.Contact),
            Cerebro_Student_ID__c = studentAibId,
            MailingCountry = mailingCountry,
            EncryptedId__c = 'jqN%2BS6XO0DNgIhcxiW%2FDh4eMv9g4fCbR1fw0hsjDG8c%3D',
            WelcomeProspectEmailSent__c = welcomeProspectEmailSent,
            WelcomeProspectSMSSent__c = welcomeSMSSent,
            Program__c = 'MBA',
            Portfolio__c = 'Domestic'
        );

        if (String.isNotEmpty(leadSource))
            contact.LeadSource = leadSource;

        if (user != null)
            contact.OwnerId = user.Id;

         Log.info('TestDataContactMock', 'createProspect', 'Inserting Contact: FirstName: {0} LastName: {1} Email: {2} Mobile: {3} ',
         new String[]{contact.firstName, contact.lastName, contact.PersonalEmail__c, contact.MobilePhone});

        if (user != null)
        {
            System.RunAs(user)
            {
                insert contact;
            }
        }
        else
        {
            insert contact;
        }

        Contact con = [SELECT Id, Phone, Email FROM Contact WHERE Email=: contact.PersonalEmail__c][0];
        contact.Phone = con.Phone;
        contact.Email = con.Email;

        return contact;
    }

    public static Contact createProspect(User user, String firstName, String lastName, String middleName, String mobilePhone, String email, String status, String studentAibId, String mailingCountry, String leadSource)
    {
        return createProspect(user, firstName, lastName, middleName, mobilePhone, email, status, studentAibId, mailingCountry, leadSource, true, true);
    }

    public static Contact createProspect(User user, String mailingCountry)
    {
        return createProspect(user, 'John', 'Smith', '', '0405558798', 'john.smith@email.com', Constants.STATUS_COLD, 'A001588974', mailingCountry, '');
    }

    public static Contact createProspect(User user, String email, String leadSource)
    {
        return createProspect(user, 'John', 'Smith', '', '0405558798', email, Constants.STATUS_COLD, 'A001588974', '', leadSource);
    }

    public static Contact createProspect(User user, String mobilePhone, String email, String  status)
    {
        return createProspect(user, 'John', 'Smith', '', mobilePhone, email, status, 'A001588974', '', '');
    }

    public static Contact createProspect(User user, String firstName, String lastName, String middleName, String mobilePhone, String email)
    {
        return createProspect(user, firstName, lastName, middleName,  mobilePhone, email, Constants.STATUS_COLD, 'A00' + System.now().format('HHmmssSS'), '', '');
    }

    public static Contact createProspectCold(User user, String mobilePhone)
    {
        return createProspect(user, 'John', 'Smith', '', mobilePhone, 'john.smith@email.com', Constants.STATUS_COLD, 'A001588974', '', '');
    }

    public static Contact createProspectCold(User user, String mobilePhone, String email)
    {
        return createProspect(user, 'John', 'Smith', '', mobilePhone, email, Constants.STATUS_COLD, 'A001588974', '', '');
    }

    public static Contact createProspectCold(User user, String firstName, String lastName, String mobilePhone, String email, String aibStudentId)
    {
        return createProspect(user, firstName, lastName, '', mobilePhone, email, Constants.STATUS_COLD, aibStudentId, '', '');
    }

    public static Contact createRolloverStudent(User user, String studentID)
    {
        return createStudent(user, studentId, true, false);
    }

    public static Contact createProspectWarm(User user)
    {
        return createProspectWarm(user, true);
    }

    public static Contact createProspectWarm(User user, Boolean saveRecord)
    {
        Contact contact = new Contact();
        contact.RecordTypeId = RecordTypeUtils.getRecordTypeId(Constants.RECORDTYPE_CONTACT_PROSPECT, SObjectType.Contact);

        if (user != null)
            contact.OwnerId = user.Id;

        // Pertinent details
        contact.Status__c = Constants.STATUS_WARM;
        contact.FirstName = 'Forrest';
        contact.LastName = 'Gump';
        contact.hed__Gender__c = 'Male';
        contact.Language_Spoken_at_Home__c = 'English';
        contact.hed__Dual_Citizenship__c = 'Australia';
        contact.hed__Country_of_Origin__c = 'Australia';
        contact.WelcomeProspectEmailSent__c = true;
        contact.WelcomeProspectSMSSent__c = true;
        contact.Program__c = 'MBA';
        contact.Portfolio__c = 'Domestic';

        // Physical location details
        contact.OtherStreet = '37 Currie Street';
        contact.OtherCity = 'Adelaide';
        contact.OtherState = 'SA';
        contact.OtherPostalcode = '5000';
        contact.OtherCountry = 'Australia';
        contact.MailingStreet = '37 Currie Street';
        contact.MailingCity = 'Adelaide';
        contact.MailingState = 'SA';
        contact.MailingPostalcode = '5000';
        contact.MailingCountry = 'Australia';
        contact.OtherAddressType__c = 'Same as Mailing Address';
        contact.Permanent_Home_Country__c = 'Australia';

        // Contact contact
        contact.HomePhone = '0408815279';
        contact.hed__PreferredPhone__c = 'Home';
        contact.MobilePhone = '61408815279';
        contact.OtherPhone = '041345';
        contact.PersonalEmail__c = 'joanne.david@gmail.com';
        contact.hed__Preferred_Email__c = 'Personal Email';
        contact.BirthDate = Date.newInstance(1975, 12, 5);

        // MBA Eligibility
        contact.HighestTertiaryLevelDegree__c = 'Bachelor\'s Degree or equivalent';
        contact.WorkExperienceYears__c = 4;
        contact.ManagementExperienceYears__c = 4;

        if (user != null)
        {
            System.RunAs(user)
            {
                insert contact;
            }
        }
        else
        {
            insert contact;
        }

        Contact con = [SELECT Id, Phone, Email FROM Contact WHERE Email=: contact.PersonalEmail__c][0];
        contact.Phone = con.Phone;
        contact.Email = con.Email;

        return contact;
    }

    public static Contact createProspectHot(User user)
    {
        return createProspectHot(user, null, true);
    }

    public static Contact createProspectHot(User user, Boolean saveRecord)
    {
        return createProspectHot(user, null, saveRecord);
    }

    public static Contact createProspectHot(User user, String studentId)
    {
        return createProspectHot(user, studentId, true);
    }

    public static Contact createProspectHot(User user, String studentId, Boolean saveRecord)
    {
        Contact contact = new Contact();
        contact.RecordTypeId = RecordTypeUtils.getRecordTypeId(Constants.RECORDTYPE_CONTACT_PROSPECT, SObjectType.Contact);

        if (user != null)
            contact.OwnerId = user.Id;

        // Pertinent details
        contact.Status__c = Constants.STATUS_HOT;
        contact.FirstName = 'Forrest';
        contact.LastName = 'Gump';
        contact.hed__Gender__c = 'Male';
        contact.Language_Spoken_at_Home__c = 'English';
        contact.hed__Dual_Citizenship__c = 'Australia';
        contact.hed__Country_of_Origin__c = 'Australia';
        contact.ProgramPortfolioBackup__c = 'DMBA';
        contact.WelcomeProspectEmailSent__c = true;
        contact.WelcomeProspectSMSSent__c = true;
        contact.Preferred_Start_Date__c = StringUtils.formatDate(getNextPreferredStartDate());
        contact.Cerebro_Student_ID__c = studentId;
        contact.Program__c = 'MBA';
        contact.Portfolio__c = 'Domestic';

        // Physical location details
        contact.OtherStreet = '37 Currie Street';
        contact.OtherCity = 'Adelaide';
        contact.OtherState = 'SA';
        contact.OtherPostalcode = '5000';
        contact.OtherCountry = 'Australia';
        contact.MailingStreet = '37 Currie Street';
        contact.MailingCity = 'Adelaide';
        contact.MailingState = 'SA';
        contact.MailingPostalcode = '5000';
        contact.MailingCountry = 'Australia';
        contact.OtherAddressType__c = 'Same as Mailing Address';
        contact.PermanentAddressType__c = 'Same as Other Address';
        contact.Permanent_Home_Country__c = 'Australia';

        // Contact contact
        contact.MobilePhone = '61408815279';
        contact.OtherPhone = '+61413695887';
        contact.HomePhone = '+61413695888';
        contact.hed__PreferredPhone__c = 'Home';
        contact.hed__WorkPhone__c = '+61413695999';
        contact.Express_Consent_Exists__c = false;
        Datetime expiredDate = System.now().addMonths(3);
        contact.Express_Consent_Expires__c = expiredDate.date();
        contact.PersonalEmail__c = 'joanne.david@gmail.com';
        contact.hed__Preferred_Email__c = 'Personal Email';
        contact.hed__WorkEmail__c = 'joanne.david@gmail.com';
        contact.BirthDate = Date.newInstance(1975, 12, 5);

        // MBA Eligibility
        contact.HighestTertiaryLevelDegree__c = 'Bachelor\'s Degree or equivalent';
        contact.WorkExperienceYears__c = 4;
        contact.ManagementExperienceYears__c = 4;

        contact.ApplicationCourse__c = 'MBA';
        contact.High_School_Level__c = 'Year 12 (or equivalent)';
        contact.HighestQualificationName__c = 'Bachelor';
        contact.HighestQualificationInstitution__c = 'Uni SA';
        contact.HighestQualificationLanguage__c = 'English';
        contact.HighestQualificationCompletionYear__c = '2018';

        if (user != null)
        {
            System.RunAs(user)
            {
                insert contact;
            }
        }
        else
        {
            insert contact;
        }

        // The following if block is to fix the deployment issue related to Acquisition Opportunty/Case PBs.
        // If Acqusition Opportunity PB is inactive, then no Application Case will be created.
        // If the Opportunity PB is active but Case PB is inactive, Case does get created, but disconnected
        // from the Opportunity (Application_Case__c is null). In either situation, we need to create the missing Case
        // or re-link the Case to the Opportunity. // TODO: on fix of PB Deployment Issue
        if (user.Alias != Constants.USER_TESTSETUP)
        {
            List<Opportunity> opps = [SELECT Id, Application_Case__c FROM Opportunity WHERE Contact__c = : contact.Id AND RecordType.DeveloperName = : Constants.RECORDTYPE_OPPORTUNITY_ACQUISITION];

            if (opps.size() > 0 && opps[0].Application_Case__c == null)
            {
                Case appCase;
                List<Case> cases = [SELECT Id FROM Case WHERE ContactId = : contact.Id AND Opportunity__c = : opps[0].Id AND RecordType.DeveloperName = : Constants.RECORDTYPE_CASE_APPLICATION];

                if (cases.size() > 0)
                {
                    appCase = cases[0];
                }
                else
                {
                    appCase = TestDataCaseMock.createApplication(user, contact, opps[0]);
                }

                opps[0].Application_Case__c = appCase.Id;

                if (user != null)
                {
                    System.RunAs(user)
                    {
                        update opps[0];
                    }
                }
                else
                {
                    update opps[0];
                }
            }
        }

        Contact con = [SELECT Id, Phone, Email FROM Contact WHERE Email=: contact.PersonalEmail__c][0];
        contact.Phone = con.Phone;
        contact.Email = con.Email;

        return contact;
    }

    public static Contact createFacultyContact(User user)
    {
        if (faculty != null)
            return faculty;

        Contact contact = new Contact();
        contact.RecordTypeId = RecordTypeUtils.getRecordTypeId(Constants.RECORDTYPE_CONTACT_FACULTY, SObjectType.Contact);

        if (user != null)
            contact.OwnerId = user.Id;

        contact.FirstName = 'Online';
        contact.LastName = 'Facilitator';

        contact.MobilePhone = '61408815278';
        contact.OtherPhone = '+61413695866';
        contact.HomePhone = '+61413695624';
        contact.hed__PreferredPhone__c = 'Home';
        contact.hed__WorkPhone__c = '+61413695127';

        contact.PersonalEmail__c = 'online.facilitator@email.com';
        contact.hed__Preferred_Email__c = 'Personal Email';

        if (user != null)
        {
            System.RunAs(user)
            {
                insert contact;
            }
        }
        else
        {
            insert contact;
        }

        Contact con = [SELECT Id, Phone, Email FROM Contact WHERE Email=: contact.PersonalEmail__c][0];
        contact.Phone = con.Phone;
        contact.Email = con.Email;
        faculty = contact;

        return contact;
    }

    public static Date getNextPreferredStartDate()
    {
        Date preferredStartDate;
        Map<String, String> pickList = SObjectUtils.getPicklistValues('Contact', 'Preferred_Start_Date__c', null);

        for (String value : pickList.values())
        {
            if (value.length() == 10)
            {
                String year = value.right(4);
                String month = value.mid(3, 2);
                String day = value.left(2);
                preferredStartDate = Date.valueOf(year + '-' + month + '-' + day);
                break;
            }
        }

        return preferredStartDate;
    }
}