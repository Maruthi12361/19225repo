public class CaseCalculateFirstEmailResponseCommand extends BaseTriggerCommand
{
    public override List<SObject> validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        List<Case> cases = new List<Case>();

        if (triggerContext == Enums.TriggerContext.BEFORE_INSERT || triggerContext == Enums.TriggerContext.BEFORE_UPDATE)
        {
            for (Case c : (List<Case>)items)
            {
                if (triggerContext == Enums.TriggerContext.BEFORE_INSERT)
                {
                    if (c.First_Email_Student_Date_Time__c != null && c.First_Email_Date_Time__c != null)
                        cases.add(c);
                }
                else if (triggerContext == Enums.TriggerContext.BEFORE_UPDATE)
                {
                    Case oldCase = (Case)oldMap.get(c.id);

                    if (c.First_Email_Student_Date_Time__c != null && c.First_Email_Date_Time__c != null && 
                        (c.First_Email_Student_Date_Time__c != oldCase.First_Email_Student_Date_Time__c || 
                         c.First_Email_Date_Time__c != oldCase.First_Email_Date_Time__c))
                        cases.add(c);
                }
            }
        }

        return cases;
    }

    public override ActionResult command(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        for (Case c: (List<Case>)items)
        {
            Decimal hours = DateTimeUtils.getTimeAsHours(DateTimeUtils.getBusinessElapsedTime(c.First_Email_Student_Date_Time__c, c.First_Email_Date_Time__c));

            if (hours != c.First_Email_Response_Time__c)
                c.First_Email_Response_Time__c = hours;
        }

        return new ActionResult(ResultStatus.SUCCESS, new List<String>{'Successfully set First Email Response Time'}); 
    }
}