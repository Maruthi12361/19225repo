public class ViewModelsMail
{
    public class Mail implements Interfaces.IViewModel
    {
        public Recipient from_x { get; set; }
        public String subject { get; set; }
        public List<Personalization> personalizations { get; set; }
        public List<Content> content { get; set; }
        public Recipient reply_to { get; set; }

        public Mail(String toAddress, String toName, String mailSubject, String fromAddress, String fromName, String bodyText, String bodyHtml, List<Recipient> ccEmail)
        {
            setupEmail(fromAddress, fromName, mailSubject, bodyText, bodyHtml);
            Recipient to = new Recipient(toName, toAddress);
            Personalization personalization = new Personalization();
            personalization.to = new List<Recipient> { to };

            if (ccEmail != null)
                personalization.cc = ccEmail;

            personalizations = new List<Personalization> { personalization };
        }

        public Mail(List<Recipient> toList, String mailSubject, String fromAddress, String fromName, String bodyText, String bodyHtml)
        {
            setupEmail(fromAddress, fromName, mailSubject, bodyText, bodyHtml);
            Personalization personalization = new Personalization();
            personalization.to = toList;
            personalizations = new List<Personalization> { personalization };
        }

        private void setupEmail(String fromAddress, String fromName, String mailSubject, String bodyText, String bodyHtml)
        {
            from_x = new Recipient(fromName, fromAddress);
            subject = mailSubject;
            content = new List<Content>();

            if (String.isNotBlank(bodyText))
                content.Add(new Content(bodyText, 'text/plain'));

            if (String.isNotBlank(bodyHtml))
                content.Add(new Content(bodyHtml, 'text/html'));
        }
    }

    public class Personalization implements Interfaces.IViewModel
    {
        public List<Recipient> to { get; set; }
        public List<Recipient> cc { get; set; }
        public List<Recipient> bcc { get; set; }
        public Map<String, String> headers { get; set; }
    }

    public class Recipient implements Interfaces.IViewModel
    {
        public String name { get; set; }
        public String email { get; set; }

        public Recipient(String displayName, String emailAddress)
        {
            name = displayName;
            email = emailAddress;
        }
    }

    public class Content implements Interfaces.IViewModel
    {
        public String type;
        public String value;

        public Content(String body, String bodyType)
        {
            type = bodyType;
            value = body;
        }
    }

    public class WelcomeEmail implements Interfaces.IViewModel
    {
        public String EventId { get; set; }
        public String Email { get; set; }
        public String ContactId { get; set; }
        public String EmailBodyText { get; set; }
        public String EmailBodyHtml { get; set; }
        public String EmailSubject { get; set; }
        public String EmailFromName { get; set; }
        public String EmailFromAddress { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public String Program { get; set; }
        public String Portfolio { get; set; }
        public String LeadSource { get; set; }
        public String EncryptedId { get; set; }
        public String InformationPack { get; set; }
        public String Status { get; set; }
        public String InferredCountry { get; set; }
        public String InferredStateRegion { get; set; }
        public String ContactCreated { get; set; }
    }

    public class Confirmation implements Interfaces.IViewModel
    {
        public Id objectId { get; set; }
        public String emailTemplate { get; set; }
        public Boolean sendToOwnerOnly { get; set; }
        public Boolean isEmailSent { get; set; }

        public Confirmation(Id objectId, String emailTemplate, Boolean sendToOwnerOnly)
        {
            this.objectId = objectId;
            this.emailTemplate = emailTemplate;
            this.sendToOwnerOnly = sendToOwnerOnly;
            this.isEmailSent = false;
        }
    }

    public class Email implements Interfaces.IViewModel
    {
        public Id ObjectId { get; set; }
        public String Subject { get; set; }
        public String BodyText { get; set; }
        public String BodyHtml { get; set; }
        public String ToAddress { get; set; }
        public String ToName { get; set; }
        public String OwnerName { get; set; }
        public String OwnerEmail { get; set; }
        public List<Recipient> CcAddress { get; set; }

        public Email (Id objectId, String subject, String bodyText, String bodyHtml, String toAddress, String toName, String ownerName, String ownerEmail)
        {
            this.ObjectId = objectId;
            this.Subject = subject;
            this.BodyText = bodyText;
            this.BodyHtml = bodyHtml;
            this.ToAddress = toAddress;
            this.ToName = toName;
            this.OwnerName = ownerName;
            this.OwnerEmail = ownerEmail;
        }
        
        public Email (Id objectId, String subject, String bodyText, String bodyHtml, String toAddress, String toName, String ownerName, String ownerEmail, List<Recipient> ccAddress)
        {
            this.ObjectId = objectId;
            this.Subject = subject;
            this.BodyText = bodyText;
            this.BodyHtml = bodyHtml;
            this.ToAddress = toAddress;
            this.ToName = toName;
            this.OwnerName = ownerName;
            this.OwnerEmail = ownerEmail;
            this.CcAddress = ccAddress;
        }
    }
    
    public class MarketingCloudPayload implements Interfaces.IViewModel
    {
        public String ContactKey { get; set; }
        public String EventDefinitionKey { get; set; }
        public Interfaces.IViewModel Data { get; set; }

        public MarketingCloudPayload(){}

        public MarketingCloudPayload(String contactKey, String eventDefinitionKey, Interfaces.IViewModel data)
        {
            this.ContactKey = contactKey;
            this.EventDefinitionKey = eventDefinitionKey;
            this.Data = data;
        }
    }
    
    public class MarketingCloudBody implements Interfaces.IViewModel
    {
        public String EventId { get; set; }
        public String Email { get; set; }
        public String Locale { get; set; }
        public String ContactId { get; set; }
        public String EmailBodyText { get; set; }
        public String EmailBodyHtml { get; set; }
        public String EmailSubject { get; set; }
        public String EmailFromName { get; set; }
        public String EmailFromAddress { get; set; }
        public String EmailToAddressCC { get; set; }
        public String EmailToAddressBCC { get; set; }
        public String EmailPreHeader { get; set; }

        public MarketingCloudBody(){}

        public MarketingCloudBody(String eventId, String locale, String contactId,
                String email, String emailSubject, String emailFromName, String emailFromAddress, String emailBodyText, 
                String emailBodyHtml, String emailPreHeader, String emailToAddressCC, String emailToAddressBCC)
        {
            this.EventId = eventId;
            this.Locale = locale;
            this.ContactId = contactId;
            this.Email = email;
            this.EmailBodyText = emailBodyText;
            this.EmailBodyHtml = emailBodyHtml;
            this.EmailSubject = emailSubject;
            this.EmailFromName = emailFromName;
            this.EmailFromAddress = emailFromAddress;
            this.EmailToAddressCC = emailToAddressCC;
            this.EmailToAddressBCC = emailToAddressBCC;
            this.EmailPreHeader = emailPreHeader;
        }
    }
}