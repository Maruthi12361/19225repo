public class SubjectOfferingService 
{
    public static List<ViewModelsWorkforce.SubjectOfferingListItem> getStaffPlanSubjects(ViewModels.Param param)
    {
        return new SubjectOfferingGetPlanSubjectsQuery().query(param);
    }

    public static ViewModelsWorkforce.SubjectOfferingStaffDetails getStaffPlanSubjectsDetails(ViewModels.Param param)
    {
        return new SubjectOfferingGetPlanDetailsQuery().query(param);
    }

    public static ActionResult saveEnrolments(ViewModels.Param param)
    {
        return new SubjectOfferingSaveEnrolmentsCommand().command(param);
    }

    public static ViewModelsWorkforce.SubjectOfferingStaffDetailsItem getStaffPlanSubjectDetails(ViewModels.Param param)
    {
        ViewModelsWorkforce.SubjectOfferingStaffDetails offeringStaffDetails = SubjectOfferingService.getStaffPlanSubjectsDetails(param);

        if (offeringStaffDetails != null && offeringStaffDetails.SubjectOfferings.size() == 1)
            return offeringStaffDetails.SubjectOfferings[0];
        else
        {
            Log.info('SubjectOfferingService', 'getStaffPlanSubjectDetails', 'Unexpected number of offering found. Expected a single offering but found {0}',
                new List<String> { String.valueOf(offeringStaffDetails.SubjectOfferings.size()) });

            return null;
        }
    }
}