@isTest 
public class OpportunitySyncStatusToCampaignCommTests
{
    private static String cerebroID = 'A001641336';

    @testSetup
    static void setup()
    {
        TestUtilities.setupHEDA();
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        TestDataCampaignMock.createSeries(user, 4, true);
        TestDataCampaignMock.createRolloverCampaignMemberStatus(user);

        Contact student = TestDataContactMock.createRolloverStudent(user, cerebroID);
        student.Manual_Followup_Date__c = Date.today().addDays(1);
        update student;

        user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        TestDataCaseMock.createRolloverCase(user, student);
    }

    @isTest
    static void ChangeRolloverCampaignForward_SetsOriginalCampaignMemberAsClosedLost()
    {
        // Arrange
        Opportunity opportunity = 
            [
                SELECT Id, Contact__c, StageName, CampaignId, RecordTypeId
                FROM Opportunity
                WHERE Contact__r.Cerebro_Student_ID__C = : cerebroID
            ];

        Id oldCampaignId = opportunity.CampaignId;        
        Opportunity newOpportunity = opportunity.clone(true, false, true, true);        
        Campaign campaign = 
            [
                SELECT Id 
                FROM Campaign
                WHERE Portfolio__c = 'Domestic' AND Programme__c  = 'MBA'
                ORDER BY StartDate DESC
                LIMIT 1
            ];

        newOpportunity.CampaignId = campaign.Id;
        newOpportunity.recalculateFormulas();

        List<Opportunity> items = new List<Opportunity>{ newOpportunity };
        Map<Id, Opportunity> oldMap = new Map<Id, Opportunity>();
        oldMap.put(opportunity.Id, opportunity);

        // Act
        ActionResult result = OpportunityService.syncRolloverStatusToCampaign(items, oldMap, Enums.TriggerContext.AFTER_UPDATE);

        // Assert
        CampaignMember campaignMember = 
            [
                SELECT Id, Status 
                FROM CampaignMember 
                WHERE ContactId = : opportunity.Contact__c AND CampaignId = : oldCampaignId
            ];

        System.assertEquals(ResultStatus.SUCCESS, result.Status);
        System.assertEquals(RolloverConstantsDefinition.ST_CLOSED_LOST, campaignMember.Status);
    }

    @isTest
    static void ChangeRolloverCampaignBackward_SetsOriginalCampaignMemberAsEnrolling()
    {
        // Arrange
        Opportunity opportunity = 
            [
                SELECT Id, Contact__c, StageName, CampaignId, RecordTypeId
                FROM Opportunity
                WHERE Contact__r.Cerebro_Student_ID__C = : cerebroID
            ];

        Id oldCampaignId = opportunity.CampaignId;
        Campaign campaign = 
            [
                SELECT Id 
                FROM Campaign
                WHERE Portfolio__c = 'Domestic' AND Programme__c  = 'MBA'
                ORDER BY StartDate DESC
                LIMIT 1
            ];

        opportunity.CampaignId = campaign.Id;
        update opportunity;

        Opportunity newOpportunity = opportunity.clone(true, false, true, true);
        newOpportunity.CampaignId = oldCampaignId;
        newOpportunity.recalculateFormulas();

        List<Opportunity> items = new List<Opportunity>{ newOpportunity };
        Map<Id, Opportunity> oldMap = new Map<Id, Opportunity>();
        oldMap.put(opportunity.Id, opportunity);

        // Act
        ActionResult result = OpportunityService.syncRolloverStatusToCampaign(items, oldMap, Enums.TriggerContext.AFTER_UPDATE);

        // Assert
        CampaignMember oldCampaignMember = 
            [
                SELECT Id, Status 
                FROM CampaignMember 
                WHERE ContactId = : opportunity.Contact__c AND CampaignId = : oldCampaignId
            ];
        CampaignMember newCampaignMember = 
            [
                SELECT Id, Status 
                FROM CampaignMember 
                WHERE ContactId = : opportunity.Contact__c AND CampaignId = : campaign.Id
            ];
        System.assertEquals(ResultStatus.SUCCESS, result.Status);
        System.assertEquals(RolloverConstantsDefinition.ST_ENROLLING, oldCampaignMember.Status);
        System.assertEquals(RolloverConstantsDefinition.ST_CLOSED_LOST, newCampaignMember.Status);
    }

    @isTest
    static void CloseWonProgression_SetsExistingCampaignMemberAsClosedWon()
    {
        // Arrange
        Opportunity opportunity = 
            [
                SELECT Id, Contact__c, StageName, CampaignId, RecordTypeId
                FROM Opportunity
                WHERE Contact__r.Cerebro_Student_ID__C = : cerebroID
            ];

        Id oldCampaignId = opportunity.CampaignId;
        Opportunity newOpportunity = opportunity.clone(true, false, true, true); 

        newOpportunity.StageName = RolloverConstantsDefinition.ST_CLOSED_WON;
        newOpportunity.recalculateFormulas();

        List<Opportunity> items = new List<Opportunity>{ newOpportunity };
        Map<Id, Opportunity> oldMap = new Map<Id, Opportunity>();
        oldMap.put(opportunity.Id, opportunity);

        // Act
        ActionResult result = OpportunityService.syncRolloverStatusToCampaign(items, oldMap, Enums.TriggerContext.AFTER_UPDATE);

        // Assert
        CampaignMember campaignMember = 
            [
                SELECT Id, Status 
                FROM CampaignMember 
                WHERE ContactId = : opportunity.Contact__c AND CampaignId = : oldCampaignId
            ];

        System.assertEquals(ResultStatus.SUCCESS, result.Status);
        System.assertEquals(RolloverConstantsDefinition.ST_CLOSED_WON, campaignMember.Status);
    }

    @isTest
    static void NoRolloverCampaignChange_IgnoresStatusSync()
    {
        // Arrange
        Opportunity opportunity = 
            [
                SELECT Id,Contact__c,StageName,CampaignId,RecordTypeId
                FROM Opportunity
                WHERE Contact__r.Cerebro_Student_ID__C = : cerebroID
            ];
        
        Id oldCampaignId = opportunity.CampaignId;
        Opportunity newOpportunity = opportunity.clone(true, false, true, true);
        newOpportunity.StageName = 'Finance';
        newOpportunity.recalculateFormulas();

        List<Opportunity> items = new List<Opportunity>{ newOpportunity };
        Map<Id, Opportunity> oldMap = new Map<Id, Opportunity>();
        oldMap.put(opportunity.Id, opportunity);

        // Act
        ActionResult result = OpportunityService.syncRolloverStatusToCampaign(items, oldMap, Enums.TriggerContext.AFTER_UPDATE);

        // Assert
        CampaignMember campaignMember = 
            [
                SELECT Id, Status 
                FROM CampaignMember 
                WHERE ContactId = : opportunity.Contact__c AND CampaignId = : oldCampaignId
            ];

        System.assertEquals(ResultStatus.SUCCESS, result.Status);
        System.assertNotEquals(RolloverConstantsDefinition.ST_CLOSED_LOST, campaignMember.Status);
    }

    @isTest
    static void NonProgressionType_IgnoresStatusSync()
    {
        // Arrange
        Opportunity opportunity = 
            [
                SELECT Id, Contact__c, StageName, CampaignId, RecordTypeId
                FROM Opportunity
                WHERE Contact__r.Cerebro_Student_ID__C = : cerebroID
            ];

        Id oldCampaignId = opportunity.CampaignId;
        Opportunity newOpportunity = opportunity.clone(true, false, true, true);
        Campaign campaign = 
            [
                SELECT Id 
                FROM Campaign
                WHERE Portfolio__c = 'Domestic' AND Programme__c  = 'MBA'
                ORDER BY StartDate DESC
                LIMIT 1
            ];

        newOpportunity.RecordTypeId = RecordTypeUtils.getRecordTypeId(Constants.RECORDTYPE_OPPORTUNITY_ACQUISITION, SObjectType.Opportunity);
        newOpportunity.CampaignId = campaign.Id;
        newOpportunity.recalculateFormulas();

        List<Opportunity> items = new List<Opportunity>{ newOpportunity };
        Map<Id, Opportunity> oldMap = new Map<Id, Opportunity>();
        oldMap.put(opportunity.Id, opportunity);

        // Act
        ActionResult result = OpportunityService.syncRolloverStatusToCampaign(items, oldMap, Enums.TriggerContext.AFTER_UPDATE);

        // Assert
        CampaignMember campaignMember = 
            [
                SELECT Id, Status 
                FROM CampaignMember 
                WHERE ContactId = : opportunity.Contact__c AND CampaignId = : oldCampaignId
            ];

        System.assertEquals(ResultStatus.SUCCESS, result.Status);
        System.assertNotEquals(RolloverConstantsDefinition.ST_CLOSED_LOST, campaignMember.Status);        
    }
}