public class LeadTriggerHandler extends BaseTriggerHandler
{
    private LeadService service = new LeadService();

    public override void afterInsert()
    {
        ActionResult result = service.createProspect(Trigger.new, Trigger.oldMap, Enums.TriggerContext.AFTER_INSERT);
        
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);
    }
}