public class ContentDocumentService
{
    public static ActionResult createFile(ViewModelsFile.Upload file)
    {
        return new ContentVersionCreateCommand().execute(new List<ViewModelsFile.Upload>{ file });
    }

    public static ActionResult createFile(String fileName, String type, Blob content, Id parentId, Id ownerId, Id communityId) //Boolean saveRecord, 
    {
        ViewModelsFile.Upload file = new ViewModelsFile.Upload();
        file.FileName = fileName;
        file.Type = type;
        file.Content = content;
        file.ParentId = parentId;
        file.OwnerId = ownerId;
        file.CommunityId = communityId;

        return new ContentVersionCreateCommand().execute(new List<ViewModelsFile.Upload>{ file });
    }

    public static ActionResult removeFile(String id)
    {
        return new ContentVersionDeleteCommand().execute(new Set<ID>{ id });
    }

    public static ViewModelsFile.ContentDetails getFile(ViewModels.Param param)
    {
        return new ContentVersionGetFileQuery().query(param);
    }

    public static List<ViewModelsFile.Details> getFiles(String parentId)
    {
        return (List<ViewModelsFile.Details>) new ContentVersionGetQuery().query(new Set<ID>{parentId});
    }

    public static List<ViewModelsFile.Details> getFiles(Set<ID> ids)
    {
        return (List<ViewModelsFile.Details>) new ContentVersionGetQuery().query(ids);
    }

    public static ActionResult updateSecurity(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        return new ContentDocumentUpdateSecurityCommand().execute(items, oldMap, triggerContext);
    }

    public static ActionResult shareWithAllUsers(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        return new ContentVersionShareWithUsersCommand().execute(items, oldMap, triggerContext);
    }

    public static ActionResult shareWith(Id parentId, Set<Id> documentIds)
    {
        ViewModelsFile.DocumentLink item = new ViewModelsFile.DocumentLink();
        item.ParentId = parentId;
        item.DocumentIds = documentIds;

        return new ContentVersionShareWithCommand().execute(new List<ViewModelsFile.DocumentLink>{item});
    }

    public static ActionResult updateType(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        return new ContentVersionUpdateTypeCommand().execute(items, oldMap, triggerContext);
    }

    public static ActionResult updateEntityFlags(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        return new ContentVersionUpdateFlagsCommand().execute(items, oldMap, triggerContext);
    }

    public static ActionResult updateOpportunityDocStatuses(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        return new ContentVersionUpdateOppDocsCommand().execute(items, oldMap, triggerContext);
    }
}