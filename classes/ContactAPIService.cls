global class ContactAPIService 
{
    WebService static String deleteContact(Id contactId)
    {
        ActionResult result = ContactService.deleteContact(contactId);
        
        if (result.isError)
            return result.message;

        return 'Contact successfully deleted';
    }
}