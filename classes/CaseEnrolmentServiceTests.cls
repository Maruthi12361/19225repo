@isTest
public class CaseEnrolmentServiceTests
{
    @testSetup
    static void setup()
    {
        // Arrange
        TestUtilities.setupHEDA();

        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        ViewModelsTestData.Data data = TestDataAcademicConfigMock.create(user);
        Contact student = TestDataContactMock.createStudent(user, 'A001641336');
        hed__Program_Enrollment__c courseEnrolment = TestDataCourseEnrolmentMock.create(user, data.course('MBA').Id, student.Id, 'Ongoing', data.coursePlan('MBA v1').Id);

        TestDataSubjectEnrolmentMock.create(user, data.subjectOffering(Date.today().addDays(59), '9050PROJ'), courseEnrolment, 'In Progress', 'Standard', null, null);
        TestDataSubjectEnrolmentMock.createProvisionalEnrolment(user, data.subjectOffering(Date.today().addDays(115), '8001LEAD'), student, 'Enrolling', 'Standard', courseEnrolment);
        TestDataSubjectEnrolmentMock.create(user, data.subjectOffering(Date.today().addDays(-53), '8002MMGT'), courseEnrolment, 'Complete', 'Standard', 'Pass 2', 50);
        TestDataSubjectEnrolmentMock.createExemption(user, '8003SHRM', 'Strategic Human Resource Management', courseEnrolment);
        TestDataSubjectEnrolmentMock.createExemption(user, '8006FMGT', 'Financial Management', courseEnrolment, true);

        TestDataCaseMock.createStudentSupportCase(user, student);
        TestDataCaseMock.createExemptionCase(user, student);
    }

    @isTest
    static void LinkSubjectEnrolmentsToNewCase_CreatesCaseEnrolments()
    {
        // Arrange
        Case supportCase = [SELECT Id, ContactId FROM Case WHERE RecordType.DeveloperName = : Constants.RECORDTYPE_CASE_STUDENT_SUPPORT];
        List<hed__Course_Enrollment__c> enrolments = [SELECT Id FROM hed__Course_Enrollment__c WHERE hed__Status__c = : Constants.SUBJECTENROLMENT_STATUS_STUDYING AND hed__Contact__c = : supportCase.ContactId ORDER BY Id];
        Set<Id> idSet = new Map<Id, hed__Course_Enrollment__c>(enrolments).keySet();

        // Act
        test.startTest();
        CaseEnrolmentService.create(supportCase.Id, idSet);
        test.stopTest();

        // Assert
        List<CaseEnrolment__c> caseEnrolments =
            [
                SELECT Id, Case__c, SubjectEnrolment__c
                FROM CaseEnrolment__c
                ORDER BY SubjectEnrolment__c
            ];
        System.assertEquals(enrolments.size(), caseEnrolments.size());

        for (Integer i = 0; i < caseEnrolments.size(); i++)
        {
            System.assertEquals(supportCase.Id, caseEnrolments[i].Case__c);
            System.assertEquals(enrolments[i].Id, caseEnrolments[i].SubjectEnrolment__c);
        }
    }

    @isTest
    static void LinkMoreSubjectEnrolmentsToExistingCase_CreatesCaseEnrolmentsWithoutDuplication()
    {
        // Arrange
        Case supportCase = [SELECT Id, ContactId FROM Case WHERE RecordType.DeveloperName = : Constants.RECORDTYPE_CASE_STUDENT_SUPPORT];
        List<hed__Course_Enrollment__c> enrolments = [SELECT Id FROM hed__Course_Enrollment__c WHERE hed__Status__c = : Constants.SUBJECTENROLMENT_STATUS_STUDYING AND hed__Contact__c = : supportCase.ContactId ORDER BY Id];
        Set<Id> idSet = new Map<Id, hed__Course_Enrollment__c>(enrolments).keySet();
        CaseEnrolmentService.create(supportCase.Id, idSet);
        List<hed__Course_Enrollment__c> allEnrolments = [SELECT Id FROM hed__Course_Enrollment__c WHERE hed__Contact__c = : supportCase.ContactId ORDER BY Id];
        idSet = new Map<Id, hed__Course_Enrollment__c>(allEnrolments).keySet();

        // Act
        test.startTest();
        CaseEnrolmentService.create(supportCase.Id, idSet);
        test.stopTest();

        // Assert
        List<CaseEnrolment__c> caseEnrolments =
            [
                SELECT Id, Case__c, SubjectEnrolment__c
                FROM CaseEnrolment__c
                ORDER BY SubjectEnrolment__c
            ];
        System.assertEquals(allEnrolments.size(), caseEnrolments.size());

        for (Integer i = 0; i < caseEnrolments.size(); i++)
        {
            System.assertEquals(supportCase.Id, caseEnrolments[i].Case__c);
            System.assertEquals(allEnrolments[i].Id, caseEnrolments[i].SubjectEnrolment__c);
        }
    }

    @isTest
    static void LinkPlanRequirementsToNewCase_CreatesProvisionalExemptionsAndCaseEnrolmentsForNewSubjectsOnly()
    {
        // Arrange
        hed__Course_Enrollment__c exemption = [SELECT Id FROM hed__Course_Enrollment__c WHERE ExemptionSubjectCode__c = '8006FMGT'];
        Case exemptionCase = [SELECT Id, ContactId FROM Case WHERE RecordType.DeveloperName = : Constants.RECORDTYPE_CASE_EXEMPTIONS];
        List<String> subjectCodes = new List<String>{'8001LEAD', '8003SHRM', '8005CGOV', '8006FMGT'};
        List<hed__Plan_Requirement__c> requirements =
            [
                SELECT Name, SubjectCode__c
                FROM hed__Plan_Requirement__c
                WHERE RecordType.DeveloperName = : Constants.RECORDTYPE_PLANREQUIREMENT_DEPENDENCYITEM AND
                    hed__Plan_Requirement__r.hed__Program_Plan__r.Name = 'MBA v1' AND SubjectCode__c IN : subjectCodes
                ORDER BY SubjectCode__c
            ];
        Set<Id> idSet = new Map<Id, hed__Plan_Requirement__c>(requirements).keySet();

        // Act
        test.startTest();
        CaseEnrolmentService.createExemptionEnrolments(exemptionCase.Id, idSet);
        test.stopTest();

        // Assert
        List<CaseEnrolment__c> caseEnrolments =
            [
                SELECT Id, Case__c, SubjectEnrolment__r.ExemptionSubjectCode__c, SubjectEnrolment__r.RecordType.DeveloperName, SubjectEnrolment__r.Type__c, SubjectEnrolment__r.hed__Status__c
                FROM CaseEnrolment__c
                ORDER BY SubjectEnrolment__r.ExemptionSubjectCode__c
            ];
        // Exclude 8003SHRM because it has been enrolled for student in setup
        subjectCodes.remove(1);
        System.assertEquals(subjectCodes.size(), caseEnrolments.size());

        for (Integer i = 0; i < caseEnrolments.size(); i++)
        {
            System.assertEquals(exemptionCase.Id, caseEnrolments[i].Case__c);
            System.assertEquals(subjectCodes[i], caseEnrolments[i].SubjectEnrolment__r.ExemptionSubjectCode__c);
            System.assertEquals(Constants.RECORDTYPE_COURSEENROLLMENT_PROVISIONAL, caseEnrolments[i].SubjectEnrolment__r.RecordType.DeveloperName);
            System.assertEquals('Exemption', caseEnrolments[i].SubjectEnrolment__r.Type__c);
            System.assertEquals(Constants.SUBJECTENROLMENT_STATUS_INTERESTED, caseEnrolments[i].SubjectEnrolment__r.hed__Status__c);
            
            if (caseEnrolments[i].SubjectEnrolment__r.ExemptionSubjectCode__c == '8006FMGT')
            {
                System.assertEquals(exemption.Id, caseEnrolments[i].SubjectEnrolment__c);
            }
        }
    }

    @isTest
    static void LinkMorePlanRequirementsToExistingCase_CreatesProvisionalExemptionsAndCaseEnrolmentsWithoutDuplication()
    {
        // Arrange
        Case exemptionCase = [SELECT Id, ContactId FROM Case WHERE RecordType.DeveloperName = : Constants.RECORDTYPE_CASE_EXEMPTIONS];
        List<hed__Plan_Requirement__c> requirements =
            [
                SELECT Name, SubjectCode__c
                FROM hed__Plan_Requirement__c
                WHERE RecordType.DeveloperName = : Constants.RECORDTYPE_PLANREQUIREMENT_DEPENDENCYITEM AND
                    hed__Plan_Requirement__r.hed__Program_Plan__r.Name = 'MBA v1' AND SubjectCode__c IN ('8005CGOV', '9002ENT')
                ORDER BY SubjectCode__c
            ];
        Set<Id> idSet = new Map<Id, hed__Plan_Requirement__c>(requirements).keySet();
        CaseEnrolmentService.createExemptionEnrolments(exemptionCase.Id, idSet);
        List<hed__Plan_Requirement__c> moreRequirements =
            [
                SELECT Name, SubjectCode__c
                FROM hed__Plan_Requirement__c
                WHERE RecordType.DeveloperName = : Constants.RECORDTYPE_PLANREQUIREMENT_DEPENDENCYITEM AND
                    hed__Plan_Requirement__r.hed__Program_Plan__r.Name = 'MBA v1' AND SubjectCode__c IN ('8005CGOV', '9002ENT', '9003NVC')
                ORDER BY SubjectCode__c
            ];
        idSet = new Map<Id, hed__Plan_Requirement__c>(moreRequirements).keySet();

        // Act
        test.startTest();
        CaseEnrolmentService.createExemptionEnrolments(exemptionCase.Id, idSet);
        test.stopTest();

        // Assert
        List<CaseEnrolment__c> caseEnrolments =
            [
                SELECT Id, Case__c, SubjectEnrolment__r.ExemptionSubjectCode__c, SubjectEnrolment__r.RecordType.DeveloperName, SubjectEnrolment__r.Type__c, SubjectEnrolment__r.hed__Status__c
                FROM CaseEnrolment__c
                ORDER BY SubjectEnrolment__r.ExemptionSubjectCode__c
            ];
        System.assertEquals(moreRequirements.size(), caseEnrolments.size());

        for (Integer i = 0; i < caseEnrolments.size(); i++)
        {
            System.assertEquals(exemptionCase.Id, caseEnrolments[i].Case__c);
            System.assertEquals(moreRequirements[i].SubjectCode__c, caseEnrolments[i].SubjectEnrolment__r.ExemptionSubjectCode__c);
            System.assertEquals(Constants.RECORDTYPE_COURSEENROLLMENT_PROVISIONAL, caseEnrolments[i].SubjectEnrolment__r.RecordType.DeveloperName);
            System.assertEquals('Exemption', caseEnrolments[i].SubjectEnrolment__r.Type__c);
            System.assertEquals(Constants.SUBJECTENROLMENT_STATUS_INTERESTED, caseEnrolments[i].SubjectEnrolment__r.hed__Status__c);
        }
    }

    @isTest
    static void InsertNewCaseEnrolments_UpdatesSubjectEnrolmentSummaryOnCase()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Case supportCase = [SELECT Id, ContactId, SubjectEnrolmentSummary__c FROM Case WHERE RecordType.DeveloperName = : Constants.RECORDTYPE_CASE_STUDENT_SUPPORT];
        List<hed__Course_Enrollment__c> enrolments =
            [
                SELECT Id, SubjectCode__c, SubjectName__c
                FROM hed__Course_Enrollment__c
                WHERE hed__Status__c = 'Completed' AND hed__Contact__c = : supportCase.ContactId
                ORDER BY SubjectCode__c
            ];
        List<CaseEnrolment__c> caseEnrolments = new List<CaseEnrolment__c>();
        String summary;

        for (hed__Course_Enrollment__c enrolment : enrolments)
        {
            if (String.isBlank(summary))
                summary = StringUtils.concat(enrolment.SubjectCode__c, enrolment.SubjectName__c, ' ');
            else
                summary = StringUtils.concat(summary, StringUtils.concat(enrolment.SubjectCode__c, enrolment.SubjectName__c, ' '), ',');

            caseEnrolments.add(TestDataCaseEnrolmentMock.create(user, supportCase.Id, enrolment.Id));
        }

        // Act
        test.startTest();
        CaseEnrolmentService.updateParent(caseEnrolments, new Map<Id, CaseEnrolment__c>(), Enums.TriggerContext.AFTER_INSERT);
        test.stopTest();

        // Assert
        supportCase = [SELECT Id, SubjectEnrolmentSummary__c FROM Case WHERE Id = : supportCase.Id];
        System.assertEquals(summary, supportCase.SubjectEnrolmentSummary__c);
    }

    @isTest
    static void DeleteCaseEnrolments_UpdatesSubjectEnrolmentSummaryOnCase()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Case supportCase = [SELECT Id, ContactId, SubjectEnrolmentSummary__c FROM Case WHERE RecordType.DeveloperName = : Constants.RECORDTYPE_CASE_STUDENT_SUPPORT];
        List<hed__Course_Enrollment__c> enrolments =
            [
                SELECT Id, SubjectCode__c, SubjectName__c
                FROM hed__Course_Enrollment__c
                WHERE hed__Status__c = 'Completed' AND hed__Contact__c = : supportCase.ContactId
                ORDER BY SubjectCode__c
            ];
        List<CaseEnrolment__c> caseEnrolments = new List<CaseEnrolment__c>();
        String summary;

        for (hed__Course_Enrollment__c enrolment : enrolments)
        {
            if (String.isBlank(summary))
                summary = StringUtils.concat(enrolment.SubjectCode__c, enrolment.SubjectName__c, ' ');
            else
                summary = StringUtils.concat(summary, StringUtils.concat(enrolment.SubjectCode__c, enrolment.SubjectName__c, ' '), ',');

            caseEnrolments.add(TestDataCaseEnrolmentMock.create(user, supportCase.Id, enrolment.Id));
        }

        CaseEnrolmentService.updateParent(caseEnrolments, new Map<Id, CaseEnrolment__c>(), Enums.TriggerContext.AFTER_INSERT);
        System.RunAs(user)
        {
            delete caseEnrolments;
        }

        // Act
        test.startTest();
        CaseEnrolmentService.updateParent(caseEnrolments, new Map<Id, CaseEnrolment__c>(), Enums.TriggerContext.AFTER_DELETE);
        test.stopTest();

        // Assert
        supportCase = [SELECT Id, SubjectEnrolmentSummary__c FROM Case WHERE Id = : supportCase.Id];
        System.assertEquals(null, supportCase.SubjectEnrolmentSummary__c);
    }

    @isTest
    static void DeleteCaseEnrolmentsOnExemptionCase_DeleteProvisionalSubjectEnrolments()
    {
        // Arrange
        Case exemptionCase = [SELECT Id, ContactId FROM Case WHERE RecordType.DeveloperName = : Constants.RECORDTYPE_CASE_EXEMPTIONS];
        List<hed__Plan_Requirement__c> requirements =
            [
                SELECT Name, SubjectCode__c
                FROM hed__Plan_Requirement__c
                WHERE RecordType.DeveloperName = : Constants.RECORDTYPE_PLANREQUIREMENT_DEPENDENCYITEM AND
                    hed__Plan_Requirement__r.hed__Program_Plan__r.Name = 'MBA v1' AND SubjectCode__c IN ('9002ENT')
                ORDER BY SubjectCode__c
            ];
        Set<Id> idSet = new Map<Id, hed__Plan_Requirement__c>(requirements).keySet();
        CaseEnrolmentService.createExemptionEnrolments(exemptionCase.Id, idSet);
        List<CaseEnrolment__c> caseEnrolments =
            [
                SELECT Id, Case__c, SubjectEnrolment__r.ExemptionSubjectCode__c, SubjectEnrolment__r.RecordType.DeveloperName
                FROM CaseEnrolment__c
                ORDER BY SubjectEnrolment__r.ExemptionSubjectCode__c
            ];
        List<Id> enrolmentIds = new List<Id>();

        for (CaseEnrolment__c item : caseEnrolments)
        {
            enrolmentIds.add(item.SubjectEnrolment__c);
        }

        // Act
        test.startTest();
        CaseEnrolmentService.deleteChild(caseEnrolments, new Map<Id, CaseEnrolment__c>(), Enums.TriggerContext.AFTER_DELETE);
        test.stopTest();

        // Assert
        List<hed__Course_Enrollment__c> enrolments = [SELECT Id FROM hed__Course_Enrollment__c WHERE Id IN : enrolmentIds];
        System.assertEquals(0, enrolments.size());
    }

    @isTest
    static void DeleteCaseEnrolmentsOnOtherCases_DoNotDeleteSubjectEnrolments()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Case supportCase = [SELECT Id, ContactId, SubjectEnrolmentSummary__c FROM Case WHERE RecordType.DeveloperName = : Constants.RECORDTYPE_CASE_STUDENT_SUPPORT];
        List<hed__Course_Enrollment__c> enrolments =
            [
                SELECT Id, SubjectCode__c, SubjectName__c
                FROM hed__Course_Enrollment__c
                WHERE hed__Status__c = 'Completed' AND hed__Contact__c = : supportCase.ContactId
                ORDER BY SubjectCode__c
            ];
        List<CaseEnrolment__c> caseEnrolments = new List<CaseEnrolment__c>();

        for (hed__Course_Enrollment__c enrolment : enrolments)
        {
            caseEnrolments.add(TestDataCaseEnrolmentMock.create(user, supportCase.Id, enrolment.Id));
        }

        // Act
        test.startTest();
        CaseEnrolmentService.deleteChild(caseEnrolments, new Map<Id, CaseEnrolment__c>(), Enums.TriggerContext.AFTER_DELETE);
        test.stopTest();

        // Assert
        List<hed__Course_Enrollment__c> remainingEnrolments =
            [
                SELECT Id, SubjectCode__c, SubjectName__c
                FROM hed__Course_Enrollment__c
                WHERE hed__Status__c = 'Completed' AND hed__Contact__c = : supportCase.ContactId
                ORDER BY SubjectCode__c
            ];
        System.assertEquals(enrolments, remainingEnrolments);
    }

    @isTest
    static void DeleteCaseEnrolmentsOnClosedCase_DoNotDeleteSubjectEnrolments()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Case supportCase = [SELECT Id, Status, ContactId FROM Case WHERE RecordType.DeveloperName = : Constants.RECORDTYPE_CASE_STUDENT_SUPPORT];
        List<hed__Course_Enrollment__c> enrolments =
            [
                SELECT Id, SubjectCode__c, SubjectName__c
                FROM hed__Course_Enrollment__c
                WHERE hed__Status__c = 'Completed' AND hed__Contact__c = : supportCase.ContactId
                ORDER BY SubjectCode__c
            ];
        List<CaseEnrolment__c> caseEnrolments = new List<CaseEnrolment__c>();

        for (hed__Course_Enrollment__c enrolment : enrolments)
        {
            caseEnrolments.add(TestDataCaseEnrolmentMock.create(user, supportCase.Id, enrolment.Id));
        }

        supportCase.Status = Constants.CASE_STATUS_CLOSED;
        update supportCase;

        // Act
        Exception ex;
        try
        {
            delete caseEnrolments;
        }
        catch(DmlException e)
        {
            ex = e;
        }

        // Assert
        System.assertNotEquals(null, ex);
        System.assertEquals(true, ex.getMessage().contains('You cannot delete Case Enrolments if their parent Cases are closed.'));
    }

    @isTest
    static void LinkSubjectsToExemptionCase_CreatesProvisionalExemptionsAndCaseEnrolments()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact prospect = TestDataContactMock.createProspectCold(user, '0405558798');
        Case exemptionCase = TestDataCaseMock.createExemptionCase(user, prospect);
        List<hed__Course__c> subjects =
            [
                SELECT Id, hed__Course_Id__c
                FROM hed__Course__c
                WHERE RecordType.DeveloperName = : Constants.RECORDTYPE_SUBJECT_DEFINITION
                ORDER BY hed__Course_Id__c
                LIMIT 2
            ];
        Set<Id> idSet = new Map<Id, hed__Course__c>(subjects).keySet();

        // Act
        test.startTest();
        CaseEnrolmentService.createExemptionEnrolments(exemptionCase.Id, idSet);
        test.stopTest();

        // Assert
        List<CaseEnrolment__c> caseEnrolments =
            [
                SELECT Id, Case__c, SubjectEnrolment__r.ExemptionSubjectCode__c, SubjectEnrolment__r.RecordType.DeveloperName, SubjectEnrolment__r.Type__c, SubjectEnrolment__r.hed__Status__c
                FROM CaseEnrolment__c
                ORDER BY SubjectEnrolment__r.ExemptionSubjectCode__c
            ];
        System.assertEquals(subjects.size(), caseEnrolments.size());

        for (Integer i = 0; i < caseEnrolments.size(); i++)
        {
            System.assertEquals(exemptionCase.Id, caseEnrolments[i].Case__c);
            System.assertEquals(subjects[i].hed__Course_Id__c, caseEnrolments[i].SubjectEnrolment__r.ExemptionSubjectCode__c);
            System.assertEquals(Constants.RECORDTYPE_COURSEENROLLMENT_PROVISIONAL, caseEnrolments[i].SubjectEnrolment__r.RecordType.DeveloperName);
            System.assertEquals('Exemption', caseEnrolments[i].SubjectEnrolment__r.Type__c);
            System.assertEquals(Constants.SUBJECTENROLMENT_STATUS_INTERESTED, caseEnrolments[i].SubjectEnrolment__r.hed__Status__c);
        }
    }
}