public class ContactResetRolloverSegmentationBatch extends BaseBatchCommand 
{
    String cerebroIDs;    
    String sql = 'SELECT Id, IsEnroledCurrentTerm__c, IsEnroledNextTerm__c, IsEnroledFutureTerm__c ' +
                 'FROM Contact ' + 
                 'WHERE (IsEnroledCurrentTerm__c = true OR IsEnroledNextTerm__c = true OR IsEnroledFutureTerm__c = true) ';

    public ContactResetRolloverSegmentationBatch()
    {
        
    }
    
    public ContactResetRolloverSegmentationBatch(String stringParam)
    {
        if (stringParam != null && String.isNotBlank(stringParam))
            cerebroIDs = stringParam;
    }
    
    public override Database.QueryLocator query(Database.BatchableContext context)
    {
        if (cerebroIDs != null)
            sql += ' AND Cerebro_Student_ID__c IN (' + cerebroIDs + ')';            
        
        return Database.getQueryLocator(sql);
    }

    public override void command(Database.BatchableContext context, List<sObject> scope)
    {
        List<Contact> contacts = (List<Contact>)scope;        
        
        for (Contact con: contacts)
        {
            if (con.IsEnroledCurrentTerm__c)
                con.IsEnroledCurrentTerm__c = false;
            
            if (con.IsEnroledNextTerm__c)
                con.IsEnroledNextTerm__c = false;
            
            if (con.IsEnroledFutureTerm__c)
                con.IsEnroledFutureTerm__c = false;
        }
        
        if (contacts.size() > 0)
            uow.registerDirty(contacts);
    }
    
    public override void finalise(Database.BatchableContext context)
    {
        ContactRolloverSegmentationBatch batch;
        
        if (cerebroIDs == null)
            batch = new ContactRolloverSegmentationBatch();
        else
            batch = new ContactRolloverSegmentationBatch(cerebroIDs);
        
        database.executebatch(batch);
    }
}