public class SkillSetTriggerHandler extends BaseTriggerHandler
{
    public override void afterInsert()
    {
        ActionResult result;

        result = SkillSetService.calculateNumberSkilledContacts(Trigger.new, Trigger.oldMap, Enums.TriggerContext.AFTER_INSERT);
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);

        result = SkillSetService.manageSkillSetUsersInGroups(Trigger.new, Trigger.oldMap, Enums.TriggerContext.AFTER_INSERT);
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);
    }

    public override void afterUpdate()
    {
        ActionResult result;

        result = SkillSetService.calculateNumberSkilledContacts(Trigger.new, Trigger.oldMap, Enums.TriggerContext.AFTER_UPDATE);
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);

        result = SkillSetService.manageSkillSetUsersInGroups(Trigger.new, Trigger.oldMap, Enums.TriggerContext.AFTER_UPDATE);
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);
    }

    public override void afterDelete()
    {
        ActionResult result;

        result = SkillSetService.calculateNumberSkilledContacts(Trigger.old, Trigger.oldMap, Enums.TriggerContext.AFTER_DELETE);
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);

        result = SkillSetService.manageSkillSetUsersInGroups(Trigger.old, Trigger.oldMap, Enums.TriggerContext.AFTER_DELETE);
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);
    }
}