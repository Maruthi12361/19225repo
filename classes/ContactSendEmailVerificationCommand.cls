public class ContactSendEmailVerificationCommand extends BaseQueueableCommand
{
    private static final String templateName = 'Email Verification';

    public ContactSendEmailVerificationCommand()
    {
        super(1);
    }

    public override List<SObject>  validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        EmailTemplate emailTemplate = EmailService.getEmailTemplate(templateName);

        if (emailTemplate == null)
        {
            String message = String.format('No email template found for {0}.', new List<String> { templateName });
            Log.error(this.className, 'validate', message);
            throw new Exceptions.ApplicationException(message);
        }

        return items;
    }

    public override ActionResult command(List<SObject> items, Id jobId)
    {
        EmailTemplate emailTemplate = EmailService.getEmailTemplate(templateName);

        for (Contact contact : (List<Contact>) items)
        {
            String message;
            Messaging.SingleEmailMessage mail = Messaging.renderStoredEmailTemplate(emailTemplate.Id, contact.Id, contact.Id);

            ActionResult result = EmailService.sendToRecipient(Constants.APIPROVIDER_DEFAULT, contact.Email, contact.Name, mail.getSubject(), contact.Owner.Email, contact.Owner.Name, null, mail.getHtmlBody(), null);

            if (result.isError)
            {
                message = String.format('Failed to send email {0} to {1}<{2}> due to: {3}.', new List<String> { templateName, contact.Name, contact.Email, String.valueOf(result.messages) });
                Log.error('ContactSendEmailVerificationCommand', 'command', message + ' ' + result);
                throw new Exceptions.ApplicationException(message);
            }

            result = TaskService.create(contact.OwnerId, contact.Id, null, Date.today(), 'Normal', Label.ID_RecordType_Task_ClientContact, 'Completed', 'Email Verification Sent', 'Email', 'Other');

            if (result.isError)
            {
                message = String.format('Failed to create Task for {0}', new List<String> { contact.Id });
                Log.error('ContactSendEmailVerificationCommand', 'command', message + ' ' + result.message);
                throw new Exceptions.ApplicationException(message);
            }
        }

        return new ActionResult(ResultStatus.SUCCESS);
    }
}