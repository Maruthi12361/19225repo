public class ViewModelsSystem
{
    public class SystemHealthCheck implements Interfaces.IViewModel
    {
        public Integer FailedChecks;
        public Integer TotalChecks;
        public Integer UserEmailsToSend;
    }
}