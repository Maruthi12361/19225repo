public class ContentVersionUpdateTypeCommand extends BaseTriggerCommand
{
    protected override List<SObject> validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        List<ContentVersion> result = new List<ContentVersion>();
        List<User> users = getUsers(SObjectUtils.getIDs(items, 'CreatedById'));

        for (ContentVersion contentVersion : (List<ContentVersion>)items)
        {
            if (triggerContext != Enums.TriggerContext.BEFORE_INSERT)
                continue;

            User createdUser = getCreatedByUser(contentVersion.CreatedById, users);

            if (createdUser == null || createdUser.Alias == Constants.USER_COURSEADVISOR)
                continue;

            result.add(contentVersion);
        }

        return result;
    }

    protected override ActionResult command(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        for (ContentVersion contentVersion : (List<ContentVersion>)items)
        {
            Log.info(this.className, 'command', 'Updating type value for {0} {1} content version', new List<String> { contentVersion.Id, contentVersion.Title });

            if (contentVersion.Title.contains(Constants.CONTENTVERSION_TYPE_INVOICE_ABBR) || contentVersion.Title.containsIgnoreCase(Constants.CONTENTVERSION_TYPE_INVOICE))
                contentVersion.Type__c = Constants.CONTENTVERSION_TYPE_INVOICE;
            else if (contentVersion.Title.contains(Constants.CONTENTVERSION_TYPE_LOO_ABBR) || contentVersion.Title.containsIgnoreCase(Constants.CONTENTVERSION_TYPE_LOO))
                contentVersion.Type__c = Constants.CONTENTVERSION_TYPE_LOO;
            else if (contentVersion.Title.contains(Constants.CONTENTVERSION_TYPE_TIMETABLE_ABBR) || contentVersion.Title.containsIgnoreCase(Constants.CONTENTVERSION_TYPE_TIMETABLE))
                contentVersion.Type__c = Constants.CONTENTVERSION_TYPE_TIMETABLE;
            else if (contentVersion.Title.contains(Constants.CONTENTVERSION_TYPE_WELCOMELETTER_ABBR) || contentVersion.Title.containsIgnoreCase(Constants.CONTENTVERSION_TYPE_WELCOMELETTER))
                contentVersion.Type__c = Constants.CONTENTVERSION_TYPE_WELCOMELETTER;
        }

        return new ActionResult(ResultStatus.SUCCESS);
    }

    private List<User> getUsers(Set<Id> ids)
    {
        return
        [
            SELECT Id, Alias
            FROM User
            WHERE Id IN :ids
        ];
    }

    private User getCreatedByUser(Id id, List<User> users)
    {
        for (User user : users)
            if (user.Id == id)
                return user;

        return null;
    }
}