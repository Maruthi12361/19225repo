@isTest
public class OpportunityScoreDemographicsBatchTest 
{
    @testSetup 
    public static void setup()
    {
        TestUtilities.setupHEDA();
        TestUtilities.disableComponent('CerebroSyncApplicationCommand');
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact contact = TestDataContactMock.createProspectHot(user);
    }

    @isTest 
    static void OpportunityWithNoDemographicScoreConfigMatch_ContactDemographicScoreUnchanged() 
    {
        // Arrange         
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        TestDataSalesScoreConfigMock.create(user, 'Opportunity', 60, 'Description', String.valueOf(Enums.Operator.EQUAL), 'This is a test opportunity');

        // Act
        test.startTest();
        OpportunityScoreDemographicsBatch batch = new OpportunityScoreDemographicsBatch();
        ID batchProcessId = Database.executeBatch(batch, 5);
        test.stopTest();

        Opportunity opportunity = [SELECT Id, Contact__r.DemographicScore__c FROM Opportunity Limit 1];

        // Assert
        System.assertEquals(0, opportunity.Contact__r.DemographicScore__c, 'Unexpected demographic score value');
    }

    @isTest 
    static void OpportunityMathingSingleDemographicScoreConfig_ContactDemographicScoreUpdatedCorrectly() 
    {
        // Arrange 
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        TestDataSalesScoreConfigMock.create(user, 'Opportunity', 60, 'Payment_Method__c', String.valueOf(Enums.Operator.EQUAL), 'Self Funded');
        
        Opportunity opp = [SELECT Id, Payment_Method__c from Opportunity LIMIT 1];
        opp.Payment_Method__c = 'Self Funded';

        // Act
        test.startTest();
        update opp;
        OpportunityScoreDemographicsBatch batch = new OpportunityScoreDemographicsBatch();
        ID batchProcessId = Database.executeBatch(batch, 5);
        test.stopTest();

        Opportunity opportunity = [SELECT Id, Contact__r.DemographicScore__c FROM Opportunity Limit 1];

        // Assert
        System.assertEquals(60, opportunity.Contact__r.DemographicScore__c, 'Unexpected demographic score value');
    }

    @isTest 
    static void OpportunityMathingCoupleDemographicScoreConfigs_DemographicScoreUpdatedCorrectly() 
    {
        // Arrange 
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        TestDataSalesScoreConfigMock.create(user, 'Contact', 60, 'OtherCountry', String.valueOf(Enums.Operator.EQUAL), 'Australia');
        TestDataSalesScoreConfigMock.create(user, 'Opportunity', 60, 'Payment_Method__c', String.valueOf(Enums.Operator.EQUAL), 'Self Funded');

        Contact contact = [SELECT Id, OtherCountry FROM Contact LIMIT 1];
        contact.OtherCountry = 'Australia';

        Opportunity opp = [SELECT Id, Payment_Method__c from Opportunity LIMIT 1];
        opp.Payment_Method__c = 'Self Funded';

        // Act
        test.startTest();
        update contact;
        update opp;
        OpportunityScoreDemographicsBatch batch = new OpportunityScoreDemographicsBatch();
        ID batchProcessId = Database.executeBatch(batch, 5);
        test.stopTest();

        Opportunity opportunity = [SELECT Id, Contact__r.DemographicScore__c FROM Opportunity Limit 1];

        // Assert
        System.assertEquals(120, opportunity.Contact__r.DemographicScore__c, 'Unexpected demographic score value');
    }
}