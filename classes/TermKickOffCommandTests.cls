@isTest
public class TermKickOffCommandTests
{
    @testSetup
    static void setup()
    {
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Account department = TestDataAccountMock.createDepartmentBusiness(user);
        hed__Term__c term = TestDataTermMock.create(user, department.Id, Date.today());
        hed__Course__c subjectDefinition = TestDataSubjectMock.createDefinitionCGOV(user);
        hed__Course__c subjectVersion = TestDataSubjectMock.createVersionCGOV(user);
        hed__Course_Offering__c subjectOffering = TestDataSubjectOfferingMock.createOffering(user, subjectVersion, term);

        hed__Course__c leadershipSubjectDefinition = TestDataSubjectMock.createDefinitionLEAD(user);
        hed__Course__c leadershipSubjectVersion = TestDataSubjectMock.createVersionLEAD(user);
        TestDataSubjectOfferingMock.createOffering(user, leadershipSubjectVersion, term);
    }

    @isTest
    static void TermWithNoPlanningStatusValue_TermPlanningStatusUpdatedAfterKickOffSuccessfully()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        hed__Term__c term = [SELECT Id FROM hed__Term__c LIMIT 1];

        // Act
        ActionResult result = new TermKickOffCommand().execute(term.Id);
        hed__Term__c updatedTerm = [SELECT Id, PlanningStatus__c FROM hed__Term__c WHERE Id = :term.Id];

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Unexpected result status.');
        System.assertEquals('In Planning', updatedTerm.PlanningStatus__c, 'Unexpected planning status value');
    }

    @isTest 
    static void TermKickOffWithCoupleSubjectOfferings_TermPlanningStatusUpdatedAndFacultyEnrolmentsCreated()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        hed__Term__c term = [SELECT Id FROM hed__Term__c LIMIT 1];
        hed__Course_Offering__c governanceSubjectOffering = [SELECT Id, Name, ActualStudents__c FROM hed__Course_Offering__c WHERE Name LIKE '%Corporate Governance%' LIMIT 1];
        hed__Course_Offering__c leadershipSubjectOffering = [SELECT Id, Name, ActualStudents__c FROM hed__Course_Offering__c WHERE Name LIKE '%Leadership%' LIMIT 1];

        governanceSubjectOffering.ActualStudents__c = 225;
        leadershipSubjectOffering.ActualStudents__c = 87;

        List<hed__Course_Offering__c> subjectOfferings = new List<hed__Course_Offering__c> { governanceSubjectOffering, leadershipSubjectOffering };
        update subjectOfferings;

        // Act
        Test.startTest();
        ActionResult result = new TermKickOffCommand().execute(term.Id);
        hed__Term__c updatedTerm = [SELECT Id, PlanningStatus__c FROM hed__Term__c WHERE Id = :term.Id];
        List<hed__Course_Enrollment__c> governanceFacultyEnrolments = getFacultyEnrolments(governanceSubjectOffering.Id);
        List<hed__Course_Enrollment__c> leadershipFacultyEnrolments = getFacultyEnrolments(leadershipSubjectOffering.Id);
        Test.stopTest();

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Unexpected result status.');
        System.assertEquals('In Planning', updatedTerm.PlanningStatus__c, 'Unexpected planning status value');

        System.assertEquals(6, governanceFacultyEnrolments.size(), 'Unexpected number of governance faculty enrolments');
        System.assertEquals(3, leadershipFacultyEnrolments.size(), 'Unexpected number of leadership faculty enrolments');

        Integer numberOfCoordinators = 0;
        Integer numberOfFacilitators = 0;

        for (hed__Course_Enrollment__c subjectOffering : governanceFacultyEnrolments)
        {
            System.assertEquals('Recommended', subjectOffering.hed__Status__c, 'Unexpected status in one of the governance faculty enrolments');

            if (subjectOffering.Role__c == 'Subject Coordinator')
            {
                numberOfCoordinators++;
                System.assertEquals(20, subjectOffering.StudentsAllocated__c, 'Unexpected number of allocated students for Subject Coordinator in one of the governance faculty enrolments');
            }
            else if (subjectOffering.Role__c == 'Online Facilitator')
                numberOfFacilitators++;
        }

        System.assertEquals(2, numberOfCoordinators, 'Unexpected number of governance faculty enrolments with Subject Coordinator role');
        System.assertEquals(4, numberOfFacilitators, 'Unexpected number of governance faculty enrolments with Online Facilitator role');
    }

    @isTest 
    static void TermKickOffWithExistingSubjectOfferingSubjectEnrolments_TermPlanningStatusUpdatedAndNoAdditonalFacultyEnrolmentsCreated()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        hed__Term__c term = [SELECT Id FROM hed__Term__c LIMIT 1];
        hed__Course_Offering__c governanceSubjectOffering = [SELECT Id, Name, ActualStudents__c FROM hed__Course_Offering__c WHERE Name LIKE '%Corporate Governance%' LIMIT 1];
        hed__Course_Offering__c leadershipSubjectOffering = [SELECT Id, Name, ActualStudents__c FROM hed__Course_Offering__c WHERE Name LIKE '%Leadership%' LIMIT 1];

        governanceSubjectOffering.ActualStudents__c = 225;
        leadershipSubjectOffering.ActualStudents__c = 87;

        List<hed__Course_Offering__c> subjectOfferings = new List<hed__Course_Offering__c> { governanceSubjectOffering, leadershipSubjectOffering };
        update subjectOfferings;

        // Act
        Test.startTest();
        ActionResult result = new TermKickOffCommand().execute(term.Id);
        result = new TermKickOffCommand().execute(term.Id);

        hed__Term__c updatedTerm = [SELECT Id, PlanningStatus__c FROM hed__Term__c WHERE Id = :term.Id];
        List<hed__Course_Enrollment__c> governanceFacultyEnrolments = getFacultyEnrolments(governanceSubjectOffering.Id);
        List<hed__Course_Enrollment__c> leadershipFacultyEnrolments = getFacultyEnrolments(leadershipSubjectOffering.Id);
        Test.stopTest();

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Unexpected result status.');
        System.assertEquals('In Planning', updatedTerm.PlanningStatus__c, 'Unexpected planning status value');

        System.assertEquals(6, governanceFacultyEnrolments.size(), 'Unexpected number of governance faculty enrolments');
        System.assertEquals(3, leadershipFacultyEnrolments.size(), 'Unexpected number of leadership faculty enrolments');
    }

    @isTest 
    static void TermKickOffWithSubjectOfferingNoActualStudents_TermPlanningStatusUpdatedAndNoFacultyEnrolmentsCreated()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        hed__Term__c term = [SELECT Id FROM hed__Term__c LIMIT 1];
        hed__Course_Offering__c governanceSubjectOffering = [SELECT Id, Name, ActualStudents__c FROM hed__Course_Offering__c WHERE Name LIKE '%Corporate Governance%' LIMIT 1];
        hed__Course_Offering__c leadershipSubjectOffering = [SELECT Id, Name, ActualStudents__c FROM hed__Course_Offering__c WHERE Name LIKE '%Leadership%' LIMIT 1];

        governanceSubjectOffering.ActualStudents__c = null;
        leadershipSubjectOffering.ActualStudents__c = null;

        List<hed__Course_Offering__c> subjectOfferings = new List<hed__Course_Offering__c> { governanceSubjectOffering, leadershipSubjectOffering };
        update subjectOfferings;

        // Act
        Test.startTest();
        ActionResult result = new TermKickOffCommand().execute(term.Id);
        
        hed__Term__c updatedTerm = [SELECT Id, PlanningStatus__c FROM hed__Term__c WHERE Id = :term.Id];
        List<hed__Course_Enrollment__c> governanceFacultyEnrolments = getFacultyEnrolments(governanceSubjectOffering.Id);
        List<hed__Course_Enrollment__c> leadershipFacultyEnrolments = getFacultyEnrolments(leadershipSubjectOffering.Id);
        Test.stopTest();

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Unexpected result status.');
        System.assertEquals('In Planning', updatedTerm.PlanningStatus__c, 'Unexpected planning status value');

        System.assertEquals(0, governanceFacultyEnrolments.size(), 'Unexpected number of governance faculty enrolments');
        System.assertEquals(0, leadershipFacultyEnrolments.size(), 'Unexpected number of leadership faculty enrolments');
    }

    private static List<hed__Course_Enrollment__c> getFacultyEnrolments(Id subjectOfferingId)
    {
        return
        [
            SELECT Id, hed__Course_Offering__c, Role__c, hed__Status__c, StudentsAllocated__c
            FROM hed__Course_Enrollment__c
            WHERE hed__Course_Offering__c = :subjectOfferingId AND RecordType.DeveloperName = 'Faculty'
        ];
    }
}