@isTest
public class CaseNotifyOfNewCommentCommandTests 
{
    @testSetup
    static void setup()
    {
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createProspectHot(user);
        User student = TestDataUserMock.createStudentPortalLoginProfileUser(user, contact.Id);
        User owner = UserService.getByAlias(Constants.USER_NON_ADMIN); 
        Case supportCase = TestDataCaseMock.createStudentSupportCase(student, contact);
        supportCase.OwnerId = owner.Id;
        update supportCase;
    }

    @isTest
    static void StudentCreateComment_NotificationSentToOwner()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        Contact contact = [SELECT Id, Email, Name FROM Contact LIMIT 1];
        Case supportCase = [SELECT Id, CaseNumber, OwnerId, Owner.Name, Owner.Email FROM Case LIMIT 1];
        User student = [SELECT Id, Name, Email, Profile.Name FROM User WHERE UserName= 'StudentPortalProfileUser@studentusers.aib.edu.au' LIMIT 1];

        // Act
        Test.startTest();
        Integer callCount = Limits.getFutureCalls();
        CaseComment comment = new CaseComment();
        comment.CommentBody = 'Student creates comment';
        comment.CreatedById = student.Id;
        comment.ParentId = supportCase.Id;
        comment.IsPublished = true;
        insert comment;
        callCount = Limits.getFutureCalls() - callCount;
        Test.stopTest();

        // Assert
        System.assertEquals(1, callCount);
    }

    @isTest 
    static void OwnerCreateComment_NotificationSentToStudent()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        Case supportCase = [SELECT Id, CaseNumber, OwnerId, Owner.Name, Owner.Email FROM Case LIMIT 1];

        // Act
        Test.startTest();
        Integer callCount = Limits.getFutureCalls();
        CaseComment comment = new CaseComment();
        comment.CommentBody = 'Case Owner creates comment';
        comment.CreatedById = supportCase.OwnerId;
        comment.ParentId = supportCase.Id;
        comment.IsPublished = true;
        insert comment;
        callCount = Limits.getFutureCalls() - callCount;
        Test.stopTest();

        // Assert
        System.assertEquals(1, callCount);
    }

    @isTest 
    static void StaffCreatesPublicComment_NotificationSentToOwnerAndStudent()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        User user = UserService.getByAlias(Constants.USER_TESTSETUP); 
        User staff = UserService.getByAlias(Constants.USER_NON_ADMIN2); 
        Case supportCase = [SELECT Id, CaseNumber, OwnerId, Owner.Name, Owner.Email FROM Case LIMIT 1];

        // Act
        Test.startTest();
        Integer callCount = Limits.getFutureCalls();
        CaseComment comment = new CaseComment();
        comment.CommentBody = 'Staff creates a Public comment';
        comment.CreatedById = staff.Id;
        comment.ParentId = supportCase.Id;
        comment.IsPublished = true;
        insert comment;
        callCount = Limits.getFutureCalls() - callCount;
        Test.stopTest();

        // Assert
        System.assertEquals(2, callCount);
    }

    @isTest
    static void StaffCreatesPrivateComment_NotificationSentToOwnerOnly()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        User user = UserService.getByAlias(Constants.USER_TESTSETUP); 
        User staff = UserService.getByAlias(Constants.USER_NON_ADMIN2); 
        Case supportCase = [SELECT Id, CaseNumber, OwnerId, Owner.Name, Owner.Email FROM Case LIMIT 1];

        // Act
        Test.startTest();
        Integer callCount = Limits.getFutureCalls();
        CaseComment comment = new CaseComment();
        comment.CommentBody = 'Staff creates a Private comment';
        comment.CreatedById = staff.Id;
        comment.ParentId = supportCase.Id;
        comment.IsPublished = false;
        insert comment;
        callCount = Limits.getFutureCalls() - callCount;
        Test.stopTest();

        // Assert
        System.assertEquals(1, callCount);
    }

    @isTest
    static void BulkInsertCommentByAPIUser_NoNotificationSent()
    {
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        User user = UserService.getByAlias(Constants.USER_TESTSETUP); 
        User apiUser = UserService.getByAlias(Constants.USER_CEREBROSYNC);
        Case supportCase = [SELECT Id, CaseNumber, OwnerId, Owner.Name, Owner.Email FROM Case LIMIT 1];

        // Act
        Test.startTest();
        Integer callCount = Limits.getFutureCalls();
        CaseComment comment = new CaseComment();
        comment.CommentBody = 'API User creates comment';
        comment.CreatedById = apiUser.Id;
        comment.ParentId = supportCase.Id;
        comment.IsPublished = true;
        insert comment;
        callCount = Limits.getFutureCalls() - callCount;
        Test.stopTest();

        // Assert
        System.assertEquals(0, callCount);
    }
}