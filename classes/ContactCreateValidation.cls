public class ContactCreateValidation extends BaseValidation
{
    public override ActionResult validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        ActionResult result = new ActionResult(ResultStatus.SUCCESS);
        List<Contact> contacts = new List<Contact>();

        for (Contact contact : (List<Contact>) items)
        {
            if (triggerContext == Enums.TriggerContext.AFTER_INSERT)
            {
                if (contact.AccountId != null)
                    contacts.add(contact);
            }
            else if (triggerContext == Enums.TriggerContext.AFTER_UPDATE)
            {
                Contact oldContact = (Contact) oldMap.get(contact.Id);

                if (contact.AccountId != null && contact.AccountId != oldContact.AccountId)
                    contacts.add(contact);
            }
        }

        if (contacts.size() == 0)
        {
            Log.info(this.className, 'validate', 'Validation: No objects meeting conditions to process.');
            return result;
        }

        Set<Id> accountIdSet = new Set<Id>();

        for (Contact contact : contacts)
        {
            accountIdSet.add(contact.AccountId);
        }

        Map<Id, Account> accountMap = new Map<Id, Account> (
            [
                SELECT Id, Name, RecordType.DeveloperName,
                    (
                        SELECT Id
                        FROM Contacts
                    )
                FROM Account
                WHERE Id IN : accountIdSet
            ]);

        for (Contact contact : contacts)
        {
            Account account = accountMap.get(contact.AccountId);

            if (account == null)
            {
                Log.info(this.className, 'validate', 'Skipping new account ' + contact.AccountId);
                continue;
            }

            if (account.RecordType.DeveloperName == Constants.RECORDTYPE_ACCOUNT_ADMINISTRATIVE && containsOtherContact(account.Contacts, contact.Id))
            {
                String errorMsg = 'There is already an existing Contact related to the selected administrative account.';
                result = new ActionResult(ResultStatus.ERROR, errorMsg);
                break;
            }
        }

        return result;
    }

    private Boolean containsOtherContact(List<Contact> contacts, Id contactId)
    {
        for (Contact contact : contacts)
        {
            if (contact.Id != contactId)
                return true;
        }

        return false;
    }
}