/**
 * An apex class that updates details of a portal user.
   Guest users are never able to access this page.
 */
@IsTest public with sharing class MyProfilePageControllerTest 
{
    @IsTest(SeeAllData=true) static void testSave() 
    {
        // Modify the test to query for a portal user that exists in your org
        List<User> existingPortalUsers = [SELECT id, profileId, userRoleId FROM User WHERE UserRoleId <> null AND UserType='CustomerSuccess'];

        if (existingPortalUsers.isEmpty()) 
        {
            User currentUser = [select id, title, firstname, lastname, email, phone, mobilephone, fax, street, city, state, postalcode, country
                                FROM User WHERE id =: UserInfo.getUserId()];
            MyProfilePageController controller = new MyProfilePageController();
            System.assertEquals(currentUser.Id, controller.getUser().Id, 'Did not successfully load the current user');
            System.assertEquals(false, controller.getIsEdit(), 'isEdit should default to false');
            controller.edit();
            System.assertEquals(true, controller.getIsEdit());
            controller.cancel();
            System.assertEquals(false, controller.getIsEdit());
            
            System.assertEquals(controller.changePassword().getUrl(), Page.ChangePassword.getUrl());
            
            String randFax = Math.rint(Math.random() * 1000) + '5551234';
            controller.getUser().Fax = randFax;
            controller.save();
            System.assertEquals(false, controller.getIsEdit());

            currentUser = [Select id, fax from User where id =: currentUser.Id];
            System.assertEquals(randFax, currentUser.fax);
        } 
        else 
        {
            User existingPortalUser = existingPortalUsers[0];
            String randFax = Math.rint(Math.random() * 1000) + '5551234';

            System.runAs(existingPortalUser) 
            {
                MyProfilePageController controller = new MyProfilePageController();
                System.assertEquals(existingPortalUser.Id, controller.getUser().Id, 'Did not successfully load the current user');
                System.assertEquals(false, controller.getIsEdit(), 'isEdit should default to false');
                controller.edit();
                System.assertEquals(true, controller.getIsEdit());

                controller.cancel();
                System.assertEquals(false, controller.getIsEdit());

                controller.getUser().Fax = randFax;
                controller.save();
                System.assertEquals(false, controller.getIsEdit());
            }

            // verify that the user was updated
            existingPortalUser = [Select id, fax from User where id =: existingPortalUser.Id];
            System.assertEquals(randFax, existingPortalUser.fax);
        }
    }
}