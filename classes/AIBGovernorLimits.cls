public class AIBGovernorLimits
{
    public Integer soqlCount { get; set; }
    public Integer dmlCount { get; set; }
    public Integer soslCount { get; set; }
    public Integer cpuTime { get; set; }

    public Integer soqlCountDelta { get; set; }
    public Integer dmlCountDelta { get; set; }
    public Integer soslCountDelta { get; set; }
    public Integer cpuTimeDelta { get; set; }

    public Integer soqlCountTotal { get; set; }
    public Integer dmlCountTotal { get; set; }
    public Integer soslCountTotal { get; set; }
    public Integer cpuTimeTotal { get; set; }

    public AIBGovernorLimits()
    {
        soqlCount = 10;
        dmlCount = 15;
        soslCount = 2;
        cpuTime = 1000;
    }

    public void initialiseGLCounts()
    {
        soqlCount = 0;
        dmlCount = 0;
        soslCount = 0;
        cpuTime = 0;
        soqlCountTotal = Limits.getLimitQueries();
        dmlCountTotal = Limits.getLimitDmlStatements();
        soslCountTotal = Limits.getLimitSoslQueries();
        cpuTimeTotal = limits.getLimitCpuTime();
    }

    public void trackGLCounts()
    {
        soqlCountDelta = soqlCount;
        dmlCountDelta = dmlCount;
        soslCountDelta = soslCount;
        cpuTimeDelta = cpuTime;

        soqlCount = Limits.getQueries();
        dmlCount = Limits.getDmlStatements();
        soslCount = Limits.getSoslQueries();
        cpuTime = limits.getCpuTime();

        soqlCountDelta = soqlCount - soqlCountDelta;
        dmlCountDelta = dmlCount - dmlCountDelta;
        soslCountDelta = soslCount - soslCountDelta;
        cpuTimeDelta = cpuTime - cpuTimeDelta;
    }

    public String getDeltaDescription(boolean includeHeading)
    {
        return (includeHeading ? 'Delta Used-' : '') + createDescription(soqlCountDelta, dmlCountDelta, soslCountDelta, cpuTimeDelta);
    }

    public String getTotalDescription(boolean includeHeading)
    {
        return (includeHeading ? 'Total Used-' : '') + createDescription(soqlCount, dmlCount, soslCount, cpuTime);
    }

    public String getAvailableDescription(boolean includeHeading)
    {
        return (includeHeading ? 'Available -' : '') + createDescription(soqlCountTotal, dmlCountTotal, soslCountTotal, cpuTimeTotal);
    }

    private String createDescription(Integer soql, Integer dml, Integer sosl, Integer cpu)
    {
        return 'SOQL:' + String.valueOf(soql).leftPad(4, ' ') + ', DML:' + String.valueOf(dml).leftPad(4, ' ') + ', SOSL:' + String.valueOf(sosl).leftPad(3, ' ') + ', CPUTime:' + String.valueOf(cpu).leftPad(5, ' ');
    }
}