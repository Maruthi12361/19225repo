public class ContentVersionGetFileQuery
{
    private Set<String> restrictedFileTypes = new Set<String>{ Constants.CONTENTVERSION_TYPE_PROOFOFID, Constants.CONTENTVERSION_TYPE_WORKEXPERIENCE };
    @testVisible
    private static final String ERROR_NOCONTACT = 'Contact no longer exists';
    @testVisible
    private static final String ERROR_NOFILE = 'File no longer exists';
    @testVisible
    private static final String ERROR_UNAUTHORIZED = 'Unauthorized request';

    public ViewModelsFile.ContentDetails query(Interfaces.IViewModel param)
    {
        return getFile((ViewModels.Param) param);
    }

    private ViewModelsFile.ContentDetails getFile(ViewModels.Param param)
    {
        Log.info('ContentVersionGetFileQuery', 'getFile', 'Getting file with following payload: ' + JSON.serialize(param));

        validate(param);
        ViewModelsFile.ContentDetails details = new ViewModelsFile.ContentDetails();
        ContentVersion version = getContentVersion(param.Identifier);

        if (version != null)
        {
            details.VersionId = version.Id;
            details.Name = version.Title;
            details.Extension = version.FileExtension;
            details.Content = version.VersionData;

            return details;
        }

        return null;
    }

    private Boolean validate(ViewModels.Param param)
    {
        if (param == null)
        {
            Log.error('ContentVersionGetFileQuery', 'validate', Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Param object' });
            throw new Exceptions.UnauthorizedException(String.format(Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Param object' }));
        }

        if (String.isEmpty(param.Identifier))
        {
            Log.error('ContentVersionGetFileQuery', 'validate', Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Identifier' });
            throw new Exceptions.UnauthorizedException(String.format(Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Identifier' }));
        }

        Log.info('ContentVersionGetFileQuery', 'validate', 'Validating content version {0}', new List<String>{ param.Identifier });

        if (String.isEmpty(param.Payload))
        {
            Log.error('ContentVersionGetFileQuery', 'validate', Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Payload' });
            throw new Exceptions.UnauthorizedException(String.format(Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Payload' }));
        }

        Map<String, Object> payloadMap = (Map<String, Object>)JSON.deserializeUntyped(param.Payload);

        if (!payloadMap.containsKey('ContactIdentifier') || String.isEmpty(String.valueOf(payloadMap.get('ContactIdentifier'))))
        {
            Log.error('ContentVersionGetFileQuery', 'validate', 'Param payload does not contain contact reference or contact reference is null');
            throw new Exceptions.UnauthorizedException(String.format(Constants.ERRORMSG_NULLPARAM, new List<String>{ 'ContactIdentifier' }));
        }
        
        String contactIdentifier = String.valueOf(payloadMap.get('ContactIdentifier'));

        if (contactIdentifier.contains('@'))
        {
            Log.error('ContentVersionGetFileQuery', 'validate', 'Contact identifier cannot be a email - {0}', new List<String> { contactIdentifier });
            throw new Exceptions.UnauthorizedException(ERROR_UNAUTHORIZED);
        }

        Contact contact = ContactService.get(contactIdentifier);
        ContentVersion version = getContentVersion(param.Identifier);

        if (contact == null)
        {
            Log.error('ContentVersionGetFileQuery', 'validate', 'No contact found {0}', new List<String>{ param.Identifier });
            throw new Exceptions.UnauthorizedException(ERROR_NOCONTACT);
        }
        else if (version == null)
        {
            Log.error('ContentVersionGetFileQuery', 'validate', 'Content version with {0} id no longer exists', new List<String>{ param.Identifier });
            throw new Exceptions.InvalidParameterException(ERROR_NOFILE);
        }
        else if (restrictedFileTypes.contains(version.Type__c))
        {
            Log.error('ContentVersionGetFileQuery', 'validate', 'Content version {0} not allowed to be downloaded because it is {1} type ',
                new List<String>{ param.Identifier, version.Type__c });
            throw new Exceptions.UnauthorizedException(ERROR_UNAUTHORIZED);
        }

        List<ContentDocumentLink> contentDocLinks = [SELECT Id, ContentDocumentId, LinkedEntityId FROM ContentDocumentLink WHERE ContentDocumentId = :version.ContentDocumentId];

        if (contentDocLinks.size() == 0)
        {
            Log.error('ContentVersionGetFileQuery', 'validate', 'Found no content document links. Cannot authenticate');
            throw new Exceptions.UnauthorizedException(ERROR_UNAUTHORIZED);
        }

        Set<Id> linkedEntityIds = SObjectUtils.getIDs(contentDocLinks, 'LinkedEntityId');

        Set<Id> contactCaseOpportunityIds = new Set<Id>();
        contactCaseOpportunityIds.addAll(SObjectUtils.getIDs(getCases(contact.Id), 'Id'));
        contactCaseOpportunityIds.addAll(SObjectUtils.getIDs(getOpportunities(contact.Id), 'Id'));

        for (Id linkedEntityId : linkedEntityIds)
            if (contactCaseOpportunityIds.contains(linkedEntityId))
                return true;

        Log.error('ContentVersionGetFileQuery', 'validate', 'Failed to find linked enitity with contact');
        throw new Exceptions.UnauthorizedException(ERROR_UNAUTHORIZED);
    }

    private List<Case> getCases(Id contactId)
    {
        return [SELECT Id, ContactId FROM Case WHERE ContactId = :contactId];
    }

    private List<Opportunity> getOpportunities(Id contactId)
    {
        return [SELECT Id, Contact__c FROM Opportunity WHERE Contact__c = :contactId];
    }

    private ContentVersion getContentVersion(String Id)
    {
        List<ContentVersion> versions = [SELECT Id, ContentDocumentId, FileExtension, Title, Type__c, VersionData FROM ContentVersion WHERE Id = :Id];

        if (versions.size() == 0)
            return null;
        
        return versions[0];
    }
}