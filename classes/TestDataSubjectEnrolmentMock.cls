@isTest
public class TestDataSubjectEnrolmentMock
{
    public static hed__Course_Enrollment__c create(User user, hed__Program_Enrollment__c courseEnrolment, String subjectCode, String subjectName, Date startDate)
    {
        return create(user, courseEnrolment, subjectCode, subjectName, startDate, 'In Progress', 'Standard', null, null);
    }

    public static hed__Course_Enrollment__c create(User user, hed__Program_Enrollment__c courseEnrolment, String code, String subject, Date startDate, String status, String enrolmentType, String grade, Decimal mark)
    {
        User setupUser = UserService.getByAlias(Constants.USER_TESTSETUP);
        Account account = TestDataAccountMock.createDepartmentBusiness(setupUser);
        hed__Course__c subjectDef = TestDataSubjectMock.createDefinition(setupUser, code, subject, account.Id);
        hed__Course__c subjectVer = TestDataSubjectMock.createVersion(setupUser, code, subject, account.Id, subjectDef.Id, true, 1);
        hed__Term__c term = TestDataTermMock.create(setupUser, account.Id, startDate);
        hed__Course_Offering__c subjectOffering = TestDataSubjectOfferingMock.createOffering(setupUser, subjectVer, term);

        return create(user, subjectOffering, courseEnrolment, status, enrolmentType, grade, mark);
    }

    public static hed__Course_Enrollment__c create(User user, hed__Course_Offering__c subjectOffering, hed__Program_Enrollment__c courseEnrolment, String status, String enrolmentType, String grade, Decimal mark)
    {
        hed__Course_Enrollment__c subjectEnrolment = new hed__Course_Enrollment__c
        (
            hed__Course_Offering__c = subjectOffering.Id,
            hed__Program_Enrollment__c = courseEnrolment.Id,
            hed__Contact__c = courseEnrolment.hed__Contact__c,
            hed__Account__c = courseEnrolment.hed__Account__c,
            Type__c = enrolmentType,
            Grade__c = grade,
            hed__Grade__c = mark,
            RecordTypeId = RecordTypeUtils.getRecordTypeId(Constants.RECORDTYPE_COURSEENROLLMENT_STUDENT, SObjectType.hed__Course_Enrollment__c)
        );

        String[] convertedStatus = getSubjectEnrolmentStatus(status);
        subjectEnrolment.hed__Status__c = convertedStatus[0];
        subjectEnrolment.StatusResultReason__c = convertedStatus[1];

        System.RunAs(user)
        {
            insert subjectEnrolment;
        }

        return subjectEnrolment;
    }

    public static hed__Course_Enrollment__c createExemption(User user, String subjectCode, String subjectName, hed__Program_Enrollment__c courseEnrolment)
    {
        return createExemption(user, subjectCode, subjectName, courseEnrolment, false);
    }

    public static hed__Course_Enrollment__c createExemption(User user, String subjectCode, String subjectName, hed__Program_Enrollment__c courseEnrolment, Boolean provisional)
    {
        hed__Course_Enrollment__c subjectEnrolment = new hed__Course_Enrollment__c
        (
            hed__Program_Enrollment__c = courseEnrolment.Id,
            hed__Contact__c = courseEnrolment.hed__Contact__c,
            hed__Account__c = courseEnrolment.hed__Account__c,
            ExemptionSubjectCode__c = subjectCode,
            ExemptionSubjectName__c = subjectName,
            Type__c = 'Exemption',
            Grade__c = 'Exemption',
            hed__Status__c = provisional ? 'Interested' : 'Completed',
            StatusResultReason__c = provisional ? null : 'Exempted',
            RecordTypeId = RecordTypeUtils.getRecordTypeId(provisional ? Constants.RECORDTYPE_COURSEENROLLMENT_PROVISIONAL : Constants.RECORDTYPE_COURSEENROLLMENT_STUDENT, SObjectType.hed__Course_Enrollment__c)
        );

        System.RunAs(user)
        {
            insert subjectEnrolment;
        }

        return subjectEnrolment;
    }

    public static hed__Course_Enrollment__c createFacultyEnrolment(User user, hed__Course_Offering__c subjectOffering, Contact facultyContact, String status, String role)
    {
        hed__Course_Enrollment__c subjectEnrolment = new hed__Course_Enrollment__c
        (
            hed__Course_Offering__c = subjectOffering.Id,
            hed__Contact__c = facultyContact.Id,
            Role__c = role,
            StudentsAllocated__c = role == 'Subject Coordinator' ? 20 : 42,
            Type__c = 'Standard',
            RecordTypeId = RecordTypeUtils.getRecordTypeId(Constants.RECORDTYPE_COURSEENROLLMENT_FACULTY, SObjectType.hed__Course_Enrollment__c)
        );

        String[] convertedStatus = getSubjectEnrolmentStatus(status);
        subjectEnrolment.hed__Status__c = convertedStatus[0];
        subjectEnrolment.StatusResultReason__c = convertedStatus[1];

        System.RunAs(user)
        {
            insert subjectEnrolment;
        }

        return subjectEnrolment;
    }

    public static hed__Course_Enrollment__c createProvisionalEnrolment(User user, hed__Course_Offering__c subjectOffering, Contact contact, String status)
    {
        return createProvisionalEnrolment(user, subjectOffering, contact, status, 'Standard');
    }

    public static hed__Course_Enrollment__c createProvisionalEnrolment(User user, hed__Course_Offering__c subjectOffering, Contact contact, String status, String type)
    {
        return createProvisionalEnrolment(user, subjectOffering, contact, status, type, null);
    }

    public static hed__Course_Enrollment__c createProvisionalEnrolment(User user, hed__Course_Offering__c subjectOffering, Contact contact, String status, String type, hed__Program_Enrollment__c courseEnrolment)
    {
        hed__Course_Enrollment__c subjectEnrolment = new hed__Course_Enrollment__c
        (
            hed__Course_Offering__c = subjectOffering.Id,
            hed__Program_Enrollment__c = courseEnrolment != null ? courseEnrolment.Id : null,
            hed__Contact__c = contact.Id,
            Type__c = type,
            RecordTypeId = RecordTypeUtils.getRecordTypeId(Constants.RECORDTYPE_COURSEENROLLMENT_PROVISIONAL, SObjectType.hed__Course_Enrollment__c)
        );

        String[] convertedStatus = getSubjectEnrolmentStatus(status);
        subjectEnrolment.hed__Status__c = convertedStatus[0];
        subjectEnrolment.StatusResultReason__c = convertedStatus[1];

        System.RunAs(user)
        {
            insert subjectEnrolment;
        }

        return subjectEnrolment;
    }

    public static String[] getSubjectEnrolmentStatus(String status)
    {
        String result, reason;

        if (!String.isBlank(status))
        {
            if (status == 'Complete' || status == 'Exempted' || status == 'Transferred')
            {
                result = 'Completed';
                reason = (status == 'Complete' ? 'Studied' : status);
            }
            else if (status == 'Did Not Sit' || status == 'Medical Compassionate' || status == 'Deferred' || status == 'At Risk De-enrolment')
            {
                result = 'Withdrawn';
                reason = status;
            }
            else if (status == 'Withdrawal Before Admin Date' || status == 'Withdrawal Replacement' || status == 'Withdrawal Special Circumstances')
            {
                result = 'Withdrawn';
                reason = status.substring(11);
            }
            else if (status == 'Withdraw Fail' || status == 'Withdraw Not Fail')
            {
                result = 'Withdrawn';
                reason = status.substring(8);
            }
            else if (status == 'In Progress')
            {
                result = 'Studying';
            }
            else
            {
                result = status;
            }
        }

        return new String[] { result, reason };
    }
}