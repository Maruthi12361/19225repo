@isTest
public class ActivitySendNotificationCommandTests 
{
    @testSetup
    static void setup()
    {
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createProspectWarm(user);
    }

    @isTest
    static void CreateTask_NotificationSent()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        User user = UserService.getByAlias('robert.k');
        Contact contact = [SELECT Id, OwnerId, AccountId FROM Contact LIMIT 1];

        // Act
        Test.startTest();
        Integer callCount = Limits.getFutureCalls();
        TestDataTaskMock.createFollowUpTask(user, contact.Id, contact.AccountId, Constants.TASKSUBJECT_FOR_NEW_CONTACT, true);
        callCount = Limits.getFutureCalls() - callCount;
        Task result = [SELECT Id, Notification__c FROM Task LIMIT 1];
        Test.stopTest();

        // Assert
        System.assertEquals(false, result.Notification__c);
        System.assertEquals(1, callCount);
    }

    @isTest
    static void UpdateTask_NotificationSent()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        User user = UserService.getByAlias('robert.k');
        Contact contact = [SELECT Id, OwnerId, AccountId FROM Contact LIMIT 1];
        TestDataTaskMock.createFollowUpTask(user, contact.Id, contact.AccountId, Constants.TASKSUBJECT_FOR_NEW_CONTACT, false);
        Task updatedTask = [SELECT Id, Description, Notification__c FROM Task LIMIT 1];
        updatedTask.Notification__c = true;
        update updatedTask;

        // Act
        Test.startTest();
        Integer callCount = Limits.getFutureCalls();
        updatedTask.Description = 'Updated Comments';
        update updatedTask;
        Task result = [SELECT Id, Description, Notification__c FROM Task LIMIT 1];
        callCount = Limits.getFutureCalls() - callCount;

        Test.stopTest();

        // Assert
        System.assertEquals(false, result.Notification__c);
        System.assertEquals(1, callCount);
    }

    @isTest
    static void CreatTaskWithSystemUser_NoNotificationSent()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = [SELECT Id, OwnerId, AccountId FROM Contact LIMIT 1];

        // Act
        Test.startTest();
        Integer callCount = Limits.getFutureCalls();
        TestDataTaskMock.createFollowUpTask(user, contact.Id, contact.AccountId, Constants.TASKSUBJECT_FOR_NEW_CONTACT, true);
        callCount = Limits.getFutureCalls() - callCount;
        Task result = [SELECT Id, Notification__c FROM Task LIMIT 1];
        Test.stopTest();

        // Assert
        System.assertEquals(true, result.Notification__c);
        System.assertEquals(0, callCount);
    }
}