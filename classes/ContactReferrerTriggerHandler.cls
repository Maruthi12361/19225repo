public class ContactReferrerTriggerHandler extends BaseTriggerHandler
{
    public override void afterInsert()
    {
        ActionResult result = ContactReferrerService.setLeadSourceValue(Trigger.new, Trigger.oldMap, Enums.TriggerContext.AFTER_INSERT);
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);
    }
}