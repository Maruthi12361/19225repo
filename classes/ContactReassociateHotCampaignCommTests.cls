@isTest
public with sharing class ContactReassociateHotCampaignCommTests 
{
    @testSetup
    private static void testSetup()
    {
        TestUtilities.setupHEDA();
    }

    @isTest
    static void ProspectSetPreferredStartDate_AddsProspectToAcquisitionCampaign()
    {
        // Arrange
        Date startDate = Date.today();
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createProspectWarm(user);
        TestDataCampaignMock.createSeries(user, startDate, 2, false);
        TestDataCampaignMock.createAcquisitionCampaignMemberStatus(user);

        List<Campaign> campaigns = [select Id, Name, Status, IsActive, TermStartDate__c from Campaign where Type = 'Acquisition'];
        Map<Id, Contact> oldMap = new Map<Id, Contact>();
        oldMap.put(contact.Id, contact);
        Contact newContact = contact.clone(true, false, true, true);
        newContact.Preferred_Start_Date__c = StringUtils.formatDate(startDate);
        newContact.Status__c = Constants.STATUS_HOT;

        // Act
        Test.startTest();
        ActionResult result = ContactService.reassociateAcquisitionCampaign(new List<Contact> { newContact }, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        Test.stopTest();

        // Assert
        contact =
            [
                SELECT Id, PreferredCampaign__r.TermStartDate__c, PreferredCampaign__r.Type,
                (
                    SELECT Id, Status, CampaignId
                      FROM CampaignMembers
                     WHERE Campaign.Type IN ('Acquisition')
                )
                  FROM Contact
                 WHERE Id = : contact.Id
            ];

        System.assertEquals(ResultStatus.SUCCESS, result.Status);
        System.assertEquals(1, contact.CampaignMembers.size());
        System.assertEquals('Sent', contact.CampaignMembers[0].Status);
        System.assertEquals('Acquisition', contact.PreferredCampaign__r.Type);
        System.assertEquals(startDate, contact.PreferredCampaign__r.TermStartDate__c);
    }

    @isTest
    static void ProspectChangePreferredStartDate_MovesProspectToAcquisitionCampaign()
    {
        // Arrange
        Date startDate = Date.today();
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createProspectWarm(user);
        TestDataCampaignMock.createSeries(user, startDate, 2, false);
        TestDataCampaignMock.createAcquisitionCampaignMemberStatus(user);

        contact.Preferred_Start_Date__c = StringUtils.formatDate(startDate);
        contact.Status__c = Constants.STATUS_HOT;
        Map<Id, Contact> oldMap = new Map<Id, Contact>();
        ActionResult result = ContactService.reassociateAcquisitionCampaign(new List<Contact> { contact }, oldMap, Enums.TriggerContext.AFTER_INSERT);

        oldMap.put(contact.Id, contact);
        Contact newContact = contact.clone(true, false, true, true);
        newContact.Preferred_Start_Date__c = StringUtils.formatDate(startDate.addDays(56));

        // Act
        Test.startTest();
        result = ContactService.reassociateAcquisitionCampaign(new List<Contact> { newContact }, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        Test.stopTest();

        // Assert
        contact =
            [
                SELECT Id, PreferredCampaign__r.TermStartDate__c, PreferredCampaign__r.Type,
                (
                    SELECT Id, Status, CampaignId
                      FROM CampaignMembers
                     WHERE Campaign.Type IN ('Acquisition')
                )
                  FROM Contact
                 WHERE Id = : contact.Id
            ];

        System.assertEquals(ResultStatus.SUCCESS, result.Status);
        System.assertEquals(2, contact.CampaignMembers.size());
        System.assertEquals('Moved', contact.CampaignMembers[0].Status);
        System.assertEquals('Sent', contact.CampaignMembers[1].Status);
        System.assertEquals('Acquisition', contact.PreferredCampaign__r.Type);
        System.assertEquals(startDate.addDays(56), contact.PreferredCampaign__r.TermStartDate__c);
    }

    @isTest
    static void ProspectReSelectPreferredStartDate_ResetsCampaignMemberStatus()
    {
        // Arrange
        Date startDate = Date.today();
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createProspectWarm(user);
        TestDataCampaignMock.createSeries(user, startDate, 2, false);
        TestDataCampaignMock.createAcquisitionCampaignMemberStatus(user);

        contact.Preferred_Start_Date__c = StringUtils.formatDate(startDate);
        contact.Status__c = Constants.STATUS_HOT;
        Map<Id, Contact> oldMap = new Map<Id, Contact>();
        ActionResult result = ContactService.reassociateAcquisitionCampaign(new List<Contact> { contact }, oldMap, Enums.TriggerContext.AFTER_INSERT);
        oldMap.put(contact.Id, contact);
        Contact newContact = contact.clone(true, false, true, true);
        newContact.Preferred_Start_Date__c = StringUtils.formatDate(startDate.addDays(56));
        result = ContactService.reassociateAcquisitionCampaign(new List<Contact> { newContact }, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        oldMap.put(newContact.Id, newContact);
        Contact newerContact = newContact.clone(true, false, true, true);
        newerContact.Preferred_Start_Date__c = StringUtils.formatDate(startDate);

        // Act
        Test.startTest();
        result = ContactService.reassociateAcquisitionCampaign(new List<Contact> { newerContact }, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        Test.stopTest();

        // Assert
        contact =
            [
                SELECT Id, PreferredCampaign__r.TermStartDate__c, PreferredCampaign__r.Type,
                (
                    SELECT Id, Status, CampaignId
                      FROM CampaignMembers
                     WHERE Campaign.Type IN ('Acquisition')
                )
                  FROM Contact
                 WHERE Id = : contact.Id
            ];

        System.assertEquals(ResultStatus.SUCCESS, result.Status);
        System.assertEquals(2, contact.CampaignMembers.size());
        System.assertEquals('Sent', contact.CampaignMembers[0].Status);
        System.assertEquals('Moved', contact.CampaignMembers[1].Status);
        System.assertEquals('Acquisition', contact.PreferredCampaign__r.Type);
        System.assertEquals(startDate, contact.PreferredCampaign__r.TermStartDate__c);
    }
}