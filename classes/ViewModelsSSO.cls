public class ViewModelsSSO
{
    public class UserGroup
    {
        public String objectId { get; set; }
        public String displayName { get; set; }
        public Boolean securityEnabled { get; set; }
    }

    public class EmailLoginDetails implements Interfaces.IViewModel
    {
        public Id contactId { get; set; }
        public String contactFirstName { get; set; }
        public String contactLastName { get; set; }
        public String contactEmail { get; set; }
        public String singleSignOnId {get; set; }
        public String password { get; set; }
        public String trackingURL { get; set; }

        public EmailLoginDetails()
        {
        }

        public EmailLoginDetails(Id contactId, String firstName, String lastName, String email, String password, String trackingURL)
        {
            this.contactId = contactId;
            this.contactFirstName = firstName;
            this.contactLastName = lastName;
            this.contactEmail = email;
            this.password = password;
            this.trackingURL = trackingURL;
        }

        public EmailLoginDetails(Id contactId, String firstName, String lastName, String email, String ssoId, String password, String trackingURL)
        {
            this.contactId = contactId;
            this.contactFirstName = firstName;
            this.contactLastName = lastName;
            this.contactEmail = email;
            this.singleSignOnId = ssoId;
            this.password = password;
            this.trackingURL = trackingURL;
        }
    }
}