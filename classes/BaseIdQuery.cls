public abstract class BaseIdQuery 
{
    public abstract List<SObject> query(Set<Id> ids);
}