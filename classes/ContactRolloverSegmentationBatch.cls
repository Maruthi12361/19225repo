public class ContactRolloverSegmentationBatch extends BaseBatchCommand 
{    
    String cerebroIDs;
    Date today = System.today();
    String query = 
        'SELECT Id, Cerebro_Student_ID__c, IsEnroledCurrentTerm__c, IsEnroledNextTerm__c, IsEnroledFutureTerm__c ' +
        'FROM Contact ' +
        'WHERE Current_Course_Status__c = \'Ongoing\' AND Cerebro_Student_ID__c <> null';

    public ContactRolloverSegmentationBatch()
    {

    }
    
    public ContactRolloverSegmentationBatch(String stringParam)
    {
        if (stringParam != null && String.isNotBlank(stringParam))
            cerebroIDs = stringParam;
    }
    
    public override Database.QueryLocator query(Database.BatchableContext context)
    {
        if (cerebroIDs != null)
            query += ' AND Cerebro_Student_ID__c IN (' + cerebroIDs + ')';

        query += ' ORDER BY Cerebro_Student_ID__c';        
        return Database.getQueryLocator(query);
    }

    public override void command(Database.BatchableContext context, List<sObject> scope)
    {
        List<Date> termDates = getTermDates('DMBA');
        Date currentTermStartDate = termDates[0];
        Date nextTermStartDate = termDates[1];
        Date futureTermStartDate = termDates[2];
        List<Contact> updatedContacts = new List<Contact>();
        
        if (currentTermStartDate == null || nextTermStartDate == null || futureTermStartDate == null)
        {
            Log.info(this.className, 'command', 'Unable to calculate the term dates, aborting the batch.');
            return;
        }

        Map<Id, List<hed__Course_Enrollment__c>> enrolmentMap = new Map<Id, List<hed__Course_Enrollment__c>>();
        List<hed__Course_Enrollment__c> enrolments =
            [
                SELECT Id, hed__Contact__c, hed__Course_Offering__r.hed__Start_Date__c
                FROM hed__Course_Enrollment__c
                WHERE hed__Course_Offering__r.hed__Start_Date__c >= :currentTermStartDate.addDays(-1) AND
                    Type__c NOT IN ('Exemption', 'Resit') AND hed__Contact__c IN : scope AND RecordType.DeveloperName = 'Student'
                ORDER BY hed__Contact__c, hed__Course_Offering__r.hed__Start_Date__c
            ];
        
        for (hed__Course_Enrollment__c enrolment : enrolments)
        {
            if (enrolmentMap.containsKey(enrolment.hed__Contact__c))
            {
                List<hed__Course_Enrollment__c> enrolmentList = enrolmentMap.get(enrolment.hed__Contact__c);
                enrolmentList.add(enrolment);
                enrolmentMap.put(enrolment.hed__Contact__c, enrolmentList);
            }
            else
                enrolmentMap.put(enrolment.hed__Contact__c, new List<hed__Course_Enrollment__c> { enrolment });
        }
        
        for (Contact student : (List<Contact>) scope)
        {
            Boolean isDirty = false;
            
            if (!enrolmentMap.containsKey(student.Id))
                continue;

            List<hed__Course_Enrollment__c> enrolmentList = enrolmentMap.get(student.Id);
            
            for (hed__Course_Enrollment__c enrolment : enrolmentList)
            {
                Date subjectStartDate = enrolment.hed__Course_Offering__r.hed__Start_Date__c;
                
                if (subjectStartDate >= currentTermStartDate && subjectStartDate < nextTermStartDate && !student.IsEnroledCurrentTerm__c)
                {
                    student.IsEnroledCurrentTerm__c = true;
                    isDirty = true;
                }
                
                if (subjectStartDate >= nextTermStartDate && subjectStartDate < futureTermStartDate && !student.IsEnroledNextTerm__c)
                {
                    student.IsEnroledNextTerm__c = true;
                    isDirty = true;
                }
                
                if (subjectStartDate >= futureTermStartDate && !student.IsEnroledFutureTerm__c)
                {
                    student.IsEnroledFutureTerm__c = true;
                    isDirty = true;
                    break;
                }
            }

            if (isDirty)            
                updatedContacts.add(student);
        }
        
        if (updatedContacts.size() > 0)
            uow.registerDirty(updatedContacts);
    }
    
    private List<Date> getTermDates(String programPortfolio) 
    {
        List<Campaign> allCampaignList = 
        [
            SELECT Id, TermStartDate__c
            FROM Campaign
            WHERE Type = 'Acquisition' AND Status NOT IN ('Aborted', 'Completed') AND IsActive = true AND Program_Portfolio__c = : programPortfolio
            ORDER BY TermStartDate__c
        ];
        Date currentTermStartDate, nextTermStartDate, futureTermStartDate;
        
        for(Campaign camp : allCampaignList)
        {
            if (camp.TermStartDate__c <= today)
            {
                if (currentTermStartDate == null)
                    currentTermStartDate = camp.TermStartDate__c;
                else
                {
                    if (currentTermStartDate < camp.TermStartDate__c)
                        currentTermStartDate = camp.TermStartDate__c;
                }
            }
            else
            {
                if (nextTermStartDate == null)
                    nextTermStartDate = camp.TermStartDate__c;
                else
                {
                    if (futureTermStartDate == null)
                    {
                        futureTermStartDate = camp.TermStartDate__c;
                        break;
                    }
                }
            }
        }
        
        Log.info(this.className, 'getTermDates', 'Query start dates from Acquisition Campaigns, current term: {0}, next term: {1}, future term: {2}', 
                 new List<String> { String.valueOf(currentTermStartDate), String.valueOf(nextTermStartDate), String.valueOf(futureTermStartDate) });        
        return new List<Date>{currentTermStartDate, nextTermStartDate, futureTermStartDate};
    }
}