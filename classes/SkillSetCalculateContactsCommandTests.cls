@isTest
public class SkillSetCalculateContactsCommandTests
{
    @testSetup
    static void setup()
    {
        TestUtilities.setupHEDA();
    }

    @isTest
    static void InsertSkillSet_IncreasesNumberOfSkilledContacts()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact facultyContact = TestDataContactMock.createFacultyContact(user);
        hed__Course__c subjectDef = TestDataSubjectMock.createDefinitionLEAD(user);
        SkillSet__c skillSet1 = TestDataSkillSetMock.createFaculty(user, facultyContact, subjectDef);
        SkillSet__c skillSet2 = TestDataSkillSetMock.createFaculty(user, facultyContact, subjectDef);
        Map<Id, SkillSet__c> oldMap = new Map<Id, SkillSet__c>();
        List<SkillSet__c> items = new List<SkillSet__c>();
        items.add(skillSet2);

        // Act
        Test.startTest();
        ActionResult result = SkillSetService.calculateNumberSkilledContacts(items, oldMap, Enums.TriggerContext.AFTER_INSERT);
        Test.stopTest();

        // Assert
        hed__Course__c updatedCourse = [SELECT Id, NumberOfSkilledContacts__c FROM hed__Course__c WHERE Id =: skillSet2.SubjectDefinition__c LIMIT 1];
        System.assertEquals(ResultStatus.SUCCESS, result.Status);
        System.assertEquals(2, updatedCourse.NumberOfSkilledContacts__c);
    }

    @isTest
    static void DeleteSkillSet_DecreasesNumberOfSkilledContacts()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact facultyContact = TestDataContactMock.createFacultyContact(user);
        hed__Course__c subjectDef = TestDataSubjectMock.createDefinitionLEAD(user);
        SkillSet__c skillSet1 = TestDataSkillSetMock.createFaculty(user, facultyContact, subjectDef);
        SkillSet__c skillSet2 = TestDataSkillSetMock.createFaculty(user, facultyContact, subjectDef);
        Map<Id, SkillSet__c> oldMap = new Map<Id, SkillSet__c>();
        List<SkillSet__c> items = new List<SkillSet__c>();
        items.add(skillSet2);

        System.RunAs(user)
        {
            delete skillSet2;
        }

        // Act
        Test.startTest();
        ActionResult result = SkillSetService.calculateNumberSkilledContacts(items, oldMap, Enums.TriggerContext.AFTER_DELETE);
        Test.stopTest();

        // Assert
        hed__Course__c updatedCourse = [SELECT Id, NumberOfSkilledContacts__c FROM hed__Course__c WHERE Id =: skillSet2.SubjectDefinition__c LIMIT 1];
        System.assertEquals(ResultStatus.SUCCESS, result.Status);
        System.assertEquals(1, updatedCourse.NumberOfSkilledContacts__c);
    }

    @isTest
    static void UpdateSkillSet_UpdatesBothParentsNumberOfSkilledContacts()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact facultyContact = TestDataContactMock.createFacultyContact(user);
        hed__Course__c subjectDef1 = TestDataSubjectMock.createDefinitionLEAD(user);
        hed__Course__c subjectDef2 = TestDataSubjectMock.createDefinitionMMGT(user);
        SkillSet__c skillSet1 = TestDataSkillSetMock.createFaculty(user, facultyContact, subjectDef1);
        SkillSet__c skillSet2 = TestDataSkillSetMock.createFaculty(user, facultyContact, subjectDef1);
        Map<Id, SkillSet__c> oldMapOriginal = new Map<Id, SkillSet__c>();
        List<SkillSet__c> itemsOriginal = new List<SkillSet__c>();
        itemsOriginal.add(skillSet2);

        ActionResult resultOriginal = SkillSetService.calculateNumberSkilledContacts(itemsOriginal, oldMapOriginal, Enums.TriggerContext.AFTER_INSERT);
        hed__Course__c originalCourse = [SELECT Id, NumberOfSkilledContacts__c FROM hed__Course__c WHERE Id =: skillSet2.SubjectDefinition__c LIMIT 1];
        double originalNumber = originalCourse.NumberOfSkilledContacts__c;

        Map<Id, SkillSet__c> oldMap = new Map<Id, SkillSet__c>{ skillSet2.id => skillSet2 };
        List<SkillSet__c> items = new List<SkillSet__c>();
        SkillSet__c newSkillSet = skillSet2.clone(true, false, true, true);
        newSkillSet.SubjectDefinition__c = subjectDef2.Id;
        items.add(newSkillSet);

        System.RunAs(user)
        {
            update newSkillSet;
        }

        // Act
        Test.startTest();
        ActionResult result = SkillSetService.calculateNumberSkilledContacts(items, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        Test.stopTest();

        // Assert
        hed__Course__c updatedCourse = [SELECT Id, NumberOfSkilledContacts__c FROM hed__Course__c WHERE Id =: newSkillSet.SubjectDefinition__c LIMIT 1];
        hed__Course__c updatedCourseOriginal = [SELECT Id, NumberOfSkilledContacts__c FROM hed__Course__c WHERE Id =: skillSet2.SubjectDefinition__c LIMIT 1];

        System.assertEquals(ResultStatus.SUCCESS, result.Status);
        System.assertEquals(1, updatedCourse.NumberOfSkilledContacts__c);
        System.assertEquals(1, updatedCourseOriginal.NumberOfSkilledContacts__c);
        System.assertEquals(2, originalNumber);
    }
}