@isTest
public class DataActionBatchGroupTests 
{
    @testSetup
    static void setup()
    {
        TestUtilities.setupHEDA();
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact1 = TestDataContactMock.createProspect(user, 'John', 'Smith', '', '0455566756', 'john.smith@gmail.com', Constants.STATUS_COLD, 'A001588498', 'Australia', 'Website (Organic)');
        Contact contact2 = TestDataContactMock.createProspect(user, 'David', 'Smith', '', '0455566757', 'david.smith@gmail.com', Constants.STATUS_COLD, 'A001588499', 'Australia', 'Website (Organic)');
        Contact contact3 = TestDataContactMock.createProspect(user, 'Joe', 'Smith', '', '0455566758', 'joe.smith@gmail.com', Constants.STATUS_COLD, 'A001588500', 'Australia', 'Website (Organic)');
        Contact contact4 = TestDataContactMock.createProspect(user, 'Sam', 'Smith', '', '0455566759', 'sam.smith@gmail.com', Constants.STATUS_COLD, 'A001588501', 'Australia', 'Website (Organic)');
        Contact contact5 = TestDataContactMock.createProspect(user, 'Michael', 'Smith', '', '0455566760', 'michael.smith@gmail.com', Constants.STATUS_COLD, 'A001588502', 'Australia', 'Website (Organic)');
    }

    @isTest
    static void ContactUpdatePortfolio_CanadaNZOtherDomesticStaff()
    {
        // Arrange
        List<Contact> contacts = [SELECT Id, Email, ProgramPortfolioBackup__c, MailingCountry FROM Contact];
        
        for (Contact con: contacts)
        {
            if (con.Email == 'john.smith@gmail.com')
                con.ProgramPortfolioBackup__c = 'DMBA';
            else if (con.Email == 'david.smith@gmail.com')
            {
                con.ProgramPortfolioBackup__c = 'IMBA';
                con.MailingCountry = 'Canada';
            }
            else if (con.Email == 'joe.smith@gmail.com')
            {
                con.ProgramPortfolioBackup__c = 'IMBA';
                con.MailingCountry = 'New Zealand';
            }
            else if (con.Email == 'sam.smith@gmail.com')
            {
                con.ProgramPortfolioBackup__c = 'IMBA';
                con.MailingCountry = 'Vietnam';
            }
            else if (con.Email == 'michael.smith@gmail.com')
            {
                con.ProgramPortfolioBackup__c = 'Staff MBA';
            }
        }

        update contacts;
        
        List<DataActionBatch> actions = new List<DataActionBatch>
        {
            new DataActionBatch('Contact', new List<ViewModels.DataAction> { new ViewModels.DataAction(Enums.DataUpdateAction.WRITE, 'Portfolio__c', 'Domestic')},'ProgramPortfolioBackup__c like \'D%\''),
            new DataActionBatch('Contact', new List<ViewModels.DataAction> { new ViewModels.DataAction(Enums.DataUpdateAction.WRITE, 'Portfolio__c', 'International (Canada)')},'ProgramPortfolioBackup__c like \'I%\' AND MailingCountry = \'Canada\''),
            new DataActionBatch('Contact', new List<ViewModels.DataAction> { new ViewModels.DataAction(Enums.DataUpdateAction.WRITE, 'Portfolio__c', 'International (New Zealand)')},'ProgramPortfolioBackup__c like \'I%\' AND MailingCountry = \'New Zealand\''),
            new DataActionBatch('Contact', new List<ViewModels.DataAction> { new ViewModels.DataAction(Enums.DataUpdateAction.WRITE, 'Portfolio__c', 'International (Other)')},'ProgramPortfolioBackup__c like \'I%\' AND (NOT MailingCountry IN (\'Canada\', \'New Zealand\'))'),
            new DataActionBatch('Contact', new List<ViewModels.DataAction> { new ViewModels.DataAction(Enums.DataUpdateAction.WRITE, 'Portfolio__c', 'Staff')},'ProgramPortfolioBackup__c like \'Staff%\'')
        };

        // Act
        test.startTest();
        DataActionBatchGroup bg = new DataActionBatchGroup(actions);
        bg.executeBatches(50);
        test.stopTest();

        // Assert
        Contact domestic = [SELECT Id, Portfolio__c FROM Contact WHERE Email = 'john.smith@gmail.com' LIMIT 1 ];
        Contact canada = [SELECT Id, Portfolio__c FROM Contact WHERE Email = 'david.smith@gmail.com' LIMIT 1 ];
        Contact nz = [SELECT Id, Portfolio__c FROM Contact WHERE Email = 'joe.smith@gmail.com' LIMIT 1 ];
        Contact other = [SELECT Id, Portfolio__c FROM Contact WHERE Email = 'sam.smith@gmail.com' LIMIT 1 ];
        Contact staff = [SELECT Id, Portfolio__c FROM Contact WHERE Email = 'michael.smith@gmail.com' LIMIT 1 ];

        System.assertEquals('Domestic', domestic.Portfolio__c, 'Unexpected Portfolio value');
        System.assertEquals('International (Canada)', canada.Portfolio__c, 'Unexpected Portfolio value');
        System.assertEquals('International (New Zealand)', nz.Portfolio__c, 'Unexpected Portfolio value');
        System.assertEquals('International (Other)', other.Portfolio__c, 'Unexpected Portfolio value');
        System.assertEquals('Staff', staff.Portfolio__c, 'Unexpected Portfolio value');
    }
}