public class ActivitySaveCommand extends BaseViewModelCommand
{
    public override List<Interfaces.IViewModel> validate(List<Interfaces.IViewModel> items)
    {
        return items;
    }
    
    public override ActionResult command(List<Interfaces.IViewModel> items)
    {        
        List<ViewModelsAcquisition.Activity> activities = typeCast(items);
        
        for (ViewModelsAcquisition.Activity activity : activities)
        {
            Task task = new Task();
            
            task.OwnerId = activity.AssignedTo;
            task.WhatId = activity.RelatedTo;
            task.WhoId = activity.Name;
            task.Type = activity.TaskType;
            task.RecordTypeId = activity.TaskRecordType;
            task.Priority = activity.Priority;
            task.Subject = activity.Subject;
            task.Status = activity.Status;
            task.ActivityDate = activity.DueDate;
            task.Description = activity.Comments;
            task.Notification__c = activity.NotifyOwner;
            
            uow.registerNew(task);
            
            Log.info(this.className, 'command', 'Creating a new {0} Task: {1}', new List<String> {task.Type, task.Subject});
        }
        
        return new ActionResult(ResultStatus.SUCCESS);
    }
    
    private List<ViewModelsAcquisition.Activity> typeCast(List<Interfaces.IViewModel> items)
    {
        List<ViewModelsAcquisition.Activity> activities = new List<ViewModelsAcquisition.Activity>();
        
        for (Interfaces.IViewModel item : items)
            activities.add((ViewModelsAcquisition.Activity) item);

        return activities;
    }
}