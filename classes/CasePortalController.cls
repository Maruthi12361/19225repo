public class CasePortalController 
{
    @AuraEnabled
    public static List<Case> getMyCases()
    {
        User user = [SELECT Id, Name, ContactId FROM User WHERE Id=: UserInfo.getUserId()];        
        
        try
        {
            List<String> recordTypes = new List<String> {Constants.RECORDTYPE_CASE_STUDENT_SUPPORT, Constants.RECORDTYPE_CASE_AWARD, Constants.RECORDTYPE_CASE_EXEMPTIONS};
                
            return [SELECT Id, CaseNumber, Subject, Status, Owner.Name, Contact.Id, CreatedDate, ClosedDate, LastModifiedDate
                    FROM Case
                    WHERE Contact.Id =: user.ContactId AND StudentPortalVisibility__c = true 
                    AND RecordType.DeveloperName IN : recordTypes
                    ORDER BY CaseNumber DESC];
        }
        catch (Exception ex)
        {
            Log.error('MyCasesController', 'getMyCases', 'Failed to retrive Cases for ContactId: ' + user.ContactId, ex);
            throw ex;
        }
    }
    
    @AuraEnabled
    public static Case getNewCaseInfo(String caseId, List<String> fileIds)
    {
        try
        {
            ContentDocumentService.shareWith(caseId, StringUtils.toIdSet(fileIds));
            
            return [SELECT Id, CaseNumber, Subject, Status, Owner.Name, Contact.Id, CreatedDate, ClosedDate, LastModifiedDate
                    FROM Case
                    WHERE Id =: caseId];
        }
        catch (Exception ex)
        {
            Log.error('MyCasesController', 'getCaseNumber', 'Failed to retrive Cases for Id: ' + caseId, ex);
            throw ex;
        }
    }
    
    @AuraEnabled
    public static Boolean deleteFiles(List<String> fileIds)
    {
        try
        {
            for (String fileId: fileIds)
                ContentDocumentService.removeFile(fileId);
            
            return true;
        }
        catch (Exception ex)
        {
            Log.error('MyCasesController', 'deleteFiles', 'Failed to delete File: ' + fileIds, ex);
            return false;
        }
    }
}