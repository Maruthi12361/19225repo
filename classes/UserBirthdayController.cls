/* Controller class used for the HiveBirthdayList component used in HIVE to display the daily birthdays */
public class UserBirthdayController 
{
    @AuraEnabled
    public static List<User> getBirthdayPhotos() 
    {
        String birthdate = Datetime.now().format('dd MM'); //'21 12'; //

        return [SELECT Id, Name, Title, SmallPhotoUrl FROM User Where IsActive = true AND Include_On_Org_Chart__c = true AND Birth_Date__c = :birthdate];
    }
}