@isTest
public class ContactSendWelcomeSMSCommandTests
{
    @testSetup
    static void setup()
    {
        TestUtilities.setupHEDA();
    }

    @isTest
    static void CreateContact_WelcomeSMSSent()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);

        // Act
        Test.startTest();
        Contact contact = TestDataContactMock.createProspect(user, 'Domestic', 'Prospect-MBA', '', '+61455789369', 'domestic.mba@gmail.com', 'Cold', 'A0000000000','','', true, false);
        Test.stopTest();

        // Assert
        Contact result = [SELECT Id, WelcomeProspectSMSSent__c FROM Contact WHERE Email = 'domestic.mba@gmail.com'];
        System.assertEquals(true, result.WelcomeProspectSMSSent__c);
    }

    @isTest
    static void CreateContactWhenNetworkServiceDown_WelcomeSMSSentFailed()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);

        // Act
        Test.startTest();
        Contact contact = TestDataContactMock.createProspect(user, 'Network-Error', 'Test', '', '+61000000000', 'domestic.test@gmail.com', 'Cold', 'X0000000000','','', true, false);
        Test.stopTest();

        // Assert
        Contact result = [SELECT Id, WelcomeProspectSMSSent__c FROM Contact WHERE Email = 'domestic.test@gmail.com'];
        System.assertEquals(false, result.WelcomeProspectSMSSent__c);
    }
}