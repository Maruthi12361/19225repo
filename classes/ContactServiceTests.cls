@isTest
public class ContactServiceTests
{
    @testSetup
    private static void testSetup()
    {
        TestUtilities.setupHEDA();
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        TestDataCountryPhoneCodeMock.create(user, 'England', '44', 'GB');
        TestDataCountryPhoneCodeMock.create(user, 'Australia', '61', 'AU');
    }

    @isTest
    static void FormatPhoneNumberDomesticOnInsert_BacksUpPhoneFieldsAndUpdatesFormatsWherePossible()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createStudent(user, 'A0000000000', false);
        contact.MobilePhoneCountry__c = 'England';

        // Act
        Test.startTest();

        ActionResult result = ContactService.formatPhoneNumbers(new List<Contact> { contact }, null, Enums.TriggerContext.BEFORE_INSERT);
        Test.stopTest();

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.Status);
        System.assertEquals('0408815279', contact.HomePhone);
        System.assertEquals('61408815279', contact.MobilePhone);
        System.assertEquals('041345', contact.OtherPhone);
        System.assertEquals('0408815279', contact.BackupHomePhone__c);
        System.assertEquals('61408815279', contact.Backup_Mobile_Phone__c);
        System.assertEquals('041345', contact.BackupOtherPhone__c);
        System.assertEquals('GB', contact.MobileLocale__c);
    }

    @isTest
    static void FormatPhoneNumberDomesticOnUpdate_BacksUpPhoneFieldsAndUpdatesFormatsWherePossible()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createStudent(user, 'A0000000000', false);
        ActionResult result = ContactService.formatPhoneNumbers(new List<SObject> { contact }, null, Enums.TriggerContext.BEFORE_INSERT);

        // Act
        Test.startTest();

        Contact updated = contact.clone(true, false, true, true);
        updated.HomePhone = '0401112223';
        updated.MobilePhone = '61401112223';
        updated.OtherPhone = '543104';
        updated.MobilePhoneCountry__c = 'Australia';

        result = ContactService.formatPhoneNumbers(new List<SObject> { updated }, new Map<Id, SObject>(new List<Contact> { contact }), Enums.TriggerContext.BEFORE_UPDATE);

        Test.stopTest();

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.Status);
        System.assertEquals('0401112223', updated.HomePhone);
        System.assertEquals('61401112223', updated.MobilePhone);
        System.assertEquals('543104', updated.OtherPhone);
        System.assertEquals('0408815279', updated.BackupHomePhone__c);
        System.assertEquals('61408815279', updated.Backup_Mobile_Phone__c);
        System.assertEquals('041345', updated.BackupOtherPhone__c);
        System.assertEquals('AU', updated.MobileLocale__c);
    }

    @isTest
    static void UpdateRolloverDetailsWithoutCerebroData_ReturnsErrorMessage()
    {
        // Arrange
        String message;
        String cerebroID = 'A001641336';
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        TestDataCampaignMock.create(user, Date.today().addDays(56), 'Domestic', 'MBA', true);
        Contact student = TestDataContactMock.createStudent(user, cerebroID);

        // Act
        try
        {
            ContactService.updateRolloverDetails(new List<Contact> {new Contact(Id = student.Id)});
        }
        catch(Exception ex)
        {
            message = ex.getMessage();
        }

        // Assert
        System.assertEquals(true, message.contains('There is no valid Course Enrolments for this contact.'));
    }

    @isTest
    static void UpdateRolloverDetailsWithCorrectCerebroID_ReturnsSuccessfulMessage()
    {
        // Arrange
        String cerebroID = 'A001641336';
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        TestDataCampaignMock.create(user, Date.today().addDays(56), 'Domestic', 'MBA', true);
        Contact student = TestDataContactMock.createStudent(user, cerebroID);
        Account course = TestDataAccountMock.createCourseMBA(user);
        hed__Program_Enrollment__c courseEnrolment = TestDataCourseEnrolmentMock.create(user, course.Id, student.Id, RolloverConstantsDefinition.CS_ONGOING, RolloverConstantsDefinition.STAGE_ENROLLED, Date.today().addDays(1));

        // Act
        ActionResult result = ContactService.updateRolloverDetails(new List<Contact> {new Contact(Id = student.Id)});

        // Assert
        Contact contact =
            [
                SELECT Id,Current_Course_Status__c,FeeHelp_Eligibility__c,Course_Start_Date__c,Next_Followup_Date__c
                  FROM Contact
                 WHERE Id = : student.Id
            ];

        System.assertEquals(ResultStatus.SUCCESS, result.Status);
        System.assertEquals(RolloverConstantsDefinition.CS_ONGOING, contact.Current_Course_Status__c);
        System.assertEquals(RolloverConstantsDefinition.FEEHELP_FHE, contact.FeeHelp_Eligibility__c);
        System.assertEquals(Date.today(), contact.Course_Start_Date__c);
        system.assertEquals(Date.today().addDays(1), contact.Next_Followup_Date__c);
    }
}