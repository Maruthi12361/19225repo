@isTest
public class PhoneNumberUtilitiesTests
{
    @testSetup 
    public static void setup()
    {
        TestUtilities.setupHEDA();
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        TestDataCountryPhoneCodeMock.create(user, 'England', '44', 'GB');
        TestDataCountryPhoneCodeMock.create(user, 'Australia', '61', 'AU');
        TestDataCountryPhoneCodeMock.create(user, 'LunarColony', '666', null);
    }

    @isTest
    public static void cleanPhoneNumberWithInvalidCharacters_ReturnsCleanedPhoneNumber()
    {
        // Arrange
        String phoneNumber1 = '44++ *1* $517 (7123456) + ';
        String phoneNumber2 = '+441+ + 517 712+(3456)+';
        String phoneNumber3 = '+44 151jhfgdhjd 771++23456 HGD';
        String phoneNumber4 = '++++4++415-1771(23456) ';
        String expected = '4415177123456';

        // Act
        String result1 = PhoneNumberUtilities.cleanNumber(phoneNumber1);
        String result2 = PhoneNumberUtilities.cleanNumber(phoneNumber2);
        String result3 = PhoneNumberUtilities.cleanNumber(phoneNumber3);
        String result4 = PhoneNumberUtilities.cleanNumber(phoneNumber4);

        // Assert
        System.assertEquals(expected, result1);
        System.assertEquals('+' + expected, result2);
        System.assertEquals('+' + expected, result3);
        System.assertEquals('+' + expected, result4);
    }

    @isTest
    public static void cleanPhoneNumber_ReturnsUnchangedPhoneNumber()
    {
        // Arrange
        String phoneNumber1 = '00115177123456';
        String phoneNumber2 = '+4415177123456';
        String phoneNumber3 = '';
        String phoneNumber4 = '    ';
        String phoneNumber5 = null;

        // Act
        String result1 = PhoneNumberUtilities.cleanNumber(phoneNumber1);
        String result2 = PhoneNumberUtilities.cleanNumber(phoneNumber2);
        String result3 = PhoneNumberUtilities.cleanNumber(phoneNumber3);
        String result4 = PhoneNumberUtilities.cleanNumber(phoneNumber4);
        String result5 = PhoneNumberUtilities.cleanNumber(phoneNumber5);

        // Assert
        System.assertEquals(phoneNumber1, result1);
        System.assertEquals(phoneNumber2, result2);
        System.assertEquals(phoneNumber3, result3);
        System.assertEquals(phoneNumber3, result4);
        System.assertEquals(phoneNumber3, result5);
    }

    @isTest
    public static void usingCountryName_GetCountryCode()
    {
        // Arrange
        String countryNameCapitalized = 'Australia';
        String countryNameUncapitalized = 'australia';
        String countryNameMixed = 'auSTraLIa';
        String countryNameMixedWithBlank = '  auSTraLIa  ';
        String countryNameBlank = '';
        String countryNameNull = null;
        String expectedCode = 'AU';

        // Act
        String result1 = PhoneNumberUtilities.getCountryCode(countryNameCapitalized);
        String result2 = PhoneNumberUtilities.getCountryCode(countryNameUncapitalized);
        String result3 = PhoneNumberUtilities.getCountryCode(countryNameMixed);
        String result4 = PhoneNumberUtilities.getCountryCode(countryNameMixedWithBlank);
        String result5 = PhoneNumberUtilities.getCountryCode(countryNameBlank);
        String result6 = PhoneNumberUtilities.getCountryCode(countryNameNull);

        // Assert
        System.assertEquals(expectedCode, result1);
        System.assertEquals(expectedCode, result2);
        System.assertEquals(expectedCode, result3);
        System.assertEquals(expectedCode, result4);
        System.assertEquals(null, result5);
        System.assertEquals(null, result6);
    }

    @isTest
    public static void usingCountryCode_GetCountryName() 
    {
        // Arrange
        String countryCodeCapitalized = 'AU';
        String countryCodeUncapitalized = 'au';
        String countryCodeMixed = 'aU';
        String countryCodeMixedWithBlank = ' aU   ';
        String countryCodeBlank = '';
        String countryCodeNull = null;
        String expectedCountry = 'Australia';

        // Act
        String result1 = PhoneNumberUtilities.getCountryName(countryCodeCapitalized);
        String result2 = PhoneNumberUtilities.getCountryName(countryCodeUncapitalized);
        String result3 = PhoneNumberUtilities.getCountryName(countryCodeMixed);
        String result4 = PhoneNumberUtilities.getCountryName(countryCodeMixedWithBlank);
        String result5 = PhoneNumberUtilities.getCountryName(countryCodeBlank);
        String result6 = PhoneNumberUtilities.getCountryName(countryCodeNull);

        // Assert
        System.assertEquals(expectedCountry, result1);
        System.assertEquals(expectedCountry, result2);
        System.assertEquals(expectedCountry, result3);
        System.assertEquals(expectedCountry, result4);
        System.assertEquals(null, result5);
        System.assertEquals(null, result6);
    }
}