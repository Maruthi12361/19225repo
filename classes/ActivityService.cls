public class ActivityService
{
    public static ActionResult create(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        return new ActivityCreateFromSMSCommand().execute(items, oldMap, triggerContext);
    }
    
    public static ActionResult createActivity(ViewModelsAcquisition.Activity Activity)
    {
        return new ActivitySaveCommand().execute(Activity);
    }
    
    public static ActionResult sendNotification(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        return new ActivitySendNotificationCommand().execute(items, oldMap, triggerContext);
    }
}