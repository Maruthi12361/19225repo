public class ActivitySendNotificationCommand extends BaseQueueableCommand
{
    List<User> taskOwners;

    public ActivitySendNotificationCommand()
    {
        super(1);
    }

    protected override List<SObject> validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        List<Task> tasks = new List<Task>();
        
        for (Task task: (List<Task>) items)
        {
            if (!task.Notification__c)
            {
                Log.info(this.className, 'validate', 'Notification is not required. Task Id {0}', new List<String> { task.Id });
                continue;
            }
            else if (triggerContext == Enums.TriggerContext.AFTER_UPDATE && SObjectUtils.hasPropertyChanged(task, oldMap.get(task.Id), 'Notification__c'))
            {
                continue;
            }

            User taskOwner = getTaskOwner(task.OwnerId, (List<Task>) items);
            
            if (taskOwner.IsSystemCreator__c)
            {
                Log.info(this.className, 'validate', 'Skip sending Notification as task created by system user {0}', new List<String> { taskOwner.Name });
                continue;
            }

            tasks.add(task);
        }

        return tasks;
    }

    public override ActionResult command(List<SObject> items, Id jobId)
    {
        if (items.size() == 0)
            return new ActionResult(ResultStatus.SUCCESS);

        User sysAdmin = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        List<ViewModelsMail.Email> emails = new List<ViewModelsMail.Email>();
        List<Task> updatedTasks = new List<Task>();
        List<Task> tasks = [SELECT Id, OwnerId, Subject, Description, Type, Priority, ActivityDate, Notification__c FROM Task WHERE Id IN: (new Map<Id, SObject>(items)).keySet()];        

        for (Task task: tasks)
        {
            if (!task.Notification__c)
                continue;

            EmailTemplate template = EmailService.getEmailTemplate(Constants.EMAILTEMPLATE_TASKNOTIFICATION);
            if (template == null)
            {
                Log.error(this.className, 'command', 'Fail to send Task Notification to as there was no Template available {0}', new List<String> { Constants.EMAILTEMPLATE_TASKNOTIFICATION});
                continue;
            } 
            
            User taskOwner = getTaskOwner(task.OwnerId, tasks);
            Messaging.SingleEmailMessage singleEmail = Messaging.renderStoredEmailTemplate(template.Id, task.Id, task.Id);
            ViewModelsMail.Email notificationEmail = new ViewModelsMail.Email(task.Id, task.Subject, singleEmail.getPlainTextBody(), singleEmail.getHtmlBody(), taskOwner.Email, taskOwner.Name, null, null);
            emails.add(notificationEmail);

            task.Notification__c = false;
            updatedTasks.add(task);
        }

        if (updatedTasks.size() > 0)
            update updatedTasks;

        for (ViewModelsMail.Email mail : emails)
            sendEmail(mail.ObjectId, mail.ToAddress, mail.ToName, mail.Subject, sysAdmin.Email, 'System', mail.BodyText, mail.BodyHtml);

        return new ActionResult(ResultStatus.SUCCESS);
    }

    @future (callout = true)
    private static void sendEmail(Id taskId, String toAddress, string toName, String subject, String fromAddress, String fromName, String textBody, String htmlBody)
    {
        ActionResult result = EmailService.sendToRecipient(Constants.APIPROVIDER_DEFAULT, toAddress, toName, subject, fromAddress, fromName, textBody, htmlBody, null);

        if (result.isError)
        {
            String message = 'Failed to send Notification Email to ' + toAddress + ' as ' + result.message;
            EmailService.send(new String[] {fromAddress}, 'Failed to send Notification Email', message);

            Log.error('ActivitySendNotificationCommand', 'sendEmail', message);
            Log.save();

            Task task = [SELECT Id, Notification__c FROM Task WHERE Id = : taskId];
            task.Notification__c = true;
            update task;
        }
    }

    private User getTaskOwner(Id taskOwnerId, List<SObject> allTasks)
    {
        if (taskOwners == null)
        {
            Set<ID> userIds = SObjectUtils.getIDs(allTasks, 'OwnerId');
            taskOwners = [SELECT Name, Email, IsSystemCreator__c FROM User WHERE Id =: userIds];    
        }

        for (User item : taskOwners)
           if (taskOwnerId == item.Id)
               return item;

        return null;
    }
}