public class TestDataSubjectOfferingMock
{
    private static Map<String, hed__Course_Offering__c> offeringMap = new Map<String, hed__Course_Offering__c>();

    public static hed__Course_Offering__c createOffering(User user, hed__Course__c subject, hed__Term__c term)
    {
        return createOffering(user, subject, term, 'Standard');
    }

    public static hed__Course_Offering__c createOffering(User user, hed__Course__c subject, hed__Term__c term, String type)
    {
        return createOfferings(user, new List<ViewModelsTestData.SubjectOffering> { new ViewModelsTestData.SubjectOffering(subject, term, type) })[0];
    }

    public static hed__Course_Offering__c createOffering(User user, String subjectCode, String subjectName, hed__Term__c term, Date startDate)
    {
        Account account = TestDataAccountMock.createDepartmentBusiness(user);
        hed__Course__c subjectDefinition = TestDataSubjectMock.createDefinition(user, subjectCode, subjectName, account.Id);
        hed__Course__c subjectVersion = TestDataSubjectMock.createVersion(user, subjectCode, subjectName, account.Id, subjectDefinition.Id, true, 1);
        ViewModelsTestData.SubjectOffering offering = new ViewModelsTestData.SubjectOffering(subjectVersion, term, Constants.SUBJECTOFFERING_TYPE_STANDARD, startDate);

        return createOfferings(user, new List<ViewModelsTestData.SubjectOffering> { offering })[0];
    }

    public static List<hed__Course_Offering__c> createOfferings(User user, List<ViewModelsTestData.SubjectOffering> subjectOfferings)
    {
        List<hed__Course_Offering__c> result = new List<hed__Course_Offering__c>();
        List<hed__Course_Offering__c> inserts = new List<hed__Course_Offering__c>();

        for (ViewModelsTestData.SubjectOffering subjectOffering: subjectOfferings)
        {
            String key = subjectOffering.Version.Name + subjectOffering.Version.VersionNumber__c + + (subjectOffering.StartDate == null ? '' : subjectOffering.StartDate.format());

            if (offeringMap.containsKey(key))
            {
                result.add(offeringMap.get(key));
                continue;
            }

            if (offeringMap.size() == 0)
            {
                List<hed__Course_Offering__c> items = 
                [
                    SELECT Name, Active__c, hed__Term__c, hed__Course__c, DurationType__c, Type__c, hed__Start_Date__c, hed__End_Date__c,
                        hed__Course__r.VersionNumber__c, AdministrationDate__c, CensusDate__c
                    FROM hed__Course_Offering__c
                ];

                for (hed__Course_Offering__c item: items)
                    offeringMap.put(item.Name + item.hed__Course__r.VersionNumber__c + item.hed__Start_Date__c, item);
            }

            if (offeringMap.containsKey(key))
            {
                result.add(offeringMap.get(key));
                continue;
            }

            hed__Course_Offering__c newItem = new hed__Course_Offering__c
            (
                Name = subjectOffering.Version.hed__Course_ID__c + ' ' + subjectOffering.Version.Name + ' ' + (subjectOffering.StartDate == null ? '' : subjectOffering.StartDate.format()),
                Active__c = true,
                hed__Term__c = subjectOffering.Term.Id,
                hed__Course__c = subjectOffering.Version.Id,
                DurationType__c = '8 Week Class',
                Type__c = subjectOffering.Type,
                hed__Start_Date__c = subjectOffering.StartDate,
                hed__End_Date__c = subjectOffering.StartDate == null ? null : subjectOffering.StartDate.addDays(56),
                AdministrationDate__c = subjectOffering.StartDate == null ? null : subjectOffering.StartDate.addDays(-21),
                CensusDate__c = subjectOffering.StartDate == null ? null : subjectOffering.StartDate.addDays(11)
            );

            offeringMap.put(key, newItem);
            inserts.add(newItem);
            result.add(newItem);
        }

        if (inserts.size() > 0)
        {
            if (user  != null)
            {
                System.RunAs(user)
                {
                    insert inserts;
                }
            }
            else
            {
                insert inserts;
            }
        }

        return result;
    }
}