@isTest
public class TestDataAttachmentMock 
{
    public static Attachment create(User user, Id parentId)
    {
        Attachment att = new Attachment
        (
            ParentId = parentId,
            Name = 'TestFile.txt',
            Description = 'Test file',
            ContentType = 'text/plain',
            Body = Blob.valueOf('This is a test file contant')
        );

        System.RunAs(user)
        {
            insert att;
        }

        return att;
    }
}