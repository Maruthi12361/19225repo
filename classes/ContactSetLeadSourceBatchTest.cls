@isTest
public class ContactSetLeadSourceBatchTest 
{
    @testSetup
    static void setup()
    {
        TestUtilities.setupHEDA();
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        TestDataLeadSourceConfigMock.create(user);
    }

    @isTest 
    static void ContactWithUnknownLeadSourceAndFacebookContactReferer_LeadSourceSetToFacebookSuccessfully() 
    {
        // Arrange 
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact contact = TestDataContactMock.createProspect(user, 'alex.test@email.com', 'Unknown');
        ContactReferrer__c referrer = TestDataContactReferrerMock.create(user, contact.id);
        contact.LeadSource = 'Unknown';
        update contact;

        // Act
        test.startTest();
        Database.executeBatch(new ContactSetLeadSourceBatch());
        test.stopTest();

        Contact updatedContact = [SELECT Id, LeadSource FROM Contact WHERE Id = :contact.Id];
        ContactReferrer__c updatedReferrer = [SELECT Id, LeadSource__c FROM ContactReferrer__c WHERE Id = :referrer.Id];

        // Assert
        System.assertEquals(updatedReferrer.LeadSource__c, updatedContact.LeadSource, 'Unexpected Lead source value');
    }

    @isTest 
    static void ContactWithWebsiteLeadSourceAndFacebookContactReferer_LeadSourceNotUpdated() 
    {
        // Arrange 
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact contact = TestDataContactMock.createProspect(user, 'alex.test@email.com', 'Website (Organic)');
        ContactReferrer__c referrer = TestDataContactReferrerMock.create(user, contact.id);

        // Act
        test.startTest();
        Database.executeBatch(new ContactSetLeadSourceBatch());
        test.stopTest();

        Contact updatedContact = [SELECT Id, LeadSource FROM Contact WHERE Id = :contact.Id];

        // Assert
        System.assertEquals('Website (Organic)', updatedContact.LeadSource, 'Unexpected Lead source value');
    }

    @isTest 
    static void ContactWithNullLeadSourceAndFacebookContactReferer_LeadSourceSetToFacebookSuccessfully() 
    {
        // Arrange 
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact contact = TestDataContactMock.createProspect(user, 'alex.test@email.com', null);
        ContactReferrer__c referrer = TestDataContactReferrerMock.create(user, contact.id);
        contact.LeadSource = null;
        update contact;

        // Act
        test.startTest();
        Database.executeBatch(new ContactSetLeadSourceBatch());
        test.stopTest();

        Contact updatedContact = [SELECT Id, LeadSource FROM Contact WHERE Id = :contact.Id];
        ContactReferrer__c updatedReferrer = [SELECT Id, LeadSource__c FROM ContactReferrer__c WHERE Id = :referrer.Id];

        // Assert
        System.assertEquals(updatedReferrer.LeadSource__c, updatedContact.LeadSource, 'Unexpected Lead source value');
    }

    @isTest 
    static void ContactWithUnknownLeadSourceAndMultipleContactReferers_LeadSourceSetToFirstCreatedContactReferrerLeadSource() 
    {
        // Arrange 
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact contact = TestDataContactMock.createProspect(user, 'alex.test@email.com', null);
        contact.LeadSource = 'Unknown';

        ContactReferrer__c referrer = TestDataContactReferrerMock.create(user, contact.id);
        ContactReferrer__c secondReferrer = TestDataContactReferrerMock.create(user, contact.id);
        secondReferrer.LeadSource__c = 'First Lead Source';
        
        update secondReferrer;
        update contact;
        
        Test.setCreatedDate(secondReferrer.Id, DateTime.now().addDays(-10));

        // Act
        test.startTest();
        Database.executeBatch(new ContactSetLeadSourceBatch());
        test.stopTest();

        Contact updatedContact = [SELECT Id, LeadSource FROM Contact WHERE Id = :contact.Id];
        ContactReferrer__c updatedSecondReferrer = [SELECT Id, LeadSource__c FROM ContactReferrer__c WHERE Id = :secondReferrer.Id];

        // Assert
        System.assertEquals('First Lead Source', updatedContact.LeadSource, 'Unexpected Lead source value');
    }
}