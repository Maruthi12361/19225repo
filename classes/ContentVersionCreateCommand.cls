public class ContentVersionCreateCommand extends BaseViewModelCommand
{
    public override List<Interfaces.IViewModel> validate(List<Interfaces.IViewModel> items)
    {
        for (ViewModelsFile.Upload att : (List<ViewModelsFile.Upload>) items)
        {
            if (att.ParentId == null)
                throw new Exceptions.ApplicationException('Attachment parent id parameter cannot be null');

            if (String.isEmpty(att.FileName))
                throw new Exceptions.ApplicationException('Attachment file name parameter cannot be null or empty');
            
            if (att.Content == null || att.Content.size() == 0)
                throw new Exceptions.ApplicationException('Attachment body parameter cannot be null or empty');
        }
        
        return items;
    }
    
    public override ActionResult command(List<Interfaces.IViewModel> items)
    {
        ContentVersion version = new ContentVersion();

        Log.info(this.className, 'command', 'Inserting {0} attachments', new List<String>{ String.valueOf(items.size()) });

        for (ViewModelsFile.Upload file : (List<ViewModelsFile.Upload>) items)
        {
            version.ContentLocation = Constants.FILE_CONTENT_LOCATION_SALESFORCE;
            version.VersionData = file.Content;
            version.Title = file.FileName;
            version.Type__c = file.Type;
            version.PathOnClient = file.FileName;
            version.FirstPublishLocationId = file.ParentId;
            version.Origin = Constants.FILE_VERSION_ORIGIN_CONTENT;

            if (file.CommunityId != null)
                version.NetworkId = file.CommunityId;

            if (file.OwnerId != null)
                version.OwnerId = file.OwnerId;

            insert version;
        }

        return new ActionResult(version.Id);
    }
}