@RestResource(urlMapping='/files/*')
global class FileController 
{
    @HttpGet
    global static void get()
    {
        String id;
        String reference;
        RestRequest req = RestContext.request;

        id = req.params.get('id');

        if (req.headers.containsKey('Reference'))
            reference = req.headers.get('Reference');
        else if (String.isNotBlank(req.params.get('reference')))
            reference = EncodingUtil.urlEncode(req.params.get('reference'), 'UTF-8') ;

        if (String.isEmpty(reference) || String.isEmpty(id))
        {
            Log.error('FileController', 'get', 'Reference and id parameter values cannot be empty');
            throw new Exceptions.InvalidParameterException('Invalid parameter values');
        }

        Map<String, String> payloadMap = new Map<String, String>
        {
            'ContactIdentifier' => reference
        };

        ViewModels.Param param = new ViewModels.Param();
        param.Identifier = Id;
        param.Payload = JSON.serialize(payloadMap);

        ViewModelsFile.ContentDetails file = ContentDocumentService.getFile(param);

        RestContext.response.addHeader('Content-Type', 'application/octet-stream');
        RestContext.response.addHeader('Content-Disposition', String.format('attachment; filename="{0}.{1}"', new List<String> { file.Name, file.Extension }));
        RestContext.response.responseBody = file.Content;
        RestContext.response.statusCode = 200;
    }

    @HttpDelete
    global static void remove()
    {
        try
        {
            RestRequest req = RestContext.request;
            String reference = req.params.get('id');

            Log.info('FileController', 'remove', 'Identifier value {0}', new List<String>{ reference });
            
            if (String.isEmpty(reference))
                throw new Exceptions.ApplicationException('Reference value cannot be blank');

            ActionResult result = ContentDocumentService.removeFile(reference);

            if (result.Status == ResultStatus.SUCCESS)
            {
                RestContext.response.addHeader('Content-Type', 'application/json');
                RestContext.response.statusCode = 200;
            }
            else
            {
                Log.error('FileController', 'remove', result.message);
                throw new Exceptions.ApplicationException(result.message);
            }
        }
        catch(Exception ex)
        {
            Log.error('FileController', 'remove', ex.getMessage());
            Map<String, String> errorMap = new Map<String, String>
            {
                'Message' => ex.getMessage(),
                'Details' => ex.getStackTraceString()
            };

            RestContext.response.addHeader('Content-Type', 'application/json');
            RestContext.response.responseBody = Blob.valueOf(JSON.serialize(errorMap));
            RestContext.response.statusCode = 400;
        }
    }
}