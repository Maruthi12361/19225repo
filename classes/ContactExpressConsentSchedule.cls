/**
 * This schedule runs a batch on a daily basis to evaluate the 
 * Express_Consent_Exists flag and OK_To_Call text for all Contacts 
 */
public class ContactExpressConsentSchedule implements Schedulable 
{
    public void execute(SchedulableContext sc)
    {
        Database.executebatch(new ContactExpressConsentBatch(ContactExpressConsentBatch.DEFAULT_QUERY), 95);
    }
}