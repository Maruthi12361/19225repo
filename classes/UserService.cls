public class UserService 
{
    public static User get(Id id)
    {
        List<User> items = new UserGetQuery().query(new Set<Id> { id });
        
        return items.size() == 1 ? items[0] : null;
    }
    
    public static List<User> get(Set<Id> ids)
    {
        return new UserGetQuery().query(ids);
    }
    
    public static User getByAlias(String alias)
    {
        List<User> items = new UserGetByAliasQuery().query(new Set<String> { alias });
        
        return items.size() == 1 ? items[0] : null;
    }
    
    public static List<User> getByAlias(Set<String> aliases)
    {
        return new UserGetByAliasQuery().query(aliases);
    }
}