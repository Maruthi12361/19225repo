public class CaseTriggerHandler extends BaseTriggerHandler
{
    public override void beforeInsert()
    {
        ActionResult result;

        result = CaseService.updateAwardAddress(Trigger.new, Trigger.oldMap, Enums.TriggerContext.BEFORE_INSERT);
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);

        result = CaseService.calculateFirstEmailResponseTime(Trigger.new, Trigger.oldMap, Enums.TriggerContext.BEFORE_INSERT);
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);
    }

    public override void afterInsert()
    {
        ActionResult result;

        result = CaseService.trackStatusHistory(Trigger.new, Trigger.oldMap, Enums.TriggerContext.AFTER_INSERT);
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);
    }

    public override void beforeUpdate()
    {
        ActionResult result;

        result = CaseService.updateAwardAddress(Trigger.new, Trigger.oldMap, Enums.TriggerContext.BEFORE_UPDATE);
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);

        result = CaseService.calculateFirstEmailResponseTime(Trigger.new, Trigger.oldMap, Enums.TriggerContext.BEFORE_UPDATE);
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);

        // Not throwing exception in order to show nicely formated UI error message
        result = CaseService.validateFileFlags(Trigger.new, Trigger.oldMap, Enums.TriggerContext.BEFORE_UPDATE);
    }

    public override void afterUpdate()
    {
        ActionResult result;

        result = CaseService.trackStatusHistory(Trigger.new, Trigger.oldMap, Enums.TriggerContext.AFTER_UPDATE);
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);

        result = CaseService.updateSSOStatus(Trigger.new, Trigger.oldMap, Enums.TriggerContext.AFTER_UPDATE);
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);
        
        result = SObjectService.sendConfirmationEmail(Trigger.new, Trigger.oldMap, Enums.TriggerContext.AFTER_UPDATE);
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);
    }
}