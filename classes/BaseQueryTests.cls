@isTest
private class BaseQueryTests
{   
    @testSetup
    static void setup()
    {
        TestUtilities.setupHEDA();
    }

    @isTest
    static void ExecuteQuery_ReturnsAllExpectedObjects()
    {
        // Arrange
        List<Case> cases = new List<Case>();
        TestBaseQuery testQuery = new TestBaseQuery();

        for(Integer i=0; i<10; i++)
        {
            Case newCase = new Case();
            newCase.Subject = 'BaseQuery_TestQuery';
            newCase.Description = 'BaseQueryTest';

            cases.add(newCase);
        }

        insert cases;

        // Act
        List<SObject> results = testQuery.query();

        // Assert
        System.assertNotEquals(null, results, 'Request should return a valid collection of items');
        System.assertEquals(10, results.size(), 'Request should return only those items identified by the ids being passed through');
    }

    class TestBaseQuery extends BaseQuery
    {
        public override List<SObject> query()
        {
            return Database.query('SELECT Id FROM Case');
        }
    }
}