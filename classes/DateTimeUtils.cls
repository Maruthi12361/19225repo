public class DateTimeUtils
{
    @TestVisible private static final Integer MILLISECONDS_PER_MINUTE = 60 * 1000;
    @TestVisible private static final Integer MILLISECONDS_PER_HOUR = 60 * MILLISECONDS_PER_MINUTE;
    @TestVisible private static final Integer MILLISECONDS_PER_DAY = 24 * MILLISECONDS_PER_HOUR;
    @TestVisible private static final Decimal BUSINESS_HOURS_PER_DAY = 8.5; 
    private static BusinessHours busHours = null;
    private static List<Time> startTimes = null;
    private static List<Time> endTimes = null;
    private static List<Boolean> businessDays = null;
    
    public enum Duration { SECONDS, MINUTES, HOURS, DAYS }

    private static void initBusinessHours()
    {
        busHours = 
        [
            SELECT Id, SundayStartTime, MondayStartTime, TuesdayStartTime, WednesdayStartTime, ThursdayStartTime, FridayStartTime,
                SaturdayStartTime, SundayEndTime, MondayEndTime,TuesdayEndTime, WednesdayEndTime, ThursdayEndTime, FridayEndTime, SaturdayEndTime
            FROM BusinessHours 
            WHERE Name = 'Default' AND isDefault = true
        ];
        startTimes = new List<Time>{ busHours.SundayStartTime, busHours.MondayStartTime, busHours.TuesdayStartTime, busHours.WednesdayStartTime, busHours.ThursdayStartTime, busHours.FridayStartTime, busHours.SaturdayStartTime };
        endTimes = new List<Time>{ busHours.SundayEndTime, busHours.MondayEndTime, busHours.TuesdayEndTime, busHours.WednesdayEndTime, busHours.ThursdayEndTime, busHours.FridayEndTime, busHours.SaturdayEndTime };
        businessDays = new List<Boolean>{ busHours.SundayStartTime != null, busHours.MondayStartTime != null, busHours.TuesdayStartTime != null, busHours.WednesdayStartTime != null, busHours.ThursdayStartTime != null, busHours.FridayStartTime != null, busHours.SaturdayStartTime != null };
    }

    public static BusinessHours getBusinessHours()
    {
        if (busHours == null)
            initBusinessHours();

        return busHours;
    }

    public static List<Time> getBusinessStartTimes()
    {
        if (startTimes == null)
            initBusinessHours();

        return startTimes;
    }

    public static List<Time> getBusinessEndTimes()
    {
        if (endTimes == null)
            initBusinessHours();

        return endTimes;
    }

    public static List<Boolean> getBusinessDays()
    {
        if (businessDays == null)
            initBusinessHours();

        return businessDays;
    }

    public static Decimal getUTCOffsetHours(Timezone tz, Datetime dateTimeToOffset)
    {
        long offsetInMillis = tz.getOffset(dateTimeToOffset == null ? System.now() : dateTimeToOffset);
        return getTimeAsHours(offsetInMillis);
    }

    public static long getElapsedTime(Datetime startDateTime, Datetime endDateTime)
    {
        if (startDateTime == null)
            return 0;

        if (endDateTime == null)
            endDateTime = System.now();

        return endDateTime.getTime() - startDateTime.getTime();
    }

    public static long getBusinessElapsedTime(Datetime startDateTime, Datetime endDateTime)
    {
        if (startDateTime == null)
            return 0;

        if (endDateTime == null)
            endDateTime = System.now();

        return BusinessHours.diff(getBusinessHours().Id, startDateTime, endDateTime);
    }

    public static Decimal getBusinessWorkHours(Date day)
    {
        if (isBusinessDay(day))
        {
            Datetime startOfDay = Datetime.newInstance(day, getWorkingStartTime(day));
            Datetime endOfDay = Datetime.newInstance(day, getWorkingEndTime(day));
            return getTimeAsHours(getBusinessElapsedTime(startOfDay, endOfDay));
        }

        return 0;
    }

    public static Boolean isBusinessDay(Date day)
    {
        // Check if it is weekday
        Date startOfWeek = day.toStartOfWeek();
        Integer index = Math.mod(Math.abs(startOfWeek.daysBetween(day)), 7);

        if (getBusinessDays().get(index))
        {
            // check if it is within Business Hours excluding Public Holiday
            Datetime d = Datetime.newInstance(day, getBusinessStartTimes().get(index));
            return BusinessHours.isWithin(getBusinessHours().Id, d);
        }

        return false;
    }

    public static Time getWorkingStartTime(Date today)
    {
        Date startOfWeek = today.toStartOfWeek();
        Integer index = Math.mod(Math.abs(startOfWeek.daysBetween(today)), 7);

        return getBusinessStartTimes().get(index);
    }

    public static Time getWorkingEndTime(Date today)
    {
        Date startOfWeek = today.toStartOfWeek();
        Integer index = Math.mod(Math.abs(startOfWeek.daysBetween(today)), 7);

        return getBusinessEndTimes().get(index);
    }

    public static Decimal getTimeAs(long timeInMilliSeconds, Duration duration)
    {
        switch on duration
        {
            when SECONDS
            {
                return Decimal.valueOf((double)timeInMilliseconds / 1000).setScale(2);
            }
            when MINUTES
            {
                return Decimal.valueOf((double)timeInMilliseconds / MILLISECONDS_PER_MINUTE).setScale(2);
            }
            when HOURS
            {
                return Decimal.valueOf((double)timeInMilliseconds / MILLISECONDS_PER_HOUR).setScale(2);
            }
            when DAYS
            {
                return Decimal.valueOf((double)timeInMilliseconds / MILLISECONDS_PER_DAY).setScale(2);
            }
            when else 
            {
                Log.error('DateTimeUtils', 'getTimeAs', 'Received an unknown/null duration: {0} returning null', new List<String>{ duration == null ? null : duration.name() });
                return null;
            }
        }
    }

    public static Decimal getTimeAsHours(long timeInMilliseconds)
    {
        return getTimeAs(timeInMilliSeconds, Duration.HOURS);
    }
}