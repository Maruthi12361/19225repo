@isTest
public class ContactCreateReferralCommandTest
{
    @testSetup
    public static void setup()
    {
        TestUtilities.setupHEDA();
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact contact = TestDataContactMock.createProspectCold(user, '0455548889');
    }

    @isTest
    static void BlankIdentifier_EmptyParameterError()
    {
        // Arrange
        String errorMessage = '';
        ViewModels.Param param = new ViewModels.Param();
        param.Identifier = '';
        param.Payload = null;

        // Act
        try
        {
           ActionResult result = new ContactCreateReferralCommand().command(param);
        }
        catch (Exception ex)
        {
           errorMessage = ex.getMessage();
        }

        // Assert
        System.assertEquals(String.format(Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Identifier' }), errorMessage, 'Unexpected error message');
    }

    @isTest 
    static void BlankPayload_EmptyPayloadError() 
    {
        // Arrange
        String errorMessage = '';
        ViewModels.Param param = new ViewModels.Param();
        param.Identifier = 'myemail@email.com';
        param.Payload = null;

        // Act
        try 
        {
           ActionResult result = new ContactCreateReferralCommand().command(param);
        }
        catch (Exception ex)
        {
           errorMessage = ex.getMessage();
        }

        // Assert
        System.assertEquals(String.format(Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Referral' }), errorMessage, 'Unexpected error message');
    }

    @isTest
    static void NewReferee_RefereeContactSuccessfullyCreated()
    {
        // Arrange
        TestUtilities.disableComponent('ReferralSendEmailsCommand');
        Contact referrer = [SELECT Id, Salutation, FirstName, PersonalEmail__c, EncryptedId__c FROM Contact Limit 1];
        Map<String, Object> prospectMap = new Map<String, Object>
        {
            'Cnt_Salutation' => 'Mr',
            'Cnt_FirstName' => 'John',
            'Cnt_LastName' => 'Referee',
            'Cnt_Email' => 'john.referee@email.com',
            'Cnt_HomePhone' => '0825478787',
            'Cnt_OriginalReferrerSource_c' => 'Original Source',
            'Cnt_Program_c' => 'MBA',
            'Cnt_Portfolio_c' => 'Domestic'
        };

        ViewModels.Param param = new ViewModels.Param();
        param.Identifier = referrer.EncryptedId__c;
        param.Payload = JSON.serialize(prospectMap);

        // Act
        test.startTest();
        ActionResult result = new ContactCreateReferralCommand().command(param);
        test.stopTest();

        String referrerSearchIdParam = '%' + referrer.Id + '%';
        Contact referee =
            [
                SELECT Id, Salutation, FirstName, LastName, PersonalEmail__c, HomePhone, OriginalReferrerSource__c,
                    (
                        SELECT Id, Source__c, Type__c
                        FROM InterestingMoments__r
                        WHERE Type__c = 'Milestone' AND Source__c LIKE :referrerSearchIdParam
                    )
                FROM Contact
                WHERE Id = :result.ResultId
            ];

        String refereeSearchIdParam = '%' + referee.Id + '%';
        Contact updateReferrer =
            [
                SELECT Id, Salutation, FirstName, LastName, PersonalEmail__c, HomePhone, OriginalReferrerSource__c,
                    (
                        SELECT Id, Source__c, Type__c
                        FROM InterestingMoments__r
                        WHERE Type__c = 'Form Completed' AND Source__c LIKE :refereeSearchIdParam
                    )
                FROM Contact
                WHERE Id = :referrer.Id
            ];

        List<Referral__c> referrals = [SELECT Id, Referrer_Contact__c, Referee_Contact__c FROM Referral__c WHERE Referrer_Contact__c = :referrer.Id AND Referee_Contact__c = :referee.Id];

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Unexpected result status value');
        System.assertEquals('Mr', referee.Salutation, 'Unexpected Saluation value');
        System.assertEquals('John', referee.FirstName, 'Unexpected First name value');
        System.assertEquals('Referee', referee.LastName, 'Unexpected Last name value');
        System.assertEquals('john.referee@email.com', referee.PersonalEmail__c, 'Unexpected email value');
        System.assertEquals('0825478787', referee.HomePhone, 'Unexpected home phone value');
        System.assertEquals('Original Source', referee.OriginalReferrerSource__c, 'Unexpected original referrer source value');

        System.assertEquals(1, referee.InterestingMoments__r.size(), 'Unexpected number of referee intresting moments');
        System.assertEquals(1, updateReferrer.InterestingMoments__r.size(), 'Unexpected number of referrer intresting moments');
        System.assertEquals(1, referrals.size(), 'Unexpected number of referrals');
    }
}