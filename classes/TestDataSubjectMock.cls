public class TestDataSubjectMock
{
    private static Map<String, hed__Course__c> definitionMap = new Map<String, hed__Course__c>();
    private static Map<String, hed__Course__c> versionMap = new Map<String, hed__Course__c>();

    public static hed__Course__c createDefinitionLEAD(User user)
    {
        Account department = TestDataAccountMock.createDepartmentBusiness(user);

        return createDefinition(user, '8001LEAD', 'Leadership', department.Id);
    }

    public static hed__Course__c createDefinitionMMGT(User user)
    {
        Account department = TestDataAccountMock.createDepartmentBusiness(user);

        return createDefinition(user, '8002MMGT', 'Marketing Management', department.Id);
    }

    public static hed__Course__c createDefinitionCGOV(User user)
    {
        Account department = TestDataAccountMock.createDepartmentBusiness(user);

        return createDefinition(user, '8005CGOV', 'Corporate Governance', department.Id);
    }

    public static hed__Course__c createDefinitionBreak(User user)
    {
        Account department = TestDataAccountMock.createDepartmentBusiness(user);

        return createDefinition(user, Constants.SUBJECT_COURSEID_BREAK, 'Break', department.Id);
    }

    public static hed__Course__c createDefinition(User user, String code, String name, Id accountId)
    {
        return createDefinitions(user, new List<ViewModelsTestData.SubjectDefinition> { new ViewModelsTestData.SubjectDefinition(code, name, accountId, null) })[0];
    }

    public static List<hed__Course__c> createDefinitions(User user, List<ViewModelsTestData.SubjectDefinition> subjectDefinitions)
    {
        hed__Course__c newItem;
        List<hed__Course__c> result = new List<hed__Course__c>();
        List<hed__Course__c> inserts = new List<hed__Course__c>();

        for (ViewModelsTestData.SubjectDefinition subjectDefinition: subjectDefinitions)
        {
            String key = subjectDefinition.Code + subjectDefinition.Name;

            if (definitionMap.containsKey(key))
            {
                result.add(definitionMap.get(key));
                continue;
            }
            
            if (definitionMap.size() == 0)
            {
                List<hed__Course__c> items = 
                [
                    SELECT hed__Course_ID__c, Name, hed__Account__c, DisciplineCode__c, AQFLevel__c, RecordTypeId, hed__Extended_Description__c
                    FROM hed__Course__c
                    WHERE RecordType.DeveloperName =: Constants.RECORDTYPE_SUBJECT_DEFINITION
                ];

                for (hed__Course__c item: items)
                    definitionMap.put(item.hed__Course_ID__c + item.Name, item);
            }

            if (definitionMap.containsKey(key))
            {
                result.add(definitionMap.get(key));
                continue;
            }

            newItem = new hed__Course__c
            (
                hed__Course_ID__c = subjectDefinition.Code,
                Name = subjectDefinition.Name,
                hed__Account__c = subjectDefinition.DepartmentId,
                hed__Extended_Description__c = subjectDefinition.Description,
                DisciplineCode__c = '080000 Management and Commerce',
                AQFLevel__c = '5',
                RecordTypeId = RecordTypeUtils.getRecordTypeId(Constants.RECORDTYPE_SUBJECT_DEFINITION, SObjectType.hed__Course__c)
            );

            definitionMap.put(key, newItem);
            inserts.add(newItem);
            result.add(newItem);
        }

        if (inserts.size() > 0)
        {
            if (user  != null)
            {
                System.RunAs(user)
                {
                    insert inserts;
                }
            }
            else
            {
                insert inserts;
            }
        }

        return result;
    }

    public static hed__Course__c createVersionLEAD(User user)
    {
        Account department = TestDataAccountMock.createDepartmentBusiness(user);
        hed__Course__c definition = createDefinitionLEAD(user);

        return createVersion(user, definition.hed__Course_ID__c, definition.Name, department.Id, definition.Id, true, 1);
    }

    public static hed__Course__c createVersionLEAD(User user, Boolean isPrimary, Double versionNumber)
    {
        Account department = TestDataAccountMock.createDepartmentBusiness(user);
        hed__Course__c definition = createDefinitionLEAD(user);

        return createVersion(user, definition.hed__Course_ID__c, definition.Name, department.Id, definition.Id, isPrimary, versionNumber);
    }

    public static hed__Course__c createVersionCGOV(User user)
    {
        Account department = TestDataAccountMock.createDepartmentBusiness(user);
        hed__Course__c definition = createDefinitionCGOV(user);

        return createVersion(user, definition.hed__Course_ID__c, definition.Name, department.Id, definition.Id, true, 1);
    }

    public static hed__Course__c createVersionCGOV(User user, Boolean isPrimary, Double versionNumber)
    {
        Account department = TestDataAccountMock.createDepartmentBusiness(user);
        hed__Course__c definition = createDefinitionCGOV(user);

        return createVersion(user, definition.hed__Course_ID__c, definition.Name, department.Id, definition.Id, isPrimary, versionNumber);
    }

    public static hed__Course__c createVersionMMGT(User user)
    {
        Account department = TestDataAccountMock.createDepartmentBusiness(user);
        hed__Course__c definition = createDefinitionMMGT(user);

        return createVersion(user, definition.hed__Course_ID__c, definition.Name, department.Id, definition.Id, true, 1);
    }

    public static hed__Course__c createVersionMMGT(User user, Boolean isPrimary, Double versionNumber)
    {
        Account department = TestDataAccountMock.createDepartmentBusiness(user);
        hed__Course__c definition = createDefinitionMMGT(user);

        return createVersion(user, definition.hed__Course_ID__c, definition.Name, department.Id, definition.Id, isPrimary, versionNumber);
    }

    public static hed__Course__c createVersionBreak(User user, Boolean isPrimary, Double versionNumber)
    {
        Account department = TestDataAccountMock.createDepartmentBusiness(user);
        hed__Course__c definition = createDefinitionBreak(user);

        return createVersion(user, definition.hed__Course_ID__c, definition.Name, department.Id, definition.Id, isPrimary, versionNumber);
    }

    public static hed__Course__c createVersion(User user, String subjectCode, String subjectName, Id departmentId, Id subjectDefinitionId, Boolean isPrimary, Double versionNumber)
    {
        return createVersions(user, new List<ViewModelsTestData.SubjectVersion> { new ViewModelsTestData.SubjectVersion(subjectCode, subjectName, departmentId, subjectDefinitionId, isPrimary, versionNumber) })[0];
    }

    public static List<hed__Course__c> createVersions(User user, List<ViewModelsTestData.SubjectVersion> subjectVersions)
    {
        List<hed__Course__c> result = new List<hed__Course__c>();
        List<hed__Course__c> inserts = new List<hed__Course__c>();

        for (ViewModelsTestData.SubjectVersion subjectVersion: subjectVersions)
        {
            String key = subjectVersion.Code + subjectVersion.Name + subjectVersion.VersionNumber;

            if (versionMap.containsKey(key))
            {
                result.add(versionMap.get(key));
                continue;
            }

            if (versionMap.size() == 0)
            {
                List<hed__Course__c> items = 
                [
                    SELECT hed__Course_ID__c, Name, hed__Account__c, DisciplineCode__c, AQFLevel__c, SubjectDefinition__c, IsPrimary__c, 
                        VersionNumber__c, RecordTypeId, HeadOfDiscipline__c, StudentsCoordinatedPerSC__c, StudentsTaughtPerSC__c, StudentsTaughtPerOLF__c
                    FROM hed__Course__c
                    WHERE RecordType.DeveloperName =: Constants.RECORDTYPE_SUBJECT_VERSION
                ];

                for (hed__Course__c item: items)
                    definitionMap.put(item.hed__Course_ID__c + item.Name + item.VersionNumber__c, item);
            }

            if (versionMap.containsKey(key))
            {
                result.add(versionMap.get(key));
                continue;
            }

            hed__Course__c newItem = new hed__Course__c
            (
                Name = subjectVersion.Name,
                hed__Course_ID__c = subjectVersion.Code,
                DisciplineCode__c = '080000 Management and Commerce',
                AQFLevel__c = '5',
                SubjectDefinition__c = subjectVersion.SubjectDefinitionId,
                hed__Account__c = subjectVersion.DepartmentId,
                IsPrimary__c = subjectVersion.IsPrimary,
                VersionNumber__c = subjectVersion.VersionNumber,
                RecordTypeId = RecordTypeUtils.getRecordTypeId(Constants.RECORDTYPE_SUBJECT_VERSION, SObjectType.hed__Course__c),
                HeadOfDiscipline__c = TestDataContactMock.createFacultyContact(user).Id,
                StudentsCoordinatedPerSC__c = 200,
                StudentsTaughtPerSC__c = 20,
                StudentsTaughtPerOLF__c = 50
            );

            versionMap.put(key, newItem);
            inserts.add(newItem);
            result.add(newItem);
        }

        if (inserts.size() > 0)
        {
            if (user  != null)
            {
                System.RunAs(user)
                {
                    insert inserts;
                }
            }
            else
            {
                insert inserts;
            }
        }

        return result;
    }
}