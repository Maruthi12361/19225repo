public class InterestingMomentAlertCACommand extends BaseQueueableCommand
{
    private final List<ViewModelsAcquisition.InterestingMomentAlert> alerts =
        new List<ViewModelsAcquisition.InterestingMomentAlert>
        {
            new ViewModelsAcquisition.InterestingMomentAlert('Email Link Clicked', new List<String> {'/online-portal-tour/'}, 'Alert Portal Tour Interested', 1, false),
            new ViewModelsAcquisition.InterestingMomentAlert('Email Link Clicked', new List<String> {'/ess-thanks-enrol/'}, 'Alert ESS Enrol Now', 1, false),
            new ViewModelsAcquisition.InterestingMomentAlert('Email Link Clicked', new List<String> {'/ess-thanks/'}, 'Alert Interested ESS', 1, false),
            new ViewModelsAcquisition.InterestingMomentAlert('Email Link Clicked', new List<String> {'/courses/aib-mba/graduate-certificate-in-management/'}, 'Alert ESS GCM Link Clicked', 1, false),
            new ViewModelsAcquisition.InterestingMomentAlert('Email Link Clicked', new List<String> {'/aib-marketing/brochures/PD-GCM-Brochure-01.pdf'}, 'Alert ESS GCM Brochure Clicked', 1, false),
            new ViewModelsAcquisition.InterestingMomentAlert('Form Completed', new List<String> {'Enquire Now', 'Check Your Eligibility', 'Reserve Your Spot'}, 'Alert Existing Prospect Fill Form Again', 2, true),
            new ViewModelsAcquisition.InterestingMomentAlert('Form Completed', new List<String> {'Need Help'}, 'Alert Request Call Back', 2, false),
            new ViewModelsAcquisition.InterestingMomentAlert('Form Completed', new List<String> {'Check Your Eligibility (Ineligible'}, 'Alert Not Eligible', 2, false),
            new ViewModelsAcquisition.InterestingMomentAlert('Form Completed', new List<String> {'Reserve Your Spot'}, 'Alert Reserve Your Spot', 2, false),
            new ViewModelsAcquisition.InterestingMomentAlert('Form Completed', new List<String> {'Online Application Form'}, 'OAF Submitted', 2, false),
            new ViewModelsAcquisition.InterestingMomentAlert('Milestone', new List<String> {'Lead Score Over 300'}, 'Alert Lead Score Over 300', 2, false)
        };

    public InterestingMomentAlertCACommand()
    {
        super(50, 'true');
    }

    protected override List<SObject> validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        List<InterestingMoment__c> result = new List<InterestingMoment__c>();

        for (InterestingMoment__c item : (List<InterestingMoment__c>)items)
        {
            if (triggerContext == Enums.TriggerContext.AFTER_INSERT && item.ContactID__c != null && item.Type__c != null && item.Source__c != null)
            {
                if (matchInterestingMomentForAlert(item.Type__c, item.Source__c) != null)
                    result.add(item);
            }
        }

        return result;
    }

    protected override ActionResult command(List<SObject> items, Id jobId)
    {
        for (InterestingMoment__c item : (List<InterestingMoment__c>)items)
        {
            List<ViewModelsAcquisition.InterestingMomentAlert> alerts = matchInterestingMomentForAlert(item.Type__c, item.Source__c);

            for (ViewModelsAcquisition.InterestingMomentAlert alert : alerts)
            {
                if (alert != null && !alert.CustomLogic)
                    sendAlert(item.ContactID__c, alert.EmailTemplate, item.Id);
            }
        }

        processCustomAlert((List<InterestingMoment__c>) items);

        return new ActionResult();
    }

    private void processCustomAlert(List<InterestingMoment__c> items)
    {
        Map<Id, InterestingMoment__c> momentMap = new Map<Id, InterestingMoment__c>();
        Set<Id> momentIdSet = new Map<Id, InterestingMoment__c>(items).keySet();
        Integer daysToSkip = 3;
        ViewModelsAcquisition.InterestingMomentAlert alert = getAlertByTemplateName('Alert Existing Prospect Fill Form Again');

        for (InterestingMoment__c item : items)
        {
            if (alert.MomentType == item.Type__c && StringUtils.containsInListIgnoreCase(item.Source__c, alert.MomentSources, alert.MatchMode))
                momentMap.put(item.ContactID__c, item);
        }

        if (Label.DaysToSkipForExistingProspectFillInFormAlert != null && Label.DaysToSkipForExistingProspectFillInFormAlert.isNumeric())
            daysToSkip = Integer.valueOf(Label.DaysToSkipForExistingProspectFillInFormAlert);

        Date earliestDate = System.today() - daysToSkip;
        List<Contact> contacts =
        [
            SELECT Id, HasOptedOutOfEmail,
            (
                SELECT Id, CreatedDate, Source__c
                FROM InterestingMoments__r
                WHERE Type__c = : alert.MomentType AND CreatedDate >= : earliestDate AND Id NOT IN : momentIdSet
            )
            FROM Contact
            WHERE RecordType.DeveloperName =: Constants.RECORDTYPE_CONTACT_PROSPECT AND ActualCreateDate__c < : earliestDate AND Id IN : momentMap.keySet()
        ];

        List<Contact> updatedContacts = new List<Contact>();

        for (Contact contact : contacts)
        {
            Boolean hasRecentMoment = false;

            if (contact.HasOptedOutOfEmail)
            {
                contact.HasOptedOutOfEmail = false;
                updatedContacts.add(contact);
            }

            for (InterestingMoment__c moment : contact.InterestingMoments__r)
            {
                if (StringUtils.containsInListIgnoreCase(moment.Source__c, alert.MomentSources, alert.MatchMode))
                {
                    hasRecentMoment = true;
                    break;
                }
            }

            if (!hasRecentMoment)
            {
                sendAlert(contact.Id, alert.EmailTemplate, momentMap.get(contact.Id).Id);
            }
        }

        if (updatedContacts.size() > 0)
            uow.registerDirty(updatedContacts);
    }

    private ViewModelsAcquisition.InterestingMomentAlert getAlertByTemplateName(String template)
    {
        for (ViewModelsAcquisition.InterestingMomentAlert alert : alerts)
        {
            if (alert.EmailTemplate == template)
                return alert;
        }

        return null;
    }

    private List<ViewModelsAcquisition.InterestingMomentAlert> matchInterestingMomentForAlert(String momentType, String momentSource)
    {
        List<ViewModelsAcquisition.InterestingMomentAlert> result;

        for (ViewModelsAcquisition.InterestingMomentAlert alert : alerts)
        {
            if (alert.MomentType == momentType && StringUtils.containsInListIgnoreCase(momentSource, alert.MomentSources, alert.MatchMode))
            {
                if (result == null)
                    result = new List<ViewModelsAcquisition.InterestingMomentAlert>();

                result.add(alert);
            }
        }

        return result;
    }

    @future(callout = true)
    private static void sendAlert(Id contactId, String templateName, Id momentId)
    {
        String fromName = Label.CAAlertMailFromName;
        String fromAddress = Label.CAAlertMailFromAddress;
        Id whatId = contactId;
        Boolean isHTMLBody = true;
        EmailTemplate template = EmailService.getEmailTemplate(templateName);
        Contact contact =
        [
            SELECT Id, OwnerId, Owner.Email, Owner.Name
            FROM Contact
            WHERE Id = : contactId
        ];
        InterestingMoment__c lastMoment =
        [
            SELECT Id, Type__c, Source__c, CreatedDate
            FROM InterestingMoment__c
            WHERE Id = : momentId
        ];

        if (templateName == 'OAF Submitted')
        {
            fromName = 'Salesforce Administrator';
            fromAddress = 'salesforce.administrator@aib.edu.au';
            isHTMLBody = false;
            Opportunity[] opportunities =
            [
                SELECT Id
                FROM Opportunity
                WHERE Contact__c = : contactId AND IsClosed = false AND RecordType.DeveloperName =: Constants.RECORDTYPE_OPPORTUNITY_ACQUISITION
                ORDER BY LastModifiedDate DESC
                LIMIT 1
            ];

            if (opportunities.size() > 0)
                whatId = opportunities[0].Id;
        }

        Messaging.SingleEmailMessage mail = Messaging.renderStoredEmailTemplate(template.Id, contact.OwnerId, whatId);
        String body = isHTMLBody ? mail.getHtmlBody() : mail.getPlainTextBody();
        body = body.replace('{{lead.Last Interesting Moment Desc:default=edit me}}', lastMoment.Type__c + ' - ' + lastMoment.Source__c);
        body = body.replace('{{lead.Last Interesting Moment Date:default=edit me}}', lastMoment.CreatedDate.format());

        ActionResult result = null;
        if (isHTMLBody)
            result = EmailService.sendToRecipient(Constants.APIPROVIDER_DEFAULT, contact.Owner.Email, contact.Owner.Name, mail.getSubject(), fromAddress, fromName, null, body, null);
        else
            result = EmailService.sendToRecipient(Constants.APIPROVIDER_DEFAULT, contact.Owner.Email, contact.Owner.Name, mail.getSubject(), fromAddress, fromName, body, null, null);

        if (result.isError)
            Log.error('InterestingMomentAlertCACommand', 'sendAlert', 'Failed to send Alert {0} to {1}<{2}> due to: {3}.', new List<String>{ templateName, contact.Owner.Name, contact.Owner.Email, result.message });
        else
            Log.info('InterestingMomentAlertCACommand', 'sendAlert', 'Successfully sent Alert {0} to {1}<{2}>.', new List<String>{ templateName, contact.Owner.Name, contact.Owner.Email });
    }
}