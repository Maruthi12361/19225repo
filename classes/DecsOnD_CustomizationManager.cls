/**
 * Applies customizations to the default Decisions on Demand functionality.
 * 
 * This class MUST be global, exist in the main namespace and be called DecsOnD_CustomizationManager.
 * 
 * Copyright 2018-2019, Decisions on Demand, Inc.
 * All Rights Reserved.
 * 
 * For support, contact support@decisionsondemand.com
 */
global class DecsOnD_CustomizationManager extends DecsonD.DefaultCustomizationManager
{
    global DecsOnD_CustomizationManager()
    {
        super();
        Log.info('DecsOnD_CustomizationManager', 'DecsOnD_CustomizationManager', 'DecsOnD_CustomizationManager instantiated');
        DecsOnD.Config.registerCustomApexType(DecsOnD.AssignmentHelper.class, DecsOnD_CustomAssignmentHelper.class);
        DecsOnD.Config.addFieldsToLoad(User.sObjectType, DecsOnD_AssignmentGlobals.EXTRA_USER_FIELDS);
        DecsOnD.Config.addFieldsToLoad(Contact.sObjectType, new Set<sObjectField>(DecsOnD_AssignmentGlobals.CONTACT_INFERRED_SKILLS_FIELDS.values()));
    }
}