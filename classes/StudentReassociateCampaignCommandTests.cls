@isTest
public class StudentReassociateCampaignCommandTests 
{
    private static User user = UserService.getByAlias(Constants.USER_TESTSETUP);
    private static String cerebroID = 'A001641336';    
    
    @testSetup
    static void setup()
    {
        TestUtilities.setupHEDA();
        TestDataContactMock.createRolloverStudent(user, cerebroID);
        TestDataCampaignMock.createSeries(user, Date.today().addDays(-112), 6, true);
        TestDataCampaignMock.createRolloverCampaignMemberStatus(user);
    }    
    
    // Test scenario: add contact to campaign according to contact's Rollover followup date 
    @isTest
    static void StudentWithoutRolloverHistory_CreatesNewCampaignMemberBasedOnFollowupDate() 
    {
        // Arrange
        Contact student = 
            [
                SELECT Id,Current_Rollover_Campaign__c,Manual_Followup_Date__c,Current_Course_Status__c,Next_Followup_Date__c,
                	Rollover_Followup_Date__c,Rollover_Status__c,Student_Type__c,Rollover_Deferred_Reason__c,Rollover_Deferred_Reason_Other__c
                FROM Contact
                WHERE Cerebro_Student_ID__c = : cerebroID
            ];
        
        Contact newStudent = student.clone(true, false, true, true);
        
        newStudent.Manual_Followup_Date__c = Date.today().addDays(70);        
        newStudent.recalculateFormulas();
        
        List<Contact> items = new List<Contact>{ newStudent };
        Map<Id, Contact> oldMap = new Map<Id, Contact>();
        oldMap.put(student.Id, student);
        
        // Act
        ActionResult result = ContactService.reassociateRolloverCampaign(items, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Executing a command should return a successful result to the caller.');
        
        // Assert
        student = 
            [
                SELECT Id,Current_Rollover_Campaign__c,Rollover_Count__c,Rollover_Deferred_Reason__c,Rollover_Followup_Date__c,
                (
                    SELECT Id,Status,CampaignId,Campaign.StartDate,Campaign.EndDate
                    FROM CampaignMembers
                )
                FROM Contact
                WHERE Id = : student.Id
            ];
        CampaignMember campaignMember = student.CampaignMembers;
        
        System.assertEquals(1, student.CampaignMembers.size());
        System.assertEquals(1, student.Rollover_Count__c);
        System.assertEquals(campaignMember.CampaignId, student.Current_Rollover_Campaign__c);
        System.assertEquals(RolloverConstantsDefinition.ST_OPEN, campaignMember.Status);
        System.assert(campaignMember.Campaign.StartDate <= newStudent.Rollover_Followup_Date__c);
        System.assert(campaignMember.Campaign.EndDate > newStudent.Rollover_Followup_Date__c);
        System.assertEquals(RolloverConstantsDefinition.RO_DF_SCDEFERRED, student.Rollover_Deferred_Reason__c);
    }
    
    // Test scenario: for international students, they should be added to IMBA campaign 
    @isTest
    static void InternationalStudent_CreatesNewIMBACampaignMember() 
    {
        // Arrange
        Contact student = 
            [
                SELECT Id,Current_Rollover_Campaign__c,Manual_Followup_Date__c,Current_Course_Status__c,Next_Followup_Date__c,
                	Rollover_Followup_Date__c,Rollover_Status__c,Student_Type__c,Rollover_Deferred_Reason__c,Rollover_Deferred_Reason_Other__c
                FROM Contact
                WHERE Cerebro_Student_ID__c = : cerebroID
            ];
        
        Contact newStudent = student.clone(true, false, true, true);
        
        newStudent.Manual_Followup_Date__c = Date.today().addDays(70);        
        newStudent.Student_Type__c = 'OLI';
        newStudent.recalculateFormulas();
        
        List<Contact> items = new List<Contact>{ newStudent };
        Map<Id, Contact> oldMap = new Map<Id, Contact>();
        oldMap.put(student.Id, student);
        
        // Act
        ActionResult result = ContactService.reassociateRolloverCampaign(items, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Executing a command should return a successful result to the caller.');
        
        // Assert
        student = 
            [
                SELECT Id,Rollover_Deferred_Reason__c,
                (
                    SELECT Id,Campaign.Programme__c,Campaign.Portfolio__c 
                    FROM CampaignMembers
                )
                FROM Contact
                WHERE Id = : student.Id
            ];
        CampaignMember campaignMember = student.CampaignMembers;
        
        System.assertEquals(1, student.CampaignMembers.size());
        System.assertEquals('International', campaignMember.Campaign.Portfolio__c);
        System.assertEquals('MBA', campaignMember.Campaign.Programme__c);
        System.assertEquals(RolloverConstantsDefinition.RO_DF_SCDEFERRED, student.Rollover_Deferred_Reason__c);
    }
    
    // Test scenario: when rollover followup date is changed, the contact should be associated with a new campaign. 
    // If the current campaign is still open, move the contact to the new campaign and delete the old one 
    @isTest
    static void StudentWithRolloverHistory_CreatesNewCampaignMemberAfterDeletingOld() 
    {
        // Arrange
        Contact student = 
            [
                SELECT Id,Current_Rollover_Campaign__c,Manual_Followup_Date__c,Next_Followup_Date__c,Rollover_Followup_Date__c,Rollover_Status__c,Student_Type__c
                FROM Contact 
                WHERE Cerebro_Student_ID__c = : cerebroID
            ];
        
        student.Manual_Followup_Date__c = Date.today().addDays(60);
        student.Rollover_Deferred_Reason_Other__c = null;
        update student;        
        
        student = 
            [
                SELECT Id,Current_Rollover_Campaign__c,Manual_Followup_Date__c,Current_Course_Status__c,Next_Followup_Date__c,
                	Rollover_Followup_Date__c,Rollover_Status__c,Student_Type__c,Rollover_Deferred_Reason__c,Rollover_Deferred_Reason_Other__c
                FROM Contact
                WHERE Id = : student.Id
            ];
        
        Id campaignId = student.Current_Rollover_Campaign__c;
        
        Contact newStudent = student.clone(true, false, true, true);        
        newStudent.Next_Followup_Date__c = newStudent.Manual_Followup_Date__c.addDays(60);
        newStudent.recalculateFormulas();
        
        List<Contact> items = new List<Contact>{ newStudent };
        Map<Id, Contact> oldMap = new Map<Id, Contact>();
        oldMap.put(student.Id, student);
        
        // Act
        ActionResult result = ContactService.reassociateRolloverCampaign(items, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Executing a command should return a successful result to the caller.');
        
        // Assert 
        student = 
            [
                SELECT Id,Rollover_Count__c,Rollover_Lost_Count__c,Current_Rollover_Campaign__c,Next_Followup_Date__c,Rollover_Deferred_Reason__c,
                (
                    SELECT Id,Status 
                    FROM CampaignMembers
                )
                FROM Contact 
                WHERE Id = : student.Id
            ];
        
        System.assertEquals(1, student.CampaignMembers.size(), 'CampaignMembers size is not correct');
        System.assertEquals(1, student.Rollover_Count__c);
        System.assertEquals(null, student.Rollover_Lost_Count__c);
        System.assertEquals(RolloverConstantsDefinition.RO_DF_ACTIVEENT, student.Rollover_Deferred_Reason__c);
        System.assertNotEquals(campaignId, student.Current_Rollover_Campaign__c);
    }
    
    // Test scenario: When a contact needs to be added to a new campaign, set the current campaign as closed lost if there have been activities.
    @isTest
    static void StudentWithActiveRolloverHistory_CreatesNewCampaignMemberAfterSettingOldAsClosedLost() 
    {
        // Arrange
        Contact student = 
            [
                SELECT Id,Current_Rollover_Campaign__c,Manual_Followup_Date__c,Next_Followup_Date__c,Rollover_Followup_Date__c,Rollover_Status__c,Student_Type__c
                FROM Contact 
                WHERE Cerebro_Student_ID__c = : cerebroID
            ];
        
        student.Next_Followup_Date__c = Date.today().addDays(1);        
        update student;
        
        student = 
            [
                SELECT Id, Rollover_Status__c
                FROM Contact 
                WHERE Id = : student.Id
            ];
        student.Rollover_Status__c = RolloverConstantsDefinition.ST_CONTACTING;
        update student;
        
        student = 
            [
                SELECT Id, Manual_Followup_Date__c,Rollover_Deferred_Reason__c 
                FROM Contact 
                WHERE Id = : student.Id
            ];
        student.Next_Followup_Date__c = Date.today().addDays(10);
        student.Manual_Followup_Date__c = Date.today().addDays(60);
        student.Rollover_Deferred_Reason__c = RolloverConstantsDefinition.RO_DF_SCDEFERRED;
        update student;
        
        student = 
            [
                SELECT Id,Current_Rollover_Campaign__c,Manual_Followup_Date__c,Current_Course_Status__c,Next_Followup_Date__c,
                	Rollover_Followup_Date__c,Rollover_Status__c,Student_Type__c,Rollover_Deferred_Reason__c,Rollover_Deferred_Reason_Other__c
                FROM Contact
                WHERE Id = : student.Id
            ];
        
        Contact newStudent = student.clone(true, false, true, true);        
        newStudent.Manual_Followup_Date__c = newStudent.Manual_Followup_Date__c.addDays(60);
        newStudent.recalculateFormulas();
        
        List<Contact> items = new List<Contact>{ newStudent };
        Map<Id, Contact> oldMap = new Map<Id, Contact>();
        oldMap.put(student.Id, student);
        
        // Act
        ActionResult result = ContactService.reassociateRolloverCampaign(items, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Executing a command should return a successful result to the caller.');
        
        // Assert 
        student = 
            [
                SELECT Id,Rollover_Status__c,Rollover_Count__c,Rollover_Lost_Count__c,Current_Rollover_Campaign__c,Next_Followup_Date__c,Rollover_Deferred_Reason__c,
                (
                    SELECT Id,Status 
                    FROM CampaignMembers
                ) 
                FROM Contact 
                WHERE Id = : student.Id
            ];
        
        System.assertEquals(RolloverConstantsDefinition.RO_DEFERRED, student.Rollover_Status__c);
        System.assertEquals(RolloverConstantsDefinition.RO_DF_SCDEFERRED, student.Rollover_Deferred_Reason__c);
        System.assertEquals(1, student.Rollover_Lost_Count__c);
        System.assertEquals(2, student.Rollover_Count__c);
        System.assertEquals(2, student.CampaignMembers.size());
    }   
    
    // Test scenario: When a contact is set to a future followup date beyond any known key dates, remove him from any existing campaign and 
    // set Rollover status as Deferred, Deferred Reason as To Be Determined.
    @isTest
    static void StudentWithNoMatchedCampaign_RemovesStudentFromExistingCampaign() 
    {
        // Arrange
        Contact student = 
            [
                SELECT Id,Current_Rollover_Campaign__c,Manual_Followup_Date__c,Next_Followup_Date__c,Rollover_Followup_Date__c,Rollover_Status__c,Student_Type__c
                FROM Contact 
                WHERE Cerebro_Student_ID__c = : cerebroID
            ];
        
        student.Manual_Followup_Date__c = Date.today().addDays(100);        
        update student;
        
        student = 
            [
                SELECT Id,Current_Rollover_Campaign__c,Manual_Followup_Date__c,Current_Course_Status__c,Next_Followup_Date__c,
                	Rollover_Followup_Date__c,Rollover_Status__c,Student_Type__c,Rollover_Deferred_Reason__c,Rollover_Deferred_Reason_Other__c
                FROM Contact
                WHERE Id = : student.Id
            ];
        
        Contact newStudent = student.clone(true, false, true, true);        
        newStudent.Next_Followup_Date__c = Date.today().addDays(365);
        newStudent.recalculateFormulas();
        
        List<Contact> items = new List<Contact>{ newStudent };
        Map<Id, Contact> oldMap = new Map<Id, Contact>();
        oldMap.put(student.Id, student);
        
        // Act
        ActionResult result = ContactService.reassociateRolloverCampaign(items, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Executing a command should return a successful result to the caller.');
        
        // Assert 
        student = 
            [
                SELECT Id,Rollover_Status__c,Rollover_Count__c,Rollover_Lost_Count__c,Current_Rollover_Campaign__c,Next_Followup_Date__c,Rollover_Deferred_Reason__c,
                (
                    SELECT Id,Status 
                    FROM CampaignMembers
                ) 
                FROM Contact 
                WHERE Id = : student.Id
            ];
        
        System.assertEquals(0, student.CampaignMembers.size());
        System.assertEquals(null, student.Current_Rollover_Campaign__c);
        System.assertEquals(RolloverConstantsDefinition.RO_DEFERRED, student.Rollover_Status__c);
        System.assertEquals(RolloverConstantsDefinition.RO_DF_TBD, student.Rollover_Deferred_Reason__c);
    }
    
    // Test scenario: When a contact is set to No Future Enrolment, remove him from any existing campaign and 
    // set Deferred Reason as Withdrawing.
    @isTest
    static void StudentWithNoFutureEnrolment_WithdrawsStudentFromExistingCampaign() 
    {
        // Arrange
        Contact student = 
            [
                SELECT Id,Current_Rollover_Campaign__c,Manual_Followup_Date__c,Next_Followup_Date__c,Rollover_Followup_Date__c,Rollover_Status__c,Student_Type__c
                FROM Contact 
                WHERE Cerebro_Student_ID__c = : cerebroID
            ];
        
        student.Manual_Followup_Date__c = Date.today().addDays(1);        
        update student;
        
        student = 
            [
                SELECT Id, Rollover_Status__c,Rollover_Lost_Count__c
                FROM Contact 
                WHERE Id = : student.Id
            ];
        student.Rollover_Status__c = RolloverConstantsDefinition.ST_CONTACTING;
        student.Rollover_Lost_Count__c = 0;
        update student;
        
        student = 
            [
                SELECT Id,Current_Rollover_Campaign__c,Manual_Followup_Date__c,Current_Course_Status__c,Next_Followup_Date__c,
                	Rollover_Followup_Date__c,Rollover_Status__c,Student_Type__c,Rollover_Deferred_Reason__c,Rollover_Deferred_Reason_Other__c
                FROM Contact
                WHERE Id = : student.Id
            ];
        
        Contact newStudent = student.clone(true, false, true, true);        
        newStudent.Rollover_Status__c = RolloverConstantsDefinition.RO_NOENROLMENTS;
        newStudent.recalculateFormulas();
        
        List<Contact> items = new List<Contact>{ newStudent };
        Map<Id, Contact> oldMap = new Map<Id, Contact>();
        oldMap.put(student.Id, student);
        
        // Act
        ActionResult result = ContactService.reassociateRolloverCampaign(items, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Executing a command should return a successful result to the caller.');
        
        // Assert 
        student = 
            [
                SELECT Id,Rollover_Status__c,Rollover_Count__c,Rollover_Lost_Count__c,Current_Rollover_Campaign__c,Next_Followup_Date__c,Rollover_Deferred_Reason__c,
                (
                    SELECT Id,Status 
                    FROM CampaignMembers
                ) 
                FROM Contact 
                WHERE Id = : student.Id
            ];
        System.assertEquals(1, student.CampaignMembers.size());
        System.assertEquals(null, student.Current_Rollover_Campaign__c);
        System.assertEquals(RolloverConstantsDefinition.RO_DF_WITHDRAW, student.Rollover_Deferred_Reason__c);
    }
    
    // Test scenario: When a contact's Rollover followup date is changes but still within the current assigned campaign period, no change should happen to the Rollover status
    // and Campaignmember status.
    @isTest
    static void StudentWithinSameCampaign_NoChangeOnExistingCampaignMember()
    {
        // Arrange
        Contact student = 
            [
                SELECT Id,Current_Rollover_Campaign__c,Manual_Followup_Date__c,Next_Followup_Date__c,Rollover_Followup_Date__c,Rollover_Status__c,Student_Type__c
                FROM Contact 
                WHERE Cerebro_Student_ID__c = : cerebroID
            ];
        
        student.Manual_Followup_Date__c = Date.today().addDays(1);
        update student;
        
        student = 
            [
                SELECT Id, Rollover_Status__c,Rollover_Lost_Count__c
                FROM Contact 
                WHERE Id = : student.Id
            ];
        student.Rollover_Status__c = RolloverConstantsDefinition.ST_CONTACTING;
        update student;
        
        student = 
            [
                SELECT Id,Current_Rollover_Campaign__c,Manual_Followup_Date__c,Current_Course_Status__c,Next_Followup_Date__c,
                	Rollover_Followup_Date__c,Rollover_Status__c,Student_Type__c,Rollover_Deferred_Reason__c,Rollover_Deferred_Reason_Other__c
                FROM Contact
                WHERE Id = : student.Id
            ];        
        Contact newStudent = student.clone(true, false, true, true);        
        newStudent.Manual_Followup_Date__c = newStudent.Manual_Followup_Date__c.addDays(1);
        newStudent.recalculateFormulas();
        
        List<Contact> items = new List<Contact>{ newStudent };
        Map<Id, Contact> oldMap = new Map<Id, Contact>();
        oldMap.put(student.Id, student);
        
        // Act
        ActionResult result = ContactService.reassociateRolloverCampaign(items, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Executing a command should return a successful result to the caller.');
        
        // Assert 
        student = 
            [
                SELECT Id,Rollover_Status__c,Rollover_Count__c,Rollover_Lost_Count__c,Current_Rollover_Campaign__c,Next_Followup_Date__c,Rollover_Deferred_Reason__c,
                (
                    SELECT Id,Status 
                    FROM CampaignMembers
                ) 
                FROM Contact 
                WHERE Id = : student.Id
            ];
        
        CampaignMember campaignMember = student.CampaignMembers;
        
        System.assertEquals(1, student.CampaignMembers.size());
        System.assertEquals(RolloverConstantsDefinition.ST_CONTACTING, student.Rollover_Status__c);
        System.assertEquals(RolloverConstantsDefinition.ST_CONTACTING, campaignMember.Status);
    }
    
    // Test scenario: If a contact rollover status is 'Deferred' but the Rollover campaign status is 'In Progress', set the contact Rollover status to 'Open - Not Contacted'.
    @isTest
    static void StudentInCurrentCampaign_SetsRolloverStatusToOpen() 
    {
        // Arrange
        Contact student = 
            [
                SELECT Id,Current_Rollover_Campaign__c,Manual_Followup_Date__c,Next_Followup_Date__c,Rollover_Followup_Date__c,Rollover_Status__c,Student_Type__c
                FROM Contact 
                WHERE Cerebro_Student_ID__c = : cerebroID
            ];
        
        student.Manual_Followup_Date__c = Date.today().addDays(60);
        update student;
        
        student = 
            [
                SELECT Id,Current_Rollover_Campaign__c,Rollover_Status__c
                FROM Contact 
                WHERE Id = : student.Id
            ];
        String oldRolloverStatus = student.Rollover_Status__c;
        
        Campaign campaign = 
            [
                SELECT Id, Status, StartDate
                FROM Campaign 
                WHERE Id = : student.Current_Rollover_Campaign__c
            ];
        campaign.Status = 'In Progress';
        campaign.StartDate = Date.today();
        update campaign;
        
        student = 
            [
                SELECT Id,Current_Rollover_Campaign__c,Manual_Followup_Date__c,Current_Course_Status__c,Next_Followup_Date__c,
                	Rollover_Followup_Date__c,Rollover_Status__c,Student_Type__c,Rollover_Deferred_Reason__c,Rollover_Deferred_Reason_Other__c
                FROM Contact
                WHERE Id = : student.Id
            ];
        
        Contact newStudent = student.clone(true, false, true, true);        
        newStudent.Manual_Followup_Date__c = newStudent.Manual_Followup_Date__c.addDays(1);
        newStudent.recalculateFormulas();
        
        List<Contact> items = new List<Contact>{ newStudent };
        Map<Id, Contact> oldMap = new Map<Id, Contact>();
        oldMap.put(student.Id, student);
        
        // Act
        ActionResult result = ContactService.reassociateRolloverCampaign(items, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Executing a command should return a successful result to the caller.');
        
        // Assert 
        student = 
            [
                SELECT Id,Rollover_Status__c,Rollover_Count__c,Rollover_Lost_Count__c,Current_Rollover_Campaign__c,Next_Followup_Date__c,Rollover_Deferred_Reason__c,
                (
                    SELECT Id,Status 
                    FROM CampaignMembers
                ) 
                FROM Contact 
                WHERE Id = : student.Id
            ];
        
        System.assertEquals(1, student.CampaignMembers.size());
        System.assertEquals(RolloverConstantsDefinition.ST_OPEN, student.Rollover_Status__c);
        System.assertEquals(RolloverConstantsDefinition.RO_DEFERRED, oldRolloverStatus);
        System.assertEquals(null, student.Rollover_Deferred_Reason__c);        
    }
    
    // Test scenario: If a contact rollover status is 'Open - Not Contacted' but the Rollover campaign status is 'Planned', set the contact Rollover status to 'Deferred'.
    @isTest
    static void StudentInFutureCampaign_SetsRolloverStatusToDeferred() 
    {
        // Arrange
        Contact student = 
            [
                SELECT Id,Current_Rollover_Campaign__c,Manual_Followup_Date__c,Next_Followup_Date__c,Rollover_Followup_Date__c,Rollover_Status__c,Student_Type__c
                FROM Contact 
                WHERE Cerebro_Student_ID__c = : cerebroID
            ];
        
        student.Next_Followup_Date__c = Date.today().addDays(60);
        update student;
        
        student = 
            [
                SELECT Id,Current_Rollover_Campaign__c,Rollover_Status__c,Rollover_Deferred_Reason__c
                FROM Contact 
                WHERE Id = : student.Id
            ];
        student.Rollover_Status__c = RolloverConstantsDefinition.ST_OPEN;
        student.Rollover_Deferred_Reason__c = null;
        update student;
        
        student = 
            [
                SELECT Id,Current_Rollover_Campaign__c,Manual_Followup_Date__c,Current_Course_Status__c,Next_Followup_Date__c,
                	Rollover_Followup_Date__c,Rollover_Status__c,Student_Type__c,Rollover_Deferred_Reason__c,Rollover_Deferred_Reason_Other__c
                FROM Contact
                WHERE Id = : student.Id
            ];
        
        Contact newStudent = student.clone(true, false, true, true);        
        newStudent.Next_Followup_Date__c = newStudent.Next_Followup_Date__c.addDays(1);
        newStudent.recalculateFormulas();
        
        List<Contact> items = new List<Contact>{ newStudent };
        Map<Id, Contact> oldMap = new Map<Id, Contact>();
        oldMap.put(student.Id, student);
        
        // Act
        ActionResult result = ContactService.reassociateRolloverCampaign(items, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Executing a command should return a successful result to the caller.');
        
        // Assert 
        student = 
            [
                SELECT Id,Rollover_Status__c,Rollover_Count__c,Rollover_Lost_Count__c,Current_Rollover_Campaign__c,Next_Followup_Date__c,Rollover_Deferred_Reason__c,
                (
                    SELECT Id,Status 
                    FROM CampaignMembers
                ) 
                FROM Contact 
                WHERE Id = : student.Id
            ];
        
        System.assertEquals(1, student.CampaignMembers.size());
        System.assertEquals(RolloverConstantsDefinition.RO_DEFERRED, student.Rollover_Status__c);
        System.assertEquals(RolloverConstantsDefinition.RO_DF_ACTIVEENT, student.Rollover_Deferred_Reason__c);        
    }
    
    // Test scenario: When a contact is added to a new campaign while he already has an open Rollover case associated with that campaign,
    // set the Rollover status and campaignmember status to be Enrolling.
    @isTest
    static void StudentWithExistingRolloverCase_SetsRolloverStatusToEnrolling() {
        // Arrange
        Contact student = 
            [
                SELECT Id,Current_Rollover_Campaign__c,Manual_Followup_Date__c,Next_Followup_Date__c,Rollover_Followup_Date__c,Rollover_Status__c,Student_Type__c
                FROM Contact 
                WHERE Cerebro_Student_ID__c = : cerebroID
            ];
        
        student.Manual_Followup_Date__c = Date.today().addDays(1);
        update student;
        
        student = 
            [
                SELECT Id, Rollover_Status__c,Current_Rollover_Campaign__c
                FROM Contact 
                WHERE Id = : student.Id
            ];
        student.Rollover_Status__c = RolloverConstantsDefinition.ST_CONTACTING;
        update student;
        
        CampaignMember campaignMember = 
            [
                SELECT Id 
                FROM CampaignMember 
                WHERE ContactId = : student.Id AND CampaignId = : student.Current_Rollover_Campaign__c
            ];
        Campaign campaign = 
            [
                SELECT Id, StartDate 
                FROM Campaign 
                WHERE Status = 'Planned' 
                ORDER BY StartDate DESC 
                LIMIT 1]
            ; 
        
        Case rolloverCase = TestDataCaseMock.createRolloverCase(user, student);
        
        rolloverCase.Rollover_Campaign__c = campaign.Id;
        update rolloverCase;
        
        student = 
            [
                SELECT Id,Current_Rollover_Campaign__c,Manual_Followup_Date__c,Current_Course_Status__c,Next_Followup_Date__c,
                	Rollover_Followup_Date__c,Rollover_Status__c,Student_Type__c,Rollover_Deferred_Reason__c,Rollover_Deferred_Reason_Other__c
                FROM Contact
                WHERE Id = : student.Id
            ];
        
        Contact newStudent = student.clone(true, false, true, true);        
        newStudent.Manual_Followup_Date__c = campaign.StartDate;
        newStudent.recalculateFormulas();
        
        List<Contact> items = new List<Contact>{ newStudent };
        Map<Id, Contact> oldMap = new Map<Id, Contact>();
        oldMap.put(student.Id, student);
        
        // Act
        ActionResult result = ContactService.reassociateRolloverCampaign(items, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Executing a command should return a successful result to the caller.');
        
        // Assert 
        student = 
            [
                SELECT Id,Rollover_Status__c,Rollover_Count__c,Rollover_Lost_Count__c,Current_Rollover_Campaign__c,Next_Followup_Date__c,Rollover_Deferred_Reason__c,
                (
                    SELECT Id,Status 
                    FROM CampaignMembers
                ) 
                FROM Contact 
                WHERE Id = : student.Id
            ];
        
        campaignMember = 
            [
                SELECT Id, Status 
                FROM CampaignMember 
                WHERE ContactId = : student.Id AND CampaignId = : student.Current_Rollover_Campaign__c
            ];
        system.assertEquals(2, student.CampaignMembers.size(), 'Contact should have 2 CampaignMembers');
        system.assertEquals(RolloverConstantsDefinition.ST_ENROLLING, student.Rollover_Status__c, 'Contact Rollover Status should be Enrolling');
        system.assertEquals(RolloverConstantsDefinition.ST_ENROLLING, campaignMember.Status, 'The new campaign member status should be Enrolling');
    }

    // Test scenario: If a contact enrols subject only for next term, the system followup date, which is 14 days prior to the start date of next term, 
    // will still fall in the current Rollover campaign. The business users actually want to push them to next Rollover Campaign, which is for next next term.
    @isTest
    static void StudentEnrollingSubjectForNextTermOnly_AssignsStudentToNextRolloverCampaign() 
    {
        // Arrange
        Contact student = 
            [
                SELECT Id, Current_Rollover_Campaign__c, Manual_Followup_Date__c, Next_Followup_Date__c, Rollover_Followup_Date__c, Rollover_Status__c, Student_Type__c
                FROM Contact
                WHERE Cerebro_Student_ID__c = : cerebroID
            ];

        student.Next_Followup_Date__c = Date.today().addDays(-60);
        student.Manual_Followup_Date__c = Date.today().addDays(1);
        update student;

        student = 
            [
                SELECT Id, Current_Rollover_Campaign__c, Manual_Followup_Date__c, Current_Course_Status__c, Next_Followup_Date__c,
                	Rollover_Followup_Date__c, Rollover_Status__c, Student_Type__c, Rollover_Deferred_Reason__c, Rollover_Deferred_Reason_Other__c
                FROM Contact
                WHERE Id = : student.Id
            ];
        Campaign currentCampaign = [SELECT StartDate, TermStartDate__c FROM Campaign WHERE Id = : student.Current_Rollover_Campaign__c];
        Contact newStudent = student.clone(true, false, true, true);        
        newStudent.Next_Followup_Date__c = currentCampaign.TermStartDate__c.addDays(-1 * Integer.valueOf(Label.RolloverStartDateOffset));
        newStudent.Manual_Followup_Date__c = null;
        newStudent.recalculateFormulas();

        List<Contact> items = new List<Contact>{ newStudent };
        Map<Id, Contact> oldMap = new Map<Id, Contact>();
        oldMap.put(student.Id, student);

        // Act
        ActionResult result = ContactService.reassociateRolloverCampaign(items, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        System.assertEquals(ResultStatus.SUCCESS, result.Status);

        // Assert 
        student = 
            [
                SELECT Id, Rollover_Status__c, Current_Rollover_Campaign__r.StartDate, Rollover_Followup_Date__c, Rollover_Deferred_Reason__c
                FROM Contact 
                WHERE Id = : student.Id
            ];

        System.assertNotEquals(currentCampaign.Id, student.Current_Rollover_Campaign__c);
        System.assertEquals(true, student.Current_Rollover_Campaign__r.StartDate > currentCampaign.StartDate);
        System.assertEquals(RolloverConstantsDefinition.RO_DEFERRED, student.Rollover_Status__c);
        System.assertEquals(RolloverConstantsDefinition.RO_DF_ACTIVEENT, student.Rollover_Deferred_Reason__c);        
    }
}