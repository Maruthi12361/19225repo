@isTest
private class BaseIdCommandTests
{
    @testSetup
    static void setup()
    {
        TestUtilities.setupHEDA();
    }

    @isTest
    static void ExecuteCommandWithoutValidationImplementation_RaisesExeceptionToCaller()
    {
        // Arrange
        Id id = '906F0000000BU2XIAW';
        TestValidationNotImplementedCommand command = new TestValidationNotImplementedCommand();

        //Act
        try
        {
            command.execute(id);
        }
        catch(Exception e)
        {
            // Assert
            System.assert(e instanceof NotImplementedException, 'Executing a command that doesnt define any validation action should raise an exception for the developer.');
        }
    }

    @isTest
    static void ExecuteCommandWithoutCommandImplementation_RaisesExeceptionToCaller()
    {
        // Arrange
        Id id = '906F0000000BU2XIAW';
        TestCommandNotImplementedCommand command = new TestCommandNotImplementedCommand();

        //Act
        try
        {
            command.execute(id);
        }
        catch(Exception e)
        {
            // Assert
            System.assert(e instanceof NotImplementedException, 'Executing a command that doesnt define any command action should raise an exception for the developer.');
        }
    }

    @isTest
    static void ExecuteCommandWithNullParameters_RaisesExeceptionToCaller()
    {
        // Arrange
        Set<Id> nullObj;
        TestCommand command = new TestCommand();

        // Act
        try
        {
            command.execute(nullObj);
        }
        catch(Exception e)
        {
            // Assert
            System.assert(e instanceof InvalidParameterValueException, 'Executing a command with a null parameter should raise an exception for the developer.');
        }
    }

    @isTest
    static void ExcecuteCommandWithEmptySetOfParameters_ReturnsSuccessResultToCaller()
    {
        // Arrange
        Set<Id> ids = new Set<Id>();
        TestCommand command = new TestCommand();

        // Act
        ActionResult result = command.execute(ids);

        // Arrange
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Executing a command without any Ids to action against should return a successful result to the caller.'); 
    }

    @isTest
    static void ExecuteCommandAfterSuccessfulValidationAndExecution_ReturnsSuccessToCaller()
    {
        // Arrange
        Set<Id> items = new Set<Id> { '906F0000000BU2XIAW' };
        TestCommand command = new TestCommand();

        // Act
        ActionResult result = command.execute(items);

        //Assert
        system.assertEquals(ResultStatus.SUCCESS, result.Status, 'Executing a command with valid Ids, validating and executing successfully, should return a successful result to the caller.'); 
    }

    @isTest
    static void ExecuteCommandWithSingleIdAfterSuccessfulValidationAndExecution_ReturnsSuccessToCaller()
    {
        // Arrange
        Id item = '906F0000000BU2XIAW';
        TestCommand command = new TestCommand();

        // Act
        ActionResult result = command.execute(item);

        //Assert
        system.assertEquals(ResultStatus.SUCCESS, result.Status, 'Executing a command with valid Ids, validating and executing successfully, should return a successful result to the caller.'); 
    }

    class TestValidationNotImplementedCommand extends BaseIdCommand
    {
    }

    class TestValidationExeceptionCommand extends BaseIdCommand 
    {
        public override Set<Id> validate(Set<Id> params)
        {
            throw new SObjectException('Unexpected exception during validation action');
        }
    }

    class TestCommandNotImplementedCommand extends BaseIdCommand
    {
        public override Set<Id> validate(Set<Id> params)
        {
            return params;
        }
    }

    class TestCommandExceptionCommand extends BaseIdCommand
    {
        public override Set<Id> validate(Set<Id> params)
        {
            return params;
        }

        public override ActionResult command(Set<Id> params)
        {
            throw new SObjectException('Unexpected exception during command action');
        }
    }

    class TestCommand extends BaseIdCommand
    {
        public override Set<Id> validate(Set<Id> params)
        {
            return params;
        }

        public override ActionResult command(Set<Id> params)
        {
            return new ActionResult();
        }
    }
}