public class CaseNotifyOfNewCommentCommand extends BaseQueueableCommand
{
    public CaseNotifyOfNewCommentCommand()
    {
        super(1);
    }

    protected override List<SObject> validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        EmailTemplate template = EmailService.getEmailTemplate(Constants.EMAILTEMPLATE_CASECOMMENT_NOTIFICATION);

        if (template == null)
        {
            String message = String.format('No email template found for {0}.', new List<String> { Constants.EMAILTEMPLATE_CASECOMMENT_NOTIFICATION });
            Log.error(this.className, 'validate', message);
            throw new Exceptions.ApplicationException(message);
        }

        List<CaseComment> comments = new List<CaseComment>();
        List<CaseComment> allComments = [SELECT Id, CommentBody, CreatedById, IsPublished, ParentId, Parent.CaseNumber, Parent.OwnerId, Parent.Owner.Name, Parent.Owner.Email, Parent.ContactId
                                         FROM CaseComment 
                                         WHERE Id In :((new Map<Id, SObject>(items)).keySet())];
        
        for (CaseComment comment: allComments)        
        {
            if (ProfileUtilities.getProfileName(comment.CreatedById) == Constants.PROFILE_AIBAPIUSER)
            {
                Log.info(this.className, 'validate', 'Skip sending Notification as Comment is Bulk Insert by API user');
                continue;
            }

            comments.add(comment);
        }
        
        return comments;
    }

    public override ActionResult command(List<SObject> items, Id jobId)
    {
        if (items.size() == 0)
            return new ActionResult(ResultStatus.SUCCESS); 

        User sysAdmin = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        EmailTemplate template = EmailService.getEmailTemplate(Constants.EMAILTEMPLATE_CASECOMMENT_NOTIFICATION);
        List<CaseComment> comments = [SELECT Id, CommentBody, CreatedById, IsPublished, ParentId, Parent.CaseNumber, Parent.OwnerId, Parent.Owner.Name, Parent.Owner.Email, Parent.ContactId 
                                      FROM CaseComment 
                                      WHERE Id In :((new Map<Id, SObject>(items)).keySet())];

        for (CaseComment comment: comments)
        {
            List<ViewModelsMail.Email> emails = new List<ViewModelsMail.Email>();
            Messaging.SingleEmailMessage singleEmail = Messaging.renderStoredEmailTemplate(template.Id, comment.Id, comment.Id);

            if (comment.CreatedById == comment.Parent.OwnerId && comment.IsPublished)
            {
                Contact student = ContactService.get(comment.Parent.ContactId, 'Name, Email', false);
                ViewModelsMail.Email mail = new ViewModelsMail.Email(comment.Id, singleEmail.getSubject(), singleEmail.getPlainTextBody(), singleEmail.getHtmlBody(), student.Email, student.Name, null, null);
                emails.add(mail);
            }
            else if (ProfileUtilities.getProfileName(comment.CreatedById) == Constants.PROFILE_STUDENTPORTAL)
            {
                ViewModelsMail.Email mail = new ViewModelsMail.Email(comment.Id, singleEmail.getSubject(), singleEmail.getPlainTextBody(), singleEmail.getHtmlBody(), comment.Parent.Owner.Email, comment.Parent.Owner.Name, null, null);
                emails.add(mail);
            }
            else
            {
                ViewModelsMail.Email mailOwner = new ViewModelsMail.Email(comment.Id, singleEmail.getSubject(), singleEmail.getPlainTextBody(), singleEmail.getHtmlBody(), comment.Parent.Owner.Email, comment.Parent.Owner.Name, null, null);
                emails.add(mailOwner);
                
                if (comment.IsPublished)
                {
                    Contact student = ContactService.get(comment.Parent.ContactId, 'Name, Email', false);
                    ViewModelsMail.Email mailStudent = new ViewModelsMail.Email(comment.Id, singleEmail.getSubject(), singleEmail.getPlainTextBody(), singleEmail.getHtmlBody(), student.Email, student.Name, null, null);
                    emails.add(mailStudent);
                }
            }

            for (ViewModelsMail.Email mail: emails)
                sendEmail(mail.ToAddress, mail.ToName, mail.Subject, sysAdmin.Email, sysAdmin.Name, mail.BodyText, mail.BodyHtml);
        }

        return new ActionResult(ResultStatus.SUCCESS);
    }

    @future (callout = true)
    private static void sendEmail(String toAddress, string toName, String subject, String fromAddress, String fromName, String textBody, String htmlBody)
    {
        ActionResult result = EmailService.sendToRecipient(Constants.APIPROVIDER_DEFAULT, toAddress, toName, subject, fromAddress, fromName, textBody, htmlBody, null);

        if (result.isError)
        {
            String message = 'Failed to send Notification Email to ' + toAddress + ' as ' + result.message;
            EmailService.send(new String[] {fromAddress}, 'Failed to send Notification Email', message);

            Log.error('CaseCommentNotificationCommand', 'sendEmail', message);
            Log.save();
        }
    }
}