public class ContentVersionShareWithUsersCommand extends BaseTriggerCommand
{
    public override List<SObject> validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        List<ContentVersion> result = new List<ContentVersion>();

        for (ContentVersion version : (List<ContentVersion>)items)
        {
            if (triggerContext == Enums.TriggerContext.AFTER_INSERT &&
                UserInfo.getProfileId() != System.Label.IDProfileAIBStudentPortalLoginUser &&
                UserInfo.getProfileId() != System.Label.IDProfileAIBStudentPortalUser)
                result.add(version);
        }

        return result;
    }

    public override ActionResult command(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        Id orgId = UserInfo.getOrganizationId();
        Set<Id> fileIds = new Set<Id>();

        for (ContentVersion version: (List<ContentVersion>)items)
            fileIds.add(version.ContentDocumentId);

        List<ContentDocumentLink> links =
            [
                SELECT Id, ContentDocumentId
                FROM ContentDocumentLink
                WHERE ContentDocumentId IN : fileIds AND LinkedEntityId = : orgId
            ];
        Map<Id, ContentDocumentLink> linkMap = new Map<Id, ContentDocumentLink>();

        for (ContentDocumentLink link: links)
            linkMap.put(link.ContentDocumentId, link);

        for (ContentVersion version: (List<ContentVersion>)items)
        {
            if (linkMap.containsKey(version.ContentDocumentId))
                continue;

            ContentDocumentLink link = new ContentDocumentLink();
            link.ContentDocumentId = version.ContentDocumentId;
            link.LinkedEntityId = orgId;
            link.ShareType = Constants.FILE_CONTENT_SHARETYPE_COLLABORTOR;
            link.Visibility = Constants.FILE_CONTENT_VISIBILITY_ALL;

            insert link;
        }

        return new ActionResult();
    }
}