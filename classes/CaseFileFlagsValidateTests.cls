@isTest
public class CaseFileFlagsValidateTests
{
    @testSetup
    static void setup()
    {
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        TestDataCaseMock.createApplication(user);
    }

    @isTest
    static void AppCaseSetVettingCompleteToTrueWithNoFile_NoFileErrorReturned()
    {
        // Arrange
        String errorMessage = '';
        Case applicationCase = [SELECT Id, Vetting_Complete__c FROM Case LIMIT 1];

        // Act
        try
        {
            applicationCase.Vetting_Complete__c = true;
            update applicationCase;
        }
        catch(Exception ex)
        {
            errorMessage = ex.getMessage();
        }

        // Assert
        System.assert(errorMessage.contains(CaseFileFlagsValidate.ERROR_MSG_NOLOOFILE), 'Unexpected error message');
    }

    @isTest
    static void CaseLOOFileAddedAndVettingCompleteSetToTrue_VettingCompleteValueSetSuccessfully()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        Case applicationCase = [SELECT Id, Vetting_Complete__c FROM Case LIMIT 1];
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);

        // Act
        Test.startTest();
        TestDataContentVersionMock.create(user, 'John LOO File.txt', 'John LOO File', applicationCase.Id);
        applicationCase.Vetting_Complete__c = true;
        update applicationCase;
        Test.stopTest();
        Case updatedCase = [SELECT Id, Vetting_Complete__c FROM Case WHERE Id = :applicationCase.Id];

        // Assert
        System.assertEquals(true, updatedCase.Vetting_Complete__c, 'Unexpected Vetting Complete field value');
    }

    @isTest
    static void NonAppCaseSetVettingCompleteToTrueWithNoFile_ValueSetSuccessfully()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact contact = [SELECT Id FROM Contact LIMIT 1];
        Case awardCase = TestDataCaseMock.createAwardCase(user, contact);
        awardCase.Vetting_Complete__c = true;

        // Act
        update awardCase;
        Case updatedCase = [SELECT Id, Vetting_Complete__c FROM Case WHERE Id = :awardCase.Id];

        // Assert
        System.assertEquals(true, updatedCase.Vetting_Complete__c, 'Unexpected Vetting Complete field value');
    }

    @isTest
    static void AppCaseSetInvoiceGeneratedValueToTrueWithNoFile_NoFileErrorReturned()
    {
        // Arrange
        String errorMessage = '';
        Case applicationCase = [SELECT Id, Invoice_Generated__c FROM Case LIMIT 1];

        // Act
        try
        {
            applicationCase.Invoice_Generated__c = true;
            update applicationCase;
        }
        catch(Exception ex)
        {
            errorMessage = ex.getMessage();
        }

        // Assert
        System.assert(errorMessage.contains(CaseFileFlagsValidate.ERROR_MSG_NOINVOICEFILE), 'Unexpected error message');
    }

    @isTest
    static void CaseInvoiceFileAdded_InvoiceGeneratedValueSetSuccessfully()
    {
        // Arrange
        Case applicationCase = [SELECT Id, Invoice_Generated__c FROM Case LIMIT 1];
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        applicationCase.Invoice_Generated__c = true;

        // Act
        Test.startTest();
        TestDataContentVersionMock.create(user, 'John INV Smith.txt', 'John INV Invoice', applicationCase.Id);
        update applicationCase;
        Test.stopTest();
        Case updatedCase = [SELECT Id, Invoice_Generated__c FROM Case WHERE Id = :applicationCase.Id];

        // Assert
        System.assertEquals(true, updatedCase.Invoice_Generated__c, 'Unexpected Invoice Generated field value');
    }

    @isTest
    static void NonAppCaseSetInvoiceGeneratedToTrueWithNoFile_ValueSetSuccessfully()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact contact = [SELECT Id FROM Contact LIMIT 1];
        Case awardCase = TestDataCaseMock.createAwardCase(user, contact);
        awardCase.Invoice_Generated__c = true;

        // Act
        update awardCase;
        Case updatedCase = [SELECT Id, Invoice_Generated__c FROM Case WHERE Id = :awardCase.Id];

        // Assert
        System.assertEquals(true, updatedCase.Invoice_Generated__c, 'Unexpected Invoice Generated field value');
    }
}