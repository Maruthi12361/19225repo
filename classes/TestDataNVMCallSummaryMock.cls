@isTest
public class TestDataNVMCallSummaryMock
{
    public static NVMStatsSF__NVM_Call_Summary__c create(User user)
    {
        return create(user, null);
    }

    public static NVMStatsSF__NVM_Call_Summary__c create(User user, Task task)
    {
        NVMStatsSF__NVM_Call_Summary__c callSummary = new NVMStatsSF__NVM_Call_Summary__c
        (
            OwnerId = user.Id,
            NVMStatsSF__TaskID__c = task != null ? task.Id : null
        );

        System.RunAs(user)
        {
            insert callSummary;
        }

        return callSummary;
    }
}