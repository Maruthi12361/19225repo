public class SingleSignOnUtilities extends BaseComponent
{
    private static SingleSignOnConfiguration__mdt singleSignOnConfig;
    private static String accessToken;
    private static String applicationAccessToken;
    private static Map<String, String> countryCodesMap;
    private static Map<String, ViewModelsSSO.UserGroup> groupsMap;

    public static SingleSignOnConfiguration__mdt getConfiguration()
    {
        if (singleSignOnConfig == null)
            singleSignOnConfig = getConfiguration(ConfigUtility.getCurrentEnvironment());

        if (singleSignOnConfig == null)
        {
            if (Test.isRunningTest())
                return TestDataSingleSignOnConfigurationMock.create();

            singleSignOnConfig = new SingleSignOnConfiguration__mdt();
            singleSignOnConfig.AzureAPIEndpoint__c = Constants.SINGLESIGNON_ACCESS_RESOURCEURL + '/aibtest.com.au';
            singleSignOnConfig.CerebroAPIEndpoint__c = 'https://api.aibtest.edu.au';
            singleSignOnConfig.StudentDomain__c = '@study.aibtest.edu.au';
        }

        return singleSignOnConfig;
    }

    public static Map<String, String> getCountryCodes()
    {
        if (countryCodesMap != null)
            return countryCodesMap;

        countryCodesMap = new Map<String, String>();
        List<SSO_Country_Codes__c> countryCodes = SSO_Country_Codes__c.getAll().values();

        for (SSO_Country_Codes__c countryCode : countryCodes)
            countryCodesMap.put(countryCode.Country__c.toUpperCase(), countryCode.code__c.toUpperCase());

        return countryCodesMap;
    }

    private static void getToken(Enums.SSOTokenType tokenType, Boolean authRequired)
    {
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        SingleSignOnConfiguration__mdt config = getConfiguration();

        String resource = '';

        if (tokenType == Enums.SSOTokenType.ACCESS)
            resource = Constants.SINGLESIGNON_ACCESS_RESOURCEURL;
        else if (tokenType == Enums.SSOTokenType.APPLICATION)
            resource = getConfiguration().CerebroResource__c;

        String azureEndpoint = config.AzureAPIEndpoint__c + Constants.SINGLESIGNON_APIENDPOINT_OAUTHTOKEN;
        String body = 'grant_type=' + Constants.SINGLESIGNON_ACCESS_GRANTTYPE 
            + '&client_secret=' + EncodingUtil.urlEncode(config.AzureAPISecret__c, 'UTF-8')
            + '&resource=' + EncodingUtil.urlEncode(resource, 'UTF-8')
            + '&client_id=' + EncodingUtil.urlEncode(config.AzureAPIID__c, 'UTF-8')
            + '&username=' + EncodingUtil.urlEncode(config.AzureUsername__c, 'UTF-8')
            + '&password=' + EncodingUtil.urlEncode(config.AzurePassword__c, 'UTF-8');
        String bodyLength =  body.length().format();

        request.setEndpoint(azureEndpoint);
        request.setMethod('POST');
        request.setHeader('Accept', 'application/json');
        request.setHeader('Host', 'login.windows.net');
        request.setHeader('Cache-Control', 'no-cache');
        request.setHeader('Content-Length', bodyLength);
        request.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        if (authRequired)
            request.setHeader('Authorization', 'Bearer '+ accessToken);
        request.setBody(body);
        request.setTimeout(120000);

        Log.info('SingleSignOnUtilities', 'getToken', 'Getting access token from Azure AD.', request, new String[] { 'Accept', 'Host', 'Cache-Control', 'Content-Type' });

        HttpResponse response = http.send(request);

        if (response.getStatusCode() != 200)
        {
            Log.error('SingleSignOnUtilities', 'getToken', 'Error getting access token. Error: {0}', new String[] { response.toString() });
            return;
        }

        Map<String, Object> bodyMap = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
        String token = (String) bodyMap.get('access_token');

        if (tokenType == Enums.SSOTokenType.ACCESS)
            accessToken = token;
        else if (tokenType == Enums.SSOTokenType.APPLICATION)
            applicationAccessToken = token;

        Log.info('SingleSignOnUtilities', 'getSingleSignOnAccessToken', 'Retrieved access token: ' + accessToken);
    }

    private static SingleSignOnConfiguration__mdt getConfiguration(String name)
    {
        List<SingleSignOnConfiguration__mdt> configs =
        [
            SELECT DeveloperName, AzureAPIEndpoint__c, AzureAPIID__c, AzureAPISecret__c, AzurePassword__c, AzureUsername__c, CerebroAPIEndpoint__c,
                CerebroResource__c, StudentDomain__c
            FROM SingleSignOnConfiguration__mdt 
            WHERE DeveloperName = :name
        ];

        if (configs.size() == 1)
            return configs[0];
        else if (configs.size() > 1)
            throw new Exceptions.ApplicationException('Multiple SingleSignOnConfiguration metadata found with same developer name ' + name);
        else
            return null;
    }

    private static Map<String, ViewModelsSSO.UserGroup> parseGroups(String jsonString)
    {
        Map<String, ViewModelsSSO.UserGroup> groupsMap = new  Map<String, ViewModelsSSO.UserGroup>();
        JSONParser parser = JSON.createParser(jsonString);

        while (parser.nextToken() != null) 
        {
            if (parser.getCurrentToken() == JSONToken.START_ARRAY) 
            {
                while (parser.nextToken() != null) 
                {
                    if (parser.getCurrentToken() == JSONToken.START_OBJECT) 
                    {
                        ViewModelsSSO.UserGroup userGroup = (ViewModelsSSO.UserGroup)parser.readValueAs(ViewModelsSSO.UserGroup.class);
                        groupsMap.put(userGroup.displayName, userGroup);
                    }
                }
            }
        }

        return groupsMap;
    }

    private static Boolean assignUserToGroup(String userObjectId, String groupObjectId)
    {
        SingleSignOnConfiguration__mdt config = getConfiguration();

        String endPoint = config.AzureAPIEndpoint__c + Constants.SINGLESIGNON_APIENDPOINT_USERASSIGNGROUP(groupObjectId);
        String body =
            '{' +
                '"url": "' + config.AzureAPIEndpoint__c + Constants.SINGLESIGNON_APIENDPOINT_OBJECTDIRECTORY(userObjectId) + '"' +
            '}';

        HttpResponse response = postRequest(config.AzureAPIEndpoint__c + Constants.SINGLESIGNON_APIENDPOINT_USERASSIGNGROUP(groupObjectId), body);
        
        if (response.getStatusCode() == 204)
        {
            Log.info('SingleSignOnUtilities', 'assignUserToGroup', 'Successfully added the user to group in Azure AD.');
            return true;
        }

        Log.error('SingleSignOnUtilities', 'assignUserToGroup', 'Failed to add user to group Error Code: {0}, Error Body: {1}',
            new String [] { String.valueOf(response.getStatusCode()), response.getBody() });
        throw new Exceptions.ApplicationException('Failed to add user to a group');
    }

    private static HttpResponse getRequest(String endpoint)
    {
        Http http = new Http();
        HttpRequest request = new HttpRequest();

        Log.info('SingleSignOnUtilities', 'getRequest', 'Get request endpoint {0}', new List<String> { endpoint });

        request.setEndpoint(endpoint);
        request.setMethod('GET');
        request.setHeader('Accept', 'application/json');
        request.setHeader('Authorization', 'Bearer '+ getAccessToken());
        request.setHeader('Content-Type', 'application/json');
        request.setHeader('Host', 'graph.windows.net');
        request.setTimeout(120000);

        Log.info('SingleSignOnUtilities', 'getRequest', 'Get request', request, new String[] { 'Accept', 'Authorization', 'Content-Type'});

        return http.send(request);
    }

    private static HttpResponse postRequest(String endpoint, String body)
    {
        Http http = new Http();
        HttpRequest request = new HttpRequest();

        Log.info('SingleSignOnUtilities', 'postRequest', 'Post request endpoint {0}', new List<String> { endpoint });

        request.setEndpoint(endPoint);
        request.setMethod('POST');
        request.setHeader('Accept', 'application/json');
        request.setHeader('Authorization', 'Bearer ' + getAccessToken());
        request.setHeader('Content-Type', 'application/json');
        request.setBody(body);
        request.setTimeout(120000);

        Log.info('SingleSignOnUtilities', 'postRequest', 'Post request', request, new String[] { 'Accept', 'Authorization', 'Content-Type'});

        return http.send(request);
    }

    public static String getAccessToken()
    {
        if (String.isBlank(accessToken))
            getToken(Enums.SSOTokenType.ACCESS, false);

        return accessToken;
    }

    public static String getApplicationAccessToken()
    {
        if (String.isBlank(applicationAccessToken))
            getToken(Enums.SSOTokenType.APPLICATION, true);

        return applicationAccessToken;
    }

    public static String getUserObjectId(String identifier)
    {
        String userPrincipalName = EncodingUtil.urlEncode(identifier,'UTF-8');
        String endPoint = getConfiguration().AzureAPIEndpoint__c + Constants.SINGLESIGNON_APIENDPOINT_USEROBJECT(userPrincipalName);
        HttpResponse response = getRequest(endpoint);

        if (response.getStatusCode() == 200)
        {
            Map<String, Object> responseMap = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());

            if (responseMap.containsKey('objectId'))
            {
                String userObjectId = (String) responseMap.get('objectId');
                if (String.isNotBlank(userObjectId))
                    return userObjectId;
            }

            Log.error('SingleSignOnUtilities', 'getUserObjectId', 'No user object id found');
            throw new Exceptions.ApplicationException('No user object id found');
        }

        if (response.getStatusCode() == 404)
        {
            Log.info('SingleSignOnUtilities', 'getUserObjectId', 'User does not exist in Azure AD.');
            return null;
        }

        String message = 'Error requesting users reference ObjectID. Unexpected response code from service. Response Code: ' + response.getStatusCode() + '. Response Status : '+ response.getStatus();
        Log.error('SingleSignOnUtilities', 'getUserObjectId', message);

        throw new Exceptions.ApplicationException(message);
    }

    public static String generatePassword(Integer length, String suffix)
    {
        Blob blobKey = crypto.generateAesKey(128);
        String key = EncodingUtil.convertToHex(blobKey);
        String password = key.substring(0,length) + suffix;

        Log.info('SingleSignOnUtilities', 'generatePassword', 'Generated password of ' + password);

        return password;
    }

    public static Set<String> getUserGroups(String userObjectId)
    {
        Set<String> allGroups = new Set<String>();
        String endPoint = getConfiguration().AzureAPIEndpoint__c + Constants.SINGLESIGNON_APIENDPOINT_USERGROUPS(userObjectId);
        String body =
            '{' +
                '"securityEnabledOnly": false' +
            '}';

        HttpResponse response = postRequest(endPoint, body);
        JSONParser parser = JSON.createParser(response.getBody());

        while (parser.nextToken() != null) 
        {
            if (parser.getCurrentToken() == JSONToken.START_ARRAY) 
            {
                while (parser.nextToken() != null) 
                {
                    String userGroup = parser.getText();

                    if (userGroup != null && userGroup.trim().length() != 0)
                        allGroups.add(userGroup);
                }
            }
        }

        Log.info('SingleSignOnUtilities', 'getUserGroups', 'User is in groups: ' + allGroups);

        return allGroups;
    }

    public static Map<String, ViewModelsSSO.UserGroup> getGroups()
    {
        if (groupsMap != null)
            return groupsMap;

        HttpResponse response = getRequest(getConfiguration().AzureAPIEndpoint__c + Constants.SINGLESIGNON_APIENDPOINT_ALLGROUPS);

        if (response.getStatusCode() == 200)
            groupsMap = parseGroups(response.getBody());
        else
            Log.error('SingleSignOnUtilities', 'getAzureGroups', 'Error retrieving directory groups. Unexpected response code from service. Response Code: '+ response.getStatusCode() + '. Response Status : '+ response.getStatus());

        Log.info('SingleSignOnUtilities', 'getAzureGroups', 'Retrieved directory groups: ' + String.valueOf(groupsMap));

        return groupsMap;
    }

    public static boolean existsInUserGroup(String userObjectId, String groupName)
    {
        ViewModelsSSO.UserGroup azureUserGroup;
        Map<String, ViewModelsSSO.UserGroup> groups = SingleSignOnUtilities.getGroups();

        Log.info('SingleSignOnUtilities', 'existsInGroup', 'Checking if user {0} exists in user group {1} ', new List<String> { userObjectId, groupName });

        if (groups.size() == 0)
        {
            Log.error('SingleSignOnUtilities', 'existsInGroup', 'No valid user groups found');
            throw new Exceptions.ApplicationException('No valid user groups found');
        }
        else if (groups.get(groupName) == null)
        {
            Log.error('SingleSignOnUtilities', 'existsInGroup', '{0} is not a valid user group', new List<String> { groupName });
            throw new Exceptions.ApplicationException('Invalid user group. Group does not exists');
        }

        azureUserGroup = groups.get(groupName);

        if (String.isEmpty(azureUserGroup.objectId))
        {
            Log.error('SingleSignOnUtilities', 'existsInGroup', '{0} does not contain object id value', new List<String> { azureUserGroup.displayName });
            throw new Exceptions.ApplicationException('Invalid user group. Missing object id');
        }

        Set<String> userGroups = getUserGroups(userObjectId);
        for (String name : userGroups)
        {
            if (name.equals(azureUserGroup.objectId))
            {
                Log.info('SingleSignOnUtilities', 'existsInGroup', 'User exists in user group ' + groupName);
                return true;
            }
        }

        Log.info('SingleSignOnUtilities', 'existsInGroup', 'User does not exist in user group ' + groupName);
        return false;
    }

    public static Boolean addUserToGroup(String userObjectId, String groupName)
    {
        if (String.isNotEmpty(userObjectId))
        {
            ViewModelsSSO.UserGroup userGroup = getGroups().get(groupName);
            return assignUserToGroup(userObjectId, userGroup.objectId);
        }
        else
        {
            Log.info('SingleSignOnUtilities', 'addUserToGroup', 'User object ID is empty. Cannot add user to {0} group', new List<String> { groupName });
            throw new Exceptions.ApplicationException('Failed to retrieved user object identifier');
        }
    }

    public static String createUser(Contact contact, String signonId, String password)
    {
        Http http = new Http();
        HttpRequest request = new HttpRequest();

        String body = '';
        String countryCode  = Constants.COUNTRYCODE_AUSTRALIA;
        Map<String, String> countryCodeMap = getCountryCodes();
        String country = String.isNotBlank(contact.MailingCountry) ? contact.MailingCountry.trim().toUpperCase() : null;

        if (String.isNotEmpty(country) && String.isNotEmpty(countryCodeMap.get(country)))
            countryCode = countryCodeMap.get(country);

        body =
            '{ ' +
                '"accountEnabled": true,' +
                '"displayName": "' + contact.name + '", ' +
                '"mailNickname": "' + contact.Cerebro_Student_ID__c + '",' +
                '"passwordProfile": { "password": "' + password + '", "forceChangePasswordNextLogin": true}, ' +
                '"userPrincipalName": "' + signonId + '",' +
                '"usageLocation": "' + countryCode + '", ' +
                '"givenName": "' + contact.FirstName + '", ' +
                '"surname": "' + contact.LastName + '" ' +
            '}';

        HttpResponse response = postRequest(getConfiguration().AzureAPIEndpoint__c + Constants.SINGLESIGNON_APIENDPOINT_USER, body);

        if (response.getStatusCode() != 201)
        {
            Log.error('SingleSignOnUtilities', 'createUser', 'Error provisioning student. Error Code: {0}, Error Body: {1}',
                new String [] { String.valueOf(response.getStatusCode()), response.getBody() });

            throw new Exceptions.ApplicationException('Error provisioning student in Azure. Error Code: ' + response.getStatusCode());
        }

        Map<String, Object> responseMap = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());

        if (responseMap.containsKey('objectId'))
        {
            String userObjectId = (String) responseMap.get('objectId');
            if (String.isNotBlank(userObjectId))
                return userObjectId;
        }

        Log.error('SingleSignOnUtilities', 'createUser', 'Error creating user. No Object Id found in response body');
        throw new Exceptions.ApplicationException('Failed to retrieve object id');
    }

    public static Boolean deleteUser(String signonId)
    { 
        Http http = new Http();
        HttpRequest request = new HttpRequest();

        Log.info('SingleSignOnUtilities', 'deleteUser', 'Deleting user with {0} single sign on id.', new List<String> { signonId });

        request.setEndpoint(getConfiguration().AzureAPIEndpoint__c + Constants.SINGLESIGNON_APIENDPOINT_USEROBJECT(EncodingUtil.urlEncode(signonId,'UTF-8')));
        request.setMethod('DELETE');
        request.setHeader('Accept', 'application/json');
        request.setHeader('Authorization', 'Bearer ' + getAccessToken());
        request.setHeader('Content-Type', 'application/json');
        request.setHeader('Host', 'graph.windows.net');
        request.setTimeout(120000);

        Log.info('SingleSignOnUtilities', 'deleteUser', 'Deleting student in Azure AD.', request, new String[] { 'Accept', 'Authorization', 'Content-Type', 'Host' });

        HttpResponse response = http.send(request);
        
        if (response.getStatusCode() != 204)
        {                   
            Log.error('SingleSignOnUtilities', 'deleteUser', 'Error deleting user in Azure AD. Error Code: {0}, Error Body: {1}',
                new String [] { String.valueOf(response.getStatusCode()), response.getBody() });
            throw new Exceptions.ApplicationException('Could not delete user account ' + signonId + '. Error Code' + response.getStatusCode());
        }

        Log.info('SingleSignOnUtilities', 'deleteUser', 'Successfully deleted user in Azure AD');

        return true;
    }

    public static Boolean resetPassword(String userObjectId, String password)
    { 
        Http http = new Http();
        HttpRequest request = new HttpRequest();

        Log.info('SingleSignOnUtilities', 'resetUser', 'Resetting user password with {0} user object id.', new List<String> { userObjectId });

        String body =
            '{' +
                '"passwordProfile": ' +
                '{' +
                    '"password": "' + password + '", ' +
                    '"forceChangePasswordNextLogin": true' +
                '}' +
            '}';

        request.setEndpoint(getConfiguration().AzureAPIEndpoint__c + Constants.SINGLESIGNON_APIENDPOINT_USEROBJECT(EncodingUtil.urlEncode(userObjectId,'UTF-8')));
        request.setMethod('POST');
        request.setHeader('X-HTTP-Method-Override','PATCH');
        request.setHeader('Accept', 'application/json');
        request.setHeader('Authorization', 'Bearer ' + getAccessToken());
        request.setHeader('Content-Type', 'application/json');
        request.setHeader('Host', 'graph.windows.net');
        request.setTimeout(120000);
        request.setBody(body);

        Log.info('SingleSignOnUtilities', 'resetPassword', 'Resetting student in Azure AD.', request, new String[] { 'Accept', 'Authorization', 'Content-Type', 'Host' });

        HttpResponse response = http.send(request);

        System.debug(response.getBody());
        
        if (response.getStatusCode() != 204)
        {                   
            Log.error('SingleSignOnUtilities', 'resetUser', 'Error reseting user password in Azure AD. Error Code: {0}, Error Body: {1}',
                new String [] { String.valueOf(response.getStatusCode()), response.getBody() });
            throw new Exceptions.ApplicationException('Could not reset user password ' + userObjectId + '. Error Code' + response.getStatusCode());
        }

        Log.info('SingleSignOnUtilities', 'resetUser', 'Successfully reset user password in Azure AD');

        return true;
    }
}