@RestResource(urlMapping = '/academic-config/*')
global class AcademicConfigController
{
    @HttpGet
    global static void get()
    {
        try
        {
            RestRequest req = RestContext.request;

            RestContext.response.addHeader('Content-Type', 'application/json');
            RestContext.response.responseBody = Blob.valueOf(JSON.serialize(AcademicConfigService.get()));
            RestContext.response.statusCode = 200;
        }
        catch (Exception ex)
        {
            Map<String, String> errorMap = new Map<String, String>
            {
                'Message' => ex.getMessage(),
                'Details' => ex.getStackTraceString()
            };

            RestContext.response.addHeader('Content-Type', 'application/json');
            RestContext.response.responseBody = Blob.valueOf(JSON.serialize(errorMap));
            RestContext.response.statusCode = 400;
        }
    }
}