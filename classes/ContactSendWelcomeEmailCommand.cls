public class ContactSendWelcomeEmailCommand extends BaseQueueableCommand
{
    public ContactSendWelcomeEmailCommand()
    {
        super(50, 5);
    }

    public override List<SObject> validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        List<Contact> filteredList = new List<Contact>();

        for (Contact contact: (List<Contact>)items)
        {
            if (contact.RecordTypeId != RecordTypeUtils.getRecordTypeId(Constants.RECORDTYPE_CONTACT_PROSPECT, SObjectType.Contact))
            {
                Log.info(this.className, 'validate', 'Skip process as Contact is not Prospect RecordType.');
                continue;
            }
            else if (contact.FirstName == null || String.isBlank(contact.FirstName))
            {
                Log.info(this.className, 'validate', 'Skip process as Contact does not have First Name.');
                continue;
            }
            else if (contact.WelcomeProspectEmailSent__c)
            {
                Log.info(this.className, 'validate', 'Welcome letter has already been sent.');
                continue;
            }
            else if (triggerContext == Enums.TriggerContext.AFTER_UPDATE && SObjectUtils.hasPropertyChanged(contact, oldMap.get(contact.Id), 'WelcomeProspectEmailSent__c'))
            {
                continue;
            }
            else
                filteredList.add(contact);
        }

        return filteredList;
    }

    public override ActionResult command(List<SObject> items, Id jobId)
    {
        if (items.size() == 0)
            return new ActionResult(ResultStatus.SUCCESS);

        List<Contact> updatedContacts = new List<Contact>();
        List<ViewModelsMail.WelcomeEmail> welcomeEmails = new List<ViewModelsMail.WelcomeEmail>();

        List<Contact> contacts = ContactService.getMany(
            (
                new Map<Id, SObject>(items)).keySet(),
                    'Id, CreatedDate, EncryptedId__c, FirstName, LastName, LeadSource, InferredCountry__c, InferredStateRegion__c, InformationPack__c, ' +
                    'Email, Program__c, Portfolio__c, Owner.Email, Owner.Name, Status__c, WelcomeProspectEmailSent__c', false);

        for (Contact contact: contacts)
        {
            if (contact.WelcomeProspectEmailSent__c)
                continue;

            EmailTemplate template = null;

            if (contact.Program__c == 'GCM')
                template = EmailService.getEmailTemplate(Constants.EMAILTEMPLATE_WELCOME_GCM);
            else
                template = EmailService.getEmailTemplate(Constants.EMAILTEMPLATE_WELCOME_MBA);

            if (template == null)
            {
                Log.error(this.className, 'command', 'Failed to send Welcome letter to {0} as there was no Welcome Email Template for {1}.', new List<String>{ contact.Name, contact.Program__c });
                continue;
            }

            Messaging.SingleEmailMessage mail = Messaging.renderStoredEmailTemplate(template.Id, contact.Id, contact.Id);

            ViewModelsMail.WelcomeEmail welcomeEmail = new ViewModelsMail.WelcomeEmail();
            welcomeEmail.FirstName = contact.FirstName;
            welcomeEmail.LastName = contact.LastName;
            welcomeEmail.Email = contact.Email;
            welcomeEmail.ContactId = contact.Id;
            welcomeEmail.Program = contact.Program__c;
            welcomeEmail.Portfolio = contact.Portfolio__c;
            welcomeEmail.LeadSource = contact.LeadSource;
            welcomeEmail.EncryptedId = contact.EncryptedId__c;
            welcomeEmail.InformationPack = contact.InformationPack__c;
            welcomeEmail.EmailBodyText = mail.getPlainTextBody();
            welcomeEmail.EmailBodyHtml = mail.getHtmlBody();
            welcomeEmail.EmailSubject = mail.getSubject();
            welcomeEmail.EmailFromName = contact.Owner.Name;
            welcomeEmail.EmailFromAddress = contact.Owner.Email;

            welcomeEmail.Status = contact.Status__c;
            welcomeEmail.InferredCountry = contact.InferredCountry__c;
            welcomeEmail.InferredStateRegion = contact.InferredStateRegion__c;
            welcomeEmail.ContactCreated = String.valueOf(contact.CreatedDate.date());

            welcomeEmails.add(welcomeEmail);
            updatedContacts.add(contact);
        }

        List<Contact> contactsForUpdate = ContactService.getMany((new Map<Id, SObject>(updatedContacts)).keySet(), 'Id, WelcomeProspectEmailSent__c', true);

        for (Contact contact: contactsForUpdate)
            contact.WelcomeProspectEmailSent__c = true;

        if (contactsForUpdate.size() > 0)
            update contactsForUpdate;

        for (ViewModelsMail.WelcomeEmail welcomeEmail : welcomeEmails)
            sendEmail(welcomeEmail.ContactId, welcomeEmail.Email, welcomeEmail.FirstName, welcomeEmail.LastName, welcomeEmail.EmailSubject,
                welcomeEmail.EmailBodyText, welcomeEmail.EmailBodyHtml, welcomeEmail.EmailFromAddress, welcomeEmail.EmailFromName, welcomeEmail.Program, 
                welcomeEmail.Portfolio, welcomeEmail.LeadSource, welcomeEmail.InformationPack, welcomeEmail.EncryptedId, welcomeEmail.Status,
                welcomeEmail.InferredCountry, welcomeEmail.InferredStateRegion, welcomeEmail.ContactCreated);

        return new ActionResult(ResultStatus.SUCCESS);
    }

    @future(callout = true)
    private static void sendEmail(Id contactId, String email, String firstName, String lastName, String subject, String bodyText, String bodyHtml, String ownerEmail, String ownerName,
                                  String program, String portfolio, String leadSource, String infoPack, String encryptedId, String status, String inferredCountry,
                                  String inferredStateRegion, String contactCreated)
    {
        ViewModelsMail.WelcomeEmail mail = new ViewModelsMail.WelcomeEmail();
        ViewModelsMail.MarketingCloudPayload payload = new ViewModelsMail.MarketingCloudPayload();

        mail.FirstName = firstName;
        mail.LastName = lastName;
        mail.Email = email;
        mail.ContactId = contactId;
        mail.Program = program;
        mail.Portfolio = portfolio;
        mail.LeadSource = leadSource;
        mail.EncryptedId = encryptedId;
        mail.InformationPack = infoPack;
        mail.EventId = String.valueOf(DateTime.now().getTime());
        mail.EmailBodyText = bodyText;
        mail.EmailBodyHtml = bodyHtml;
        mail.EmailSubject = subject;
        mail.EmailFromName = ownerName;
        mail.EmailFromAddress = ownerEmail;
        mail.Status = status;
        mail.InferredCountry = inferredCountry;
        mail.InferredStateRegion = inferredStateRegion;
        mail.ContactCreated = contactCreated;

        payload.ContactKey  = String.valueOf(DateTime.now().getTime());
        payload.EventDefinitionKey = Constants.MARKETINGCLOUD_SENDWELCOME_EVENT_PROD;
        payload.Data = mail;

        if (ConfigUtility.isSandbox())
        {
            payload.EventDefinitionKey = Constants.MARKETINGCLOUD_SENDWELCOME_EVENT_SANDBOX;

            if (!Test.isRunningTest())
                mail.Email = Label.SMTPOverrideEmail;
        }

        ActionResult result = MarketingCloudUtils.sendRequest(payload);

        if (result.isError)
        {
            User sysAdmin = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
            String message = 'Failed to send Welcome Email to ' + mail.Email + ' as ' + result.message;
            EmailService.sendLegacy(sysAdmin.Email, 'Failed to send Welcome Email', message);
            Log.error('ContactSendWelcomeEmailCommand', 'sendEmail', message);
            Log.save();
            Contact contact = ContactService.get(contactId, 'Id, WelcomeProspectEmailSent__c', true);
            contact.WelcomeProspectEmailSent__c = false;
            update contact;
        }
    }
}