@isTest
public class BaseQueueableCommandTests
{
    @testSetup
    static void setup()
    {
        TestUtilities.setupHEDA();
    }

    @isTest
    static void ExecuteQueuable_ProcessesAllObjects()
    {
        // Arrange
        ActionResult result;
        List<Case> results;
        List<Case> cases = new List<Case>();
        TestBaseQueueableCommand command = new TestBaseQueueableCommand();

        for (Integer i = 0; i < 10; i++)
        {
            Case newCase = new Case();
            newCase.Subject = 'New ' + System.now().format('HHmmssSS');
            newCase.Description = 'BaseQueueableTest';

            cases.add(newCase);
        }

        insert cases;

        cases = [SELECT Id, Subject, Description FROM Case];

        // Act
        Test.startTest();
        result = command.execute(cases, new Map<Id, SObject>(), Enums.TriggerContext.AFTER_INSERT);
        Test.stopTest();

        results = [SELECT Id, Subject, Description FROM Case WHERE Subject LIKE 'Updated%'];

        // Assert
        System.assertNotEquals(null, results, 'Request should return a valid collection of items');
        System.assertEquals(10, results.size(), 'All items should have updated subjects once queued items have been executed');
    }

    @isTest
    static void ExecuteCommandWithNullParameters_RaisesExeceptionToCaller()
    {
        // Arrange
        List<SObject> nullObj;
        TestBaseQueueableCommand command = new TestBaseQueueableCommand();

        // Act
        try
        {
            command.execute(nullObj, new Map<Id, SObject>(), Enums.TriggerContext.AFTER_INSERT);
        }
        catch(Exception e)
        {
            //Assert
            System.assert(e instanceof InvalidParameterValueException, 'Executing a command with a null parameter should raise an exception for the developer.');
        }
    }

    @isTest
    static void ExcecuteCommandWithEmptySetOfParameters_ReturnsSuccessResultToCaller()
    {
        // Arrange
        List<SObject> items = new List<SObject>();
        TestBaseQueueableCommand command = new TestBaseQueueableCommand();

        // Act
        ActionResult result = command.execute(items, new Map<Id, SObject>(), Enums.TriggerContext.AFTER_INSERT);

        // Arrange
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Executing a command without any items to action against should return a successful result to the caller.'); 
    }

    @isTest
    static void ExecuteCommandAfterSuccessfulValidationAndExecution_ReturnsSuccessToCaller()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        TestBaseQueueableCommand command = new TestBaseQueueableCommand();
        Case item = TestDataCaseMock.createApplication(user);
        List<Case> items = new List<Case> { item };

        // Act
        ActionResult result = command.execute(items, new Map<Id, SObject>(), Enums.TriggerContext.AFTER_INSERT);

        //Assert
        system.assertEquals(ResultStatus.SUCCESS, result.Status, 'Executing a command with valid items, validating and executing successfully, should return a successful result to the caller.'); 
    }
}