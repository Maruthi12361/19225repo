public abstract class BaseKeySetQuery 
{
    protected abstract List<SObject> query(Map<String, String> keyset);
}