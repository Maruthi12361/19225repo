public class GroupGetQuery extends BaseIdQuery
{
    public override List<SObject> query(Set<Id> ids)
    {
        List<Group> items = 
            [
                SELECT
                    Name,
                    Type
                FROM Group
                WHERE Id IN : ids
            ];

        return items;
    }
}