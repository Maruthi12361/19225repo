/**
 * Auto Generated and Deployed by the Declarative Lookup Rollup Summaries Tool package (dlrs)
 **/
@IsTest
private class dlrs_Subject_ContentTest
{
    @testSetup
    static void setup()
    {
        TestUtilities.setupHEDA();
    }

    @IsTest
    private static void testTrigger()
    {
        // Force the dlrs_Subject_ContentTrigger to be invoked, fails the test if org config or other Apex code prevents this.
        dlrs.RollupService.testHandler(new Subject_Content__c());
    }
}