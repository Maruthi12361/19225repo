public class OpportunityScoreDemographicsBatch extends BaseBatchCommand
{
    private static final List<String> closedStages = Constants.OPPORTUNITY_ACQUISITION_CLOSEDSTAGES;
    private static final Id prospectRecordTypeId = RecordTypeUtils.getRecordTypeId(Constants.RECORDTYPE_CONTACT_PROSPECT, SObjectType.Contact);

    private List<SalesScoreConfig__c> salesScoreConfigs;

    public override Database.QueryLocator query(Database.BatchableContext context)
    {
        return Database.getQueryLocator(getIdQuery());
    }

    public override void command(Database.BatchableContext context, List<sObject> scope)
    {
        Map<Id, Double> contactDemographicScoreMap = new Map<Id, Double>();
        List<Opportunity> opps = getOpportunities(SObjectUtils.getIDs(scope, 'ID'));

        salesScoreConfigs = getSalesScoreConfigs();

        for (Opportunity opp : opps)
        {
            if (opp.contact__r == null)
            {
                Log.info(this.className, 'command', 'No contact associated with opportunity:' + opp.Id);
                continue;
            }

            Double totalDemographicScore = getTotalBehaviourScore(opp);

            if (opp.contact__r.DemographicScore__c != totalDemographicScore)
            {
                Double score;
                Double maximumScore = Double.valueOf(System.Label.LeadScoring_DemographicMaximum);

                if (totalDemographicScore > maximumScore)
                {
                    Log.info(this.className, 'command', 'Setting Demographic score to maximum score {0} as total demographic score is greater: {1}. ContactID: {2}', 
                        new List<String> { String.valueOf(maximumScore), String.valueOf(totalDemographicScore), opp.contact__r.Id });
                    if (opp.contact__r.DemographicScore__c != maximumScore)
                        score = maximumScore;
                }
                else
                {
                    Log.info(this.className, 'command', 'Setting Demographic score to total demographic score: {0}. ContactID: {1}', 
                        new List<String> { String.valueOf(totalDemographicScore), opp.contact__r.Id });
                    score = totalDemographicScore;
                }

                if (!contactDemographicScoreMap.containsKey(opp.Contact__r.Id) && score != null)
                    contactDemographicScoreMap.put(opp.Contact__r.Id, score);
            }
        }

        if (contactDemographicScoreMap.size() > 0)
            save(contactDemographicScoreMap);
    }

    private void save(Map<Id, Double> scoreMap)
    {
        List<Contact> contacts = [SELECT Id, DemographicScore__c FROM Contact WHERE Id in :scoreMap.keySet()];

        for (Contact contact : contacts)
            contact.DemographicScore__c = scoreMap.get(contact.Id);

        uow.registerDirty(contacts);
    }

    private Double getTotalBehaviourScore(Opportunity opp)
    {
        Double totalDemographicScore = 0;

        for (SalesScoreConfig__c config : salesScoreConfigs)
        {
            String fieldValue;
            if (config.ScoreType__c == 'Contact')
            {
                fieldValue = opp.contact__r.get(config.Source__c) == null ? '' : String.valueOf(opp.contact__r.get(config.Source__c));

                if (StringUtils.isLogicalStatementTrue(config.Operator__c, fieldValue, config.Value__c))
                    totalDemographicScore += config.ScoreValue__c;
            }
            else if (config.ScoreType__c == 'Opportunity')
            {
                fieldValue = opp.get(config.Source__c) == null ? '' : String.valueOf(opp.get(config.Source__c));

                if (StringUtils.isLogicalStatementTrue(config.Operator__c, fieldValue,  config.Value__c))
                {
                    totalDemographicScore += config.ScoreValue__c;
                    break;
                }
            }
        }

        return totalDemographicScore;
    }

    private List<Opportunity> getOpportunities(Set<Id> oppIds)
    {
        return Database.query(getQuery(oppIds));
    }

    private List<SalesScoreConfig__c> getSalesScoreConfigs()
    {
        Set<String> demographicScoreTypes = new Set<String>{ 'Contact', 'Opportunity' };

        return
            [
                SELECT Id, Active__c, Operator__c, ScoreType__c, ScoreValue__c, Source__c, Value__c, DisplayOrder__c
                  FROM SalesScoreConfig__c
                 WHERE Active__c = true AND ScoreType__c in :demographicScoreTypes
                 ORDER BY DisplayOrder__c
            ];
    }

    private String getIdQuery()
    {
        return
            'SELECT Id, StageName, LastModifiedDate ' +
            '  FROM Opportunity ' +
            ' WHERE StageName NOT IN :closedStages AND LastModifiedDate = LAST_N_DAYS:30';
    }

    private String getQuery(Set<Id> oppIds)
    {
        return
            'SELECT ' +
                'Id, ApplicationDate__c, ApplicationCutoffDate__c, CLOODate__c, ConcurrentSubjects__c, Consent_Full_Name__c, ' +
                'ContactFirstName__c, Contact__c, Course_Info_Read__c, CreditTransferRequested__c, ' +
                'Custom_Timetable_Required__c, Description, English_Proficiency_Details__c, English_Proficiency_Test__c, ' +
                'English_Proficiency__c, Expected_Admin_Date_Calc__c, FEEHELP_Previous_Firstname__c, FEEHELP_Previous_Lastname__c, FEEHELP_Previous_Names__c, ' +
                'FEE_HELP_Forms_Submitted__c, Financial_Support_Source__c, Has_Special_Needs__c, Invoice_Recipient_Address__c, Invoice_Recipient_Fax__c, ' +
                'Invoice_Recipient_Mobile__c, Invoice_Recipient_Name__c, Invoice_Recipient_Telephone__c, IsExpatDeclared__c, LastModifiedDate, ' +
                'Last_Year_of_Participation__c, Linked_Case_Status__c, Linked_Case_URL__c, Lost_Description__c, Lost_Reason__c, Make_RPL_claim__c, ' +
                'Management_Responsibilities__c, ManualFollowUpDate__c, NVM_Owner_ID__c, Name, NumberOfEnroledSubjects__c, OAF_Agreed_Course_Information_Version__c, ' +
                'OAF_Agreed_Terms_And_Condition_Version__c, OAF_Application_Type__c, OAF_Last_Submitted__c, OAF_Submit_Count__c, OAF_Submitted__c, ' +
                'Offer_Documentation_Sent__c, Own_Business_Years__c, OwnerLinkedInURL__c, Owner_for_Commission__c, Parent_1_Highest_Educational_Attainment__c, ' +
                'Parent_2_Highest_Educational_Attainment__c, Parent_Education_Options__c, Payment_Method__c, Primary_Location_During_Studies__c, ' +
                'ProcessBuilderLogOpp__c, RecordTypeId, Remittance_Received__c, Signature__c, Special_Needs_Advice_Required__c, StageName, ' +
                'StandardTimetableAgreement__c, Studying_in_Australia__c, TermMoveCount__c, Terms_And_Condition_Agreed__c, Verbal_Checklist_Complete__c, ' +
                'Vetting_Complete__c, Welcome_Letter_Sent__c, With_Prospect_and_CA__c, ' +
                'Contact__r.Id, Contact__r.Name, Contact__r.Salutation, Contact__r.FirstName, Contact__r.MiddleName, Contact__r.LastName, Contact__r.Email, Contact__r.Phone, ' +
                'Contact__r.MobilePhone, Contact__r.HomePhone, Contact__r.Birthdate, Contact__r.hed__Gender__c, Contact__r.WorkExperienceYears__c, Contact__r.ManagementExperienceYears__c, ' +
                'Contact__r.Highest_Qualification__c, Contact__r.PreferredTimeframe__c, Contact__r.ApplicationCourse__c, Contact__r.Preferred_Start_Date__c, ' +
                'Contact__r.OtherStreet, Contact__r.OtherCity, Contact__r.OtherPostalCode, Contact__r.OtherState, Contact__r.OtherCountry, Contact__r.OtherAddressType__c, ' +
                'Contact__r.MailingStreet, Contact__r.MailingCity, Contact__r.MailingPostalCode, Contact__r.MailingState, Contact__r.MailingCountry, Contact__r.PermanentAddressType__c, ' +
                'Contact__r.Permanent_Home_Address__c, Contact__r.Permanent_Home_Suburb_Town__c, Contact__r.Permanent_Home_Postcode__c, Contact__r.Permanent_Home_State_Province_Region__c, ' +
                'Contact__r.Permanent_Home_Country__c, Contact__r.hed__Dual_Citizenship__c, Contact__r.hed__Country_of_Origin__c, Contact__r.Passport_Number__c, Contact__r.Visa_Type__c, ' +
                'Contact__r.Visa_Expiry_Date__c, Contact__r.Impairments__c, Contact__r.Special_Needs_Details__c, Contact__r.High_School_Level__c, Contact__r.YearOfLeavingHighSchool__c, ' +
                'Contact__r.High_School_State_Territory__c, Contact__r.EnglishLevel__c, Contact__r.HighestQualificationName__c, Contact__r.HighestQualificationInstitution__c, ' +
                'Contact__r.HighestQualificationLanguage__c, Contact__r.RefereeOneFullName__c, Contact__r.RefereeOneJobTitle__c, Contact__r.RefereeOneEmployer__c, ' +
                'Contact__r.RefereeOneWorkEmail__c, Contact__r.RefereeOneMobile__c, Contact__r.RefereeTwoFullName__c, Contact__r.RefereeTwoJobTitle__c, ' +
                'Contact__r.RefereeTwoEmployer__c, Contact__r.RefereeTwoWorkEmail__c, Contact__r.RefereeTwoMobile__c, Contact__r.Aboriginal_or_Torres_Strait_Islander_Des__c, ' +
                'Contact__r.Indigenous_Support_Services__c, Contact__r.Language_Spoken_at_Home__c, Contact__r.hed__UniversityEmail__c, ' +
                'Contact__r.hed__WorkEmail__c, Contact__r.hed__AlternateEmail__c, Contact__r.PersonalEmail__c, Contact__r.PrimaryMotivations__c, ' +
                'Contact__r.HighestQualificationCompletionYear__c, Contact__r.CHESSN_Number__c, Contact__r.hed__Citizenship__c, Contact__r.HighestTertiaryLevelDegree__c, ' +
                'Contact__r.Status__c, Contact__r.AustralianVisaHolder__c, Contact__r.EmailVerified__c, Contact__r.MobilePhoneCountry__c, Contact__r.PhoneCountry__c, ' +
                'Contact__r.RefereeOneMobileCountry__c, Contact__r.RefereeTwoMobileCountry__c, Contact__r.LastModifiedDate, Contact__r.DemographicScore__c, ' +
                'Contact__r.ActualCreateDate__c, Contact__r.Alumni__c, Contact__r.ANZPermanentResidency__c, Contact__r.AtRisk__c, Contact__r.NVMAutoCLIDContact__c, ' +
                'Contact__r.Backup_Mobile_Phone__c, Contact__r.Backup_Phone__c, Contact__r.BBAComplete__c, Contact__r.BehaviourDecaySum__c, Contact__r.BehaviourScore__c, ' +
                'Contact__r.BehaviourScoreRollup__c, Contact__r.Campaign_Contact_ID__c, Contact__r.CASLImpliedConsentExpiry__c, Contact__r.Cerebro_Student_ID__c, ' +
                'Contact__r.Cerebro_Student_ID_Link__c, Contact__r.CerebroSyncMessage__c, Contact__r.Course_Start_Date__c, Contact__r.Current_Course_Status__c, ' +
                'Contact__r.Current_Employer__c, Contact__r.Current_Position__c, Contact__r.CurrentSalary__c, Contact__r.CurrentStudent__c, ' +
                'Contact__r.Days_Left_Until_Implied_Consent_Expires__c, Contact__r.Days_Until_Implied_SMS_Consent_Expires__c, Contact__r.DisqualifiedReason__c, ' +
                'Contact__r.Email_Express_Consent_Date__c, Contact__r.Email_Express_Consent_Exists__c, Contact__r.Emergency_Contact_Full_Name__c, ' +
                'Contact__r.Emergency_Contact_Mobile__c, Contact__r.Emergency_Contact_Relationship__c, Contact__r.Emergency_Contact_Telephone__c, ' +
                'Contact__r.Emergency_Contact_Title__c, Contact__r.Enabled_All_Apps_Access__c, Contact__r.Enabled_Signup_Access__c, Contact__r.EncryptedId__c, ' +
                'Contact__r.Exam_Time__c, Contact__r.Express_Consent_Exists__c, Contact__r.Express_Consent_Expires__c, Contact__r.Express_Consent_Given__c, ' +
                'Contact__r.FeeHelp_Approved__c, Contact__r.FeeHelp_Eligibility__c, Contact__r.FeeSet__c, Contact__r.FeesInCreditBalance__c, ' +
                'Contact__r.GCMComplete__c, Contact__r.Implied_Consent_Expiry_Date__c, Contact__r.Incomplete_Subjects__c, Contact__r.InferredCity__c, ' +
                'Contact__r.InferredCompany__c, Contact__r.InferredCountry__c, Contact__r.InferredMetropolitanArea__c, Contact__r.InferredPhoneAreaCode__c, ' +
                'Contact__r.InferredPostalCode__c, Contact__r.InferredStateRegion__c, Contact__r.IsDecayLessThenScore__c, Contact__r.IsEnroledCurrentTerm__c, ' +
                'Contact__r.IsEnroledFutureTerm__c, Contact__r.IsEnroledNextTerm__c, Contact__r.IsMBAEligible__c, Contact__r.Is_My_Contact__c, ' +
                'Contact__r.Is_My_Team_Contact__c, Contact__r.IsWarmCampaignMember__c, Contact__r.LastCLOODate__c, Contact__r.LastCallDateTime__c, ' +
                'Contact__r.LastCallOutcome__c, Contact__r.LastCourseCompletionDate__c, Contact__r.Last_Used_Funding_Source__c, Contact__r.LeadId__c, ' +
                'Contact__r.Legacy_CRM_ID__c, Contact__r.LinkedIn_Profile__c, Contact__r.Lookup_Mobile__c, Contact__r.MailingAddressFull__c, ' +
                'Contact__r.Mailing_Postcode_Not_Applicable__c, Contact__r.MBAComplete__c, Contact__r.MBA_Course_Name__c, Contact__r.MC_Subjects__c, ' +
                'Contact__r.Next_Step__c, Contact__r.Next_Followup_Date__c, Contact__r.NVMContactOwnerID__c, ' +
                'Contact__r.NVM_Owner_Email__c, Contact__r.OAF_Application_Id__c, Contact__r.Objection__c, Contact__r.OK_To_Call__c, ' +
                'Contact__r.OriginalReferrerSource__c, Contact__r.OtherAddressFull__c, Contact__r.Other_Postcode_Not_Applicable__c, ' +
                'Contact__r.OwnerLinkedInUrl__c, Contact__r.Passed_Subjects__c, Contact__r.PermanentAddressFull__c, Contact__r.Perm_Postcode_Not_Applicable__c, ' +
                'Contact__r.PreferredStartDateTime__c, Contact__r.Primary_SRO_Student_Profile_Notes__c, Contact__r.ProgramPortfolio__c, Contact__r.Recycled__c, ' +
                'Contact__r.PreferredTimeframeLastModified__c, Contact__r.Required_Subjects__c, Contact__r.Rollover_Call_Count__c, Contact__r.Rollover_Colour__c, ' +
                'Contact__r.Rollover_Contact_History__c, Contact__r.Rollover_Count__c, Contact__r.Rollover_Deferred_Reason__c, ' +
                'Contact__r.Rollover_Deferred_Reason_Other__c, Contact__r.RolloverEligible__c, Contact__r.Rollover_Email_Count__c, ' +
                'Contact__r.Rollover_Followup_Date__c, Contact__r.Rollover_Lost_Count__c, Contact__r.RolloverSegmentation__c, Contact__r.Rollover_Status__c, ' +
                'Contact__r.SMS_Express_Consent_Date__c, Contact__r.SMS_Express_Consent_Exists__c, Contact__r.SMS_Implied_Consent_Expiry_Date__c, Contact__r.SourceDetail__c, ' +
                'Contact__r.Manual_Followup_Date__c, Contact__r.SSO_ID__c, Contact__r.StandardTimetableAgreement__c, Contact__r.Student_Type__c, ' +
                'Contact__r.DoNotCall, Contact__r.LeadSource, Contact__r.HasOptedOutOfEmail, Contact__r.RecordTypeId ' +
            '  FROM Opportunity ' +
            ' WHERE Id IN :oppIds AND Contact__r.RecordTypeId = :prospectRecordTypeId';
    }
}