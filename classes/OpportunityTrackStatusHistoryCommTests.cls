@isTest
public class OpportunityTrackStatusHistoryCommTests
{
    @testSetup
    static void setup()
    {
        TestUtilities.setupHEDA();
    }

    @isTest
    static void TrackStatusOnNewOpportunity_CreatesNewStatusTrackingHistoryRecord()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createProspectHot(user);
        Opportunity opportunity = TestDataOpportunityMock.createApplyingAcquisition(user, contact);

        // Act
        Test.startTest();
        ActionResult result = OpportunityService.trackStatusHistory(new List<Opportunity> { opportunity }, null, Enums.TriggerContext.AFTER_INSERT);
        List<Status_Tracking_History__c> histories =
        [
            SELECT Id, Contact_ID__c, Status_Stage__c, StartDateTime__c, EndDateTime__c, Type__c, Department__c, Direction__c, Next_Status_Stage__c, Next_User__c
            FROM Status_Tracking_History__c
            WHERE Opportunity_ID__c = :opportunity.Id
        ];
        Test.stopTest();

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.Status);
        System.assertEquals(1, histories.size());
        System.assertEquals('Applying', histories[0].Status_Stage__c);
        System.assertEquals('State Change', histories[0].Type__c);
        System.assertEquals('Sales', histories[0].Department__c);
        System.assertEquals(null, histories[0].EndDateTime__c);
    }

    @isTest
    static void TrackForwardStatusOnExistingOpportunity_CreatesNewAndUpdatesExistingStatusTrackingHistoryRecord()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createProspectHot(user);
        Opportunity opportunity = TestDataOpportunityMock.createApplyingAcquisition(user, contact);
        ActionResult result = OpportunityService.trackStatusHistory(new List<Opportunity> { opportunity }, null, Enums.TriggerContext.AFTER_INSERT);
        Map<Id, Opportunity> oldMap = new Map<Id, Opportunity>();
        oldMap.put(opportunity.Id, opportunity);
        Opportunity newOpportunity = opportunity.clone(true, false, true, true);
        newOpportunity.StageName = 'Vetting';

        // Act
        Test.startTest();
        result = OpportunityService.trackStatusHistory(new List<Opportunity> { newOpportunity }, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        List<Status_Tracking_History__c> histories =
        [
            SELECT Id, Contact_ID__c, Status_Stage__c, StartDateTime__c, EndDateTime__c, Type__c, Department__c, Direction__c, Next_Status_Stage__c, Next_User__c
            FROM Status_Tracking_History__c
            WHERE Opportunity_ID__c = :opportunity.Id
        ];
        Test.stopTest();

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.Status);
        System.assertEquals(2, histories.size());
        System.assertEquals('Applying', histories[0].Status_Stage__c);
        System.assertEquals('State Change', histories[0].Type__c);
        System.assertEquals('Sales', histories[0].Department__c);
        System.assertNotEquals(null, histories[0].EndDateTime__c);
        System.assertEquals('Forward', histories[0].Direction__c);
        System.assertEquals('Vetting', histories[0].Next_Status_Stage__c);
        System.assertEquals(UserInfo.getName(), histories[0].Next_User__c);

        System.assertEquals('Vetting', histories[1].Status_Stage__c);
        System.assertEquals('State Change', histories[1].Type__c);
        System.assertEquals('Sales', histories[1].Department__c);
        System.assertEquals(null, histories[1].EndDateTime__c);
    }

    @isTest
    static void TrackBackwardStatusOnExistingOpportunity_CreatesNewAndUpdatesExistingStatusTrackingHistoryRecord()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createProspectHot(user);
        Opportunity opportunity = TestDataOpportunityMock.create(user, contact, 'Vetting', true);
        ActionResult result = OpportunityService.trackStatusHistory(new List<Opportunity> { opportunity }, null, Enums.TriggerContext.AFTER_INSERT);
        Map<Id, Opportunity> oldMap = new Map<Id, Opportunity>();
        oldMap.put(opportunity.Id, opportunity);
        Opportunity newOpportunity = opportunity.clone(true, false, true, true);
        newOpportunity.StageName = 'Applying';

        // Act
        Test.startTest();
        result = OpportunityService.trackStatusHistory(new List<Opportunity> { newOpportunity }, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        List<Status_Tracking_History__c> histories =
        [
            SELECT Id, Contact_ID__c, Status_Stage__c, StartDateTime__c, EndDateTime__c, Type__c, Department__c, Direction__c, Next_Status_Stage__c, Next_User__c
            FROM Status_Tracking_History__c
            WHERE Opportunity_ID__c = :opportunity.Id
        ];
        Test.stopTest();

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.Status);
        System.assertEquals(2, histories.size());
        System.assertEquals('Vetting', histories[0].Status_Stage__c);
        System.assertEquals('State Change', histories[0].Type__c);
        System.assertEquals('Sales', histories[0].Department__c);
        System.assertNotEquals(null, histories[0].EndDateTime__c);
        System.assertEquals('Backward', histories[0].Direction__c);
        System.assertEquals('Applying', histories[0].Next_Status_Stage__c);
        System.assertEquals(UserInfo.getName(), histories[0].Next_User__c);

        System.assertEquals('Applying', histories[1].Status_Stage__c);
        System.assertEquals('State Change', histories[1].Type__c);
        System.assertEquals('Sales', histories[1].Department__c);
        System.assertEquals(null, histories[1].EndDateTime__c);
    }
    
    @isTest
    static void TrackStatusOnNewOpportunityWithChangingCampaign_CreatesNewStatusTrackingHistoryRecordWithCampaign()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createProspectHot(user);
        Opportunity opportunity = TestDataOpportunityMock.createApplyingAcquisition(user, contact);
        Campaign campaign = TestDataCampaignMock.create(user, 'Domestic', 'MBA', false);

        // Act
        Test.startTest();
        ActionResult result = OpportunityService.trackStatusHistory(new List<Opportunity> { opportunity }, null, Enums.TriggerContext.AFTER_INSERT);
        Map<Id, Opportunity> oldMap = new Map<Id, Opportunity>();
        oldMap.put(opportunity.Id, opportunity);
        Opportunity newOpportunity = opportunity.clone(true, false, true, true);
        newOpportunity.CampaignId = campaign.Id;
        result = OpportunityService.trackStatusHistory(new List<Opportunity> { newOpportunity }, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        List<Status_Tracking_History__c> histories =
        [
            SELECT Id, Contact_ID__c, Status_Stage__c, StartDateTime__c, EndDateTime__c, Type__c, Department__c, Direction__c, Next_Status_Stage__c, Next_User__c, Campaign__c
            FROM Status_Tracking_History__c
            WHERE Opportunity_ID__c = :opportunity.Id
        ];
        Test.stopTest();

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.Status);
        System.assertEquals(1, histories.size());
        System.assertEquals('Applying', histories[0].Status_Stage__c);
        System.assertEquals('State Change', histories[0].Type__c);
        System.assertEquals('Sales', histories[0].Department__c);
        System.assertEquals(null, histories[0].EndDateTime__c);
        System.assertEquals(campaign.Id, histories[0].Campaign__c);
    }
}