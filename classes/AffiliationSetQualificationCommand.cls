public class AffiliationSetQualificationCommand extends BaseTriggerCommand
{
    public override List<SObject> validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        List<hed__Affiliation__c> highestQualifications = new List<hed__Affiliation__c>();
        Id recordTypeId = RecordTypeUtils.getRecordTypeId(Constants.RECORDTYPE_AFFILIATION_QUALIFICATION, SObjectType.hed__Affiliation__c);

        for (hed__Affiliation__c affiliation : (List<hed__Affiliation__c>) items)
        {
            if (affiliation.RecordTypeId == recordTypeId && affiliation.IsHighestQualification__c)
                highestQualifications.add(affiliation);
        }

        return highestQualifications;
    }
    
    public override ActionResult command(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        List<hed__Affiliation__c> qualifications = getQualifications(SObjectUtils.getIDs(items, 'hed__Contact__c'));

        for (hed__Affiliation__c affiliation : (List<hed__Affiliation__c>) items)
        {
            List<hed__Affiliation__c> contactQualifications = getContactQualifications(affiliation.hed__Contact__c, qualifications, affiliation.Type__c);

            for (hed__Affiliation__c qualification : contactQualifications)
            {
                if (qualification.Id != affiliation.Id && qualification.IsHighestQualification__c)
                {
                    qualification.IsHighestQualification__c = false;
                    uow.registerDirty(qualification);
                }
            }
        }

        return new ActionResult(ResultStatus.SUCCESS);
    }

    private List<hed__Affiliation__c> getQualifications(Set<ID> contactIds)
    {
        return
        [
            SELECT Id, hed__Contact__c, IsHighestQualification__c, Type__c
            FROM hed__Affiliation__c
            WHERE hed__Contact__c IN :contactIds AND RecordType.DeveloperName = :Constants.RECORDTYPE_AFFILIATION_QUALIFICATION
        ];
    }

    private List<hed__Affiliation__c> getContactQualifications(Id contactId, List<hed__Affiliation__c> qualifications, String type)
    {
        List<hed__Affiliation__c> contactQualifications = new List<hed__Affiliation__c>();

        for (hed__Affiliation__c affiliation : qualifications)
            if (affiliation.hed__Contact__c == contactId && affiliation.Type__c == type)
                contactQualifications.add(affiliation);

        return contactQualifications;
    }
}