public class InterestingMomentTriggerHandler extends BaseTriggerHandler
{
    public override void afterInsert()
    {
        ActionResult result = InterestingMomentService.sendCAAlert(Trigger.new, Trigger.oldMap, Enums.TriggerContext.AFTER_INSERT);
        
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);
        
        result = InterestingMomentService.updateOpportunityStatus(Trigger.new, Trigger.oldMap, Enums.TriggerContext.AFTER_INSERT);
        
        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);
    }
    
    public override void afterUpdate()
    {        
    }
}