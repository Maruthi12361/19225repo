@IsTest
public class LogTest
{
    @testSetup
    static void setup()
    {
        TestUtilities.setupHEDA();
    }

    @isTest
    static void LogInvalidClassName_FailsToValidate()
    {
        try
        {
            Log.info(null, 'LogTest_Info', 'Log info message - basic');
        }
        catch (Exception e)
        {
            System.assertEquals(String.format(Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Class Name' }), e.getMessage());
        }
    }

    @isTest
    static void LogInvalidMethodName_FailsToValidate()
    {
        try
        {
            Log.info('ClassName', null, 'Log info message - basic');
        }
        catch (Exception e)
        {
            System.assertEquals(String.format(Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Function Name' }), e.getMessage());
        }
    }

    @isTest
    static void LogTest_Info()
    {
        // Arrange
        String[] items = new String[]{'One', 'Two', 'Three'};
        Log.info('LogTest', 'LogTest_Info', 'Log info message - basic');
        Log.info('LogTest', 'LogTest_Info', 'Log info message with items - {0}, {1}, {2}', items);

        // Act
        Log.save();
        List<AppLog__c> savedLogs = [SELECT Id, Type__c, Message__c FROM AppLog__c];

        // Assert
        System.assertEquals(0, savedLogs.size(), 'No log records should be save to db');
    }

    @isTest
    static void LogTest_Error()
    {
        // Arrange
        String msg = 'Unique log error message';
        Log.error('LogTest', 'LogTest_Error', msg);

        // Act
        Log.save();
        AppLog__c savedLog = [SELECT Id, Type__c, Message__c FROM AppLog__c Limit 1];

        // Assert
        System.assertNotEquals(null, savedLog, 'Log record not saved to db');
        System.assertEquals('ERROR', savedLog.Type__c, 'Invalid log type');
        System.assert(savedLog.Message__c.contains(msg), 'Invalid log message');
    }

    @isTest
    static void LogTest_ErrorAndWarnWithItems()
    {
        // Arrange
        String className = 'LogTest';
        String methodName = 'LogTest_ErrorWithItems';
        String[] items = new String[]{'One', 'Two', 'Three'};
        String msgWarn = 'Log warn message - {0}, {1}, {2}';
        String msgError = 'Log error message - {0}, {1}, {2}';

        Log.warn(className, methodName, msgWarn, items);
        Log.error(className, methodName, msgError, items);

        // Act
        Log.save();

        // Assert
        List<AppLog__c> savedLogs = [SELECT Id, Type__c, Message__c FROM AppLog__c];
        System.assert(savedLogs.size() > 1, 'At least two log record should be saved to db');

        AppLog__c warnLog = getLog(savedLogs, string.format(msgWarn, items));
        System.assertNotEquals(null, warnLog, 'Log warn message not saved to db');
        System.assertEquals('WARN', warnLog.Type__c, 'Invalid log warn message type');

        AppLog__c errorLog = getLog(savedLogs, string.format(msgError, items));
        System.assertNotEquals(null, errorLog, 'Log error message not saved to db');
        System.assertEquals('ERROR', errorLog.Type__c, 'Invalid log error message type');
    }

    @isTest
    static void LogTest_ErrorWithExceptions()
    {
        // Arrange
        String className = 'LogTest';
        String methodName = 'LogTest_ErrorWithException';
        String msgError = 'Test log error message';
        Exceptions.InvalidParameterException nullEx;
        Exceptions.InvalidParameterException ex = new Exceptions.InvalidParameterException('Test exception msg');

        Log.error(className, methodName, msgError, ex);
        Log.error(className, methodName, msgError, nullEx);

        // Act
        Log.save();

        // Assert
        List<AppLog__c> savedLogs = [SELECT Id, Type__c, Message__c FROM AppLog__c];
        System.assert(savedLogs.size() > 1, 'At least two log records should be saved to db');

        AppLog__c errorLog = getLog(savedLogs, msgError);
        System.assertNotEquals(null, errorLog, 'Log error message not saved to db');
        System.assertEquals('ERROR', errorLog.Type__c, 'Invalid log error message type');
        System.assert(errorLog.Message__c.contains('exception msg'), 'Exception message not added');
    }

    @isTest
    static void LogTest_InfoAndError()
    {
        // Arrange
        String className = 'LogTest';
        String methodName = 'LogTest_InfoAndError';
        String[] items = new String[]{'One', 'Two', 'Three'};
        String msgInfoOne = 'Log info first message';
        String msgInfoTwo = 'Log info second message with items - {0}, {1}, {2}';
        String msgError = 'Log error message';

        Log.info(className, methodName, msgInfoOne);
        Log.info(className, methodName, msgInfoTwo, items);
        Log.error(className, methodName, msgError);

        // Act
        Log.save();

        // Assert
        List<AppLog__c> savedLogs = [SELECT Id, Type__c, Message__c FROM AppLog__c];
        System.assert(savedLogs.size() > 2, 'At least three log records should be saved to db');

        AppLog__c firstInfoLog = getLog(savedLogs, msgInfoOne);
        System.assertNotEquals(null, firstInfoLog, 'Log info message not saved to db');
        System.assertEquals('INFO', firstInfoLog.Type__c, 'Invalid log info message type');

        AppLog__c secondInfoLog = getLog(savedLogs, string.format(msgInfoTwo, items));
        System.assertNotEquals(null, secondInfoLog, 'Log info message not saved to db');
        System.assertEquals('INFO', secondInfoLog.Type__c, 'Invalid log info message type');

        AppLog__c errorLog = getLog(savedLogs, msgError);
        System.assertNotEquals(null, errorLog, 'Log error message not saved to db');
        System.assertEquals('ERROR', errorLog.Type__c, 'Invalid log error message type');
    }

    @isTest
    static void LogTest_InfoWithHttpRequestAndError()
    {
        // Arrange
        String className = 'LogTest';
        String methodName = 'LogTest_InfoWithHttpRequestAndError';
        String msg = 'Unique log info message with http request';
        String msgError = 'Log error message';

        // TODO: Move http requset to test data mock file
        HttpRequest req = new HttpRequest();
        req.setEndpoint('https://fakeurl.com');
        req.setMethod('POST');
        req.setBody('Request body from log test info with http request');
        List<String> headers = new List<String>{'Content-Type: application/x-www-form-urlencoded','Accept: application/json'};

        Log.info(className, methodName, msg, req, headers);
        Log.error(className, methodName, msgError);

        // Act
        Log.save();

        // Assert
        List<AppLog__c> savedLogs = [SELECT Id, Type__c, Message__c FROM AppLog__c];
        System.assert(savedLogs.size() > 1, 'At least two log records should be saved to db');

        AppLog__c infoLog = getLog(savedLogs, msg);
        System.assertNotEquals(null, infoLog, 'Log info message not saved to db');
        System.assertEquals('INFO', infoLog.Type__c, 'Invalid log info message type');

        AppLog__c errorLog = getLog(savedLogs, msgError);
        System.assertNotEquals(null, errorLog, 'Log error message not saved to db');
        System.assertEquals('ERROR', errorLog.Type__c, 'Invalid log error message type');
    }

    @isTest
    static void LogTest_WarnAndError()
    {
        // Arrange
        String className = 'LogTest';
        String methodName = 'LogTest_WarnAndError';
        String msgWarn = 'Log warn message';
        String msgError = 'Log error message';

        Log.warn(className, methodName, msgWarn);
        Log.error(className, methodName, msgError);

        // Act
        Log.save();

        // Assert
        List<AppLog__c> savedLogs = [SELECT Id, Type__c, Message__c FROM AppLog__c];
        System.assert(savedLogs.size() > 1, 'At least two log records should be saved to db');

        AppLog__c firstWarnLog = getLog(savedLogs, msgWarn);
        System.assertNotEquals(null, firstWarnLog, 'Log warn message not saved to db');
        System.assertEquals('WARN', firstWarnLog.Type__c, 'Invalid log warn message type');

        AppLog__c errorLog = getLog(savedLogs, msgError);
        System.assertNotEquals(null, errorLog, 'Log error message not saved to db');
        System.assertEquals('ERROR', errorLog.Type__c, 'Invalid log error message type');
    }

    @isTest
    static void LogTest_Deletion()
    {
        // Arrange
        Integer testSize = 10;
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        List<AppLog__c> logs = TestDataAppLogMock.createAppLogs(user, LoggingLevel.INFO, testSize);

        // Act
        ActionResult result = Log.deleteLogs(logs);

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.status, 'Deletion of app logs failed');
    }

    private static AppLog__c getLog(List<AppLog__c> logs, String msg)
    {
        for (AppLog__c log : logs)
        {
            if (log.Message__c.contains(msg))
                return log;
        }
        return null;
    }
}