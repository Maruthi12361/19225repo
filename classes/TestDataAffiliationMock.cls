@isTest
public class TestDataAffiliationMock
{
    public static hed__Affiliation__c createAppointment(User user, Account educationalAccount, Contact facultyContact, String role, String status)
    {
        hed__Affiliation__c appointment = new hed__Affiliation__c
        (
            hed__Account__c = educationalAccount.Id,
            hed__Contact__c = facultyContact.Id,
            hed__Role__c = role,
            hed__Status__c = status,
            hed__StartDate__c = Date.Today(),
            Discipline__c = '080000 Management and Commerce',
            RoleDescription__c = role + ' Description',
            ContractDEEDFinalised__c = true,
            EBookAccessProvided__c = true,
            ITAccessProvided__c = true,
            OnlineInductionCompleted__c = true,
            WILCopiesForFinanceProvided__c = true,
            WorkInstructionLetterWILIssued__c = true,
            RecordTypeId = RecordTypeUtils.getRecordTypeId(Constants.RECORDTYPE_AFFILIATION_APPOINTMENT, SObjectType.hed__Affiliation__c)
        );

        System.RunAs(user)
        {
            insert appointment;
        }

        return appointment;
    }

    public static hed__Affiliation__c createQualification(User user, Account educationalAccount, Contact facultyContact, String type, String status, Boolean isHighestQualification)
    {
        hed__Affiliation__c qualification = new hed__Affiliation__c
        (
            hed__Account__c = educationalAccount.Id,
            hed__Contact__c = facultyContact.Id,
            Type__c = type,
            hed__Status__c = status,
            IsHighestQualification__c = isHighestQualification,
            RoleDescription__c = type + ' Description',
            YearOfConferral__c = '2010',
            RecordTypeId = RecordTypeUtils.getRecordTypeId(Constants.RECORDTYPE_AFFILIATION_QUALIFICATION, SObjectType.hed__Affiliation__c)
        );

        System.RunAs(user)
        {
            insert qualification;
        }

        return qualification;
    }
}