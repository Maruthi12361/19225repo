public abstract class BaseSingleIdQuery 
{
    public abstract SObject query(Id ids);
}