public class InterestingMomentService
{
    public static ActionResult sendCAAlert(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        return new InterestingMomentAlertCACommand().execute(items, oldMap, triggerContext); 
    }
    
    public static ActionResult updateOpportunityStatus(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        return new InterestingMomentUpdateStatusCommand().execute(items, oldMap, triggerContext); 
    }
}