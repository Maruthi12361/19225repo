public class PlanRequirementUpdateParentCommand extends BaseTriggerCommand
{
    static Id recordTypeId = RecordTypeUtils.getRecordTypeId(Constants.RECORDTYPE_PLANREQUIREMENT_DEPENDENCYITEM, SObjectType.hed__Plan_Requirement__c);

    public override List<SObject> validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        List<hed__Plan_Requirement__c> result = new List<hed__Plan_Requirement__c>();

        for (hed__Plan_Requirement__c requirement : (List<hed__Plan_Requirement__c>)items)
        {
            if (requirement.RecordTypeId == recordTypeId)
            {
                if (triggerContext == Enums.TriggerContext.AFTER_INSERT || triggerContext == Enums.TriggerContext.AFTER_DELETE)
                {
                    if (requirement.hed__Plan_Requirement__c != null)
                        result.add(requirement);
                }
                else if (triggerContext == Enums.TriggerContext.AFTER_UPDATE)
                {
                    hed__Plan_Requirement__c oldValue = (hed__Plan_Requirement__c) oldMap.get(requirement.Id);

                    if (requirement.hed__Plan_Requirement__c != oldValue.hed__Plan_Requirement__c)
                        result.add(requirement);
                }
            }
        }

        return result;
    }

    public override ActionResult command(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        Set<Id> idSet = new Set<Id>();

        for (hed__Plan_Requirement__c requirement : (List<hed__Plan_Requirement__c>)items)
        {
            if (triggerContext == Enums.TriggerContext.AFTER_UPDATE)
            {
                hed__Plan_Requirement__c oldValue = (hed__Plan_Requirement__c) oldMap.get(requirement.Id);

                if (requirement.hed__Plan_Requirement__c != null)
                    idSet.add(requirement.hed__Plan_Requirement__c);

                if (oldValue.hed__Plan_Requirement__c != null)
                    idSet.add(oldValue.hed__Plan_Requirement__c);
            }
            else
                idSet.add(requirement.hed__Plan_Requirement__c);
        }

        List<AggregateResult> parentRequirements =
            [
                SELECT hed__Plan_Requirement__c, COUNT(Id)
                FROM hed__Plan_Requirement__c
                WHERE hed__Plan_Requirement__c IN : idSet
                GROUP BY hed__Plan_Requirement__c
            ];

        for (AggregateResult result : parentRequirements)
        {
            Id parentId = (Id)result.get('hed__Plan_Requirement__c');
            hed__Plan_Requirement__c requirement = new hed__Plan_Requirement__c(Id = parentId);
            requirement.NumberOfSubjects__c = Integer.valueOf(result.get('expr0'));
            idSet.remove(parentId);
            uow.registerDirty(requirement);
        }

        for (Id key : idSet)
        {
            hed__Plan_Requirement__c requirement = new hed__Plan_Requirement__c(Id = key);
            requirement.NumberOfSubjects__c = 0;
            uow.registerDirty(requirement);
        }

        return new ActionResult();
    }
}