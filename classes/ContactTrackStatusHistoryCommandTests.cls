@isTest
public class ContactTrackStatusHistoryCommandTests
{
    @testSetup
    static void setup()
    {
        TestUtilities.setupHEDA();
    }

    @isTest
    static void TrackCallStageAndStatusOnNewContact_CreatesNewStatusTrackingHistoryRecord()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createProspectWarm(user);

        // Act
        Test.startTest();
        ActionResult result = ContactService.trackStatusHistory(new List<Contact> { contact }, null, Enums.TriggerContext.AFTER_INSERT);
        List<Status_Tracking_History__c> histories =
        [
            SELECT Id, Contact_ID__c, Status_Stage__c, StartDateTime__c, EndDateTime__c, Call_Stage__c, Call_Outcome__c, Department__c, Type__c, Direction__c, Next_Status_Stage__c, Next_User__c
            FROM Status_Tracking_History__c
            WHERE Contact_ID__c = : contact.Id
        ];
        Test.stopTest();

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.Status);
        System.assertEquals(2, histories.size());

        System.assertEquals('Cold', histories[0].Status_Stage__c);
        System.assertEquals('State Change', histories[0].Type__c);
        System.assertEquals('Sales', histories[0].Department__c);
        System.assertNotEquals(null, histories[0].EndDateTime__c);
        System.assertEquals('Forward', histories[0].Direction__c);
        System.assertEquals('Warm', histories[0].Next_Status_Stage__c);
        System.assertEquals(UserInfo.getName(), histories[0].Next_User__c);

        System.assertEquals('Warm', histories[1].Status_Stage__c);
        System.assertEquals('State Change', histories[1].Type__c);
        System.assertEquals('Sales', histories[1].Department__c);
        System.assertEquals(null, histories[1].EndDateTime__c);
    }

    @isTest
    static void TrackStatusOnExistingContact_CreatesNewAndUpdatesExistingStatusTrackingHistoryRecord()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createProspectCold(user, '61408815279');
        Map<Id, Contact> oldMap = new Map<Id, Contact>();
        ContactService.trackStatusHistory(new List<Contact> { contact }, oldMap, Enums.TriggerContext.AFTER_INSERT);
        oldMap.put(contact.Id, contact);
        Contact newContact = contact.clone(true, false, true, true);
        newContact.Status__c = Constants.STATUS_DISQUALIFIED;

        // Act
        Test.startTest();
        ActionResult result = ContactService.trackStatusHistory(new List<Contact> { newContact }, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        List<Status_Tracking_History__c> histories =
        [
            SELECT Id, Contact_ID__c, Status_Stage__c, StartDateTime__c, EndDateTime__c, Call_Stage__c, Call_Outcome__c, Department__c, Type__c, Direction__c, Next_Status_Stage__c, Next_User__c
            FROM Status_Tracking_History__c
            WHERE Contact_ID__c = : contact.Id
            ORDER BY CreatedDate
        ];
        Test.stopTest();

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.Status);
        System.assertEquals(2, histories.size());

        System.assertEquals('Cold', histories[0].Status_Stage__c);
        System.assertEquals('State Change', histories[0].Type__c);
        System.assertEquals('Sales', histories[0].Department__c);
        System.assertNotEquals(null, histories[0].EndDateTime__c);
        System.assertEquals('Backward', histories[0].Direction__c);
        System.assertEquals('Disqualified', histories[0].Next_Status_Stage__c);
        System.assertEquals(UserInfo.getName(), histories[0].Next_User__c);

        System.assertEquals('Disqualified', histories[1].Status_Stage__c);
        System.assertEquals('State Change', histories[1].Type__c);
        System.assertEquals('Sales', histories[1].Department__c);
        System.assertEquals(null, histories[1].EndDateTime__c);
    }
}