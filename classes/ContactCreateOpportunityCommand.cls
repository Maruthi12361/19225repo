public class ContactCreateOpportunityCommand extends BaseTriggerCommand
{
    private static Map<Id, Opportunity> activeRecordMap = new Map<Id, Opportunity>();

    public override List<SObject> validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        List<Contact> result = new List<Contact>();
        for (Contact contact : (List<Contact>)items)
        {
            if (contact.RecordTypeId != System.Label.ID_RecordType_Contact_Prospect || contact.Status__c != Constants.STATUS_HOT)
                continue; // Contact is not a prospect or status does not equal hot

            if (triggerContext == Enums.TriggerContext.AFTER_INSERT || triggerContext == Enums.TriggerContext.AFTER_UPDATE && 
                SObjectUtils.hasAnyPropertyChanged(contact, oldMap.get(contact.Id), new List<String> {'Status__c', 'AccountId'}))
            {
                if (contact.Preferred_Start_Date__c == null || contact.Preferred_Start_Date__c == 'At A Later Date' || contact.AccountId == null)
                {
                    Log.info(this.className, 'validate', 'Contact {0} status changes to hot but Preferred Start Date or AccountId is null. Opportunity not created.', new List<String>{contact.Id});
                    continue;
                }

                if (activeRecordMap.containsKey(contact.Id))
                {
                    Log.info(this.className, 'validate', 'Contact {0} has already had opportunity created in this transaction. Opportunity not created.', new List<String>{contact.Id});
                    continue;
                }

                result.add(contact); // Status is hot and it has been changed
            }
        }

        return result;
    }

    public override ActionResult command(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        Set<Id> ids = SObjectUtils.getIDs(items, 'Id');
        List<Opportunity> opps = getOpenOpportunities(ids);

        for (Contact contact : (List<Contact>)items)
        {
            List<Opportunity> contactOpportunities = getContactOpenOpportunities(opps, contact.Id);

            if (contactOpportunities.size() > 0)
            {
                Log.info(this.className, 'command', 'There are open opportunities, no need to create new one');
                continue;
            }

            createNewOpportunity(contact);
        }

        return new ActionResult();
    }

    private Opportunity createNewOpportunity(Contact contact)
    {
        String name = String.format('{0} {1}', new List<String>{String.isEmpty(contact.FirstName) ? '' : contact.FirstName, contact.LastName});

        String year = contact.Preferred_Start_Date__c.right(4);
        String month = contact.Preferred_Start_Date__c.mid(3, 2);
        String day = contact.Preferred_Start_Date__c.left(2);
        Date preferredStartDate = Date.valueOf(year + '-' + month + '-' + day);
        String portfolio = contact.Portfolio__c;
        String program = contact.Program__c;
        
        Opportunity opp = new Opportunity
        (
            Name = name + ' - ' + contact.ProgramPortfolio__c,
            CloseDate = preferredStartDate.addDays(11),
            StageName = 'Applying',
            AccountId = contact.AccountId,
            Contact__c = contact.Id,
            OwnerId = contact.OwnerId,
            RecordTypeId = System.Label.ID_RecordType_Opportunity_Acquisition,
            Program__c = program,
            Portfolio__c = portfolio
        );

        if (portfolio != null)
        {
            if (portfolio.contains('Domestic'))
                opp.Primary_Location_During_Studies__c = 'Australia';
            else if (portfolio.contains('Canada'))
                opp.Primary_Location_During_Studies__c = 'Canada';
            else if (portfolio.contains('New Zealand'))
                opp.Primary_Location_During_Studies__c = 'New Zealand';
        }

        uow.registerNew(opp);
        activeRecordMap.put(contact.Id, opp);

        Log.info(this.className, 'createNewOpportunity', 'Creating new Acquisition Opportunity for ' + contact.Id + ' contact');

        return opp;
    }

    private List<Opportunity> getContactOpenOpportunities(List<Opportunity> opps, Id contactID)
    {
        List<Opportunity> result = new List<Opportunity>();

        for (Opportunity opp : opps)
        {
            if (opp.Contact__c == contactID)
                result.add(opp);
        }

        return result;
    }

    private List<Opportunity> getOpenOpportunities(Set<Id> ids)
    {
        List<Opportunity> opps = 
            [
                SELECT Id, RecordTypeId, StageName, Contact__c 
                FROM Opportunity 
                WHERE RecordTypeId = :System.Label.ID_RecordType_Opportunity_Acquisition AND Contact__c IN :ids AND IsClosed = false
            ];
        return opps;
    }
}