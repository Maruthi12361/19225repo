public class CaseFileFlagsValidate extends BaseValidation
{
    @testVisible
    private static final String ERROR_MSG_NOLOOFILE = 'No Letter of Offer files found for case. Cannot set Vetting Complete field value';
    @testVisible
    private static final String ERROR_MSG_NOINVOICEFILE = 'No Invoice files found for case. Cannot set Invoice Generated field value to true';

    public override ActionResult validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        Id applicationRecordTypeId = RecordTypeUtils.getRecordTypeId(Constants.RECORDTYPE_CASE_APPLICATION, SObjectType.Case);
        List<Case> applicationCases = new List<Case>();

        for (Case currentCase : (List<Case>) items)
        {
            Case oldValue = (Case) oldMap.get(currentCase.Id);

            if (currentCase.RecordTypeId == applicationRecordTypeId &&
                ((currentCase.Vetting_Complete__c != oldValue.Vetting_Complete__c && currentCase.Vetting_Complete__c != null) ||
                (currentCase.Invoice_Generated__c != oldValue.Invoice_Generated__c && currentCase.Invoice_Generated__c != null)))
                applicationCases.add(currentCase);
        }

        if (applicationCases.size() > 0)
        {
            List<ViewModelsFile.Details> files = getFiles(SObjectUtils.getIDs(applicationCases, 'Id'));

            for (Case currentCase : applicationCases)
            {
                Case oldValue = (Case) oldMap.get(currentCase.Id);
                List<ViewModelsFile.Details> looFiles = getCaseFiles(currentCase.Id, Constants.CONTENTVERSION_TYPE_LOO, files);

                if (currentCase.Vetting_Complete__c != oldValue.Vetting_Complete__c && currentCase.Vetting_Complete__c != null && looFiles.size() == 0)
                {
                    Log.error(this.className, 'validate', 'Case: {0} Message: {1}', new List<String> { currentCase.Id, ERROR_MSG_NOLOOFILE });
                    currentCase.addError(ERROR_MSG_NOLOOFILE);
                    return new ActionResult(ResultStatus.ERROR, ERROR_MSG_NOLOOFILE);
                }

                List<ViewModelsFile.Details> invoiceFiles = getCaseFiles(currentCase.Id, Constants.CONTENTVERSION_TYPE_INVOICE, files);

                if (currentCase.Invoice_Generated__c != oldValue.Invoice_Generated__c && currentCase.Invoice_Generated__c != null && invoiceFiles.size() == 0)
                {
                    Log.error(this.className, 'validate', 'Case: {0} Message: {1}', new List<String> { currentCase.Id, ERROR_MSG_NOINVOICEFILE });
                    currentCase.addError(ERROR_MSG_NOINVOICEFILE);
                    return new ActionResult(ResultStatus.ERROR, ERROR_MSG_NOINVOICEFILE);
                }
            }
        }

        return new ActionResult(ResultStatus.SUCCESS);
    }

    private List<ViewModelsFile.Details> getFiles(Set<ID> ids)
    {
        return ContentDocumentService.getFiles(ids);
    }

    private List<ViewModelsFile.Details> getCaseFiles(ID caseId, String type, List<ViewModelsFile.Details> files)
    {
        List<ViewModelsFile.Details> caseFiles = new List<ViewModelsFile.Details>();

        for (ViewModelsFile.Details file : files)
            if (file.EntityId == caseId && file.Type == type)
                caseFiles.add(file);

        return caseFiles;
    }
}