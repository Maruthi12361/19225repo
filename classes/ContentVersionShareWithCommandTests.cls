@isTest
public class ContentVersionShareWithCommandTests
{
    @testSetup
    public static void setup()
    {
        TestUtilities.setupHEDA();
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createProspectHot(user);
        TestDataOpportunityMock.createApplyingAcquisition(user, contact);
        TestDataContentVersionMock.create(UserService.getByAlias(Constants.USER_SYSTEMADMIN), contact.Id);
    }

    @isTest
    static void ShareWith_UpdatesLinksToIncludeReference()
    {
        // Arrange
        Id contactId = [SELECT Id FROM Contact LIMIT 1].Id;
        Id documentId = [SELECT ContentDocumentId FROM ContentVersion LIMIT 1].ContentDocumentId;

        // Act
        ActionResult result = ContentDocumentService.shareWith(contactId, new Set<Id> { documentId });

        // Assert
        List<ContentDocumentLink> links =
            [
                SELECT Id, LinkedEntityId, ShareType, Visibility
                FROM ContentDocumentLink
                WHERE ContentDocumentId = : documentId AND LinkedEntityId = : contactId
            ];
        System.assertEquals(ResultStatus.SUCCESS, result.Status);
        System.assertEquals(1, links.size());
        System.assertEquals(Constants.FILE_CONTENT_SHARETYPE_VIEWER, links[0].ShareType);
        System.assertEquals(Constants.FILE_CONTENT_VISIBILITY_STANDARD, links[0].Visibility);
    }

    @isTest
    static void ShareWithAgain_DoesntUpdatesLinksToIncludeReferenceTwice()
    {
        // Arrange
        Id contactId = [SELECT Id FROM Contact LIMIT 1].Id;
        Id documentId = [SELECT ContentDocumentId FROM ContentVersion LIMIT 1].ContentDocumentId;

        // Act
        ActionResult result = ContentDocumentService.shareWith(contactId, new Set<Id> { documentId });
        result = ContentDocumentService.shareWith(contactId, new Set<Id> { documentId });

        // Assert
        List<ContentDocumentLink> links =
            [
                SELECT Id, LinkedEntityId, ShareType, Visibility
                FROM ContentDocumentLink
                WHERE ContentDocumentId = : documentId AND LinkedEntityId = : contactId
            ];
        System.assertEquals(ResultStatus.SUCCESS, result.Status);
        System.assertEquals(1, links.size());
        System.assertEquals(Constants.FILE_CONTENT_SHARETYPE_VIEWER, links[0].ShareType);
        System.assertEquals(Constants.FILE_CONTENT_VISIBILITY_STANDARD, links[0].Visibility);
    }
}