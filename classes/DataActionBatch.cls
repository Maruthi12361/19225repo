public class DataActionBatch extends BaseBatchCommand
{
    private String objectName { get; set; }
    private List<ViewModels.DataAction> actions { get; set; }
    private String whereClause { get; set; }

    public DataActionBatch()
    {
        super(30);
    }

    public DataActionBatch(String objectName, List<ViewModels.DataAction> actions)
    {
        this(objectName, actions, null);
    }

    public DataActionBatch(String objectName, List<ViewModels.DataAction> actions, String whereClause)
    {
        validate(objectName, actions);
        this.objectName = objectName;
        this.actions = actions;
        this.whereClause = whereClause;
    }

    public override Database.QueryLocator query(Database.BatchableContext BC)
    {
        Set<String> fields = new set<String>();

        for (ViewModels.DataAction action : actions)
        {
            if (action.FirstField != null) 
                fields.add(action.FirstField);
            if (action.SecondField != null) 
                fields.add(action.SecondField);
        }

        String query =
            'SELECT ' + String.join(new List<String>(fields), ', ') +
            ' FROM ' + objectName;

        if (this.whereClause != null)
        {
            query = query + ' WHERE (' + whereClause + ')';
        }

        Log.info(this.className, 'query', 'Generated query: ' + query);

        return Database.getQueryLocator(query);
    }

    public override void command(Database.BatchableContext BC, List<sObject> scope)
    {
        List<sObject> objectsToUpdate = new List<SObject>();

        for (sObject currentObject : scope)
        {
            Boolean toUpdateObject = false;

            for (ViewModels.DataAction action : actions)
            {
                if (action.ActionName == Enums.DataUpdateAction.MOVE)
                {
                    Object fromValue = currentObject.get(action.FirstField);
                    Object toValue = currentObject.get(action.SecondField);

                    if (fromValue != toValue)
                    {
                        currentObject.put(action.SecondField, fromValue);
                        toUpdateObject = true;
                    }
                }
                else if (action.ActionName == Enums.DataUpdateAction.WRITE)
                {
                    Object toField = currentObject.get(action.FirstField);
                    Object fromValue = action.UpdateValue;
                    
                    if (toField != fromValue)
                    {
                        if (updateField(currentObject, action.FirstField, fromValue))
                            toUpdateObject = true;
                    }
                }
                else if (action.ActionName == Enums.DataUpdateAction.TRANSFORM)
                {
                    Object fromValue = currentObject.get(action.FirstField);
                    Schema.DisplayType fieldType = SObjectUtils.getFieldType(currentObject, action.FirstField);
                    
                    switch on action.ActionType 
                    {
                        when 'ADDDAYS'
                        {
                            if (fieldType == Schema.DisplayType.Date)
                            {
                                Date fromDate = Date.valueOf(fromValue);
                                if (fromDate != null)
                                {
                                    currentObject.put(action.SecondField, fromDate.addDays(Integer.valueOf(action.UpdateValue)));
                                    toUpdateObject = true;
                                }
                            }
                            else if (fieldType == Schema.DisplayType.Datetime)
                            {
                                Datetime fromDateTime = Datetime.valueOf(fromValue);
                                if (fromDateTime != null)
                                {
                                    currentObject.put(action.SecondField, fromDateTime.addDays(Integer.valueOf(action.UpdateValue)));
                                    toUpdateObject = true;
                                }
                            }
                            else
                            {
                                Log.info(this.className, 'command', 'Invalid field type {0} for action type {1}', new List<String>{ String.valueOf(fieldType), action.ActionType });   
                            }
                        }
                        when 'ADDHOURS'
                        {
                            if (fieldType == Schema.DisplayType.Datetime)
                            {
                                Datetime fromDateTime = Datetime.valueOf(fromValue);
                                if (fromDateTime != null)
                                {
                                    currentObject.put(action.SecondField, fromDateTime.addHours(Integer.valueOf(action.UpdateValue)));
                                    toUpdateObject = true;
                                }
                            }
                            else
                            {
                                Log.info(this.className, 'command', 'Invalid field type {0} for action type {1}', new List<String>{ String.valueOf(fieldType), action.ActionType });   
                            }
                        }
                        when else
                        {
                            Log.info(this.className, 'command', 'No valid action type found');
                        }
                    }
                }
            }

            if (toUpdateObject)
                objectsToUpdate.add(currentObject);
        }

        if (objectsToUpdate.size() > 0)
            Database.update(objectsToUpdate);
    }

    private void validate(String objectName, List<ViewModels.DataAction> actions)
    {
        if (String.isBlank(objectName))
        {
            Log.error(this.className, 'DataActionBatch', Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Object Name' });
            throw new Exceptions.InvalidParameterException(String.format(Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Object Name' }));
        }
        else if (actions == null || actions.size() == 0)
        {
            Log.error(this.className, 'DataActionBatch', 'Actions parameter value cannot be null or empty list');
            throw new Exceptions.InvalidParameterException('Action parameter is null or empty list');
        }
    }

    private Boolean updateField(SObject obj, String fieldName, Object fieldValue)
    {
        Boolean isDirty = false;

        try
        {
            Schema.DisplayType fieldType = SObjectUtils.getFieldType(obj, fieldName);

            if (fieldValue == null)
            {
                obj.put(fieldName, fieldValue);
                isDirty = true;
            }
            else if (fieldType == Schema.DisplayType.String)
            {
                Integer length = SObjectUtils.getMaxStringLength(obj, fieldName);
                String stringValue = fieldValue.toString();
                stringValue = StringUtils.copyLeft(stringValue, length);

                obj.put(fieldName, stringValue);
                isDirty = true;
            }
            else if (fieldType == Schema.DisplayType.Integer)
            {
                obj.put(fieldName, Integer.valueOf(fieldValue));
                isDirty = true;
            }
            else if (fieldType == Schema.DisplayType.Double || fieldType == Schema.DisplayType.PERCENT)
            {
                obj.put(fieldName, Double.valueOf(fieldValue));
                isDirty = true;
            }
            else if (fieldType == Schema.DisplayType.Date)
            {
                obj.put(fieldName, Date.valueOf(fieldValue.toString()));
                isDirty = true;
            }
            else if (fieldType == Schema.DisplayType.Datetime)
            {
                String dateStringValue = fieldValue.toString();
                dateStringValue = dateStringValue.replace('T', ' ');
                dateStringValue = dateStringValue.replace('Z', ' ');

                obj.put(fieldName, Datetime.valueOfGmt(dateStringValue));
                isDirty = true;
            }
            else if (fieldType == Schema.DisplayType.MultiPicklist)
            {
                obj.put(fieldName, fieldValue.toString().replace(',', ';'));
                isDirty = true;
            }
            else if (fieldType == Schema.DisplayType.Boolean)
            {
                obj.put(fieldName, Boolean.valueOf(fieldValue));
                isDirty = true;
            }
            else
            {
                obj.put(fieldName, fieldValue);
                isDirty = true;
            }
        }
        catch(Exception ex)
        {
            String errMsg = String.format('Failed to set value: {0} on fieldName: {1} in Object: {2}.', new List<String>{ String.valueOf(fieldValue), fieldName, String.valueOf(obj.getSObjectType()) });
            Log.error(this.className, 'updateField', errMsg, ex);
            throw new Exceptions.ApplicationException(errMsg);
        }

        return isDirty;
    }
}