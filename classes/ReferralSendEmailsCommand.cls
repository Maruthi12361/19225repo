public  class ReferralSendEmailsCommand extends BaseQueueableCommand
{
    public ReferralSendEmailsCommand()
    {
        super(20, 'true');
    }

    protected override List<SObject> validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        for (Referral__c referral : (List<Referral__c>) items)
        {
            if (referral.Referrer_Contact__c == null || referral.Referee_Contact__c == null)
            {
                String message = 'Referrer Contact and Referree Contact field values cannot be null';
                Log.error(this.className, 'validate', message);
                throw new Exceptions.InvalidParameterException(message);
            }
        }

        return items;
    }

    protected override ActionResult command(List<SObject> items, Id jobId)
    {
        for (Referral__c referral : (List<Referral__c>) items)
        {
            sendEmail(referral.Referee_Contact__c, 'Welcome Email Referee', referral.Id);
            sendEmail(referral.Referrer_Contact__c, 'Thank You Email Referral', referral.Id);
        }

        return new ActionResult(ResultStatus.SUCCESS);
    }

    @future(callout = true)
    private static void sendEmail(Id contactId, String templateName, Id referralId)
    {
        String fromName;
        String fromAddress;
        EmailTemplate template = EmailService.getEmailTemplate(templateName);
        Contact contact =
        [
            SELECT Id, Name, Email, OwnerId, Owner.Email, Owner.Name
            FROM Contact
            WHERE Id = :contactId
        ];
        Referral__c referral =
        [
            SELECT
                Id, Referee_Contact__r.FirstName, Referee_Contact__r.LastName, Referee_Contact__r.Name,
                Referrer_Contact__r.FirstName, Referrer_Contact__r.LastName, Referrer_Contact__r.Name
            FROM Referral__c
            WHERE Id = :referralId
        ];

        Messaging.SingleEmailMessage mail = Messaging.renderStoredEmailTemplate(template.Id, contact.OwnerId, contact.Id);
        String body = mail.getHtmlBody();

        if (templateName == 'Thank You Email Referral')
        {
            fromName = Label.Referral_ThankYouEmail_FromName;
            fromAddress = Label.Referral_ThankYouEmail_FromAddress;

            body = body.replace('{{referee.FirstName:default=edit me}}', referral.Referee_Contact__r.FirstName);
            body = body.replace('{{referee.LastName:default=edit me}}', referral.Referee_Contact__r.LastName);
            body = body.replace('{{referee.Name:default=edit me}}', referral.Referee_Contact__r.Name);
        }
        else if (templateName == 'Welcome Email Referee')
        {
            fromName = Label.Referral_WelcomeEmailReferee_FromName;
            fromAddress = Label.Referral_WelcomeEmailReferee_FromAddress;
        }

        ActionResult result = EmailService.sendToRecipient(Constants.APIPROVIDER_DEFAULT, contact.Email, contact.Name, mail.getSubject(), fromAddress, fromName, null, body, null);

        if (result.isError)
            Log.error('ReferralSendEmailsCommand', 'sendEmail', 'Failed to send email {0} to {1}<{2}> due to: {3}.', new List<String>{ templateName, contact.Owner.Name, contact.Owner.Email, result.message });
        else
            Log.info('ReferralSendEmailsCommand', 'sendEmail', 'Successfully sent email {0} to {1}<{2}>.', new List<String>{ templateName, contact.Name, contact.Email });
    }
}