public class OptionService 
{
    public static ViewModels.PicklistOptions getPickListValues(ViewModels.Param param)
    {
        return new OptionGetPicklistValuesQuery().query(param);
    }
}