@isTest
public class ContactEnableSSOCommandTests
{
    @testSetup
    static void setup()
    {
        TestUtilities.setupHEDA();
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createProspectCold(user, 'John', 'Test','61408815278', 'john.test@yahoo.com', 'A0019789789');
    }

    @isTest
    static void ProspectNotCreatedInAzureWithSSOProspectProvisioningStatus_SSOStatusUpdatedSuccessfully()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        TestUtilities.disableComponent('ContactSendSSOLoginDetailsCommand');
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createProspectCold(user, 'John', 'Test','61408815278', 'john.test@yahoo.com', 'A0000000000');
        contact.SingleSignOnStatus__c = Constants.CONTACT_SINGLESIGNONSTATUS_PROSPECTPROVISIONING;
        update contact;

        // Act
        Test.startTest();
        ActionResult result = ContactService.enableSSO(new List<Contact> { contact });
        Test.stopTest();

        Contact updatedContact = [SELECT Id, SingleSignOnStatus__c, SSO_ID__c, SingleSignOnStatusLastUpdated__c FROM Contact WHERE Id = :contact.Id];

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.status, 'Unexpected result status');
        System.assertEquals(Constants.CONTACT_SINGLESIGNONSTATUS_PROSPECTENABLED, updatedContact.SingleSignOnStatus__c, 'Unexpected single sign on status');
        System.assert(String.isNotEmpty(updatedContact.SSO_ID__c), 'Expected SSO ID to have value');
        System.assert(updatedContact.SingleSignOnStatusLastUpdated__c != null, 'Expected single sign on status last update date to have value');
    }

    @isTest
    static void CreatedProspectInAzureWithSSOProspectProvisioningStatus_SSOStatusUpdatedSuccessfully()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = [SELECT Id, Cerebro_Student_ID__c, SingleSignOnStatus__c FROM Contact LIMIT 1];
        contact.SingleSignOnStatus__c = Constants.CONTACT_SINGLESIGNONSTATUS_PROSPECTPROVISIONING;
        update contact;

        // Act
        Test.startTest();
        ActionResult result = ContactService.enableSSO(new List<Contact> { contact });
        Test.stopTest();

        Contact updatedContact = [SELECT Id, SingleSignOnStatus__c, SSO_ID__c, SingleSignOnStatusLastUpdated__c FROM Contact WHERE Id = :contact.Id];

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.status, 'Unexpected result status');
        System.assertEquals(Constants.CONTACT_SINGLESIGNONSTATUS_PROSPECTENABLED, updatedContact.SingleSignOnStatus__c, 'Unexpected single sign on status');
        System.assert(String.isNotEmpty(updatedContact.SSO_ID__c), 'Expected SSO ID to have value');
        System.assert(updatedContact.SingleSignOnStatusLastUpdated__c != null, 'Expected single sign on status last update date to have value');
    }

    @isTest
    static void CreatedProspectInAzureWithSSOStudentProvisioningStatus_SSOStatusUpdatedSuccessfully()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = [SELECT Id, Cerebro_Student_ID__c, SingleSignOnStatus__c FROM Contact LIMIT 1];
        contact.SingleSignOnStatus__c = Constants.CONTACT_SINGLESIGNONSTATUS_STUDENTPROVISIONING;
        update contact;

        // Act
        Test.startTest();
        ActionResult result = ContactService.enableSSO(new List<Contact> { contact });
        Test.stopTest();

        Contact updatedContact = [SELECT Id, SingleSignOnStatus__c, SSO_ID__c, SingleSignOnStatusLastUpdated__c FROM Contact WHERE Id = :contact.Id];

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.status, 'Unexpected result status');
        System.assertEquals(Constants.CONTACT_SINGLESIGNONSTATUS_STUDENTENABLED, updatedContact.SingleSignOnStatus__c, 'Unexpected single sign on status');
        System.assert(String.isNotEmpty(updatedContact.SSO_ID__c), 'Expected SSO ID to have value');
        System.assert(updatedContact.SingleSignOnStatusLastUpdated__c != null, 'Expected single sign on status last update date to have value');
    }
}