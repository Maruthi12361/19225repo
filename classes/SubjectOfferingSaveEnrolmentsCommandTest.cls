@isTest
public class SubjectOfferingSaveEnrolmentsCommandTest
{
    @testSetup
    public static void setup()
    {
        TestUtilities.setupHEDA();
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Account department = TestDataAccountMock.createDepartmentBusiness(user);
        hed__Term__c term = TestDataTermMock.create(user, department.Id, Date.today());
        hed__Course__c subjectDefinition = TestDataSubjectMock.createDefinitionCGOV(user);
        hed__Course__c subjectVersion = TestDataSubjectMock.createVersionCGOV(user);
        hed__Course_Offering__c subjectOffering = TestDataSubjectOfferingMock.createOffering(user, subjectVersion, term);

        Contact facultyContact = TestDataContactMock.createFacultyContact(user);
        TestDataSubjectEnrolmentMock.createFacultyEnrolment(user,subjectOffering, facultyContact, 'Recommended', 'Subject Coordinator');
    }

    @isTest
    static void BlankIdentifier_EmptyParameterError()
    {
        // Arrange
        String errorMessage = '';
        ViewModels.Param param = new ViewModels.Param();
        param.Identifier = '';
        param.Payload = null;

        // Act
        try
        {
           ActionResult result = new SubjectOfferingSaveEnrolmentsCommand().command(param);
        }
        catch (Exception ex)
        {
           errorMessage = ex.getMessage();
        }

        // Assert
        System.assert(errorMessage.contains('Identifier parameter cannot be empty or null'), 'Unexpected error message');
    }

    @isTest
    static void BlankPayload_EmptyPayloadError()
    {
        // Arrange
        String errorMessage = '';
        hed__Course_Offering__c subjectOffering = [SELECT Id FROM hed__Course_Offering__c LIMIT 1];
        ViewModels.Param param = new ViewModels.Param();
        param.Identifier = subjectOffering.Id;
        param.Payload = null;

        // Act
        try
        {
           ActionResult result = new SubjectOfferingSaveEnrolmentsCommand().command(param);
        }
        catch (Exception ex)
        {
           errorMessage = ex.getMessage();
        }

        // Assert
        System.assert(errorMessage.contains('Enrolments payload cannot be empty or null'), 'Unexpected error message');
    }

    @isTest
    static void SubjectOfferingWithEnrolment_EnrolmentUpdatedSuccessfully()
    {
        // Arrange
        hed__Course_Offering__c subjectOffering = [SELECT Id FROM hed__Course_Offering__c LIMIT 1];
        hed__Course_Enrollment__c subjectEnrolment = [SELECT Id, hed__Course_Offering__c FROM hed__Course_Enrollment__c WHERE hed__Course_Offering__c = :subjectOffering.Id];
        List<ViewModelsWorkforce.SubjectEnrolmentStaffDetailsItem> enrolments = new List<ViewModelsWorkforce.SubjectEnrolmentStaffDetailsItem>();

        ViewModelsWorkforce.SubjectEnrolmentStaffDetailsItem enrolment = new ViewModelsWorkforce.SubjectEnrolmentStaffDetailsItem();
        enrolment.SubjectEnrolmentId = subjectEnrolment.Id;
        enrolment.RoleName = 'Subject Coordinator';
        enrolment.AllocatedStudents = 18;
        enrolment.Status = 'Recommended';

        enrolments.add(enrolment);

        ViewModels.Param param = new ViewModels.Param();
        param.Identifier = subjectOffering.Id;
        param.Payload = JSON.serialize(enrolments);

        // Act
        test.startTest();
        ActionResult result = new SubjectOfferingSaveEnrolmentsCommand().command(param);
        hed__Course_Enrollment__c updatedSubjectEnrolment =
        [
            SELECT
                Id, hed__Course_Offering__c, Role__c, hed__Status__c, StudentsAllocated__c
            FROM hed__Course_Enrollment__c 
            WHERE hed__Course_Offering__c = :subjectOffering.Id
        ];
        test.stopTest();

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Unexpected result status value');
        System.assertEquals(enrolment.RoleName, updatedSubjectEnrolment.Role__c, 'Unexpected Role value');
        System.assertEquals(enrolment.AllocatedStudents, updatedSubjectEnrolment.StudentsAllocated__c, 'Unexpected student allocated value');
        System.assertEquals('Draft', updatedSubjectEnrolment.hed__Status__c, 'Unexpected status value');
    }

    @isTest
    static void SubjectOfferingWithNewEnrolment_EnrolmentSavedSuccessfully()
    {
        // Arrange
        hed__Course_Offering__c subjectOffering = [SELECT Id FROM hed__Course_Offering__c LIMIT 1];
        hed__Course_Enrollment__c subjectEnrolment = [SELECT Id, hed__Course_Offering__c FROM hed__Course_Enrollment__c WHERE hed__Course_Offering__c = :subjectOffering.Id];
        List<ViewModelsWorkforce.SubjectEnrolmentStaffDetailsItem> enrolments = new List<ViewModelsWorkforce.SubjectEnrolmentStaffDetailsItem>();

        ViewModelsWorkforce.SubjectEnrolmentStaffDetailsItem newEnrolment = new ViewModelsWorkforce.SubjectEnrolmentStaffDetailsItem();
        newEnrolment.SubjectEnrolmentId = 'UUID12548';
        newEnrolment.RoleName = 'Online Facilitator';
        newEnrolment.AllocatedStudents = 21;
        newEnrolment.Status = 'Recommended';

        enrolments.add(newEnrolment);

        ViewModels.Param param = new ViewModels.Param();
        param.Identifier = subjectOffering.Id;
        param.Payload = JSON.serialize(enrolments);

        // Act
        test.startTest();
        ActionResult result = new SubjectOfferingSaveEnrolmentsCommand().command(param);
        List<hed__Course_Enrollment__c> offeringEnrolments =
        [
            SELECT
                Id, hed__Course_Offering__c, Role__c, hed__Status__c, StudentsAllocated__c
            FROM hed__Course_Enrollment__c
            WHERE hed__Course_Offering__c = :subjectOffering.Id
        ];
        test.stopTest();

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Unexpected result status value');
        System.assertEquals(2, offeringEnrolments.size(), 'Unexpected number of enrolments');
    }
}