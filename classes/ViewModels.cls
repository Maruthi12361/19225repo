public class ViewModels
{
    public class IdWrap
    {
        public ID ID;
    }

    public class Code implements Interfaces.IBaseCode
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }

    public class LightningCode implements Interfaces.IBaseCode
    {
        public string label { get; set; }
        public string value { get; set; }
    }

    public class Param implements Interfaces.IViewModel
    {
        public Param()
        {
            AllowUpdate = true;
        }

        public String Identifier { get; set; }
        public String Payload { get; set; }
        public Boolean AllowUpdate { get; set; }
        public Boolean IsAuthenticated { get; set; }
    }

    public class PicklistValue
    {
        public String label { get; set; }
        public String value { get; set; }
        public Integer[] validFor { get; set; }
        public Object attributes { get; set; }
    }

    public class PicklistOptions implements Interfaces.IViewModel
    {
        public object Data { get; set; }
    }

    public class DataAction implements Interfaces.IViewModel
    {
        public Enums.DataUpdateAction ActionName { get; set; }
        public String FirstField { get; set; }
        public String SecondField { get; set; }
        public String UpdateValue { get; set; }
        public String ActionType { get; set; }

        public DataAction(Enums.DataUpdateAction actionName, String firstValue, String secondValue)
        {
            this.ActionName = actionName;
            if (actionName == Enums.DataUpdateAction.MOVE)
            {
                this.FirstField = firstValue;
                this.SecondField = secondValue;
            }
            else if (actionName == Enums.DataUpdateAction.WRITE)
            {
                this.FirstField = firstValue;
                this.UpdateValue = secondValue;
            }
        }

        public DataAction(Enums.DataUpdateAction actionName, String firstValue, String secondValue, String updateValue, String actionType)
        {
            if (actionName == Enums.DataUpdateAction.TRANSFORM)
            {
                this.ActionName = actionName;
                this.FirstField = firstValue;
                this.SecondField = secondValue;
                this.UpdateValue = updateValue;
                this.ActionType = actionType;
            }
            else
            {
                this(actionName, firstValue, secondValue);
            }
        }
    }
}