public class OpportunityMoveTermBatch extends BaseBatchCommand
{
    private String query =
        'SELECT Id, CampaignId, Campaign.Type, Campaign.TermStartDate__c, Contact__r.ProgramPortfolio__c, TermMoveCount__c, CloseDate ' +
        'FROM Opportunity ' +
        'WHERE RecordType.DeveloperName = \'' + Constants.RECORDTYPE_OPPORTUNITY_ACQUISITION + '\' AND Contact__c <> null AND IsClosed = false AND CloseDate < TODAY AND StageName <> \'Commencing\' ';

    private String opportunityIds;

    private static List<Campaign> allCampaignList =
        [
            SELECT Id, Name, TermStartDate__c, TermCensusDate__c, Portfolio__c, Programme__c
            FROM Campaign
            WHERE Type IN ('Acquisition') AND Status IN : Constants.CAMPAIGN_ACTIVE_STATUS AND IsActive = true AND TermStartDate__c > TODAY
            ORDER BY TermStartDate__c
        ];

    public OpportunityMoveTermBatch()
    {
    }

    public OpportunityMoveTermBatch(String stringParam)
    {
        if (stringParam != null && String.isNotBlank(stringParam))
            opportunityIds = stringParam;
    }

    public override Database.QueryLocator query(Database.BatchableContext context)
    {
        if (opportunityIds != null)
        {
            query += ' AND Id IN (' + opportunityIds + ')';
        }

        return Database.getQueryLocator(query);
    }

    public override void command(Database.BatchableContext context, List<sObject> scope)
    {
        List<Opportunity> updates = new List<Opportunity>();

        for (Opportunity opportunity : (List<Opportunity>) scope)
        {
            Campaign campaign;
            Date preferredDate;
            Boolean matchByCensusDate = false;

            if (opportunity.TermMoveCount__c >= 2)
            {
                opportunity.StageName = 'Closed Lost';
                opportunity.Lost_Reason__c = 'Term Move Auto Closed';
                updates.add(opportunity);
                continue;
            }

            if (opportunity.CampaignId == null || opportunity.Campaign.Type != 'Acquisition' || opportunity.Campaign.TermStartDate__c == null)
            {
                preferredDate = opportunity.CloseDate;
                matchByCensusDate = true;
            }
            else
                preferredDate = opportunity.Campaign.TermStartDate__c;

            campaign = getNextCampaignByDate(preferredDate, opportunity.Contact__r.ProgramPortfolio__c, matchByCensusDate);

            if (campaign != null)
            {
                opportunity.CampaignId = campaign.Id;

                if (opportunity.TermMoveCount__c == null)
                    opportunity.TermMoveCount__c = 1;
                else
                    opportunity.TermMoveCount__c = opportunity.TermMoveCount__c + 1;
                
                updates.add(opportunity);
            }
        }

        if (updates.size() > 0)
            uow.registerDirty(updates);
    }

    private Campaign getNextCampaignByDate(Date preferredDate, String programPortfolio, Boolean matchByCensusDate)
    {
        Campaign assignedCampaign;
        String portfolio = 'Domestic';
        String programme = 'MBA';
        Integer daysBetween;

        if (!String.isBlank(programPortfolio))
        {
            if (programPortfolio.startsWith('TC'))
            {
                portfolio = 'TC';
            }
            else
            {
                if (programPortfolio.contains('BBA'))
                    programme = 'BBA';

                if (programPortfolio.contains('IMBA') || programPortfolio.contains('IBBA'))
                    portfolio = 'International';
            }
        }

        for (Campaign campaign : allCampaignList)
        {
            daysBetween = matchByCensusDate ? preferredDate.daysBetween(campaign.TermCensusDate__c) : preferredDate.daysBetween(campaign.TermStartDate__c);

            if (daysBetween > 0 && campaign.Programme__c == programme && campaign.Portfolio__c == portfolio)
            {
                assignedCampaign = campaign;
                break;
            }
        }

        Log.info(this.className, 'getNextCampaignByDate', 'Match by census date: ' + matchByCensusDate + ', ' + preferredDate + ', ' + programPortfolio + ' to New Campaign: ' + assignedCampaign);

        return assignedCampaign;
    }
}