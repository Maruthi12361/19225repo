@isTest
public class ContactSendSSOLoginDetailsCommandTests
{
    @isTest
    static void InvalidTemplate_EmailNotFoundErrorReturned()
    {
        // Arrange
        String errorMsg = '';
        ContactSendSSOResetPassEmailCommand.templateName = 'Non existant template';

        // Act
        Test.startTest();
        try
        {
            ActionResult result = ContactService.sendSSOResetPasswordEmail(null);
        } 
        catch (Exception ex)
        {
            errorMsg = ex.getMessage();
        }
        Test.stopTest();

        // Assert
        System.assertEquals(true, errorMsg.startsWith('No email template found'), 'Unexpected email not found error message');
    }

    @isTest
    static void NullParameter_NullParameterErrorReturned()
    {
        // Arrange
        String errorMessage = '';
        String expectedErrorMessage = String.format(Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Param object' });

        // Act
        try
        {
            ActionResult result = ContactService.sendSSOEmail(null);
        }
        catch (Exception ex)
        {
            errorMessage = ex.getMessage();
        }

        // Assert
        System.assertEquals(errorMessage, expectedErrorMessage, 'Unexpected null parameter error message');
    }


    @isTest
    static void EmptyContactId_EmptyContactIdErrorReturned()
    {
        // Arrange
        String errorMessage = '';
        String expectedErrorMessage = String.format(Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Contact Id' });
        ViewModelsSSO.EmailLoginDetails emailDetails = new ViewModelsSSO.EmailLoginDetails();

        // Act
        try
        {
            ActionResult result = ContactService.sendSSOEmail(emailDetails);
        }
        catch (Exception ex)
        {
            errorMessage = ex.getMessage();
        }

        // Assert
        System.assertEquals(errorMessage, expectedErrorMessage, 'Unexpected contact id error message');
    }

    @isTest
    static void EmptySingleSignOnId_EmptySingleSignOnIdErrorReturned()
    {
        // Arrange
        String errorMessage = '';
        String expectedErrorMessage = String.format(Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Single Sign On Id' });
        ViewModelsSSO.EmailLoginDetails emailDetails = new ViewModelsSSO.EmailLoginDetails();
        emailDetails.contactId = '0032O000004byrhQAA';

        // Act
        try
        {
            ActionResult result = ContactService.sendSSOEmail(emailDetails);
        }
        catch (Exception ex)
        {
            errorMessage = ex.getMessage();
        }

        // Assert
        System.assertEquals(errorMessage, expectedErrorMessage, 'Unexpected single sign on id error message');
    }

    @isTest
    static void EmptyContactEmail_EmptyEmailErrorReturned()
    {
        // Arrange
        String errorMessage = '';
        String expectedErrorMessage = String.format(Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Contact email' });
        ViewModelsSSO.EmailLoginDetails emailDetails = new ViewModelsSSO.EmailLoginDetails();
        emailDetails.contactId = '0032O000004byrhQAA';
        emailDetails.singleSignOnId = 'A0001978578@study.edu.au';

        // Act
        try
        {
            ActionResult result = ContactService.sendSSOEmail(emailDetails);
        }
        catch (Exception ex)
        {
            errorMessage = ex.getMessage();
        }

        // Assert
        System.assertEquals(errorMessage, expectedErrorMessage, 'Unexpected email error message');
    }

    @isTest
    static void EmptyContactFirstName_EmptyFirstNameErrorReturned()
    {
        // Arrange
        String errorMessage = '';
        String expectedErrorMessage = String.format(Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Contact first name' });
        ViewModelsSSO.EmailLoginDetails emailDetails = new ViewModelsSSO.EmailLoginDetails();
        emailDetails.contactId = '0032O000004byrhQAA';
        emailDetails.singleSignOnId = 'A0001978578@study.edu.au';
        emailDetails.contactEmail = 'john.smith@email.com';

        // Act
        try
        {
            ActionResult result = ContactService.sendSSOEmail(emailDetails);
        }
        catch (Exception ex)
        {
            errorMessage = ex.getMessage();
        }

        // Assert
        System.assertEquals(errorMessage, expectedErrorMessage, 'Unexpected first naem error message');
    }

    @isTest
    static void EmptyContactLastName_EmptyLastNameErrorReturned()
    {
        // Arrange
        String errorMessage = '';
        String expectedErrorMessage = String.format(Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Contact last name' });
        ViewModelsSSO.EmailLoginDetails emailDetails = new ViewModelsSSO.EmailLoginDetails();
        emailDetails.contactId = '0032O000004byrhQAA';
        emailDetails.singleSignOnId = 'A0001978578@study.edu.au';
        emailDetails.contactEmail = 'john.smith@email.com';
        emailDetails.contactFirstName = 'John';

        // Act
        try
        {
            ActionResult result = ContactService.sendSSOEmail(emailDetails);
        }
        catch (Exception ex)
        {
            errorMessage = ex.getMessage();
        }

        // Assert
        System.assertEquals(errorMessage, expectedErrorMessage, 'Unexpected last name error message');
    }

    @isTest
    static void EmptyPassword_EmptyPasswordErrorReturned()
    {
        // Arrange
        String errorMessage = '';
        String expectedErrorMessage = String.format(Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Password' });
        ViewModelsSSO.EmailLoginDetails emailDetails = new ViewModelsSSO.EmailLoginDetails();
        emailDetails.contactId = '0032O000004byrhQAA';
        emailDetails.singleSignOnId = 'A0001978578@study.edu.au';
        emailDetails.contactEmail = 'john.smith@email.com';
        emailDetails.contactFirstName = 'Smith';
        emailDetails.contactLastName = 'Smith';

        // Act
        try
        {
            ActionResult result = ContactService.sendSSOEmail(emailDetails);
        }
        catch (Exception ex)
        {
            errorMessage = ex.getMessage();
        }

        // Assert
        System.assertEquals(errorMessage, expectedErrorMessage, 'Unexpected password error message');
    }

    @isTest
    static void ValidParameters_EmailSuccessfullSent()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        TestDataContactMock.createProspectCold(user, 'John', 'Test','61408815278', 'john.test@yahoo.com', 'A0019789789');
        Contact contact = [SELECT Id, FirstName, LastName, Email, Cerebro_Student_ID__c, EnrolmentTrackingURL__c FROM Contact LIMIT 1];

        ViewModelsSSO.EmailLoginDetails emailDetails =
            new ViewModelsSSO.EmailLoginDetails(contact.Id, contact.FirstName, contact.LastName, contact.email, contact.Cerebro_Student_ID__c + '@study.edu.au', 'pass@#e3', contact.EnrolmentTrackingURL__c);

        // Act
        Test.startTest();
        ActionResult result = ContactService.sendSSOEmail(emailDetails);
        Test.stopTest();

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.status, 'Unexpected status');
        System.assertEquals(1, TestHttpCalloutMock.requests.size());
    }
}