@IsTest
public class TestDataLeadMock 
{
    public static Lead create(User user)
    {
        Lead lead = new Lead
        (
            OwnerId = user.Id,
            Status = 'New',
            FirstName = 'John',
            LastName = 'Smith',
            Email = 'john.smith@eamil.com',
            Company = 'John Smith Company',
            Phone = '0411122233',
            MobilePhone = '0411122238',
            Country = 'Australia'
        );

        System.RunAs(user)
        {
            insert lead;
        }

        return lead;
    }
}