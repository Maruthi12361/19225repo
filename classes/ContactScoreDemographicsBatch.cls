public class ContactScoreDemographicsBatch extends BaseBatchCommand
{
    private static final String contactConfigType = 'Contact';
    private static final String opportunityConfigType = 'Opportunity';
    private static final String prospectTypeName = Constants.RECORDTYPE_CONTACT_PROSPECT;
    private static final List<String> closedStages = Constants.OPPORTUNITY_ACQUISITION_CLOSEDSTAGES;

    private List<SalesScoreConfig__c> salesScoreConfigs;

    public override Database.QueryLocator query(Database.BatchableContext context)
    {
        return Database.getQueryLocator(getIdQuery()); 
    }

    public override void command(Database.BatchableContext context, List<sObject> scope)
    {
        Map<Id, Double> contactDemographicScoreMap = new Map<Id, Double>();
        List<Contact> contacts = getContacts(SObjectUtils.getIDs(scope, 'ID'));

        salesScoreConfigs = getSalesScoreConfigs();

        for (Contact contact : contacts)
        {
            Double totalDemographicScore = getTotalBehaviourScore(contact);

            if (contact.DemographicScore__c != totalDemographicScore)
            {
                Double score;
                Double maximumScore = Double.valueOf(System.Label.LeadScoring_DemographicMaximum);

                if (totalDemographicScore > maximumScore)
                {
                    Log.info(this.className, 'command', 'Setting Demographic score to maximum score {0} as total demographic score is greater: {1}. ContactID: {2}', 
                        new List<String> { String.valueOf(maximumScore), String.valueOf(totalDemographicScore), contact.Id });
                    if (contact.DemographicScore__c != maximumScore)
                        score = maximumScore;
                }
                else
                {
                    Log.info(this.className, 'command', 'Setting Demographic score to total demographic score: {0}. ContactID: {1}', 
                        new List<String> { String.valueOf(totalDemographicScore), contact.Id });
                    score = totalDemographicScore;
                }

                if (!contactDemographicScoreMap.containsKey(contact.Id) && score != null)
                    contactDemographicScoreMap.put(contact.Id, score);
            }
        }

        if (contactDemographicScoreMap.size() > 0)
            save(contactDemographicScoreMap);
    }

    protected override void finalise(Database.BatchableContext context)
    {
        Database.executebatch(new OpportunityScoreDemographicsBatch(), 50);
    }

    private void save(Map<Id, Double> scoreMap)
    {
        List<Contact> contacts = [SELECT Id, DemographicScore__c FROM Contact WHERE Id in :scoreMap.keySet()];

        for (Contact contact : contacts)
            contact.DemographicScore__c = scoreMap.get(contact.Id);

        uow.registerDirty(contacts);
    }

    private Double getTotalBehaviourScore(Contact contact)
    {
        Double totalDemographicScore = 0;

        for (SalesScoreConfig__c config : salesScoreConfigs)
        {
            String fieldValue;
            if (config.ScoreType__c == contactConfigType)
            {
                fieldValue = contact.get(config.Source__c) == null ? '' : String.valueOf(contact.get(config.Source__c));

                if (StringUtils.isLogicalStatementTrue(config.Operator__c, fieldValue, config.Value__c))
                    totalDemographicScore += config.ScoreValue__c;
            }
            else if (contact.Opportunities__r.size() > 0 && config.ScoreType__c == opportunityConfigType)
            {
                for (Opportunity opp : contact.Opportunities__r)
                {
                    fieldValue = opp.get(config.Source__c) == null ? '' : String.valueOf(opp.get(config.Source__c));

                    if (StringUtils.isLogicalStatementTrue(config.Operator__c, fieldValue,  config.Value__c))
                    {
                        totalDemographicScore += config.ScoreValue__c;
                        break;
                    }
                }
            }
        }

        return totalDemographicScore;
    }

    private List<SalesScoreConfig__c> getSalesScoreConfigs()
    {
        Set<String> demographicScoreTypes = new Set<String>{ 'Contact', 'Opportunity' };

        return
            [
                SELECT Id, Active__c, Operator__c, ScoreType__c, ScoreValue__c, Source__c, Value__c, DisplayOrder__c
                  FROM SalesScoreConfig__c
                 WHERE Active__c = true AND ScoreType__c in :demographicScoreTypes
                 ORDER BY DisplayOrder__c
            ];
    }

    private List<Contact> getContacts(Set<Id> contactIds)
    {
        return Database.query(getQuery(contactIds));
    }

    private String getIdQuery()
    {
        return
            'SELECT Id, RecordTypeId ' +
            '  FROM Contact ' +
            ' WHERE LastModifiedDate = LAST_N_DAYS:30 AND RecordType.Name = :prospectTypeName';
    }

    private String getQuery(Set<Id> contactIds)
    {
        return
            'SELECT ' +
                'Id, ANZPermanentResidency__c, Aboriginal_or_Torres_Strait_Islander_Des__c, ActualCreateDate__c, Alumni__c, ApplicationCourse__c, AtRisk__c, ' +
                'AustralianVisaHolder__c, BBAComplete__c, Backup_Mobile_Phone__c, Backup_Phone__c, BehaviourDecaySum__c, BehaviourScoreRollup__c, BehaviourScore__c, ' +
                'Birthdate, CASLImpliedConsentExpiry__c, CHESSN_Number__c, Campaign_Contact_ID__c, CerebroSyncMessage__c, ' +
                'Cerebro_Student_ID_Link__c, Cerebro_Student_ID__c, Course_Start_Date__c, CurrentSalary__c, CurrentStudent__c, Current_Course_Status__c, ' +
                'Current_Employer__c, Current_Position__c, Days_Left_Until_Implied_Consent_Expires__c, Days_Until_Implied_SMS_Consent_Expires__c, ' +
                'DemographicScore__c, DisqualifiedReason__c, DoNotCall, Email, EmailVerified__c, Email_Express_Consent_Date__c, Email_Express_Consent_Exists__c, ' +
                'Emergency_Contact_Full_Name__c, Emergency_Contact_Mobile__c, Emergency_Contact_Relationship__c, Emergency_Contact_Telephone__c, ' +
                'Emergency_Contact_Title__c, Enabled_All_Apps_Access__c, Enabled_Signup_Access__c, EncryptedId__c, EnglishLevel__c, Exam_Time__c, ' +
                'Express_Consent_Exists__c, Express_Consent_Expires__c, Express_Consent_Given__c, FeeHelp_Approved__c, FeeHelp_Eligibility__c, FeeSet__c, ' +
                'FeesInCreditBalance__c, FirstName, GCMComplete__c, HasOptedOutOfEmail, High_School_Level__c,High_School_State_Territory__c, ' +
                'HighestQualificationCompletionYear__c, HighestQualificationInstitution__c, HighestQualificationLanguage__c, HighestQualificationName__c, ' +
                'HighestTertiaryLevelDegree__c, Highest_Qualification__c, HomePhone, Impairments__c, Implied_Consent_Expiry_Date__c, Incomplete_Subjects__c, ' +
                'Indigenous_Support_Services__c, InferredCity__c, InferredCompany__c, InferredCountry__c, InferredMetropolitanArea__c, InferredPhoneAreaCode__c, ' +
                'InferredPostalCode__c, InferredStateRegion__c, IsDecayLessThenScore__c, IsEnroledCurrentTerm__c, IsEnroledFutureTerm__c, IsEnroledNextTerm__c, ' +
                'IsMBAEligible__c, IsWarmCampaignMember__c, Is_My_Contact__c, Is_My_Team_Contact__c, Language_Spoken_at_Home__c, LastCallDateTime__c, LastCLOODate__c, ' +
                'LastCallOutcome__c, LastCourseCompletionDate__c, LastModifiedDate, LastName, Last_Used_Funding_Source__c, LeadId__c, LeadSource, Legacy_CRM_ID__c, ' +
                'LinkedIn_Profile__c, Lookup_Mobile__c, MBAComplete__c, MBA_Course_Name__c, MC_Subjects__c, MailingAddressFull__c, MailingCity, MailingCountry, ' +
                'MailingPostalCode, MailingState, MailingStreet, Mailing_Postcode_Not_Applicable__c, ManagementExperienceYears__c, Manual_Followup_Date__c, ' +
                'MiddleName, MobilePhone, MobilePhoneCountry__c, NVMAutoCLIDContact__c, NVMContactOwnerID__c, NVM_Owner_Email__c, Name, ' +
                'Next_Followup_Date__c, Next_Step__c, OAF_Application_Id__c, OK_To_Call__c, Objection__c, OriginalReferrerSource__c, OtherAddressFull__c, ' +
                'OtherAddressType__c, OtherCity, OtherCountry, OtherPostalCode, OtherState, OtherStreet, Other_Postcode_Not_Applicable__c, OwnerLinkedInUrl__c, ' +
                'Passed_Subjects__c, Passport_Number__c, Perm_Postcode_Not_Applicable__c, PermanentAddressFull__c, PermanentAddressType__c, ' +
                'Permanent_Home_Address__c, Permanent_Home_Country__c, Permanent_Home_Postcode__c, Permanent_Home_State_Province_Region__c, ' +
                'Permanent_Home_Suburb_Town__c, PersonalEmail__c, Phone, PhoneCountry__c, PreferredStartDateTime__c, PreferredTimeframeLastModified__c, ' +
                'PreferredTimeframe__c, Preferred_Start_Date__c, PrimaryMotivations__c, Primary_SRO_Student_Profile_Notes__c, ProgramPortfolio__c, Recycled__c, ' +
                'RefereeOneEmployer__c, RefereeOneFullName__c, RefereeOneJobTitle__c, RefereeOneMobileCountry__c, RefereeOneMobile__c, ' +
                'RefereeOneWorkEmail__c, RefereeTwoEmployer__c, RefereeTwoFullName__c, RefereeTwoJobTitle__c, RefereeTwoMobileCountry__c, ' +
                'RefereeTwoMobile__c, RefereeTwoWorkEmail__c, Required_Subjects__c, RolloverEligible__c, RolloverSegmentation__c, Rollover_Call_Count__c, ' +
                'Rollover_Colour__c, Rollover_Contact_History__c, Rollover_Count__c, Rollover_Deferred_Reason_Other__c, Rollover_Deferred_Reason__c, ' +
                'Rollover_Email_Count__c, Rollover_Followup_Date__c, Rollover_Lost_Count__c, Rollover_Status__c, SMS_Express_Consent_Date__c, ' +
                'SMS_Express_Consent_Exists__c, SMS_Implied_Consent_Expiry_Date__c, SSO_ID__c, Salutation, SourceDetail__c, Special_Needs_Details__c, ' +
                'StandardTimetableAgreement__c, Status__c, Student_Type__c, Visa_Expiry_Date__c, Visa_Type__c, WorkExperienceYears__c, YearOfLeavingHighSchool__c, ' +
                'hed__AlternateEmail__c, hed__Citizenship__c, hed__Country_of_Origin__c, hed__Dual_Citizenship__c, hed__Gender__c, hed__UniversityEmail__c, hed__WorkEmail__c, ' +
                '( ' +
                        'SELECT ' +
                            'Id, ApplicationDate__c, ApplicationCutoffDate__c, CLOODate__c, ConcurrentSubjects__c, Consent_Full_Name__c, ' +
                            'ContactFirstName__c, Course_Info_Read__c, CreditTransferRequested__c, ' +
                            'Custom_Timetable_Required__c, English_Proficiency_Details__c, English_Proficiency_Test__c, ' +
                            'English_Proficiency__c, Expected_Admin_Date_Calc__c, FEEHELP_Previous_Firstname__c, FEEHELP_Previous_Lastname__c, ' +
                            'FEEHELP_Previous_Names__c, FEE_HELP_Forms_Submitted__c, Financial_Support_Source__c, Has_Special_Needs__c, Invoice_Recipient_Address__c, ' +
                            'Invoice_Recipient_Fax__c, Invoice_Recipient_Mobile__c, Invoice_Recipient_Name__c, Invoice_Recipient_Telephone__c, IsExpatDeclared__c, ' +
                            'LastModifiedDate, Last_Year_of_Participation__c, Linked_Case_Status__c, Linked_Case_URL__c, Lost_Description__c, Lost_Reason__c, ' +
                            'Make_RPL_claim__c, Management_Responsibilities__c, ManualFollowUpDate__c, Name, NVM_Owner_ID__c, NumberOfEnroledSubjects__c, ' +
                            'OAF_Agreed_Course_Information_Version__c, OAF_Agreed_Terms_And_Condition_Version__c, OAF_Application_Type__c, OAF_Last_Submitted__c, ' +
                            'OAF_Submit_Count__c, OAF_Submitted__c, Offer_Documentation_Sent__c, Own_Business_Years__c, OwnerLinkedInURL__c, Owner_for_Commission__c, ' +
                            'Parent_1_Highest_Educational_Attainment__c, Parent_2_Highest_Educational_Attainment__c, Parent_Education_Options__c, Payment_Method__c, ' +
                            'Primary_Location_During_Studies__c, ProcessBuilderLogOpp__c, RecordTypeId, Remittance_Received__c, Signature__c, ' +
                            'Special_Needs_Advice_Required__c, StageName, StandardTimetableAgreement__c, Studying_in_Australia__c, TermMoveCount__c, ' +
                            'Terms_And_Condition_Agreed__c, Verbal_Checklist_Complete__c, Vetting_Complete__c, Welcome_Letter_Sent__c, With_Prospect_and_CA__c ' +
                        '  FROM Opportunities__r ' +
                        ' WHERE StageName NOT IN :closedStages ' +
                    ') ' +
                '  FROM Contact ' +
                ' WHERE Id = :contactIds';
    }
}