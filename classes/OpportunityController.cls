public class OpportunityController 
{
    private static final Map<String, String> opportunityCampaingTypeMap = new Map<String, String>
    {
        'Acquisition' => 'Acquisition',
        'Progression' => 'Rollover'
    };

    @AuraEnabled
    public static String getMoveTermCampaignData(Id id)
    {
        ViewModelsOpportunity.MoveTermCampaign result = new ViewModelsOpportunity.MoveTermCampaign();
        Opportunity opp = getOpportunity(id);

        if (opp != null)
        {
            if (opportunityCampaingTypeMap.containsKey(opp.RecordType.DeveloperName))
                result.Campaigns = getCampaigns(opportunityCampaingTypeMap.get(opp.RecordType.DeveloperName), opp.CampaignId);
            else
                Log.info('OpportunityController', 'getTermMoveReasons', 'Opportunity {0} type has no supported campaign type', new String[] { opp.RecordType.DeveloperName });

            result.Options = getOptions();
        }
        else
            Log.info('OpportunityController', 'getMoveTermCampaignData', 'No opportunity {0} found in database', new List<String> { id });

        return JSON.serialize(result);
    }

    private static Opportunity getOpportunity(Id id)
    {
        return
        [
            SELECT CampaignId, Id, RecordType.DeveloperName
            FROM Opportunity
            WHERE Id = :id
        ];
    }

    private static List<ViewModels.LightningCode> getCampaigns(String type, Id oppCampaignId)
    {
        List<ViewModels.LightningCode> campaignOptions = new List<ViewModels.LightningCode>();
        List<Campaign> campaigns = 
        [
            SELECT Id, Name, Type, IsActive
            FROM Campaign
            WHERE Type = :type AND IsActive = TRUE AND Id != :oppCampaignId
            ORDER BY Name
        ];

        if (campaigns.size() > 0)
        {
            for (Campaign campaign : campaigns)
            {
                ViewModels.LightningCode option = new ViewModels.LightningCode();
                option.label = campaign.Name;
                option.value = campaign.Id;

                campaignOptions.add(option);
            }
        }

        return campaignOptions;
    }

    private static Object getOptions()
    {
        Map<String, Object> optionMap = new Map<String, Object>
        {
            'list' => 'TermMoveReasons',
            'isLightningModel' => true
        };
        ViewModels.Param optionParam = new ViewModels.Param();
        optionParam.Payload = JSON.serialize(optionMap);

        Viewmodels.PicklistOptions options =  OptionService.getPickListValues(optionParam);

        return options.Data;
    }
}