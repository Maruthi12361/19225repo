@isTest
public class SkillSetGroupUserManagementCommandTests
{
    @testSetup
    static void setup()
    {
        TestUtilities.setupHEDA();
    }

    @isTest
    static void InsertCourseAdvisorSkillSet_AssignSkillSetUserToEmptyGroup()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Group g = [SELECT Id FROM Group WHERE DeveloperName =: Constants.GROUP_COURSE_ADVISORS AND Type = 'Regular' LIMIT 1];
        delete [SELECT Id FROM GroupMember WHERE GroupId =: g.Id];
        List<GroupMember> groupMembersInGroupBefore = [SELECT Id, UserOrGroupId FROM GroupMember WHERE GroupId =: g.Id];

        SkillSet__c skillSet = TestDataSkillSetMock.createStaff(user, user, Constants.GROUP_COURSE_ADVISORS, 'Gender: Indeterminate');
        List<SkillSet__c> items = new List<SkillSet__c>();
        items.add(skillSet);

        // Act
        Test.startTest();
        ActionResult result = SkillSetService.manageSkillSetUsersInGroups(items, new Map<Id, SkillSet__c>(), Enums.TriggerContext.AFTER_INSERT);
        Test.stopTest();

        // Assert
        List<GroupMember> groupMembersInGroupAfter = [SELECT Id, UserOrGroupId FROM GroupMember WHERE GroupId =: g.Id];
        User userAfter = [SELECT Id, inGroup__c FROM User WHERE Id =: groupMembersInGroupAfter[0].UserOrGroupId];
        System.assertEquals(ResultStatus.SUCCESS, result.Status);
        System.assertEquals(0, groupMembersInGroupBefore.size());
        System.assertEquals(1, groupMembersInGroupAfter.size(), 'Creation of a SkillSet of Type__c CourseAdvisor should\'ve added the associated user to the CoursAdvisor group.');
        System.assertEquals(Constants.GROUP_COURSE_ADVISORS, userAfter.inGroup__c, 'After being assigned to CourseAdvisor group CourseAdvisor user should have inGroup__c = CourseAdvisor.');
    }

    @isTest
    static void DeleteExistingSkillSet_RemoveUserFromGroup()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Group g = [SELECT Id FROM Group WHERE DeveloperName =: Constants.GROUP_COURSE_ADVISORS AND Type = 'Regular' LIMIT 1];
        GroupMember gm = [SELECT Id, userOrGroupId FROM GroupMember WHERE GroupId =: g.Id][0];
        User staff = UserService.get(gm.UserOrGroupId);
        delete [SELECT Id FROM GroupMember WHERE GroupId =: g.Id AND userOrGroupId != : staff.Id];
        List<GroupMember> groupMembersInGroupBefore = [SELECT Id, UserOrGroupId FROM GroupMember WHERE GroupId =: g.Id];

        SkillSet__c skillSet = TestDataSkillSetMock.createStaff(user, staff, Constants.GROUP_COURSE_ADVISORS, 'Gender: Indeterminate');
        List<SkillSet__c> items = new List<SkillSet__c>();
        items.add(skillSet);
        System.runAs(user)
        {
            delete skillSet;
        }

        // Act
        Test.startTest();
        ActionResult result = SkillSetService.manageSkillSetUsersInGroups(items, new Map<Id, SkillSet__c>(), Enums.TriggerContext.AFTER_DELETE);
        Test.stopTest();

        // Assert
        List<GroupMember> groupMembersInGroupAfter = [SELECT Id, UserOrGroupId FROM GroupMember WHERE GroupId =: g.Id];
        User userAfter = [SELECT Id, inGroup__c FROM User WHERE Id =: staff.Id];
        System.assertEquals(ResultStatus.SUCCESS, result.Status);
        System.assertEquals(1, groupMembersInGroupBefore.size());
        System.assertEquals(0, groupMembersInGroupAfter.size(), 'Deletion of a SkillSet of Type__c CourseAdvisor should\'ve removed the associated user from the COurseAdvisor group.');
        System.assertEquals(null, userAfter.inGroup__c, 'After being removed from CourseAdvisor group user should not have a value in inGroup__c.');
    }

    @isTest
    static void UpdateSkillSetWithDifferentUser_AssignNewUserToGroupWhileRemovingOldUser()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Group g = [SELECT Id FROM Group WHERE DeveloperName =: Constants.GROUP_COURSE_ADVISORS AND Type = 'Regular' LIMIT 1];
        GroupMember gm = [SELECT Id, userOrGroupId FROM GroupMember WHERE GroupId =: g.Id][0];
        User staff = UserService.get(gm.UserOrGroupId);
        delete [SELECT Id FROM GroupMember WHERE GroupId =: g.Id AND userOrGroupId != : staff.Id];
        List<GroupMember> groupMembersInGroupBefore = [SELECT Id, UserOrGroupId FROM GroupMember WHERE GroupId =: g.Id];

        SkillSet__c skillSet = TestDataSkillSetMock.createStaff(user, staff, Constants.GROUP_COURSE_ADVISORS, null);
        Map<Id, SkillSet__c> oldMap = new Map<Id, SkillSet__c>{ skillSet.id => skillSet };
        List<SkillSet__c> items = new List<SkillSet__c>();
        SkillSet__c updatedSkillSet = skillSet.clone(true, false, true, true);
        updatedSkillSet.User__c = user.id;
        items.add(updatedSkillSet);
        System.RunAs(user)
        {
            update updatedSkillSet;
        }

        // Act
        Test.startTest();
        ActionResult result = SkillSetService.manageSkillSetUsersInGroups(items, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        Test.stopTest();

        // Assert
        List<GroupMember> groupMembersInGroupAfter = [SELECT Id, UserOrGroupId FROM GroupMember WHERE GroupId =: g.Id];
        User staffAfter = [SELECT Id, inGroup__c FROM User WHERE Id =: staff.Id];
        User userAfter = [SELECT Id, inGroup__c FROM User WHERE Id =: user.Id];
        System.assertEquals(ResultStatus.SUCCESS, result.Status);
        System.assertEquals(1, groupMembersInGroupBefore.size());
        System.assertEquals(1, groupMembersInGroupAfter.size());
        System.assertEquals(staff.Id, groupMembersInGroupBefore[0].UserOrGroupId);
        System.assertEquals(user.Id, groupMembersInGroupAfter[0].UserOrGroupId);
        System.assertNotEquals(groupMembersInGroupBefore[0].UserOrGroupId, groupMembersInGroupAfter[0].UserOrGroupId);
        System.assertEquals(null, staffAfter.inGroup__c, 'After being removed from CourseAdvisor group user should not have a value inGroup__c.');
        System.assertEquals(Constants.GROUP_COURSE_ADVISORS, userAfter.inGroup__c, 'After being assigned to CourseAdvisor group CourseAdvisor user should have inGroup__c = CourseAdvisor.');
    }

    @isTest
    static void UpdateTwoSkillSetsSwapUsers_SwapUsersGroupsAndSwapUserInGroupValues()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Group groupCC = [SELECT Id FROM Group WHERE DeveloperName =: Constants.GROUP_COURSE_CONSULTANTS AND Type = 'Regular' LIMIT 1];
        Group groupCA = [SELECT Id FROM Group WHERE DeveloperName =: Constants.GROUP_COURSE_ADVISORS AND Type = 'Regular' LIMIT 1];
        GroupMember gmCC = [SELECT Id, userOrGroupId FROM GroupMember WHERE GroupId =: groupCC.Id][0];
        GroupMember gmCA = [SELECT Id, userOrGroupId FROM GroupMember WHERE GroupId =: groupCA.Id][0];
        User staffCC = UserService.get(gmCC.UserOrGroupId);
        User staffCA = UserService.get(gmCA.UserOrGroupId);
        delete [SELECT Id FROM GroupMember WHERE GroupId =: groupCC.Id AND userOrGroupId != : staffCC.Id];
        delete [SELECT Id FROM GroupMember WHERE GroupId =: groupCA.Id AND userOrGroupId != : staffCA.Id];
        List<GroupMember> groupMembersInGroupCCBefore = [SELECT Id, UserOrGroupId FROM GroupMember WHERE GroupId =: groupCC.Id];
        List<GroupMember> groupMembersInGroupCABefore = [SELECT Id, UserOrGroupId FROM GroupMember WHERE GroupId =: groupCA.Id];

        SkillSet__c skillSetCC = TestDataSkillSetMock.createStaff(user, staffCC, Constants.GROUP_COURSE_CONSULTANTS, null);
        SkillSet__c skillSetCA = TestDataSkillSetMock.createStaff(user, staffCA, Constants.GROUP_COURSE_ADVISORS, null);
        Map<Id, SkillSet__c> oldMap = new Map<Id, SkillSet__c>{ skillSetCC.Id => skillSetCC, skillSetCA.Id => skillSetCA };
        List<SkillSet__c> items = new List<SkillSet__c>();
        SkillSet__c updatedSkillSetCC = skillSetCC.clone(true, false, true, true);
        SkillSet__c updatedSkillSetCA = skillSetCA.clone(true, false, true, true);
        updatedSkillSetCC.User__c = staffCA.Id;
        updatedSkillSetCA.User__c = staffCC.Id;
        items.add(updatedSkillSetCC);
        items.add(updatedSkillSetCA);
        System.RunAs(user)
        {
            update updatedSkillSetCC;
            update updatedSkillSetCA;
        }

        // Act
        Test.startTest();
        ActionResult result = SkillSetService.manageSkillSetUsersInGroups(items, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        Test.stopTest();

        // Assert
        List<GroupMember> groupMembersInGroupCCAfter = [SELECT Id, UserOrGroupId FROM GroupMember WHERE GroupId =: groupCC.Id];
        List<GroupMember> groupMembersInGroupCAAfter = [SELECT Id, UserOrGroupId FROM GroupMember WHERE GroupId =: groupCA.Id];
        User staffCCAfter = [SELECT Id, inGroup__c FROM User WHERE Id =: staffCC.Id];
        User staffCAAfter = [SELECT Id, inGroup__c FROM User WHERE Id =: staffCA.Id];
        System.assertEquals(ResultStatus.SUCCESS, result.Status);
        System.assertEquals(1, groupMembersInGroupCCBefore.size(), 'Group size before and after should remain the same at 1 user as we are only swapping users.');
        System.assertEquals(1, groupMembersInGroupCABefore.size(), 'Group size before and after should remain the same at 1 user as we are only swapping users.');
        System.assertEquals(1, groupMembersInGroupCCAfter.size(), 'Group size before and after should remain the same at 1 user as we are only swapping users.');
        System.assertEquals(1, groupMembersInGroupCAAfter.size(), 'Group size before and after should remain the same at 1 user as we are only swapping users.');
        System.assertEquals(groupMembersInGroupCCBefore[0].UserOrGroupId, groupMembersInGroupCAAfter[0].UserOrGroupId, 'CC Staff member after update/swap should now be in CA group.');
        System.assertEquals(groupMembersInGroupCABefore[0].UserOrGroupId, groupMembersInGroupCCAfter[0].UserOrGroupId, 'CA Staff member after update/swap should now be in CC group.');
        System.assertEquals(Constants.GROUP_COURSE_ADVISORS, staffCCAfter.inGroup__c, 'After being removed from CourseConsultant group and put in CourseAdvisor group user should have inGroup__c = CourseAdvisor.');
        System.assertEquals(Constants.GROUP_COURSE_CONSULTANTS, staffCAAfter.inGroup__c, 'After being removed from CourseAdvisor group and put in CourseConsultant group user should have inGroup__c = CourseConsultant.');
    }
}