public class InterestingMomentUpdateScoringSchedule implements Schedulable 
{
    public void execute(SchedulableContext sc)
    {
        Database.executebatch(new InterestingMomentUpdateScoringBatch(), 200);
    } 
}