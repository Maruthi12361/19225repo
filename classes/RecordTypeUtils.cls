public class RecordTypeUtils
{
    public static Id getRecordTypeId(String developerName, String objectTypeName)
    {
        Schema.SObjectType sObjectType = ((SObject)Type.forName(objectTypeName).newInstance()).getSObjectType();

        return getRecordTypeId(developerName, sObjectType.getDescribe());
    }

    public static Id getRecordTypeId(String developerName, Schema.DescribeSObjectResult objectDescribeResult)
    {
        if (objectDescribeResult != null)
        {
            Map<String, RecordTypeInfo> recTypes = objectDescribeResult.getRecordTypeInfosByDeveloperName();

            for (String key : recTypes.keySet())
            {
                if (key.equalsIgnoreCase(developerName))
                    return recTypes.get(key).getRecordTypeId();
            }
        }

        return null;
    }
}