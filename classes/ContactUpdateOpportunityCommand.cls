public class ContactUpdateOpportunityCommand extends BaseQueueableCommand
{
    public ContactUpdateOpportunityCommand()
    {
        super(50, 5);
    }
    
    public override List<SObject> validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        List<Contact> filteredList = new List<Contact>();
        
        for (Contact contact: (List<Contact>)items)
        {
            if (triggerContext == Enums.TriggerContext.AFTER_UPDATE && SObjectUtils.hasAnyPropertyChanged(contact, oldMap.get(contact.Id), new List<String>{'Program__c', 'Portfolio__c'}))
                filteredList.add(contact);
        }
        
        return filteredList;
    }
    
    public override ActionResult command(List<SObject> items, Id jobId)
    {
        if (items.size() == 0)
            return new ActionResult(ResultStatus.SUCCESS);
        
        Map<Id, Opportunity> contactOppMap = getContactOpportunities((new Map<Id, SObject>(items)).keySet());
        List<Opportunity> updatedOpp = new List<Opportunity>();
        
        for (Contact contact: (List<Contact>) items)
        {
            Opportunity opp = contactOppMap.get(contact.Id);
            
            if (opp != null)
            {
                opp.Program__c = contact.Program__c;
                opp.Portfolio__c = contact.Portfolio__c;
                updatedOpp.add(opp);
            }
        }
        
        if (updatedOpp.size() > 0)
            update updatedOpp;
        
        return new ActionResult(ResultStatus.SUCCESS);
    }
    
    private Map<Id, Opportunity> getContactOpportunities(Set<Id> contactIds)
    {
        Map<Id, Opportunity> oppMap = new Map<Id, Opportunity>();
        List<Opportunity> opps =
        [
            SELECT Id, Contact__c, Program__c, Portfolio__c
            FROM Opportunity
            WHERE (NOT StageName IN ('Closed Won', 'Closed Lost')) AND (Contact__c IN :contactIds)
        ];

        for (Opportunity opp : opps)
            oppMap.put(opp.Contact__c, opp);

        return oppMap;
    }
}