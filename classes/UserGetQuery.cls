public class UserGetQuery extends BaseIdQuery
{
    public override List<SObject> query(Set<Id> ids)
    {
        List<User> items =
        [
            SELECT Alias, CommunityNickname, Email, EmailEncodingKey, FirstName, InGroup__c, IsActive, LanguageLocaleKey, LastName, LinkedInURL__c, 
                LocaleSidKey, Name, Phone, Profile.Name, ProfileId, Small_Photo_URL__c, TimeZoneSidKey, Title, Username, UserRoleId
            FROM User
            WHERE Id = :ids
            AND IsActive = true
        ];

        return items;
    }
}