public class UserGetByAliasQuery extends BaseStringQuery
{
    public override List<SObject> query(Set<String> values)
    {
        List<User> items =
        [
            SELECT Id, Alias, CommunityNickname, DecsOnD_Assignment_Tier__c, Email, EmailEncodingKey, FirstName, InGroup__c, IsActive, LanguageLocaleKey, LastName, LinkedInURL__c,
                LocaleSidKey, Name, Phone, ProfileId, Small_Photo_URL__c, TimeZoneSidKey,  Title, Username, UserPreferencesLightningExperiencePreferred, UserRoleId
            FROM User
            WHERE Alias = :values AND IsActive = true
        ];

        return items;
    }
}