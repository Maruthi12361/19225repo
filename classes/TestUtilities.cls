public class TestUtilities 
{
    private static List<String> disabledComponents = new List<String>();
    
    public static Boolean throwOnException = true;
    
    public static void enableComponent(String componentName)
    {
        Integer index = disabledComponents.indexOf(componentName);
        
        if (index >= 0)
            disabledComponents.remove(index);
    }
    
    public static void disableComponent(String componentName)
    {
        Integer index = disabledComponents.indexOf(componentName);
        
        if (index < 0)
            disabledComponents.add(componentName);
    }
    
    public static Boolean isComponentEnabled(String componentName)
    {
        Integer index = disabledComponents.indexOf(componentName);
        
        return index < 0;
    }
    
    public static void setupHEDA()
    {
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        TestDataTDTMConfigMock.create(user);
    }
}