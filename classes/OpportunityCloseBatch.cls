public class OpportunityCloseBatch extends BaseBatchCommand   
{
    protected override Database.QueryLocator query(Database.BatchableContext context)
    {
        String query = 'SELECT Id, StageName, Lost_Reason__c, Contact__c FROM Opportunity Where StageName = \'Commencing\' AND Contact__c <> NULL';
        return Database.getQueryLocator(query);
    }
    
    public override void command(Database.BatchableContext BC, List<sObject> scope) 
    {          
        ActionResult result = OpportunityService.close(scope);

        if (result.isError)
            throw new Exceptions.ApplicationException(result.message);
    }
}