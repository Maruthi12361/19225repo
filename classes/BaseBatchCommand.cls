public abstract class BaseBatchCommand extends BaseComponent implements Database.Batchable<sObject>
{
    private Boolean uowScope = false;
    private static Integer executions = 0;
    public BaseBatchCommand next { get; set; }
    public Integer nextSize { get; set; }

    protected UnitOfWork uow
    {
        get
        {
            return UnitOfWork.current;
        }
    }

    protected abstract Database.QueryLocator query(Database.BatchableContext context);

    protected abstract void command(Database.BatchableContext context, List<sObject> scope);

    protected virtual void finalise(Database.BatchableContext context)
    {

    }

    public BaseBatchCommand()
    {
        // Retry 5 times for all batches by default
        super(5);
    }

    public BaseBatchCommand(Integer retry)
    {
        // Set a different retry number or 0 to disable retry
        super(retry);
    }

    public Database.QueryLocator start(Database.BatchableContext context)
    {
        Log.info(this.className, 'start', 'Starting execution of batch.');
        Log.save();

        return query(context);
    }

    public void execute(Database.BatchableContext context, List<sObject> scope)
    {
        executions++;
        Log.info(this.className, 'execute', 'Processing batch group ' + executions);
        ActionResult result;

        for (Integer i = 0 ; i <= retry; i++)
        {
            List<sObject> clonedItems = scope.deepClone(true, true, true);
            result = executeBatch(context, clonedItems);

            if (result.Status == ResultStatus.SUCCESS || (!result.message.contains('UNABLE_TO_LOCK_ROW') && !result.message.contains('Please try again')))
                break;

            Log.info(this.className, 'execute', 'Unable to lock row, remaining retry {0}', new List<String>{ String.valueOf(retry - i) });
        }
        
        if (result.Status != ResultStatus.SUCCESS)
            throw new Exceptions.ApplicationException(result.message);

        Log.info(this.className, 'execute', 'Processed batch group ' + executions);
        Log.save();
    }

    private ActionResult executeBatch(Database.BatchableContext context, List<sObject> scope)
    {
        ActionResult result = new ActionResult();

        if (UnitOfWork.current == null)
        {
            uowScope = true;
            UnitOfWork.current = new UnitOfWork();
        }

        try
        {
            command(context, scope);

            if (uowScope)
            {
                uow.commitWork();
                uowScope = false;
            }
        }
        catch (Exception ex)
        {
            result = new ActionResult(ResultStatus.ERROR, 'Error executing command: ' + StringUtils.getExceptionMessage(ex));
            Log.error(this.className, 'executeBatch', 'Error executing command: ', ex);
            UnitOfWork.current = null;
            uowScope = false;
        }

        return result;
    }

    public virtual void finish(Database.BatchableContext context)
    {
        Log.info(this.className, 'finish', 'Finishing execution of batch.');

        AsyncApexJob job =
            [
                SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJObItems, CreatedBy.Email
                FROM AsyncApexJob
                WHERE Id =: context.getJobId()
            ];

        if (job.NumberOfErrors > 0)
        {
            EmailService.sendLegacy
            (
                job.CreatedBy.Email,
                this.className + ' Batch Error',
                String.format('Batch {0} completed, processing {1} batches with {2} batch failures.', new List<String> { this.className, String.valueOf(job.TotalJobItems), String.valueOf(job.NumberOfErrors)})
            );
        }

        Log.info(this.className, 'finish', 'Finished execution of batch, processing {0} batches with {1} batch failures.', new List<String> { String.valueOf(job.TotalJobItems), String.valueOf(job.NumberOfErrors)});
        Log.save();

        if (next != null)
        {
            if (nextSize > 0)
                Database.executeBatch(next, nextSize);
            else 
                Database.executeBatch(next);
        }

        finalise(context);
    }
}