@isTest
public class TestDataAppLogMock 
{
    public static List<AppLog__c> createAppLogs(User user, LoggingLevel type, Integer size){
        List<AppLog__c> logs = new List<AppLog__c>();
        for(Integer i = 0; i < size; i++)
        {
            String handler = String.format('{0}.{1}',new List<String>{'TestDataAppLogMock', 'createAppLogs'});
            AppLog__c appLog = new AppLog__c(
                Type__c = String.valueOf(type),
                Message__c = String.format('[{0}][{1}] {2}',new List<String>{Datetime.now().format('dd/MM/yyyy hh:mm:ss:SSS'), handler, 'Message number ' + i+1}),
                Handler__c = handler,
                CreatedDate = Date.parse('20/04/2018'));
            logs.add(appLog);
        }
        
        System.RunAs(user)
        {
            insert logs;
        }
        return logs;
    }
}