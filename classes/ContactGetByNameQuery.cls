public class ContactGetByNameQuery extends BaseStringQuery
{
    public override List<SObject> query(Set<String> values)
    {
        List<Contact> items = 
        [
            SELECT
            	Name,
            	FirstName,
            	LastName,
            	MobilePhone,
            	Student_Type__c
            FROM Contact
            WHERE Name = :values
        ];

        return items;
    }
}