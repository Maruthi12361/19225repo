public class TestDataAccountMock
{
    private static Map<String, Account> courseMap = new Map<String, Account>();
    private static Map<String, Account> institutionMap = new Map<String, Account>();
    private static Map<String, Account> departmentMap = new Map<String, Account>();
    private static Map<String, Account> programMap = new Map<String, Account>();
    private static Account businessOrg;

    public static Account create(User user, String name)
    {
        Account newItem = new Account
        (
            Name = name
        );

        System.RunAs(user)
        {
            insert newItem;
        }

        return newItem;
    }

    public static Account createFromContact(User user, Contact contact)
    {
        Account newItem = new Account
        (
            Name = contact.FirstName + ' ' + contact.MiddleName + ' ' + contact.LastName + ' - Administartion Account',
            hed__Primary_Contact__c = contact.Id
        );

        System.RunAs(user)
        {
            insert newItem;
        }

        return newItem;
    }

    public static Account createBusinessOrg(User user)
    {
        String orgName = 'AUSTRALIAN INSTITUTE OF BUSINESS PTY LTD';

         if (businessOrg != null)
            return businessOrg;

        List<Account> items =
            [
                SELECT Active__c, Name, RecordTypeId, Website
                FROM Account
                WHERE Name =: orgName AND RecordType.DeveloperName =: Constants.RECORDTYPE_ACCOUNT_BUSINESSORG
            ];

        if (items.size() > 0)
        {
            businessOrg = items[0];
            return businessOrg;
        }

        Account newItem = new Account
        (
            Active__c = true,
            Name = 'AUSTRALIAN INSTITUTE OF BUSINESS PTY LTD',
            RecordTypeId = RecordTypeUtils.getRecordTypeId(Constants.RECORDTYPE_ACCOUNT_BUSINESSORG, SObjectType.Account),
            Website = 'https://www.aib.edu.au/'
        );

        if (user  != null)
        {
            System.RunAs(user)
            {
                insert newItem;
            }
        }
        else
        {
            insert newItem;
        }

        businessOrg = newItem;
        return newItem;
    }

    public static Account createCourseBBA(User user)
    {
        Account department = TestDataAccountMock.createDepartmentBusiness(user);

        return createCourse(user, 'Bachelor of Business Administration', 'BBA', 'BBA', 'BBA', department.Id);
    }

    public static Account createCourseGCM(User user)
    {
        Account department = TestDataAccountMock.createDepartmentBusiness(user);

        return createCourse(user, 'Graduate Certificate in Management', 'GCM', 'GCM', 'GCM', department.Id);
    }

    public static Account createCourseMBA(User user)
    {
        Account department = TestDataAccountMock.createDepartmentBusiness(user);

        return createCourse(user, 'Master of Business Administration', 'MBA', 'MBA', 'MBA', department.Id);
    }

    public static Account createCourseMBAFin(User user)
    {
        Account department = TestDataAccountMock.createDepartmentBusiness(user);

        return createCourse(user, 'Master of Business Administration (Finance)', 'MBA (Finance)', 'MBA (Fin)', 'MBA', department.Id);
    }

    public static Account createCourse(User user, String name, String productName, String abbreviatedName, String program, Id departmentId)
    {
        return createCourses(user, new List<ViewModelsTestData.Course> { new ViewModelsTestData.Course(name, productName, abbreviatedName, program, departmentId) })[0];
    }

    public static List<Account> createCourses(User user, List<ViewModelsTestData.Course> courses)
    {
        List<Account> result = new List<Account>();
        List<Account> inserts = new List<Account>();

        for (ViewModelsTestData.Course course: courses)
        {
            String key = course.Name + course.Program;

            if (courseMap.containsKey(key))
            {
                result.add(courseMap.get(key));
                continue;
            }

            if (courseMap.size() == 0)
            {
                List<Account> items =
                    [
                        SELECT Active__c, Name, ProductName__c, Program__c, AbbreviatedName__c, AcademicOrganisationalUnitCode__c, RecordTypeId, ParentId
                        FROM Account
                        WHERE RecordType.DeveloperName =: Constants.RECORDTYPE_ACCOUNT_ACADEMICPROGRAM
                    ];

                for (Account item: items)
                    courseMap.put(item.Name + item.Program__c, item);
            }

            if (courseMap.containsKey(key))
            {
                result.add(courseMap.get(key));
                continue;
            }

            Account newItem = new Account
            (
                Active__c = true,
                Name = course.Name,
                ProductName__c = course.ProductName,
                Program__c = course.Program,
                AbbreviatedName__c = course.AbbreviatedName != null ? course.AbbreviatedName : (course.Program == 'MBA' ? '40957' : '40939'),
                AcademicOrganisationalUnitCode__c = 'AOU',
                RecordTypeId = RecordTypeUtils.getRecordTypeId(Constants.RECORDTYPE_ACCOUNT_ACADEMICPROGRAM, SObjectType.Account),
                ParentId = course.DepartmentId
            );

            courseMap.put(key, newItem);
            inserts.add(newItem);
            result.add(newItem);
        }

        if (inserts.size() > 0)
        {
            if (user  != null)
            {
                System.RunAs(user)
                {
                    insert inserts;
                }
            }
            else
            {
                insert inserts;
            }
        }

        return result;
    }

    public static Account createDepartment(User user, String name, Id institutionId)
    {
        return createDepartments(user, new List<ViewModelsTestData.Department> { new ViewModelsTestData.Department(name, institutionId) })[0];
    }

    public static List<Account> createDepartments(User user, List<ViewModelsTestData.Department> departments)
    {
        List<Account> result = new List<Account>();
        List<Account> inserts = new List<Account>();

        for (ViewModelsTestData.Department department: departments)
        {
            String key = department.Name;

            if (departmentMap.containsKey(key))
            {
                result.add(departmentMap.get(key));
                continue;
            }

            if (departmentMap.size() == 0)
            {
                List<Account> items =
                    [
                        SELECT Active__c, Name, Program__c, AcademicOrganisationalUnitCode__c, RecordTypeId, ParentId
                        FROM Account
                        WHERE RecordType.DeveloperName =: Constants.RECORDTYPE_ACCOUNT_DEPARTMENT
                    ];

                for (Account item: items)
                    departmentMap.put(item.Name, item);
            }

            if (departmentMap.containsKey(key))
            {
                result.add(departmentMap.get(key));
                continue;
            }

            Account newItem = new Account
            (
                Active__c = true,
                Name = department.Name,
                AcademicOrganisationalUnitCode__c = 'AOU',
                RecordTypeId = RecordTypeUtils.getRecordTypeId(Constants.RECORDTYPE_ACCOUNT_DEPARTMENT, SObjectType.Account),
                ParentId = department.InstitutionId
            );

            departmentMap.put(key, newItem);
            inserts.add(newItem);
            result.add(newItem);
        }

        if (inserts.size() > 0)
        {
            if (user  != null)
            {
                System.RunAs(user)
                {
                    insert inserts;
                }
            }
            else
            {
                insert inserts;
            }
        }

        return result;
    }

    public static Account createDepartmentBusiness(User user)
    {
        Account institution = TestDataAccountMock.createInstitutionAIB(user);

        return createDepartment(user, Constants.ACCOUNT_DEPARTMENT_AIBBUSINESS, institution.Id);
    }

    public static Account createDepartmentResearch(User user)
    {
        Account institution = TestDataAccountMock.createInstitutionAIB(user);

        return createDepartment(user, Constants.ACCOUNT_DEPARTMENT_AIBRESEARCH, institution.Id);
    }

    public static Account createInstitution(User user, String name)
    {
        return createInstitution(user, name, null, null);
    }

    public static Account createInstitution(User user, String name, Id businessOrgId, String website)
    {
        ViewModelsTestData.Institution a = new ViewModelsTestData.Institution(name, businessOrgId, website);

        return createInstitutions(user, new List<ViewModelsTestData.Institution> { a  } )[0];
    }

    public static List<Account> createInstitutions(User user, List<ViewModelsTestData.Institution> institutions)
    {
        List<Account> result = new List<Account>();
        List<Account> inserts = new List<Account>();

        for (ViewModelsTestData.Institution institution: institutions)
        {
            String key = institution.Name;

            if (institutionMap.containsKey(key))
            {
                result.add(institutionMap.get(key));
                continue;
            }

            if (institutionMap.size() == 0)
            {
                List<Account> items =
                    [
                        SELECT Active__c, Name, Program__c, AcademicOrganisationalUnitCode__c, RecordTypeId, ParentId
                        FROM Account
                        WHERE RecordType.DeveloperName =: Constants.RECORDTYPE_ACCOUNT_INSTITUTION
                    ];

                for (Account item: items)
                    institutionMap.put(item.Name, item);
            }

            if (institutionMap.containsKey(key))
            {
                result.add(institutionMap.get(key));
                continue;
            }

            Account newItem = new Account
            (
                Active__c = true,
                Name = institution.Name,
                AcademicOrganisationalUnitCode__c = 'AOU',
                RecordTypeId = RecordTypeUtils.getRecordTypeId(Constants.RECORDTYPE_ACCOUNT_INSTITUTION, SObjectType.Account),
                parentId = institution.BusinessOrgId
            );

            institutionMap.put(key, newItem);
            inserts.add(newItem);
            result.add(newItem);
        }

        if (inserts.size() > 0)
        {
            if (user  != null)
            {
                System.RunAs(user)
                {
                    insert inserts;
                }
            }
            else
            {
                insert inserts;
            }
        }

        return result;
    }

    public static Account createInstitutionAIB(User user)
    {
        Account businessOrg = TestDataAccountMock.createBusinessOrg(user);

        return createInstitution(user, 'Australian Institute of Business', businessOrg.Id, 'https://www.aib.edu.au/');
    }
}