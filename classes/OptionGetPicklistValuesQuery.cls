public class OptionGetPicklistValuesQuery 
{
    private static final Set<String> useContactRecordTypePicklistValues = new Set<String> { 'Countries' };
    private static final Set<String> useOpportunityRecordTypePicklistValues = new Set<String> { 'PrimaryLocationDuringStudies' };
    private static final Set<String> useFacultyRecordTypePicklistValues = new Set<String> { 'SubjectEnrolmentRoles', 'SubjectEnrolmentStatuses' };

    private Map<String, String> listMap = getListMap();

    public ViewModels.PicklistOptions query(ViewModels.Param param)
    {    
        ViewModels.PicklistOptions result = new ViewModels.PicklistOptions();
        Log.info('OptionGetPicklistValuesQuery', 'query', 'Query invoked with following parameters ' + JSON.serialize(param));
        validate(param);

        result.Data = JSON.deserializeUntyped(JSON.serialize(getValues(param)));

        return result;
    }

    private Map<String, Object> getValues(ViewModels.Param param)
    {
        String errorMsg = '';
        List<String> paramLists;
        Map<String, Object> optionMap;
        Boolean isLightningModel = false;
        Map<string, Object> results = new Map<string, Object>();

        if (param != null && param.payload != null && param.payload != '{}')
        {
            optionMap = (Map<String, Object>)JSON.deserializeUntyped(param.Payload);
            paramLists = String.valueOf(optionMap.get('list')).split(',');

            if (optionMap.containsKey('isLightningModel'))
                isLightningModel = Boolean.valueOf(optionMap.get('isLightningModel'));
        }

        if (paramLists != null)
        {
            Map<String, String> contactOpportunityTypeMap = getContactOpportunityTypeMap();
            String paramType = String.valueOf(optionMap.get('type'));

            for (String paramListName : paramLists)
            {
                String recordType = '';
                String objectName = '';
                String listName = getListMapKey(paramListName);
                String fieldName = listMap.get(listName);

                if (fieldName.startsWithIgnoreCase(Constants.VIEWMODEL_CONTACTPREFIX))
                {
                    objectName = 'Contact';

                    if (useContactRecordTypePicklistValues.contains(listName))
                        recordType = paramType;
                }
                else if (fieldName.startsWithIgnoreCase(Constants.VIEWMODEL_OPPORTUNITYPREFIX))
                {
                    objectName = 'Opportunity';

                    if (useOpportunityRecordTypePicklistValues.contains(listName))
                        recordType = contactOpportunityTypeMap.get(paramType);
                }
                else if (fieldName.startsWithIgnoreCase(Constants.VIEWMODEL_SUBJECTEROLMENTPREFIX))
                {
                    objectName = 'hed__Course_Enrollment__c';

                    if (useFacultyRecordTypePicklistValues.contains(listName))
                        recordType = Constants.RECORDTYPE_SUBJECTENROLMENT_FACULTY.toLowerCase();
                }
                else
                {
                    errorMsg = String.format('Invalid object name prefix: {0}', new List<String> { fieldName });
                    Log.error('OptionGetPicklistValuesQuery', 'getValues', errorMsg);
                    throw new Exceptions.ApplicationException(errorMsg);
                }

                results.put(listName, getCodes(objectName, fieldName.substringAfter('_'), recordType, isLightningModel)) ;
            }
        }
        else 
        {
            for (String listName : listMap.keySet())
            {
                String recordType = '';
                String objectName = '';
                String fieldName = listMap.get(listName);

                if (fieldName.startsWithIgnoreCase(Constants.VIEWMODEL_CONTACTPREFIX))
                {
                    objectName = 'Contact';

                    if (useContactRecordTypePicklistValues.contains(listName))
                        recordType = Constants.RECORDTYPE_CONTACT_PROSPECT;
                }
                else if (fieldName.startsWithIgnoreCase(Constants.VIEWMODEL_OPPORTUNITYPREFIX))
                {
                    objectName = 'Opportunity';

                    if (useOpportunityRecordTypePicklistValues.contains(listName))
                        recordType = Constants.RECORDTYPE_OPPORTUNITY_ACQUISITION;
                }
                else if (fieldName.startsWithIgnoreCase(Constants.VIEWMODEL_SUBJECTEROLMENTPREFIX))
                {
                    objectName = 'hed__Course_Enrollment__c';

                    if (useFacultyRecordTypePicklistValues.contains(listName))
                        recordType = Constants.RECORDTYPE_SUBJECTENROLMENT_FACULTY.toLowerCase();
                }
                else
                {
                    errorMsg = String.format('Invalid object name prefix: {0}', new List<String> { fieldName });
                    Log.error('OptionGetPicklistValuesQuery', 'getValues', errorMsg);
                    throw new Exceptions.ApplicationException(errorMsg);
                }

                results.put(listName, getCodes(objectName, fieldName.substringAfter('_'), recordType, isLightningModel)) ;
            }
        }

        return results;
    }

    private List<Interfaces.IBaseCode> getCodes(String objectName, String fieldName, String recordType, Boolean isLightningModel)
    {
        Map<String, String> picklistValues = SObjectUtils.getPicklistValues(objectName, fieldName, recordType);

        if (picklistValues.size() == 0)
            return null;

        List<Interfaces.IBaseCode> codes = new List<Interfaces.IBaseCode>();
        
        if ((fieldName == 'Preferred_Start_Date__c' && picklistValues.size() <= 6) || fieldName != 'Preferred_Start_Date__c')
        {
            for (String name : picklistValues.keySet())
            {
                if (isLightningModel)
                {
                    ViewModels.LightningCode code = new ViewModels.LightningCode();
                    code.label = name;
                    code.value = picklistValues.get(name);
                    codes.add(code);
                }
                else 
                {
                    ViewModels.Code code = new ViewModels.Code();
                    code.Name = name;
                    code.Value = picklistValues.get(name);
                    codes.add(code);
                }
            }
        }
        else
        {
            Integer index = 0;
            for (String name : picklistValues.keySet())
            {
                if (index < 5 || (index == (picklistValues.size() - 1)))
                {
                    if (isLightningModel)
                    {
                        ViewModels.LightningCode code = new ViewModels.LightningCode();
                        code.label = name;
                        code.value = picklistValues.get(name);
                        codes.add(code);
                    }
                    else 
                    {
                        ViewModels.Code code = new ViewModels.Code();
                        code.Name = name;
                        code.Value = picklistValues.get(name);
                        codes.add(code);
                    }
                }
                index++;
            }
        }

        return codes;
    }

    private void validate(ViewModels.Param param)
    {
        Set<String> validRecordTypes = new Set<String>
        {
            Constants.RECORDTYPE_CONTACT_STUDENT.toLowerCase(), Constants.RECORDTYPE_CONTACT_PROSPECT.toLowerCase(), Constants.RECORDTYPE_SUBJECTENROLMENT_FACULTY.toLowerCase()
        };

        if (param != null && param.payload != null && param.payload != '{}')
        {
            Map<String, Object> optionMap = (Map<String, Object>)JSON.deserializeUntyped(param.Payload);

            for (String key : optionMap.keySet())
            {
                String errorMsg = '';

                if (key == 'list')
                {
                    List<String> lists = String.valueOf(optionMap.get(key)).split(',');

                    if (lists.size() == 0)
                    {
                        errorMsg = 'Unexpected number of list values. Expecting at list one list name';
                        Log.error('OptionGetPicklistValuesQuery', 'validate', errorMsg);
                        throw new Exceptions.ApplicationException(errorMsg);
                    }

                    for (String listName : lists)
                    {
                        if (!isInListMap(listName))
                        {
                            errorMsg = String.format('List name not supported: {0}', new String [] { listName });
                            Log.error('OptionGetPicklistValuesQuery', 'validate', errorMsg);
                            throw new Exceptions.ApplicationException(errorMsg);
                        }
                    }
                }
                else if (key == 'type')
                {
                    String type = String.valueOf(optionMap.get(key));

                    if (!validRecordTypes.contains(type.toLowerCase()))
                    {
                        errorMsg = String.format('Invalid record type: {0}. List of valid record types: {1}', new List<String> { type, JSON.serialize(validRecordTypes) });
                        Log.error('OptionGetPicklistValuesQuery', 'validate', errorMsg);
                        throw new Exceptions.ApplicationException(errorMsg);
                    }
                }
            }
        }
    }

    private boolean isInListMap(String listName)
    {
        for (String name : listMap.keySet())
        {
            if (name.toLowerCase().trim() == listName.toLowerCase().trim())
                return true;
        }

        return false;
    }

    private String getListMapKey(String listName)
    {
        for (String name : listMap.keySet())
        {
            if (name.toLowerCase().trim() == listName.toLowerCase().trim())
                return name;
        }

        return null;
    }

    private Map<String, String> getListMap()
    {
        return new Map<String, String>
        {
            'Salutations' => Constants.VIEWMODEL_CONTACTPREFIX + 'Salutation',
            'Genders' => Constants.VIEWMODEL_CONTACTPREFIX + 'hed__Gender__c',
            'HighestQualificationTypes' => Constants.VIEWMODEL_CONTACTPREFIX + 'Highest_Qualification__c',
            'PreferredTimeframes' => Constants.VIEWMODEL_CONTACTPREFIX + 'PreferredTimeframe__c',
            'ApplicationCourses' => Constants.VIEWMODEL_CONTACTPREFIX + 'ApplicationCourse__c',
            'PreferredStartDates' => Constants.VIEWMODEL_CONTACTPREFIX + 'Preferred_Start_Date__c',
            'Countries' => Constants.VIEWMODEL_CONTACTPREFIX + 'hed__Country_of_Origin__c',
            'VisaTypes' => Constants.VIEWMODEL_CONTACTPREFIX + 'Visa_Type__c',
            'Impairments' => Constants.VIEWMODEL_CONTACTPREFIX + 'Impairments__c',
            'HighSchoolLevels' => Constants.VIEWMODEL_CONTACTPREFIX + 'High_School_Level__c',
            'Languages' => Constants.VIEWMODEL_CONTACTPREFIX + 'Language_Spoken_at_Home__c',
            'FinacialSupportSources' => Constants.VIEWMODEL_OPPORTUNITYPREFIX + 'Financial_Support_Source__c',
            'AboriginalOrTorresStraitIslanderDescents' => Constants.VIEWMODEL_CONTACTPREFIX + 'Aboriginal_or_Torres_Strait_Islander_Des__c',
            'PrimaryMotivations' => Constants.VIEWMODEL_CONTACTPREFIX + 'PrimaryMotivations__c',
            'EnglishProficiencies' => Constants.VIEWMODEL_OPPORTUNITYPREFIX + 'English_Proficiency__c',
            'EnglishProficiencyTests' => Constants.VIEWMODEL_OPPORTUNITYPREFIX + 'English_Proficiency_Test__c',
            'HighestQualifications' => Constants.VIEWMODEL_CONTACTPREFIX + 'Highest_Qualification__c',
            'ParentEducationOptions' => Constants.VIEWMODEL_OPPORTUNITYPREFIX + 'Parent_Education_Options__c',
            'ParentHighestEducationalAttainments' => Constants.VIEWMODEL_OPPORTUNITYPREFIX + 'Parent_1_Highest_Educational_Attainment__c',
            'HighestTertiaryLevelDegrees' => Constants.VIEWMODEL_CONTACTPREFIX + 'HighestTertiaryLevelDegree__c',
            'HighSchoolStateTerritories' => Constants.VIEWMODEL_CONTACTPREFIX + 'High_School_State_Territory__c',
            'OtherAddressTypes' => Constants.VIEWMODEL_CONTACTPREFIX + 'OtherAddressType__c',
            'PermanentAddressTypes' => Constants.VIEWMODEL_CONTACTPREFIX + 'PermanentAddressType__c',
            'AustralianVisaHolders' => Constants.VIEWMODEL_CONTACTPREFIX + 'AustralianVisaHolder__c',
            'PrimaryLocationDuringStudies' => Constants.VIEWMODEL_OPPORTUNITYPREFIX + 'Primary_Location_During_Studies__c',
            'AgeRanges' => Constants.VIEWMODEL_CONTACTPREFIX + 'CalculatorAgeRange__c',
            'RemainingWorkYears' => Constants.VIEWMODEL_CONTACTPREFIX + 'CalculatorRemainingWorkYears__c',
            'SubjectEnrolmentRoles' => Constants.VIEWMODEL_SUBJECTEROLMENTPREFIX + 'Role__c',
            'SubjectEnrolmentStatuses' => Constants.VIEWMODEL_SUBJECTEROLMENTPREFIX + 'hed__Status__c',
            'TermMoveReasons' => Constants.VIEWMODEL_OPPORTUNITYPREFIX + 'TermMoveReason__c'
        };
    }

    private Map<String, String> getContactOpportunityTypeMap()
    {
        return new Map<String, String>
        {
            Constants.RECORDTYPE_CONTACT_PROSPECT.toLowerCase() => Constants.RECORDTYPE_OPPORTUNITY_ACQUISITION.toLowerCase(),
            Constants.RECORDTYPE_CONTACT_STUDENT.toLowerCase() => Constants.RECORDTYPE_OPPORTUNITY_PROGRESSION.toLowerCase()
        };
    }
}