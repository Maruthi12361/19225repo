public class AffiliationService
{
    public static ActionResult updateHighestQualification(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        return new AffiliationSetQualificationCommand().execute(items, oldMap, triggerContext);
    }
}