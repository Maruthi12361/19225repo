public class ContactReferrerSetLeadSourceCommand extends BaseTriggerCommand
{
    public override List<SObject> validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        return items; 
    }

    public override ActionResult command(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        List<LeadSourceConfig__c> configs = getLeadConfigs();
        Set<Id> referrerIds = SObjectUtils.getIDs(items, 'ID');
        List<ContactReferrer__c> referrers = getContactReferrers(referrerIds);
        List<Contact> contactsToUpdate = new List<Contact>();

        Log.info(this.className, 'command', 'Setting Lead Source field value for following Contact Referrers: ' + JSON.serialize(referrerIds));

        for (ContactReferrer__c referrer : referrers)
        {
            if (String.isEmpty(referrer.LeadSource__c))
                setLeadSourceValue(configs, referrer);

            if ((referrer.ContactID__r.LeadSource == null || referrer.ContactID__r.LeadSource == 'Unknown') && !String.isEmpty(referrer.LeadSource__c))
            {
                Log.info(this.className, 'command', 'Updating contact {0} Lead Status to {1}', new List<String> { referrer.ContactID__c, referrer.LeadSource__c });
                referrer.ContactID__r.LeadSource = referrer.LeadSource__c;
                contactsToUpdate.add(referrer.ContactID__r);
            }
        }

        Database.update(referrers);

        if (contactsToUpdate.size() > 0)
            uow.registerDirty(contactsToUpdate);

        return new ActionResult(ResultStatus.SUCCESS);
    }

    private List<ContactReferrer__c> getContactReferrers(Set<Id> ids)
    {
        return 
            [
                SELECT
                    Id, utm_campaign__c, utm_content__c, utm_gclid__c, utm_keyword__c, utm_medium__c, utm_source__c, utm_term__c,
                    LeadSource__c, ContactID__c, ContactID__r.PersonalEmail__c, ContactID__r.OriginalReferrerSource__c, ContactID__r.LeadSource,
                    utm_referralSource__c
                FROM ContactReferrer__c
                WHERE Id IN :ids
            ];
    }

    private List<LeadSourceConfig__c> getLeadConfigs()
    {
        return 
        [
            SELECT 
                Id, Name, EmailOperator__c, EmailValue__c, GclidOperator__c, GclidValue__c,  ReferrerOperator__c, ReferrerValue__c, DisplayOrder__c, 
                UtmCampaignOperator__c, UtmCampaignValue__c, UtmContentOperator__c, UtmContentValue__c, UtmKeywordOperator__c, UtmKeywordValue__c,
                UtmMediumOperator__c, UtmMediumValue__c, UtmSourceOperator__c, UtmSourceValue__c, UtmTermOperator__c, UtmTermValue__c, Value__c, 
                Active__c, UtmReferralSourceOperator__c, UtmReferralSourceValue__c
            FROM 
                LeadSourceConfig__c
            WHERE Active__c = True
            ORDER BY DisplayOrder__c
        ];
    }

    private List<LeadSourceConfig__c> getSourceConfigs(List<LeadSourceConfig__c> configs, String source)
    {
        List<LeadSourceConfig__c> result = new List<LeadSourceConfig__c>();

        for (LeadSourceConfig__c config : configs)
        {
            if (!String.isEmpty(config.UtmSourceValue__c))
            {
                for (String utmSource : config.UtmSourceValue__c.split(','))
                {
                    if (StringUtils.equalIgnoreCase(utmSource, source))
                        result.add(config);
                }
            }
            else 
            {
                if (StringUtils.equalIgnoreCase(config.UtmSourceValue__c, source))
                    result.add(config);
            }
        }

        return result;
    }

    private void setLeadSourceValue(List<LeadSourceConfig__c> configs, ContactReferrer__c referrer)
    {
        List<LeadSourceConfig__c> leadSourceConfigs = getSourceConfigs(configs, referrer.utm_source__c);

        for (LeadSourceConfig__c config : leadSourceConfigs)
        {
            if (hasMatchingRule(config, referrer))
            {
                Log.info(this.className, 'setLeadSourceValue', 'Matching rule: {0}. Setting value to: {1}', new List<String>{ config.Name, config.Value__c });
                referrer.LeadSource__c = config.Value__c;
                break;
            }
        }
    }

    private Boolean hasMatchingRule(LeadSourceConfig__c config, ContactReferrer__c referrer) 
    {
        Boolean result = false;

        if (!String.isEmpty(config.EmailOperator__c))
        {
            if (!StringUtils.isLogicalStatementTrue(config.EmailOperator__c, referrer.ContactID__r.PersonalEmail__c, config.EmailValue__c))
                return false;

            result = true;
        }

        if (!String.isEmpty(config.GclidOperator__c))
        {
            if (!StringUtils.isLogicalStatementTrue(config.GclidOperator__c, referrer.utm_gclid__c, config.GclidValue__c))
                return false;

            result = true; 
        }

        if (!String.isEmpty(config.ReferrerOperator__c))
        {
            if (!StringUtils.isLogicalStatementTrue(config.ReferrerOperator__c, referrer.ContactID__r.OriginalReferrerSource__c, config.ReferrerValue__c))
                return false;

            result = true;
        }

        if (!String.isEmpty(config.UtmCampaignOperator__c))
        {
            if (!StringUtils.isLogicalStatementTrue(config.UtmCampaignOperator__c, referrer.utm_campaign__c, config.UtmCampaignValue__c))
                return false;

            result = true;
        }

        if (!String.isEmpty(config.UtmContentOperator__c))
        {
            if (!StringUtils.isLogicalStatementTrue(config.UtmContentOperator__c, referrer.utm_content__c, config.UtmContentValue__c))
                return false;

            result = true;
        }

        if (!String.isEmpty(config.UtmKeywordOperator__c))
        {
            if (!StringUtils.isLogicalStatementTrue(config.UtmKeywordOperator__c, referrer.utm_keyword__c, config.UtmKeywordValue__c))
                return false;

            result = true;
        }

        if (!String.isEmpty(config.UtmMediumOperator__c))
        {
            if (!StringUtils.isLogicalStatementTrue(config.UtmMediumOperator__c, referrer.utm_medium__c, config.UtmMediumValue__c))
                return false;

            result = true;
        }

        if (!String.isEmpty(config.UtmSourceOperator__c))
        {
            if (!StringUtils.isLogicalStatementTrue(config.UtmSourceOperator__c, referrer.utm_source__c, config.UtmSourceValue__c))
                return false;

            result = true;
        }

        if (!String.isEmpty(config.UtmTermOperator__c))
        {
            if (!StringUtils.isLogicalStatementTrue(config.UtmTermOperator__c, referrer.utm_term__c, config.UtmTermValue__c))
                return false;

            result = true;
        }

        if (!String.isEmpty(config.UtmReferralSourceOperator__c))
        {
            if (!StringUtils.isLogicalStatementTrue(config.UtmReferralSourceOperator__c, referrer.utm_referralSource__c, config.UtmReferralSourceValue__c))
                return false;

            result = true;
        }

        return result;
    }
}