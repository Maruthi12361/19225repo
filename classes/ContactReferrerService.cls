public class ContactReferrerService 
{
    public static ActionResult setLeadSourceValue(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        return new ContactReferrerSetLeadSourceCommand().execute(items, oldMap, triggerContext);
    }

    public static ActionResult create(ViewModelsAcquisition.ContactReferrer referrer)
    {
        return new ContactReferrerSaveCommand().execute(referrer);
    }
}