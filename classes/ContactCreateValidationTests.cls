@isTest
public class ContactCreateValidationTests
{
    @testSetup
    static void setup()
    {
        TestUtilities.setupHEDA();
    }

    @isTest
    static void ValidateAccountWhenCreatingContact_ThrowsError()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createProspectCold(user, 'John', 'Test','61408815278', 'john.test@yahoo.com', '');
        Account account = TestDataAccountMock.createFromContact(user, contact);
        contact.AccountId = account.Id;
        update contact;
        
        contact = TestDataContactMock.createProspectCold(user, 'Nick', 'Test','61408815279', 'nick.test@yahoo.com', '');
        contact.AccountId = account.Id;

        // Act
        Test.startTest();
        ActionResult result = ContactService.validateAccount(new List<Contact> { contact }, new Map<Id, Contact>(), Enums.TriggerContext.AFTER_INSERT);
        Test.stopTest();
        
        // Assert
        System.assertEquals(ResultStatus.ERROR, result.status);
        System.assertEquals(true, result.message.contains('There is already an existing Contact related to the selected administrative account'));
    }
    
    @isTest
    static void ValidateAccountWhenChangingAccountOfExistingContact_ThrowsError()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createProspectCold(user, 'John', 'Test','61408815278', 'john.test@yahoo.com', '');
        Account account = TestDataAccountMock.createFromContact(user, contact);
        contact.AccountId = account.Id;
        update contact;
        
        contact = TestDataContactMock.createProspectCold(user, 'Nick', 'Test','61408815279', 'nick.test@yahoo.com', '');
        Contact newContact = contact.clone(true, false, true, true);
        
        newContact.AccountId = account.Id;        
        List<Contact> items = new List<Contact>{ newContact };
        Map<Id, Contact> oldMap = new Map<Id, Contact>();
        oldMap.put(contact.Id, contact);

        // Act
        Test.startTest();
        ActionResult result = ContactService.validateAccount(items, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        Test.stopTest();
        
        // Assert
        System.assertEquals(ResultStatus.ERROR, result.status);
        System.assertEquals(true, result.message.contains('There is already an existing Contact related to the selected administrative account'));
    }
}