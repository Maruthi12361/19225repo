public class ContactDeleteCommand extends BaseIdCommand
{
    List<Account> contactAccounts = new List<Account>();

    protected override Set<ID> validate(Set<ID> items)
    {
        return items;
    }

    protected override ActionResult command(Set<ID> items)
    {
        List<Contact> contacts = getContacts(items);
        Set<Id> accountIds = getContactAccountIds(contacts);

        if (accountIds.size() > 0)
            this.contactAccounts = getContactAccounts(accountIds);

        for (Contact c : contacts)
        {
            Boolean toDeleteAccount = false;
            if (c.Account != null)
            {
                Account contactAcc = getAccount(c.Account.Id);
                if (contactAcc == null)
                    throw new Exceptions.ApplicationException('Failed to retrive an account from db. AccountID: ' + c.Account.Id);

                if (c.Account.hed__Primary_Contact__c != null)
                    if (c.Account.hed__Primary_Contact__c == c.Id)
                    {
                        Log.info(this.className, 'command', 'Removing Reciprocal contact {0} from {1} account', 
                            new List<String> { c.Account.hed__Primary_Contact__c, c.Account.Id });
                        c.Account.hed__Primary_Contact__c = null;
                        uow.registerDirty(c.Account);
                    }

                 toDeleteAccount = shouldDeleteAccount(c, contactAcc);   
            }

            if (c.Cases.size() > 0)
            {
                delete c.Cases; // Had to commit deltion of opportunities now
                Log.info(this.className, 'command', 'Deleting following cases: {0}', new List<String> { JSON.serializePretty(c.Cases) });
            }

            if (c.Opportunities__r.size() > 0)
            {
                delete c.Opportunities__r; // Had to commit deletion of opportunities now
                Log.info(this.className, 'command', 'Deleting following opportunities: {0}', new List<String> { JSON.serializePretty(c.Opportunities__r) });
            }

            if (c.ContactReferrers__r.size() > 0)
            {
                delete c.ContactReferrers__r;
                Log.info(this.className, 'command', 'Deleting following contact referrals: {0}', new List<String> { JSON.serializePretty(c.ContactReferrers__r) });
            }

            uow.registerDeleted(c);
            Log.info(this.className, 'command', 'Deleting {0} contact', new List<String> { c.Id });
 
            if (toDeleteAccount)
            {
                uow.registerDeleted(c.Account);
                Log.info(this.className, 'command', 'Deleting {0} account', new List<String> { c.Account.Id });
            }
        }

        return new ActionResult();
    }

    private List<Contact> getContacts(Set<ID> ids)
    {
        try
        {
            String sql = 
                'SELECT ' + 
                    'Id, Account.Id, Account.hed__Primary_Contact__c, ' +
                    '( ' +
                        'SELECT Id ' +
                        'FROM Cases' +
                    '), ' +
                    '( ' +
                        'SELECT Id ' +
                        'FROM Opportunities__r ' +
                    '), ' +
                    '( ' +
                        'SELECT Id '+
                        'FROM ContactReferrers__r '+
                    ')' +
                'FROM Contact ' +
                'WHERE Id in :ids';

            return Database.query(sql);
        }
        catch(Exception ex)
        {
            Log.error(this.className, 'getUsers', 'Failed to retrive contacts from db with following ids' + JSON.serializePretty(ids));
            throw ex;
        }
    }

    private List<Account> getContactAccounts(Set<Id> accountIds)
    {
        String sql = 
            'SELECT ' +
                'Id, hed__Primary_Contact__c, ' +
                '( ' +
                    'SELECT Id ' +
                    'FROM Cases' +
                '), ' +
                '( ' +
                    'SELECT Id ' +
                    'FROM Contacts' +
                '), ' +
                '( ' +
                    'SELECT Id ' +
                    'FROM Opportunities' +
                ') ' +
            'FROM Account ' + 
            'WHERE Id in :accountIds';

        return Database.query(sql);
    }

    private Account getAccount(Id accountId)
    {
        for (Account a : this.contactAccounts)
            if (a.Id == accountId)
                return a;

        return null;
    }

    private Set<Id> getContactAccountIds(List<Contact> contacts)
    {
        Set<Id> ids = new Set<Id>();
        for (Contact c : contacts)
            if (c.Account != null)
                ids.add(c.Account.Id);

        return ids;
    }

    private Boolean shouldDeleteAccount(Contact c, Account acc)
    {
        Set<Id> accountContacts = SObjectUtils.getIds(acc.Contacts, 'ID');

        if (accountContacts.size() > 1)
            return false; // there is more then one associated contact with account

        Set<ID> contactCases = SObjectUtils.getIds(c.Cases, 'ID');
        Set<ID> accountCases = SObjectUtils.getIds(acc.Cases, 'ID');

        if (accountCases.size() > contactCases.size())
            return false; // there are more account cases then contact's
        else if (accountCases.size() == contactCases.size() && !accountCases.containsAll(contactCases))
            return false; // not all of the contact cases belong to this account
        else if (!contactCases.containsAll(accountCases))
            return false; // not all of the account cases belong to this contact

        Set<ID> contactOpportunities = SObjectUtils.getIds(c.Opportunities__r, 'ID');
        Set<ID> accountOpportunities = SObjectUtils.getIds(acc.Opportunities, 'ID');

        if (accountOpportunities.size() > contactOpportunities.size())
            return false; // there are more account opportunities then contact's
        else if (accountOpportunities.size() == contactOpportunities.size() && !accountOpportunities.containsAll(contactOpportunities))
            return false; // not all of the contact opportunities belong to this account
        else if (!contactOpportunities.containsAll(accountOpportunities))
            return false; // not all of the account opportunities belong to this contact

        return true;
    }
}