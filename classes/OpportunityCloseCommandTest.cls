@isTest
public class OpportunityCloseCommandTest
{
    @testSetup
    public static void setup()
    {
        TestUtilities.setupHEDA();
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createProspectCold(user, '04045566889');
        Opportunity Opportunity  = TestDataOpportunityMock.createCommencing(user, contact);
    }

    @isTest
    static void OpportunityCloseCommandTest_NoCourses()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact student = [SELECT Id FROM Contact LIMIT 1];
        Account course = TestDataAccountMock.createCourseMBA(user);
        List<Opportunity> opps = getOpportunities();

        // Act
        test.startTest();
        ActionResult result = new OpportunityCloseCommand().execute(opps);
        Opportunity updatedOpportunity = [SELECT Id, StageName, Lost_Reason__c FROM Opportunity limit 1];
        test.stopTest();

        // Assert
        System.assertNotEquals(null, updatedOpportunity, 'Opportunit shouldn\'t be null');
        System.assertEquals('Commencing', updatedOpportunity.StageName, 'Opportunity stage shouldn\'t be changed');
    }

    @isTest
    static void OpportunityCloseCommandTest_NoActiveCourses()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact student = [SELECT Id FROM Contact LIMIT 1];
        Account course = TestDataAccountMock.createCourseMBA(user);
        hed__Program_Enrollment__c courseEnrolment = TestDataCourseEnrolmentMock.create(user, course.Id, student.Id, 'Completed');
        List<Opportunity> opps = getOpportunities();

        // Act
        test.startTest();
        ActionResult result = new OpportunityCloseCommand().execute(opps);
        Opportunity updatedOpportunity = [SELECT Id, StageName, Lost_Reason__c FROM Opportunity limit 1];
        test.stopTest();

        // Assert
        System.assertNotEquals(null, updatedOpportunity, 'Opportunit shouldn\'t be null');
        System.assertEquals('Closed Lost', updatedOpportunity.StageName, 'Opportunity stage should be set to Close Lost');
        System.assertEquals('Withdrawn Before Census', updatedOpportunity.Lost_Reason__c, 'Opportunity lost reason should be set to Withdrawn Before Census');
    }

    @isTest
    static void OpportunityCloseCommandTest_ActiveCourseNoEnrolments()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact student = [SELECT Id FROM Contact LIMIT 1];
        Account course = TestDataAccountMock.createCourseMBA(user);
        hed__Program_Enrollment__c courseEnrolment = TestDataCourseEnrolmentMock.create(user, course.Id, student.Id, 'Approved');
        List<Opportunity> opps = getOpportunities();

        // Act
        test.startTest();
        ActionResult result = new OpportunityCloseCommand().execute(opps);
        Opportunity updatedOpportunity = [SELECT Id, StageName FROM Opportunity limit 1];
        test.stopTest();

        // Assert
        System.assertNotEquals(null, updatedOpportunity, 'Opportunit shouldn\'t be null');
        System.assertEquals('Commencing', updatedOpportunity.StageName, 'Opportunity stage shouldn\'t be changed');
    }

    @isTest
    static void OpportunityCloseCommandTest_ActiveCourseEnrolmentsFutureCensusDate()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact student = [SELECT Id FROM Contact LIMIT 1];
        Account course = TestDataAccountMock.createCourseMBA(user);
        TestDataCourseEnrolmentMock.create(user, course.Id, student.Id, 'Approved');
        course = TestDataAccountMock.createCourseGCM(user);
        hed__Program_Enrollment__c courseEnrolment = TestDataCourseEnrolmentMock.create(user, course.Id, student.Id, 'Ongoing');
        TestDataSubjectEnrolmentMock.create(user, courseEnrolment, 'TEST0001', 'Test Subject One', Date.today().addDays(-10));
        List<Opportunity> opps = getOpportunities();

        // Act
        test.startTest();
        ActionResult result = new OpportunityCloseCommand().execute(opps);
        Opportunity updatedOpportunity = [SELECT Id, StageName FROM Opportunity limit 1];
        test.stopTest();

        // Assert
        System.assertNotEquals(null, updatedOpportunity, 'Opportunit shouldn\'t be null');
        System.assertEquals('Commencing', updatedOpportunity.StageName, 'Opportunity stage shouldn\'t be changed');
    }

    @isTest
    static void OpportunityCloseCommandTest_ActiveCourseEnrolmentsOnCensusDateNoGrade()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact student = [SELECT Id FROM Contact LIMIT 1];
        Account course = TestDataAccountMock.createCourseMBA(user);
        hed__Program_Enrollment__c courseEnrolment = TestDataCourseEnrolmentMock.create(user, course.Id, student.Id, 'Ongoing');
        TestDataSubjectEnrolmentMock.create(user, courseEnrolment, 'TEST0001', 'Test Subject One', Date.today().addDays(-11));
        List<Opportunity> opps = getOpportunities();

        // Act
        test.startTest();
        ActionResult result = new OpportunityCloseCommand().execute(opps);
        Opportunity updatedOpportunity = [SELECT Id, StageName FROM Opportunity limit 1];
        test.stopTest();

        // Assert
        System.assertNotEquals(null, updatedOpportunity, 'Opportunit shouldn\'t be null');
        System.assertEquals('Closed Won', updatedOpportunity.StageName, 'Opportunity stage should be Closed Won');
    }

    @isTest
    static void OpportunityCloseCommandTest_ActiveCourseEnrolmentsPastCensusDateNoGrade()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact student = [SELECT Id FROM Contact LIMIT 1];
        Account course = TestDataAccountMock.createCourseMBA(user);
        hed__Program_Enrollment__c courseEnrolment = TestDataCourseEnrolmentMock.create(user, course.Id, student.Id, 'Ongoing');
        TestDataSubjectEnrolmentMock.create(user, courseEnrolment, 'TEST0001', 'Test Subject One', Date.today().addDays(-12));
        List<Opportunity> opps = getOpportunities();

        // Act
        test.startTest();
        ActionResult result = new OpportunityCloseCommand().execute(opps);
        Opportunity updatedOpportunity = [SELECT Id, StageName FROM Opportunity limit 1];
        test.stopTest();

        // Assert
        System.assertNotEquals(null, updatedOpportunity, 'Opportunit shouldn\'t be null');
        System.assertEquals('Closed Won', updatedOpportunity.StageName, 'Opportunity stage should be Closed Won');
    }

    @isTest
    static void OpportunityCloseCommandTest_ActiveCourseEnrolmentsPastCensusDateInvalidGrade()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact student = [SELECT Id FROM Contact LIMIT 1];
        Account course = TestDataAccountMock.createCourseMBA(user);
        hed__Program_Enrollment__c courseEnrolment = TestDataCourseEnrolmentMock.create(user, course.Id, student.Id, 'Ongoing');
        TestDataSubjectEnrolmentMock.create(user, courseEnrolment, 'TEST001', 'Test Subject One', Date.today().addDays(-12), 'Withdrawn', 'Standard', 'Withdraw Not Fail', null);
        List<Opportunity> opps = getOpportunities();

        // Act
        test.startTest();
        ActionResult result = new OpportunityCloseCommand().execute(opps);
        Opportunity updatedOpportunity = [SELECT Id, StageName FROM Opportunity limit 1];
        test.stopTest();

        // Assert
        System.assertNotEquals(null, updatedOpportunity, 'Opportunit shouldn\'t be null');
        System.assertEquals('Commencing', updatedOpportunity.StageName, 'Opportunity stage shouldn\'t be changed');
    }

    @isTest
    static void OpportunityCloseCommandTest_ActiveCourseEnrolmentsPastCensusDateExemptions()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact student = [SELECT Id FROM Contact LIMIT 1];
        Account course = TestDataAccountMock.createCourseMBA(user);
        hed__Program_Enrollment__c courseEnrolment = TestDataCourseEnrolmentMock.create(user, course.Id, student.Id, 'Ongoing');
        TestDataSubjectEnrolmentMock.create(user, courseEnrolment, 'TEST001', 'Test Subject One', Date.today().addDays(-12), 'Completed', 'Exemption', 'Exemption', null);
        List<Opportunity> opps = getOpportunities();

        // Act
        test.startTest();
        ActionResult result = new OpportunityCloseCommand().execute(opps);
        Opportunity updatedOpportunity = [SELECT Id, StageName FROM Opportunity limit 1];
        test.stopTest();

        // Assert
        System.assertNotEquals(null, updatedOpportunity, 'Opportunity shouldn\'t be null');
        System.assertEquals('Commencing', updatedOpportunity.StageName, 'Opportunity stage shouldn\'t be changed');
    }

    @isTest
    static void OpportunityCloseCommandTest_ActiveCourseEnrolmentsPastCensusDateGrade()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact student = [SELECT Id FROM Contact LIMIT 1];
        Account course = TestDataAccountMock.createCourseMBA(user);
        hed__Program_Enrollment__c courseEnrolment = TestDataCourseEnrolmentMock.create(user, course.Id, student.Id, 'Ongoing');
        TestDataSubjectEnrolmentMock.create(user, courseEnrolment, 'TEST001', 'Test Subject One', Date.today().addDays(-12), 'In Progress', 'Standard', null, null);
        List<Opportunity> opps = getOpportunities();

        // Act
        test.startTest();
        ActionResult result = new OpportunityCloseCommand().execute(opps);
        Opportunity updatedOpportunity = [SELECT Id, StageName FROM Opportunity limit 1];
        test.stopTest();

        // Assert
        System.assertNotEquals(null, updatedOpportunity, 'Opportunit shouldn\'t be null');
        System.assertEquals('Closed Won', updatedOpportunity.StageName, 'Opportunity stage should be Closed Won');
    }

    @isTest
    static void OpportunityCloseCommandTest_UnmetPrerequisitesNotTriggerValidationRuleForAdminUser()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact student = [SELECT Id FROM Contact LIMIT 1];
        Account course = TestDataAccountMock.createCourseMBA(user);
        hed__Program_Enrollment__c courseEnrolment = TestDataCourseEnrolmentMock.create(user, course.Id, student.Id, 'Ongoing');
        TestDataSubjectEnrolmentMock.create(user, courseEnrolment, 'TEST001', 'Test Subject One', Date.today().addDays(-12), 'In Progress', 'Standard', null, null);
        List<Opportunity> opps = getOpportunities();

        // Act
        test.startTest();
        System.RunAs(UserService.getByAlias(Constants.USER_SYSTEMADMIN))
        {
            ActionResult result = new OpportunityCloseCommand().execute(opps);
        }
        Opportunity updatedOpportunity = [SELECT Id, StageName, Offer_Documentation_Sent__c FROM Opportunity limit 1];
        test.stopTest();

        // Assert
        System.assertNotEquals(null, updatedOpportunity, 'Opportunit shouldn\'t be null');
        System.assertEquals('Closed Won', updatedOpportunity.StageName, 'Opportunity stage should be Closed Won');
    }

    private static List<Opportunity> getOpportunities()
    {
        String query = 'SELECT Id, StageName, Lost_Reason__c, Contact__r.Cerebro_Student_ID__c FROM Opportunity Limit 1';

        return Database.query(query);
    }
}