@isTest
public class TestDataCourseEnrolmentMock
{
    public static hed__Program_Enrollment__c create(User user, Id courseId, Id contactId, String status)
    {
        return create(user, courseId, contactId, status, null, null);
    }

    public static hed__Program_Enrollment__c create(User user, Id courseId, Id contactId, String status, Id coursePlanId)
    {
        return create(user, courseId, contactId, status, null, null, coursePlanId);
    }

    public static hed__Program_Enrollment__c create(User user, Id courseId, Id contactId, String status, String stageStatus, Date followUpDate)
    {
        return create(user, courseId, contactId, status, stageStatus, followUpDate, null);
    }

    public static hed__Program_Enrollment__c create(User user, Id courseId, Id contactId, String status, String stageStatus, Date followUpDate, Id coursePlanId)
    {
        hed__Program_Enrollment__c courseEnrolment = new hed__Program_Enrollment__c
        (
            hed__Contact__c = contactId,
            hed__Account__c = courseId,            
            CurrentStageStatus__c = stageStatus,
            NextFollowUpDate__c = followUpDate,
            hed__Start_Date__c = Date.today(),
            FeeHelpEligibilityType__c = RolloverConstantsDefinition.FEEHELP_FHE,
            CourseEntryBasis__c = 'MBA Direct Entry',
            hed__Program_Plan__c = coursePlanId
        );
        String[] convertedStatus = getCourseEnrolmentStatus(status);
        courseEnrolment.Status__c = convertedStatus[0];
        courseEnrolment.StatusResultReason__c = convertedStatus[1];
        
        System.RunAs(user)
        {
            insert courseEnrolment;
        }
        
        return courseEnrolment;
    }
    
    public static String[] getCourseEnrolmentStatus(String status)
    {
        String result, reason;

        if (!String.isBlank(status))
        {
            if (status == 'Provisionally Approved' || status == 'Approved')
                result = 'Approved';
            else if (status == 'Transferred' || status == 'Completed')
            {
                result = 'Completed';
                reason = (status == 'Transferred' ? 'Transferred' : 'Awarded');
            }
            else if (status == 'Withdrawn' || status == 'Expired' || status == 'Excluded' || status == 'Expelled')
            {
                result = 'Incomplete';
                reason = status;
            }
            else if (status == 'Expired Approved' || status == 'Expired Provisionally Approved')
            {
                result = 'Incomplete';
                reason = 'Expired Approved';
            }
            else
                result = status;
        }

        return new String[] { result, reason };
    }
}