public class ContactSumBehaviourDecayBatch extends BaseBatchCommand
{
    List<InterestingMoment__c> interestingMoments;

    public override Database.QueryLocator query(Database.BatchableContext BC) 
    {
        return Database.getQueryLocator(getQuery());
    }

    public override void command(Database.BatchableContext BC, List<sObject> scope) 
    {
        interestingMoments = getInterestingMoments(SObjectUtils.getIDs(scope, 'ID'));

        for (Contact contact : (List<Contact>) scope)
        {
            List<InterestingMoment__c> moments = getContactMoments(contact.Id);

            if (moments.size() > 0 )
            {
                Double totalBehaviourDecay = 0;

                for (InterestingMoment__c moment : moments)
                    totalBehaviourDecay += moment.BehaviourDecay__c;

                if (contact.BehaviourDecaySum__c != totalBehaviourDecay)
                {
                    contact.BehaviourDecaySum__c = totalBehaviourDecay;
                    uow.registerDirty(contact);
                }
            }
        }
    }

    private String getQuery()
    {
        return 
            'SELECT Id, BehaviourDecaySum__c ' +
            'FROM Contact ' +
            'WHERE IsDecayLessThenScore__c = True';
    }

    private List<InterestingMoment__c> getInterestingMoments(Set<ID> contactIds)
    {
        return [SELECT Id, BehaviourDecay__c, ContactID__c FROM InterestingMoment__c WHERE ContactID__c in :contactIds];
    }

    private List<InterestingMoment__c> getContactMoments(Id contactId)
    {
        List<InterestingMoment__c> moments = new List<InterestingMoment__c>();

        for (InterestingMoment__c moment : interestingMoments)
            if (moment.ContactID__c == contactId)
                moments.add(moment);

        return moments;
    }
}