public class OnlineApplicationController
{
    public string RedirectToURL
    {
        get
        {
            String encryptedId = ApexPages.currentPage().getParameters().get('oid');
            String encodedId = EncodingUtil.urlEncode(encryptedId, 'UTF-8');
            String currentURL = URL.getSalesforceBaseUrl().toExternalForm() + ApexPages.currentPage().getUrl().replace('/apex/', '/');

            User user = UserService.get(UserInfo.getUserId());

            if (user.Profile.Name != 'OAF Profile')
                return null;

            if (currentURL.startsWith(System.Label.OAFBaseURL + 'online-application/?id='))
                return null;
            
            return System.Label.OAFBaseURL + 'online-application/?id=' + encodedId;
        }
    }
}