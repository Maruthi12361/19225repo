public class Constants
{
    public static final String ACCOUNT_INSTITUTION_AIB = 'Australian Institute of Business';
    public static final String ACCOUNT_DEPARTMENT_AIBBUSINESS = 'AIB School of Business Administration';
    public static final String ACCOUNT_DEPARTMENT_AIBRESEARCH = 'AIB School of Research & Higher Degrees';

    public static final String ENTITYID_CASE = '500';
    public static final String ENTITYID_CONTACT = '003';
    public static final String ENTITYID_OPPORTUNITY = '006';
    public static final String ENTITYID_USER = '005';
    public static final String ENTITYID_TASK = '00T';
    public static final String ENTITYID_TERM = 'a4a';
    public static final String ENTITYID_COURSE_OFFERING = hed__Course_Offering__c.sObjectType.getDescribe().getKeyPrefix();

    public static final String USER_COURSEADVISOR = 'CA_guest';
    public static final String USER_DATALOAD = 'data';
    public static final String USER_NON_ADMIN = 'reilly.e';
    public static final String USER_NON_ADMIN2 = 'nathan.w';
    public static final String USER_SYNC = 'AIBSync';
    public static final String USER_SYSTEMADMIN = 'salesfor';
    public static final String USER_TESTSETUP = 'data';
    public static final String USER_CEREBROSYNC = 'crbsync';

    public static final Id USERID_AUTOPROC = '00536000000ojB1AAI';
    public static final Id USERID_CAREERFAQ = '0053600000GTGgzAAH';
    public static final Id USERID_CAREERONE = '0053600000GTG2LAAX';
    public static final Id USERID_NVM_ADMINISTRATOR = '00536000005SHv3AAG';
    public static final Id USERID_OAF = '00536000008TX63AAG';
    public static final Id USERID_ROUNDROBIN = '0052e000000LS1MAAW';

    public static final Id QUEUEID_FINANCE_TEAM = '00G36000001MJYeEAO';

    public static final String GROUP_COURSE_ADVISORS = 'CourseAdvisor';
    public static final String GROUP_COURSE_CONSULTANTS = 'CourseConsultant';
    public static final String GROUP_HEALTH_CHECK_TEAM = 'HealthCheckTeam';

    public static final String PROFILE_CPQINTEGRATION = 'CPQ Integration User';
    public static final String PROFILE_AIBAPIUSER = 'AIB API User';
    public static final String PROFILE_STUDENTPORTAL = 'AIB Student Portal Login User';

    public static final String PERMISSION_DATALOAD_BYPASS = 'DataLoadBypass';

    public static final String AIB_DL = 'AIB Distance Learning';
    public static final String AIB_LOCAL = 'AIB Local';

    public static final Integer HTTPSTATUS_OK = 200;
    public static final Integer HTTPSTATUS_CREATE = 201;
    public static final Integer HTTPSTATUS_ACCEPTED = 202;
    public static final Integer HTTPSTATUS_NOCONTENT = 204;
    public static final Integer HTTPSTATUS_BADREQUEST = 400;
    public static final Integer HTTPSTATUS_NOTFOUND = 404;
    public static final Integer HTTPSTATUS_SERVICEUNAVAILABLE = 503;

    public static final String HTTPMETHOD_GET = 'GET';
    public static final String HTTPMETHOD_POST = 'POST';
    public static final String HTTPMETHOD_PUT = 'PUT';
    public static final String HTTPMETHOD_DELETE = 'DELETE';

    public static final String COUNTRYCODE_AUSTRALIA = 'AU';

    public static final String STUDENTCENTRAL_EMAIL = 'studentcentral@aib.com.au';
    public static final String STUDENTCENTRAL_NAME = 'AIB Student Central';

    // Contact Status
    public static final String STATUS_HOT = 'Hot';
    public static final String STATUS_COLD = 'Cold';
    public static final String STATUS_WARM = 'Warm';
    public static final String STATUS_DISQUALIFIED = 'Disqualified';
    public static final String STATUS_PROGRESSION = 'Progression';

    // Cerebro Course Student Status
    public static final String CEREBRO_CSS_ONGOING = 'Ongoing';
    public static final String CEREBRO_CSS_EXPIRED = 'Expired';
    public static final String CEREBRO_CSS_APPROVED = 'Approved';
    public static final String CEREBRO_CSS_EXPELLED = 'Expelled';
    public static final String CEREBRO_CSS_EXCLUDED = 'Excluded';
    public static final String CEREBRO_CSS_SUSPENDED = 'Suspended';
    public static final String CEREBRO_CSS_COMPLETED = 'Completed';
    public static final String CEREBRO_CSS_WITHDRAWN = 'Withdrawn';
    public static final String CEREBRO_CSS_PROVISIONALLYAPPROVED = 'Provisionally Approved';
    public static final String CEREBRO_CSS_EXPIREDPROVISIONALLYAPPROVED = 'Expired Provisionally Approved';

    // Cerebro Grade Codes
    public static final String CEREBRO_GRADE_WNF = 'WNF';
    public static final String CEREBRO_GRADE_MC = 'MC';
    public static final String CEREBRO_GRADE_WF = 'WF';
    public static final String CEREBRO_GRADE_DNS = 'DNS';
    public static final String CEREBRO_GRADE_FAIL = 'F';

    // DNC OK_to_Call and DNC Status values
    public static final String DNC_CAN_CALL_STATUS = 'Can call';
    public static final String DNC_DO_NOT_CALL_STATUS = 'Do not call';
    public static final string DNC_OK_ANY_NUMBER = 'OK - Any Number';
    public static final string DNC_NOT_OK_ANY_NUMBER = 'NOT OK on any number';

    // Grading status
    public static final String GRADING_STATUS_BLANK = '';
    public static final String GRADING_STATUS_PENDINGMARKING = 'Pending Marking';
    public static final String GRADING_STATUS_AVAILABLE = 'Available';

    // View models
    public static final String VIEWMODEL_CONTACTPREFIX = 'Cnt_';
    public static final String VIEWMODEL_CONTACTHEDAPREFIX = 'Cnt_hed_';
    public static final String VIEWMODEL_OPPORTUNITYPREFIX = 'Opp_';
    public static final String VIEWMODEL_CASEPREFIX = 'Case_';
    public static final String VIEWMODEL_SUBJECTEROLMENTPREFIX = 'SubEnr_';
    public static final String VIEWMODEL_ACTIVITYPREFIX = 'Act_';
    public static final String VIEWMODEL_CUSTOMFIELDSUFFIX = '_c';
    public static final String VIEWMODEL_OWNERPREFIX = 'Owner_';

    // Case Status Types
    public static final String CASE_STATUS_NEW = 'New';
    public static final String CASE_STATUS_WAITING = 'Waiting';
    public static final String CASE_STATUS_IN_PROGRESS = 'In Progress';
    public static final String CASE_STATUS_WITH_CONTACT = 'With Contact';
    public static final String CASE_STATUS_CLOSED = 'Closed';

    // Case Department Types
    public static final String CASE_DEPT_ACQUISITIONS = 'Acquisitions';
    public static final String CASE_DEPT_ADMISSIONS = 'Admissions';
    public static final String CASE_DEPT_FINANCE = 'Finance';
    public static final String CASE_DEPT_PROGRESSIONS = 'Progressions';
    public static final String CASE_DEPT_STUDENT_CENTRAL = 'Student Central';
    public static final String CASE_DEPT_TIMETABLING = 'Timetabling';

    // Record Types - Standard Objects
    public static final String RECORDTYPE_ACCOUNT_ADMINISTRATIVE = 'Administrative';
    public static final String RECORDTYPE_ACCOUNT_BUSINESSORG = 'Business_Organization';
    public static final String RECORDTYPE_ACCOUNT_ACADEMICPROGRAM = 'Academic_Program';
    public static final String RECORDTYPE_ACCOUNT_DEPARTMENT = 'University_Department';
    public static final String RECORDTYPE_ACCOUNT_INSTITUTION = 'Educational_Institution';
    public static final String RECORDTYPE_CAMPAIGN_ACQUISITION = 'Acquisition';
    public static final String RECORDTYPE_CAMPAIGN_TERMINTAKE = 'TermIntake';
    public static final String RECORDTYPE_CASE_APPLICATION = 'Application';
    public static final String RECORDTYPE_CASE_AWARD = 'Award';
    public static final String RECORDTYPE_CASE_EXEMPTIONS = 'Exemptions';
    public static final String RECORDTYPE_CASE_INTERNAL = 'Internal';
    public static final String RECORDTYPE_CASE_ROLLOVER = 'Rollover';
    public static final String RECORDTYPE_CASE_STUDENT_SUPPORT = 'Student_Support';
    public static final String RECORDTYPE_CONTACT_FACULTY = 'Faculty';
    public static final String RECORDTYPE_CONTACT_PROSPECT = 'Prospect';
    public static final String RECORDTYPE_CONTACT_STUDENT = 'Students';
    public static final String RECORDTYPE_OPPORTUNITY_ACQUISITION = 'Acquisition';
    public static final String RECORDTYPE_OPPORTUNITY_PROGRESSION = 'Progression';
    public static final String RECORDTYPE_TASK_CLIENTCONTACT = 'Client_Contact';
    public static final String RECORDTYPE_TASK_FOLLOWUP = 'Follow_Up_Tasks';
    public static final String RECORDTYPE_TASK_MIR = 'MIR_Activity';

    // Record Types - Custom Objects
    public static final String RECORDTYPE_AFFILIATION_APPOINTMENT = 'Appointment';
    public static final String RECORDTYPE_AFFILIATION_QUALIFICATION = 'QualificationAccreditationorMembership';
    public static final String RECORDTYPE_COURSEENROLLMENT_STUDENT = 'Student';
    public static final String RECORDTYPE_COURSEENROLLMENT_PROVISIONAL = 'Provisional';
    public static final String RECORDTYPE_COURSEENROLLMENT_FACULTY = 'Faculty';
    public static final String RECORDTYPE_PLANREQUIREMENT_COURSEPLANREQUIREMENT = 'CoursePlanRequirement';
    public static final String RECORDTYPE_PLANREQUIREMENT_DEPENDENCYITEM = 'DependencyItem';
    public static final String RECORDTYPE_PLANREQUIREMENT_SUBJECTREQUIREMENT = 'SubjectRequirement';
    public static final String RECORDTYPE_SKILLSET_FACULTY = 'Faculty';
    public static final String RECORDTYPE_SKILLSET_STAFF = 'Staff';
    public static final String RECORDTYPE_SUBJECT_DEFINITION = 'SubjectDefinition';
    public static final String RECORDTYPE_SUBJECT_VERSION = 'SubjectVersion';
    public static final String RECORDTYPE_SUBJECTENROLMENT_FACULTY = 'Faculty';

    public static final Set<String> RECORDTYPES_CONTACT = new Set<String> { RECORDTYPE_CONTACT_PROSPECT, RECORDTYPE_CONTACT_STUDENT };

    public static final List<String> OPPORTUNITY_ACQUISITION_CLOSEDSTAGES = new List<String>{ 'Closed Won', 'Closed Lost' };

    public static final String OPPORTUNITY_CLOSEDLOST_DUPLICATE = 'Duplicate';
    public static final String OPPORTUNITY_CLOSEDLOST_NOTELIGIBLE = 'Not Eligible';
    public static final String OPPORTUNITY_CLOSEDLOST_TERMMOVEAUTOCLOSED = 'Term Move Auto Closed';

    // Program Portfolio
    public static final String DOMESTIC_MBA = 'DMBA';
    public static final String INTERNATIONAL_MBA = 'IMBA';

    public static final Set<String> GCM_COURSES = new Set<String>
    {
        'GCM',
        'GCM (Focused on Transforming from Manager to Leader)',
        'GCM (Focused on Board Member Essentials)',
        'GCM (Focused on New Manager Essentials)',
        'GCM (Focused on Stepping Up to the C-Suite)',
        'GCM (Focused on Business Owner’s Essentials)',
        'GCM (Focused on Project Manager Essentials)',
        'GCM (Focused on Operation Manager Essentials)'
    };

    public static final Integer MAXIMUM_SIZE_OF_ATTACHMENT = 25; // 25MB

    public static final String ERRORMSG_NULLPARAM = '{0} parameter must have a value. Cannot be empty or null.';

    public static final Set<String> ALLOWED_UPLOAD_FILEEXTENSIONS = new Set<String>{ 'pdf', 'doc', 'docx', 'rtf', 'txt', 'jpg', 'jpeg', 'png', 'xls', 'xlsx', 'heic', 'heif' };

    public static final Set<String> PROSPECT_PRIMARYFIELDS = new Set<String>
    {
        'Cnt_Email', 'Cnt_FirstName', 'Cnt_LastName', 'Cnt_HomePhone', 'Cnt_WorkExperienceYears_c', 'Cnt_ManagementExperienceYears_c', 'Cnt_Program_c',
        'Cnt_HighestTertiaryLevelDegree_c', 'Cnt_hed_Citizenship_c', 'Cnt_PrimaryMotivations_c', 'Cnt_PreferredTimeframe_c', 'Cnt_RecordType', 'Cnt_Portfolio_c',
        'Cnt_MobilePhone', 'Cnt_MailingCountry', 'Cnt_HomePhoneCountry_c', 'Cnt_MobilePhoneCountry_c', 'Cnt_Salutation', 'Cnt_OriginalReferrerSource_c',
        'Cnt_CalculatorAgeRange_c', 'Cnt_CalculatorPayPeriod_c', 'Cnt_CalculatorRemainingWorkYears_c', 'Cnt_CalculatorSalaryPerAnnum_c', 'Cnt_CalculatorStudyCommuting_c',
        'Cnt_CalculatorStudyEvening_c', 'Cnt_CalculatorStudyMorning_c', 'Cnt_CalculatorStudyWeekend_c', 'Cnt_CalculatorStudyWork_c', 'Cnt_InferredCountry_c',
        'Cnt_InferredCity_c', 'Cnt_InferredStateRegion_c', 'Cnt_InferredPostalCode_c', 'Cnt_hed_PreferredPhone_c', 'Cnt_hed_WorkPhone_c', 'Cnt_WorkPhoneCountry_c',
        'Cnt_OtherPhone', 'Cnt_OtherPhoneCountry_c', 'Cnt_LeadSource', 'Cnt_IPOverride__c'
    };

    public static final Map<String, String> EMAIL_ENDPOINTPAGEMAP = new Map<String, String>
    {
        'reserve-your-spot' => 'Reserve Your Spot',
        'check-your-eligibility' => 'Check Your Eligibility'
    };

    public static final String CONTACT_FIELDS = 'Aboriginal_or_Torres_Strait_Islander_Des__c, BirthDate, CHESSN_Number__c, Cerebro_Student_ID__c, CreatedDate, Email, ' +
        'Email_Express_Consent_Date__c, Email_Express_Consent_Exists__c, Express_Consent_Exists__c, Express_Consent_Expires__c, Express_Consent_Given__c, FirstName, ' +
        'HasOptedOutOfEmail, High_School_Level__c, High_School_State_Territory__c, HighestTertiaryLevelDegree__c, HomePhone, Id, Impairments__c, InferredCountry__c, ' +
        'Language_Spoken_at_Home__c, LastName, MailingCity, MailingCountry, MailingPostalcode, MailingState, MailingStreet, ManagementExperienceYears__c, MiddleName, ' +
        'MobilePhone, Name, OAF_Application_Id__c, OK_To_Call__c, OtherAddressType__c, OtherCity, OtherCountry, OtherPostalcode, OtherState, OtherStreet, PermanentAddressType__c, ' +
        'Permanent_Home_Address__c, Permanent_Home_Country__c, Permanent_Home_Postcode__c, Permanent_Home_State_Province_Region__c, Permanent_Home_Suburb_Town__c, ' +
        'PersonalEmail__c, Phone, Portfolio__c, PrimaryMotivations__c, Program__c, ProgramPortfolio__c, RecordTypeId, RecordType.DeveloperName, RecordType.Name, ' +
        'Salutation, SingleSignOnStatus__c, SMS_Express_Consent_Exists__c, Status__c, Student_Type__c, StudyPersona__c, WorkExperienceYears__c, YearOfArrivalInAustralia__c, ' +
        'hed__Country_Of_Origin__c, hed__Dual_Citizenship__c, hed__Gender__c';

    // Contact single sign on status
    public static final String CONTACT_SINGLESIGNONSTATUS_NONE = 'None';
    public static final String CONTACT_SINGLESIGNONSTATUS_PROSPECTPROVISIONING = 'Prospect Provisioning';
    public static final String CONTACT_SINGLESIGNONSTATUS_PROSPECTENABLED = 'Prospect Enabled';
    public static final String CONTACT_SINGLESIGNONSTATUS_STUDENTPROVISIONING = 'Student Provisioning';
    public static final String CONTACT_SINGLESIGNONSTATUS_STUDENTENABLED = 'Student Enabled';
    public static final String CONTACT_SINGLESIGNONSTATUS_DISABLED = 'Disabled';

    public static final List<String> CAMPAIGN_ACTIVE_STATUS = new List<String>{ 'In Progress', 'Next', 'Planned' };

    // File Content
    public static final String FILE_CONTENT_LOCATION_SALESFORCE = 'S';
    public static final String FILE_CONTENT_LOCATION_EXTERNAL = 'E';
    public static final String FILE_CONTENT_LOCATION_SOCIAL_CUSTOMER_SERVICE = 'L';
    public static final String FILE_VERSION_ORIGIN_CONTENT = 'C';
    public static final String FILE_VERSION_ORIGIN_CHATTER = 'H';
    public static final String FILE_CONTENT_SHARETYPE_VIEWER = 'V';
    public static final String FILE_CONTENT_SHARETYPE_COLLABORTOR = 'C';
    public static final String FILE_CONTENT_SHARETYPE_INFERRED = 'I';
    public static final String FILE_CONTENT_VISIBILITY_ALL = 'AllUsers';
    public static final String FILE_CONTENT_VISIBILITY_STANDARD = 'InternalUsers';
    public static final String FILE_CONTENT_VISIBILITY_SHARED = 'SharedUsers';

    // Content Version types
    public static final String CONTENTVERSION_TYPE_LOO = 'Letter of Offer';
    public static final String CONTENTVERSION_TYPE_LOO_ABBR = 'LOO';
    public static final String CONTENTVERSION_TYPE_TIMETABLE = 'Timetable';
    public static final String CONTENTVERSION_TYPE_TIMETABLE_ABBR = 'TT';
    public static final String CONTENTVERSION_TYPE_INVOICE = 'Invoice';
    public static final String CONTENTVERSION_TYPE_INVOICE_ABBR = 'INV';
    public static final String CONTENTVERSION_TYPE_PROOFOFID = 'Proof of ID';
    public static final String CONTENTVERSION_TYPE_TRANSCRIPT = 'Transcript';
    public static final String CONTENTVERSION_TYPE_ENGLISHPROFICIENCY = 'English Proficiency';
    public static final String CONTENTVERSION_TYPE_WORKEXPERIENCE = 'Work Experience';
    public static final String CONTENTVERSION_TYPE_SPECIALNEEDS = 'Special Needs';
    public static final String CONTENTVERSION_TYPE_WELCOMELETTER = 'Welcome Letter';
    public static final String CONTENTVERSION_TYPE_WELCOMELETTER_ABBR = 'WL';

    public static final Set<String> CONTENTVERSION_TYPE_OAFDOCTYPES = new Set<String>
    {
        CONTENTVERSION_TYPE_PROOFOFID, CONTENTVERSION_TYPE_TRANSCRIPT, CONTENTVERSION_TYPE_ENGLISHPROFICIENCY,
        CONTENTVERSION_TYPE_WORKEXPERIENCE, CONTENTVERSION_TYPE_SPECIALNEEDS
    };

    // Docs statuses
    public static final String DOCSTATUS_NOTREQUIRED = 'Not Required';
    public static final String DOCSTATUS_REQUIRED = 'Required';
    public static final String DOCSTATUS_REQUIRESREVIEW = 'Requires Review';
    public static final String DOCSTATUS_REJECTED = 'Rejected';
    public static final String DOCSTATUS_ACCEPTED = 'Accepted';

    // Appointment affiliation positive statuses
    public static final Set<String> AFFILIATION_APPOINTMENTSTATUSES = new Set<String> { 'Provisional', 'Current', 'Continuing' };

    public static final Set<String> AFFILIATION_ROLE_OLF = new Set<String> { 'Online Facilitator', 'Online Facilitator / Subject Coordinator' };

    // Academic Information
    public static final String SUBJECT_COURSEID_BREAK = 'BREAK';
    public static final String SUBJECTOFFERING_TYPE_BREAK = 'Break';
    public static final String SUBJECTOFFERING_TYPE_STANDARD = 'Standard';
    public static final String SUBJECTOFFERING_TYPE_VIRTUAL = 'Virtual';

    // Subject Enrolment statuses
    public static final String SUBJECTENROLMENT_STATUS_INTERESTED = 'Interested';
    public static final String SUBJECTENROLMENT_STATUS_ENROLLING = 'Enrolling';
    public static final String SUBJECTENROLMENT_STATUS_ENROLLED = 'Enrolled';
    public static final String SUBJECTENROLMENT_STATUS_STUDYING = 'Studying';
    public static final String SUBJECTENROLMENT_STATUS_COMPLETED = 'Completed';
    public static final String SUBJECTENROLMENT_STATUS_WITHDRAWING = 'Withdrawing';
    public static final String SUBJECTENROLMENT_STATUS_WITHDRAWN = 'Withdrawn';
    public static final String SUBJECTENROLMENT_STATUS_REQUESTED = 'Requested';

    // Marketing Cloud
    public static final String MARKETINGCLOUD_AUTH_ACCOUNTID = '100020822';
    public static final String MARKETINGCLOUD_AUTH_ENDPOINT = 'https://mck5dk-9xrxy0s9pwrlbj-vv94w0.auth.marketingcloudapis.com/v2/token';
    public static final String MARKETINGCLOUD_AUTH_CLIENTID = 'ar51uqtf6gbu4qhomqpnn8si';
    public static final String MARKETINGCLOUD_AUTH_CLIENTSECRET = 'uN0ImdzrmfTdLdoBiFhD0Ir5';
    public static final String MARKETINGCLOUD_AUTH_GRANTTYPE = 'client_credentials';
    public static final String MARKETINGCLOUD_SEND_ENDPOINT = 'https://mck5dk-9xrxy0s9pwrlbj-vv94w0.rest.marketingcloudapis.com/interaction/v1/events';
    public static final String MARKETINGCLOUD_SEND_EVENT = 'APIEvent-260f6605-7c5c-30d1-c69e-72e05791104a';
    public static final String MARKETINGCLOUD_SENDWELCOME_EVENT_PROD = 'APIEvent-eb4a472e-549b-b999-46e2-a8f0f11929df';
    public static final String MARKETINGCLOUD_SENDWELCOME_EVENT_SANDBOX = 'APIEvent-f5b41329-d0c0-0de4-e315-ddce64d72aa9';

    // API Provider
    public static final String APIPROVIDER_MARKETINGCLOUD = 'MarketingCloud';
    public static final String APIPROVIDER_SENDGRID = 'SendGrid';
    public static final String APIPROVIDER_SMSMAGIC = 'SMSMagic';
    public static final String APIPROVIDER_DEFAULT = 'Default';

    // SMS Magic
    public static final String SMSMAGIC_APIKEY = '7460fc22b400584d3462bde9089a49b7';
    public static final String SMSMAGIC_AUTH_ENDPOINT = 'https://api.sms-magic.com/v1/api_key/validate';
    public static final String SMSMAGIC_SEND_ENDPOINT = 'https://api.sms-magic.com/v1/sms/send';

    // SendGrid
    public static final String SENDGRID_SEND_ENDPOINT = 'https://api.sendgrid.com/v3/mail/send';

    // Single Sign On
    public static final String SINGLESIGNON_ACCESS_PROVISIONALGROUP = 'SG-Students-Provisional';
    public static final String SINGLESIGNON_ACCESS_STUDENTGROUP = 'SG-Students-Full';
    public static final String SINGLESIGNON_ACCESS_GRANTTYPE = 'password';
    public static final String SINGLESIGNON_ACCESS_RESOURCEURL = 'https://graph.windows.net';

    public static final String SINGLESIGNON_APIENDPOINT_OAUTHTOKEN = '/oauth2/token?api-version=1.0';
    public static final String SINGLESIGNON_APIENDPOINT_USER = '/users?api-version=1.6';
    public static final String SINGLESIGNON_APIENDPOINT_ALLGROUPS = '/groups?api-version=1.6&$top=999';
    public static String SINGLESIGNON_APIENDPOINT_USERGROUPS(String userPrincipalName)
    {
        return String.format('/users/{0}/getMemberGroups?api-version=1.6', new String[] { userPrincipalName });
    }
    public static String SINGLESIGNON_APIENDPOINT_USEROBJECT(String userPrincipalName)
    {
        return String.format('/users/{0}?api-version=1.6', new String[] { userPrincipalName });
    }
    public static String SINGLESIGNON_APIENDPOINT_USERASSIGNGROUP(String groupObjectId)
    {
        return String.format('/groups/{0}/$links/members?api-version=1.6', new String[] { groupObjectId });
    }
    public static String SINGLESIGNON_APIENDPOINT_OBJECTDIRECTORY(String userObjectId)
    {
        return String.format('/directoryObjects/{0}', new String[] { userObjectId });
    }

    public static final String SKILLSET_WEIGHTING_ALWAYS = 'Always';
    public static final String SKILLSET_WEIGHTING_NEVER = 'Never';

    // Task Subject
    public static final String TASKSUBJECT_FOR_NEW_CONTACT = 'Prospect Created! Follow Up Enquiry';
    public static final String TASKSUBJECT_FOR_EXISTING_CONTACT = 'Follow Up Enquiry via website';

    // Email Templates
    public static final String EMAILTEMPLATE_WELCOME_GCM = 'GCM Welcome Email Template';
    public static final String EMAILTEMPLATE_WELCOME_MBA = 'MBA Welcome Email Template';
    public static final String EMAILTEMPLATE_WELCOME_SMS = 'SMS Welcome Template';
    public static final String EMAILTEMPLATE_TASKNOTIFICATION = 'New Task Notification';
    public static final String EMAILTEMPLATE_CASECOMMENT_NOTIFICATION = 'Case Comment Notification';

    // Confirmation Emails
    public static final String CONFIRMATIONEMAIL_APPLICATIONMIRREQUEST = 'ApplicationMIRRequest';
    public static final String CONFIRMATIONEMAIL_APPLICATIONREJECTION = 'ApplicationRejection';
    public static final String CONFIRMATIONEMAIL_APPLICATIONSUBMISSION = 'ApplicationSubmission';
    public static final String CONFIRMATIONEMAIL_FEEHELPDETAILSCONFIRMATION = 'FEEHELPDetailsConfirmation';
    public static final String CONFIRMATIONEMAIL_FEEHELPDETALSREQUEST = 'FEEHELPDetailsRequest';
    public static final String CONFIRMATIONEMAIL_INVOICEPAYMENTCONFIRMATION = 'InvoicePaymentConfirmation';
    public static final String CONFIRMATIONEMAIL_INVOICEPAYMENTREQUEST = 'InvoicePaymentRequest';
    public static final String CONFIRMATIONEMAIL_LOOACCEPTANCECONFIRMATION = 'LOOAcceptanceConfirmation';
    public static final String CONFIRMATIONEMAIL_LOOACCEPTANCEREQUEST = 'LOOAcceptanceRequest';
    public static final String CONFIRMATIONEMAIL_TIMETABLEACCEPTANCECONFIRMATION = 'TimetableAcceptanceConfirmation';
    public static final String CONFIRMATIONEMAIL_TIMETABLEACCEPTANCEREQUEST = 'TimetableAcceptanceRequest';
    public static final String CONFIRMATIONEMAIL_WELCOMETOAIB = 'WelcomeToAIB';
}