public class ContactReenableSSOCommand extends BaseTriggerCommand
{
    protected override List<SObject> validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        List<Contact> result = new List<Contact>();
        Id prospectRecordTypeId = RecordTypeUtils.getRecordTypeId(Constants.RECORDTYPE_CONTACT_PROSPECT, SObjectType.Contact);

        for (Contact contact : (List<Contact>) items)
        {
            if (contact.RecordTypeId != prospectRecordTypeId)
                continue;

            if (((contact.SingleSignOnStatus__c == Constants.CONTACT_SINGLESIGNONSTATUS_NONE && String.isNotEmpty(contact.SingleSignOnSyncMessage__c)) ||
                contact.SingleSignOnStatus__c == Constants.CONTACT_SINGLESIGNONSTATUS_PROSPECTPROVISIONING || 
                contact.SingleSignOnStatus__c == Constants.CONTACT_SINGLESIGNONSTATUS_STUDENTPROVISIONING) && 
                contact.SingleSignOnStatusLastUpdated__c <= Datetime.now().addHours(-1))
            {
                result.add(contact);
            }
        }

        Log.info(this.className, 'validate', 'Reenabling SSO for {0} contacts', new List<String> { String.valueOf(result.size()) });

        return result;
    }

    protected override ActionResult command(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        return ContactService.enableSSO(items);
    }
}