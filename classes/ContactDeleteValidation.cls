public class ContactDeleteValidation extends BaseValidation 
{
    static final String allowedProfile = 'System Administrator';

    public override ActionResult validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        ActionResult result = new ActionResult(ResultStatus.SUCCESS);
        String profileName = ProfileUtilities.getCurrentUserProfileName();
        List<Contact> contacts = getContacts(SObjectUtils.getIds(items, 'Id'));

        for(Contact c : contacts)
        {
            if(c.RecordType.DeveloperName == Constants.RECORDTYPE_CONTACT_STUDENT && profileName != allowedProfile)
            {
                String errorMsg = 'User not allowed to performed a record merge as this would delete Student contact type';
                result = new ActionResult(ResultStatus.ERROR, errorMsg);
                break;
            }
        }

        return result;
    }

    private List<Contact> getContacts(Set<Id> ids)
    {
        List<Contact> contacts = [SELECT Id, RecordType.DeveloperName FROM Contact WHERE Id in :ids];
        return contacts;
    }
}