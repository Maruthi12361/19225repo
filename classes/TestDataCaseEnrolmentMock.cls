@isTest
public class TestDataCaseEnrolmentMock
{
    public static CaseEnrolment__c create(User user, Id caseId, Id subjectEnrolmentId)
    {
        CaseEnrolment__c caseEnrolment = new CaseEnrolment__c
        (
            Case__c = caseId,
            SubjectEnrolment__c = subjectEnrolmentId
        );

        System.runAs(user)
        {
            insert caseEnrolment;
        }

        return caseEnrolment;
    }
}