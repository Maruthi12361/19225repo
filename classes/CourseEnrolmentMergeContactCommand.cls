public class CourseEnrolmentMergeContactCommand extends BaseTriggerCommand
{
    public CourseEnrolmentMergeContactCommand()
    {
        super(5);
    }

    public override List<SObject> validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        List<hed__Program_Enrollment__c> result = new List<hed__Program_Enrollment__c>();

        if (triggerContext == Enums.TriggerContext.AFTER_UPDATE)
        {
            for (hed__Program_Enrollment__c newValue : (List<hed__Program_Enrollment__c>) items)
            {
                hed__Program_Enrollment__c oldValue = (hed__Program_Enrollment__c) oldMap.get(newValue.Id);

                if (newValue.Status__c != oldValue.Status__c || newValue.hed__End_Date__c != oldValue.hed__End_Date__c || newValue.StatusResultReason__c != oldValue.StatusResultReason__c)
                {
                    result.add(newValue);
                }
            }
        }
        else if (triggerContext == Enums.TriggerContext.AFTER_INSERT || triggerContext == Enums.TriggerContext.AFTER_DELETE)
        {
            result.addAll((List<hed__Program_Enrollment__c>)items);
        }

        return result;
    }

    public override ActionResult command(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        Map<Id, List<hed__Program_Enrollment__c>> enrolmentMap = new Map<Id, List<hed__Program_Enrollment__c>>();

        for (hed__Program_Enrollment__c enrolment : (List<hed__Program_Enrollment__c>) items)
        {
            List<hed__Program_Enrollment__c> enrolments = enrolmentMap.get(enrolment.hed__Contact__c);

            if (enrolments == null)
            {
                enrolments = new List<hed__Program_Enrollment__c> { enrolment };
                enrolmentMap.put(enrolment.hed__Contact__c, enrolments);
            }
            else
            {
                enrolments.add(enrolment);
            }
        }
        
        List<Contact> contacts = 
            [
                SELECT Id,Alumni__c,CurrentStudent__c,BBAComplete__c,GCMComplete__c,MBAComplete__c,LastCourseCompletionDate__c,
                (
                    SELECT Id,Status__c,StatusResultReason__c,hed__End_Date__c,Programme__c
                    FROM hed__Program_Enrollments__r
                )
                FROM Contact
                WHERE RecordType.DeveloperName =: Constants.RECORDTYPE_CONTACT_STUDENT
                AND Id IN : enrolmentMap.keySet()
            ];
        Map<String, List<hed__Program_Enrollment__c>> mergedEnrolmentMap = new Map<String, List<hed__Program_Enrollment__c>>();

        for (Contact student : contacts)
        {
            Map<Id, hed__Program_Enrollment__c> existingCourseEnrolments = new Map<Id, hed__Program_Enrollment__c>(student.hed__Program_Enrollments__r);
            Map<Id, hed__Program_Enrollment__c> updatedCourseEnrolments = new Map<Id, hed__Program_Enrollment__c>(enrolmentMap.get(student.Id));

            for (hed__Program_Enrollment__c newValue : updatedCourseEnrolments.values())
            {
                if (triggerContext != Enums.TriggerContext.AFTER_DELETE)
                    existingCourseEnrolments.put(newValue.Id, newValue);
                else if (existingCourseEnrolments.containsKey(newValue.Id))
                    existingCourseEnrolments.remove(newValue.Id);
            }

            mergedEnrolmentMap.put(student.Id, existingCourseEnrolments.values());
        }

        List<Contact> updatedContacts = new List<Contact>();

        for (Contact contact : contacts)
        {
            Boolean isDirty = false;
            Boolean isCurrentStudent = false;
            Boolean isBBAComplete = false;
            Boolean isGCMComplete = false;
            Boolean isMBAComplete = false;
            Date lastCourseCompletionDate;
            List<hed__Program_Enrollment__c> courseEnrolments = mergedEnrolmentMap.get(contact.Id);

            for (hed__Program_Enrollment__c ce : courseEnrolments)
            {
                if (isCurrentStudent || ce.Status__c == 'Ongoing' || ce.Status__c == 'Approved' || (ce.Status__c == 'Incomplete' && (ce.StatusResultReason__c == 'Excluded' || ce.StatusResultReason__c == 'Suspended')))
                    isCurrentStudent = true;

                if (isBBAComplete || ce.StatusResultReason__c == 'Awarded' && ce.Programme__c == 'BBA')
                    isBBAComplete = true;

                if (isGCMComplete || ce.StatusResultReason__c == 'Awarded' && ce.Programme__c == 'GCM')
                    isGCMComplete = true;

                if (isMBAComplete || ce.StatusResultReason__c == 'Awarded' && ce.Programme__c == 'MBA')
                    isMBAComplete = true;

                if (ce.StatusResultReason__c == 'Awarded' && ce.hed__End_Date__c != null && 
                    (ce.Programme__c == 'BBA' || ce.Programme__c == 'GCM' || ce.Programme__c == 'MBA'))
                {
                    if (lastCourseCompletionDate == null || lastCourseCompletionDate < ce.hed__End_Date__c)
                        lastCourseCompletionDate = ce.hed__End_Date__c;
                }
            }

            if (contact.CurrentStudent__c != isCurrentStudent)
            {
                contact.CurrentStudent__c = isCurrentStudent;
                isDirty = true;
            }

            if (contact.BBAComplete__c != isBBAComplete)
            {
                contact.BBAComplete__c = isBBAComplete;
                isDirty = true;
            }

            if (contact.GCMComplete__c != isGCMComplete)
            {
                contact.GCMComplete__c = isGCMComplete;
                isDirty = true;
            }

            if (contact.MBAComplete__c != isMBAComplete)
            {
                contact.MBAComplete__c = isMBAComplete;
                isDirty = true;
            }

            if (contact.Alumni__c != (isBBAComplete || isMBAComplete))
            {
                contact.Alumni__c = isBBAComplete || isMBAComplete;
                isDirty = true;
            }

            if (contact.LastCourseCompletionDate__c != lastCourseCompletionDate)
            {
                contact.LastCourseCompletionDate__c = lastCourseCompletionDate;
                isDirty = true;
            }

            if (isDirty)
                updatedContacts.add(contact);
        }

        if (updatedContacts.size() > 0)
            uow.registerDirty(updatedContacts);

        return new ActionResult(ResultStatus.SUCCESS);
    }
}