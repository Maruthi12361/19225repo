public class ContactGetManyQuery
{
    public List<Contact> query(Set<Id> ids, String fields, Boolean forUpdate)
    {
        List<Contact> contacts;
        String query = 'SELECT ' + fields + ' FROM Contact WHERE Id IN : ids';

        if (forUpdate)
            query += ' FOR UPDATE';

        return Database.query(query);
    }
}