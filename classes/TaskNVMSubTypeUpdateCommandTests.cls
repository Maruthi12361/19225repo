@isTest
public class TaskNVMSubTypeUpdateCommandTests
{

    @testSetup
    public static void setup()
    {
        TestUtilities.setupHEDA();
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact contact = TestDataContactMock.createProspectCold(user, '0455548889');
    }

    @isTest
    static void NewNVMTask_TaskSubTypeUpdated()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact contact = [SELECT Id FROM Contact Limit 1];

        // Act
        Task task = TestDataTaskMock.createNVMCallTask(user, contact.Id, 'Contact Successful', 'Test NVM SubType Update Before Insert', true);

        // Assert
        System.assertEquals('Call', task.TaskSubType);
    }

    @isTest
    static void NewNonNVMTask_TaskSubTypeNotUpdated()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact contact = [SELECT Id FROM Contact Limit 1];

        // Act
        Task task = TestDataTaskMock.createRolloverCallTask(user, contact.Id, null, 'Contact Successful', 'Test Non NVM SubType Not Updated Before Insert', true);

        // Assert
        System.assertEquals('Task', task.TaskSubType);
    }
}