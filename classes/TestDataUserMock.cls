@isTest
public class TestDataUserMock
{
    public static User createStudentPortalLoginProfileUser(User user, Id contactId)
    {
        User newUser = new User(
            username = 'StudentPortalProfileUser@studentusers.aib.edu.au',
            lastName = 'Tester', 
            contactId = contactId,
            profileId = (Id)System.Label.IDProfileAIBStudentPortalLoginUser,
            alias = 'prtlUser',
            communityNickname = 'StudentPortalProfileTestUser',
            email = 'StudentPortalProfileUser@studentusers.aib.edu.au.invalid',
            emailEncodingKey = 'UTF-8',
            timeZoneSidKey = 'Australia/Adelaide',
            localeSidKey = 'en_AU',
            languageLocaleKey = 'en_US'
        );

        System.RunAs(user)
        {
            insert newUser;
        }

        return newUser;
    }
}