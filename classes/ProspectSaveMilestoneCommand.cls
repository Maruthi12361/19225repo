public class ProspectSaveMilestoneCommand extends BaseTriggerCommand
{
    private static Map<Id, Set<String>> activeContactMap = new Map<Id, Set<String>>();
    private static List<SalesScoreConfig__c> salesScoreConfigs;

    protected override List<SObject> validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        List<Contact> result = new List<Contact>();

        for (Contact item : (List<Contact>)items)
        {
            if (triggerContext == Enums.TriggerContext.AFTER_UPDATE)
            {
                if (item.RecordTypeId == System.Label.ID_RecordType_Contact_Prospect)
                {
                    Boolean isValid = false;
                    Contact oldContact = (Contact) oldMap.get(item.Id);
                    Set<String> sourceTypes = activeContactMap.get(item.Id);

                    if (item.HasOptedOutOfEmail && !oldContact.HasOptedOutOfEmail)
                    {
                        if (sourceTypes == null || !sourceTypes.contains('Unsubscribed'))
                            isValid = true;
                    }

                    if (!isValid && item.DoNotCall && !oldContact.DoNotCall)
                    {
                        if (sourceTypes == null || !sourceTypes.contains('Do Not Call'))
                            isValid = true;
                    }

                    if (!isValid && item.LeadScore__c >= 300 && oldContact.LeadScore__c < 300)
                    {
                        if (sourceTypes == null || !sourceTypes.contains('Lead Score Over 300'))
                            isValid = true;
                    }

                    if (isValid)
                        result.add(item);
                }
            }
        }

        return result;
    }

    protected override ActionResult command(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        List<InterestingMoment__c> moments = new List<InterestingMoment__c>();
        salesScoreConfigs = getSalesScoreConfigs();

        for (Contact item : (List<Contact>)items)
        {
            Contact oldContact = (Contact) oldMap.get(item.Id);
            Set<String> sourceTypes = activeContactMap.get(item.Id);

            if (sourceTypes == null)
            {
                sourceTypes = new Set<String>();
                activeContactMap.put(item.Id, sourceTypes);
            }

            if (item.HasOptedOutOfEmail && !oldContact.HasOptedOutOfEmail)
            {
                if (sourceTypes == null || !sourceTypes.contains('Unsubscribed'))
                {
                    InterestingMoment__c moment = createInterestingMoment(item.Id, 'Unsubscribed');
                    moments.add(moment);
                    sourceTypes.add('Unsubscribed');
                }
            }

            if (item.DoNotCall && !oldContact.DoNotCall)
            {
                if (sourceTypes == null || !sourceTypes.contains('Do Not Call'))
                {
                    InterestingMoment__c moment = createInterestingMoment(item.Id, 'Do Not Call');
                    moments.add(moment);
                    sourceTypes.add('Do Not Call');
                }
            }

            if (item.LeadScore__c >= 300 && oldContact.LeadScore__c < 300)
            {
                if (sourceTypes == null || !sourceTypes.contains('Lead Score Over 300'))
                {
                    InterestingMoment__c moment = createInterestingMoment(item.Id, 'Lead Score Over 300');
                    moments.add(moment);
                    sourceTypes.add('Lead Score Over 300');
                }
            }
        }

        if (moments.size() > 0)
            uow.registerNew(moments);

        return new ActionResult();
    }

    private InterestingMoment__c createInterestingMoment(Id contactId, String source)
    {
        InterestingMoment__c moment = new InterestingMoment__c();
        moment.ContactID__c = contactId;
        moment.Type__c = 'Milestone';
        moment.Source__c = source;
        moment.EventDate__c = Datetime.now();
        moment.BehaviourScore__c = getBehaviourScoreValue(moment.Type__c, moment.Source__c);

        return moment;
    }

    private List<SalesScoreConfig__c> getSalesScoreConfigs()
    {
        return 
            [
                SELECT Id, Active__c, Operator__c, ScoreType__c, ScoreValue__c, Source__c, Value__c, DisplayOrder__c
                  FROM SalesScoreConfig__c
                 WHERE Active__c = true AND ScoreType__c = 'Milestone' AND Source__c IN ('Unsubscribed', 'Do Not Call', 'Lead Score Over 300')
                 ORDER BY DisplayOrder__c
            ];
    }

    private List<SalesScoreConfig__c> getInterestingMomentSalesScolreConfigs(String scoreType)
    {
        List<SalesScoreConfig__c> result = new List<SalesScoreConfig__c>();

        for (SalesScoreConfig__c config : salesScoreConfigs)
        {
            if (config.ScoreType__c == scoreType)
                result.add(config);
        }

        return result;
    }

    private Double getBehaviourScoreValue(String scoreType, String source)
    {
        List<SalesScoreConfig__c> interestingMomentConfigs = getInterestingMomentSalesScolreConfigs(scoreType);

        for (SalesScoreConfig__c config : interestingMomentConfigs)
        {
            if (StringUtils.isLogicalStatementTrue(config.Operator__c, source, config.Source__c))
                return config.ScoreValue__c;
        }

        return 0;
    }
}