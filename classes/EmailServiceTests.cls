@isTest
private class EmailServiceTests
{
    @testSetup
    static void setup()
    {
        TestUtilities.setupHEDA();
    }

    @isTest
    private static void SendSimple_QueuesEmailsForDelivery()
    {
        // Arrange
        Integer emailCount = 0;
        UnitOfWork.current = new UnitOfWork(new Schema.SObjectType[] {});

        // Act
        for (Integer i = 0; i < 10; i++)
        {
            EmailService.send(UserInfo.getUserEmail(),
                'EmailService Test',
                'Test Email Number: ' + i + ' out of 10 emails');
        }

        emailCount = UnitOfWork.current.emailCount;
        UnitOfWork.current = null;

        // Assert
        System.assertEquals(10, emailCount);
    }

    @isTest
    private static void SendComplex_QueuesEmailsForDelivery()
    {
        // Arrange
        Integer emailCount = 0;
        UnitOfWork.current = new UnitOfWork(new Schema.SObjectType[] {});

        // Act
        for (Integer i = 0; i < 10; i++)
        {
            EmailService.send(UserInfo.getUserEmail(),
                'EmailService Test',
                'Test Email Number: ' + i + ' out of 10 emails',
                'Test Name',
                'test.reply@aib.edu.au');
        }

        emailCount = UnitOfWork.current.emailCount;
        UnitOfWork.current = null;

        // Assert
        System.assertEquals(10, emailCount);
    }

    @isTest
    private static void SendSimple_ThrowsErrorIndicatingDeveloperWireupIncorrect()
    {
        // Arrange
        UnitOfWork.current = null;

        // Act
        try
        {
            EmailService.send(UserInfo.getUserEmail(),
                'EmailService Test',
                'Test email');
        }
        catch (Exception e)
        {
            // Assert
            System.assert(e instanceof Exceptions.ApplicationException, 'Sending emails without a UnitOfWork should indicate to developer that wireup is incorrect.');
        }
    }

    @isTest
    private static void SendComplex_ThrowsErrorIndicatingDeveloperWireupIncorrect()
    {
        // Arrange
        UnitOfWork.current = null;

        // Act
        try
        {
            EmailService.send(UserInfo.getUserEmail(),
                'EmailService Test',
                'Test email',
                'Test Name',
                'test.reply@aib.edu.au');
        }
        catch (Exception e)
        {
            // Assert
            System.assert(e instanceof Exceptions.ApplicationException, 'Sending emails without a UnitOfWork should indicate to developer that wireup is incorrect.');
        }
    }

    @isTest
    private static void SendLegacySimple_QueuesEmailsForDeliveryWithoutUnitOfWork()
    {
        // Arrange
        Integer emailCount = 0;
        UnitOfWork.current = null;

        // Act
        for (Integer i = 0; i < 10; i++)
        {
            EmailService.sendLegacy(UserInfo.getUserEmail(),
                'EmailService Test',
                'Test Email Number: ' + i + ' out of 10 emails');
        }

        // Assert
        System.assert(true, 'SendLegacy should create its own Unit Of Work if detecting one does not exist');
    }

    @isTest
    private static void SendLegacyComplex_QueuesEmailsForDeliveryWithoutUnitOfWork()
    {
        // Arrange
        Integer emailCount = 0;
        UnitOfWork.current = null;

        // Act
        for (Integer i = 0; i < 10; i++)
        {
            EmailService.sendLegacy(UserInfo.getUserEmail(),
                'EmailService Test',
                'Test Email Number: ' + i + ' out of 10 emails',
                'Test Name',
                'test.reply@aib.edu.au');
        }

        // Assert
        System.assert(true, 'SendLegacy should create its own Unit Of Work if detecting one does not exist');
    }

    @isTest
    private static void SendToRecipientWithSendGridProvider_SendEmailViaSendGrid()
    {
        // Act
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        ActionResult result = EmailService.sendToRecipient(Constants.APIPROVIDER_SENDGRID, UserInfo.getUserEmail(), 'EmailService Test', 'Default Text', 'test.reply@aib.edu.au', 'SendToRecipientWithSendGridProvider_SendEmailViaSendGrid', null, '<html><head></head><body>Email test html content</body></html>', null);
        Test.stopTest();

        // Assert
        System.assert(result.isSuccess, 'Fail to send email by SendGrid');
        System.assertEquals('Email is successfully sent via SendGrid', result.messages[0]);
    }

    @isTest
    private static void SendToRecipientWithMarketingCloudProvider_SendEmailViaMarketingCloud()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        TestDataContactMock.createProspect(user, 'Anna', 'Smith', '', '0455566755', 'anna.smith@gmail.com', Constants.STATUS_COLD, 'A001588497', 'Australia', 'Website (Organic)');
        Contact contact = [SELECT Id, Email, FirstName, LastName FROM Contact LIMIT 1];

        // Act
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        ActionResult result = EmailService.sendToRecipient(Constants.APIPROVIDER_MARKETINGCLOUD, contact.Email, contact.FirstName + ' ' + contact.LastName, 'MarketingCloud Test', UserInfo.getUserEmail(), userInfo.getFirstName() + ' ' + userInfo.getLastName(), 'This is text content body', '<html><head></head><body>This is html content body</body></html>', null);
        Test.stopTest();

        // Assert
        System.assert(result.isSuccess, 'Fail to send email by Marketing Cloud');
        System.assertEquals('Message is successfully sent via MarketingCloud', result.messages[0]);
    }

    @isTest
    private static void SendToRecipientWithMarketingCloudProvider_SendEmailViaDefaultConfiguration()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        TestDataContactMock.createProspect(user, 'Anna', 'Smith', '', '0455566755', 'anna.smith@gmail.com', Constants.STATUS_COLD, 'A001588497', 'Australia', 'Website (Organic)');
        Contact contact = [SELECT Id, Email, FirstName, LastName FROM Contact LIMIT 1];

        // Act
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        ActionResult result = EmailService.sendToRecipient(Constants.APIPROVIDER_DEFAULT, UserInfo.getUserEmail(), 'EmailService Test', 'Default Text', 'test.reply@aib.edu.au', 'SendToRecipientWithMarketingCloudProvider_SendEmailViaDefaultConfiguration', null, '<html><head></head><body>Email test html content</body></html>', null);
        Test.stopTest();

        // Assert
        System.assert(result.isSuccess, 'Fail to send email via SendGrid');
        System.assertEquals('Email is successfully sent via SendGrid', result.messages[0]);
    }

    @isTest
    private static void SendToRecipientWithMarketingCloudProvider_SendEmailNoDefaultConfiguration()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        TestDataContactMock.createProspect(user, 'Anna', 'Smith', '', '0455566755', 'anna.smith@gmail.com', Constants.STATUS_COLD, 'A001588497', 'Australia', 'Website (Organic)');
        Contact contact = [SELECT Id, Email, FirstName, LastName FROM Contact LIMIT 1];

        // Act
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        ActionResult result = EmailService.sendToRecipient(null, UserInfo.getUserEmail(), 'EmailService Test', 'Default Text', 'test.reply@aib.edu.au', 'SendToRecipientWithMarketingCloudProvider_SendEmailNoDefaultConfiguration', null, '<html><head></head><body>Email test html content</body></html>', null);
        Test.stopTest();

        // Assert
        System.assert(result.isSuccess, 'Fail to send email via SendGrid');
        System.assertEquals('Email is successfully sent via SendGrid', result.messages[0]);
    }
}