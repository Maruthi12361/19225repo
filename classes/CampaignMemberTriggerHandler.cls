public class CampaignMemberTriggerHandler extends BaseTriggerHandler 
{
	public override void afterDelete()
	{
		Set<Id> contactIds = new Set<Id>();
		Set<String> exludedWarmCampaign = new Set<String>(Label.ContactExcludedWarmCampaigns.split(','));

		for (CampaignMember cm : (List<CampaignMember>) Trigger.old)
		{
			if (cm.ContactId != null || cm.ContactId != '' && cm.Campaign.Type == 'Warm' && exludedWarmCampaign.contains(cm.Campaign.Name))
			{
				Log.info(this.className, 'afterDelete', 'Contact no longer in warm campaign ' + cm.ContactId);
				contactIds.add(cm.ContactId);
			}
		}
		
		if (contactIds.size() > 0)
			ContactService.handleRemovalCampaignMember(contactIds);
	}
}