public class SObjectService 
{
    public static ActionResult identifyAllDuplicates(List<SObject> items)
    {
        return new SObjectIdentifyAllDuplicatesCommand().execute(items);
    }

    public static ActionResult sendConfirmationEmail(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        return new SObjectSendConfirmationEmailCommand().execute(items, oldMap, triggerContext);
    }
}