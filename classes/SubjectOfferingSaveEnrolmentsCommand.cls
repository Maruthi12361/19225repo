public class SubjectOfferingSaveEnrolmentsCommand extends BaseComponent
{
    private Boolean validate(Object parameter)
    {
        ViewModels.Param param = (ViewModels.Param) parameter;

        if (String.isEmpty(param.Identifier))
        {
            Log.error(this.className, 'validate', 'Identifier parameter is null');
            throw new Exceptions.InvalidParameterException('Identifier parameter cannot be empty or null');
        }
        else if (String.isEmpty(param.Payload))
        {
            Log.error(this.className, 'validate', 'Payload parameter is null');
            throw new Exceptions.InvalidParameterException('Enrolments payload cannot be empty or null');
        }
        else
        {
            List<ViewModelsWorkforce.SubjectEnrolmentStaffDetailsItem> enrolments =
                (List<ViewModelsWorkforce.SubjectEnrolmentStaffDetailsItem>) JSON.deserialize(param.Payload, List<ViewModelsWorkforce.SubjectEnrolmentStaffDetailsItem>.class);

            if (enrolments.size() == 0)
            {
                Log.error(this.className, 'validate', 'Payload parameter body is empty');
                throw new Exceptions.InvalidParameterException('Payload parameter body cannot be empty');
            }
        }

        return true;
    }

    public ActionResult command(Object parameter)
    {
        validate(parameter);

        ActionResult result = new ActionResult(ResultStatus.SUCCESS);
        ViewModels.Param param = (ViewModels.Param) parameter;
        List<hed__Course_Enrollment__c> createEnrolments = new List<hed__Course_Enrollment__c>();
        List<hed__Course_Enrollment__c> updateEnrolments = new List<hed__Course_Enrollment__c>();
        List<hed__Course_Enrollment__c> removeEnrolments = new List<hed__Course_Enrollment__c>();
        Id facultyRecordTypeId = RecordTypeUtils.getRecordTypeId(Constants.RECORDTYPE_COURSEENROLLMENT_FACULTY, SObjectType.hed__Course_Enrollment__c);

        Log.info('SubjectOfferingSaveEnrolmentsCommand', 'command', 'Request made with following parameter: Identifier {0} \n Prospect {1}',
            new List<String>{ param.Identifier, param.Payload });

        List<ViewModelsWorkforce.SubjectEnrolmentStaffDetailsItem> modelEnrolments =
            (List<ViewModelsWorkforce.SubjectEnrolmentStaffDetailsItem>) JSON.deserialize(param.Payload, List<ViewModelsWorkforce.SubjectEnrolmentStaffDetailsItem>.class);
        List<hed__Course_Enrollment__c> allSubjectEnrloments = getSubjectEnrolments(getEnrolmentIds(modelEnrolments));
        List<hed__Course_Enrollment__c> enrolmentsToSendEmail =new List<hed__Course_Enrollment__c>();

        for (ViewModelsWorkforce.SubjectEnrolmentStaffDetailsItem modelEnrolment : modelEnrolments)
        {
            if (modelEnrolment.SubjectEnrolmentId == null || modelEnrolment.SubjectEnrolmentId.startsWithIgnoreCase('UUID'))
            {
                if (modelEnrolment.IsDeleted != null && modelEnrolment.IsDeleted)
                    continue;

                hed__Course_Enrollment__c newEnrolment = new hed__Course_Enrollment__c
                (
                    Role__c = modelEnrolment.RoleName,
                    hed__Contact__c = String.isEmpty(modelEnrolment.StaffId) ? null : modelEnrolment.StaffId,
                    hed__Course_Offering__c = param.Identifier,
                    StudentsAllocated__c = modelEnrolment.AllocatedStudents,
                    hed__Status__c = modelEnrolment.Status == 'Recommended' ? 'Draft' : modelEnrolment.Status,
                    RecordTypeId = facultyRecordTypeId
                );

                createEnrolments.add(newEnrolment);

                if (newEnrolment.hed__Status__c == 'Requested' && newEnrolment.hed__Contact__c != null)
                    enrolmentsToSendEmail.add(newEnrolment);
            }
            else 
            {
                hed__Course_Enrollment__c enrolment = getSubjectEnrolment(modelEnrolment.SubjectEnrolmentId, allSubjectEnrloments);

                if (enrolment == null)
                {
                    Log.error(this.className, 'command', 'Expected to find Subject Enrolment ({0}) in db', new List<String> { modelEnrolment.SubjectEnrolmentId });
                    throw new Exceptions.ApplicationException('Subject enrolment not found. ID: ' + modelEnrolment.SubjectEnrolmentId);
                }

                if (modelEnrolment.IsDeleted != null && modelEnrolment.IsDeleted)
                {
                    removeEnrolments.add(enrolment);
                    continue;
                }

                String currentStatus = enrolment.hed__Status__c;

                enrolment.Role__c = modelEnrolment.RoleName;
                enrolment.hed__Contact__c = modelEnrolment.StaffId;
                enrolment.StudentsAllocated__c = modelEnrolment.AllocatedStudents;
                enrolment.hed__Status__c = modelEnrolment.Status == 'Recommended' ? 'Draft' : modelEnrolment.Status;

                if (enrolment.hed__Status__c == 'Requested' && currentStatus != 'Requested' && enrolment.hed__Contact__c != null)
                    enrolmentsToSendEmail.add(enrolment);

                updateEnrolments.add(enrolment);
            }
        }

        if (createEnrolments.size() > 0)
            Database.insert(createEnrolments);

        if (updateEnrolments.size() > 0)
            Database.update(updateEnrolments);

        if (removeEnrolments.size() > 0)
            Database.delete(removeEnrolments);

        if (enrolmentsToSendEmail.size() > 0)
            result = SubjectEnrolmentService.sendWorkInstructionsEmail(enrolmentsToSendEmail);

        return result;
    }

    private Set<Id> getEnrolmentIds(List<ViewModelsWorkforce.SubjectEnrolmentStaffDetailsItem> enrolments)
    {
        Set<Id> ids = new Set<Id>();

        for (ViewModelsWorkforce.SubjectEnrolmentStaffDetailsItem enrolment : enrolments)
            if (enrolment.SubjectEnrolmentId != null && !enrolment.SubjectEnrolmentId.startsWithIgnoreCase('UUID'))
                ids.add(enrolment.SubjectEnrolmentId);

        return ids;
    }

    private List<hed__Course_Enrollment__c> getSubjectEnrolments(Set<ID> ids)
    {
        return
        [
            SELECT 
                Id, hed__Course_Offering__c, hed__Contact__c, hed__Status__c, StudentsAllocated__c, Role__c, RecordTypeId
            FROM hed__Course_Enrollment__c
            WHERE Id IN :ids
        ];
    }

    private hed__Course_Enrollment__c getSubjectEnrolment(Id id, List<hed__Course_Enrollment__c> enrolments)
    {
        for (hed__Course_Enrollment__c enrolment : enrolments)
            if (enrolment.Id == id)
                return enrolment;
        
        return null;
    }
}