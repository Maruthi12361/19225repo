@isTest 
public class CampaignUpdateStatusBatchTests
{
    @testSetup 
    static void setup() 
    {
        // Arrange
        TestUtilities.setupHEDA();
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        // Create 7 campaigns for both Rollover and Acquisition, 1 In Progress, 4 past and 2 future
        TestDataCampaignMock.createSeries(user, Date.today().addDays(-170), 7, true);
        TestDataCampaignMock.createSeries(user, Date.today().addDays(-170), 7, false);        
        List<Campaign> allCamps = 
            [
                SELECT Id, Type, Program_Portfolio__c, Status, TermStartDate__c, TermLookup__r.Status__c
                FROM Campaign
                WHERE RecordType.DeveloperName = 'TermIntake'
            ];

        for (Campaign camp : allCamps)
            camp.Status = null;
        
        update allCamps;
    }
    
    @isTest 
    static void CampaignsWithBlankStatus_SetsCorrectStatusByTypeAndProgramPortfolio()
    {
        // Act        
        Test.startTest();
        Database.executeBatch(new TermUpdateStatusBatch());
        Test.stopTest(); 
        
        // Assert
        List<Campaign> allCamps = 
            [
                SELECT Id, Type, Program_Portfolio__c, Status, TermStartDate__c, TermLookup__r.Status__c
                FROM Campaign
                WHERE RecordType.DeveloperName = 'TermIntake'
                ORDER BY Program_Portfolio__c, TermStartDate__c
            ];
        Map<String, List<Campaign>> campMap = new Map<String, List<Campaign>>();

        for (Campaign camp : allCamps)
        {
            String key = camp.Type + camp.Program_Portfolio__c;
            List<Campaign> camps = campMap.get(key);

            if (camps == null)
            {
                camps = new List<Campaign>();
                campMap.put(key, camps);
            }

            camps.add(camp);
        }

        for (String key : campMap.keySet())
        {
            List<Campaign> camps = campMap.get(key);
            System.assertEquals(7, camps.size());
            System.assertEquals('Completed', camps[0].Status);
            System.assertEquals('Completed', camps[1].Status);
            System.assertEquals('Completed', camps[2].Status);
            System.assertEquals('Previous', camps[3].Status);
            System.assertEquals('In Progress', camps[4].Status);
            System.assertEquals('Next', camps[5].Status);
            System.assertEquals('Planned', camps[6].Status);
        }
    }
}