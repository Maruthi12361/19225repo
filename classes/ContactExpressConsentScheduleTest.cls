@isTest
private class ContactExpressConsentScheduleTest
{
    @testSetup
    public static void setup()
    {
        TestUtilities.setupHEDA();
        User user = UserService.getByAlias('data');
        TestDataContactMock.createProspectHot(user,  true);
    }

    /**
     * Test a schedule to run a batch to evalute the Express Consent for Contacts
     * with a valid Express Consent Expires date
     */
    @isTest static void test_ContactExpressConsentSchedule()
    {
        // Arrange
        Contact con = ContactService.get([SELECT Id FROM Contact LIMIT 1].Id);
        Datetime runTime = System.now().addMinutes(5);
        String cron = runTime.second() + ' ' + runTime.minute() + ' ' + runTime.hour() + ' ' + 
                      runTime.day() + ' ' + runTime.month() + ' ? ' + runTime.year();

        // Act
        Test.startTest();
        ContactExpressConsentSchedule validConsentSchedule = new ContactExpressConsentSchedule();
        String jobId = System.schedule('test_ContactExpressConsentSchedule_Valid', cron, validConsentSchedule);
        validConsentSchedule.execute(null);
        test.stopTest();

        // Assert
        Contact actual = ContactService.get([SELECT Id FROM Contact LIMIT 1].Id);
        System.assertEquals(true, actual.Express_Consent_Exists__c, 'Incorrect Express Consent for Contact.');
    }
}