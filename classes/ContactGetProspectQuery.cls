public class ContactGetProspectQuery
{
    private Set<String> contactFields = new Set<String>
    {
        'Id', 'OwnerID', 'RecordTypeId', 'Status__c', 'ProgramPortfolio__c', 'LeadSourceReferenceId__c', 'LeadSourceTests__c', 'RecordType.DeveloperName'
    };
    private Set<String> oppFields = new Set<String>{ 'Id', 'Application_Case__c', 'IsClosed', 'OAF_Submit_Count__c', 'RecordTypeId', 'StageName' };

    public ViewModelsAcquisition.Prospect query(Object param)
    {
        return getProspect((ViewModels.Param) param);
    }

    public ViewModelsAcquisition.Prospect queryFiles(Object param)
    {
        return getProspectFiles((ViewModels.Param) param);
    }

    private ViewModelsAcquisition.Prospect getProspect(ViewModels.Param param)
    {
        User user;
        Opportunity opp;
        Case applicationCase;
        Boolean isAuthenticated = !param.IsAuthenticated || param.Identifier == '' || param.Identifier.contains('@') ? false : true;
        Contact contact = getContact(param.Identifier);
        Map<String, Object> prospectMap = (Map<String, Object>) JSON.deserializeUntyped(JSON.serialize(new ViewModelsAcquisition.Prospect()));
        Map<String, Object> prospectDetailsMap = (Map<String, Object>) JSON.deserializeUntyped(JSON.serialize(new ViewModelsAcquisition.Details()));

        if (contact != null && isAuthenticated)
        {
            user = UserService.get(contact.OwnerId);

            if (contact.Opportunities__r.size() > 0)
            {
                opp = contact.Opportunities__r[0];
                Set<ID> fileParamIds = new Set<ID>{ opp.Id };

                if (opp.Application_Case__c != null)
                {
                    fileParamIds.add(opp.Application_Case__c);
                    applicationCase = getApplicationCase(opp.Application_Case__c);
                }

                List<ViewModelsFile.Details> files = ContentDocumentService.getFiles(fileParamIds);

                if (files != null && files.size() > 0)
                    prospectMap.put('files', files);
            }

            for (String fieldName : prospectDetailsMap.keySet())
            {
                if (fieldName == 'Id')
                {
                    prospectDetailsMap.put(fieldName, EncryptionUtil.Encrypt(contact.Id));
                    continue;
                }

                if (opp != null && fieldName == 'Opp_IsLost')
                {
                    prospectDetailsMap.put(fieldName, opp.StageName == 'Closed Lost');
                    continue;
                }

                setMapValue(prospectDetailsMap, contact, opp, applicationCase, user, fieldName);
            }

            prospectDetailsMap.put('NextIntakeCloseDate', getNextIntakeCloseDate(contact.ProgramPortfolio__c));
            prospectDetailsMap.put('LeadSourceReferenceId', contact.LeadSourceReferenceId__c);
            prospectDetailsMap.put('LeadSourceTests', String.isEmpty(contact.LeadSourceTests__c) ? '' : contact.LeadSourceTests__c.replace(';', ','));
        }

        // Set IP address
        prospectDetailsMap.put('IPAddress', getUserIPAddress());
        prospectDetailsMap.put('OAFTermsAndConditionsVersion', System.Label.OAFTermsAndConditionsVersion);

        prospectMap.put('details', prospectDetailsMap);

        Log.info('ContactGetProspectQuery', 'getProspect', 'Created Prospect Object: {0}', new List<String>{ JSON.serialize(prospectMap) });

        return (ViewModelsAcquisition.Prospect) JSON.deserialize(JSON.serialize(prospectMap), ViewModelsAcquisition.Prospect.class);
    }

    private void setMapValue(Map<String, Object> prospectDetailsMap, Contact contact, Opportunity opp, Case applicationCase, User user, String fieldName)
    {
        String objectFieldName = fieldName;

        if (fieldName == 'Cnt_RecordType')
        {
            prospectDetailsMap.put(fieldName, contact.RecordType.DeveloperName);
            return;
        }

        if (objectFieldName.endsWith(Constants.VIEWMODEL_CUSTOMFIELDSUFFIX))
        {
            // Custom field
            objectFieldName = (fieldName.removeEnd(Constants.VIEWMODEL_CUSTOMFIELDSUFFIX)) + '__c';
        }

        if (objectFieldName.startsWith(Constants.VIEWMODEL_CONTACTHEDAPREFIX))
        {
            // Custom HEDA field which requires double underscore, ie: fieldname: hed__Gender__c and ViewModel: Cnt_hed_Gender_c
            objectFieldName = (objectFieldName.replace(Constants.VIEWMODEL_CONTACTHEDAPREFIX, Constants.VIEWMODEL_CONTACTHEDAPREFIX + '_'));
        }

        if (objectFieldName.indexOfIgnoreCase(Constants.VIEWMODEL_CONTACTPREFIX) != -1)
        {
            // Contact field
            prospectDetailsMap.put(fieldName, getFieldValue(contact, objectFieldName));
        }
        else if (opp != null && objectFieldName.indexOfIgnoreCase(Constants.VIEWMODEL_OPPORTUNITYPREFIX) != -1)
        {
            // Opportunity field
            prospectDetailsMap.put(fieldName, getFieldValue(opp, objectFieldName));
        }
        else if (applicationCase != null && objectFieldName.indexOfIgnoreCase(Constants.VIEWMODEL_CASEPREFIX) != -1)
        {
            // Opportunity application case field
            prospectDetailsMap.put(fieldName, getFieldValue(applicationCase, objectFieldName));
        }
        else if (user != null && user.InGroup__c == 'CourseAdvisor' && objectFieldName.indexOfIgnoreCase(Constants.VIEWMODEL_OWNERPREFIX) != -1)
        {
            // User field
            prospectDetailsMap.put(fieldName, getFieldValue(user, objectFieldName));
        }
    }

    private ViewModelsAcquisition.Prospect getProspectFiles(ViewModels.Param param)
    {
        Contact contact = getContact(param.Identifier);
        Map<String, Object> prospectMap = (Map<String, Object>) JSON.deserializeUntyped(JSON.serialize(new ViewModelsAcquisition.Prospect()));

        if (contact != null)
        {
            if (contact.Opportunities__r.size() > 0)
            {
                List<ViewModelsFile.Details> files;
                Opportunity opportunity = contact.Opportunities__r[0];
                Set<ID> fileParamIds = new Set<ID>{ opportunity.Id };

                if (opportunity.Application_Case__c != null)
                    fileParamIds.add(opportunity.Application_Case__c);

                files = ContentDocumentService.getFiles(fileParamIds);

                if (files != null && files.size() > 0)
                    prospectMap.put('files', files);
            }
        }

        Log.info('ContactGetProspectQuery', 'getProspectFiles', 'Created Prospect Object: {0}', new List<String>{ JSON.serialize(prospectMap) });
        return (ViewModelsAcquisition.Prospect) JSON.deserialize(JSON.serialize(prospectMap), ViewModelsAcquisition.Prospect.class);
    }

    private Contact getContact(String identifier)
    {
        try
        {
            Map<String, Object> modelMap = (Map<String, Object>) JSON.deserializeUntyped(JSON.serialize(new ViewModelsAcquisition.Details()));

            for (String fieldName : modelMap.keySet())
            {
                if (!fieldName.startsWith(Constants.VIEWMODEL_CONTACTPREFIX) && !fieldName.startsWith(Constants.VIEWMODEL_CONTACTHEDAPREFIX) && !fieldName.startsWith(Constants.VIEWMODEL_OPPORTUNITYPREFIX))
                    continue;

                if (fieldName == 'Cnt_RecordType' || fieldName == 'Opp_IsLost')
                    continue;

                if (fieldName.endsWith(Constants.VIEWMODEL_CUSTOMFIELDSUFFIX))
                    fieldName = (fieldName.removeEnd(Constants.VIEWMODEL_CUSTOMFIELDSUFFIX)) + '__c';

                // Custom HEDA field which requires double underscore, ie: fieldname: hed__Gender__c and ViewModel: Cnt_hed_Gender_c
                if (fieldName.startsWith(Constants.VIEWMODEL_CONTACTHEDAPREFIX))
                    fieldName = (fieldName.replace(Constants.VIEWMODEL_CONTACTHEDAPREFIX, Constants.VIEWMODEL_CONTACTHEDAPREFIX + '_'));

                if (fieldName.startsWith(Constants.VIEWMODEL_CONTACTPREFIX))
                    contactFields.add(fieldName.substringAfter('_'));
                else if (fieldName.startsWith(Constants.VIEWMODEL_OPPORTUNITYPREFIX))
                    oppFields.add(fieldName.substringAfter('_'));
            }

            String query =
                '{contactFields}, ' +
                '( ' +
                    'SELECT {oppFields} ' +
                    'FROM Opportunities__r ' +
                    'WHERE RecordType.DeveloperName = \'' + Constants.RECORDTYPE_OPPORTUNITY_ACQUISITION + '\' AND IsClosed = false ' +
                ') ';

            query = query.replace('{contactFields}', String.join(new List<String>(contactFields), ', '));
            query = query.replace('{oppFields}', String.join(new List<String>(oppFields), ', '));

            return ContactService.get(identifier, query, false);
        }
        catch (Exception ex)
        {
            Log.error('ContactGetProspectQuery', 'getContact', ex.getMessage() + ': ' + identifier);
            throw new Exceptions.ApplicationException(ex.getMessage() + ': ' + identifier);
        }
    }

    private Case getApplicationCase(Id caseApplicationId)
    {
        return
        [
            SELECT Id, Invoice_Generated__c, Invoice_Processed__c, Invoice_Required__c, FEE_HELP_Processed__c, FeeHelpDetailsRequired__c, Status, Vetting_Complete__c,
                Timetable_Processed__c, Welcome_Letter_Sent__c, WelcomeLetterViewed__c
            FROM Case
            WHERE Id = :caseApplicationId
        ];
    }

    private static String getUserIPAddress()
    {
        String ipAddress = '';

        if (ApexPages.currentPage() != null)
        {
            ipAddress = ApexPages.currentPage().getHeaders().get('True-Client-IP');

            if (String.isEmpty(ipAddress))
                ipAddress = ApexPages.currentPage().getHeaders().get('X-Salesforce-SIP');
        }
        else if (RestContext.request != null)
        {
            ipAddress = RestContext.request.headers.get('X-Salesforce-SIP');
        }

        return ipAddress;
    }

    private Object getFieldValue(Sobject obj, String fieldName)
    {
        fieldName = fieldName.substringAfter('_');
        Schema.DisplayType fieldType = SObjectUtils.getFieldType(obj, fieldName);

        if (fieldType == Schema.DisplayType.MultiPicklist)
        {
            String values = String.valueOf(obj.get(fieldName));

            if (!String.isEmpty(values))
                return values.replace(';', ',');
        }

        return obj.get(fieldName);
    }

    private Date getNextIntakeCloseDate(String programPortfolio)
    {
        if (programPortfolio != Constants.DOMESTIC_MBA && programPortfolio != Constants.INTERNATIONAL_MBA)
            programPortfolio = Constants.DOMESTIC_MBA;

        List<Campaign> campaigns =
            [
                SELECT Id, Name, Program_Portfolio__c, StartDate, ApplicationCutoffDate__c
                FROM Campaign
                WHERE ApplicationCutoffDate__c >= TODAY
                    AND Program_Portfolio__c =:programPortfolio
                    AND Type =: Constants.RECORDTYPE_CAMPAIGN_ACQUISITION
                    AND IsActive = true
                ORDER BY StartDate
                LIMIT 1
            ];

        if (campaigns.size() == 1)
        {
            return campaigns[0].ApplicationCutoffDate__c;
        }

        return null;
    }
}