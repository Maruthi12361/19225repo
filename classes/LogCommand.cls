public class LogCommand extends BaseCommand 
{
    public LogCommand()
    {
        this.saveLogs = false;
    }

    public override List<SObject> validate(List<SObject> params)
    {
        //Commit logs to be saved only if there are logs with errors
        List<AppLog__c> logs = (List<AppLog__c>) params;

        for(AppLog__c log : logs)
        {
            if(log.Type__c ==  String.valueOf(LoggingLevel.ERROR))
                return params;
        }
        return new List<AppLog__c>();
    }

    public override ActionResult command(List<SObject> params)
    {
        try
        {
            uow.registerNew((List<AppLog__c>) params);
            return new ActionResult(ResultStatus.SUCCESS, new List<String>{'Successfully set logs'}); 
        }
        catch(Exception ex)
        {
            //Log error to sytem debug
            String error = String.Format('{0} - {1}, \n{2}', new List<String>{'LogCommand', ex.getMessage(), ex.getStackTraceString()});
            System.debug(LoggingLevel.ERROR, error);
            return new ActionResult(ResultStatus.ERROR, new List<String>{error}); 
        }
    }
}