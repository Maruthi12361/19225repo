@isTest
public class TestDataLeadSourceConfigMock 
{
    public static LeadSourceConfig__c create(User user) 
    {
        return create(user, 'facebook', 'Facebook (Paid)');
    }

    public static LeadSourceConfig__c create(User user, String source, String value) 
    {
        return create(user, '', source, 'facebook', 'paid-social, dynamic-remarketing, lead-gen, instagram', '', '', '', '', '', value);
    }

    public static LeadSourceConfig__c create(User user, String gclid, String source, String content, String medium, String email, String referrer, 
        String campaign, String keyword, String term, String value) 
    {
        LeadSourceConfig__c config = new LeadSourceConfig__c
        (
            Name = 'AMP QA Testing - ' + source,
            GclidValue__c = gclid,
            GclidOperator__c = String.isEmpty(gclid) ? String.valueOf(Enums.Operator.EMPTY) : String.valueOf(Enums.Operator.NOT_EMPTY),
            UtmSourceValue__c = source, 
            UtmSourceOperator__c = String.isEmpty(source) ? '' : source.contains(',') ? String.valueOf(Enums.Operator.ANY_VALUE_IN) : String.valueOf(Enums.Operator.EQUAL),
            UtmContentValue__c = content, 
            UtmContentOperator__c = String.isEmpty(content) ? '' : String.valueOf(Enums.Operator.EQUAL),
            UtmMediumValue__c = medium,
            UtmMediumOperator__c = String.isEmpty(medium) ? '' : String.valueOf(Enums.Operator.ANY_VALUE_IN),
            EmailValue__c = email,
            EmailOperator__c = String.isEmpty(email) ? '' : String.valueOf(Enums.Operator.CONTAINS),
            ReferrerValue__c = referrer,
            ReferrerOperator__c = String.isEmpty(referrer) ? '' : String.valueOf(Enums.Operator.CONTAINS),
            UtmCampaignValue__c = campaign,
            UtmCampaignOperator__c = String.isEmpty(campaign) ? '' : String.valueOf(Enums.Operator.EQUAL),
            UtmKeywordValue__c = keyword,
            UtmKeywordOperator__c = String.isEmpty(keyword) ? '' : String.valueOf(Enums.Operator.CONTAINS),
            UtmTermValue__c = term,
            UtmTermOperator__c =  String.isEmpty(term) ? '' : String.valueOf(Enums.Operator.CONTAINS),
            Value__c = value
        );
        
        System.RunAs(user)
        {
            insert config;
        }

        return config;
    }
}