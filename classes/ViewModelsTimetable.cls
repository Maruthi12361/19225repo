public class ViewModelsTimetable
{
    public class Data implements Interfaces.IViewModel
    {
        public Data()
        {
            Details = new ViewModelsTimetable.Details();
            Courses = new List<ViewModelsTimetable.Course>();
            Subjects = new List<ViewModelsTimetable.Subject>();
        }

        public ViewModelsTimetable.Details Details { get; set; }
        public List<ViewModelsTimetable.Course> Courses { get; set; }
        public List<ViewModelsTimetable.Subject> Subjects { get; set; }
    }

    public class Details implements Interfaces.IViewModel
    {
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public Boolean FeeHelpEligible { get; set; }
        public Decimal FeeHelpCredit { get; set; }
        public Decimal FeesInCredit { get; set; }
        public String RecordType { get; set; }
        public String StudyPersona { get; set; }
    }

    public class Course
    {
        public String CourseId { get; set; }
        public String CoursePlanId { get; set; }
        public String CoursePathway { get; set; }
        public String Status { get; set; }
        public Datetime Commenced { get; set; }
    }

    public class Subject
    {
        public Id Id { get; set; }
        public String TermId { get; set; }
        public Id SubjectId { get; set; }
        public String Grade { get; set; }
        public String Status { get; set; }
        public String Type { get; set; }
        public Decimal Price { get; set; }
        public Decimal Discount { get; set; }
        public Boolean CanEnrol { get; set; }
    }

    public class EnrolmentDetails implements Interfaces.IViewModel
    {
        public EnrolmentDetails()
        {
            Enrolments = new List<ViewModelsTimetable.Enrolment>();
        }

        public String Reference { get; set; }
        public String StudyPersona { get; set; }
        public TimetablePayment TimetablePayment { get; set; }

        public List<ViewModelsTimetable.Enrolment> Enrolments { get; set; }
    }

    public class Enrolment
    {
        public String SubjectId { get; set; }
        public String TermId { get; set; }
        public String Type { get; set; }
        public String Status { get; set; }

        public Enrolment(String type, String status, String termId, String subjectId)
        {
            this.SubjectId = subjectId;
            this.TermId = termId;
            this.Type = type;
            this.Status = status;
        }
    }

    public class TimetablePayment
    {
        public String UpfrontPaymentType { get; set; }
        public String CardNumber { get; set; }
        public String CardExpiryMonth { get; set; }
        public String CardExpiryYear { get; set; }
        public String CardCVV { get; set; }
    }

    public class EnrolmentActions
    {
        public List<ViewModelsTimetable.EnrolmentAction> AllActions { get; set; }

        public EnrolmentActions(){}

        public EnrolmentActions(List<hed__Course_Enrollment__c> subjectEnrolments)
        {
            AllActions = new List<ViewModelsTimetable.EnrolmentAction>();

            for (hed__Course_Enrollment__c subjectEnrolment : subjectEnrolments)
            {
                addAction(Enums.DatabaseAction.NONE, subjectEnrolment);
            }
        }

        public void addAction(Enums.DatabaseAction action, hed__Course_Enrollment__c subjectEnrolment)
        {
            addAction(new EnrolmentAction(action, subjectEnrolment));
        }

        public void addAction(EnrolmentAction enrolmentAction)
        {
            AllActions.add(enrolmentAction);
        }

        public List<ViewModelsTimetable.EnrolmentAction> getEnrolmentActions(Enums.DatabaseAction typeAction)
        {
            List<ViewModelsTimetable.EnrolmentAction> toReturn = new List<ViewModelsTimetable.EnrolmentAction>();
            if (allActions == null)
                return toReturn;

            for (ViewModelsTimetable.EnrolmentAction action : AllActions)
                if (action.Action == typeAction)
                    toReturn.add(action);

            return toReturn;
        }
    }

    public class EnrolmentAction
    {
        public hed__Course_Enrollment__c SubjectEnrolment { get; set; }
        public Enums.DatabaseAction Action { get; set; } //ADD, NONE, REMOVE, EDIT, UNKNOWN

        public EnrolmentAction(Enums.DatabaseAction action, hed__Course_Enrollment__c subjectEnrolment)
        {
            this.Action = action;
            this.SubjectEnrolment = subjectEnrolment;
        }
    }

    public class SaveResult implements Interfaces.IViewModel
    {
        public SaveResult(){}

        public Id ContactId { get; set; }
        public String StudyPersona { get; set; }
        public Integer PayloadSize { get; set; }
        public Integer InsertSubjectOfferings { get; set; }
        public Integer InsertSubjectEnrolments { get; set; }
        public Integer DeleteSubjectEnrolments { get; set; }
        public Integer UpdateSubjectEnrolments { get; set; }
        public String InitialLimits { get; set; }     // Captures SQL/DML/CPU across initial pre-enrolment read/updates. i.e: Persona, CreditCard, Contact retrieval
        public String PayloadLimits { get; set; }     // Captures SQL/DML/CPU across the bulk of the payload performing the loop across each enrolment
        public String TotalUsedLimits { get; set; }   // Provides a total of SQL/DML/CPU aross the entire command
        public String MaxAvailbleLimits { get; set; } // Shows the maximum SQL/DML/CPU that is available
        public EnrolmentDetails Payload { get; set; }

        public List<String> getResultDescription()
        {
            return new List<String>
            {
                'Completed TimetableSaveComamand for ContactId: ' + ContactId + ', StudyPersona: ' + StudyPersona,
                'Enrolment Payload Size     :' + String.valueOf(PayloadSize).leftPad(10, ' '),
                'Insert SubjectOfferings    :' + String.valueOf(InsertSubjectOfferings).leftPad(10, ' '),
                'Insert SubjectEnrolments   :' + String.valueOf(InsertSubjectEnrolments).leftPad(10, ' '),
                'Delete SubjectEnrolments   :' + String.valueOf(DeleteSubjectEnrolments).leftPad(10, ' '),
                'Update SubjectEnrolments   :' + String.valueOf(UpdateSubjectEnrolments).leftPad(10, ' '),
                'AIBLimits.firstSnapshot    :' + InitialLimits,    
                'AIBLimits.payloadSnapshot  :' + PayloadLimits,   
                'AIBLimits.totalUsedSnapshot:' + TotalUsedLimits,  
                'AIBLimits.maxAvailableCount:' + MaxAvailbleLimits,
                'Payload:' + JSON.serializePretty(Payload)
            };
        }
    }
}