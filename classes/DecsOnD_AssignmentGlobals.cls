public class DecsOnD_AssignmentGlobals
{
    public static final String ASSIGNMENT_OPTIONS_PARAM = 'AssignmentOptions';

    // Constants related to weighted load balancing
    public static final String WEIGHTED_OPTION = 'Weighted';

    public static final sObjectField USER_DO_NOT_ASSIGN_FIELD = null;
    public static final sObjectField ASSIGNMENT_TIER_FIELD = User.DecsOnD_Assignment_Tier__c;
    public static final sObjectField ASSIGNMENT_WEIGHT_FIELD = User.DecsOnD_Assignment_Weight__c;

    // Constants related to skills-based assignment
    public static final String SKILLS_BASED_OPTION = 'SkillsBased';
    public static final String COURSE_ADVISOR_SKILLSET_TYPE = 'CourseAdvisor';

    // Map of inferred skills to the corresponding fields on the Contact object. The skills names
    // (keys in the maps) should be the same as prefixes of SubType__c values on Skillset__c object
    public static final Map<String, sObjectField> CONTACT_INFERRED_SKILLS_FIELDS = new Map<String, sObjectField>
    {
        'Entry Pathway' => Contact.InferredEntryPathway__c,
        'Gender' => Contact.InferredGender__c,
        'Portfolio' => Contact.InferredPortfolio__c
    };

    public static final Map<String, sObjectField> CONTACT_INFERRED_CC_SKILLS_FIELDS = new Map<String, sObjectField>
    {
        'Portfolio' => Contact.InferredPortfolio__c
    };

    // Mapping of Weighting picklist values to the percentage probability that the skillset will be used for matching purposes
    public static final Map<String, Integer> WEIGHTING_VALUES_MAP = new Map<String, Integer>
    {
        Constants.SKILLSET_WEIGHTING_ALWAYS => 100,
        'Frequently' => 90,
        'Often' => 75,
        'Sometimes' => 50,
        'Occasionally' => 25,
        'Rarely' => 10,
        Constants.SKILLSET_WEIGHTING_NEVER => 0
    };

    public static final Set<sObjectField> EXTRA_USER_FIELDS = initExtraUserFields();

    private static Set<sObjectField> initExtraUserFields()
    {
        Set<sObjectField> fields = new Set<sObjectField>{ User.TimeZoneSidKey, ASSIGNMENT_TIER_FIELD, ASSIGNMENT_WEIGHT_FIELD };

        if (USER_DO_NOT_ASSIGN_FIELD != null)
        {
            fields.add(USER_DO_NOT_ASSIGN_FIELD);
        }

        return fields;
    }

    // Deprecated -- maintain for backward compatibility
    public static final String WEIGHTED_ROUND_ROBIN_MODE = 'WeightedRoundRobin';
}