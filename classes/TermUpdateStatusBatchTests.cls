@isTest
public class TermUpdateStatusBatchTests
{
    @testSetup
    static void setup()
    {
        // Arrange
        TestUtilities.setupHEDA();
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Account account = TestDataAccountMock.createDepartmentBusiness(user);
        // Create 7 terms, 1 current, 3 past and 3 future. Leave Term status blank.
        TestDataTermMock.createSeries(user, Date.today().addDays(-170), 7, account.Id, false);
        account = TestDataAccountMock.createDepartmentResearch(user);
        TestDataTermMock.createSeries(user, Date.today().addDays(-170), 7, account.Id, false);
    }

    @isTest
    static void TermsWithBlankStatus_SetsCorrectStatusByAccount()
    {
        // Act
        Test.startTest();
        Database.executeBatch(new TermUpdateStatusBatch());
        Test.stopTest();

        // Assert
        List<hed__Term__c> allTerms =
        [
            SELECT Id, hed__Start_Date__c, Status__c, hed__Account__c
            FROM hed__Term__c
            ORDER BY hed__Account__c, hed__Start_Date__c
        ];
        Map<Id, List<hed__Term__c>> termsMap = new Map<Id, List<hed__Term__c>>();

        for (hed__Term__c term : allTerms)
        {
            List<hed__Term__c> terms = termsMap.get(term.hed__Account__c);

            if (terms != null)
                terms.add(term);
            else
            {
                terms = new List<hed__Term__c> { term };
                termsMap.put(term.hed__Account__c, terms);
            }
        }

        for (Id key : termsMap.keySet())
        {
            List<hed__Term__c> terms = termsMap.get(key);
            System.assertEquals(7, terms.size());
            System.assertEquals('Past', terms[0].Status__c);
            System.assertEquals('Past', terms[1].Status__c);
            System.assertEquals('Previous', terms[2].Status__c);
            System.assertEquals('Current', terms[3].Status__c);
            System.assertEquals('Next', terms[4].Status__c);
            System.assertEquals('Future', terms[5].Status__c);
            System.assertEquals('Future', terms[6].Status__c);
        }
    }
}