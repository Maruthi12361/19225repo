public class TermUpdateStatusCommand extends BaseCommand
{
    public override List<SObject> validate(List<SObject> items)
    {
        return items;
    }

    public override ActionResult command(List<SObject> items)
    {
        List<hed__Term__c> updateTerms = new List<hed__Term__c>();
        Map<Id, List<hed__Term__c>> termsMap = new Map<Id, List<hed__Term__c>>();

        for (hed__Term__c term : (List<hed__Term__c>) items)
        {
            List<hed__Term__c> terms = termsMap.get(term.hed__Account__c);

            if (terms != null)
                terms.add(term);
            else
            {
                terms = new List<hed__Term__c> { term };
                termsMap.put(term.hed__Account__c, terms);
            }
        }

        for (Id key : termsMap.keySet())
        {
            List<hed__Term__c> terms = termsMap.get(key);
            List<Date> termDates = getTermDates(terms);
            Date prevTermDate = termDates[0];
            Date curTermDate = termDates[1];
            Date nextTermDate = termDates[2];

            if (prevTermDate == null && curTermDate == null && nextTermDate == null)
            {
                Log.info(this.className, 'command', 'There are no enough Term records to update for ' + key);
                continue;
            }

            for (hed__Term__c term : terms)
            {
                Boolean isDirty = false;

                if (prevTermDate != null)
                {
                    if (term.hed__Start_Date__c < prevTermDate)
                    {
                        if (term.Status__c != 'Past')
                        {
                            term.Status__c = 'Past';
                            isDirty = true;
                        }
                    }
                    else if (term.hed__Start_Date__c == prevTermDate)
                    {
                        if (term.Status__c != 'Previous')
                        {
                            term.Status__c = 'Previous';
                            isDirty = true;
                        }
                    }
                }

                if (curTermDate != null)
                {
                    if (term.hed__Start_Date__c == curTermDate)
                    {
                        if (term.Status__c != 'Current')
                        {
                            term.Status__c = 'Current';
                            isDirty = true;
                        }
                    }
                }

                if (nextTermDate != null)
                {
                    if (term.hed__Start_Date__c == nextTermDate)
                    {
                        if (term.Status__c != 'Next')
                        {
                            term.Status__c = 'Next';
                            isDirty = true;
                        }
                    }
                    else if (term.hed__Start_Date__c > nextTermDate)
                    {
                        if (term.Status__c != 'Future')
                        {
                            term.Status__c = 'Future';
                            isDirty = true;
                        }
                    }
                }

                if (isDirty)
                    updateTerms.add(term);
            }
        }

        if (updateTerms.size() > 0)
            update updateTerms;

        return new ActionResult(ResultStatus.SUCCESS);
    }

    private List<Date> getTermDates(List<hed__Term__c> terms)
    {
        Date today = System.today();
        Date prevTermDate, curTermDate, nextTermDate;

        for (hed__Term__c term : terms)
        {        
            if (term.hed__Start_Date__c <= today)
            {
                if (curTermDate == null)
                    curTermDate = term.hed__Start_Date__c;
                else
                {
                    if (curTermDate < term.hed__Start_Date__c)
                    {
                        prevTermDate = curTermDate;
                        curTermDate = term.hed__Start_Date__c;
                    }
                }
            }
            else
            {
                if (nextTermDate == null)
                {
                    nextTermDate = term.hed__Start_Date__c;
                    break;
                }
            }
        }

        Log.info(this.className, 'getTermDates', 'Query dates from Terms, prev term: {0}, current term: {1}, next term: {2}',
                 new List<String> { String.valueOf(prevTermDate), String.valueOf(curTermDate), String.valueOf(nextTermDate) });
        return new List<Date>{prevTermDate, curTermDate, nextTermDate};
    }
}