@RestResource(urlMapping='/upload/file/prospect/*')
global class ProspectUploadFileController
{
    @HttpPost
    global static void save()
    {
        try
        {
            String reference = '';
            RestRequest req = RestContext.request;
            RestResponse res = RestContext.response;
            ViewModelsFile.Upload att = new ViewModelsFile.Upload();

            if (req.headers.containsKey('Reference'))
            	att.Reference = req.headers.get('Reference');

            if (req.headers.containsKey('FileName'))
                att.FileName = req.headers.get('FileName');

            if (req.headers.containsKey('Description'))
                att.Type = req.headers.get('Description');

            att.Content = req.requestBody;
            Integer fileSize = (att.Content == null ? 0 : att.Content.size()) / 1024 / 1024;

            if (fileSize > Constants.MAXIMUM_SIZE_OF_ATTACHMENT )
            {
                String message = 'Attachment size {0}MB excceeds MAXIMUM_SIZE_OF_ATTACHMENT of 25MB';
                Log.info('ProspectUploadFileController', 'save', message, new List<String>{String.valueOf(fileSize)});
                throw new Exceptions.ApplicationException(message);
            }

            Log.info('ProspectUploadFileController', 'save', 'Reference value {0} \n FileName: {1} \n FileSize: {2}MB', new List<String>{ att.Reference, att.FileName, String.valueOf(fileSize) });

            ActionResult result = ContactService.uploadAttachment(att);

            if (result.Status == ResultStatus.SUCCESS)
            {
                ViewModels.Param param = new ViewModels.Param();
                param.Identifier = att.Reference;
                param.IsAuthenticated = att.Reference == '' || att.Reference.contains('@') ? false : true;

                RestContext.response.addHeader('Content-Type', 'application/json');
                RestContext.response.responseBody = Blob.valueOf(JSON.serialize(ContactService.getProspect(param)));
                RestContext.response.statusCode = 200;
            }
            else
            {
                Log.error('ProspectUploadFileController', 'save', result.message);
                throw new Exceptions.ApplicationException(result.message);
            }
        }
        catch (Exception ex)
        {
            Map<String, String> errorMap = new Map<String, String>
            {
                'Message' => ex.getMessage(),
                'Details' => ex.getStackTraceString()
            };

            RestContext.response.addHeader('Content-Type', 'application/json');
            RestContext.response.responseBody = Blob.valueOf(JSON.serialize(errorMap));
            RestContext.response.statusCode = 400;
        }
    }
}