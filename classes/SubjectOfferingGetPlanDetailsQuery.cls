public class SubjectOfferingGetPlanDetailsQuery 
{
    private static Map<String, String> roleCapacityMap = new Map<String, String>
    {
        'Head of Discipline' => 'StudentsTaughtPerSC__c',
        'Subject Coordinator' => 'StudentsTaughtPerSC__c',
        'Online Facilitator' => 'StudentsTaughtPerOLF__c',
        'Online Facilitator (SC)' => 'StudentsTaughtPerOLF__c',
        'Marker' => 'StudentsTaughtPerOLF__c'
    };

    public ViewModelsWorkforce.SubjectOfferingStaffDetails query(Object param)
    {
        return getSubjectOfferingDetails((ViewModels.Param) param);
    }

    private ViewModelsWorkforce.SubjectOfferingStaffDetails getSubjectOfferingDetails(ViewModels.Param param)
    {
        Log.info('SubjectOfferingGetPlanDetailsQuery', 'getSubjectOfferingDetails', 'Getting subject offering details with following param values: ' + JSON.serialize(param));

        ViewModelsWorkforce.SubjectOfferingStaffDetails offeringDetails = new ViewModelsWorkforce.SubjectOfferingStaffDetails();
        List<hed__Course_Offering__c> subjectOfferings = getSubjectOfferings(param.Identifier);
        List<hed__Course_Enrollment__c> facultySubjectEnrolments = getFacultySubjectEnrolments(SObjectUtils.getIds(subjectOfferings, 'Id'));
        List<hed__Affiliation__c> appointments =  getStaffAppointmentAffiliations(SObjectUtils.getIds(facultySubjectEnrolments, 'hed__Contact__c'));

        for (hed__Course_Offering__c offering : subjectOfferings)
        {
            ViewModelsWorkforce.SubjectOfferingStaffDetailsItem offeringItem = new ViewModelsWorkforce.SubjectOfferingStaffDetailsItem();

            offeringItem.SubjectOfferingId = offering.Id;
            offeringItem.SubjectName = offering.hed__Course__r.Name;
            offeringItem.SubjectCode = offering.hed__Course__r.hed__Course_ID__c;
            offeringItem.StartDate = offering.hed__Start_Date__c;
            offeringItem.EndDate = offering.hed__End_Date__c;

            offeringItem.ActualStudents = (Integer) offering.ActualStudents__c;
            offeringItem.ProjectedStudents = (Integer) offering.ProjectedStudents__c;
            offeringItem.Capacity = (Integer) offering.hed__Capacity__c;
            offeringItem.ProjectedCapacity = (Integer) offering.ProjectedCapacity__c;
            offeringItem.HealthStatus = offering.PlanningHealth__c;
            offeringItem.RoleStudentCapacities = getRoleCapacities(offering.hed__Course__r);

            List<hed__Course_Enrollment__c> facultyEnrolments = getFacultyEnrolments(offering.Id, facultySubjectEnrolments);

            for (hed__Course_Enrollment__c enrolment : facultyEnrolments)
            {
                hed__Affiliation__c appointment =  getStaffAppointment(enrolment.hed__Contact__c,enrolment.Role__c, appointments);
                ViewModelsWorkforce.SubjectEnrolmentStaffDetailsItem enrolmentItem = new ViewModelsWorkforce.SubjectEnrolmentStaffDetailsItem();

                enrolmentItem.SubjectEnrolmentId = enrolment.Id;
                enrolmentItem.StaffId = enrolment.hed__Contact__r.Id;
                enrolmentItem.StaffName = enrolment.hed__Contact__r.Name;
                enrolmentItem.AllocatedStudents = (Integer) enrolment.StudentsAllocated__c;
                enrolmentItem.Status = enrolment.hed__Status__c;
                enrolmentItem.StaffPrerequisitesCompleted = appointment == null ? false : appointment.PrerequisitesCompleted__c;

                if (!String.isEmpty(enrolment.Role__c))
                {
                    enrolmentItem.RoleName = enrolment.Role__c;
                    enrolmentItem.MaxCapacity = enrolment.Role__c.containsIgnoreCase('Online Facilitator') ? 
                        (Integer) offering.hed__Course__r.StudentsTaughtPerOLF__c : (Integer) offering.hed__Course__r.StudentsTaughtPerSC__c;
                }

                offeringItem.SubjectEnrolments.add(enrolmentItem);
            }

            offeringDetails.SubjectOfferings.add(offeringItem);
        }

        return offeringDetails;
    }

    private List<hed__Course_Offering__c> getSubjectOfferings(String identifier)
    {
        List<hed__Course_Offering__c> subjectOfferings;
        String query =
            'SELECT ' +
                'ActualStudents__c, Name, PlanningHealth__c, ProjectedCapacity__c, ProjectedStudents__c, SubjectCode__c, hed__Course__r.hed__Course_ID__c, ' +
                'hed__End_Date__c, Id, hed__Capacity__c, hed__Course__r.AQFLevel__c, hed__Course__r.HeadOfDiscipline__c, hed__Course__r.Name, ' +
                'hed__Course__r.StudentsTaughtPerOLF__c, hed__Course__r.StudentsTaughtPerSC__c, hed__Start_Date__c, hed__Term__c ' +
            'FROM hed__Course_Offering__c ';

        if (identifier.startsWith(Constants.ENTITYID_TERM))
            query = query + ' WHERE hed__Term__c = :identifier ';
        else 
            query = query + ' WHERE Id = :identifier ';

        query = query + 'ORDER BY hed__Course__r.Name';

        subjectOfferings =  Database.query(query);

        return subjectOfferings;
    }

    private static List<hed__Course_Enrollment__c> getFacultySubjectEnrolments(Set<ID> subjectOfferingIds)
    {
        List<hed__Course_Enrollment__c> subjectEnrolments = new List<hed__Course_Enrollment__c>();

        subjectEnrolments =
        [
            SELECT Id, IsDeleted, Role__c, Sequence__c, StudentsAllocated__c, hed__Contact__c, hed__Contact__r.Email, hed__Contact__r.FirstName, 
                hed__Contact__r.Id, hed__Contact__r.LastName, hed__Contact__r.Name, hed__Course_Offering__c, hed__Status__c
            FROM hed__Course_Enrollment__c
            WHERE hed__Course_Offering__c IN :subjectOfferingIds AND  RecordType.DeveloperName = 'Faculty' AND IsDeleted = false
            ORDER BY Sequence__c, hed__Contact__r.Name
        ];

        return subjectEnrolments;
    }

    private static  List<hed__Course_Enrollment__c> getFacultyEnrolments(Id subjectOfferingId, List<hed__Course_Enrollment__c> facultySubjectEnrolments)
    {
        List<hed__Course_Enrollment__c> facultyEnrolments = new List<hed__Course_Enrollment__c>();

        for (hed__Course_Enrollment__c enrolment : facultySubjectEnrolments)
        {
            if (enrolment.hed__Course_Offering__c == subjectOfferingId)
                facultyEnrolments.add(enrolment);
        }

        return facultyEnrolments;
    }

    private static List<hed__Course_Enrollment__c> getSubjectOfferingSubjectEnrolments(ID subjectOfferingId, List<hed__Course_Enrollment__c> subjectEnrolments)
    {
        List<hed__Course_Enrollment__c> subjectOfferingSubjectEnrolments = new List<hed__Course_Enrollment__c>();

        for (hed__Course_Enrollment__c subjectEnrolment : subjectEnrolments)
        {
            if (subjectEnrolment.Id == subjectOfferingId)
                subjectOfferingSubjectEnrolments.add(subjectEnrolment);
        }

        return subjectOfferingSubjectEnrolments;
    }

    private static List<ViewModelsWorkforce.RoleStudentCapacity> getRoleCapacities(hed__Course__c subjectVersion)
    {
        List<ViewModelsWorkforce.RoleStudentCapacity> roleCapacities = new List<ViewModelsWorkforce.RoleStudentCapacity>();

        for (String key : roleCapacityMap.keySet())
        {
            ViewModelsWorkforce.RoleStudentCapacity roleCapacity = new ViewModelsWorkforce.RoleStudentCapacity();
            roleCapacity.RoleName = key;
            roleCapacity.Capacity = String.valueOf(subjectVersion.get(roleCapacityMap.get(key)));

            roleCapacities.add(roleCapacity);
        }

        return roleCapacities;
    }

    private List<hed__Affiliation__c> getStaffAppointmentAffiliations(Set<Id> contactIds)
    {
        return
        [
            SELECT Id, hed__Contact__c, hed__Account__c, hed__Account__r.Name, hed__Role__c, PrerequisitesCompleted__c, hed__Status__c
            FROM hed__Affiliation__c
            WHERE 
                RecordType.DeveloperName = 'Appointment' AND hed__Contact__c IN :contactIds AND 
                hed__Account__r.Name = :Constants.ACCOUNT_INSTITUTION_AIB AND hed__Status__c = :Constants.AFFILIATION_APPOINTMENTSTATUSES
        ];
    }

    private hed__Affiliation__c getStaffAppointment(Id contactId, String role, List<hed__Affiliation__c> appointmentAffiliations)
    {
        List<hed__Affiliation__c> appointments = new List<hed__Affiliation__c>();

        for (hed__Affiliation__c appointment : appointmentAffiliations)
        {
            if (appointment.hed__Contact__c == contactId && appointment.hed__Role__c == role)
                appointments.add(appointment);
        }

        if (appointments.size() > 0)
            return appointments[0];

        return null;
    }
}