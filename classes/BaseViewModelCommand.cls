public abstract class BaseViewModelCommand extends BaseComponent
{
    protected UnitOfWork uow 
    { 
        get 
        { 
            return UnitOfWork.current;
        }
    }
    
    public BaseViewModelCommand()
    {
        super();
    }
    
    protected abstract List<Interfaces.IViewModel> validate(List<Interfaces.IViewModel> items);

    protected abstract ActionResult command(List<Interfaces.IViewModel> items);

    public ActionResult execute(Interfaces.IViewModel item)
    {
        List<Interfaces.IViewModel> items = new List<Interfaces.IViewModel>();
        items.add(item);
        return execute(items);
    }

    public ActionResult execute(List<Interfaces.IViewModel> items)
    {   
        Boolean isValid = true;
        Boolean uowScope = false;
        ActionResult result = new ActionResult(ResultStatus.SUCCESS);
        List<Interfaces.IViewModel> validItems;

        if (items == null)
            throw new InvalidParameterValueException('items', 'Command execution was requested but was provided with no information to act on');

        if (!isEnabled)
        {
            Log.info(this.className, 'execute', 'Skipping command. Disabled through configuration.'); 
            isValid = false;
        }  

        if(isValid)
        {
            // Validation
            try
            {
                Log.info(this.className, 'execute', 'Validating command.');
                validItems = validate(items);
                
                if (validItems.size() == 0)
                {
                    Log.info(this.className, 'execute', 'Validation: No objects meeting conditions to process.');   
                    isValid = false; 
                }
            }
            catch(Exception ex)
            {    
                exc = ex;       
                isValid = false; 
                Log.error(this.className, 'execute', 'Error validating command: ', ex);
                result = new ActionResult(ResultStatus.ERROR, 'Error validating command ' + this.className + ': ' + StringUtils.getExceptionMessage(ex));
            } 
        }

        if(isValid)
        {
            // Command
            try
            {
                if (UnitOfWork.current == null)
                {
                    uowScope = true;
                    UnitOfWork.current = new UnitOfWork();
                }
                
                Log.info(this.className, 'execute', 'Executing command.');
                result = command(validItems);
                
                if (uowScope)
                {
                    UnitOfWork.current.commitWork();
                    uowScope = false;
                }
            }
            catch (Exception ex)
            {        
                exc = ex;   
                isValid = false; 
                Log.error(this.className, 'execute', 'Error executing command: ', ex);
                result = new ActionResult(ResultStatus.ERROR, 'Error executing command ' + this.className + ': ' + StringUtils.getExceptionMessage(ex));
            }
        }

        if (!trigger.isExecuting && this.saveLogs)
            Log.save();
          
        if (exc != null && Test.isRunningTest() && TestUtilities.throwOnException)
            throw exc;

        return result;
    }
}