public class SMSSendViaSMSMagicCommand extends BaseViewModelCommand
{
    public override List<Interfaces.IViewModel> validate(List<Interfaces.IViewModel> params)
    {
        return params;
    }

    public override ActionResult command(List<Interfaces.IViewModel> params)
    {
        ActionResult result = new ActionResult();

        for (ViewModelsSMS.Details param: (List<ViewModelsSMS.Details>) params)
        {
            if (ConfigUtility.isSandbox())
                return EmailService.sendToRecipient(Constants.APIPROVIDER_SENDGRID, Label.SMTPOverrideEmail, param.ToPhoneNumber,'SMS Message Via SMSMagic', UserInfo.getUserEmail(), param.FromName, param.Message, null, null);

            Http http = new Http();
            HttpRequest request = new HttpRequest();
            ViewModelsSMS.SMSMagicPayload body = new ViewModelsSMS.SMSMagicPayload(param.FromName, param.ToPhoneNumber, param.Message);

            request.setEndpoint(Constants.SMSMAGIC_SEND_ENDPOINT);
            request.setMethod('POST');
            request.setHeader('apiKey', Constants.SMSMAGIC_APIKEY);
            request.setHeader('Accept', 'application/json');
            request.setHeader('Content-Type', 'application/json');
            request.setHeader('Cache-Control', 'no-cache');
            request.setTimeout(120000);
            request.setBody(JSON.serialize(body));
            HttpResponse response = http.send(request);

            List<Object> errors;
            List<String> messages = new List<String>();

            if (response.getStatusCode() != 200)
            {
                Map<String, Object> responseBody = (Map<String, Object>)JSON.deserializeUntyped(response.getBody());
                errors = (List<Object>)responseBody.get('errors');

                for (Object error : errors)
                {
                    messages.add(String.valueOf(error));
                }

                Log.error(this.className, 'command', 'Could not send message. Error Code: {0}, Error Body: {1}', new String[]{ String.valueOf(response.getStatusCode()), response.getBody() });

                result.status = ResultStatus.ERROR;
                result.messages = messages;
            }
        }

        return result;
    }
}