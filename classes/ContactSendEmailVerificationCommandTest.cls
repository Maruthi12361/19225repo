@isTest
public class ContactSendEmailVerificationCommandTest 
{
    @testSetup
    static void setup()
    {
        TestUtilities.setupHEDA();
    }

    @isTest 
    static void SendEmailVerification_EmailSentAndTaskActivityCreated() 
    {
        // Arrange 
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact contact = TestDataContactMock.createProspectCold(user, '0455548889');

        // Act
        Test.startTest();
        ActionResult result = new ContactSendEmailVerificationCommand().execute(new List<Contact> { contact }, null, null);
        Test.stopTest();
        Task createdTask = [SELECT Id, WhoId, Subject FROM Task WHERE WhoId = :contact.Id];

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Unexpected status value'); 
        System.assertEquals(contact.Id, createdTask.WhoId, 'Unexpected who id value'); 
        System.assertEquals('Email Verification Sent', createdTask.Subject, 'Unexpected subject value'); 
    }
}