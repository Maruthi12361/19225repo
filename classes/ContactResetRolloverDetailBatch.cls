public class ContactResetRolloverDetailBatch extends BaseBatchCommand
{
    public String query =
        'SELECT Id ' +
        'FROM Contact ' +
        'WHERE Current_Course_Status__c <> null';
    public String cerebroIDs;

    public ContactResetRolloverDetailBatch()
    {
    }

    public ContactResetRolloverDetailBatch(String stringParam)
    {
        if(stringParam != null && String.isNotBlank(stringParam))
        {
            query += ' AND Cerebro_Student_ID__c IN (' + stringParam + ')';
            cerebroIDs = stringParam;
        }
    }

    public override Database.QueryLocator query(Database.BatchableContext BC)
    {
        return Database.getQueryLocator(query);
    }

    public override void command(Database.BatchableContext BC, List<sObject> scope)
    {
        for (Contact contact : (List<Contact>) scope)
        {
            contact.Current_Course_Status__c = null;
            contact.FeeHelp_Eligibility__c = null;
            contact.Course_Start_Date__c = null;
            contact.Next_Followup_Date__c = null;
        }

        update scope;
    }

    public override void finalise(Database.BatchableContext BC)
    {
        ContactUpdateRolloverDetailBatch batch;

        if(cerebroIDs == null)
            batch = new ContactUpdateRolloverDetailBatch();
        else
            batch = new ContactUpdateRolloverDetailBatch(cerebroIDs);

        database.executebatch(batch);
   }
}