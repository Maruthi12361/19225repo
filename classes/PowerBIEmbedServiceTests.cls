@isTest
private class PowerBIEmbedServiceTests
{
    public static testMethod void powerBIEmbedContact()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact testContact = TestDataContactMock.createStudent(user, 'A000000000', 'A000000000@aib.edu.au', true, false);

        PageReference pageRef = Page.PowerBIEmbedContactAcquisition;
        ApexPages.StandardController sc = new ApexPages.StandardController(testContact);
        pageRef = new PageReference(testContact.Id);
        Test.setCurrentPage(pageRef);

        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());

        // Act
        Test.startTest();
        PowerBIEmbedService pbi = new PowerBIEmbedService(sc);
        pbi.storeToken();
        Test.stopTest();

        // Assert
        System.assertNotEquals(null, pbi.orgCache);
        System.assertEquals('Contact', pbi.mappedObject);
    }
}