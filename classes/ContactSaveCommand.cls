public class ContactSaveCommand extends BaseComponent
{
    @testVisible
    private static final string unsubscribeEmailField = 'Cnt_HasOptedOutOfEmail';
    @testVisible
    private static final string unsubscribePhoneField = 'Cnt_DoNotCall';
    @testVisible
    private static final string errorNoContactFound = 'No contact found with {0} identifier';
    @testVisible
    private static final string errorUnexpectedNumberOfFields = 'Unexpected number of fields. Expecting at least one field and maximum of two fields in payload';
    @testVisible
    private static final string errorUnexpectedField = 'Unexpected field in payload. Expected {0} and {1} fields';

    public Boolean validate(ViewModels.Param param)
    {
        Contact contact;
        Map<String, Object> payloadMap;

        if (param == null)
        {
            Log.error(this.className, 'validate', Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Param object' });
            throw new Exceptions.InvalidParameterException(String.format(Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Param object'}));
        }

        contact = ContactService.get(param.Identifier, 'Id', false);

        if (contact == null)
        {
            Log.error(this.className, 'validate', errorNoContactFound, new List<String>{ param.Identifier });
            throw new Exceptions.InvalidParameterException(String.format(errorNoContactFound, new List<String> { param.Identifier }));
        }

        if (String.isEmpty(param.payload))
        {
            Log.error(this.className, 'validate', Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Payload' });
            throw new Exceptions.InvalidParameterException(String.format(Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Payload' }));
        }

        try
        {
            payloadMap = (Map<String, Object>)JSON.deserializeUntyped(param.payload);
        }
        catch (Exception ex)
        {
            Log.error(this.className, 'validate', ex.getMessage(), ex);
            throw new Exceptions.InvalidParameterException(ex.getMessage());
        }

        if (payloadMap.size() == 0 || payloadMap.size() > 2)
            throw new Exceptions.InvalidParameterException(errorUnexpectedNumberOfFields);
        else if (payloadMap.size() == 1 && payloadMap.containsKey(unsubscribePhoneField) || payloadMap.containsKey(unsubscribeEmailField))
            return true;
        else if (payloadMap.containsKey(unsubscribePhoneField) && payloadMap.containsKey(unsubscribeEmailField))
            return true;

        throw new Exceptions.InvalidParameterException(String.format(errorUnexpectedField, new List<String> { unsubscribeEmailField, unsubscribePhoneField }));
    }

    public ActionResult command(ViewModels.Param param)
    {
        Boolean contactUpdated = false;
        validate(param);

        Contact contact = ContactService.get(param.Identifier, 'Id, HasOptedOutOfEmail, DoNotCall', false);
        Map<String, Object> payloadMap = (Map<String, Object>)JSON.deserializeUntyped(param.payload);

        for (String fieldName : payloadMap.keySet())
        {
            Object fieldValue = payloadMap.get(fieldName);

            if (fieldName.endsWith(Constants.VIEWMODEL_CUSTOMFIELDSUFFIX))
                fieldName = (fieldName.removeEnd(Constants.VIEWMODEL_CUSTOMFIELDSUFFIX)) + '__c';

            if (fieldName.startsWith(Constants.VIEWMODEL_CONTACTPREFIX))
                fieldName = fieldName.substringAfter('_');

            if (updateField(contact, fieldName, fieldValue))
                contactUpdated = true;
        }

        if (contactUpdated)
            Database.update(contact);

        return new ActionResult(ResultStatus.SUCCESS);
    }

    private Boolean updateField(SObject obj, String fieldName, Object fieldValue)
    {
        Boolean isDirty = false;

        try
        {
            Object currentFieldValue = obj.get(fieldName);

            if (currentFieldValue != fieldValue)
            {
                obj.put(fieldName, fieldValue);
                isDirty = true;
            }
        }
        catch(Exception ex)
        {
            String errMsg = String.format('Failed to set value to {0} field', new List<String>{ fieldName });
            Log.error(this.className, 'updateField', errMsg, ex);
            throw new Exceptions.ApplicationException(errMsg);
        }

        return isDirty;
    }
}