public abstract class BaseComponent
{
    private Boolean enabled;
    private Boolean enabledSet = false;
    protected Exception exc;
    protected String className;

    public String enabledText = '';
    public boolean saveLogs { get; set; }
    protected Integer retry { get; set; }

    public Boolean isEnabled
    {
        get
        {
            // Test Overrides
            if (Test.isRunningTest())
            {
                return TestUtilities.isComponentEnabled(this.className);
            }

            if (enabledSet == false)
            {
                if (!Test.isRunningTest() && enabledText == 'false')
                    enabled = false;
                else
                    enabled = true;

                Log.info('BaseQueueableCommand', 'isEnabled', 'Enabled: ' + enabled);

                enabledSet = true;
            }

            return enabledSet;
        }
        set
        {
            enabled = value;
            enabledSet = true;
        }
    }

    public BaseComponent()
    {
        this.saveLogs = true;
        this.className = String.valueOf(this).split(':')[0];
        this.retry = 0;
    }

    public BaseComponent(Integer retry)
    {
        this.saveLogs = true;
        this.className = String.valueOf(this).split(':')[0];
        this.retry = retry;
    }
}