@isTest
public class ContactScoreDemographicsBatchTest 
{
    @testSetup 
    public static void setup()
    {
        TestUtilities.setupHEDA();
        TestUtilities.disableComponent('CerebroSyncApplicationCommand');
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact contact = TestDataContactMock.createProspectHot(user);
        //TestDataOpportunityMock.create(user, contact, '', true);
    }

    @isTest 
    static void ContactWithNoDemographicScoreConfigMatch_DemographicScoreUnchanged() 
    {
        // Arrange         
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        TestDataSalesScoreConfigMock.create(user, 'Contact', 60, 'OtherCountry', String.valueOf(Enums.Operator.EQUAL), 'Germany');

        // Act
        test.startTest();
        ContactScoreDemographicsBatch batch = new ContactScoreDemographicsBatch();
        ID batchProcessId = Database.executeBatch(batch, 5);
        test.stopTest();

        Contact updatedContact = [SELECT Id, OtherCountry, DemographicScore__c FROM Contact Limit 1];

        // Assert
        System.assertEquals(0, updatedContact.DemographicScore__c, 'Unexpected demographic score value');
    }

    @isTest 
    static void ContactMathingSingleDemographicScoreConfig_DemographicScoreUpdatedCorrectly() 
    {
        // Arrange 
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        TestDataSalesScoreConfigMock.create(user, 'Contact', 60, 'OtherCountry', String.valueOf(Enums.Operator.EQUAL), 'Australia');

        // Act
        test.startTest();
        ContactScoreDemographicsBatch batch = new ContactScoreDemographicsBatch();
        ID batchProcessId = Database.executeBatch(batch, 5);
        test.stopTest();

        Contact updatedContact = [SELECT Id, OtherCountry, DemographicScore__c FROM Contact Limit 1];

        // Assert
        System.assertEquals(60, updatedContact.DemographicScore__c, 'Unexpected demographic score value');
    }

    @isTest 
    static void ContactMathingCoupleDemographicScoreConfigs_DemographicScoreUpdatedCorrectly() 
    {
        // Arrange 
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        TestDataSalesScoreConfigMock.create(user, 'Contact', 60, 'OtherCountry', String.valueOf(Enums.Operator.EQUAL), 'Australia');
        TestDataSalesScoreConfigMock.create(user, 'Opportunity', 60, 'Payment_Method__c', String.valueOf(Enums.Operator.EQUAL), 'Self Funded');

        Opportunity opp = [SELECT Id, Payment_Method__c from Opportunity LIMIT 1];
        opp.Payment_Method__c = 'Self Funded';

        // Act
        test.startTest();
        update opp;
        ContactScoreDemographicsBatch batch = new ContactScoreDemographicsBatch();
        ID batchProcessId = Database.executeBatch(batch, 5);
        test.stopTest();

        Contact updatedContact = [SELECT Id, OtherCountry, DemographicScore__c FROM Contact Limit 1];

        // Assert
        System.assertEquals(120, updatedContact.DemographicScore__c, 'Unexpected demographic score value');
    }
}