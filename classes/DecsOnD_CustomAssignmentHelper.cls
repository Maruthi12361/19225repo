/**
 * Customizes Decisions on Demand assignment logic to:
 * - apply weighted assignment
 *
 * This class is registered to the Decisions on Demand API in DecsOnD_CustomizationManager
 *
 * Copyright 2018-2019, Decisions on Demand, Inc.
 * All Rights Reserved.
 *
 * For support, contact support@decisionsondemand.com
*/
global without sharing class DecsOnD_CustomAssignmentHelper extends DecsOnD.AssignmentHelper
{
    @TestVisible
    private String additionalMessage = null;
    private sObject currentMainRecord = null;

    global DecsOnD_CustomAssignmentHelper()
    {
        super();
        DecsOnD.Config.addFieldsToLoad(User.SObjectType, new Set<sObjectField>{User.DecsOnD_Assignment_Tier__c, User.DecsOnD_Assignment_Weight__c});
        System.debug(LoggingLevel.info, 'DecsOnD_CustomAssignmentHelper instance created');
    }

    // ===================================================================================================
    // API overrides
    // ===================================================================================================
    global override sObject getAssignedOwner(DecsOnD.PolicyActionRecord actionRec)
    {
        this.currentMainRecord = actionRec.getContext().checkMainRecord();
        return super.getAssignedOwner(actionRec);
    }

    /**
     * Override method to add support for Weighted assignment option
     */
    global override User getAssignedOwner(Group grp, Map<String, Object> parameters)
    {
        Set<String> options = getAssignmenOptions(parameters);
        if (options.contains(DecsOnD_AssignmentGlobals.WEIGHTED_OPTION)) {
            return getAssignedOwnerWeighted(grp, parameters);
        } else {
            return super.getAssignedOwner(grp, parameters);
        }
        // Note skills-based assignment is handled in getEligibleUsers
    }

    /**
     * Override standard behavior to filter users based on skills, if required
     */
    global override List<User> getEligibleUsers(Group grp, Map<String, Object> parameters)
    {
        List<User> eligibleUsers = super.getEligibleUsers(grp, parameters);
        Set<String> options = getAssignmenOptions(parameters);
        if (options.contains(DecsOnD_AssignmentGlobals.SKILLS_BASED_OPTION)) {
            return filterUsersBySkills(grp, eligibleUsers, parameters);
        } else {
            return eligibleUsers;
        }
    }

    /**
     * Called by AssignOwnerAction at the start of the prepare method
     */
    global override void startPrepare(DecsOnD.PolicyActionRecord actionRec)
    {
        super.startPrepare(actionRec);
        this.additionalMessage = null;
    }

    global override void finishPrepare(DecsOnD.PolicyActionRecord actionRec, sObject owner)
    {
        super.finishPrepare(actionRec, owner);
        if (this.additionalMessage != null)
        {
            System.debug(LoggingLevel.info, 'DecisionsOnDemand assignment summary:' + this.additionalMessage);
            actionRec.appendToMainMessage(this.additionalMessage);
            this.additionalMessage = null;
        }
    }

    // ===================================================================================================
    // Custom functionality
    // ===================================================================================================
    private List<User> filterUsersBySkills(Group grp, List<User> eligibleUsers, Map<String, Object> parameters)
    {
        if ((this.currentMainRecord == null) || DecsOnD.Utilities.isEmptyList(eligibleUsers)) {
            return eligibleUsers;
        } else if (!(this.currentMainRecord instanceof Contact)) {
            this.additionalMessage = 'Skills-based assignment cannot be applied to a ' + this.currentMainRecord.getSObjectType();
            return eligibleUsers;
        }
        Integer eligibleUserCount = eligibleUsers.size();
        System.debug(LoggingLevel.info, 'Applying skills-based filtering for ' + eligibleUserCount + ' eligible users, for Group(param):' + parameters.get('group'));
        Skillset__c[] skillsets = null;
        try
        {
            skillsets = findSkillsets((Contact)this.currentMainRecord, eligibleUsers, grp.DeveloperName);
            if (skillsets.isEmpty())
                this.additionalMessage = 'None of the ' + eligibleUserCount + ' users have any matching skills for Type:' + grp.DeveloperName;
        }
        catch (Exception e)
        {
            System.debug(LoggingLevel.ERROR, 'Unable to determine skillset: ' + e.getMessage());
            this.additionalMessage = '!! Unable to determine skillsets [Internal error: ' + e.getMessage() + ']';
        }
        if (DecsOnD.Utilities.isEmptyList(skillsets))
            return eligibleUsers;

        Map<Id, Set<String>> skillsMap = new Map<Id, Set<String>>();
        for (Skillset__c skillset : skillsets)
        {
            Integer skillSetWeightingValue = DecsOnD_AssignmentGlobals.WEIGHTING_VALUES_MAP.get(skillSet.Weighting__c);
            Integer randomNumber = Integer.valueof((Math.random() * 100)); //0-99

            // If the randomNumber > than the skillsets weighting then failed the check so skip other wise everything progresses as normal adding the Skillset Subtype as a match
            // The probability of the check failing increases as the weighting decreases and hence the 0-100 with 0 indicating always fails to include and 100 always includes
            if (randomNumber > skillSetWeightingValue)
                continue;

            Set<String> userSkills = skillsMap.get(skillset.User__c);
            if (userSkills == null)
            {
                userSkills = new Set<String>();
                skillsMap.put(skillset.User__c, userSkills);
            }
            userSkills.add(skillset.SubType__c);
        }

        // Select only the users with the maximum count of skills
        integer maxCount = 1; // Start at 1 -- there must be at least one user with 1 skill
        List<User> filteredUsers = new List<User>();

        // Iterate through all users -- we need to preserve the order for the Round Robin
        for (User eligibleUser : eligibleUsers)
        {
            Set<String> userSkills = skillsMap.get(eligibleUser.Id);
            integer count = (userSkills == null) ? 0 : userSkills.size();
            if (count >= maxCount)
            {
                System.debug(LoggingLevel.info, 'Found user ' + eligibleUser.Name + ' with ' + count + ' skills: ' + userSkills + ', while maxCount:' + maxCount);
                if (count > maxCount)
                {
                    System.debug(LoggingLevel.info, 'Updated maximum skills count from current:' + maxCount + ' to:' + count);
                    filteredUsers.clear();
                    maxCount = count;
                }
                filteredUsers.add(eligibleUser);
            }
        }
        this.additionalMessage = '' + filteredUsers.size() + ' out of ' + eligibleUserCount + ' users have ' + maxCount + ' matching skill(s). ' + (DecsOnD.Utilities.isEmptyList(filteredUsers) ? ' Returning ' + eligibleUsers.size() + ' original users instead.' : '');
        return (DecsOnD.Utilities.isEmptyList(filteredUsers)) ? eligibleUsers : filteredUsers;
    }

    /**
     * Apply weighted assignment
     */
    private User getAssignedOwnerWeighted(Group grp, Map<String, Object> parameters)
    {
        List<User> eligibleUsers = getEligibleUsers(grp, parameters);
        Double maxAssignmentWeight = computeMaxAssignmentWeight(eligibleUsers, parameters);
        if ((maxAssignmentWeight <= 0) || DecsOnD.Utilities.isEmptyList(eligibleUsers)) 
            throw new DecsOnD.ApplyPolicyException('no eligible users with positive assignment weight');            

        System.debug(LoggingLevel.info, 'Applying weighted assignment for group ' + grp.Name + ' with ' + eligibleUsers.size() + ' eligible users. Max assignment weight: ' + maxAssignmentWeight);

        User selectedOwner = null;
        integer numCycles = 0;
        integer maxIterations = 10; // Make sure we don't loop endlessly
        integer maxCycles = eligibleUsers.size() * maxIterations;
        while ((selectedOwner == null) && (numCycles < maxCycles)) 
        {
            numCycles++;
            User candidateOwner = getGroupAssignmentHelper().selectNextUser(grp.Id, eligibleUsers, parameters);
            if (eligibleUsers.size() == 1) 
            {
                // If this is the only eligible user -- assign regardless of weight. We should still make 
                // the call to selectNextUser above so that the Round Robin accounting is managed correctly
                selectedOwner = candidateOwner;
                System.debug(LoggingLevel.info, 'User ' + selectedOwner.Name + ' is the only eligible user in group ' + grp.Name);
                break;
            }
            Double assignmentWeight = getAssignmentWeight(candidateOwner);

            // Normalize the assignment weight
            assignmentWeight = (assignmentWeight > 0.0) ? Math.min(1.0, assignmentWeight / maxAssignmentWeight) : 0.0;

            if (Math.random() >= assignmentWeight)
            {
                System.debug(LoggingLevel.info, 'User ' + candidateOwner.Name + ' was skipped [normalized assignment weight: ' + assignmentWeight + ']');
            }
            else
            {
                System.debug(LoggingLevel.debug, 'User ' + candidateOwner.Name + ' was selected [normalized assignment weight: ' + assignmentWeight + ']');
                selectedOwner = candidateOwner;
            }
        }
        if (selectedOwner == null) 
        {
            System.debug(LoggingLevel.WARN, 'No user selected even after ' + maxIterations + ' iterations. Defaulting to standard assignment');
            selectedOwner = super.getAssignedOwner(grp, parameters);
        }
        return selectedOwner;
    }

    /**
     * Determine the highest assignment weight for any eligible user.
     * Used to normalize the weights to a [0..1] range
     */
    private Double computeMaxAssignmentWeight(List<User> users, Map<String, Object> parameters)
    {
        Double maxWeight = 0.0;
        if (DecsOnD.Utilities.isEmptyList(users))
            return maxWeight;

        for (User usr : users)
        {
            Double assignmentWeight = getAssignmentWeight(usr);

            if (assignmentWeight > maxWeight) {
                maxWeight = assignmentWeight;
            }
        }

        System.debug(LoggingLevel.INFO, 'Max assignment weight set to ' + maxWeight);
        return maxWeight;
    }

    // ===================================================================================================
    // Support methods
    // ===================================================================================================
    @TestVisible
    private static Set<String> getAssignmenOptions(Map<String, Object> parameters)
    {
        String optionsParam = DecsOnD.Utilities.getStringParameter(parameters, DecsOnD_AssignmentGlobals.ASSIGNMENT_OPTIONS_PARAM, true);
        Set<String> optionsSet = new Set<String>();
        if (optionsParam != null)
        {
            String[] options = optionsParam.split(';');
            for (String option: options)
                optionsSet.add(option.trim());
        }
        else if (DecsOnD.AssignmentUtils.getAssignmentMode(parameters) == DecsOnD_AssignmentGlobals.WEIGHTED_ROUND_ROBIN_MODE)
        {
            // Backward compatibility
            optionsSet.add(DecsOnD_AssignmentGlobals.WEIGHTED_OPTION);
        }
        return optionsSet;
    }

    @TestVisible
    private static Double getAssignmentWeight(User usr)
    {
        if (usr == null)
            return 0.0;

        return Math.max(0.0, Double.valueOf(usr.get(DecsOnD_AssignmentGlobals.ASSIGNMENT_WEIGHT_FIELD)));
    }

    /**
     * Determine skillsets
     * Throw an exception if an internal error occurs. Calling code should handle it
     */
    @TestVisible
    private static Skillset__c[] findSkillsets(Contact c, List<User> users, String ssType) {
        if (DecsOnD.Utilities.isEmptyList(users))
            return new Skillset__c[]{};

        String[] subTypes = new String[]{};
        Map<String, sObjectField> fields = ssType == Constants.GROUP_COURSE_ADVISORS ? DecsOnD_AssignmentGlobals.CONTACT_INFERRED_SKILLS_FIELDS : DecsOnD_AssignmentGlobals.CONTACT_INFERRED_CC_SKILLS_FIELDS;
        for (String skill : fields.keySet())
        {
            sObjectField inferredSkillField = fields.get(skill);
            Object skillValue = c.get(inferredSkillField);
            if (skillValue != null)
                subTypes.add(skill + ': ' + String.valueOf(skillValue));
        }
        Skillset__c[] skillsets= [SELECT Id, Name , Type__c, SubType__c, User__c, Weighting__c FROM Skillset__c WHERE Type__c = :ssType AND SubType__c IN :subTypes AND User__c IN :users];
        System.debug(LoggingLevel.info, 'Found ' + skillsets.size() + ' total skills of type:' + ssType + ' and sub-type(s):' + subTypes + ' for ' + users.size() + ' users. SkillSets:' + skillsets);
        return skillsets;
    }
}