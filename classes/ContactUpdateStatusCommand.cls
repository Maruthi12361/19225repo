public class ContactUpdateStatusCommand extends BaseTriggerCommand
{
    private List<Opportunity> opps = new List<Opportunity>();
    private final List<String> warmStatusFields = new List<String>
    {
        'HighestTertiaryLevelDegree__c', 'WorkExperienceYears__c', 'ManagementExperienceYears__c', 'Status__c'
    };
    private final List<String> hotStatusFields = new List<String>
    {
        'Preferred_Start_Date__c', 'ApplicationCourse__c', 'FirstName', 'LastName', 'Phone',
        'Email', 'MailingStreet', 'MailingCity', 'MailingState', 'MailingCountry', 'hed__Country_of_Origin__c', 'High_School_Level__c',
        'HighestQualificationName__c', 'HighestQualificationInstitution__c', 'HighestQualificationLanguage__c',
        'HighestQualificationCompletionYear__c'
    };

    protected override List<SObject> validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        List<Contact> result = new List<Contact>();
        for (Contact c : (List<Contact>)items)
        {
            if (c.RecordTypeId != System.Label.ID_RecordType_Contact_Prospect)
                continue; // Contact is not a prospect

            if (c.Status__c == Constants.STATUS_DISQUALIFIED)
                continue; // Contact status is Disqualified, do not update status

            if (triggerContext == Enums.TriggerContext.BEFORE_INSERT)
            {
                result.add(c);
                continue;
            }

            Contact oldContact = (Contact) oldMap.get(c.Id);

            if (SObjectUtils.hasAnyPropertyChangedIgnoreCase(c, oldContact, warmStatusFields) ||
                SObjectUtils.hasAnyPropertyChangedIgnoreCase(c, oldContact, hotStatusFields))
                result.add(c);
        }
        return result;
    }

    protected override ActionResult command(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        this.opps = getOpps(items);

        for (Contact c : (List<Contact>)items)
            updateStatus(c);

        return new ActionResult();
    }

    private void updateStatus(Contact c)
    {
        List<Opportunity> openOpps = getContactOpps(c.Id);

        if (openOpps.size() > 0)
        {
            if (c.Status__c != Constants.STATUS_HOT)
                c.Status__c  = Constants.STATUS_HOT; // Override status to hot as it has at least one open opportunity

            Log.info(this.className, 'updateStatus', 'Overriding contact status to hot as there is at least one open Opportunity');
            return;
        }

        if (isContactWarm(c))
        {
            if (isContactHot(c))
            {
                if (c.Status__c != Constants.STATUS_HOT)
                    c.Status__c = Constants.STATUS_HOT;
            }
            else
            {
                if (c.Status__c != Constants.STATUS_WARM)
                    c.Status__c = Constants.STATUS_WARM;
            }
        }
        else
        {
            if (c.Status__c != Constants.STATUS_COLD)
                c.Status__c = Constants.STATUS_COLD;
        }
    }

    private Boolean isContactHot(Contact c)
    {
        Boolean hasName = !String.isEmpty(c.FirstName) && !String.isEmpty(c.LastName);
        Boolean hasPhoneAndEmail = !String.isEmpty(c.Phone) && !String.isEmpty(c.Email) ;
        Boolean hasCourseAndStartDate = !String.isEmpty(c.Preferred_Start_Date__c) && c.Preferred_Start_Date__c != 'At A Later Date'
            && !String.isEmpty(c.ApplicationCourse__c);

        Boolean hasMailingAddress = (!String.isEmpty(c.MailingStreet) && !String.isEmpty(c.MailingCity) &&
            !String.isEmpty(c.MailingState) && !String.isEmpty(c.MailingCountry));

        Boolean hasHighSchoolAndCountryOfBirth = !String.isEmpty(c.hed__Country_of_Origin__c) && !String.isEmpty(c.High_School_Level__c);

        Boolean hasQualification = !String.isEmpty(c.HighestQualificationName__c) && !String.isEmpty(c.HighestQualificationInstitution__c) &&
            !String.isEmpty(c.HighestQualificationLanguage__c) && c.HighestQualificationCompletionYear__c != null;

        return (hasName && hasPhoneAndEmail && hasCourseAndStartDate && (hasMailingAddress) && hasHighSchoolAndCountryOfBirth && hasQualification );
    }

    // Note: the logic here needs to match front-end logic in new OAF
    private Boolean isContactWarm(Contact c)
    {
        Integer workExperience = c.WorkExperienceYears__c == null ? 0 : Integer.valueOf(c.WorkExperienceYears__c);
        Integer managementExperience = c.ManagementExperienceYears__c == null ? 0 : Integer.valueOf(c.ManagementExperienceYears__c);

        if (String.isEmpty(c.HighestTertiaryLevelDegree__c))
            return false;

        if (c.HighestTertiaryLevelDegree__c == 'Master\'s degree, post graduate degree or higher')
            return true;

        if (c.HighestTertiaryLevelDegree__c == 'Bachelor\'s Degree or equivalent' && (managementExperience >= 2 || workExperience >= 2))
            return true;

        if (c.HighestTertiaryLevelDegree__c == 'Associate Degree, Advanced Diploma or equivalent' && (managementExperience >= 3 || workExperience >= 3))
            return true;

        if (c.HighestTertiaryLevelDegree__c == 'High school, Certificate III, IV or Diploma' && (managementExperience >= 5 ||
            (managementExperience >= 3 && workExperience >= 5)))
            return true;

        return false;
    }

    private List<Opportunity> getOpps(List<Contact> contacts)
    {
        Set<Id> ids = SObjectUtils.getIDs(contacts, 'ID');
        String sql = 'SELECT Id, StageName, Contact__c ' +
                     'FROM Opportunity ' +
                     'WHERE Contact__c IN :ids AND IsClosed = false AND RecordType.DeveloperName = \'' + Constants.RECORDTYPE_OPPORTUNITY_ACQUISITION + '\'';

        return Database.query(sql);
    }

    private List<Opportunity> getContactOpps(Id contactId)
    {
        List<Opportunity> openOpps = new List<Opportunity>();
        for (Opportunity opp : this.opps)
            if (opp.Contact__c == contactId)
                openOpps.add(opp);

        return openOpps;
    }
}