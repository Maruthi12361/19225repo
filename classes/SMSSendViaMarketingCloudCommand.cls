public class SMSSendViaMarketingCloudCommand extends BaseViewModelCommand
{
    public override List<Interfaces.IViewModel> validate(List<Interfaces.IViewModel> params)
    {
        return params;
    }

    public override ActionResult command(List<Interfaces.IViewModel> params)
    {
        for (ViewModelsSMS.Details payload: (List<ViewModelsSMS.Details>) params)
        {
            if (ConfigUtility.isSandbox() && !Test.isRunningTest())
            {
                String body = '"' + payload.Message + '"' + ' is sent to ' + payload.ToPhoneNumber;
                ViewModelsMail.Email mail = new ViewModelsMail.Email(null, 'SMS Message Via MarketingCloud', '', body, Label.SMTPOverrideEmail, payload.ToPhoneNumber, payload.FromName, UserInfo.getUserEmail());
                
                return MarketingCloudUtils.sendEmail(mail);
            }
            else
                return MarketingCloudUtils.sendSMS(payload);
        }

        return new ActionResult();
    }
}