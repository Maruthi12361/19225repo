@isTest
public class CaseUpdateSSOStatusCommandTests 
{
    @testSetup
    static void setup()
    {
        TestUtilities.setupHEDA();
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        TestUtilities.disableComponent('CerebroSyncApplicationCommand');
        User user = UserService.getByAlias(Constants.USER_COURSEADVISOR);
        Contact contact = TestDataContactMock.createProspectHot(user, 'A0019545878');
    }

    @isTest
    static void PaymentCompleteUpdatedToTrue_SSOStatusUpdatedSuccessfully()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        TestUtilities.disableComponent('ContactSendSSOLoginDetailsCommand');
        TestUtilities.disableComponent('CaseUpdateContactSSOStatusCommand');
        Contact contact = [SELECT Id, Cerebro_Student_ID__c, SingleSignOnStatus__c FROM Contact LIMIT 1];
        Case appCase = [SELECT Id, ContactId, Invoice_Processed__c, FEE_HELP_Processed__c, Vetting_Complete__c, Payment_Complete__c, RecordTypeId FROM Case LIMIT 1];

        contact.SingleSignOnStatus__c = Constants.CONTACT_SINGLESIGNONSTATUS_PROSPECTENABLED;
        update contact;

        Case cloneAppCase = appCase.clone(true, true);
        Map<Id, SOBject> oldMap = new Map<Id,SObject>();
        oldMap.put(cloneAppCase.Id, cloneAppCase);

        // Act
        Test.startTest();
        appCase.Invoice_Processed__c = true;
        ActionResult result = CaseService.updateSSOStatus(new List<Case> { appCase }, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        Test.stopTest();

        Contact updatedContact = [SELECT Id, SingleSignOnStatus__c, SSO_ID__c, SingleSignOnStatusLastUpdated__c FROM Contact WHERE Id = :contact.Id];

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.status, 'Unexpected result status');
        System.assertEquals(Constants.CONTACT_SINGLESIGNONSTATUS_STUDENTPROVISIONING, updatedContact.SingleSignOnStatus__c, 'Unexpected single sign on status');
    }
}