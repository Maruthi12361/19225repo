@isTest
public class DecisionsOnDemandAssignmentTests
{
    @testSetup
    static void setup()
    {
        TestUtilities.setupHEDA();
        TestUtilities.disableComponent('CerebroSyncApplicationCommand');
        delete [SELECT Id, userOrGroupId FROM GroupMember WHERE Group.DeveloperName =: Constants.GROUP_COURSE_ADVISORS OR Group.DeveloperName =: Constants.GROUP_COURSE_CONSULTANTS];
    }

    @isTest
    static void CreateTwoFullMatchUsers_AssignToEitherUser()
    {
        // Arrange
        User data = UserService.getByAlias(Constants.USER_TESTSETUP);

        List<User> testUsers = [SELECT Id, inGroup__c FROM User WHERE Profile.Name = 'AIB Platform' AND isActive = true LIMIT 7];
        List<User> courseAdvisors = new List<User>{ testUsers[0], testUsers[1], testUsers[2], testUsers[3], testUsers[4], testUsers[5] };
        List<User> fullMatchUsers = new List<User>{ testUsers[0], testUsers[1] };
        User courseConsultant = testUsers[6];

        List<SkillSet__c> items = new List<SkillSet__c>();
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[0], Constants.GROUP_COURSE_ADVISORS, 'Gender: Male', false));
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[0], Constants.GROUP_COURSE_ADVISORS, 'Entry Pathway: Bachelor Degree', false));
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[0], Constants.GROUP_COURSE_ADVISORS, 'Portfolio: Domestic', false));
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[1], Constants.GROUP_COURSE_ADVISORS, 'Gender: Male', false));
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[1], Constants.GROUP_COURSE_ADVISORS, 'Entry Pathway: Bachelor Degree', false));
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[1], Constants.GROUP_COURSE_ADVISORS, 'Portfolio: Domestic', false));
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[2], Constants.GROUP_COURSE_ADVISORS, 'Gender: Female', false));
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[2], Constants.GROUP_COURSE_ADVISORS, 'Entry Pathway: Bachelor Degree', false));
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[3], Constants.GROUP_COURSE_ADVISORS, 'Gender: Male', false));
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[3], Constants.GROUP_COURSE_ADVISORS, 'Entry Pathway: Work Experience', false));
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[4], Constants.GROUP_COURSE_ADVISORS, 'Gender: Indeterminate', false));
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[4], Constants.GROUP_COURSE_ADVISORS, 'Entry Pathway: Work Experience', false));
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[5], Constants.GROUP_COURSE_ADVISORS, 'Gender: Indeterminate', false));
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[5], Constants.GROUP_COURSE_ADVISORS, 'Entry Pathway: Associate Degree', false));
        items.add(TestDataSkillSetMock.createStaff(data, courseConsultant, Constants.GROUP_COURSE_CONSULTANTS, 'Portfolio: Domestic', false));
        insert items;

        Contact hotContact = TestDataContactMock.createProspectHot(data);
        System.RunAs(data)
        {
            hotContact.OwnerId = courseConsultant.Id;
            hotContact.Portfolio__c = 'Domestic';
            update hotContact;
        }
        Contact contactBefore = [SELECT Id, salutation, OwnerId, InferredEntryPathway__c, InferredGender__c, InferredPortfolio__c FROM Contact WHERE Id =: hotContact.Id ];

        // Act
        Test.startTest();
        contactBefore.salutation = 'Mr';
        update contactBefore;
        contactBefore.recalculateFormulas();
        Test.stopTest();
        simulateContactBusinessPolicy(contactBefore, Constants.GROUP_COURSE_ADVISORS);

        // Assert
        Contact contactAfter = [SELECT Id, Owner.Name, OwnerId, Owner.inGroup__c FROM Contact WHERE Id =: contactBefore.Id];
        System.assertNotEquals(courseConsultant.Id, contactAfter.OwnerId, 'The original Course Consultant Owner Id should not be the current Contacts Owner as this should have been re-assigned to a Course Advisor.');
        System.assertEquals(Constants.GROUP_COURSE_ADVISORS, contactAfter.Owner.inGroup__c, 'The Owner of the Contact after re-assignment should be a Course Advisor.');
        System.assertEquals(new Map<Id, User>(courseAdvisors).keySet().contains(contactAfter.OwnerId), true, 'The list of all CourseAdvisor Ids should contain the current contact.ownerId.');
        System.assertEquals(new Map<Id, User>(fullMatchUsers).keySet().contains(contactAfter.OwnerId), true, 'The current contact.ownerId of the re-assigned Contact should be one of the 2 full match users that where set up based on the skill sets above.');
    }

    @isTest
    static void CreateTwoFullMatchUsersWithWeightNeverOnOneSkillSet_AssignToUserWithNotNeverSkillSet()
    {
        // Arrange
        User data = UserService.getByAlias(Constants.USER_TESTSETUP);

        List<User> testUsers = [SELECT Id, inGroup__c FROM User WHERE Profile.Name = 'AIB Platform' AND isActive = true LIMIT 7];
        List<User> courseAdvisors = new List<User>{ testUsers[0], testUsers[1], testUsers[2], testUsers[3], testUsers[4], testUsers[5] };
        User courseConsultant = testUsers[6];

        List<SkillSet__c> items = new List<SkillSet__c>();
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[0], Constants.GROUP_COURSE_ADVISORS, 'Gender: Male', Constants.SKILLSET_WEIGHTING_NEVER, false));
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[0], Constants.GROUP_COURSE_ADVISORS, 'Entry Pathway: Bachelor Degree', false));
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[0], Constants.GROUP_COURSE_ADVISORS, 'Portfolio: Domestic', false));
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[1], Constants.GROUP_COURSE_ADVISORS, 'Gender: Male', false));
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[1], Constants.GROUP_COURSE_ADVISORS, 'Entry Pathway: Bachelor Degree', false));
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[1], Constants.GROUP_COURSE_ADVISORS, 'Portfolio: Domestic', false));
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[2], Constants.GROUP_COURSE_ADVISORS, 'Gender: Female', false));
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[2], Constants.GROUP_COURSE_ADVISORS, 'Entry Pathway: Bachelor Degree', false));
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[2], Constants.GROUP_COURSE_ADVISORS, 'Portfolio: International (Canada)', false));
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[3], Constants.GROUP_COURSE_ADVISORS, 'Gender: Male', false));
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[3], Constants.GROUP_COURSE_ADVISORS, 'Entry Pathway: Work Experience', false));
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[3], Constants.GROUP_COURSE_ADVISORS, 'Portfolio: International (Other)', false));
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[4], Constants.GROUP_COURSE_ADVISORS, 'Gender: Indeterminate', false));
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[4], Constants.GROUP_COURSE_ADVISORS, 'Entry Pathway: Work Experience', false));
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[4], Constants.GROUP_COURSE_ADVISORS, 'Portfolio: International (New Zealand)', false));
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[5], Constants.GROUP_COURSE_ADVISORS, 'Gender: Indeterminate', false));
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[5], Constants.GROUP_COURSE_ADVISORS, 'Entry Pathway: Associate Degree', false));
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[5], Constants.GROUP_COURSE_ADVISORS, 'Portfolio: International (Other)', false));
        items.add(TestDataSkillSetMock.createStaff(data, courseConsultant, Constants.GROUP_COURSE_CONSULTANTS, 'Portfolio: Domestic', false));
        insert items;

        Contact hotContact = TestDataContactMock.createProspectHot(data);
        System.RunAs(data)
        {
            hotContact.OwnerId = courseConsultant.Id;
            hotContact.Portfolio__c = 'Domestic';
            update hotContact;
        }
        Contact contactBefore = [SELECT Id, salutation, OwnerId, InferredEntryPathway__c, InferredGender__c, InferredPortfolio__c FROM Contact WHERE Id =: hotContact.Id ];

        // Act
        Test.startTest();
        contactBefore.salutation = 'Mr';
        update contactBefore;
        contactBefore.recalculateFormulas();
        Test.stopTest();
        simulateContactBusinessPolicy(contactBefore, Constants.GROUP_COURSE_ADVISORS);

        // Assert
        Contact contactAfter = [SELECT Id, Owner.Name, OwnerId, Owner.inGroup__c FROM Contact WHERE Id =: contactBefore.Id];
        System.assertNotEquals(courseConsultant.Id, contactAfter.OwnerId, 'The original Course Consultant Owner Id should not be the current Contacts Owner as this should have been re-assigned to a Course Advisor.');
        System.assertNotEquals(testUsers[0].Id, contactAfter.OwnerId, 'The Course Advisor with SkillSet correct SkillSet but weighting of Never should not get a match so Owner Id should not be that user.');
        System.assertEquals(Constants.GROUP_COURSE_ADVISORS, contactAfter.Owner.inGroup__c, 'The Owner of the Contact after re-assignment should be a Course Advisor.');
        System.assertEquals(new Map<Id, User>(courseAdvisors).keySet().contains(contactAfter.OwnerId), true, 'The list of all CourseAdvisor Ids should contain the current contact.ownerId.');
        System.assertEquals(testUsers[1].Id, contactAfter.OwnerId, 'The second CourseAdvisor with 2 matching skillsets and without weighting of Never is who the contact should be re-assigned to.');
    }

    @isTest
    static void CreateThreeSingleMatchUsers_AssignToOneOfThreeUsers()
    {
        // Arrange
        User data = UserService.getByAlias(Constants.USER_TESTSETUP);

        List<User> testUsers = [SELECT Id, inGroup__c FROM User WHERE Profile.Name = 'AIB Platform' AND isActive = true LIMIT 7];
        List<User> courseAdvisors = new List<User>{ testUsers[0], testUsers[1], testUsers[2], testUsers[3], testUsers[4], testUsers[5] };
        List<User> singleMatchUsers = new List<User>{ testUsers[0], testUsers[1], testUsers[2] };
        User courseConsultant = testUsers[6];

        List<SkillSet__c> items = new List<SkillSet__c>();
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[0], Constants.GROUP_COURSE_ADVISORS, 'Gender: Male', false));
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[0], Constants.GROUP_COURSE_ADVISORS, 'Entry Pathway: Associate Degree', false));
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[1], Constants.GROUP_COURSE_ADVISORS, 'Gender: Female', false));
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[1], Constants.GROUP_COURSE_ADVISORS, 'Portfolio: International (Other)', false));
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[2], Constants.GROUP_COURSE_ADVISORS, 'Entry Pathway: Bachelor Degree', false));
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[2], Constants.GROUP_COURSE_ADVISORS, 'Portfolio: Domestic', false));
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[3], Constants.GROUP_COURSE_ADVISORS, 'Gender: Female', false));
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[3], Constants.GROUP_COURSE_ADVISORS, 'Entry Pathway: Work Experience', false));
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[4], Constants.GROUP_COURSE_ADVISORS, 'Gender: Indeterminate', false));
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[4], Constants.GROUP_COURSE_ADVISORS, 'Portfolio: International (Canada)', false));
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[5], Constants.GROUP_COURSE_ADVISORS, 'Gender: Indeterminate', false));
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[6], Constants.GROUP_COURSE_CONSULTANTS, 'Portfolio: Domestic', false));
        insert items;

        Contact hotContact = TestDataContactMock.createProspectHot(data);
        System.RunAs(data)
        {
            hotContact.OwnerId = courseConsultant.Id;
            hotContact.Portfolio__c = 'International (Other)';
            update hotContact;
        }
        Contact contactBefore = [SELECT Id, salutation, OwnerId, InferredEntryPathway__c, InferredGender__c, InferredPortfolio__c FROM Contact WHERE Id =: hotContact.Id ];

        // Act
        Test.startTest();
        contactBefore.salutation = 'Mr';
        update contactBefore;
        contactBefore.recalculateFormulas();
        Test.stopTest();
        simulateContactBusinessPolicy(contactBefore, Constants.GROUP_COURSE_ADVISORS);

        // Assert
        Contact contactAfter = [SELECT Id, OwnerId, Owner.inGroup__c FROM Contact WHERE Id =: contactBefore.Id];
        System.assertNotEquals(courseConsultant.Id, contactAfter.OwnerId, 'The original Course Consultant Owner Id should not be the current Contacts Owner as this should have been re-assigned to a Course Advisor.');
        System.assertEquals(Constants.GROUP_COURSE_ADVISORS, contactAfter.Owner.inGroup__c, 'The Owner of the Contact after re-assignment should be a Course Advisor.');
        System.assertEquals(new Map<Id, User>(courseAdvisors).keySet().contains(contactAfter.OwnerId), true, 'The list of all CourseAdvisor Ids should contain the current contact.ownerId.');
        System.assertEquals(new Map<Id, User>(singleMatchUsers).keySet().contains(contactAfter.OwnerId), true, 'The current contact.ownerId of the re-assigned Contact should be one of the 3 single skill set match users that where set up based on the mixture of Gender and Entry Path Way skill sets above.');
    }

    @isTest
    static void CreateSixZeroMatchUsers_AssignRandomlyToOneOfSixUsers()
    {
        // Arrange
        User data = UserService.getByAlias(Constants.USER_TESTSETUP);

        List<User> testUsers = [SELECT Id, inGroup__c FROM User WHERE Profile.Name = 'AIB Platform' AND isActive = true LIMIT 7];
        List<User> courseAdvisors = new List<User>{ testUsers[0], testUsers[1], testUsers[2], testUsers[3], testUsers[4], testUsers[5] };
        List<User> singleMatchUsers = new List<User>{ testUsers[0], testUsers[1], testUsers[2], testUsers[3], testUsers[4], testUsers[5] };
        User courseConsultant = testUsers[6];

        List<SkillSet__c> items = new List<SkillSet__c>();
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[0], Constants.GROUP_COURSE_ADVISORS, 'Gender: Indeterminate', false));
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[0], Constants.GROUP_COURSE_ADVISORS, 'Entry Pathway: Associate Degree', false));
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[0], Constants.GROUP_COURSE_ADVISORS, 'Portfolio: International (Other)', false));
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[1], Constants.GROUP_COURSE_ADVISORS, 'Gender: Indeterminate', false));
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[2], Constants.GROUP_COURSE_ADVISORS, 'Entry Pathway: Associate Degree', false));
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[3], Constants.GROUP_COURSE_ADVISORS, 'Gender: Female', false));
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[3], Constants.GROUP_COURSE_ADVISORS, 'Entry Pathway: Work Experience', false));
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[3], Constants.GROUP_COURSE_ADVISORS, 'Portfolio: International (Canada)', false));
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[4], Constants.GROUP_COURSE_ADVISORS, 'Gender: Indeterminate', false));
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[4], Constants.GROUP_COURSE_ADVISORS, 'Entry Pathway: Work Experience', false));
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[5], Constants.GROUP_COURSE_ADVISORS, 'Gender: Indeterminate', false));
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[6], Constants.GROUP_COURSE_CONSULTANTS, 'Portfolio: Domestic', false));
        insert items;

        Contact hotContact = TestDataContactMock.createProspectHot(data);
        System.RunAs(data)
        {
            hotContact.OwnerId = courseConsultant.Id;
            hotContact.Portfolio__c = 'International (New Zealand)';
            update hotContact;
        }
        Contact contactBefore = [SELECT Id, salutation, OwnerId, InferredEntryPathway__c, InferredGender__c, InferredPortfolio__c FROM Contact WHERE Id =: hotContact.Id ];

        // Act
        Test.startTest();
        contactBefore.salutation = 'Mr';
        update contactBefore;
        contactBefore.recalculateFormulas();
        Test.stopTest();
        simulateContactBusinessPolicy(contactBefore, Constants.GROUP_COURSE_ADVISORS);

        // Assert
        Contact contactAfter = [SELECT Id, OwnerId, Owner.inGroup__c FROM Contact WHERE Id =: contactBefore.Id];
        System.assertNotEquals(courseConsultant.Id, contactAfter.OwnerId, 'The original Course Consultant Owner Id should not be the current Contacts Owner as this should have been re-assigned to a Course Advisor.');
        System.assertEquals(Constants.GROUP_COURSE_ADVISORS, contactAfter.Owner.inGroup__c, 'The Owner of the Contact after re-assignment should be a Course Advisor.');
        System.assertEquals(new Map<Id, User>(courseAdvisors).keySet().contains(contactAfter.OwnerId), true, 'The list of all CourseAdvisor Ids should contain the current contact.ownerId.');
        System.assertEquals(new Map<Id, User>(singleMatchUsers).keySet().contains(contactAfter.OwnerId), true, 'The current contact.ownerId of the re-assigned Contact should be any one of the 6 Course Advisors as none have a matching skill set so round robin amongst them.');
    }

    @isTest
    static void CreateTwoFullMatchUsersWithNeverWeighting_AssignRandomlyToOneOfSixUsers()
    {
        // Arrange
        User data = UserService.getByAlias(Constants.USER_TESTSETUP);

        List<User> testUsers = [SELECT Id, inGroup__c FROM User WHERE Profile.Name = 'AIB Platform' AND isActive = true LIMIT 7];
        List<User> courseAdvisors = new List<User>{ testUsers[0], testUsers[1], testUsers[2], testUsers[3], testUsers[4], testUsers[5] };
        List<User> singleMatchUsers = new List<User>{ testUsers[0], testUsers[1], testUsers[2], testUsers[3], testUsers[4], testUsers[5] };
        User courseConsultant = testUsers[6];

        List<SkillSet__c> items = new List<SkillSet__c>();
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[0], Constants.GROUP_COURSE_ADVISORS, 'Gender: Male', Constants.SKILLSET_WEIGHTING_NEVER, false));
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[0], Constants.GROUP_COURSE_ADVISORS, 'Entry Pathway: Bachelor Degree', Constants.SKILLSET_WEIGHTING_NEVER, false));
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[0], Constants.GROUP_COURSE_ADVISORS, 'Portfolio: International (Other)', Constants.SKILLSET_WEIGHTING_NEVER, false));
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[1], Constants.GROUP_COURSE_ADVISORS, 'Gender: Male', Constants.SKILLSET_WEIGHTING_NEVER, false));
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[1], Constants.GROUP_COURSE_ADVISORS, 'Entry Pathway: Bachelor Degree', Constants.SKILLSET_WEIGHTING_NEVER, false));
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[1], Constants.GROUP_COURSE_ADVISORS, 'Portfolio: International (Other)', Constants.SKILLSET_WEIGHTING_NEVER, false));
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[2], Constants.GROUP_COURSE_ADVISORS, 'Entry Pathway: Associate Degree', false));
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[3], Constants.GROUP_COURSE_ADVISORS, 'Gender: Female', false));
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[3], Constants.GROUP_COURSE_ADVISORS, 'Entry Pathway: Work Experience', false));
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[4], Constants.GROUP_COURSE_ADVISORS, 'Gender: Indeterminate', false));
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[4], Constants.GROUP_COURSE_ADVISORS, 'Entry Pathway: Work Experience', false));
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[5], Constants.GROUP_COURSE_ADVISORS, 'Gender: Indeterminate', false));
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[6], Constants.GROUP_COURSE_CONSULTANTS, 'Portfolio: Domestic', false));
        insert items;

        Contact hotContact = TestDataContactMock.createProspectHot(data);
        System.RunAs(data)
        {
            hotContact.OwnerId = courseConsultant.Id;
            hotContact.Portfolio__c = 'International (Other)';
            update hotContact;
        }
        Contact contactBefore = [SELECT Id, salutation, OwnerId, InferredEntryPathway__c, InferredGender__c, InferredPortfolio__c FROM Contact WHERE Id =: hotContact.Id ];

        // Act
        Test.startTest();
        contactBefore.salutation = 'Mr';
        update contactBefore;
        contactBefore.recalculateFormulas();
        Test.stopTest();
        simulateContactBusinessPolicy(contactBefore, Constants.GROUP_COURSE_ADVISORS);

        // Assert
        Contact contactAfter = [SELECT Id, OwnerId, Owner.inGroup__c FROM Contact WHERE Id =: contactBefore.Id];
        System.assertNotEquals(courseConsultant.Id, contactAfter.OwnerId, 'The original Course Consultant Owner Id should not be the current Contacts Owner as this should have been re-assigned to a Course Advisor.');
        System.assertEquals(Constants.GROUP_COURSE_ADVISORS, contactAfter.Owner.inGroup__c, 'The Owner of the Contact after re-assignment should be a Course Advisor.');
        System.assertEquals(true, new Map<Id, User>(courseAdvisors).keySet().contains(contactAfter.OwnerId), 'The list of all CourseAdvisor Ids should contain the current contact.ownerId.');
        System.assertEquals(true, new Map<Id, User>(singleMatchUsers).keySet().contains(contactAfter.OwnerId), 'The current contact.ownerId of the re-assigned Contact should be any one of the 6 Course Advisors as none have a matching skill set that doesnt also have weighting NEVER so round robin amongst them.');
    }

    @isTest
    static void CreateTwoFullMatchCCUsers_AssignToEitherCCUser()
    {
        // Arrange
        TestUtilities.disableComponent('CerebroSyncApplicationCommand');
        User data = UserService.getByAlias(Constants.USER_TESTSETUP);
        User oafUser = UserService.getByAlias(Constants.USER_COURSEADVISOR);

        List<User> testUsers = [SELECT Id, inGroup__c FROM User WHERE Profile.Name = 'AIB Platform' AND isActive = true LIMIT 7];
        List<User> courseConsultants = new List<User>{ testUsers[0], testUsers[1], testUsers[2], testUsers[3], testUsers[4], testUsers[5], testUsers[6] };
        List<User> fullMatchUsers = new List<User>{ testUsers[0], testUsers[1] };

        List<SkillSet__c> items = new List<SkillSet__c>();
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[0], Constants.GROUP_COURSE_CONSULTANTS, 'Portfolio: International (Canada)', false));
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[1], Constants.GROUP_COURSE_CONSULTANTS, 'Portfolio: International (Canada)', Constants.SKILLSET_WEIGHTING_ALWAYS, false));
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[2], Constants.GROUP_COURSE_CONSULTANTS, 'Portfolio: Domestic', false));
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[2], Constants.GROUP_COURSE_CONSULTANTS, 'Portfolio: International (Other)', 'Frequently', false));
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[3], Constants.GROUP_COURSE_CONSULTANTS, 'Portfolio: International (Canada)', Constants.SKILLSET_WEIGHTING_NEVER, false));
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[3], Constants.GROUP_COURSE_CONSULTANTS, 'Portfolio: Domestic', 'Often', false));
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[4], Constants.GROUP_COURSE_CONSULTANTS, 'Portfolio: International (Canada)', 'Never', false));
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[6], Constants.GROUP_COURSE_CONSULTANTS, 'Portfolio: Domestic', false));
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[5], Constants.GROUP_COURSE_CONSULTANTS, 'Portfolio: International (New Zealand)', 'Sometimes', false));
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[5], Constants.GROUP_COURSE_CONSULTANTS, 'Portfolio: Domestic', 'Occasionally', false));
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[6], Constants.GROUP_COURSE_CONSULTANTS, 'Portfolio: International (Other)',  Constants.SKILLSET_WEIGHTING_ALWAYS, false));
        items.add(TestDataSkillSetMock.createStaff(data, testUsers[6], Constants.GROUP_COURSE_CONSULTANTS, 'Portfolio: Domestic', 'Rarely', false));
        insert items;

        Contact hotContact = TestDataContactMock.createProspectHot(data);
        System.RunAs(data)
        {
            hotContact.OwnerId = oafUser.Id;
            update hotContact;
        }
        Contact contactBefore = [SELECT Id, salutation, OwnerId, InferredEntryPathway__c, InferredGender__c, InferredPortfolio__c FROM Contact WHERE Id =: hotContact.Id ];

        // Act
        Test.startTest();
        contactBefore.Portfolio__c = 'International (Canada)';
        update contactBefore;
        contactBefore.recalculateFormulas();
        Test.stopTest();
        simulateContactBusinessPolicy(contactBefore, Constants.GROUP_COURSE_CONSULTANTS);

        // Assert
        Contact contactAfter = [SELECT Id, Owner.Name, OwnerId, Owner.inGroup__c FROM Contact WHERE Id =: contactBefore.Id];

        System.assertNotEquals(oafUser.Id, contactAfter.OwnerId, 'The original OAF Course Advisor Owner Id should not be the current Contacts Owner as this should have been re-assigned to one of the two matching Course Consultant.');
        System.assertEquals(Constants.GROUP_COURSE_CONSULTANTS, contactAfter.Owner.inGroup__c, 'The Owner of the Contact after re-assignment should be a Course Consultant.');
        System.assertEquals(new Map<Id, User>(courseConsultants).keySet().contains(contactAfter.OwnerId), true, 'The list of all CourseConsultant Ids should contain the current contact.ownerId.');
        System.assertEquals(new Map<Id, User>(fullMatchUsers).keySet().contains(contactAfter.OwnerId), true, 'The current contact.ownerId of the re-assigned Contact should be one of the 2 matching users set up based on skill sets above.');
    }

    private static void simulateContactBusinessPolicy(Contact ctct, String groupName)
    {
        DecsOnD__Policy__c policy = DecsOnD.TestUtils.createTestPolicy('Contact', 'Assignment', true);
        DecsOnD.PolicyInvocationContext context = new DecsOnD.PolicyInvocationContext(policy);

        Map<String, Object> params = new Map<String, Object>();
        params.put(DecsOnD.AssignmentConstants.GROUP_PARAMETER, groupName);
        params.put(DecsOnD_AssignmentGlobals.ASSIGNMENT_OPTIONS_PARAM, DecsOnD_AssignmentGlobals.SKILLS_BASED_OPTION);
        DecsOnD.PolicyActionRecord actionRec = DecsOnD.TestUtils.createTestActionRecord(context, ctct, DecsOnD.AssignOwnerAction.NAME, params);

        // Apply the policy
        actionRec.action.prepare(actionRec);
        DecsOnD.PolicyActionHandler.applyAction(actionRec);
        if (actionRec.batch != null)
            actionRec.batch.apply();
    }
}