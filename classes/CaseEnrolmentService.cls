public class CaseEnrolmentService
{
    public static ActionResult create(Id caseId, Set<Id> enrolmentIds)
    {
        List<ViewModelsCaseEnrolment.Details> items = new List<ViewModelsCaseEnrolment.Details>();

        for (Id enrolmentId : enrolmentIds)
        {
            ViewModelsCaseEnrolment.Details viewModel = new ViewModelsCaseEnrolment.Details();
            viewModel.caseId = caseId;
            viewModel.subjectEnrolmentId = enrolmentId;
            items.add(viewModel);
        }

        return create(items);
    }

    public static ActionResult create(List<ViewModelsCaseEnrolment.Details> items)
    {
        return new CaseEnrolmentCreateCommand().execute(items);
    }

    public static ActionResult createExemptionEnrolments(Id caseId, Set<Id> recordIds)
    {
        List<ViewModelsCaseEnrolment.ExemptionDetails> items = new List<ViewModelsCaseEnrolment.ExemptionDetails>();

        for (Id recordId : recordIds)
        {
            ViewModelsCaseEnrolment.ExemptionDetails viewModel = new ViewModelsCaseEnrolment.ExemptionDetails();
            viewModel.caseId = caseId;
            viewModel.recordId = recordId;
            items.add(viewModel);
        }

        return createExemptionEnrolments(items);
    }

    public static ActionResult createExemptionEnrolments(List<ViewModelsCaseEnrolment.ExemptionDetails> items)
    {
        return new CaseEnrolmentCreateExemptionCommand().execute(items);
    }

    public static ActionResult updateParent(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        return new CaseEnrolmentUpdateParentCommand().execute(items, oldMap, triggerContext);
    }

    public static ActionResult deleteChild(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        return new CaseEnrolmentDeleteChildCommand().execute(items, oldMap, triggerContext);
    }

    public static ActionResult validateDeletion(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        return new CaseEnrolmentDeleteValidation().execute(items, oldMap, triggerContext);
    }

    public static List<Interfaces.IViewModel> querySubjectsByPortalUser(Id userId)
    {
        return new CaseEnrolmentGetSubjectsQuery().queryByPortalUser(userId);
    }

    public static ViewModelsCaseEnrolment.Payload querySubjectsByCase(Id caseId)
    {
        return new CaseEnrolmentGetSubjectsQuery().queryByCase(caseId);
    }
}