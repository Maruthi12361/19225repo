@isTest
public class ContentVersionUpdateTypeCommandTests
{
    @testSetup
    public static void setup()
    {
        TestUtilities.setupHEDA();
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createProspectHot(user);
        TestDataOpportunityMock.createApplyingAcquisition(user, contact);
    }

    @isTest
    static void CreateNewInvoiceFiles_InvoiceTypeSetSuccessfully()
    {
        // Arrange
        Contact contact = [SELECT Id FROM Contact LIMIT 1];

        // Act
        ContentVersion contentVersion = TestDataContentVersionMock.create(UserService.getByAlias(Constants.USER_SYSTEMADMIN), 'John Smith Invoice.txt', 'John Smith Invoice', contact.Id);
        ContentVersion secondContentVersion = TestDataContentVersionMock.create(UserService.getByAlias(Constants.USER_SYSTEMADMIN), 'John INV Smith.txt', 'John INV Smith', contact.Id);
        ContentVersion thirdContentVersion = TestDataContentVersionMock.create(UserService.getByAlias(Constants.USER_SYSTEMADMIN), 'John Invocing.txt', 'John Invocing', contact.Id);
        ContentVersion fourthContentVersion = TestDataContentVersionMock.create(UserService.getByAlias(Constants.USER_SYSTEMADMIN), 'John inv Smith.txt', 'John inv Smith.txt', contact.Id);
        ContentVersion updatedContentVersion = [ SELECT Id, Type__c FROM ContentVersion WHERE Id = :contentVersion.Id ];
        ContentVersion updatedSecondContentVersion = [ SELECT Id, Type__c FROM ContentVersion WHERE Id = :secondContentVersion.Id ];
        ContentVersion updatedThirdContentVersion = [ SELECT Id, Type__c FROM ContentVersion WHERE Id = :thirdContentVersion.Id ];
        ContentVersion updatedFourthContentVersion = [ SELECT Id, Type__c FROM ContentVersion WHERE Id = :fourthContentVersion.Id ];

        // Assert
        System.assertEquals(Constants.CONTENTVERSION_TYPE_INVOICE, updatedContentVersion.Type__c, 'Unexpected version type for first file');
        System.assertEquals(Constants.CONTENTVERSION_TYPE_INVOICE, updatedSecondContentVersion.Type__c, 'Unexpected version type for second file');
        System.assertEquals(null, updatedThirdContentVersion.Type__c, 'Unexpected version type for third file');
        System.assertEquals(null, updatedFourthContentVersion.Type__c, 'Unexpected version type for fourth file');
    }

    @isTest
    static void CreateNewLOOFiles_LOOTypeSetSuccessfully()
    {
        // Arrange
        Contact contact = [SELECT Id FROM Contact LIMIT 1];

        // Act
        ContentVersion contentVersion = TestDataContentVersionMock.create(UserService.getByAlias(Constants.USER_SYSTEMADMIN), 'John Smith Letter of Offer.txt', 'John Smith Letter of Offer', contact.Id);
        ContentVersion secondContentVersion = TestDataContentVersionMock.create(UserService.getByAlias(Constants.USER_SYSTEMADMIN), 'John LOO Smith.txt', 'John LOO Smith', contact.Id);
        ContentVersion thirdContentVersion = TestDataContentVersionMock.create(UserService.getByAlias(Constants.USER_SYSTEMADMIN), 'John Looing.txt', 'John Looing', contact.Id);
        ContentVersion fourthContentVersion = TestDataContentVersionMock.create(UserService.getByAlias(Constants.USER_SYSTEMADMIN), 'John loo Smith.txt', 'John loo Smith', contact.Id);
        ContentVersion updatedContentVersion = [ SELECT Id, Type__c FROM ContentVersion WHERE Id = :contentVersion.Id ];
        ContentVersion updatedSecondContentVersion = [ SELECT Id, Type__c FROM ContentVersion WHERE Id = :secondContentVersion.Id ];
        ContentVersion updatedThirdContentVersion = [ SELECT Id, Type__c FROM ContentVersion WHERE Id = :thirdContentVersion.Id ];
        ContentVersion updatedFourthContentVersion = [ SELECT Id, Type__c FROM ContentVersion WHERE Id = :fourthContentVersion.Id ];

        // Assert
        System.assertEquals(Constants.CONTENTVERSION_TYPE_LOO, updatedContentVersion.Type__c, 'Unexpected version type for first file');
        System.assertEquals(Constants.CONTENTVERSION_TYPE_LOO, updatedSecondContentVersion.Type__c, 'Unexpected version type for second file');
        System.assertEquals(null, updatedThirdContentVersion.Type__c, 'Unexpected version type for third file');
        System.assertEquals(null, updatedFourthContentVersion.Type__c, 'Unexpected version type for fourth file');
    }

    @isTest
    static void CreateNewTimetableFiles_TimetableTypeSetSuccessfully()
    {
        // Arrange
        Contact contact = [SELECT Id FROM Contact LIMIT 1];

        // Act
        ContentVersion contentVersion = TestDataContentVersionMock.create(UserService.getByAlias(Constants.USER_SYSTEMADMIN), 'John Smith Timetable.txt', 'John Smith Timetable', contact.Id);
        ContentVersion secondContentVersion = TestDataContentVersionMock.create(UserService.getByAlias(Constants.USER_SYSTEMADMIN), 'John TT Smith.txt', 'John TT Smith', contact.Id);
        ContentVersion thirdContentVersion = TestDataContentVersionMock.create(UserService.getByAlias(Constants.USER_SYSTEMADMIN), 'John Smitting.txt', 'John Smitting', contact.Id);
        ContentVersion fourthContentVersion = TestDataContentVersionMock.create(UserService.getByAlias(Constants.USER_SYSTEMADMIN), 'John tt Smith.txt', 'John t Smith', contact.Id);
        ContentVersion updatedContentVersion = [ SELECT Id, Type__c FROM ContentVersion WHERE Id = :contentVersion.Id ];
        ContentVersion updatedSecondContentVersion = [ SELECT Id, Type__c FROM ContentVersion WHERE Id = :secondContentVersion.Id ];
        ContentVersion updatedThirdContentVersion = [ SELECT Id, Type__c FROM ContentVersion WHERE Id = :thirdContentVersion.Id ];
        ContentVersion updatedFourthContentVersion = [ SELECT Id, Type__c FROM ContentVersion WHERE Id = :fourthContentVersion.Id ];

        // Assert
        System.assertEquals(Constants.CONTENTVERSION_TYPE_TIMETABLE, updatedContentVersion.Type__c, 'Unexpected version type for first file');
        System.assertEquals(Constants.CONTENTVERSION_TYPE_TIMETABLE, updatedSecondContentVersion.Type__c, 'Unexpected version type for second file');
        System.assertEquals(null, updatedThirdContentVersion.Type__c, 'Unexpected version type for third file');
        System.assertEquals(null, updatedFourthContentVersion.Type__c, 'Unexpected version type for fourth file');
    }
}