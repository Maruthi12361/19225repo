public class ContactReassociateHotCampaignCommand extends BaseTriggerCommand
{
    static Map<Id, Contact> activeRecordMap = new Map<Id, Contact>();

    private static List<Campaign> allCampaignList;

    public override List<SObject> validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        List<Contact> contactList = new List<Contact>();

        if (triggerContext == Enums.TriggerContext.AFTER_UPDATE)
        {
            for (Contact newContact : (List<Contact>) items)
            {
                Contact oldContact = (Contact) oldMap.get(newContact.Id);

                if (newContact.Status__c == Constants.STATUS_HOT && newContact.Preferred_Start_Date__c != null && !newContact.Preferred_Start_Date__c.startsWith('At A Later Date') &&
                    (newContact.Status__c != oldContact.Status__c || newContact.Preferred_Start_Date__c != oldContact.Preferred_Start_Date__c || newContact.ProgramPortfolio__c != oldContact.ProgramPortfolio__c))
                {
                    if (activeRecordMap.containsKey(newContact.Id))
                    {
                        Contact activeRecord = activeRecordMap.get(newContact.Id);

                        if (activeRecord.PreferredCampaign__c == newContact.PreferredCampaign__c)
                        {
                            Log.info(this.className, 'validate', 'Contact {0} has already been processed in this transaction. CampaignMember not created. ', new List<String>{newContact.Id});
                            continue;
                        }
                    }

                    contactList.add(newContact);
                }
            }
        }
        else if (triggerContext == Enums.TriggerContext.AFTER_INSERT)
        {
            for (Contact newContact : (List<Contact>) items)
            {
                if (newContact.Status__c == Constants.STATUS_HOT && newContact.Preferred_Start_Date__c != null && !newContact.Preferred_Start_Date__c.startsWith('At A Later Date'))
                    contactList.add(newContact);
            }
        }

        return contactList;
    }

    public override ActionResult command(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        Map<Id, Contact> contactMap = new Map<Id, Contact>((List<Contact>)items);

        List<Contact> contactList = 
            [
                SELECT Id, PreferredCampaign__c, ProgramPortfolio__c,
                (
                    SELECT Id, Status, CampaignId
                    FROM CampaignMembers
                    WHERE Campaign.Type IN ('Acquisition')
                ),
                (
                    SELECT Id, CampaignId
                    FROM Opportunities__r
                    WHERE RecordType.DeveloperName = : Constants.RECORDTYPE_OPPORTUNITY_ACQUISITION AND IsClosed = false
                    ORDER By LastModifiedDate DESC
                    LIMIT 1
                )
                FROM Contact
                WHERE RecordType.DeveloperName =: Constants.RECORDTYPE_CONTACT_PROSPECT AND Id IN : contactMap.keySet()
            ];

        List<CampaignMember> newCampaignMemberList = new List<CampaignMember>();
        List<CampaignMember> updatedCampaignMemberList = new List<CampaignMember>();
        List<Contact> updatedContactList = new List<Contact>();
        List<Opportunity> updatedOpportunityList = new List<Opportunity>();

        for (Contact contact : contactList)
        {
            Contact newContact = contactMap.get(contact.Id);
            String year = newContact.Preferred_Start_Date__c.right(4);
            String month = newContact.Preferred_Start_Date__c.mid(3, 2);
            String day = newContact.Preferred_Start_Date__c.left(2);
            Date preferredStartDate = Date.valueOf(year + '-' + month + '-' + day);

            CampaignMember newCampaignMember = createNewCampaignMember(contact.Id, preferredStartDate, newContact.Portfolio__c, newContact.Program__c);
            CampaignMember currentCampaignMember;

            for (CampaignMember campaignMember : contact.CampaignMembers)
            {
                if (newCampaignMember != null && campaignMember.CampaignId == newCampaignMember.CampaignId)
                {
                    newCampaignMember = campaignMember;
                    Log.info(this.className, 'command', 'Found existing campaign member: ' + campaignMember);
                }

                if (campaignMember.CampaignId == contact.PreferredCampaign__c)
                {
                    currentCampaignMember = campaignMember;
                    Log.info(this.className, 'command', 'Found current campaign member: ' + campaignMember);
                }
            }

            if (newCampaignMember != null)
            {
                if (currentCampaignMember != null && currentCampaignMember.CampaignId != newCampaignMember.CampaignId && currentCampaignMember.Status != 'Moved')
                {
                    currentCampaignMember.Status = 'Moved';
                    updatedCampaignMemberList.add(currentCampaignMember);
                }

                if (newCampaignMember.Id == null)
                    newCampaignMemberList.add(newCampaignMember);
                else if (newCampaignMember.Status != 'Sent')
                {
                    newCampaignMember.Status = 'Sent';
                    updatedCampaignMemberList.add(newCampaignMember);
                }

                if (contact.PreferredCampaign__c != newCampaignMember.CampaignId)
                {
                    contact.PreferredCampaign__c = newCampaignMember.CampaignId;
                    updatedContactList.add(contact);
                    activeRecordMap.put(contact.Id, contact);

                    if (contact.Opportunities__r.size() > 0)
                    {
                        Opportunity opportunity = contact.Opportunities__r[0];

                        if (opportunity.CampaignId != newCampaignMember.CampaignId)
                        {
                            opportunity.CampaignId = newCampaignMember.CampaignId;
                            updatedOpportunityList.add(opportunity);
                        }
                    }
                }
            }
        }

        if (newCampaignMemberList.size() > 0)
            uow.registerNew(newCampaignMemberList);

        if (updatedCampaignMemberList.size() > 0)
            uow.registerDirty(updatedCampaignMemberList);

        if (updatedContactList.size() > 0)
            uow.registerDirty(updatedContactList);

        if (updatedOpportunityList.size() > 0)
            uow.registerDirty(updatedOpportunityList);

        return new ActionResult(ResultStatus.SUCCESS);
    }

    private CampaignMember createNewCampaignMember(Id contactId, Date preferredStartDate, String portfolio, String program)
    {
        CampaignMember newCampaignMember;
        Campaign assignedCampaign;

        if (!String.isBlank(portfolio))
        {
            if (portfolio.startsWith('TC'))
                portfolio = 'TC';
            else if (portfolio.startsWith('International'))
                portfolio = 'International';
            else
                portfolio = 'Domestic';
        }

        if (!String.isBlank(program))
        {
            if (program != 'BBA' && program != 'MBA' && program != 'GCM')
                program = 'MBA';
        }
        
        if (allCampaignList == null)
        {
            allCampaignList = 
                [
                    SELECT Id, TermStartDate__c, Portfolio__c, Programme__c
                    FROM Campaign
                    WHERE Type IN ('Acquisition') AND Status IN : Constants.CAMPAIGN_ACTIVE_STATUS AND IsActive = true
                    ORDER BY TermStartDate__c
                ];
        }

        for (Campaign campaign : allCampaignList)
        {
            if (campaign.TermStartDate__c == preferredStartDate && campaign.Programme__c == program && campaign.Portfolio__c == portfolio)
            {
                assignedCampaign = campaign;
                break;
            }
        }

        // Create campaign member
        if (assignedCampaign != null)
        {
            newCampaignMember = New CampaignMember();
            newCampaignMember.CampaignId = assignedCampaign.Id;
            newCampaignMember.Campaign = assignedCampaign;
            newCampaignMember.ContactId = ContactId;
        }
        Log.info(this.className, 'createNewCampaignMember', 'Match preferredStartDate: ' + preferredStartDate + ' to New Campaign: ' + assignedCampaign);

        return newCampaignMember;
    }
}