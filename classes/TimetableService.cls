public class TimetableService
{
    public static ViewModelsTimetable.Data getTimetable(ViewModels.Param param)
    {
        return (ViewModelsTimetable.Data) new TimetableGetQuery().query(param);
    }

    public static ActionResult saveTimetable(ViewModelsTimetable.EnrolmentDetails param)
    {
        return new TimetableSaveCommand().command(param);
    }
}