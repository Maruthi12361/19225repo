public class ContactSendSSOResetPassEmailCommand extends BaseComponent
{
    @testVisible
    private static String templateName = 'Single Sign On - Reset Password';

    public ViewModelsSSO.EmailLoginDetails validate(ViewModelsSSO.EmailLoginDetails param)
    {
        EmailTemplate emailTemplate = EmailService.getEmailTemplate(templateName);

        if (emailTemplate == null)
        {
            String message = String.format('No email template found for {0}', new List<String> { templateName });
            Log.error(this.className, 'validate', message);
            throw new Exceptions.ApplicationException(message);
        }

        if (param == null)
        {
            Log.error(this.className, 'validate', Constants.ERRORMSG_NULLPARAM, new List<String> { 'Param object' });
            throw new Exceptions.ApplicationException(String.format(Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Param object' }));
        }
        else if (param.contactId == null)
        {
            Log.error(this.className, 'validate', Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Contact Id' });
            throw new Exceptions.ApplicationException(String.format(Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Contact Id' }));
        }
        else if (String.isBlank(param.contactEmail))
        {
            Log.error(this.className, 'validate', Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Contact email' });
            throw new Exceptions.ApplicationException(String.format(Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Contact email' }));
        }
        else if (String.isBlank(param.contactFirstName))
        {
            Log.error(this.className, 'validate', Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Contact first name' });
            throw new Exceptions.ApplicationException(String.format(Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Contact first name' }));
        }
        else if (String.isBlank(param.contactLastName))
        {
            Log.error(this.className, 'validate', Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Contact last name' });
            throw new Exceptions.ApplicationException(String.format(Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Contact last name' }));
        }
        else if (String.isBlank(param.password))
        {
            Log.error(this.className, 'validate', Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Password' });
            throw new Exceptions.ApplicationException(String.format(Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Password' }));
        }

        return param;
    }

    public ActionResult command(ViewModelsSSO.EmailLoginDetails param)
    {
        if (!isEnabled)
        {
            Log.info(this.className, 'command', 'Skipping command. Disabled through configuration.'); 
            return new ActionResult(ResultStatus.SUCCESS);
        }

        validate(param);
        EmailTemplate emailTemplate = EmailService.getEmailTemplate(templateName);

        Log.info(this.className, 'command', 'Sending SSO reset email for {0}', new List<String> { param.contactId });

        String body = emailTemplate.HtmlValue;
        User currentUser = UserService.get(UserInfo.getUserId());
        Contact contact = ContactService.get(param.contactId, 'Id, FirstName, LastName, OAFBaseURL__c, EncryptedId__c', false);

        body = body.replace('{contact.FirstName}', contact.FirstName);
        body = body.replace('{contact.OAFBaseURL__c}', contact.OAFBaseURL__c);
        body = body.replace('{contact.EncryptedId__c}', contact.EncryptedId__c);
        body = body.replace('{password}', param.password);

        if (currentUser.Alias != Constants.USER_COURSEADVISOR)
        {
            body = body.replace('{user.FirstName}', currentUser.FirstName);
            body = body.replace('{user.Title}', String.isNotEmpty(currentUser.Title) ? currentUser.Title : '');
        }
        else
        {
            body = body.replace('{user.FirstName}', 'Student Central');
            body = body.replace('{user.Title}', '');
        }

        return sendEmail(param.contactEmail, param.contactFirstName + ' ' + param.contactLastName, emailTemplate.Subject, body, Constants.STUDENTCENTRAL_EMAIL, Constants.STUDENTCENTRAL_NAME);
    }

    public static ActionResult sendEmail(String email, String name, String subject, String body, String replyEmail, String replyName)
    {
        ActionResult result = EmailService.sendToRecipient(Constants.APIPROVIDER_DEFAULT, email, name, subject, replyEmail, replyName, null, body, null);

        if (result.isError)
            Log.error('ContactSendSSOLoginDetailsCommand', 'sendEmail', 'Failed to send Reset details email {0} to {1}<{2}> due to: {3}.', new List<String> { templateName, name, email, String.valueOf(result.messages) });

        return result;
    }
}