public class CaseUpdateSSOStatusCommand extends BaseTriggerCommand
{
    protected override List<SObject> validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        List<Case> result = new List<Case>();
        Id applicationRecordTypeId = RecordTypeUtils.getRecordTypeId(Constants.RECORDTYPE_CASE_APPLICATION, SObjectType.Case);

        for (Case currentCase : (List<Case>) items)
        {
            Case oldValue = (Case) oldMap.get(currentCase.Id);

            if (currentCase.RecordTypeId == applicationRecordTypeId && currentCase.contactId != null &&
                ((currentCase.FEE_HELP_Processed__c != oldValue.FEE_HELP_Processed__c && currentCase.FEE_HELP_Processed__c == true) ||
                (currentCase.Invoice_Processed__c != oldValue.Invoice_Processed__c && currentCase.Invoice_Processed__c == true)))
            {
                result.add(currentCase);
            }
        }

        return result;
    }

    protected override ActionResult command(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        List<Contact> contactsToEnableSSO = new List<Contact>();
        List<Contact> contacts = Contactservice.getMany(SObjectUtils.getIDs(items, 'contactId'), 'Id, Cerebro_Student_ID__c, SingleSignOnStatus__c', false);

        for (Case currentCase : (List<Case>) items)
        {
            Contact contact = getContact(currentCase.contactId, contacts);

            if (contact == null)
            {
                Log.info(this.className, 'command', 'No contact found {0}', new List<String> { currentCase.contactId });
                continue;
            }

            if (String.isBlank(contact.Cerebro_Student_ID__c))
            {
                Log.info(this.className, 'command', 'No cerebro student identifier found for {0} contact', new List<String> { currentCase.contactId });
                continue;
            }

            if (contact.SingleSignOnStatus__c == Constants.CONTACT_SINGLESIGNONSTATUS_PROSPECTENABLED)
            {
                contact.SingleSignOnStatus__c = Constants.CONTACT_SINGLESIGNONSTATUS_STUDENTPROVISIONING;
                contact.SingleSignOnStatusLastUpdated__c = Datetime.now();
                contactsToEnableSSO.add(contact);
                uow.registerDirty(contact);
            }
        }

        return ContactService.enableSSO(contactsToEnableSSO);
    }

    private Contact getContact(Id contactId, List<Contact> contacts)
    {
        for (Contact contact : contacts)
            if (contact.Id == contactId)
                return contact;

        return null;
    }
}