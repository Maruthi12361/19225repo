public class ContactUpdateUserOnChangeCommand extends BaseTriggerCommand
{
    public override List<SObject> validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        List<Contact> result = new List<Contact>();

        if (triggerContext == Enums.TriggerContext.AFTER_UPDATE)
        {
            for (Contact newContact : (List<Contact>) items)
            {
                Contact oldContact = (Contact) oldMap.get(newContact.Id);

                if (newContact.HasPortalUser__c && SObjectUtils.hasAnyPropertyChanged(newContact, oldContact, new List<String> {'FirstName', 'MiddleName', 'LastName'}))
                {
                    result.add(newContact);
                }
            }
        }

        return result;
    }

    public override ActionResult command(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        Map<Id, Contact> contactMap = new Map<Id, Contact>((List<Contact>)items);

        for (User portalUser :
             [
                 SELECT Id, FirstName, MiddleName, LastName, ContactId
                 FROM User
                 WHERE ContactId != NULL AND IsActive = true AND ContactId IN : items
             ])
        {
            Boolean isDirty = false;
            Contact contact = contactMap.get(portalUser.ContactId);

            if (!StringUtils.equal(contact.FirstName, portalUser.FirstName))
            {
                portalUser.FirstName = contact.FirstName;
                isDirty = true;
            }

            if (!StringUtils.equal(contact.MiddleName, portalUser.MiddleName))
            {
                portalUser.MiddleName = contact.MiddleName;
                isDirty = true;
            }

            if (!StringUtils.equal(contact.LastName, portalUser.LastName))
            {
                portalUser.LastName = contact.LastName;
                isDirty = true;
            }

            if (isDirty)
                uow.registerDirty(portalUser);
        }

        return new ActionResult(ResultStatus.SUCCESS);
    }
}