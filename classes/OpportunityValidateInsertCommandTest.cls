@IsTest
public class OpportunityValidateInsertCommandTest
{
    @testSetup
    static void setup()
    {
        TestUtilities.setupHEDA();
    }

    @IsTest
    static void AdminUserCreatesOpportunityWithNoContact_OpportunitySuccessfullyCreated()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);

        // Act
        Opportunity opp = TestDataOpportunityMock.create(user);

        // Assert
        System.assertNotEquals(null, opp, 'Opportunity suppose to be created');
    }

    @IsTest
    static void NonAdminUserCreatesOpportunityWithNoContact_OpportunityPreventedFromCreation()
    {
        // Arrange
        String errorMsg = '';
        User user = UserService.getByAlias(Constants.USER_COURSEADVISOR);

        // Act
        try
        {
            Opportunity opp = TestDataOpportunityMock.create(user);
        }
        catch(Exception e)
        {
            errorMsg = e.getMessage();
        }

        // Assert
        System.assertEquals(true, errorMsg.contains('Opportunity is not associated with a contact') , 'Unexpected error message');
    }

    @IsTest 
    static void NonAdminUserCreatesOpportunityWithContactNotInHotStatus_OpportunityPreventedFromCreation()
    {
        // Arrange
        String errorMsg = '';
        User user = UserService.getByAlias(Constants.USER_COURSEADVISOR);
        Contact contact = TestDataContactMock.createProspectWarm(user);

        // Act
        try
        {
            Opportunity opp = TestDataOpportunityMock.create(user, contact, 'Applying', true);
        }
        catch(Exception e)
        {
            errorMsg = e.getMessage();
        }

        // Assert
        System.assertEquals(true, errorMsg.contains('Associated contact not in a Hot status') , 'Unexpected error message');
    }

    @IsTest
    static void NonAdminUserCreatesOpportunityWithContactInHotStatusWithOpenOpp_OpportunityPreventedFromCreation()
    {
        // Arrange
        TestUtilities.disableComponent('CerebroSyncApplicationCommand');
        String errorMsg = '';
        User user = UserService.getByAlias(Constants.USER_COURSEADVISOR);
        Contact contact = TestDataContactMock.createProspectHot(user, true);

        // Act
        try
        {
            Opportunity opp = TestDataOpportunityMock.create(user, contact, 'Applying', true);
        }
        catch(Exception e)
        {
            errorMsg = e.getMessage();
        }

        // Assert
        System.assertEquals(true, errorMsg.contains('There is at least one open acquisition opportunity associated with Contact') , 'Unexpected error message');
    }
}