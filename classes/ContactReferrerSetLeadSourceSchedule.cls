public class ContactReferrerSetLeadSourceSchedule implements Schedulable 
{
    public void execute(SchedulableContext sc)
    {
        Database.executebatch(new ContactReferrerSetLeadSourceBatch(), 10);
    }
}