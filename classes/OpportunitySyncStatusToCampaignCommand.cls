public class OpportunitySyncStatusToCampaignCommand extends BaseTriggerCommand
{
    public override List<SObject> validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        List<Opportunity> filteredList = new List<Opportunity>();        
        
        for (Opportunity newOpportunity: (List<Opportunity>)items)
        {   
            if (newOpportunity.RecordTypeId != Id.valueOf(Label.ID_RecordType_Opportunity_Progression))
            {
                Log.info(this.className, 'validate', 'Ignoring Opportunity ' + newOpportunity.Id + '. Not a Progression record type.');
                continue;
            }
            
            if (triggerContext == Enums.TriggerContext.AFTER_UPDATE)
            {
                Opportunity oldOpportunity = (Opportunity) oldMap.get(newOpportunity.Id);
                
                if (newOpportunity.StageName == oldOpportunity.StageName && newOpportunity.CampaignId == oldOpportunity.CampaignId)
                {
                    Log.info(this.className, 'validate', 'Ignoring Opportunity ' + newOpportunity.Id + '. StageName/Campaign has not changed.');
                    continue;
                }
                
                if (newOpportunity.StageName != 'Closed Won' && newOpportunity.StageName != 'Closed Lost' && newOpportunity.CampaignId == oldOpportunity.CampaignId)
                {
                    Log.info(this.className, 'validate', 'Ignoring Opportunity ' + newOpportunity.Id + '. Status is not Closed Won/Lost.');
                    continue;
                }
            }
            else if (triggerContext != Enums.TriggerContext.AFTER_INSERT)
                continue;
            
            filteredList.add(newOpportunity);
        }
        
        return filteredList;
    }
    
    public override ActionResult command(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        Map<string, Opportunity> opportunityMap = new Map<string, Opportunity>();
        List<CampaignMember> updates = new List<CampaignMember>();
        List<Id> campaignIds = new List<Id>();
        List<Id> contactIds = new List<Id>();
        
        for (Opportunity newOpportunity : (List<Opportunity>)items)
        {            
            contactIds.add(newOpportunity.Contact__c);
            Id campaignId = newOpportunity.CampaignId;
            
            campaignIds.add(campaignId);
            opportunityMap.put((string)campaignId + (string)newOpportunity.Contact__c, newOpportunity);
            
            if (triggerContext == Enums.TriggerContext.AFTER_UPDATE)
            {
                Opportunity oldOpportunity = (Opportunity) oldMap.get(newOpportunity.Id);
                
                if (newOpportunity.CampaignId != oldOpportunity.CampaignId)
                {
                    campaignId = oldOpportunity.CampaignId;
                    campaignIds.add(campaignId);
                    opportunityMap.put((string)campaignId + (string)newOpportunity.Contact__c, newOpportunity);
                }
            }
        }
        
        List<CampaignMember> members = 
            [
                SELECT Id, CampaignId, Status, Campaign_Contact_Id__c
                FROM CampaignMember
                WHERE CampaignId IN :campaignIds
                    AND ContactId IN :contactIds
            ];
        
        for (CampaignMember member : members) 
        {
            Opportunity newOpportunity = opportunityMap.get(member.Campaign_Contact_Id__c);
            
            if (newOpportunity == null)
                continue;
            
            if (triggerContext == Enums.TriggerContext.AFTER_UPDATE)
            {
                Opportunity oldOpportunity = (Opportunity) oldMap.get(newOpportunity.Id);
                
                if (newOpportunity.CampaignId != oldOpportunity.CampaignId)
                {
                    if (member.CampaignId == oldOpportunity.CampaignId)
                    {
                        if (member.Status != RolloverConstantsDefinition.ST_CLOSED_LOST)
                        {
                            member.Status = RolloverConstantsDefinition.ST_CLOSED_LOST;
                            updates.add(member);
                        }
                    }
                    else if (member.CampaignId == newOpportunity.CampaignId)
                    {
                        if (member.Status != RolloverConstantsDefinition.ST_ENROLLING)
                        {
                            member.Status = RolloverConstantsDefinition.ST_ENROLLING;
                            updates.add(member);
                        }
                    }
                }
                else if (member.Status != newOpportunity.StageName)
                {
                    member.Status = newOpportunity.StageName; 
                    updates.add(member);
                }
            }
            else if (triggerContext == Enums.TriggerContext.AFTER_INSERT && member.Status != RolloverConstantsDefinition.ST_ENROLLING)
            {
                member.Status = RolloverConstantsDefinition.ST_ENROLLING;
                updates.add(member);
            }
        }
        
        if (updates.size() > 0)
            uow.registerDirty(updates);
        
        return new ActionResult(ResultStatus.SUCCESS);
    }
}