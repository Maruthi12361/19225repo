@isTest
public class ContactGetOfferingFacultiesQueryTests
{
    @testSetup
    public static void setup()
    {
        TestUtilities.setupHEDA();
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Account educationalAccount = TestDataAccountMock.create(user, Constants.ACCOUNT_INSTITUTION_AIB);

        Account department = TestDataAccountMock.createDepartmentBusiness(user);
        hed__Term__c term = TestDataTermMock.create(user, department.Id, Date.today());
        hed__Course__c subjectDefinition = TestDataSubjectMock.createDefinitionLEAD(user);
        hed__Course__c subjectVersion = TestDataSubjectMock.createVersionLEAD(user);
        TestDataSubjectOfferingMock.createOffering(user, subjectVersion, term);

        Contact facultyContact = TestDataContactMock.createFacultyContact(user);
        TestDataSkillSetMock.createFaculty(user, facultyContact, subjectDefinition);
        TestDataAffiliationMock.createAppointment(user, educationalAccount, facultyContact, 'Online Facilitator', 'Current');
    }

    @isTest
    static void SubjectOfferingWithSkilledFacultyContact_SuccessfullyReturnedListOfFacultyContacts()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        hed__Course_Offering__c subjectOffering = [SELECT Id FROM hed__Course_Offering__c LIMIT 1];

        ViewModels.Param param = new ViewModels.Param();
        param.Identifier = subjectOffering.Id;

        // Act
        test.startTest();
        List<ViewModelsWorkforce.FacultyStaffPlanOffering> facultyContacts = new ContactGetOfferingFacultiesQuery().query(param);
        test.stopTest();

        // Assert
        System.assertEquals(1, facultyContacts.size(), 'Expected one faculty contact to be returned');
    }
}