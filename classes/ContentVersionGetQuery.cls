public class ContentVersionGetQuery extends BaseViewModelQuery
{
    public override List<Interfaces.IViewModel> query(Set<Id> parentIds)
    {
        List<ViewModelsFile.Details> items = new List<ViewModelsFile.Details>();
        List<ContentDocumentLink> cdLinks =
        [
            SELECT Id, ContentDocumentId, LinkedEntityId, ContentDocument.LatestPublishedVersionId,
                ContentDocument.LatestPublishedVersion.Title, ContentDocument.LatestPublishedVersion.FileType,
                ContentDocument.LatestPublishedVersion.ContentSize, ContentDocument.LatestPublishedVersion.CreatedDate,
                ContentDocument.LatestPublishedVersion.Type__c
            FROM ContentDocumentLink
            WHERE LinkedEntityId = : parentIds AND ContentDocument.LatestPublishedVersion.Type__c != null
        ];

        for (ContentDocumentLink cdLink : cdLinks)
        {
            ViewModelsFile.Details details = new ViewModelsFile.Details();
            details.Id = cdLink.ContentDocumentId;
            details.VersionId = cdLink.ContentDocument.LatestPublishedVersionId;
            details.EntityId = cdLink.LinkedEntityId;
            details.Name = cdLink.ContentDocument.LatestPublishedVersion.Title;
            details.Type = cdLink.ContentDocument.LatestPublishedVersion.Type__c;
            details.BodyLength = cdLink.ContentDocument.LatestPublishedVersion.ContentSize;
            details.CreatedDate = cdLink.ContentDocument.LatestPublishedVersion.CreatedDate;
            details.ContentType = cdLink.ContentDocument.LatestPublishedVersion.FileType;
            items.add(details);
        }

        return items;
    }
}