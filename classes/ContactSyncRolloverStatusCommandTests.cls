@isTest
public class ContactSyncRolloverStatusCommandTests
{
    private static String cerebroID = 'A001641336';    
    
    @testSetup
    static void setup()
    {   
        TestUtilities.setupHEDA();
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        TestDataCampaignMock.createSeries(user, 4, true);
        TestDataCampaignMock.createRolloverCampaignMemberStatus(user);
        
        Contact student = TestDataContactMock.createRolloverStudent(user, cerebroID);
        student.Manual_Followup_Date__c = Date.today().addDays(1);
        update student;        
    }
    
    @isTest 
    static void ChangeRolloverStatusToContacting_SyncsContactingToCampaignMember() 
    {        
         // Arrange
        Contact student = 
            [
                SELECT Id,Current_Rollover_Campaign__c,Rollover_Status__c
                FROM Contact
                WHERE Cerebro_Student_ID__c = : cerebroID
            ];
        
        Contact newStudent = student.clone(true, false, true, true);
        
        newStudent.Rollover_Status__c = RolloverConstantsDefinition.ST_CONTACTING;
        newStudent.recalculateFormulas();
        
        List<Contact> items = new List<Contact>{ newStudent };
        Map<Id, Contact> oldMap = new Map<Id, Contact>();
        oldMap.put(student.Id, student);
        
        // Act
        ActionResult result = ContactService.syncRolloverStatusToCampaign(items, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Executing a command should return a successful result to the caller.');
        
        // Assert        
        CampaignMember campaignMember = 
            [
                SELECT Id, Status 
                FROM CampaignMember 
                WHERE ContactId = : student.Id
            ];
        system.assertEquals(RolloverConstantsDefinition.ST_CONTACTING, campaignMember.Status, 'Campaign Member Status is not synced');
    }
    
    @isTest 
    static void ChangeRolloverStatusToEnrolling_SyncsEnrollingToCampaignMember() 
    {        
         // Arrange
        Contact student = 
            [
                SELECT Id,Current_Rollover_Campaign__c,Rollover_Status__c
                FROM Contact
                WHERE Cerebro_Student_ID__c = : cerebroID
            ];
        
        Contact newStudent = student.clone(true, false, true, true);
        
        newStudent.Rollover_Status__c = RolloverConstantsDefinition.ST_ENROLLING;
        newStudent.recalculateFormulas();
        
        List<Contact> items = new List<Contact>{ newStudent };
        Map<Id, Contact> oldMap = new Map<Id, Contact>();
        oldMap.put(student.Id, student);
        
        // Act
        ActionResult result = ContactService.syncRolloverStatusToCampaign(items, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Executing a command should return a successful result to the caller.');
        
        // Assert        
        CampaignMember campaignMember = 
            [
                SELECT Id, Status 
                FROM CampaignMember 
                WHERE ContactId = : student.Id
            ];
        system.assertEquals(RolloverConstantsDefinition.ST_ENROLLING, campaignMember.Status, 'Campaign Member Status is not synced');
    }
    
    @isTest 
    static void ChangeRolloverStatusToOthers_NoSyncToCampaignMember() 
    {        
         // Arrange
        Contact student = 
            [
                SELECT Id,Current_Rollover_Campaign__c,Rollover_Status__c
                FROM Contact
                WHERE Cerebro_Student_ID__c = : cerebroID
            ];
        
        Contact newStudent = student.clone(true, false, true, true);
        
        newStudent.Rollover_Status__c = RolloverConstantsDefinition.RO_NOENROLMENTS;
        newStudent.recalculateFormulas();
        
        List<Contact> items = new List<Contact>{ newStudent };
        Map<Id, Contact> oldMap = new Map<Id, Contact>();
        oldMap.put(student.Id, student);
        
        // Act
        ActionResult result = ContactService.syncRolloverStatusToCampaign(items, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Executing a command should return a successful result to the caller.');
        
        // Assert        
        CampaignMember campaignMember = 
            [
                SELECT Id, Status 
                FROM CampaignMember 
                WHERE ContactId = : student.Id
            ];
        system.assertEquals(RolloverConstantsDefinition.ST_OPEN, campaignMember.Status, 'Campaign Member Status is not synced');
    }
    
    @isTest 
    static void ChangeRolloverStatusWithoutCampaign_NoSyncToCampaignMember() 
    {        
         // Arrange
        Contact student = 
            [
                SELECT Id,Current_Rollover_Campaign__c,Rollover_Status__c
                FROM Contact
                WHERE Cerebro_Student_ID__c = : cerebroID
            ];
        
        Contact newStudent = student.clone(true, false, true, true);
        
        newStudent.Rollover_Status__c = RolloverConstantsDefinition.ST_ENROLLING;
        newStudent.Current_Rollover_Campaign__c = null;
        newStudent.recalculateFormulas();
        
        List<Contact> items = new List<Contact>{ newStudent };
        Map<Id, Contact> oldMap = new Map<Id, Contact>();
        oldMap.put(student.Id, student);
        
        // Act
        ActionResult result = ContactService.syncRolloverStatusToCampaign(items, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Executing a command should return a successful result to the caller.');
        
        // Assert        
        CampaignMember campaignMember = 
            [
                SELECT Id, Status 
                FROM CampaignMember 
                WHERE ContactId = : student.Id
            ];
        system.assertEquals(RolloverConstantsDefinition.ST_OPEN, campaignMember.Status, 'Campaign Member Status is not synced');
    }
}