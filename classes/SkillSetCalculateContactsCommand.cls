public class SkillSetCalculateContactsCommand extends BaseTriggerCommand
{
    public override List<SObject> validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        List<hed__Course__c> coursesToRecalc = new List<hed__Course__c>();
        Set<Id> idSet = new Set<Id>();

        for (SkillSet__c skillSet : (List<SkillSet__c>) items)
        {
            if (triggerContext == Enums.TriggerContext.AFTER_INSERT || triggerContext == Enums.TriggerContext.AFTER_DELETE)
            {
                if (skillSet.SubjectDefinition__c != null)
                    idSet.add(skillSet.SubjectDefinition__c);
            }
            else if (triggerContext == Enums.TriggerContext.AFTER_UPDATE)
            {
                if (skillSet.SubjectDefinition__c != null)
                    idSet.add(skillSet.SubjectDefinition__c);

                SkillSet__c oldValue = (SkillSet__c)oldMap.get(skillSet.Id);
                if (oldValue.SubjectDefinition__c != null && oldValue.SubjectDefinition__c != skillSet.SubjectDefinition__c)
                    idSet.add(oldValue.SubjectDefinition__c);
            }
        }

        if (idSet.size() > 0)
            coursesToRecalc = [SELECT Id FROM hed__Course__c WHERE Id in : idSet];

        return coursesToRecalc;
    }

    public override ActionResult command(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        Set<Id> idSet = new Set<Id>();

        for (hed__Course__c subject : (List<hed__Course__c>) items)
            idSet.add(subject.Id);

        List<AggregateResult> childCount =
            [
                SELECT subjectDefinition__c, COUNT(Id)
                FROM SkillSet__C
                WHERE subjectDefinition__c IN : idSet
                GROUP BY subjectDefinition__c
            ];

        for (AggregateResult result : childCount)
        {
            Id parentId = (Id)result.get('subjectDefinition__c');
            hed__Course__c subject = new hed__Course__c(Id = parentId);
            subject.NumberOfSkilledContacts__c = Integer.valueOf(result.get('expr0'));
            idSet.remove(parentId);
            uow.registerDirty(subject);
        }

        for (Id key : idSet)
        {
            hed__Course__c subject = new hed__Course__c(Id = key);
            subject.NumberOfSkilledContacts__c = 0;
            uow.registerDirty(subject);
        }

        return new ActionResult(ResultStatus.SUCCESS);
    }
}