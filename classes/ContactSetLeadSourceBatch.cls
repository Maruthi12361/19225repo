public class ContactSetLeadSourceBatch extends BaseBatchCommand
{
    private Set<String> leadSourceValues = new Set<String> { 'Unknown' };

    public override Database.QueryLocator query(Database.BatchableContext context)
    {
        return Database.getQueryLocator(getQuery()); 
    }

    public override void command(Database.BatchableContext context, List<sObject> scope)
    {
        List<ContactReferrer__c> referrers = new List<ContactReferrer__c>();

        for (Contact contact : (List<Contact>) scope)
        {
            for (ContactReferrer__c referrer : contact.ContactReferrers__r)
            {
                referrers.add(referrer);
            }
        }

        if (referrers.size() > 0)
            ContactReferrerService.setLeadSourceValue(referrers, null, Enums.TriggerContext.AFTER_INSERT);
    }

    private string getQuery()
    {
        return 
            'SELECT ' + 
                'Id, LeadSource,  ' +
                '( ' +
                    'SELECT ' + 
                        'Id, LeadSource__c, CreatedDate ' + 
                    'FROM ContactReferrers__r ' + 
                    'WHERE LeadSource__c <> null ' +   
                    'ORDER BY CreatedDate ' + 
                    'LIMIT 1 ' +
                ') ' +  
            'FROM Contact ' + 
            'WHERE LeadSource = null OR LeadSource IN :leadSourceValues';
    }
}