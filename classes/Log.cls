global class Log
{
    static List<AppLog__c> appLogs = new List<AppLog__c>();

    public static void info(String className, String functionName, String message)
    {
        create(LoggingLevel.INFO, className, functionName, message);
    }

    public static void info(String className, String functionName, String message, String[] items)
    {
        create(LoggingLevel.INFO, className, functionName, String.format(message, items));
    }

    public static void info(String className, String functionName, String message, HttpRequest request, String[] headers)
    {
        String logMessage = '';
        String requestHeaders = '';

        for (String key: headers)
        {
            if (requestHeaders.length() > 0)
                requestHeaders = requestHeaders + ',';

            requestHeaders = requestHeaders + key + ': ' + request.getHeader(key);
        }

        logMessage += String.format('{0}\nRequest Endpoint: {1}\nRequest Method: {2}\Request Headers: {3}\nRequest Body: {4}',
            new String[]
            {
                message,
                request.getEndpoint(),
                request.getMethod(),
                requestHeaders,
                request.getBody()
            });

        create(LoggingLevel.INFO, className, functionName, logMessage);
    }

    public static void warn(String className, String functionName, String message)
    {
        create(LoggingLevel.WARN, className, functionName, message);
    }

    public static void warn(String className, String functionName, String message, String[] items)
    {
        create(LoggingLevel.WARN, className, functionName, String.format(message, items));
    }

    public static void error(String className, String functionName, String message)
    {
        create(LoggingLevel.ERROR, className, functionName, message);
    }

    public static void error(String className, String functionName, String message, String[] items)
    {
        create(LoggingLevel.ERROR, className, functionName, String.format(message, items));
    }

    public static void error(String className, String functionName, String message, Exception ex)
    {
        String logMessage = '';

        if (ex != null)
            logMessage += String.format('{0}\n Exception Message: {1} \n Type: {2} \n StackTrace: {3}', new String[] { message, ex.getMessage(), ex.getTypeName(), ex.getStackTraceString() });
        else
            logMessage = message;

        create(LoggingLevel.ERROR, className, functionName, logMessage);
    }

    public static ActionResult deleteLogs(List<AppLog__c> params)
    {
        return new LogDeleteCommand().execute(params);
    }

    public static ActionResult save()
    {
        ActionResult result = new LogCommand().execute(appLogs);
        if (result.Status == ResultStatus.SUCCESS)
            appLogs = new List<AppLog__c>();

        return result;
    }

    private static void validateParams(String className, String functionName)
    {
        if (String.isBlank(className))
            throw new Exceptions.InvalidParameterException(String.format(Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Class Name' }));

        if (String.isBlank(functionName))
            throw new Exceptions.InvalidParameterException(String.format(Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Function Name' }));
    }

    private static void create(LoggingLevel level, String className, String functionName, String message)
    {
        validateParams(className, functionName);
        String handler = String.format('{0}.{1}', new List<String>{className, functionName});

        AppLog__c appLog = new AppLog__c
        (
            Type__c = String.valueOf(level),
            Message__c = String.format('[{0}][{1}] {2}', new List<String>{ Datetime.now().format('dd/MM/yyyy hh:mm:ss:SSS'), handler, message }),
            Handler__c = handler
        );
        appLogs.add(appLog); 

        System.debug(level, appLog.Message__c);	
    }
}