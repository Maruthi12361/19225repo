public class ContactSaveInterestingMomentCommand extends BaseComponent
{
    List<SalesScoreConfig__c> salesScoreConfigs;

    private Boolean validate(Object parameter)
    {
        ViewModels.Param param = (ViewModels.Param) parameter;
        Log.info(this.className, 'validate', 'Identifier {0} \n Payload {1}', new List<String>{ param.Identifier, param.Payload });

        if (String.isEmpty(param.Identifier))
        {
            Log.error(this.className, 'validate',  Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Identifier' });
            throw new Exceptions.InvalidParameterException(String.format(Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Identifier' }));
        }
        else if (String.isEmpty(param.Payload))
        {
            Log.error(this.className, 'validate',  Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Payload' });
            throw new Exceptions.InvalidParameterException(String.format(Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Payload' }));
        }
        else if (getContact(param) == null)
        {
            Log.error(this.className, 'validate', 'Contact does not exists in database: ' + param.Identifier);
            throw new Exceptions.InvalidParameterException('Contact does not exist');
        }

        return true;
    }

    public ActionResult command(Object parameter)
    {
        validate(parameter);

        ViewModels.Param param = (ViewModels.Param) parameter;
        Contact contact = getContact(param);

        salesScoreConfigs = getSalesScoreConfigs();
        List<InterestingMoment__c> moments = createIntrestingMoments(param.Payload, contact.Id);

        try
        {
            if (moments.size() > 0)
                Database.insert(moments);
            else
                Log.info(this.className, 'command', 'There was no data for intresting moments to be created');
        }
        catch(DmlException ex)
        {
            String message = ex.getMessage();
            ActionResult result = new ActionResult(ResultStatus.ERROR, message);
            Log.error(this.className, 'command', message);
            return result;
        }

        return new ActionResult(ResultStatus.SUCCESS);
    }

    private Contact getContact(ViewModels.Param param)
    {
        try
        {
            return ContactService.get(param.Identifier, 'Id', false);
        }
        catch (Exception ex) 
        {
            Log.error(this.className, 'getContact', ex.getMessage());
            throw new Exceptions.ApplicationException(ex.getMessage());
        }
    }

    private void updateField(SObject obj, String fieldName, Object fieldValue)
    {
        Schema.DisplayType fieldType = SObjectUtils.getFieldType(obj, fieldName);

        if (SObjectUtils.isStringType(obj, fieldName) && fieldValue != null)
        {
            Integer length = SObjectUtils.getMaxStringLength(obj, fieldName);
            String stringValue = fieldValue.toString();
            stringValue = StringUtils.copyLeft(stringValue, length);
            fieldValue = (Object)stringValue;
        }

        if (fieldType == Schema.DisplayType.Date)
            obj.put(fieldName, Date.valueOf(fieldValue.toString()));
        else if (fieldType == Schema.DisplayType.Datetime)
        {
            String dateStringValue = fieldValue.toString();
            dateStringValue = dateStringValue.replace('T', ' ');
            dateStringValue = dateStringValue.replace('Z', ' ');

            obj.put(fieldName, Datetime.valueOfGmt(dateStringValue));
        }
        else
            obj.put(fieldName, fieldValue);
    }

    private List<InterestingMoment__c> createIntrestingMoments(String param, Id contactId)
    {
        List<InterestingMoment__c> result = new List<InterestingMoment__c>();
        List<Object> moments = (List<Object>)JSON.deserializeUntyped(param);

        for(Object obj : moments)
        {
            Boolean createMoment = false;
            InterestingMoment__c moment = new InterestingMoment__c();
            Map<String, Object> intrestingMomentMap = (Map<String, Object>)JSON.deserializeUntyped(JSON.serialize(obj));

            for (String fieldName : intrestingMomentMap.keySet())
            {
                Object fieldValue = intrestingMomentMap.get(fieldName);

                if (fieldValue == null || fieldValue == '')
                {
                    Log.info(this.className, 'createIntrestingMoments', 'No value to update for {0} field', new List<String>{ fieldValue.toString() });
                    continue;
                }

                updateField(moment, fieldName, fieldValue);
                createMoment = true;
            }

            if (createMoment)
            {
                moment.ContactID__c = contactId;
                moment.BehaviourScore__c = getBehaviourScoreValue(moment.Type__c, moment.Source__c);
                result.add(moment);
            }   
            else
            {
                Log.info(this.className, 'createIntrestingMoments', 'Interesting moment not created. None of the fields had value. {0}', 
                    new List<String>{ JSON.serialize(intrestingMomentMap) });
            }
        }

        return result;
    }

    private List<SalesScoreConfig__c> getSalesScoreConfigs()
    {
        return 
        [
            SELECT Id, Operator__c, ScoreType__c, ScoreValue__c, Source__c
            FROM  SalesScoreConfig__c
            WHERE Active__c = true
            ORDER BY DisplayOrder__c
        ];
    }

    private List<SalesScoreConfig__c> getInterestingMomentSalesScoreConfigs(String scoreType)
    {
        List<SalesScoreConfig__c> result = new List<SalesScoreConfig__c>();

        for (SalesScoreConfig__c config : salesScoreConfigs)
        {
            if (config.ScoreType__c == scoreType)
                result.add(config);
        }

        return result;
    }

    private Double getBehaviourScoreValue(String scoreType, String source)
    {
        List<SalesScoreConfig__c> interestingMomentConfigs = getInterestingMomentSalesScoreConfigs(scoreType);

        for (SalesScoreConfig__c config : interestingMomentConfigs)
        {
            if (StringUtils.isLogicalStatementTrue(config.Operator__c, source, config.Source__c))
                return config.ScoreValue__c;
        }

        return 0;
    }
}