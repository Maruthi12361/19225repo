public class CaseEnrolmentCreateCommand extends BaseViewModelCommand
{
    public override List<Interfaces.IViewModel> validate(List<Interfaces.IViewModel> viewModels)
    {
        return viewModels;
    }

    public override ActionResult command(List<Interfaces.IViewModel> viewModels)
    {
        List<CaseEnrolment__c> caseEnrolments = new List<CaseEnrolment__c>();
        Set<Id> caseIds = new Set<Id>();
        
        for (ViewModelsCaseEnrolment.Details detail : (List<ViewModelsCaseEnrolment.Details>) viewModels)
        {
            caseIds.add(detail.caseId);
        }
        
        List<CaseEnrolment__c> currentCaseEnrolments = [SELECT Id, Case__c, SubjectEnrolment__c FROM CaseEnrolment__c WHERE Case__c IN : caseIds];
        Set<String> keySet = new Set<String>();
        
        for (CaseEnrolment__c record : currentCaseEnrolments)
        {
            keySet.add(StringUtils.concat(record.Case__c, record.SubjectEnrolment__c, ''));
        }

        for (ViewModelsCaseEnrolment.Details detail : (List<ViewModelsCaseEnrolment.Details>) viewModels)
        {            
            if (keySet.contains(StringUtils.concat(detail.caseId, detail.subjectEnrolmentId, '')))
                continue;

            CaseEnrolment__c caseEnrolment = new CaseEnrolment__c();
            caseEnrolment.Case__c = detail.caseId;
            caseEnrolment.SubjectEnrolment__c = detail.subjectEnrolmentId;
            caseEnrolments.add(caseEnrolment);
        }

        if (caseEnrolments.size() > 0)
            insert caseEnrolments;

        return new ActionResult();
    }
}