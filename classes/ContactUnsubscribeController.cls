@RestResource(urlMapping='/contacts/unsubscribe/*')
global class ContactUnsubscribeController 
{
    private static final string unsubscribeEmailField = 'Cnt_HasOptedOutOfEmail';
    private static final string unsubscribePhoneField = 'Cnt_DoNotCall';

    @HttpPost
    global static void save()
    {
        String reference = '';
        RestRequest req = RestContext.request;
        ViewModels.Param reqParam = new ViewModels.Param();
        Map<String, Object> optionMap = new Map<String, Object>();

        if (req.headers.containsKey('Reference'))
            reference = req.headers.get('Reference');

        validate(reference, req.requestBody.toString());

        reqParam.Identifier = reference;
        reqParam.Payload =  req.requestBody.toString();

        Log.info('ContactUnsubscribeController', 'save', 'Identifier value {0} \n Payload: {1}', new List<String>{ reqParam.Identifier, reqParam.Payload });

        ActionResult result;

        for (Integer i = 0; i < 5; i++)
        {
            result = ContactService.unsubscribe(reqParam);

            if (result.Status == ResultStatus.SUCCESS || (!result.message.contains('UNABLE_TO_LOCK_ROW') && !result.message.contains('Please try again')))
                break;

            Log.info('ContactUnsubscribeController', 'save', 'Unable to lock row, remaining retry {0}', new List<String>{ String.valueOf(4 - i) });
        }

        if (result.Status == ResultStatus.SUCCESS)
        {
            RestContext.response.addHeader('Content-Type', 'application/json');
            RestContext.response.statusCode = 200;
        }
        else 
        {
            Log.error('ContactUnsubscribeController', 'save', result.message);
            throw new Exceptions.ApplicationException(result.message);
        }
    }

    private static Boolean validate(String reference, String payload)
    {
        Map<String, Object> payloadMap;

        Log.info('ContactUnsubscribeController', 'validatePayload', 'Validating following payload {0}', new List<String>{ payload });

        if (String.isEmpty(reference))
            throw new Exceptions.InvalidParameterException(String.format(Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Reference' }));

        if (String.isEmpty(payload))
            throw new Exceptions.InvalidParameterException(String.format(Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Payload object' }));

        try
        {
            payloadMap = (Map<String, Object>)JSON.deserializeUntyped(payload);
        }
        catch (Exception ex)
        {
            throw new Exceptions.InvalidParameterException(ex.getMessage());
        }

        if (payloadMap.size() == 0 || payloadMap.size() > 2)
            throw new Exceptions.InvalidParameterException('Unexpected number of fields. Expecting at least one field and maximum of two fields in payload');
        else if (payloadMap.size() == 1 && payloadMap.containsKey(unsubscribePhoneField) || payloadMap.containsKey(unsubscribeEmailField))
            return true;
        else if (payloadMap.containsKey(unsubscribePhoneField) && payloadMap.containsKey(unsubscribeEmailField))
            return true;

        throw new Exceptions.InvalidParameterException(String.format('Unexpected field in payload. Expected {0} and {1} fields', new List<String> { unsubscribeEmailField, unsubscribePhoneField }) );
    }
}