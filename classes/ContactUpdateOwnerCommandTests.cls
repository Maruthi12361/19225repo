@isTest
public class ContactUpdateOwnerCommandTests
{
    @testSetup
    public static void setup()
    {
        TestUtilities.setupHEDA();
    }

    @isTest
    static void CreateAutoProcContact_UpdateOwnerToOAFUser() 
    {
        // Arrange
        User user = UserService.get(Constants.USERID_AUTOPROC);

        // Act
        Test.startTest();
        Contact contact = TestDataContactMock.createProspectWarm(user);
        Test.stopTest();

        // Assert
        Contact updatedContact = [SELECT Id, OwnerId FROM Contact WHERE Id = :contact.Id];
        System.assertEquals(Constants.USERID_OAF, updatedContact.OwnerId, 'Unexpected OwnerId value where user was not changed from AutoProc to Course Advisor');
    }

    @isTest
    static void CreateNonAutoProcContact_DoNotUpdateOwnerToOAFUser() 
    {
        // Arrange
        User user = UserService.get(Constants.USERID_CAREERFAQ);

        // Act
        Test.startTest();
        Contact contact = TestDataContactMock.createProspectWarm(user);
        Test.stopTest();

        // Assert
        Contact updatedContact = [SELECT Id, OwnerId FROM Contact WHERE Id = :contact.Id];
        System.assertEquals(Constants.USERID_CAREERFAQ, updatedContact.OwnerId, 'Unexpected OwnerId value user shouldnt have been changed');
    }
}