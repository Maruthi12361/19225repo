@isTest
public class TestDataSalesScoreConfigMock 
{
    public static SalesScoreConfig__c create(User user) 
    {
        return create(user, 'Web Page Access', 'Google', 'Google');
    }

    public static SalesScoreConfig__c create(User user, String scoreType, String source, String value) 
    {
        return create(user, scoreType, null, source, '', value);
    }

    public static SalesScoreConfig__c create(User user, String scoreType, Double scoreValue, String source, String operator, String value) 
    {
        SalesScoreConfig__c config = new SalesScoreConfig__c
        (
            Name = 'Config - ' + source,
            ScoreType__c = String.isEmpty(scoreType) ? 'Web Page Access' : scoreType,
            ScoreValue__c = scoreValue == null ? 50 : scoreValue,
            Source__c = String.isEmpty(source) ? 'Google' : source,
            Operator__c = String.isEmpty(operator) ? String.valueOf(Enums.Operator.EQUAL) : operator,
            Value__c = value == null ? 'Google' : value,
            Active__c = true
        );
        
        System.RunAs(user)
        {
            insert config;
        }

        return config;
    }
}