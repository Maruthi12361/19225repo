public class IdentifyAllDuplicatesBatch extends BaseBatchCommand 
{
    private string objectName;

    public IdentifyAllDuplicatesBatch(String objectName)
    {
        this.objectName = objectName;
    }
    protected override Database.QueryLocator query(Database.BatchableContext context)
    {
        String query = 'SELECT Id FROM '+ this.objectName + ' WHERE IsDeleted = false';
        return Database.getQueryLocator(query);
    }
    
    public override void command(Database.BatchableContext BC, List<sObject> scope) 
    {          
        SObjectService.identifyAllDuplicates(scope);
    }
}