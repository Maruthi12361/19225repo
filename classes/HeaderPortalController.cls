public class HeaderPortalController 
{
    @AuraEnabled
    public static User getUserById(String userId)
    {
        String id = (userId == null) ? UserInfo.getUserId() : userId;
        User user = [SELECT Id, Name, IsProfilePhotoActive, SmallPhotoUrl, FirstName, MiddleName, LastName, MobilePhone, Phone, Email, Title, Department 
                     FROM User 
                     WHERE Id=:id];

        return user;
    }
    
    @AuraEnabled
    public static Contact getContactByUserId(String userId)
    {
        String id = (userId == null) ? UserInfo.getUserId() : userId;
        List<Contact> contact = [SELECT FirstName, MiddleName, LastName, Email, HomePhone, MobilePhone, hed__WorkPhone__c, 
                                    MailingStreet, MailingCity, MailingState, MailingPostalCode, MailingCountry,
                                    RolloverEligible__c, Rollover_Status__c, AtRisk__c, IsEnroledNextTerm__c,  IsEnroledFutureTerm__c 
                                FROM Contact 
                                WHERE Id in (SELECT ContactId FROM User WHERE Id=:id)];

        if (contact.size() > 0)
            return contact[0];
        else 
            return null;
    }
}