public class TestDataTermMock
{
    private static Map<String, hed__Term__c> termMap = new Map<String, hed__Term__c>();

    public static hed__Term__c create(User user, Id departmentId, Date subjectStartDate)
    {
        return create(user, new List<ViewModelsTestData.Term> { new ViewModelsTestData.Term(departmentId, subjectStartDate) })[0];
    }

    public static hed__Term__c createVirtual(User user, Id departmentId)
    {
        return create(user, new List<ViewModelsTestData.Term> { new ViewModelsTestData.Term(departmentId, 'Virtual Term') })[0];
    }

    public static List<hed__Term__c> create(User user, List<ViewModelsTestData.Term> terms)
    {
        List<hed__Term__c> result = new List<hed__Term__c>();
        List<hed__Term__c> inserts = new List<hed__Term__c>();

        for (ViewModelsTestData.Term term: terms)
        {
            String year = term.SubjectStartDate == null ? '0' : String.valueOf(term.SubjectStartDate.year());
            Integer termNumber = term.SubjectStartDate == null ? null : getTermNumber(term.SubjectStartDate.month());
            String name = term.Name != null ? term.Name : year + ' Term ' + String.valueOf(termNumber);
            String key = term.DepartmentId + (term.SubjectStartDate != null ? term.SubjectStartDate.format() : term.Name);

            if (termMap.containsKey(key))
            {
                result.add(termMap.get(key));
                continue;
            }

            if (termMap.size() == 0)
            {
                List<hed__Term__c> items =
                    [
                        SELECT Name, hed__Account__c, Year__c, hed__Grading_Period_Sequence__c, hed__Start_Date__c, hed__End_Date__c,
                            EnrolmentCutoffDate__c, FirstAdministrationDate__c, FirstCensusDate__c, FirstSubjectStartDate__c
                        FROM hed__Term__c
                    ];

                for (hed__Term__c item: items)
                    termMap.put(item.hed__Account__c + (item.FirstSubjectStartDate__c != null ? item.FirstSubjectStartDate__c.format() : item.Name), item);
            }

            if (termMap.containsKey(key))
            {
                result.add(termMap.get(key));
                continue;
            }

            hed__Term__c newItem = new hed__Term__c
            (
                Name = name,
                hed__Account__c = term.DepartmentId,
                Year__c = year,
                hed__Grading_Period_Sequence__c = termNumber,
                hed__Start_Date__c = term.SubjectStartDate,
                hed__End_Date__c = term.SubjectStartDate == null ? null : term.SubjectStartDate.addDays(7 * 8),
                FirstAdministrationDate__c = term.SubjectStartDate == null ? null : term.SubjectStartDate.addDays(-21),
                EnrolmentCutoffDate__c = term.SubjectStartDate == null ? null : term.SubjectStartDate.addDays(-25),
                FirstCensusDate__c = term.SubjectStartDate == null ? null : term.SubjectStartDate.addDays(11),
                FirstSubjectStartDate__c = term.SubjectStartDate
            );

            termMap.put(key, newItem);
            result.add(newItem);
            inserts.add(newItem);
        }

        if (inserts.size() > 0)
        {
            if (user != null)
            {
                System.RunAs(user)
                {
                    insert inserts;
                }
            }
            else
            {
                insert inserts;
            }
        }

        return result;
    }

    private static Integer getTermNumber(Integer month)
    {
        Integer termNumber = 6;

        if (month >= 1 && month < 3)
            termNumber = 1;
        else if (month >= 3 && month < 5)
            termNumber = 2;
        else if (month >= 5 && month < 7)
            termNumber = 3;
        else if (month >= 7 && month < 9)
            termNumber = 4;
        else if (month >= 9 && month < 11)
            termNumber = 5;

        return termNumber;
    }

    public static List<hed__Term__c> createSeries(User user, Date courseStartDate, Integer occurrence, Id accountId, boolean updateStatus)
    {
        List<ViewModelsTestData.Term> terms = new List<ViewModelsTestData.Term>();

        for (Integer i = 0; i < occurrence; i++)
            terms.add(new ViewModelsTestData.Term(accountId, courseStartDate.addDays(i * 56)));

        List<hed__Term__c> result = TestDataTermMock.create(user, terms);   
        if (updateStatus)
            TermService.updateStatus([SELECT Id, hed__Start_Date__c, Status__c, hed__Account__c, Name FROM hed__Term__c WHERE hed__Account__c != null AND hed__Start_Date__c != null ORDER BY hed__Start_Date__c]);

        return result;
    }

    public static List<hed__Term__c> createSeries(User user, Id departmentId, boolean updateStatus)
    {
        List<ViewModelsTestData.Term> terms = new List<ViewModelsTestData.Term>();

        for (Integer termNo = 0; termNo < 12; termNo++)
        {
            // Future Term
            terms.add(new ViewModelsTestData.Term(departmentId, Date.today().addDays((termNo * (7 * 8)) + 3)));

            // Historic Term
            terms.add(new ViewModelsTestData.Term(departmentId, Date.today().addDays((-(termNo + 1) * (7 * 8)) + 3)));

            // Virtual Term
            if (termNo < 6)
                terms.add(new ViewModelsTestData.Term(departmentId, 'Virtual Term ' + (termNo + 1)));
        }

        List<hed__Term__c> result = TestDataTermMock.create(user, terms);
        if (updateStatus)
            TermService.updateStatus([SELECT Id, hed__Start_Date__c, Status__c, hed__Account__c, Name FROm hed__Term__c WHERE hed__Account__c != null AND hed__Start_Date__c != null ORDER BY hed__Start_Date__c]);

        return result;
    }
}