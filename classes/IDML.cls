public interface IDML
{
    void dmlInsert(List<SObject> objList);
    void dmlUpdate(List<SObject> objList);
    void dmlDelete(List<SObject> objList);
}