public class CaseCalculateLastInteractionCommand extends BaseCommand
{
    public override List<SObject> validate(List<SObject> items)
    {
        List<Case> cases = new List<Case>();

        for (Case c: (List<Case>)items)
            if (c.Latest_Interaction_Date_Time__c != null)
                cases.add(c);

        return cases;
    }

    public override ActionResult command(List<SObject> items)
    {
        for (Case c: (List<Case>)items)
        {
            Decimal hours = DateTimeUtils.getTimeAsHours(DateTimeUtils.getBusinessElapsedTime(c.Latest_Interaction_Date_Time__c, c.ClosedDate));

            if (hours != c.Last_Interaction_Till_Yesterday__c)
            {
                c.Last_Interaction_Till_Yesterday__c = hours;

                if (!Trigger.isExecuting)
                    uow.registerDirty(c);
            }
        }

        return new ActionResult(ResultStatus.SUCCESS, new List<String>{'Successfully calculated/updated Case Last Interaction DateTime'});
    }
}