public class CaseEnrolmentCreateExemptionCommand extends BaseViewModelCommand
{
    public override List<Interfaces.IViewModel> validate(List<Interfaces.IViewModel> viewModels)
    {
        return viewModels;
    }

    public override ActionResult command(List<Interfaces.IViewModel> viewModels)
    {
        List<hed__Course_Enrollment__c> enrolments = new List<hed__Course_Enrollment__c>();
        Set<Id> caseIds = new Set<Id>();
        Set<Id> recordIds = new Set<Id>();
        SObjectType recordType;

        for (ViewModelsCaseEnrolment.ExemptionDetails detail : (List<ViewModelsCaseEnrolment.ExemptionDetails>) viewModels)
        {
            caseIds.add(detail.caseId);
            recordIds.add(detail.recordId);

            if (recordType == null)
            {
                recordType = detail.recordId.getSobjectType();
            }
        }

        Map<Id, Case> caseMap = new Map<Id, Case>([SELECT Id, ContactId FROM Case WHERE Id IN : caseIds]);
        Set<Id> contactIds = new Set<Id>();

        for (Case item : caseMap.values())
        {
            contactIds.add(item.ContactId);
        }

        Map<String, hed__Course_Enrollment__c> subjectEnrolmentMap = new Map<String, hed__Course_Enrollment__c>();
        Set<String> subjectCodeSet = new Set<String>();
        Map<Id, hed__Course__c> subjectMap;
        Map<String, Id> courseEnrolmentMap = new Map<String, Id>();
        Map<Id, Id> planMap = new Map<Id, Id>();

        if (recordType == hed__Plan_Requirement__c.getSobjectType())
        {
            subjectMap = new Map<Id, hed__Course__c>();
            Map<Id, hed__Plan_Requirement__c> requirementMap = new Map<Id, hed__Plan_Requirement__c>(
                [
                    SELECT hed__Plan_Requirement__r.hed__Program_Plan__r.Name, hed__Course__r.Name, hed__Course__r.hed__Course_ID__c, hed__Course__r.hed__Credit_Hours__c, hed__Course__r.AQFLevel__c
                    FROM hed__Plan_Requirement__c
                    WHERE Id IN : recordIds
                ]);

            for (hed__Plan_Requirement__c item : requirementMap.values())
            {
                planMap.put(item.Id, item.hed__Plan_Requirement__r.hed__Program_Plan__c);
                subjectMap.put(item.Id, item.hed__Course__r);
                subjectCodeSet.add(item.hed__Course__r.hed__Course_ID__c);
            }

            List<hed__Program_Enrollment__c> courseEnrolments =
                [
                    SELECT Id, hed__Contact__c, hed__Program_Plan__c
                    FROM hed__Program_Enrollment__c
                    WHERE hed__Contact__c IN : contactIds AND Status__c IN ('Approved', 'Ongoing') AND hed__Program_Plan__c IN : planMap.values()
                ];

            for (hed__Program_Enrollment__c item : courseEnrolments)
            {
                courseEnrolmentMap.put(StringUtils.concat(item.hed__Contact__c, item.hed__Program_Plan__c, ''), item.Id);
            }
        }
        else
        {
            subjectMap = new Map<Id, hed__Course__c>(
                [
                    SELECT Name, hed__Course_ID__c, hed__Credit_Hours__c, AQFLevel__c
                    FROM hed__Course__c
                    WHERE Id IN : recordIds
                ]);

            for (hed__Course__c item : subjectMap.values())
            {
                subjectCodeSet.add(item.hed__Course_ID__c);
            }
        }

        // Query any existing Subject Enrolments for the student, whose Subjects will be excluded from Exemption. We should ignore the provisional and withdrawn Subject Enrolments.
        // However, if there exists a provisional Exemption enrolment, we should link it to Exemption Case instead of creating another provisional Exemption enrolment for the same Subject.
        List<hed__Course_Enrollment__c> currentSubjectEnrolments =
            [
                SELECT hed__Contact__c, SubjectCode__c, Type__c, RecordType.DeveloperName
                FROM hed__Course_Enrollment__c
                WHERE hed__Contact__c IN : contactIds AND SubjectCode__c IN : subjectCodeSet AND
                    ((RecordType.DeveloperName = : Constants.RECORDTYPE_COURSEENROLLMENT_STUDENT AND
                      hed__Status__c != : Constants.SUBJECTENROLMENT_STATUS_WITHDRAWN) OR
                     (Type__c = 'Exemption' AND RecordType.DeveloperName = : Constants.RECORDTYPE_COURSEENROLLMENT_PROVISIONAL))
            ];
        Map<String, hed__Course_Enrollment__c> enrolmentMap = new Map<String, hed__Course_Enrollment__c>();

        for (hed__Course_Enrollment__c record : currentSubjectEnrolments)
        {
            enrolmentMap.put(StringUtils.concat(record.hed__Contact__c, record.SubjectCode__c, ''), record);
        }

        for (ViewModelsCaseEnrolment.ExemptionDetails detail : (List<ViewModelsCaseEnrolment.ExemptionDetails>) viewModels)
        {
            Id contactId = caseMap.get(detail.caseId).ContactId;
            hed__Course__c subject = subjectMap.get(detail.recordId);
            hed__Course_Enrollment__c enrolment = enrolmentMap.get(StringUtils.concat(contactId, subject.hed__Course_ID__c, ''));

            if (enrolment != null)
            {
                if (enrolment.Type__c == 'Exemption' && enrolment.RecordType.DeveloperName == Constants.RECORDTYPE_COURSEENROLLMENT_PROVISIONAL)
                {
                    subjectEnrolmentMap.put(StringUtils.concat(detail.caseId, detail.recordId, ''), enrolment);
                }

                continue;
            }

            enrolment = new hed__Course_Enrollment__c();
            enrolment.RecordTypeId = RecordTypeUtils.getRecordTypeId(Constants.RECORDTYPE_COURSEENROLLMENT_PROVISIONAL, SObjectType.hed__Course_Enrollment__c);
            enrolment.hed__Contact__c = caseMap.get(detail.caseId).ContactId;
            enrolment.ExemptionSubjectCode__c = subject.hed__Course_ID__c ;
            enrolment.ExemptionSubjectName__c = subject.Name;
            enrolment.Type__c = 'Exemption';
            enrolment.hed__Status__c = Constants.SUBJECTENROLMENT_STATUS_INTERESTED;

            if (recordType == hed__Plan_Requirement__c.getSobjectType())
            {
                enrolment.hed__Program_Enrollment__c = courseEnrolmentMap.get(StringUtils.concat(contactId, planMap.get(detail.recordId), ''));
            }

            enrolments.add(enrolment);
            subjectEnrolmentMap.put(StringUtils.concat(detail.caseId, detail.recordId, ''), enrolment);
        }

        ActionResult result = new ActionResult();

        if (enrolments.size() > 0)
        {
            insert enrolments;
        }
        
        if (subjectEnrolmentMap.size() > 0)
        {
            List<ViewModelsCaseEnrolment.Details> items = new List<ViewModelsCaseEnrolment.Details>();
            for (ViewModelsCaseEnrolment.ExemptionDetails detail : (List<ViewModelsCaseEnrolment.ExemptionDetails>) viewModels)
            {
                hed__Course_Enrollment__c enrolment = subjectEnrolmentMap.get(StringUtils.concat(detail.caseId, detail.recordId, ''));

                if (enrolment == null)
                    continue;

                ViewModelsCaseEnrolment.Details viewModel = new ViewModelsCaseEnrolment.Details();
                viewModel.caseId = detail.caseId;
                viewModel.subjectEnrolmentId = enrolment.Id;
                items.add(viewModel);
            }

            result = CaseEnrolmentService.create(items);
        }

        return result;
    }
}