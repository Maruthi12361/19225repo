public class FacultyController
{
    @AuraEnabled(cacheable=true)
    public static Object getSubjectOfferingFaculties(String offeringId)
    {
        if (String.isEmpty(offeringId))
            throw new Exceptions.InvalidParameterException('Parameter value cannot be blank');

        ViewModels.Param param = new ViewModels.Param();
        param.Identifier = offeringId;

        return JSON.serialize(ContactService.getSubjectOfferingFaculties(param));
    }
}