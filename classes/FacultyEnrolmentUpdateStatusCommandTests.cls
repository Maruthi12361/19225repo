@isTest
public class FacultyEnrolmentUpdateStatusCommandTests
{
    @testSetup
    static void setup()
    {
        TestUtilities.setupHEDA();
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Account department = TestDataAccountMock.createDepartmentBusiness(user);
        hed__Term__c term = TestDataTermMock.create(user, department.Id, Date.today());
        hed__Course__c subjectDefinition = TestDataSubjectMock.createDefinitionCGOV(user);
        hed__Course__c subjectVersion = TestDataSubjectMock.createVersionCGOV(user);

        TestDataContactMock.createFacultyContact(user);
        TestDataSubjectOfferingMock.createOffering(user, subjectVersion, term);
    }

    @isTest
    static void BlankIdentifier_EmptyParameterError()
    {
        // Arrange
        String errorMessage = '';
        ViewModels.Param param = new ViewModels.Param();
        param.Identifier = '';
        param.Payload = null;

        // Act
        try
        {
           ActionResult result = new FacultyEnrolmentUpdateStatusCommand().command(param);
        }
        catch (Exception ex)
        {
           errorMessage = ex.getMessage();
        }

        // Assert
        System.assert(errorMessage.contains('Identifier parameter cannot be empty or null'), 'Unexpected error message');
    }

    @isTest
    static void BlankPayload_EmptyPayloadError()
    {
        // Arrange
        String errorMessage = '';
        ViewModels.Param param = new ViewModels.Param();
        param.Identifier = '5xCEWFYecBoRlNqvjtvUN3F%2F9c%2FjrXrJdpIzXJGvLJM%3D';
        param.Payload = null;

        // Act
        try
        {
           ActionResult result = new FacultyEnrolmentUpdateStatusCommand().command(param);
        }
        catch (Exception ex)
        {
           errorMessage = ex.getMessage();
        }

        // Assert
        System.assert(errorMessage.contains('Subject Enrolment payload cannot be empty or null'), 'Unexpected error message');
    }

    @isTest
    static void MissingIdField_MissingIdError()
    {
        // Arrange
        String errorMessage = '';
        Map<String, Object> prospectMap = new Map<String, Object>
        {
            'IsAvailable' => 'true'
        };

        ViewModels.Param param = new ViewModels.Param();
        param.Identifier = '5xCEWFYecBoRlNqvjtvUN3F%2F9c%2FjrXrJdpIzXJGvLJM%3D';
        param.Payload = JSON.serialize(prospectMap);

        // Act
        try
        {
           ActionResult result = new FacultyEnrolmentUpdateStatusCommand().command(param);
        }
        catch (Exception ex)
        {
           errorMessage = ex.getMessage();
        }

        // Assert
        System.assert(errorMessage.contains('Missing Id field'), 'Unexpected error message');
    }

    @isTest
    static void MissingIsAvailableField_MissingEncryptedIdError()
    {
        // Arrange
        String errorMessage = '';
        Map<String, Object> prospectMap = new Map<String, Object>
        {
            'Id' => 'a4P2O00000022v8UAA'
        };

        ViewModels.Param param = new ViewModels.Param();
        param.Identifier = '5xCEWFYecBoRlNqvjtvUN3F%2F9c%2FjrXrJdpIzXJGvLJM%3D'; 
        param.Payload = JSON.serialize(prospectMap);

        // Act
        try
        {
           ActionResult result = new FacultyEnrolmentUpdateStatusCommand().command(param);
        }
        catch (Exception ex)
        {
           errorMessage = ex.getMessage();
        }

        // Assert
        System.assert(errorMessage.contains('Missing IsAvailable field'), 'Unexpected error message');
    }

    @isTest
    static void ProspectEncryptedIdProvided_NoFacultyContactError()
    {
        // Arrange
        String errorMessage = '';
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact prospect = TestDataContactMock.createProspectCold(user, '0455548889');
        Contact createdProspect = [SELECT Id, EncryptedId__c FROM Contact WHERE Id = :prospect.Id];

        Map<String, Object> prospectMap = new Map<String, Object>
        {
            'Id' => 'a4P2O00000022v8UAA',
            'IsAvailable' => 'true'
        };

        ViewModels.Param param = new ViewModels.Param();
        param.Identifier = createdProspect.EncryptedId__c;
        param.Payload = JSON.serialize(prospectMap);

        // Act
        try
        {
           ActionResult result = new FacultyEnrolmentUpdateStatusCommand().command(param);
        }
        catch (Exception ex)
        {
           errorMessage = ex.getMessage();
        }
        System.debug(errorMessage);
        // Assert
        System.assert(errorMessage.contains('No faculty contact found'), 'Unexpected error message');
    }

    @isTest
    static void FacultyContactHasNoEnrolments_NoEnrolmentError()
    {
        // Arrange
        String errorMessage = '';
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact facultyContact = [SELECT Id, EncryptedId__c FROM Contact LIMIT 1];

        Map<String, Object> prospectMap = new Map<String, Object>
        {
            'Id' => 'a4P2O00000022v8UAA',
            'IsAvailable' => 'true'
        };

        ViewModels.Param param = new ViewModels.Param();
        param.Identifier = facultyContact.EncryptedId__c;
        param.Payload = JSON.serialize(prospectMap);

        // Act
        try
        {
           ActionResult result = new FacultyEnrolmentUpdateStatusCommand().command(param);
        }
        catch (Exception ex)
        {
           errorMessage = ex.getMessage();
        }

        // Assert
        System.assert(errorMessage.contains('Subject enrolment not found'), 'Unexpected error message');
    }

    @isTest
    static void SubjectEnrolmentNotInRequestedStatus_StatusNotUpdated()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact facultyContact = [SELECT Id, EncryptedId__c FROM Contact LIMIT 1];
        hed__Course_Offering__c subjectOffering = [SELECT Id FROM hed__Course_Offering__c LIMIT 1];
        hed__Course_Enrollment__c enrolment = TestDataSubjectEnrolmentMock.createFacultyEnrolment(user, subjectOffering, facultyContact, 'Enrolled', 'Subject Coordinator');

        Map<String, Object> prospectMap = new Map<String, Object>
        {
            'Id' => enrolment.Id,
            'IsAvailable' => 'true'
        };

        ViewModels.Param param = new ViewModels.Param();
        param.Identifier = facultyContact.EncryptedId__c;
        param.Payload = JSON.serialize(prospectMap);

        // Act
        Test.startTest();
        ActionResult result = new FacultyEnrolmentUpdateStatusCommand().command(param);
        hed__Course_Enrollment__c updatedEnrolment = [SELECT Id, hed__Status__c FROM hed__Course_Enrollment__c WHERE Id = :enrolment.Id];
        Test.stopTest();

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Unexpected result status value');
        System.assertEquals('Enrolled', updatedEnrolment.hed__Status__c, 'Unexpected enrolment status value');
    }

    @isTest
    static void SubjectEnrolmentInRequestStatusOLFAvailable_StatusUpdatedSuccessfully()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact facultyContact = [SELECT Id, EncryptedId__c FROM Contact LIMIT 1];
        hed__Course_Offering__c subjectOffering = [SELECT Id FROM hed__Course_Offering__c LIMIT 1];
        hed__Course_Enrollment__c enrolment = TestDataSubjectEnrolmentMock.createFacultyEnrolment(user, subjectOffering, facultyContact, 'Requested', 'Online Facilitator');

        Map<String, Object> prospectMap = new Map<String, Object>
        {
            'Id' => enrolment.Id,
            'IsAvailable' => 'true'
        };

        ViewModels.Param param = new ViewModels.Param();
        param.Identifier = facultyContact.EncryptedId__c;
        param.Payload = JSON.serialize(prospectMap);

        // Act
        Test.startTest();
        ActionResult result = new FacultyEnrolmentUpdateStatusCommand().command(param);
        hed__Course_Enrollment__c updatedEnrolment = [SELECT Id, hed__Status__c FROM hed__Course_Enrollment__c WHERE Id = :enrolment.Id];
        Test.stopTest();

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Unexpected result status value');
        System.assertEquals('Enrolled', updatedEnrolment.hed__Status__c, 'Unexpected enrolment status value');
    }

    @isTest
    static void SubjectEnrolmentInRequestStatusOLFNotAvailable_StatusUpdatedSuccessfully()
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact facultyContact = [SELECT Id, EncryptedId__c FROM Contact LIMIT 1];
        hed__Course_Offering__c subjectOffering = [SELECT Id FROM hed__Course_Offering__c LIMIT 1];
        hed__Course_Enrollment__c enrolment = TestDataSubjectEnrolmentMock.createFacultyEnrolment(user, subjectOffering, facultyContact, 'Requested', 'Online Facilitator');

        Map<String, Object> prospectMap = new Map<String, Object>
        {
            'Id' => enrolment.Id,
            'IsAvailable' => 'false'
        };

        ViewModels.Param param = new ViewModels.Param();
        param.Identifier = facultyContact.EncryptedId__c;
        param.Payload = JSON.serialize(prospectMap);

        // Act
        Test.startTest();
        ActionResult result = new FacultyEnrolmentUpdateStatusCommand().command(param);
        hed__Course_Enrollment__c updatedEnrolment = [SELECT Id, hed__Status__c FROM hed__Course_Enrollment__c WHERE Id = :enrolment.Id];
        Test.stopTest();

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Unexpected result status value');
        System.assertEquals('Rejected', updatedEnrolment.hed__Status__c, 'Unexpected enrolment status value');
    }
}