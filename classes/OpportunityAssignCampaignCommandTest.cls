@IsTest
public class OpportunityAssignCampaignCommandTest
{
    @testSetup 
    static void setup()
    {
        TestUtilities.setupHEDA();
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
    }

    @isTest
    static void HotDomesticProspectCreatedWithMatchingCampaign_OpportunityWithCampaignSuccessfullyCreated()
    {
        // Arrange
        TestUtilities.disableComponent('CerebroSyncApplicationCommand');
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Campaign campaign = TestDataCampaignMock.create(user, TestDataContactMock.getNextPreferredStartDate(), 'Domestic', 'MBA', false, false);

        // Act
        Test.startTest();
        Contact contact = TestDataContactMock.createProspectHot(user);
        Contact updatedContact = [SELECT Id, Status__c FROM Contact WHERE Id = :contact.Id];
        Test.stopTest();

        Opportunity createdOpportunity = [SELECT Id, Contact__c, CampaignId, Campaign.Programme__c, Campaign.Portfolio__c, CloseDate FROM Opportunity WHERE Contact__c = :updatedContact.Id];

        // Assert
        System.assertNotEquals(null, createdOpportunity.CampaignId, 'Opportunity expected to have campaign set');
        System.assertEquals('MBA', createdOpportunity.Campaign.Programme__c);
        System.assertEquals('Domestic', createdOpportunity.Campaign.Portfolio__c);
    }

    
    @isTest
    static void HotInternationalProspectCreatedWithMatchingCampaign_OpportunityWithCampaignSuccessfullyCreated()
    {
        // Arrange
        TestUtilities.disableComponent('CerebroSyncApplicationCommand');
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Campaign campaign = TestDataCampaignMock.create(user, TestDataContactMock.getNextPreferredStartDate(), 'International', 'GCM', false, false);

        // Act
        Test.startTest();
        
        Contact contact = TestDataContactMock.createProspectHot(user);
        contact.Portfolio__c = 'International (Other)';
        contact.Program__c = 'GCM';
        update contact;
        Contact updatedContact = [SELECT Id, Status__c FROM Contact WHERE Id = :contact.Id];
        Test.stopTest();

        Opportunity createdOpportunity = [SELECT Id, Contact__c, CampaignId, Campaign.Programme__c, Campaign.Portfolio__c, CloseDate FROM Opportunity WHERE Contact__c = :updatedContact.Id];

        // Assert
        System.assertNotEquals(null, createdOpportunity.CampaignId, 'Opportunity expected to have campaign set');
        System.assertEquals('GCM', createdOpportunity.Campaign.Programme__c);
        System.assertEquals('International', createdOpportunity.Campaign.Portfolio__c);
    }

    @isTest
    static void NewHotProspectCreatedWithNoMatchinCampaign_OpportunityWithNoCampaignSuccessfullyCreated()
    {
        // Arrange
        TestUtilities.disableComponent('CerebroSyncApplicationCommand');
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Campaign campaign = TestDataCampaignMock.create(user, Date.valueOf('2019-09-25'), 'Domestic', 'MBA', false, false);

        // Act
        Test.startTest();
        Contact contact = TestDataContactMock.createProspectHot(user);
        Contact updatedContact = [SELECT Id, Status__c FROM Contact WHERE Id = :contact.Id];
        Test.stopTest();

        Opportunity createdOpportunity = [SELECT Id, Contact__c, CampaignId, CloseDate FROM Opportunity WHERE Contact__c = :updatedContact.Id];

        // Assert
        System.assertEquals(null, createdOpportunity.CampaignId, 'Opportunity not expected to have campaign set');
    }
}