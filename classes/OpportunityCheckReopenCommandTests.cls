@isTest
public class OpportunityCheckReopenCommandTests
{
    @testSetup
    static void setup()
    {
        TestUtilities.setupHEDA();
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createProspectHot(user);
        TestDataOpportunityMock.create(user, contact, 'Applying', true);
        TestDataOpportunityMock.create(user, contact, 'Closed Lost', true);
        TestDataOpportunityMock.create(user, contact, 'Closed Lost', true);
    }

    @isTest
    static void ReopenClosedOppForProspectWithOpenOpp_OppFailsToReopenWithError()
    {
        // Arrange
        Exception ex;
        List<Opportunity> opps =
            [
                SELECT Id, RecordTypeId, IsClosed, Lost_Reason__c, StageName, Contact__c
                FROM Opportunity
                WHERE IsClosed = true
            ];

        // Act
        try
        {
            opps[0].StageName = 'Applying';
            update opps[0];
        }
        catch(DmlException e)
        {
            ex = e;
        }

        // Assert
        System.assertNotEquals(null, ex);
        System.assertEquals(true, ex.getMessage().contains('You cannot reopen this opportunity as there is other open acquisition opportunity associated with the same Contact.'));
    }

    @isTest
    static void ReopenClosedOppForProspectWithNoOpenOpps_OppIsReopened()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        List<Opportunity> opps =
            [
                SELECT Id, RecordTypeId, IsClosed, Lost_Reason__c, StageName, Contact__c
                FROM Opportunity
                WHERE IsClosed = false
            ];
        opps[0].StageName = 'Closed Lost';
        opps[0].Lost_Reason__c = Constants.OPPORTUNITY_CLOSEDLOST_DUPLICATE;
        update opps[0];
        opps =
            [
                SELECT Id, RecordTypeId, IsClosed, Lost_Reason__c, StageName, Contact__c
                FROM Opportunity
                WHERE IsClosed = true
            ];

        // Act
        opps[0].StageName = 'Applying';
        update opps[0];

        // Assert
        Opportunity opp =
            [
                SELECT Id, RecordTypeId, IsClosed, Lost_Reason__c, StageName, Contact__c
                FROM Opportunity
                WHERE Id = : opps[0].Id
            ];
        System.assertEquals(false, opp.IsClosed);
    }
}