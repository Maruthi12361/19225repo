public class StatusTrackingHistoryService
{
    public static ActionResult calculateStageAgeInBusinessMinutes(List<SObject> items)
    {
        return new StatusTrackingHistoryCalcAgeCommand().execute(items);
    }
}