@isTest
public class ActivityCreateFromSMSCommandTests
{
    @testSetup 
    static void setup()
    {
        TestUtilities.setupHEDA();
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        TestDataContactMock.createProspectCold(user, '+614444455555');
    }
    
    @isTest
    static void NewInboundSMS_CreatesActivity() 
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        SObject sms = TestDataSMSMock.create(user, null, null, '+614444455555', 'Test SMS', true);
        List<SObject> items = new List<SObject>{ sms };
        Map<Id, SObject> oldMap = new Map<Id, SObject>();
                
        // Act
        ActionResult result = ActivityService.create(items, oldMap, Enums.TriggerContext.AFTER_INSERT);
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Executing a command should return a successful result to the caller.');
        
        // Assert
        Task smsTask =
            [
                SELECT Id, WhoId, WhatId, OwnerId, Subject, Type, ActivityDate
                FROM Task
                WHERE RecordType.DeveloperName = 'Client_Contact' AND WhatId = : sms.Id
            ];
        
        System.assertNotEquals(null, smsTask);
        System.assertEquals('SMS', smsTask.Type);
        System.assertEquals(null, smsTask.WhoId);
        System.assertEquals(((smagicinteract__Incoming_SMS__c)sms).OwnerId, smsTask.OwnerId);
        System.assertEquals(((smagicinteract__Incoming_SMS__c)sms).CreatedDate.date(), smsTask.ActivityDate);
        System.assertEquals(true, smsTask.Subject.contains('+614444455555'));
    }
    
    @isTest
    static void NewOutboundSMS_CreatesActivity() 
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        SObject sms = TestDataSMSMock.create(user, null, null, '+614444455555', 'Test SMS', false);
        List<SObject> items = new List<SObject>{ sms };
        Map<Id, SObject> oldMap = new Map<Id, SObject>();
                
        // Act
        ActionResult result = ActivityService.create(items, oldMap, Enums.TriggerContext.AFTER_INSERT);
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Executing a command should return a successful result to the caller.');
        
        // Assert
        Task smsTask =
            [
                SELECT Id, WhoId, WhatId, OwnerId, Subject, Type, ActivityDate
                FROM Task
                WHERE RecordType.DeveloperName = 'Client_Contact' AND WhatId = : sms.Id
            ];
        
        System.assertNotEquals(null, smsTask);
        System.assertEquals('SMS', smsTask.Type);
        System.assertEquals(null, smsTask.WhoId);
        System.assertEquals(((smagicinteract__smsMagic__c)sms).OwnerId, smsTask.OwnerId);
        System.assertEquals(((smagicinteract__smsMagic__c)sms).CreatedDate.date(), smsTask.ActivityDate);
        System.assertEquals(true, smsTask.Subject.contains('+614444455555'));
    }
    
    @isTest
    static void NewInboundSMSOnContact_CreatesActivityRelatedToContact() 
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = 
            [
                SELECT Id,MobilePhone
                FROM Contact
                WHERE MobilePhone = '+614444455555'
            ];
        
        SObject sms = TestDataSMSMock.create(user, contact.Id, null, contact.MobilePhone, 'Test SMS', true);
        List<SObject> items = new List<SObject>{ sms };
        Map<Id, SObject> oldMap = new Map<Id, SObject>();
                
        // Act
        ActionResult result = ActivityService.create(items, oldMap, Enums.TriggerContext.AFTER_INSERT);
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Executing a command should return a successful result to the caller.');
        
        // Assert
        Task smsTask =
            [
                SELECT Id, WhoId, WhatId, OwnerId, Subject, Type, ActivityDate
                FROM Task
                WHERE RecordType.DeveloperName = 'Client_Contact' AND WhatId = : sms.Id AND WhoId = : contact.Id
            ];
        
        System.assertNotEquals(null, smsTask);
        System.assertEquals('SMS', smsTask.Type);
        System.assertEquals(((smagicinteract__Incoming_SMS__c)sms).OwnerId, smsTask.OwnerId);
        System.assertEquals(((smagicinteract__Incoming_SMS__c)sms).CreatedDate.date(), smsTask.ActivityDate);
        System.assertEquals(true, smsTask.Subject.contains(contact.MobilePhone));
    }
    
    @isTest
    static void NewOutboundSMSOnContact_CreatesActivityRelatedToContact() 
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = 
            [
                SELECT Id,MobilePhone
                FROM Contact
                WHERE MobilePhone = '+614444455555'
            ];
        
        SObject sms = TestDataSMSMock.create(user, contact.Id, null, contact.MobilePhone, 'Test SMS', false);
        List<SObject> items = new List<SObject>{ sms };
        Map<Id, SObject> oldMap = new Map<Id, SObject>();
                
        // Act
        ActionResult result = ActivityService.create(items, oldMap, Enums.TriggerContext.AFTER_INSERT);
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Executing a command should return a successful result to the caller.');
        
        // Assert
        Task smsTask =
            [
                SELECT Id, WhoId, WhatId, OwnerId, Subject, Type, ActivityDate
                FROM Task
                WHERE RecordType.DeveloperName = 'Client_Contact' AND WhatId = : sms.Id AND WhoId = : contact.Id
            ];
        
        System.assertNotEquals(null, smsTask);
        System.assertEquals('SMS', smsTask.Type);
        System.assertEquals(((smagicinteract__smsMagic__c)sms).OwnerId, smsTask.OwnerId);
        System.assertEquals(((smagicinteract__smsMagic__c)sms).CreatedDate.date(), smsTask.ActivityDate);
        System.assertEquals(true, smsTask.Subject.contains(contact.MobilePhone));
    }
    
    @isTest
    static void UpdatedInboundSMSOnContactWithoutActivity_CreatesActivityRelatedToContact() 
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = 
            [
                SELECT Id,MobilePhone
                FROM Contact
                WHERE MobilePhone = '+614444455555'
            ];
        
        SObject sms = TestDataSMSMock.create(user, null, null, contact.MobilePhone, 'Test SMS', true);
        smagicinteract__Incoming_SMS__c newSms = (smagicinteract__Incoming_SMS__c)sms.clone(true, false, true, true);
        newSms.smagicinteract__Contact__c = contact.Id;
        List<SObject> items = new List<SObject>{ newSms };
        Map<Id, SObject> oldMap = new Map<Id, SObject>();
        oldMap.put(sms.Id, sms);
                
        // Act
        ActionResult result = ActivityService.create(items, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Executing a command should return a successful result to the caller.');
        
        // Assert
        Task smsTask =
            [
                SELECT Id, WhoId, WhatId, OwnerId, Subject, Type, ActivityDate
                FROM Task
                WHERE RecordType.DeveloperName = 'Client_Contact' AND WhatId = : sms.Id AND WhoId = : contact.Id
            ];
        
        System.assertNotEquals(null, smsTask);
        System.assertEquals('SMS', smsTask.Type);
        System.assertEquals(((smagicinteract__Incoming_SMS__c)sms).OwnerId, smsTask.OwnerId);
        System.assertEquals(((smagicinteract__Incoming_SMS__c)sms).CreatedDate.date(), smsTask.ActivityDate);
        System.assertEquals(true, smsTask.Subject.contains(contact.MobilePhone));
    }
    
    @isTest
    static void UpdatedOutboundSMSOnContactWithoutActivity_CreatesActivityRelatedToContact() 
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = 
            [
                SELECT Id,MobilePhone
                FROM Contact
                WHERE MobilePhone = '+614444455555'
            ];
        
        SObject sms = TestDataSMSMock.create(user, contact.Id, null, null, 'Test SMS', false);
        smagicinteract__smsMagic__c newSms = (smagicinteract__smsMagic__c)sms.clone(true, false, true, true);
        newSms.smagicinteract__PhoneNumber__c = contact.MobilePhone;
        List<SObject> items = new List<SObject>{ newSms };
        Map<Id, SObject> oldMap = new Map<Id, SObject>();
        oldMap.put(sms.Id, sms);
                
        // Act
        ActionResult result = ActivityService.create(items, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Executing a command should return a successful result to the caller.');
        
        // Assert
        Task smsTask =
            [
                SELECT Id, WhoId, WhatId, OwnerId, Subject, Type, ActivityDate
                FROM Task
                WHERE RecordType.DeveloperName = 'Client_Contact' AND WhatId = : sms.Id AND WhoId = : contact.Id
            ];
        
        System.assertNotEquals(null, smsTask);
        System.assertEquals('SMS', smsTask.Type);
        System.assertEquals(((smagicinteract__smsMagic__c)sms).OwnerId, smsTask.OwnerId);
        System.assertEquals(((smagicinteract__smsMagic__c)sms).CreatedDate.date(), smsTask.ActivityDate);
        System.assertEquals(true, smsTask.Subject.contains(contact.MobilePhone));
    }
    
    @isTest
    static void UpdatedInboundSMSWithActivity_UpdatesActivityWithLatestInfo() 
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = 
            [
                SELECT Id,MobilePhone
                FROM Contact
                WHERE MobilePhone = '+614444455555'
            ];
        
        SObject sms = TestDataSMSMock.create(user, null, null, contact.MobilePhone, 'Test SMS', true);
        List<SObject> items = new List<SObject>{ sms };
        Map<Id, SObject> oldMap = new Map<Id, SObject>();                
        ActionResult result = ActivityService.create(items, oldMap, Enums.TriggerContext.AFTER_INSERT);
        
        smagicinteract__Incoming_SMS__c newSms = (smagicinteract__Incoming_SMS__c)sms.clone(true, false, true, true);
        newSms.smagicinteract__Contact__c = contact.Id;
        items = new List<SObject>{ newSms };
        oldMap = new Map<Id, SObject>();
        oldMap.put(sms.Id, sms);
                
        // Act
        result = ActivityService.create(items, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Executing a command should return a successful result to the caller.');
        
        // Assert
        List<Task> smsTasks =
            [
                SELECT Id, WhoId, WhatId, OwnerId, Subject, Type, ActivityDate
                FROM Task
                WHERE RecordType.DeveloperName = 'Client_Contact' AND WhatId = : sms.Id
            ];
        
        System.assertEquals(1, smsTasks.size());
        System.assertEquals('SMS', smsTasks[0].Type);
        System.assertEquals(((smagicinteract__Incoming_SMS__c)sms).OwnerId, smsTasks[0].OwnerId);
        System.assertEquals(((smagicinteract__Incoming_SMS__c)sms).CreatedDate.date(), smsTasks[0].ActivityDate);
        System.assertEquals(true, smsTasks[0].Subject.contains(contact.MobilePhone));
        System.assertEquals(contact.Id, smsTasks[0].WhoId);
    }
    
    @isTest
    static void UpdatedOutboundSMSWithActivity_UpdatesActivityWithLatestInfo() 
    {
        // Arrange
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = 
            [
                SELECT Id,MobilePhone
                FROM Contact
                WHERE MobilePhone = '+614444455555'
            ];
        
        SObject sms = TestDataSMSMock.create(user, null, null, contact.MobilePhone, 'Test SMS', false);
        List<SObject> items = new List<SObject>{ sms };
        Map<Id, SObject> oldMap = new Map<Id, SObject>();                
        ActionResult result = ActivityService.create(items, oldMap, Enums.TriggerContext.AFTER_INSERT);
        
        smagicinteract__smsMagic__c newSms = (smagicinteract__smsMagic__c)sms.clone(true, false, true, true);
        newSms.smagicinteract__Contact__c = contact.Id;
        items = new List<SObject>{ newSms };
        oldMap = new Map<Id, SObject>();
        oldMap.put(sms.Id, sms);
                
        // Act
        result = ActivityService.create(items, oldMap, Enums.TriggerContext.AFTER_UPDATE);
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Executing a command should return a successful result to the caller.');
        
        // Assert
        List<Task> smsTasks =
            [
                SELECT Id, WhoId, WhatId, OwnerId, Subject, Type, ActivityDate
                FROM Task
                WHERE RecordType.DeveloperName = 'Client_Contact' AND WhatId = : sms.Id
            ];
        
        System.assertEquals(1, smsTasks.size());
        System.assertEquals('SMS', smsTasks[0].Type);
        System.assertEquals(true, smsTasks[0].Subject.contains(contact.MobilePhone));
        System.assertEquals(((smagicinteract__smsMagic__c)sms).OwnerId, smsTasks[0].OwnerId);
        System.assertEquals(((smagicinteract__smsMagic__c)sms).CreatedDate.date(), smsTasks[0].ActivityDate);
        System.assertEquals(contact.Id, smsTasks[0].WhoId);
    }
}