public class AttachmentCreateCommand extends BaseCommand
{
    public override List<SObject> validate(List<SObject> items)
    {
        for (Attachment att : (List<Attachment>) items)
        {
            if (att.ParentId == null)
                throw new Exceptions.ApplicationException('Attachment parent id parameter cannot be null');

            if (String.isEmpty(att.Name))
                throw new Exceptions.ApplicationException('Attachment name parameter cannot be null or empty');

            if (att.body == null || att.body.size() == 0)
                throw new Exceptions.ApplicationException('Attachment body parameter cannot be null or empty');
        }

        return items;
    }

    public override ActionResult command(List<SObject> items)
    {
        Log.info(this.className, 'command', 'Inserting {0} attachments', new List<String>{ String.valueOf(items.size()) });

        Database.insert(items);

        return new ActionResult(ResultStatus.SUCCESS);
    }
}