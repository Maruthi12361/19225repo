public class SubjectService
{
    public static ActionResult createVersion(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        return new SubjectCreateVersionCommand().execute(items, oldMap, triggerContext);
    }
    
    public static ActionResult updatePrimaryVersion(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        return new SubjectUpdatePrimaryVersionCommand().execute(items, oldMap, triggerContext);
    }
}