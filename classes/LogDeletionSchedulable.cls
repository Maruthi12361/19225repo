global class LogDeletionSchedulable implements Schedulable
{
    global void execute(SchedulableContext ctx)
    {
        LogDeletionBatch deleteBatch = new LogDeletionBatch();
        Database.executeBatch(deleteBatch,1000);
    }
}