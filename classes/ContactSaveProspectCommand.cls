public class ContactSaveProspectCommand extends BaseComponent
{
    private static final String lastNameField = 'Cnt_LastName';
    private static final String emailField = 'Email';
    private static final String homePhoneField = 'HomePhone';
    private static final String personalEmailField = 'PersonalEmail__c';
    private static final String workEmailField = 'hed__WorkEmail__c';
    private static final String uniEmailField = 'hed__UniversityEmail__c';
    private static final String alternateEmailField = 'hed__AlternateEmail__c';
    private static final String activityHowCanWeHelp = 'Act_HowCanWeHelp__c';
    private static final String activityComments = 'Act_Comments__c';
    private Id prospectRecordTypeId = RecordTypeUtils.getRecordTypeId(Constants.RECORDTYPE_CONTACT_PROSPECT, SObjectType.Contact);

    private Set<String> notRewritableFields = new Set<String> { 'OriginalReferrerSource__c' };
    private Set<String> utmFields = new Set<String>{ 'utm_campaign__c', 'utm_content__c', 'utm_keyword__c', 'utm_medium__c', 'utm_source__c', 'utm_term__c', 'utm_gclid__c', 'utm_referralSource__c' };
    private Set<String> calculatorEmails = new Set<String> { 'Calculator_FeeHelp', 'Calculator_TimeToStudy', 'Calculator_ROI' };

    private Set<String> contactFields = new Set<String>
    {
        'Id', 'hed__Preferred_Email__c', 'hed__PreferredPhone__c', 'LeadSourceReferenceId__c', 'LeadSourceTests__c', 'RecordTypeId', 'Status__c', 'WelcomeProspectEmailSent__c',
        emailField, homePhoneField, personalEmailField, workEmailField, uniEmailField, alternateEmailField
    };
    private Set<String> oppFields = new Set<String>{ 'Id', 'Application_Case__c', 'IsClosed', 'OAF_Submit_Count__c', 'RecordTypeId' };

    private Boolean validate(Object parameter)
    {
        ViewModels.Param param = (ViewModels.Param) parameter;

        if (String.isEmpty(param.Identifier))
        {
            Log.error(this.className, 'validate',  Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Identifier' });
            throw new Exceptions.InvalidParameterException(String.format(Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Identifier' }));
        }
        else if (String.isEmpty(param.Payload))
        {
            Log.error(this.className, 'validate',  Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Prospect' });
            throw new Exceptions.InvalidParameterException(String.format(Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Prospect' }));
        }
        else
        {
            Map<String, Object> prospectMap = (Map<String, Object>)JSON.deserializeUntyped(param.Payload);

            if (prospectMap.size() == 0)
            {
                Log.error(this.className, 'validate',  Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Prospect Body' });
                throw new Exceptions.InvalidParameterException(String.format(Constants.ERRORMSG_NULLPARAM, new List<String>{ 'Prospect Body' }));
            }
        }

        return true;
    }

    public ActionResult command(Object parameter)
    {
        validate(parameter);

        ViewModels.Param param = (ViewModels.Param) parameter;
        Log.info(this.className, 'command', 'Request made with following parameter: Identifier {0} \n Prospect {1}', new List<String>{ param.Identifier, param.Payload });

        Boolean contactCreated = false;
        Boolean opportunityUpdated = false;
        Boolean contactUpdated = false;
        Boolean applicationCaseUpdate = false;
        Boolean activityCreate = false;
        Opportunity opp;
        Contact contact;
        Case applicationCase;

        try
        {
            contact = getContact(param);
        }
        catch(QueryException ex)
        {
            String message = ex.getMessage();
            ActionResult result = new ActionResult(ResultStatus.ERROR, message);
            Log.error(this.className, 'command', message);
            return result;
        }

        Boolean contactReferrerDataExists = false;
        Boolean isAuthenticated = param.Identifier.contains('@') ? false : true;
        Map<String, Object> prospectMap = (Map<String, Object>)JSON.deserializeUntyped(param.Payload);

        if (contact == null)
        {
            if (!prospectMap.containsKey('Cnt_Email') || (prospectMap.containsKey('Cnt_Email') && String.isEmpty(String.valueOf(prospectMap.get('Cnt_Email')))))
            {
                Log.error(this.className, 'command', 'Contact Email is a required field in order to create new contact');
                return new ActionResult(ResultStatus.ERROR, 'Contact Email address value is required');
            }

            String lastName = '.';

            if (prospectMap.containsKey('Cnt_LastName') && !String.isEmpty(String.valueOf(prospectMap.get('Cnt_LastName'))))
            {
                lastName = String.valueOf(prospectMap.get('Cnt_LastName'));
            }

            contact = new Contact
            (
                LastName = lastName,
                PersonalEmail__c = String.valueOf(prospectMap.get('Cnt_Email')),
                hed__Preferred_Email__c = 'Personal Email',
                RecordTypeId = prospectRecordTypeId,
                LeadSource = 'Unknown',
                CustomInformationPack__c = String.valueOf(prospectMap.get('Cnt_CustomInformationPack_c')),
                Program__c = String.valueOf(prospectMap.get('Cnt_Program_c')),
                Portfolio__c = String.valueOf(prospectmap.get('Cnt_Portfolio_c')) 
            );

            if (prospectMap.containsKey('Cnt_OwnerID') && !String.isEmpty(String.valueOf(prospectMap.get('Cnt_OwnerID'))))
            {
                contact.OwnerId = String.valueOf(prospectMap.get('Cnt_OwnerID'));
            }

            if (prospectMap.containsKey('Cnt_HomePhone') && !String.isEmpty(String.valueOf(prospectMap.get('Cnt_HomePhone'))))
            {
                contact.hed__PreferredPhone__c = 'Home';
                contact.HomePhone = String.valueOf(prospectMap.get('Cnt_HomePhone'));
            }

            if (prospectMap.containsKey('Cnt_WelcomeProspectEmailSent_c'))
            {
                contact.WelcomeProspectEmailSent__c = Boolean.valueOf(prospectMap.get('Cnt_WelcomeProspectEmailSent_c'));
            }

            Database.Insert(contact);
            contactReferrerDataExists = true;
            contactCreated = true;
        }
        else if (!param.AllowUpdate)
        {
            Log.info(this.className, 'command', 'Contact {0} not updated as update is not allow', new List<String> { contact.Id });
            ActionResult result = new ActionResult(ResultStatus.SUCCESS);
            result.ResultId = contact.Id;

            return result;
        }
        else if (contact.RecordTypeId != prospectRecordTypeId)
        {
            Log.error(this.className, 'command', 'Contact {0} is not a prospect recordtype: {1} instead of: {2}', new List<String>{ contact.Id, contact.RecordTypeId, prospectRecordTypeId });
            return new ActionResult(ResultStatus.ERROR, 'Updating non-prospect contact is not allowed');
        }

        Set<String> acquisitionStatuses = new Set<String>{Constants.STATUS_COLD, Constants.STATUS_WARM, Constants.STATUS_HOT};

        if (contact != null && !acquisitionStatuses.contains(contact.Status__c))
            contact.Status__c = Constants.STATUS_COLD;

        if (contact != null && contact.Opportunities__r.size() > 0)
        {
            opp = contact.Opportunities__r[0];

            if (opp.Application_Case__c != null)
                applicationCase = getApplicationCase(opp.Application_Case__c);
        }

        for (String fieldName : prospectMap.keySet())
        {
            Object fieldValue = prospectMap.get(fieldName);
            Boolean isPrimaryField = isPrimaryField(fieldName);

            if (fieldName == lastNameField && String.isEmpty(String.valueOf(fieldValue)))
                fieldValue = '.';

            if (fieldName.endsWith(Constants.VIEWMODEL_CUSTOMFIELDSUFFIX) && !utmFields.contains(fieldName))
                fieldName = (fieldName.removeEnd(Constants.VIEWMODEL_CUSTOMFIELDSUFFIX)) + '__c';

            // Custom HEDA field which requires double underscore, ie: fieldname: hed__Gender__c and ViewModel: Cnt_hed_Gender_c
            if (fieldName.startsWith(Constants.VIEWMODEL_CONTACTHEDAPREFIX))
                fieldName = (fieldName.replace(Constants.VIEWMODEL_CONTACTHEDAPREFIX, Constants.VIEWMODEL_CONTACTHEDAPREFIX + '_'));

            if (fieldName.indexOfIgnoreCase(Constants.VIEWMODEL_CONTACTPREFIX) != -1)
            {
                if (!isAuthenticated)
                {
                    if (!isPrimaryField)
                    {
                        Log.info(this.className, 'command', 'Contact non primary field {0} not updated as contact ({1}) is not authenticated', new List<String> { fieldName, contact.Id });
                        continue;
                    }
                    else if (fieldName == 'Cnt_Email')
                    {
                        Log.info(this.className, 'command', 'Contact Email field not updated as contact ({0}) is not authenticated', new List<String> { contact.Id });
                        continue;
                    }
                }

                fieldName = fieldName.substringAfter('_');

                if (fieldName == emailField)
                {
                    switch on contact.hed__Preferred_Email__c
                    {
                        when 'Personal Email'
                        {
                            fieldName = personalEmailField;
                        }    
                        when 'Work'
                        {
                            fieldName = workEmailField;
                        }
                        when 'University'
                        {
                            fieldName = uniEmailField;
                        }
                        when else
                        {
                            fieldName = alternateEmailField;
                        }
                    }
                }
                // If HomePhone is being set and we dont already have a value for PreferredPhone then set it by default to Home
                else if (fieldName == homePhoneField && !String.isEmpty(String.valueOf(fieldValue)) && String.isEmpty(contact.hed__PreferredPhone__c))
                {
                    contact.hed__PreferredPhone__c = 'Home';
                    contactUpdated = true;
                }

                if (updateField(contact, fieldName, fieldValue))
                    contactUpdated = true;
            }
            else if (fieldName.indexOfIgnoreCase(Constants.VIEWMODEL_OPPORTUNITYPREFIX) != -1)
            {
                if (opp == null)
                {
                    String errorMsg = 'Trying to update Opportunity\'s ' + fieldName + ' field value when Contact does not have any open Acquisition Opportunities';
                    Log.error(this.className, 'command', errorMsg);
                    return new ActionResult(ResultStatus.ERROR, errorMsg);
                }

                if (!isAuthenticated && !isPrimaryField)
                {
                    Log.info(this.className, 'command', 'Opportunity non primary field {0} not updated as contact ({1}) is not authenticated', new List<String> { fieldName, contact.Id });
                    continue;
                }

                fieldName = fieldName.substringAfter('_');

                if (fieldName == 'OAF_Submitted__c' && Boolean.valueOf(fieldValue) && !opp.OAF_Submitted__c)
                {
                    opp.OAF_Submit_Count__c += 1;
                    opportunityUpdated = true;
                }

                if (updateField(opp, fieldName, fieldValue))
                    opportunityUpdated = true;
            }
            else if (fieldName.indexOfIgnoreCase(Constants.VIEWMODEL_CASEPREFIX) != -1)
            {
                if (applicationCase == null)
                {
                    String errorMsg = 'Trying to update Opportunity\'s application case ' + fieldName + ' field value when there are is no Case Application';
                    Log.error(this.className, 'command', errorMsg);
                    return new ActionResult(ResultStatus.ERROR, errorMsg);
                }

                if (!isAuthenticated)
                {
                    Log.info(this.className, 'command', 'Application case field {0} not updated as contact ({1}) is not authenticated', new List<String> { fieldName, contact.Id });
                    continue;
                }

                fieldName = fieldName.substringAfter('_');

                if (updateField(applicationCase, fieldName, fieldValue))
                    applicationCaseUpdate = true;
            }
            else if (fieldName.indexOfIgnoreCase(Constants.VIEWMODEL_ACTIVITYPREFIX) != -1)
            {
                if ((fieldName == activityHowCanWeHelp || fieldName == activityComments) &&  fieldValue != null)
                    activityCreate = true;
            }
            else if (utmFields.contains(fieldName))
            {
                contactReferrerDataExists = true;
            }
            else if (fieldName == 'SendEmail')
            {
                if (calculatorEmails.contains(String.valueOf(fieldValue)))
                {
                    Log.info(this.className, 'command', 'Sending calculator email');

                    ViewModels.Param emailParam = new ViewModels.Param();
                    ViewModelsAcquisition.CalculatorEmail calculatorEmail = new ViewModelsAcquisition.CalculatorEmail();
                    calculatorEmail.EmailName = String.valueOf(fieldValue);

                    emailParam.Identifier = contact.Id;
                    emailParam.Payload =  JSON.serialize(calculatorEmail);
                    ContactService.sendCalculatorEmail(emailParam);
                }
                else
                {
                    Log.info(this.className, 'command', 'No recognised emails to send');
                }
            }
            else if (fieldName == 'LeadSourceReferenceId')
            {
                if (!String.isEmpty(String.valueOf(fieldValue)) && String.isEmpty(contact.LeadSourceReferenceId__c))
                {
                    contact.LeadSourceReferenceId__c = String.valueOf(fieldValue);
                    contactUpdated = true;
                }
            }
            else if (fieldName == 'LeadSourceTests')
            {
                List<String> sourceTests = String.valueOf(fieldValue).split(',');

                if (sourceTests.size() == 0)
                    continue;

                if (String.isEmpty(contact.LeadSourceTests__c))
                {
                    contact.LeadSourceTests__c = fieldValue.toString().replace(',', ';');
                    contactUpdated = true;
                    continue;
                }

                Set<String> picklistValues = new Set<String>(new List<String>(contact.LeadSourceTests__c.split(';')));

                for (String value : sourceTests)
                    picklistValues.add(value.trim());

                contact.LeadSourceTests__c = String.join(new List<String>(picklistValues), ';');
                contactUpdated = true;
            }
            else
            {
                Log.error(this.className, 'command', 'Unknown field: {0}', new List<String>{ fieldName });
                return new ActionResult(ResultStatus.ERROR, 'Unknown field: ' + fieldName);
            }
        }

        try
        {
            if (contactUpdated)
                Database.update(contact);
            else
                Log.info(this.className, 'command', 'No change is detected on Contact');

            if (activityCreate)
                createActivity(contact.Id, contactCreated, prospectMap);
            else
                Log.info(this.className, 'command', 'No activity created');

            if (opportunityUpdated)
                Database.update(opp);
            else
                Log.info(this.className, 'command', 'No change is detected on Opportunity');

            if (applicationCaseUpdate)
                Database.update(applicationCase);
            else
                Log.info(this.className, 'command', 'No change is detected on Opportunity\'s application case ');

            if (contactReferrerDataExists)
                handleContactReferrerData(contact.Id, prospectMap);
        }
        catch(DmlException ex)
        {
            String message = ex.getMessage();
            ActionResult result = new ActionResult(ResultStatus.ERROR, message);
            Log.error(this.className, 'command', message);
            return result;
        }

        ActionResult result = new ActionResult(ResultStatus.SUCCESS);
        result.ResultId = contact.Id;

        if (contactCreated)
            result.messages.add('Created');

        return result;
    }

    private Boolean updateField(SObject obj, String fieldName, Object fieldValue)
    {
        Boolean isDirty = false;

        try
        {
            Schema.DisplayType fieldType = SObjectUtils.getFieldType(obj, fieldName);
            Object currentFieldValue = obj.get(fieldName);

            if (SObjectUtils.isStringType(obj, fieldName) && fieldValue != null)
            {
                Integer length = SObjectUtils.getMaxStringLength(obj, fieldName);
                String stringValue = fieldValue.toString();
                stringValue = StringUtils.copyLeft(stringValue, length);
                fieldValue = (Object)stringValue;
            }

            if (notRewritableFields.contains(fieldName) && fieldValue != null)
            {
                if (currentFieldValue == null)
                {
                    obj.put(fieldName, fieldValue);
                    isDirty = true;
                }
            }
            else if (fieldValue == null)
            {
                if (currentFieldValue != null)
                {
                    obj.put(fieldName, fieldValue);
                    isDirty = true;
                }
            }
            else if (fieldType == Schema.DisplayType.Date)
            {
                if (currentFieldValue != Date.valueOf(fieldValue.toString()))
                {
                    obj.put(fieldName, Date.valueOf(fieldValue.toString()));
                    isDirty = true;
                }
            }
            else if (fieldType == Schema.DisplayType.Datetime)
            {
                String dateStringValue = fieldValue.toString();
                dateStringValue = dateStringValue.replace('T', ' ');
                dateStringValue = dateStringValue.replace('Z', ' ');

                if (currentFieldValue != Datetime.valueOfGmt(dateStringValue))
                {
                    obj.put(fieldName, Datetime.valueOfGmt(dateStringValue));
                    isDirty = true;
                }
            }
            else if (fieldType == Schema.DisplayType.MultiPicklist)
            {
                if (currentFieldValue != fieldValue.toString().replace(',', ';'))
                {
                    obj.put(fieldName, fieldValue.toString().replace(',', ';'));
                    isDirty = true;
                }
            }
            else if (currentFieldValue != fieldValue)
            {
                obj.put(fieldName, fieldValue);
                isDirty = true;
            }
        }
        catch(Exception ex)
        {
            String errMsg = String.format('Failed to set value to {0} field', new List<String>{ fieldName });
            Log.error(this.className, 'updateField', errMsg, ex);
            throw new Exceptions.ApplicationException(errMsg);
        }

        return isDirty;
    }

    private void handleContactReferrerData(Id contactId, Map<String, Object> prospectMap)
    {
        Log.info(this.className, 'handleContactReferrerData', 'Creating contact referrer record for Contact: ' + contactId);

        ViewModelsAcquisition.ContactReferrer referrer = new ViewModelsAcquisition.ContactReferrer();
        referrer.ContactId = contactId;
        referrer.utm_campaign = (String) prospectMap.get('utm_campaign__c');
        referrer.utm_content = (String) prospectMap.get('utm_content__c');
        referrer.utm_keyword = (String) prospectMap.get('utm_keyword__c');
        referrer.utm_medium = (String) prospectMap.get('utm_medium__c');
        referrer.utm_source = (String) prospectMap.get('utm_source__c');
        referrer.utm_term = (String) prospectMap.get('utm_term__c');
        referrer.utm_gclid = (String) prospectMap.get('utm_gclid__c');
        referrer.utm_referralSource = (String) prospectMap.get('utm_referralSource__c');

        ContactReferrerService.create(referrer);
    }

    private void createActivity(Id contactId, Boolean isNewContact, Map<String, Object> prospectMap)
    {
        Log.info(this.className, 'handleActivity', 'Creating a call activity record for Contact: ' + contactId);
        
        Contact contact = [SELECT Id, AccountId, OwnerId FROM Contact WHERE Id =: contactId];
        Id followUpTaskRecordTypeId = RecordTypeUtils.getRecordTypeId(Constants.RECORDTYPE_TASK_FOLLOWUP, SObjectType.Task);
        String howCanWeHelp = (String) prospectMap.get('Act_HowCanWeHelp_c');
        String comments = (String) prospectMap.get('Act_Comments_c');
        
        ViewModelsAcquisition.Activity activity = new ViewModelsAcquisition.Activity();

        activity.IsNewContact = isNewContact;
        activity.AssignedTo = contact.OwnerId;
        activity.RelatedTo = contact.AccountId;
        activity.Name = contactId;
        activity.TaskType = 'Call';
        activity.TaskRecordType = followUpTaskRecordTypeId;
        activity.Priority = 'High';
        activity.Status = 'Open';
        activity.DueDate = System.today().addDays(1);
        activity.NotifyOwner = true;

        if (isNewContact)
            activity.Subject = Constants.TASKSUBJECT_FOR_NEW_CONTACT;
        else
            activity.Subject = Constants.TASKSUBJECT_FOR_EXISTING_CONTACT;

        activity.Comments = 'How can we help?\r\n' + howCanWeHelp + '\r\n \r\n' + 'Comments provided: ' + (comments == null ? 'None' : comments);

        ActivityService.createActivity(activity);
    }

    private Contact getContact(ViewModels.Param param)
    {
        Map<String, Object> fieldMap = (Map<String, Object>)JSON.deserializeUntyped(param.Payload);
        Map<String, Object> modelMap = (Map<String, Object>)JSON.deserializeUntyped(JSON.serialize(new ViewModelsAcquisition.Details()));

        for (String fieldName : fieldMap.keySet())
        {
            if (!fieldName.startsWith(Constants.VIEWMODEL_CONTACTPREFIX) && !fieldName.startsWith(Constants.VIEWMODEL_OPPORTUNITYPREFIX))
                continue;

            if (!modelMap.containsKey(fieldName))
            {
                Log.error(this.className, 'getContact', '{0} field not in ViewModelsAcquisition.Details. Cannot update', new List<String> { fieldName });
                throw new Exceptions.InvalidParameterException('Invalid field: ' + fieldName);
            }

            if (fieldName.endsWith(Constants.VIEWMODEL_CUSTOMFIELDSUFFIX))
                fieldName = (fieldName.removeEnd(Constants.VIEWMODEL_CUSTOMFIELDSUFFIX)) + '__c';

            // Custom HEDA field which requires double underscore, ie: fieldname: hed__Gender__c and ViewModel: Cnt_hed_Gender_c
            if (fieldName.startsWith(Constants.VIEWMODEL_CONTACTHEDAPREFIX))
                fieldName = (fieldName.replace(Constants.VIEWMODEL_CONTACTHEDAPREFIX, Constants.VIEWMODEL_CONTACTHEDAPREFIX + '_'));

            if (fieldName.startsWith(Constants.VIEWMODEL_CONTACTPREFIX))
                contactFields.add(fieldName.substringAfter('_'));
            else if (fieldName.startsWith(Constants.VIEWMODEL_OPPORTUNITYPREFIX))
                oppFields.add(fieldName.substringAfter('_'));
        }

        String query =
            '{contactFields}, ' +
            '( ' +
                'SELECT {oppFields} ' +
                'FROM Opportunities__r ' +
                'WHERE RecordType.DeveloperName = \'' + Constants.RECORDTYPE_OPPORTUNITY_ACQUISITION + '\' AND IsClosed = false ' +
                'LIMIT 1 ' +
            ') ';
        
        query = query.replace('{contactFields}', String.join(new List<String>(contactFields), ', '));
        query = query.replace('{oppFields}', String.join(new List<String>(oppFields), ', '));

        Log.info(this.className, 'getContact', query);

        return ContactService.get(param.Identifier, query, false);
    }

    private Boolean isPrimaryField(String fieldName)
    {
        for (String primaryField : Constants.PROSPECT_PRIMARYFIELDS)
        {
            if (primaryField.equalsIgnoreCase(fieldName))
                return true;
        }

        return false;
    }

    private Case getApplicationCase(Id caseApplicationId)
    {
        return
        [
            SELECT Id, Invoice_Generated__c, Invoice_Processed__c, Invoice_Required__c, FEE_HELP_Processed__c, FeeHelpDetailsRequired__c, WelcomeLetterViewed__c
            FROM Case
            WHERE Id = :caseApplicationId
        ];
    }
}