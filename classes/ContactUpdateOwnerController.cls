public class ContactUpdateOwnerController
{
    ApexPages.StandardSetController setCon;
    private static List<PermissionsetAssignment> permSets = [SELECT Id, AssigneeId FROM PermissionSetAssignment WHERE PermissionSet.Name = 'TeamLeader'];
    private Set<Id> teamLeaders = new Set<Id>();

    public ContactUpdateOwnerController(ApexPages.StandardSetController controller)
    {
        setCon = controller;
        for (PermissionSetAssignment ps : permSets)
            teamLeaders.add(ps.AssigneeId);
    }

    public PageReference updateOwnerToRoundRobin()
    {
        if (!teamLeaders.contains(UserInfo.getUserId()) && UserInfo.getProfileId() != '00e36000000mUroAAE')
            return setCon.cancel();

        Map<Id, Contact> selectedContacts = new Map<Id,Contact>((List<Contact>)setCon.getSelected());

        List<Contact> contacts =
        [
            SELECT Id, ownerid
            FROM Contact
            WHERE id in : selectedContacts.keySet() AND
                ((RecordType.DeveloperName =: Constants.RECORDTYPE_CONTACT_PROSPECT AND Status__c !=: Constants.STATUS_DISQUALIFIED) OR
                 (RecordType.DeveloperName =: Constants.RECORDTYPE_CONTACT_STUDENT AND Status__c =: Constants.STATUS_PROGRESSION))
        ];
        for (Contact contact : contacts)
            contact.ownerid = Constants.USERID_ROUNDROBIN;

        update contacts;

        return setCon.save();
    }
}