@isTest
public class BaseViewModelCommandTest
{
    @testSetup
    static void setup()
    {
        TestUtilities.setupHEDA();
    }

    @isTest
    static void ExecuteCommandWithNullParameters_RaisesExeceptionToCaller()
    {
        // Arrange
        List<ViewModelsAcquisition.ContactReferrer> nullObj;
        TestCommand command = new TestCommand();

        // Act
        try
        {
            command.execute(nullObj);
        }
        catch(Exception ex)
        {
            //Assert
            System.assert(ex instanceof InvalidParameterValueException, 'Executing a command with a null parameter should raise an exception for the developer.');
        }
    }

    @isTest
    static void ExcecuteCommandWithEmptySetOfParameters_ReturnsSuccessResultToCaller()
    {
        // Arrange
        List<Interfaces.IViewModel> items = new List<Interfaces.IViewModel>();
        TestCommand command = new TestCommand();

        // Act
        ActionResult result = command.execute(items);

        // Arrange
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Executing a command without any items to action against should return a successful result to the caller.'); 
    }

    @isTest
    static void ExecuteCommandAfterSuccessfulValidationAndExecution_ReturnsSuccessToCaller()
    {
        // Arrange
        ViewModelsAcquisition.ContactReferrer referrer = getViewModel();
        List<ViewModelsAcquisition.ContactReferrer> items = new List<ViewModelsAcquisition.ContactReferrer> { referrer };
        TestCommand command = new TestCommand();

        // Act
        ActionResult result = command.execute(items);

        //Assert
        system.assertEquals(ResultStatus.SUCCESS, result.Status, 'Executing a command with valid items, validating and executing successfully, should return a successful result to the caller.'); 
    }

    @isTest
    static void ExecuteCommandWithSingleObjectAfterSuccessfulValidationAndExecution_ReturnsSuccessToCaller()
    {
        // Arrange
        ViewModelsAcquisition.ContactReferrer item = getViewModel();
        TestCommand command = new TestCommand();

        // Act
        ActionResult result = command.execute(item);

        //Assert
        system.assertEquals(ResultStatus.SUCCESS, result.Status, 'Executing a command with valid items, validating and executing successfully, should return a successful result to the caller.'); 
    }

    class TestValidationExeceptionCommand extends BaseViewModelCommand
    {
        public override List<Interfaces.IViewModel> validate(List<Interfaces.IViewModel> params)
        {
            throw new SObjectException('Unexpected exception during validation action');
        }

        public override ActionResult command(List<Interfaces.IViewModel> params)
        {
            return new ActionResult();
        }
    }

    class TestCommandExceptionCommand extends BaseViewModelCommand
    {
        public override List<Interfaces.IViewModel> validate(List<Interfaces.IViewModel> params)
        {
            return params;
        }

        public override ActionResult command(List<Interfaces.IViewModel> params)
        {
            throw new SObjectException('Unexpected exception during command action');
        }
    }

    class TestCommand extends BaseViewModelCommand
    {
        public override List<Interfaces.IViewModel> validate(List<Interfaces.IViewModel> params)
        {
            return params;
        }

        public override ActionResult command(List<Interfaces.IViewModel> params)
        {
            return new ActionResult();
        }
    }

    private static ViewModelsAcquisition.ContactReferrer getViewModel()
    {
        return new ViewModelsAcquisition.ContactReferrer();
    }
}