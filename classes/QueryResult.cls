public class QueryResult
{
    public String queryName { get; set; }
    public String queryDescription { get; set; }
    public List<String> headers { get; set; }
    public List<List<String>> results { get; set; }
    public Integer totalResults { get; set; }

    public class QueryResultListWrapper
    {
        // Keeps track of the TOTAL number of QueryResults passed in even if they returned succesfully with no failures.
        public Integer totalResultsCount = 0;
        public List<QueryResult> resultList = new List<QueryResult>();

        public QueryResultListWrapper(List<QueryResult> results)
        {
            for(QueryResult result : results)
                if (++totalResultsCount > 0 && result.totalResults > 0)
                    resultList.add(result);
        }
    }

    public QueryResult(String queryName, String queryDescription, List<String> headers, List<AggregateResult> results)
    {
        this.queryName = queryName;
        this.queryDescription = queryDescription;
        this.headers = headers;
        this.totalResults = 0;
        this.results = new List<List<String>>();
        processResults(results);
    }

    private void processResults(List<AggregateResult> results)
    {
        for (AggregateResult result : results)
        {
            List<String> row = new List<String>();
            for (String header : headers)
                row.add(String.valueOf(result.get(header)));

            // Note: The first String in the row is always expected to represent the count of failures for that row with remaining Strings representing values for the grouping columns
            this.totalResults += Integer.valueOf(row[0]);
            this.results.add(row);
        }
    }

    public String getAsHTMLTable()
    {
        Integer printTotal = 0;
        String tableHTML = '<table align="center" border="1" cellpadding="5" cellspacing="0" width="90%" style="color: #000000;font-size: 12px;font-family: Helvetica, Arial, sans-serif;">';

        tableHTML += '<tr>';
        for (String header : this.headers)
            tableHTML += '<td><b>' + header + (printTotal++ == 0 ? ' (' + this.totalResults + ')' : '') + '</b></td>';

        tableHTML += '</tr>';

        for (List<String> row : this.results)
        {
            tableHTML += '<tr>';
            for (String value : row)
                tableHTML += '<td>' + value + '</td>';

            tableHTML += '</tr>';
        }
        tableHTML += '</table>';
        return tableHTML;
    }
}