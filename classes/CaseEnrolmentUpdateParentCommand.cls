public class CaseEnrolmentUpdateParentCommand extends BaseTriggerCommand
{
    public override List<SObject> validate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        List<CaseEnrolment__c> result = new List<CaseEnrolment__c>();

        for (CaseEnrolment__c item : (List<CaseEnrolment__c>)items)
        {
            if (triggerContext == Enums.TriggerContext.AFTER_INSERT || triggerContext == Enums.TriggerContext.AFTER_DELETE)
            {
                if (item.Case__c != null)
                    result.add(item);
            }
        }

        return result;
    }

    public override ActionResult command(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        Set<Id> idSet = new Set<Id>();

        for (CaseEnrolment__c item : (List<CaseEnrolment__c>)items)
        {
            idSet.add(item.Case__c);
        }

        List<CaseEnrolment__c> caseEnrolments =
            [
                SELECT Case__c, SubjectEnrolment__r.SubjectCode__c, SubjectEnrolment__r.SubjectName__c
                FROM CaseEnrolment__c
                WHERE Case__c IN : idSet AND SubjectEnrolment__c <> null
                ORDER BY Case__c, SubjectEnrolment__r.SubjectCode__c
            ];
        Map<Id, String> caseMap = new Map<Id, String>();

        for (CaseEnrolment__c item : caseEnrolments)
        {
            String summary = caseMap.get(item.Case__c);

            if (String.isBlank(summary))
                summary = StringUtils.concat(item.SubjectEnrolment__r.SubjectCode__c, item.SubjectEnrolment__r.SubjectName__c, ' ');
            else
                summary = StringUtils.concat(summary, StringUtils.concat(item.SubjectEnrolment__r.SubjectCode__c, item.SubjectEnrolment__r.SubjectName__c, ' '), ',');

            caseMap.put(item.Case__c, summary);
        }

        for (Id key : idSet)
        {
            Case updatedCase = new Case(Id = key, SubjectEnrolmentSummary__c = caseMap.get(key));
            uow.registerDirty(updatedCase);
        }

        return new ActionResult();
    }
}