@isTest
public class SMSSendViaMarketingCloudCommandTests 
{
    @testSetup
    static void setup()
    {
        TestUtilities.setupHEDA();
        User user = UserService.getByAlias(Constants.USER_TESTSETUP);
        Contact contact = TestDataContactMock.createProspect(user, 'Anna', 'Smith', '', '0455566755', 'anna.smith@gmail.com', Constants.STATUS_COLD, 'A001588497', 'Australia', 'Website (Organic)');
    }
    
    @isTest
    private static void SendToRecipientWithMarketingCloudProvider_SendSMSViaMarketingCloud()
    {
        // Arrange
        Contact contact = [SELECT Id, Email, FirstName, LastName, MobilePhone FROM Contact LIMIT 1];

        // Act
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new TestHttpCalloutMock());
        ActionResult result = SMSService.sendToRecipient(Constants.APIPROVIDER_MARKETINGCLOUD, 'SMSServiceTest', contact.MobilePhone, 'SendToRecipient_SendSMSByMarketingCloud');
        Test.stopTest();
        
        // Assert
        System.assert(result.isSuccess, 'Fail to send SMS by Marketing Cloud API');
        System.assertEquals('Message is successfully sent via MarketingCloud', result.messages[0]);
    }
}