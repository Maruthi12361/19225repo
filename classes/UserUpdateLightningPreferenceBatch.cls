public class UserUpdateLightningPreferenceBatch extends BaseBatchCommand
{
    public String query =
        'SELECT Id, UserPreferencesLightningExperiencePreferred ' +
        'FROM User ' +
        'WHERE IsActive = true AND UserPreferencesLightningExperiencePreferred = false AND Profile.Name != \'System Administrator\'';

    public override Database.QueryLocator query(Database.BatchableContext BC)
    {
        return Database.getQueryLocator(query);
    }

    public override void command(Database.BatchableContext BC, List<sObject> scope)
    {
        for (User user : (List<User>) scope)
            user.UserPreferencesLightningExperiencePreferred = true;

        update scope;
    }
}