public class GroupMemberUtils
{
    private static Map<String, Set<Id>> allGroupsMembers = new Map<String, Set<Id>>();

    public static boolean containsUser(String groupDeveloperName, Id userId)
    {
        Set<Id> groupMembers = getGroupMembers(groupDeveloperName);

        return groupMembers.contains(userId);
    }

    public static Set<Id> getGroupMembers(String groupDeveloperName)
    {
        Set<Id> groupMembers = allGroupsMembers.get(groupDeveloperName);
        
        if (groupMembers == null)
        {
            List<GroupMember> gms = [SELECT userOrGroupId FROM GroupMember WHERE Group.DeveloperName =: groupDeveloperName];
            groupMembers = new Set<Id>();

            for (GroupMember gm : gms)
                groupMembers.add(gm.UserOrGroupId);

            allGroupsMembers.put(groupDeveloperName, groupMembers);
        }

        return groupMembers;
    }
}