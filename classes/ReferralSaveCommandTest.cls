@isTest
public class ReferralSaveCommandTest
{
    @testSetup
    static void setup()
    {
        TestUtilities.setupHEDA();
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact contact = TestDataContactMock.createStudent(user);
    }

    @isTest
    static void CreateNewReferral_ReferralSuccessfullyCreated()
    {
        // Arrange
        TestUtilities.disableComponent('ReferralSendEmailsCommand');
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact referrer = [SELECT Id FROM Contact LIMIT 1];
        Contact referee = TestDataContactMock.createProspect(user, 'Australia');
        Referral__c referral = TestDataReferralMock.create(user, referrer.Id, referee.Id, false);

        // Act
        ActionResult result = new ReferralSaveCommand().execute(referral);
        List<Referral__c> referrals = [SELECT Id, Referrer_Contact__c, Referee_Contact__c FROM Referral__c WHERE Referrer_Contact__c = :referral.Referrer_Contact__c AND Referee_Contact__c = :referral.Referee_Contact__c];

        // Assert
        System.assertEquals(ResultStatus.SUCCESS, result.Status, 'Unexpected action result status');
        System.assertEquals(1, referrals.size(), 'Unexpected number of referrals created');
    }

    @isTest
    static void CreateNewReferralWithoutReferrerId_InvalidParameterExceptionThrown()
    {
        // Arrange
        TestUtilities.disableComponent('ReferralSendEmailsCommand');
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact referrer = [SELECT Id FROM Contact LIMIT 1];
        Contact referee = TestDataContactMock.createProspect(user, 'Australia');
        Referral__c referral = TestDataReferralMock.create(user, null, referee.Id, false);
        String errorMsg = '';

        // Act
        try
        {
            ActionResult result = new ReferralSaveCommand().execute(referral);
        }
        catch (Exception ex)
        {
            errorMsg = ex.getMessage();
        }

        // Assert
        System.assertEquals(true, errorMsg.contains('Referrer Contact and Referree Contact field values cannot be null'), 'Unexpected exception message');
    }

    @isTest
    static void CreateNewReferralWithoutRefereeId_InvalidParameterExceptionThrown()
    {
        // Arrange
        TestUtilities.disableComponent('ReferralSendEmailsCommand');
        User user = UserService.getByAlias(Constants.USER_SYSTEMADMIN);
        Contact referrer = [SELECT Id FROM Contact LIMIT 1];
        Contact referee = TestDataContactMock.createProspect(user, 'Australia');
        Referral__c referral = TestDataReferralMock.create(user, referrer.Id, null, false);
        String errorMsg = '';

        // Act
        try
        {
            ActionResult result = new ReferralSaveCommand().execute(referral);
        }
        catch (Exception ex)
        {
            errorMsg = ex.getMessage();
        }

        // Assert
        System.assertEquals(true, errorMsg.contains('Referrer Contact and Referree Contact field values cannot be null'), 'Unexpected exception message');
    }
}