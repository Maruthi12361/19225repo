public class SubjectEnrolmentService
{
    public static ActionResult calculate(List<SObject> items, Map<Id, SObject> oldMap, Enums.TriggerContext triggerContext)
    {
        return new SubjectEnrolmentCalculateCommand().execute(items, oldMap, triggerContext);
    }

    public static ActionResult sendWorkInstructionsEmail(List<SObject> items)
    {
        return new SubjectEnrolmentSendEmailCommand().execute(items, null, null);
    }
}