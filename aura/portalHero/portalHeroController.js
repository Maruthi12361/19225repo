({
    doInit : function(component, event, helper) 
    {
        var action = component.get("c.getContactByUserId");
        action.setCallback(this, function(response)
        {
            var state = response.getState();

            if (state === "SUCCESS")
            {
                var con = response.getReturnValue();
                component.set("v.student", con);   

                if (con != null && con.RolloverEligible__c && !con.IsEnroledNextTerm__c && !con.IsEnroledFutureTerm__c
                   && (con.AtRisk__c === 'No' || con.AtRisk__c == null ) && !con.Rollover_Status__c.includes('No Future Enrollments')
                   && !con.Rollover_Status__c.includes('All Stages Scheduled'))
                {
                    component.set("v.isNewStudent",false);
                    component.set("v.isRolloverStudent",true);
                    component.set("v.backgroundClass","rolloverBackground");
                }
                else
                {
                    component.set("v.isNewStudent",true);
                    component.set("v.isRolloverStudent",false);
                    component.set("v.backgroundClass","contentPanel");
                }
            }
        });

        $A.enqueueAction(action);
    }
})