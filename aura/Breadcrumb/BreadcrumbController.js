({
    initialize : function(component){
        var links = [{
            title: "Home",
            href: "/s/"
        }];
        
        for(var i = 1; i <= 5; i++) {
            links.push({
                title: component.get('v.title_' + i),
                href: component.get('v.link_' + i)
            })
        }
        
        component.set( "v.link_list", links );
    },
    doneRendering: function(component, event) {
        var current = component.find("currentURL");
        current.set("v.value", window.location.pathname);
        current.set("v.label", document.title);
    }
})