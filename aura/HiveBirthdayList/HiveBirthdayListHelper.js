({
  // Fetch the accounts from the Apex controller
  getPhotoList: function(component) {
    var action = component.get('c.getBirthdayPhotos');

    // Set up the callback
    var self = this;
    action.setCallback(this, function(actionResult) {
     component.set('v.photos', actionResult.getReturnValue());
    });
    $A.enqueueAction(action);
  }
})