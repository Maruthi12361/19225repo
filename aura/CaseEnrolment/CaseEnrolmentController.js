({
    doInit : function(component, event, helper)
    {
        var portalMode = component.get("v.portalMode");
        
        if (portalMode === true)        
        {
            component.set("v.showForm", true);
        }
        else
        {
            helper.checkCase(component);
        }
    },
    
    showData : function(component, event, helper)
    {
        if (component.get("v.showForm") === true)
        {
            component.set("v.showData", true);
            helper.loadSubjects(component);
        }
    },

    loadData : function(component, event, helper)
    {
        if (component.get("v.showData") === true)
        {
            helper.loadSubjects(component);
        }
    },    

    selectRows : function (component, event, helper)
    {
        var selectedRows = event.getParam('selectedRows');
        component.set("v.selectedRows", selectedRows);
        helper.clearInfo(component);
    },

    saveData : function(component, event, helper)
    {
        var selectedRows = component.get("v.selectedRows");
        helper.clearError(component);

        if (selectedRows && selectedRows.length > 0)
        {
            var param = [];            
            selectedRows.forEach(function(item){                
                param.push(item.Id);
            });
            helper.saveEnrolments(component, param, null);
        }
        else
        {
            helper.showInfo(component, 'Please select subjects first.');
        }
    },

    onCancel : function(component, event, helper)
    {
        helper.closeUI(component);
    },

    handleCreateEvent : function(component, event, helper)
    {
        var caseId = event.getParam("CaseId");
        var selectedRows = component.get("v.selectedRows");

        if (caseId && selectedRows && selectedRows.length > 0)
        {
            var param = [];            
            selectedRows.forEach(function(item){                
                param.push(item.Id);
            });
            helper.saveEnrolments(component, param, caseId);
        }
    }
})