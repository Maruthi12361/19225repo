({
    checkCase : function(component)
    {
        var action = component.get("c.getCaseInfo");
        this.showInfo(component, 'Loading...');
        action.setParams({"caseId": component.get("v.recordId")});
        action.setCallback(
            this, function(response)
            {
                var state = response.getState();

                if (state === "SUCCESS")
                {
                    var caseInfo = response.getReturnValue();
                    component.set("v.showForm", caseInfo && !caseInfo.IsClosed && caseInfo.RecordType.DeveloperName !== "Application" && caseInfo.RecordType.DeveloperName !== "Rollover");
                    this.clearInfo(component);
                }
                else
                {
                    this.clearInfo(component);
                    this.showError(component, response);
                }
            }
        );
        $A.enqueueAction(action);
    },

    loadSubjects : function(component)
    {
        var selectedRows = component.get("v.selectedRows");
        var portalMode = component.get("v.portalMode");

        if (selectedRows && selectedRows.length > 0)
        {
            component.set("v.selectedRows", []);
            component.set("v.selectingRows", []);
        }

        var action;
        this.showInfo(component, 'Loading...');

        if (portalMode === false)
        {
            action = component.get("c.getSubjects");
            action.setParams({"caseId": component.get("v.recordId")});
        }
        else
        {
            action = component.get("c.getSubjectsByPortalUser");
        }

        action.setCallback(
            this, function(response)
            {
                var state = response.getState();

                if (state === "SUCCESS")
                {
                    var dataRows;

                    if (portalMode === true)
                    {
                        dataRows = response.getReturnValue();
                        this.renderTableAsEnrolmentsForPortal(component);
                    }
                    else
                    {
                        var payload = response.getReturnValue();

                        if (payload && payload.Type && payload.Data)
                        {
                            dataRows = this.transformData(payload.Data);

                            if (payload.Type === "Exemptions")
                            {
                                if (payload.SubType === "hed__Course__c")
                                {
                                    this.renderTableAsSubjects(component);
                                }
                                else
                                {
                                    this.renderTableAsPlanRequirements(component);
                                }
                            }
                            else
                            {
                                this.renderTableAsEnrolments(component);
                            }
                        }
                        else
                        {
                            if (payload.Message)
                                this.showInfo(component, payload.Message);
                            else
                                this.showInfo(component, "Unknown payload received from server");
                        }
                    }

                    if (dataRows.length > 0)
                        this.clearInfo(component);
                    else
                        this.showInfo(component, "No subjects are available to link at this moment.");

                    component.set("v.data", dataRows);
                }
                else
                {
                    this.clearInfo(component);
                    this.showError(component, response);
                }
            }
        );
        $A.enqueueAction(action);
    },

    renderTableAsEnrolments : function(component)
    {
        component.set('v.columns', [
            { label: 'Subject', fieldName: 'Url', type: 'url', typeAttributes: { label: { fieldName: 'Name', target: '_blank' } } },
            { label: 'Type', fieldName: 'EnrolmentType', type: 'text' },
            { label: 'Subject Offering', fieldName: 'SubjectOfferingUrl', type: 'url', initialWidth: 300, typeAttributes: { label: { fieldName: 'SubjectOffering' } } },
            { label: 'Status', fieldName: 'Status', type: 'text' },
            { label: 'Grade', fieldName: 'Grade', type: 'text' },
            { label: 'Admin Date', fieldName: 'AdminDate', type: 'date' },
            { label: 'Course', fieldName: 'CourseEnrolmentUrl', type: 'url', typeAttributes: { label: { fieldName: 'CourseEnrolment' } } }
        ]);
    },

    renderTableAsEnrolmentsForPortal : function(component)
    {
        component.set('v.columns', [
            { label: 'Subject Name', fieldName: 'Name', type: 'text' },
            { label: 'Term', fieldName: 'Term', type: 'text' },            
            { label: 'Admin Date', fieldName: 'AdminDate', type: 'date' },
            { label: 'Start Date', fieldName: 'StartDate', type: 'date' },
            { label: 'Status', fieldName: 'Status', type: 'text' }
        ]);
    },

    renderTableAsPlanRequirements : function(component)
    {
        component.set('v.columns', [
            { label: 'Course Plan', fieldName: 'Course', type: 'text' },
            { label: 'Plan Requirement', fieldName: 'Category', type: 'text' },
            { label: 'Subject', fieldName: 'Url', type: 'url', typeAttributes: { label: { fieldName: 'SubjectName', target: '_blank' } } },
            { label: 'Code', fieldName: 'SubjectCode', type: 'text' },
            { label: 'Units', fieldName: 'Units', type: 'number', cellAttributes: { alignment: 'left' } },
            { label: 'AQF Level', fieldName: 'AQF', type: 'text' }
        ]);
    },

    renderTableAsSubjects : function(component)
    {
        component.set('v.columns', [
            { label: 'Subject', fieldName: 'Url', type: 'url', typeAttributes: { label: { fieldName: 'SubjectName', target: '_blank' } } },
            { label: 'Code', fieldName: 'SubjectCode', type: 'text' },
            { label: 'Units', fieldName: 'Units', type: 'number', cellAttributes: { alignment: 'left' } },
            { label: 'AQF Level', fieldName: 'AQF', type: 'text' }
        ]);
    },

    saveEnrolments : function(component, param, caseId)
    {
        this.showInfo(component, 'Saving...');
        var saveContactAction = component.get("c.createCaseEnrolments");
        var id = caseId ? caseId : component.get("v.recordId");
        saveContactAction.setParams({
            "caseId": id,
            "param": param
        });

        saveContactAction.setCallback(
            this, function(response)
            {
                var state = response.getState();

                if (state === "SUCCESS")
                {
                    var resultsToast = $A.get("e.force:showToast");

                    resultsToast.setParams({
                        "title": "Case Enrolments Saved",
                        "message": "The selected subjects are linked with Case.",
                        "type": "success"
                    });
                    resultsToast.fire();
                    this.closeUI(component);
                    $A.get('e.force:refreshView').fire();                    
                }
                else
                {
                    this.clearInfo(component);
                    this.showError(component, response);
                }
            }
        );
        $A.enqueueAction(saveContactAction);
    },
    
    closeUI: function(component)
    {
        component.set("v.showData", false);
        component.set("v.data", null);
        this.clearInfo(component);
        this.clearError(component);
    },

    transformData: function(dataRows)
    {
        var newData = [];
        dataRows.forEach(function (item) {
            let newItem = {};

            for (let key in item)
            {
                let value = item[key];

                if (key === 'Id')
                    newItem[key] = value;

                if (key.substr(key.length - 2, 2) === 'Id')
                    newItem[key.substr(0, key.length - 2) + 'Url'] = value ? '/' + value : null;
                else
                    newItem[key] = value;
            };

            newData.push(newItem);
        });

        return newData;
    },

    showError: function(component, response)
    {
        let errors = response.getError();
        let message = 'Unknown error';

        if (errors && Array.isArray(errors) && errors.length > 0)
            message = errors[0].message;

        console.error(message);
        component.set("v.error", message);
        var resultsToast = $A.get("e.force:showToast");
        resultsToast.setParams({
            "title": "Failed To Save Case Enrolments",
            "message": message,
            "type": "error"
        });
        resultsToast.fire();
    },

    clearError: function(component)
    {
        component.set("v.error", "");
    },

    showInfo: function(component, message)
    {
        component.set("v.info", message);
    },

    clearInfo: function(component)
    {
        component.set("v.info", "");
    }
})