({
    init : function(cmp) 
    {
        var action = cmp.get("c.getTopics");
        action.setCallback
        (
            this, 
            function(response) 
            {
                var state = response.getState();     
                
                if (state === "SUCCESS") 
                {
                    var data = response.getReturnValue();                   
                    
                    if (data)                    
                        cmp.set("v.topics", data);                    
                    else if (state === "ERROR" || state === "INCOMPLETE") 
                        console.log(response.getError());                    
                }
            }
        );
        
        $A.enqueueAction(action);
    }
})