({
    doInit : function(component, event, helper) 
    {
        jQuery("document").ready(function()
        {
            jQuery(".icon-menu").click(function()
            {   
                toggleMobileMenu();
            });

            jQuery(".mobile-menu-item").click(function()
            {
                toggleMobileMenu();
            });

            jQuery("#mobileOverlay").click(function()
            {
                toggleMobileMenu();
            });

            function toggleMobileMenu()
            {
                jQuery("nav").toggleClass("toggle-search");
                jQuery(".toggle-menu").toggleClass("nav-open"); 
                jQuery("#mobileOverlay").toggleClass("overlay");
            };
        });

        var action = component.get("c.getUserById");
        action.setCallback(this, function(response)
        {
            var state = response.getState();

            if (state === "SUCCESS")
            {
                var userInfo = response.getReturnValue();
                component.set("v.userInfo", userInfo);

                var url = userInfo.SmallPhotoUrl.substr(0, userInfo.SmallPhotoUrl.length -1);
                component.set("v.photoUrl", url + "M");
                component.set("v.firstName", userInfo.FirstName.substr(0,1));
                component.set("v.lastName", userInfo.LastName.substr(0,1));
            }
        });

        $A.enqueueAction(action);
    }    
})