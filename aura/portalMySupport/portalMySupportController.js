({
    doInit: function(component, event, helper)
    {
        helper.init(component);
    },

    sortCases: function(component, event, helper)
    {
        var id = event.getSource().get("v.name");
        var fieldName = component.find(id).get("v.value");
        helper.sortBy(component, fieldName);
    },

    showNewCaseForm: function(component, event, helper)
    {
        component.set("v.showList", false);
        component.set("v.showForm", true);
    },

    handleCancelNewCase: function(component, event, helper)
    {
        component.set("v.showList", true);
        component.set("v.showForm", false);
    },

    handleNewCase: function(component, event, helper)
    {
        helper.getCases(component);
        component.set("v.showList", true);
        component.set("v.showForm", false);
    }
})