({
    init: function(cmp) 
    {       
        this.initSortFields(cmp);
        this.getCases(cmp);
    },
    
    initSortFields: function(cmp)
    {
        var fields = [
            {name: "Case Number", value: "CaseNumber"},
            {name: "Case Title", value: "Subject"},
            {name: "Case Status", value: "Status"},
            {name: "Officer", value: "OwnerName"},
            {name: "Open Date", value: "CreatedDate"},
            {name: "Closed Date", value: "ClosedDate"}
        ];
        
        cmp.set("v.fields", fields);
    },
    
    getCases: function(cmp)
    {
        var action = cmp.get("c.getMyCases");
        action.setCallback
        (
            this,
            function(response) 
            {
                var state = response.getState();
                if (state === "SUCCESS") 
                {
                    var data = response.getReturnValue();
                    
                    if (data)
                    {
                        var cases = [data.length];
                        for (var i=0; i<data.length; i++)
                        {
                            var entry = new Object();
                            entry.Id = data[i].Id;
                            entry.CaseNumber = (data[i].CaseNumber == null) ? '':data[i].CaseNumber;
                            entry.Subject = (data[i].Subject == null) ? '':data[i].Subject;
                            entry.Status = (data[i].Status == null) ? '':data[i].Status;
                            entry.OwnerName = (data[i].Owner.Name == null) ? '':data[i].Owner.Name;
                            entry.CreatedDate = (data[i].CreatedDate == null) ? '':data[i].CreatedDate;
                            entry.ClosedDate = (data[i].ClosedDate == null) ? '':data[i].ClosedDate;
                            cases[i] = entry;
                        }

                        cmp.set("v.cases", cases);
                    }
                    else if (state === "ERROR" || state === "INCOMPLETE") 
                        console.log(response.getError());
                }
            }
        );
        
        $A.enqueueAction(action);
    },
    
    sortBy: function(cmp, field)
    {
        var cases = cmp.get("v.cases");
        cases.sort(function(a,b)
        {
            var value1 = a[field].toUpperCase();
            var value2 = b[field].toUpperCase();

            if (field == 'CaseNumber')
                return b[field] - a[field];
            else if (field == 'CreatedDate' || field == 'ClosedDate')
            {
                if (value1 < value2) return 1;
                if (value1 > value2) return -1;
                return 0;
            }
            else
            {
                if (value1 < value2) return -1;
                if (value1 > value2) return 1;
                return 0;
            }
        });

        cmp.set("v.cases", cases);
    }
})