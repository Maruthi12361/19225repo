({
    doInit : function(component, event, helper) 
    {
        helper.getContactInfo(component);
        helper.getUserInfo(component);
    }
})