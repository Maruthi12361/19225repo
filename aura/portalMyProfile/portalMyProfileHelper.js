({
    getContactInfo : function(component) 
    {
        var userId = component.get("v.userId");
        var action = component.get("c.getContactByUserId");

        if (userId != null)
            action.setParams({userId: userId});
        
        action.setCallback(this, function(response)
        {
            var state = response.getState();
            
            if (state === "SUCCESS")
            {
                var contact = response.getReturnValue();
                
                if (contact != null)
                {
                    component.set("v.contactInfo", contact); 
                    var address = contact.MailingStreet + "\n" + contact.MailingCity + "\n" + contact.MailingState
                                + " " + contact.MailingPostalCode + "\n" + contact.MailingCountry;
                    component.set("v.address",address);
                }
            }
            else if (state === "ERROR" || state === "INCOMPLETE") 
                console.log(response.getError());
        });
        
        $A.enqueueAction(action); 
    }, 
    
    getUserInfo: function(component)
    {
        var userId = component.get("v.userId");
        var action = component.get("c.getUserById");

        if (userId != null)
            action.setParams({userId: userId});
        
        action.setCallback(this, function(response)
        {
            var state = response.getState();
            
            if (state === "SUCCESS")
            {
                var user = response.getReturnValue();
                component.set("v.userInfo", user);

                var url = user.SmallPhotoUrl.substr(0, user.SmallPhotoUrl.length -1);
                component.set("v.photoUrl", url + "M");
                component.set("v.firstName", user.FirstName.substr(0,1));
                component.set("v.lastName", user.LastName.substr(0,1));
            }
        });

        $A.enqueueAction(action); 
    }
})