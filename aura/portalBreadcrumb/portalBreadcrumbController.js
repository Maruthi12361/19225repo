({
    doInit : function(component, event, helper) 
    {
        var breadcrumbs = [{label: component.get("v.home"), url: component.get("v.home_link")}];
        for(var i=1; i<=5; i++)
        {
            breadcrumbs.push({label: component.get("v.label_" + i), url: component.get("v.link_"+ i)});
        }
        
        component.set("v.breadcrumbs", breadcrumbs);        
    },
    
    doneRendering: function(component, event) 
    {
        var current = component.find("currentURL");
        current.set("v.value", window.location.pathname);
        current.set("v.label", document.title);        
    }
})