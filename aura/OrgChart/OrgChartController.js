({
    afterScriptsLoaded : function(component, event, helper) {
        var action = component.get("c.getUsers");
        var chartElement = component.find("chart").getElement();
        
        var aibTheme = {
            "size":[500,220],
            "toolbarHeight":46,
            "textPoints":[
                {"x":10,"y":200,"width":490},
                {"x":250,"y":40,"width":250},
                {"x":250,"y":65,"width":250},
                {"x":250,"y":90,"width":250},
                {"x":250,"y":115,"width":250},
                {"x":250,"y":140,"width":250}
            ],
            "textPointsNoImage":[
                {"x":10,"y":200,"width":490},
                {"x":10,"y":40,"width":490},
                {"x":10,"y":65,"width":490},
                {"x":10,"y":90,"width":490},
                {"x":10,"y":115,"width":490},
                {"x":10,"y":140,"width":490}
            ],
            "box":"<rect x=\"0\" y=\"0\" height=\"220\" width=\"500\" rx=\"10\" ry=\"10\" class=\"get-box\" />",
            "text":"<text width=\"[width]\" class=\"get-text get-text-[index]\" x=\"[x]\" y=\"[y]\">[text]</text>",
            "image":"<clipPath id=\"getVivianClip\"><polygon class=\"get-box\" points=\"20,70 75,-20 185,-20 240,70 185,160 75,160\"/></clipPath>" +
            "<image preserveAspectRatio=\"xMidYMid slice\" clip-path=\"url(#getVivianClip)\" xlink:href=\"[href]\" x=\"-20\" y=\"-20\" height=\"200\" width=\"300\"/>"
        }
        
        window.getOrgChart.themes.aib = aibTheme;
        
        action.setCallback(this, function(response){
            var users = response.getReturnValue();
            if(users) {
                var tree = helper.createTreeFromArray(users, "Id", "ManagerId");
                var sortedUsers = [];
                helper.createArrayFromTree(tree, sortedUsers);
                
                var idMap = {};
                for (var i = 1; i < sortedUsers.length + 1; ++i) {
                    var user = sortedUsers[i - 1];
                    idMap[user.Id] = i;
                }
                
                var userMap = sortedUsers.map(function(user) {
                    var id = idMap[user.Id];
                    var parentId = user.ManagerId ? 
                        idMap[user.ManagerId] 
                    : null;
                    
                    return {
                        id: id, 
                        parentId: parentId,
                        Name: user.Name,
                        Extension: user.Extension ? "#" + user.Extension : null,
                        Title: user.Title,
                        Department: user.Department,
                        SmallPhotoUrl: user.SmallPhotoUrl,
                        FullPhotoUrl: user.FullPhotoUrl,
                        FireWarden: user["Fire_Warden__c"],
                        FirstAid: user["First_Aid_Officer__c"],
                        ContactOfficer: user["Contact_Officer__c"]
                    };
                });
                
                try {
                    
                    var orgChart = new getOrgChart(chartElement, {
                        orientation: getOrgChart.RO_LEFT,
                        theme: "aib",
                        scale: "auto",
                        enableEdit: false,
                        enableDetailsView: false,
                        enablePrint: true,
                        expandToLevel: component.get("v.expandToLevel"),
                        color: component.get("v.color"),
                        primaryFields: ["Name", "Department", "Title", "Extension"],
                        photoFields: ["FullPhotoUrl"],
                        linkType: component.get("v.linkType"),
                        idField: "id",
                        parentIdField: "parentId",
                        levelSeparation: component.get("v.levelSeparation"),
                        subtreeSeparation: component.get("v.subtreeSeparation"),
                        dataSource: userMap,
                        renderNodeEvent: function(sender, args) {
                            var flagMap = {
                                "FireWarden": "https://aib-hive.force.com/s/fire-icon.png",
                                "FirstAid": "https://aib-hive.force.com/s/firstaid.png",
                                "ContactOfficer": "https://aib-hive.force.com/s/heart_red.png"
                            };
                            
                            var template = '<image preserveAspectRatio="xMidYMid slice" xlink:href="[href]" x="[x]" y="[y]" height="32" width="32" />',
                                x = 250,
                                y = 115;
                            
                            for(var i in flagMap) {
                                if(args.node.data[i]) {
                                    var flag = template
                                        .replace("[href]", flagMap[i])
                                        .replace("[x]", x)
                                        .replace("[y]", y);
                                    
                                    args.content.splice(args.content.length - 2, 0, flag);
                                    x += 40;
                                }
                            }
                        }
                    });
                } catch(err) {
                    console.log(err);
                    throw err;
                }
            }
        });
        
        $A.enqueueAction(action);
    }
})