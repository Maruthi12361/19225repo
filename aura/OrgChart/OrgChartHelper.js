({
    createTreeFromArray : function(data, idProperty, parentProperty) {
        var map = {};
        var tree = [];
        
        for(var i = 0; i < data.length; ++i) {
            var datum = data[i];
            map[datum[idProperty]] = datum;
        }
        
        for(var i = 0; i < data.length; ++i) {
            var datum = data[i];
            
            if(!datum[parentProperty]) {
                tree.push(datum);
            } else {
                var parent = map[datum[parentProperty]];
                parent.Children = parent.Children || [];
                parent.Children.push(datum);
            }
        }
        
        return tree;
    },
    createArrayFromTree: function(tree, arr) {
        var children = [];
        for(var i = 0; i < tree.length; ++i) {
            var datum = tree[i];
               arr.push(datum);
            if(datum.Children && datum.Children.length > 0) {
                children.push.apply(children, datum.Children);
            }
        }
        
        if(children.length > 0) {
            this.createArrayFromTree(children, arr);
        }
    }
})