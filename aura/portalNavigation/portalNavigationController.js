({        
    onClick : function(component, event, helper) 
    {
        var id = event.target.dataset.menuItemId;
        if (id)
        {
            component.getSuper().navigate(id);
        }
    },
    
    showSearch : function(component, event, helper) 
    {        
        var ele = document.getElementById("divSearch");
        ele.style.display='block';
        document.getElementById("overlay").style.display = "block";        
    },
    
    hideSearch: function(component, event, helper)
    {
        var ele = document.getElementById("divSearch");
        ele.style.display='none';        
        document.getElementById("overlay").style.display = "none";
    },
           
    handleCloseSpan: function(component, event, helper)
    {       
        // Get the modal
        var modal = document.getElementById('myModal');               
        
        modal.style.display = "none";
    }
})