({
    afterScriptsLoaded : function(component, event, helper) {  
        var j$ = jQuery.noConflict();
        var sliderElement = component.find("slidejs").getElement();
        
        try {
            j$(sliderElement).slidesjs({
                width: component.get("v.width"),
                height: component.get("v.height"),
                start: component.get("v.startingSlide"),
                play: {
                    active: false,
                    effect: component.get("v.animationType"),
                    interval: component.get("v.slideInterval"),
                    auto: component.get("v.autoplay"),
                    pauseOnHover: component.get("v.pauseOnHover")
                },
                effect: {
                    slide: {
                        speed: component.get("v.animationSpeed")
                    },
                    fade: {
                        speed: component.get("v.animationSpeed"),
                        crossfade: true
                    }
                }
            });
        } catch(err) {
            console.log(err);
            throw err;
        }
    }
})