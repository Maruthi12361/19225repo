({
    initialize : function(component){
        var action = component.get("c.getUser");
        action.setCallback(this, function(response){
            var user = response.getReturnValue();
            if(user) {
                component.set('v.user', user);
            }
        });
        $A.enqueueAction(action);
    }
})