({
    doInit: function(component, event, helper)
    {
        var userId = $A.get("$SObjectType.CurrentUser.Id");
        component.set("v.userId", userId);
    },

    handleSubmit: function(component, event, helper)
    {
        event.preventDefault();
        component.set("v.showSpinner", true);
        component.set("v.disabled", true);

        var fields = event.getParam("fields");
        fields.Subject = component.find("CaseSubject").get("v.value");
        fields.Description = component.find("CaseDescription").get("v.value");

        component.find('NewCaseForm').submit(fields);
    },

    handleSuccess: function(component, event, helper)
    {
        var payload = event.getParams().response;
        var id = payload.id;
        var files = helper.getFileIds(component);
        var action = component.get("c.getNewCaseInfo");

        action.setParams({caseId : id, fileIds: files});
        action.setCallback
        (
            this,
            function(response)
            {
                var state = response.getState();

                if (state === "SUCCESS")
                {
                    var data = response.getReturnValue();

                    component.set("v.caseNumber", data.CaseNumber);
                    component.set("v.saved",true);
                    component.set("v.files", []);
                }
                else if (state === "ERROR" || state === "INCOMPLETE")
                {
                    console.log(response.getError());
                }

                component.set("v.showSpinner",false);
                component.set("v.disabled",false);
            }
        );
        $A.enqueueAction(action);

        var evtCreated = component.getEvent("createdNewCase");
        evtCreated.setParams({"context" : component.get("v.parentName")});
        evtCreated.fire();
        var lwc = component.find('LwcCaseEnrolment');
        lwc.handleCreate(id)
    },

    cancelEvent: function(component, event, helper)
    {
        var ids = helper.getFileIds(component);

        if (ids.length > 0)
            helper.removeFiles(component, ids, true);

        var evtCancel = component.getEvent("cancelNewCase");
        evtCancel.setParams({"context" : component.get("v.parentName")});
        evtCancel.fire();
    },

    uploaded: function(component, event, helper)
    {
        // This will contain the List of File uploaded data and status
        var uploadedFiles = event.getParam("files");
        var files = component.get("v.files");
        
        if (files.length == 0)
            files = uploadedFiles;
        else
        {
            for (var i=0; i < uploadedFiles.length; i++)
                files.push(uploadedFiles[i]);
        }
        component.set("v.files", files);
    },

    removeFile: function(component, event, helper)
    {
        var file_ids = [event.getSource().get("v.id")];
        helper.removeFiles(component, file_ids, false);
    }
})