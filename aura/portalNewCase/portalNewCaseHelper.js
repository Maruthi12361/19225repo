({
    removeFiles : function(component, file_ids, isAllFiles)
    {        
        var action = component.get("c.deleteFiles");        
        
        action.setParams({file_ids : file_ids});             
        action.setCallback
        (
            this, 
            function(response) 
            {
                var state = response.getState();     
                
                if (state === "SUCCESS") 
                {                                            
                    var allFiles = component.get("v.files");
                    
                    if(isAllFiles)
                        allFiles.clear();
                       else
                    {
                        for(var i=0; i< allFiles.length; i++)
                        {                        
                            if(allFiles[i].documentId === file_ids[0])
                                allFiles.splice(i,1);                            
                        }                                            
                    }                    
                    
                    component.set("v.files",allFiles);                    
                }
                else if (state === "ERROR" || state === "INCOMPLETE") 
                        console.log(response.getError());                 
            }
        );        
        $A.enqueueAction(action);
    },
    
    getFileIds: function(component)
    {
        var allFiles = component.get("v.files");
        var file_ids = new Array(allFiles.length);
        
        if (allFiles.length > 0)
        {            
            for(var i=0; i<allFiles.length; i++)
                file_ids[i] = allFiles[i].documentId;                     
        }
        
        return file_ids;
    }
})