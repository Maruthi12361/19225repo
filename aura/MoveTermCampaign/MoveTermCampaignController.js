({
    init : function(component, event, helper)
    {
        var id = component.get("v.recordId");
        helper.getData(component, id);
    },

    handleLoad : function(component, event, helper) 
    {
        component.set('v.showSpinner', false);
    },

    handleSubmit : function(component, event, helper) 
    {
        event.preventDefault();
        helper.disableForm(component);
        var fields = event.getParam("fields");

        fields.TermMoveReason__c = component.find("TermMoveReason").get("v.value");
        fields.CampaignId = component.find("NewCampaignId").get("v.value");
        fields.TermMoveCount__c = !fields.TermMoveCount__c ? 1 : fields.TermMoveCount__c + 1;
        fields.TermMoveLastModified__c = new Date().toISOString();

        component.find('moveCampaignForm').submit(fields);
    },

    handleError : function(component, event, helper) 
    {
        // errors are handled by lightning:inputField and lightning:messages
        // so this just hides the spinner
        helper.enableForm(component);
    },

    handleSuccess : function(component, event, helper) 
    {
        helper.enableForm(component);
        component.set('v.edit', true);

        // Display a successful message in a "toast"
        var resultsToast = $A.get("e.force:showToast");
        resultsToast.setParams
        ({
            "type": "success",
            "message": "Campaign is successfully moved!."
        });
        resultsToast.fire();

        helper.navigateToDetails(component);
    },

    handleMoveButtonClick : function(component, event, helper)
    {
        component.set('v.edit', false);
    },

    handleCancelButtonClick : function(component, event, helper)
    {
        component.set('v.edit', true);
    },

    handleCampaignChange : function(component, event, helper)
    {
        helper.enableSaveButton(component);
    },

    handleTermChange : function(component, event, helper)
    {
        helper.enableSaveButton(component);
    }
})