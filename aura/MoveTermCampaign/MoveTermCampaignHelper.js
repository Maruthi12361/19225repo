({
    getData : function(component, opportunityId)
    {
        var action = component.get("c.getMoveTermCampaignData");

        action.setParams({ id : opportunityId });
        action.setCallback(this, function(response)
        {
            var state = response.getState();
            if (state === "SUCCESS")
            {
                var result = JSON.parse(response.getReturnValue());
                if (result)
                {
                    if (result.Options && result.Options.TermMoveReasons)
                        component.set('v.termMoveReasons', result.Options.TermMoveReasons);

                    if (result.Campaigns)
                        component.set('v.campaigns', result.Campaigns);
                }
            }

            if (state === "ERROR")
            {
                var resultsToast = $A.get("e.force:showToast");
                resultsToast.setParams
                ({
                    "type": "error",
                    "message": response.getError()
                });
                resultsToast.fire();
            }
        });

        $A.enqueueAction(action);
    },

    navigateToDetails : function(component)
    {
        var navigateEvent = $A.get("e.force:navigateToSObject");
        navigateEvent.setParams
        ({
          "recordId": component.get("v.recordId"),
          "slideDevName": "detail"
        });
        navigateEvent.fire();
    },

    enableSaveButton : function(component)
    {
        if (component.find("TermMoveReason").get("v.value") && component.find("NewCampaignId").get("v.value"))
            component.set("v.disabledButton", false);
        else
            component.set("v.disabledButton", true);
    },

    enableForm : function(component)
    {
        component.set('v.showSpinner', false);
        component.set('v.disabled', false);
        component.set('v.disabledButton', false);
    },

    disableForm : function(component)
    {
        component.set('v.disabled', true);
        component.set('v.disabledButton', true);
        component.set('v.showSpinner', true);
    }
})