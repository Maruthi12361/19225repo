({
    // Fetch the accounts from the Apex controller
    getList: function(component) {
        var action = component.get('c.getList');
        
        // Set up the callback
        var self = this;
        action.setCallback(this, function(actionResult) {
            component.set('v.chatterGroups', actionResult.getReturnValue());
        });
        $A.enqueueAction(action);
    }
})