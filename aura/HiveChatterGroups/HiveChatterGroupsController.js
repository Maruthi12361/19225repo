({
    doInit: function(component, event, helper) {      
        // Fetch the account list from the Apex controller   
        helper.getList(component);
    },
    show: function(component, event) {
        var id = event.currentTarget.dataset.id;
        var showEvent = $A.get("e.force:navigateToSObject");
        showEvent.setParams({
            "recordId": id
        });
        showEvent.fire();
    },
    showTooltip : function(component, event, helper) {
        var toggleText = component.find("tooltip");
        component.set("v.style", "position:absolute;top:" + (event.pageY - 23) + "px;left:" + (event.pageX + 23) + "px;");
        component.set("v.tooltipText", event.currentTarget.dataset.name + " (Public access, " + event.currentTarget.dataset.members + " member(s))");
        $A.util.toggleClass(toggleText, "toggle");
    },
    hideTooltip : function(component, event, helper) {
        var toggleText = component.find("tooltip");
        $A.util.toggleClass(toggleText, "toggle");
    }
})