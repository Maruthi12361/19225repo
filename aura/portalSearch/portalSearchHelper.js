({
    navigateToSearchResult : function(component, event) 
    {
        var searchText = component.get('v.searchText');
        var ele = document.getElementById("divSearch");
        ele.style.display='none';        
        document.getElementById("overlay").style.display = "none";

        jQuery("nav").removeClass("toggle-search");
        jQuery(".toggle-menu").removeClass("nav-open"); 
        jQuery("#mobileOverlay").removeClass("overlay");

        var navEvt = $A.get("e.force:navigateToURL");
        navEvt.setParams({"url": "/global-search/" + searchText});
        navEvt.fire();
    }
})