({
    handleClick : function(component, event, helper) 
    {
        helper.navigateToSearchResult(component, event);          
    },
    
    handleKeyPress: function(component, event, helper)
    {
        if (event.which == 13)
        {            
            helper.navigateToSearchResult(component, event); 
        }
    }
})